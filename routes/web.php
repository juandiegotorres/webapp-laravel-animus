<?php

use App\Client;
use App\Envase;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MessageNotificationController;
use App\PriceList;
use App\Product;
use App\RawMaterial;
use GuzzleHttp\Psr7\Request;

Auth::routes();

Route::middleware('auth')->group(function () {
    //


    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/grafico-ventas', 'HomeController@graficoVentas')->name('grafico.ventas');
    Route::get('/grafico-compras', 'HomeController@graficoCompras')->name('grafico.compras');
    Route::get('/calendario-cheques', 'HomeController@calendarioCheques')->name('calendario.cheques');

    //================================================   Clientes  ===========================================================
    Route::get('/clientes', 'ClientController@index')->name('clients.index');
    Route::get('/clientes/create', 'ClientController@create')->name('clients.create');
    Route::post('/clientes', 'ClientController@store')->name('clients.store');
    Route::get('/clientes/{client}', 'ClientController@show')->name('clients.show');
    Route::get('/clientes/{client}/edit', 'ClientController@edit')->name('clients.edit');
    Route::put('/clientes/{client}', 'ClientController@update')->name('clients.update');
    Route::put('/clientes/{client}/eliminar', 'ClientController@delete')->name('clients.delete');
    //Obtener localidades para cargar select en clients/create.blade.php
    Route::post('/localidades/{id_provincia}', 'ClientController@obtenerLocalidades');

    //================================================   Proveedores  ===========================================================
    Route::get('/listar-proveedores/{tipo?}', 'ProviderController@index')->name('providers.index');
    Route::get('/proveedores/create', 'ProviderController@create')->name('providers.create');
    Route::post('/proveedores', 'ProviderController@store')->name('providers.store');
    Route::get('/proveedores/{provider}', 'ProviderController@show')->name('providers.show');
    Route::get('/proveedores/{provider}/edit', 'ProviderController@edit')->name('providers.edit');
    Route::put('/proveedores/{provider}', 'ProviderController@update')->name('providers.update');
    Route::put('/proveedores/{provider}/eliminar', 'ProviderController@delete')->name('providers.delete');
    Route::get('/proveedores-json', 'ProviderController@listar')->name('providers.json');
    Route::get('/proveedores-tipo/{tipo}', 'ProviderController@tipoProveedor');

    //================================================   Empleados   ===========================================================
    Route::get('/empleados', 'EmployeeController@index')->name('employees.index');
    Route::get('/empleados/create', 'EmployeeController@create')->name('employees.create');
    Route::post('/empleados', 'EmployeeController@store')->name('employees.store');
    Route::get('/empleados/{employee}', 'EmployeeController@show')->name('employees.show');
    Route::get('/empleados/{employee}/edit', 'EmployeeController@edit')->name('employees.edit');
    Route::put('/empleados/{employee}', 'EmployeeController@update')->name('employees.update');
    Route::put('/empleados/{employee}/eliminar', 'EmployeeController@delete')->name('employees.delete');
    Route::get('/empleados/cargos/json', 'EmployeeController@obtenerCargos');

    //================================================    Cargos   ===========================================================
    Route::get('/cargos', 'ChargeController@index')->name('charges.index');
    Route::post('/cargos', 'ChargeController@store')->name('charges.store');
    Route::put('/cargos/update', 'ChargeController@update')->name('charges.update');
    Route::put('/cargos/{charge}/eliminar', 'ChargeController@delete')->name('charges.delete');

    //================================================Materias Primas===========================================================
    Route::get('/materias-primas', 'RawMaterialController@index')->name('raw-material.index');
    Route::get('/materias-primas-', 'RawMaterialController@index')->name('raw-material.index2');
    Route::post('/materias-primas', 'RawMaterialController@store')->name('raw-material.store');
    Route::post('/materias-primas/from-compra', 'RawMaterialController@storeFromPurchase')->name('purchase-raw-material.storeFromPurchase');
    Route::put('/materias-primas/update', 'RawMaterialController@update')->name('raw-material.update');
    Route::put('/materias-primas/{rawMaterial}/eliminar', 'RawMaterialController@delete')->name('raw-material.delete');

    //================================================Insumos-Servicios==========================================================
    Route::get('/insumos-servicios', 'SupplieServiceController@index')->name('supplie-service.index');
    Route::post('/insumos-servicios', 'SupplieServiceController@store')->name('supplie-service.store');
    Route::put('/insumos-servicios/update', 'SupplieServiceController@update')->name('supplie-service.update');
    Route::put('/insumo/eliminar/{insumo}', 'SupplieServiceController@deleteInsumo')->name('supplie.delete');
    Route::put('/servicio/eliminar/{insumo}', 'SupplieServiceController@deleteServicio')->name('service.delete');


    route::get('/obtener-insumos-dt', 'SupplieServiceController@dtInsumos')->name('dt.insumos');
    route::get('/obtener-servicios-dt', 'SupplieServiceController@dtServicios')->name('dt.servicios');
    //================================================   Compras    ===========================================================
    //Devuelve la vista previa a la compra donde se elige si se va a comprar materia prima o Insumo/servicic
    Route::get('/compras', 'PurchaseController@index')->name('purchase.index');
    Route::get('/compras/{tipoCompra}/{idProveedor}', 'PurchaseController@tipoCompra')->name('purchase.tipoCompra');

    //=========================================== Compra de Materias Primas ====================================================
    Route::get('/materias-primas/compra/{id_proveedor}', 'PurchaseRawMaterialController@index')->name('purchase-raw-material.index');
    Route::get('/materias-primas/compra/ingreso-mp/{rawMaterialEntry}', 'PurchaseRawMaterialController@indexDesdeIngresoMP')->name('compraMP-desdeIngreso.index');
    Route::post('/materias-primas/confirmacion-compra', 'PurchaseRawMaterialController@store')->name('purchase-raw-material.store');
    Route::post('/materias-primas/ingreso-mp/confirmacion-compra', 'PurchaseRawMaterialController@storeDesdeIngresoMP')->name('compraMP-desdeIngreso.store');
    Route::get('/materias-primas/historial-compra/{purchaseRawMaterial}', 'PurchaseRawMaterialController@show')->name('purchase-raw-material.show');

    //Proveedores de materia prima
    Route::get('/materias-primas/proveedores-mp', 'PurchaseRawMaterialController@proveedoresMateriaPrima');
    //Obtener materias primas para llenar el select
    Route::get('/materias-primas-json', 'PurchaseRawMaterialController@obtenerMateriasPrimas');

    //========================================= Compra de Insumos - Servicios ==================================================
    Route::get('/insumos-servicios/compra/{id_proveedor}', 'PurchaseSupplieServiceController@index')->name('purchase-supplie-service.index');
    Route::post('/insumos-servicios/confirmacion-compra/insumo', 'PurchaseSupplieServiceController@storeInsumo')->name('purchase-supplie-service.store-insumo');
    Route::post('/insumos-servicios/confirmacion-compra/servicio', 'PurchaseSupplieServiceController@storeServicio')->name('purchase-supplie-service.store-servicio');
    Route::get('/insumos-servicios/historial-compra/{purchaseSupplieService}', 'PurchaseSupplieServiceController@show')->name('purchase-supplie-service.show');
    Route::post('/insumos-servicios/from-compra', 'SupplieServiceController@storeFromPurchase')->name('purchase-supplie-service.storeFromPurchase');

    //Proveedores de insumos - servicios
    Route::get('/insumos-json', 'PurchaseSupplieServiceController@obtenerInsumos');
    Route::get('/servicios-json', 'PurchaseSupplieServiceController@obtenerServicios');

    //========================================= Compra de Indumentarias ==================================================
    Route::post('/indumentarias/confirmacion-compra', 'PurchaseIndumentaryController@store')->name('purchase-indumentary.store');
    Route::get('/indumentarias/historial-compra/{purchaseIndumentary}', 'PurchaseIndumentaryController@show')->name('purchase-indumentary.show');


    //============================================= Historial de Compras =====================================================
    Route::get('/historial-compras', 'PurchaseHistoryController@index')->name('purchase-history.index');
    //Obtiene un JSON con el historial de todas las compras de materia prima
    Route::get('/historial-compras/materias-primas', 'PurchaseHistoryController@obtenerHistorialMP')->name('purchase-history.mp');
    //Obtiene un JSON con el historial de todas las compras de insumos y servicios
    Route::get('/historial-compras/insumos', 'PurchaseHistoryController@obtenerHistorialInsumos')->name('purchase-history.insumos');

    Route::get('/historial-compras/servicios', 'PurchaseHistoryController@obtenerHistorialServicios')->name('purchase-history.servicios');

    Route::get('/historial-compras/indumentarias', 'PurchaseHistoryController@obtenerHistorialIndumentarias')->name('purchase-history.indumentarias');
    Route::post('/historial-compras/filtrar', 'PurchaseHistoryController@filtrar')->name('historial-compras.filtrar');
    //Devuelve un JSON con un filtro de fechas
    Route::get('/filtro/fechaInicio={fechaInicio?}&fechaFin={fechaFin}&proveedor={idproveedor?}&tipo={tipo?}', 'PurchaseHistoryController@filtroMP');
    Route::get('/imprimir-compra/materia-prima/{idCompra}', 'PurchaseHistoryController@imprimirCompraMP')->name('imprimir.compra');
    Route::post('/historial-compras/cancelar-compra', 'PurchaseHistoryController@cancelarCompra')->name('cancelar.compra');

    //============================================= Usuarios =====================================================
    Route::post('/comprobar-usuario', 'UserController@comprobarUsuario')->name('users.password-check');
    Route::get('/usuarios', 'UserController@index')->name('users.index');
    Route::post('/usuario/crear', 'UserController@create')->name('users.create');
    Route::get('/obtenerUsuarios', 'UserController@obtenerUsuarios')->name('users.users');
    Route::post('/usuario/cambiar-password', 'UserController@changePassword')->name('users.change');
    Route::post('/usuario/asignar-roles', 'UserController@asignarRoles')->name('users.roles');
    // Route::get('/usuario/roles/{idUsuario}', 'UserController@rolesDelUsuario')->name('users.has-roles');


    //========================================= Entregas Indumentaria ==================================================
    Route::get('/entregar-indumentaria/{idEmpleado}', 'IndumentaryDistributionController@index')->name('indumentary-distribution.index');
    Route::post('/entregar-indumentaria', 'IndumentaryDistributionController@store')->name('indumentary-distribution.store');
    Route::get('/obtener-empleados-json', 'IndumentaryDistributionController@obtenerEmpleados');

    //========================================= Historial Entregas Insumos ==================================================
    Route::get('/historial-entrega-indumentaria', 'DistributionHistoryController@index')->name('distribution-history.index');
    Route::get('/historial-entrega-indumentaria/empleado/{idEmpleado?}', 'DistributionHistoryController@obtenerHistorial');
    Route::put('/historial-entrega-indumentaria/eliminar/{id}', 'DistributionHistoryController@delete')->name('distribution-history.delete');

    //========================================= Sueldos ==================================================
    Route::get('/sueldos', 'SalaryController@index')->name('salaries.index');
    Route::get('/sueldos/dt', 'SalaryController@indexDT');
    Route::post('/sueldos/filtrar', 'SalaryController@filtrar')->name('salaries.filtrar');
    Route::get('/sueldos/agregar', 'SalaryController@create')->name('salaries.create');
    Route::post('/sueldos/agregar', 'SalaryController@store')->name('salaries.store');
    Route::get('/sueldos/{salary}/edit', 'SalaryController@edit')->name('salaries.edit');
    Route::put('/sueldos/{salary}', 'SalaryController@update')->name('salaries.update');
    Route::put('/sueldos/{salary}/eliminar', 'SalaryController@delete')->name('salaries.delete');

    //========================================= Pago de Sueldos ==================================================
    Route::get('/pago-sueldos', 'SalaryPaymentController@index')->name('salary-payments.index');
    Route::get('/pago-sueldos/dt', 'SalaryPaymentController@indexDT');
    Route::post('/pago-sueldos/filtrar', 'SalaryPaymentController@filtrar')->name('salary-payments.filtrar');
    Route::get('/pago-sueldos/agregar', 'SalaryPaymentController@create')->name('salary-payments.create');
    Route::post('/pago-sueldos/agregar', 'SalaryPaymentController@store')->name('salary-payments.store');
    Route::get('/pago-sueldos/{salaryPayment}/edit', 'SalaryPaymentController@edit')->name('salary-payments.edit');
    Route::put('/pago-sueldos/{salaryPayment}', 'SalaryPaymentController@update')->name('salary-payments.update');
    Route::put('/pago-sueldos/{salaryPayment}/eliminar', 'SalaryPaymentController@delete')->name('salary-payments.delete');

    //========================================= Balance Sueldos ==================================================
    Route::get('/balance-sueldos', 'EmployeeSalaryBalanceController@index')->name('salary-balance.index');
    Route::get('/balance-sueldos/dt/empleado/{employee_id?}', 'EmployeeSalaryBalanceController@indexDT');
    Route::get('/balance-sueldos/dt/empleado={employee_id?}&fechaInicio={fechaInicio?}&fechaFin={fechaFin?}', 'EmployeeSalaryBalanceController@filtrarFechas');
    // Route::get('/balance-sueldos/filtro/empleado={idempleado?}&fechaInicio={fechaInicio?}&fechaFin={fechaFin?}', 'BalanceCompraPagosController@filtrarFechas');
    //========================================= Indumentaria ==================================================
    Route::get('/indumentaria', 'IndumentaryController@index')->name('indumentary.index');
    Route::get('/indumentarias-json', 'IndumentaryController@indumentariasjson')->name('indumentary.json');
    Route::post('/indumentaria', 'IndumentaryController@store')->name('indumentary.store');
    Route::put('/indumentaria/{indumentary}', 'IndumentaryController@update')->name('indumentary.update');
    Route::put('/indumentaria/delete/{indumentary}', 'IndumentaryController@delete')->name('indumentary.delete');

    //========================================= Procuctos ==================================================
    Route::get('/productos', 'ProductController@index')->name('products.index');
    Route::get('/dtproductos', 'ProductController@datatable');
    Route::get('/productoss-json', 'ProductController@productosJson')->name('products.json');
    Route::post('/productos', 'ProductController@store')->name('products.store');
    Route::put('/productos/{product}', 'ProductController@update')->name('products.update');
    Route::put('/productos/delete/{product}', 'ProductController@delete')->name('products.delete');
    Route::get('/productos/mostrar-variantes/{idproducto}', 'ProductController@mostrarVariantes')->name('products.mostrarVariantes');
    Route::put('/productos/modificar-variante/{variante}', 'ProductController@updateVariante')->name('variantes.update');
    Route::put('/productos/eliminar-variante/{variante}', 'ProductController@deleteVariante')->name('variantes.delete');

    //========================================= Listas Precios ==================================================
    Route::get('/listas-precios', 'PriceListController@index')->name('price-lists.index');
    Route::get('/listas-precios-', 'PriceListController@index')->name('price-lists.index2');
    Route::get('/dtListaPrecios', 'PriceListController@datatable');
    Route::post('/listas-precios', 'PriceListController@store')->name('price-lists.store');
    Route::put('/listas-precios/update/{priceList}', 'PriceListController@update')->name('price-lists.update');
    Route::put('/listas-precios/delete/{priceList}', 'PriceListController@delete')->name('products.delete');

    //========================================= Detalle listas Precios ==================================================
    Route::get('/detalle-lista-precio/{priceList}', 'DetailPriceListController@index')->name('detail-price-lists.index');
    Route::get('/dtDetalleListaPrecios/{priceList}', 'DetailPriceListController@datatable');
    Route::post('/producto-lista-precio', 'DetailPriceListController@store')->name('detail-price-lists.store');
    Route::put('/modificar-precio-producto/{detailPriceList}', 'DetailPriceListController@update')->name('detail-price-lists.update');
    Route::delete('/eliminar-precio-producto/{priceList}', 'DetailPriceListController@destroy');
    Route::get('/obtenerVariantes/{idproducto}', 'DetailPriceListController@obtenerVariantes');

    //========================================= Cheques ==================================================
    Route::get('/cheques', 'ChequeController@index')->name('cheques.index');
    Route::get('/dtcheques/{emitidoRecibido}', 'ChequeController@datatable');
    Route::get('/cheques/mostrar/{cheque}', 'ChequeController@show')->name('cheques.show');
    Route::get('/cheques/mostrar-json/{cheque}', 'ChequeController@showJson');
    Route::get('/cheques/crear/{emitidoRecibido}', 'ChequeController@create')->name('cheques.create');
    Route::post('/cheques', 'ChequeController@store')->name('cheques.store');
    Route::get('/cheques/modificar/{cheque}', 'ChequeController@edit')->name('cheques.edit');
    Route::put('/cheques/modificar/{cheque}', 'ChequeController@update')->name('cheques.update');
    Route::put('/cheques/eliminar/{cheque}', 'ChequeController@delete')->name('cheques.delete');
    Route::put('/cheques/cambiar-estado/{cheque}', 'ChequeController@cambiarEstado')->name('cheques.cambiarEstado');

    //========================================= Bancos ==================================================
    Route::get('/bancos', 'BankController@index')->name('banks.index');
    Route::post('/bancos', 'BankController@store')->name('banks.store');
    Route::put('/bancos/modificar/{bank}', 'BankController@update')->name('banks.update');
    // Route::put('/bancos/eliminar/{cheque}', 'BankController@delete')->name('banks.delete');

    //========================================= Ingreso de materia prima ==================================================
    Route::get('/ingreso/materia-prima', 'RawMaterialEntryController@index')->name('ingresoMP.index');
    Route::get('/ingreso/materia-prima/dt', 'RawMaterialEntryController@datatable');
    Route::get('/ingreso/materia-prima/crear/{idProveedor?}', 'RawMaterialEntryController@create')->name('ingresoMP.create');
    Route::get('/ingreso/materia-prima/mostrar/{rawMaterialEntry}', 'RawMaterialEntryController@show')->name('ingresoMP.show');
    Route::post('/ingreso/materia-prima', 'RawMaterialEntryController@store')->name('ingresoMP.store');
    Route::get('/ingreso/materia-prima/descartar/{rawMaterialEntry}', 'RawMaterialEntryController@showDescartar')->name('ingresoMP.descartar');
    Route::post('/ingreso/materia-prima/descartar/{rawMaterialEntry}', 'RawMaterialEntryController@storeDescartar')->name('ingresoMP.store-descartar');
    Route::delete('/ingreso/materia-prima/eliminar/descarte/{descarte}/{ingreso}', 'RawMaterialEntryController@eliminarDescarte')->name('ingresoMP.eliminar-descarte');
    // Route::get('/ingreso/materia-prima/modificar/{cheque}', 'RawMaterialEntryController@edit')->name('ingresoMP.edit');
    // Route::put('/ingreso/materia-prima/modificar/{cheque}', 'RawMaterialEntryController@update')->name('ingresoMP.update');
    Route::put('/ingreso/materia-prima/eliminar/{ingreso}', 'RawMaterialEntryController@delete')->name('ingresoMP.delete');
    Route::post('/ingreso/materia-prima/filtrar', 'RawMaterialEntryController@filtrar')->name('ingresoMP.filtrar');

    //========================================= Roles ========================================
    Route::get('/roles', 'RoleController@index')->name('roles.index');
    Route::get('/dtRoles', 'RoleController@dtroles')->name('roles.dt');
    Route::post('/roles', 'RoleController@store')->name('roles.store');
    Route::put('/rol/modificar/{role}', 'RoleController@update')->name('role.update');
    Route::put('/rol/eliminar/{role}', 'RoleController@destroy')->name('role.destroy');
    Route::get('/permisos/{idRol}', 'RoleController@mostrarPermisos')->name('permisos.index');
    Route::put('/roles/permisos/{role}', 'RoleController@editarPermisos')->name('permisos.update');

    //======================================== Facturas ======================================
    Route::get('/facturas', 'FacturaController@index')->name('facturas.index');
    Route::post('/facturas/dt', 'FacturaController@datatable')->name('facturas.datatable');
    Route::get('/facturas/crear/{emitida_recibida}', 'FacturaController@create')->name('facturas.create');
    Route::get('/facturas/mostrar/{id}', 'FacturaController@show');
    Route::post('/facturas', 'FacturaController@store')->name('facturas.store');
    Route::get('/facturas/edit/{factura}', 'FacturaController@edit')->name('facturas.edit');
    Route::put('/facturas/modificar/{factura}', 'FacturaController@update')->name('facturas.update');
    Route::put('/facturas/eliminar/{factura}', 'FacturaController@delete')->name('facturas.delete');

    //======================================== Balance Impositivo =============================
    Route::get('/balance-impositivo', 'BalanceImpositivoController@index')->name('balance-impositivo.index');
    Route::post('/balance-impositivo', 'BalanceImpositivoController@datatable')->name('balance-impositivo.datatable');

    //======================================== Descarozado =============================
    Route::get('/entregas-descarozado', 'DescarozadoController@index')->name('entrega-descarozado.index');
    Route::get('/entregas-descarozado/datatable', 'DescarozadoController@datatable')->name('entrega-descarozado.datatable');
    Route::post('/entregas-descarozado/create', 'DescarozadoController@create')->name('entrega-descarozado.create');
    Route::post('/entregas-descarozado', 'DescarozadoController@store')->name('entrega-descarozado.store');
    Route::get('/entregas-descarozado/mostrar/{id}', 'DescarozadoController@show')->name('entrega-descarozado.show');
    Route::get('/entregas-descarozado/edit/{descarozado}', 'DescarozadoController@edit')->name('entrega-descarozado.edit');
    Route::post('/entregas-descarozado/update/{descarozado}', 'DescarozadoController@update')->name('entrega-descarozado.update');
    Route::put('/entregas-descarozado/eliminar/{descarozado}', 'DescarozadoController@delete')->name('entrega-descarozado.delete');
    Route::post('/devolucion-descarozado/registrar', 'DescarozadoController@devolucionDescarozado')->name('devolucion-descarozado.create');

    //======================================= Pagos a Proveedores =========================
    Route::get('/pagos', 'PagoController@index')->name('pagos.index');
    Route::post('/pagos/datatable', 'PagoController@datatable')->name('pagos.dt');
    Route::post('/pagos', 'PagoController@store')->name('pagos.store');
    Route::get('/pagos/create/{proveedor}', 'PagoController@create')->name('pagos.create');
    Route::get('/pagos/edit/{pago}', 'PagoController@edit')->name('pagos.edit');
    Route::put('/pagos/update/{pago}', 'PagoController@update')->name('pagos.update');
    Route::put('/pagos/eliminar/{pago}', 'PagoController@delete')->name('pagos.delete');
    Route::get('/pagos/obtener-metodos-pago/{idpago}', 'PagoController@metodosPago')->name('pagos.metodosPago');



    //======================================= Balance compras y pagos a Proveedores =========================
    Route::get('/balance-compras-pagos', 'BalanceCompraPagosController@index')->name('balance-proveedor.index');
    Route::get('/balance-compras-pagos/dt/{idproveedor?}', 'BalanceCompraPagosController@datatable')->name('balance-proveedor.dt');
    Route::get('/balance-compras-pagos/filtro/proveedor={idproveedor?}&fechaInicio={fechaInicio?}&fechaFin={fechaFin?}', 'BalanceCompraPagosController@filtrarFechas');

    //======================================= Vender  =====================================================
    Route::post('/vender', 'VentaController@index')->name('vender.index');
    Route::post('/vender/guardar', 'VentaController@store')->name('vender.store');
    Route::get('/seleccionar-cliente', 'VentaController@seleccionarCliente')->name('seleccionar.cliente');
    Route::put('/venta/eliminar/{venta}', 'VentaController@delete')->name('vender.delete');
    Route::get('/vender/desde-orden/{orden}', 'VentaController@showFromOrden')->name('vender-desde-orden.show');
    Route::post('/vender/desde-orden/store', 'VentaController@storeFromOrden')->name('vender-desde-orden.store');

    //======================================= Historial de Ventas  =====================================================
    Route::get('/historial-ventas', 'HistorialVentasController@index')->name('historial-ventas.index');
    Route::get('/historial-ventas/datatable', 'HistorialVentasController@datatable')->name('historial-ventas.dt');
    Route::get('/historial-ventas/detalle/{venta}', 'HistorialVentasController@show')->name('historial-ventas.show');
    Route::post('/historial-ventas/filtrar', 'HistorialVentasController@filtrar')->name('historial-ventas.filtrar');

    //======================================= Cobros  =====================================================
    Route::get('/cobros', 'CobroController@index')->name('cobros.index');
    Route::post('/cobros/datatable', 'CobroController@datatable')->name('cobros.dt');
    Route::get('/seleccionar-cliente-cobros', 'CobroController@seleccionarCliente')->name('seleccionar.cliente.cobros');
    Route::get('/cobros/create/{client}', 'CobroController@create')->name('cobros.create');
    Route::post('/cobros/create', 'CobroController@store')->name('cobros.store');
    Route::get('/cobros/edit/{cobro}', 'CobroController@edit')->name('cobros.edit');
    Route::put('/cobros/update/{cobro}', 'CobroController@update')->name('cobros.update');
    Route::put('/cobros/eliminar/{cobro}', 'CobroController@delete')->name('cobros.delete');
    Route::get('/cobros/obtener-metodos-pago/{idcobro}', 'CobroController@metodosPago')->name('cobros.metodosPago');

    //======================================= Balance ventas y cobros a clientes =========================
    Route::get('/balance-ventas-cobros', 'BalanceVentasCobrosController@index')->name('balance-cliente.index');
    Route::get('/balance-ventas-cobros/dt/{idcliente?}', 'BalanceVentasCobrosController@datatable')->name('balance-cliente.dt');
    Route::get('/balance-ventas-cobros/filtro/cliente={idcliente?}&fechaInicio={fechaInicio?}&fechaFin={fechaFin?}', 'BalanceVentasCobrosController@filtrarFechas');

    //======================================= Impresion PDF =========================



    Route::get('/imprimir-pdf/balance-sueldo/{idempleado}/{fechaFin}/{fechaInicio?}', 'PDFController@pdfBalanceSueldo')->name('balance-sueldo.pdf');
    Route::get('/imprimir-pdf/balance-compras-pagos/{idproveedor}/{fechaFin}/{fechaInicio?}', 'PDFController@pdfBalanceComprasPagos')->name('balance-compras-pagos.pdf');
    Route::get('/imprimir-pdf/balance-ventas-cobros/{idcliente}/{fechaFin}/{fechaInicio?}', 'PDFController@pdfBalanceVentasCobros')->name('balance-ventas-cobros.pdf');
    Route::get('/imprimir-pdf/balance-impositivo/{mes?}/{desde?}/{hasta?}/{verPositivo?}', 'PDFController@pdfBalanceImpositivo')->name('balance-impositivo.pdf');

    //terminado
    Route::get('/imprimir-pdf/recibo-pago-productor/{pago}', 'PDFController@pdfReciboPagoProductor')->name('pago.pdf');
    Route::get('/imprimir-pdf/recibo-pago-cobro/{cobro}', 'PDFController@pdfReciboPagoCobro')->name('cobro.pdf');
    Route::get('/imprimir-pdf/pago-sueldo/{pagoSueldo}', 'PDFController@pdfPagoSueldo')->name('pago-sueldo.pdf');
    Route::get('/imprimir-pdf/venta/{venta}', 'PDFController@pdfVenta')->name('venta.pdf');
    Route::get('/imprimir-pdf/entrega-indumentaria/{entrega}', 'PDFController@pdfEntregaIndumentaria')->name('entrega-indumentaria.pdf');
    Route::get('/imprimir-pdf/descarozado/{descarozado}', 'PDFController@pdfDescarozado')->name('descarozado.pdf');
    Route::get('/imprimir-pdf/lista-compra/{priceList}', 'PDFController@pdfListaPrecio')->name('lista-precio.pdf');
    Route::get('/imprimir-pdf/compra/{idcompra}/{tipoCompra}', 'PDFController@pdfCompra')->name('compra.pdf');
    Route::get('/imprimir-pdf/ingreso-fruta/{ingreso}', 'PDFController@pdfIngresoFruta')->name('ingreso-fruta.pdf');
    Route::get('/imprimir-pdf/entrega-descarozado/{descarozado}', 'PDFController@pdfEntregaDescarozado')->name('entrega-descarozado.pdf');
    Route::get('/imprimir-pdf/sueldo/{sueldo}', 'PDFController@pdfSueldo')->name('sueldo.pdf');

    //todavia no diseñado
    Route::get('/imprimir-pdf/liquidacion/{liquidacion}', 'PDFController@pdfLiquidacion')->name('liquidacion.pdf');
    Route::get('/imprimir-pdf/orden-pago', 'PDFController@pdfOrdenPago')->name('orden-pago.pdf');
    Route::get('/imprimir-pdf/orden-pedido', 'PDFController@pdfOrdenPedido')->name('orden-pedido.pdf');

    //======================================== Envases ==============================
    Route::get('/envases', 'EnvaseController@index')->name('envases.index');
    Route::get('/envases/datatable', 'EnvaseController@datatable')->name('envases.dt');
    Route::post('/envases', 'EnvaseController@store')->name('envases.store');
    Route::put('/envases/update/{envase}', 'EnvaseController@update')->name('envases.update');
    Route::put('/envases/delete/{envase}', 'EnvaseController@delete')->name('envases.delete');

    //======================================== Stock Insumos - Productos ==============================
    Route::get('/control-stock', 'StockProductosInsumosController@index')->name('stock.index');
    Route::get('/control-stock/datatable-movimientos', 'StockProductosInsumosController@datatableMovimientos')->name('stock.dt-movimientos');
    Route::get('/control-stock/datatable-productos', 'StockProductosInsumosController@datatableProductos')->name('stock.dt-productos');
    Route::get('/control-stock/datatable-insumos', 'StockProductosInsumosController@datatableInsumos')->name('stock.dt-insumos');
    Route::post('/control-stock/producto', 'StockProductosInsumosController@storeMovimientoProducto')->name('stock.producto');
    Route::post('/control-stock/insumo', 'StockProductosInsumosController@storeMovimientoInsumo')->name('stock.insumo');
    Route::put('/control-stock/cancelar-movimiento/{movimiento}', 'StockProductosInsumosController@cancelarMovimiento')->name('stock.cancelar');

    //======================================== Stock Envases ==============================
    Route::get('/control-stock/envase', 'StockEnvasesController@create')->name('stock.envase-index');
    Route::get('/control-stock/datatable-envases', 'StockEnvasesController@datatableEnvase')->name('stock.dt-envases');
    Route::get('/control-stock/detalle-movimiento/{idmovimiento}', 'StockEnvasesController@detalleMovimiento');
    Route::put('/control-stock/borrar-movimiento/{movimiento}', 'StockEnvasesController@borrarMovimiento');
    Route::post('/control-stock/envase', 'StockEnvasesController@store')->name('stock.envase');

    //======================================== Pre Orden Compra ==============================
    Route::get('/orden-compra', 'OrdenCompraController@index')->name('orden-compra.index');
    Route::get('/pre-orden-compra', 'OrdenCompraController@listar')->name('orden-compra.listar');
    Route::get('/pre-orden-compra/datatable', 'OrdenCompraController@dt')->name('orden-compra.dt');
    Route::post('/pre-orden-compra/filtrar', 'OrdenCompraController@filtrar')->name('orden-compra.filtrar');
    Route::get('/pre-orden-compra/show/{orden}', 'OrdenCompraController@show')->name('orden-compra.show');
    Route::get('/pre-orden-compra/{tipoOrden}/{idProveedor}', 'OrdenCompraController@create')->name('orden-compra.create');
    Route::put('/pre-orden-compra/delete/{orden}', 'OrdenCompraController@delete')->name('orden-compra.delete');
    Route::post('/pre-orden-compra/insumo', 'OrdenCompraController@storeInsumo')->name('orden-compra.storeInsumo');
    Route::post('/pre-orden-compra/indumentaria', 'OrdenCompraController@storeIndumentaria')->name('orden-compra.storeIndumentaria');
    Route::post('/pre-orden-compra/servicio', 'OrdenCompraController@storeServicio')->name('orden-compra.storeServicio');

    //Crear compra a partir de la orden de compra
    Route::get('/insumos-servicios/from-orden-compra/{orden}', 'OrdenCompraController@showFromOrden')->name('purchase-supplie.show-from-orden');
    Route::post('/insumos-servicios/from-orden-compra/insumo', 'PurchaseSupplieServiceController@storeFromOrden')->name('purchase-supplie.store-from-orden');
    Route::post('/insumos-servicios/from-orden-compra/indumentaria', 'PurchaseIndumentaryController@storeFromOrden')->name('purchase-indumentarie.store-from-orden');

    //======================================== Pre Orden Venta ==============================
    Route::get('/orden-venta', 'OrdenVentaController@index')->name('orden-venta.index');
    Route::get('/orden-venta/dt', 'OrdenVentaController@datatable')->name('orden-venta.dt');
    Route::get('/orden-venta/seleccionar-cliente', 'OrdenVentaController@seleccionarCliente')->name('orden-venta.seleccionar-cliente');
    Route::post('/orden-venta/create', 'OrdenVentaController@create')->name('orden-venta.create');
    Route::post('/orden-venta/store', 'OrdenVentaController@store')->name('orden-venta.store');
    Route::get('/orden-venta/show/{orden}', 'OrdenVentaController@show')->name('orden-venta.show');
    Route::put('/orden-venta/delete/{orden}', 'OrdenVentaController@delete')->name('orden-venta.delete');
    Route::post('/orden-venta/filtrar', 'OrdenVentaController@filtrar')->name('orden-venta.filtrar');

    //======================================== Liquidaciones ==============================
    Route::get('/liquidaciones', 'LiquidacionController@index')->name('liquidaciones.index');
    Route::get('/liquidaciones/dt', 'LiquidacionController@datatable')->name('liquidaciones.dt');
    Route::get('/liquidaciones/create', 'LiquidacionController@create')->name('liquidaciones.create');
    Route::post('/liquidaciones/calcular-balance', 'LiquidacionController@calcularBalance')->name('liquidaciones.calcular-balance');
    Route::post('/liquidaciones/store', 'LiquidacionController@store')->name('liquidaciones.store');
    Route::get('/liquidaciones/show/{liquidacion}', 'LiquidacionController@show')->name('liquidaciones.show');
    Route::get('/liquidaciones/edit/{liquidacion}', 'LiquidacionController@edit')->name('liquidaciones.edit');
    Route::put('/liquidaciones/update/{liquidacion}', 'LiquidacionController@update')->name('liquidaciones.update');
    Route::put('/liquidaciones/delete/{id}', 'LiquidacionController@delete');

    //======================================== Totales de ingreso ==============================
    Route::get('/totales-ingreso', 'TotalesIngresoController@index')->name('totales-ingreso.index');
    Route::post('/totales-ingreso/filtrar', 'TotalesIngresoController@filtrar')->name('totales-ingreso.filtrar');




    Route::get('/prueba-ingreso', function () {
        $envases = Envase::all();
        $materiasPrimas = RawMaterial::all();

        return view('prueba-ingreso')
            ->with('envases', $envases)
            ->with('materiasPrimas', $materiasPrimas);
    });


    Route::get('/probandoArray', 'RawMaterialEntryController@probandoArray');
    Route::get('/obtener-clientes', function () {
        $clientes = Client::join('localities', 'clients.idLocalidad', '=', 'localities.id')
            ->join('provinces', 'localities.id_provincia', '=', 'provinces.id')
            ->join('price_lists', 'clients.price_list_id', '=', 'price_lists.id')
            ->select('clients.*', 'localities.localidad', 'provinces.provincia', 'price_lists.nombre AS listaPrecios', 'price_lists.fechaVigencia')
            ->get();

        return response()->json($clientes);
    });
    // Route::get('/proband', 'ServicesController@store');
    Route::get('/prueba', 'ServicesController@store');
});
