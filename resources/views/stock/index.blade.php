@extends('adminlte::page')

@section('title', 'Stock')

@section('content_header')
    <h1>Movimientos de stock</h1>
@stop

@section('content')


    <ul class="nav nav-tabs " id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="movimientos-tab" data-toggle="tab" href="#movimientos" role="tab"
                aria-controls="movimientos" aria-selected="true">Movimientos Envases</a>
        </li>
        @can('stock-general')
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="productos-tab" data-toggle="tab" href="#productos" role="tab"
                    aria-controls="productos" aria-selected="false">Movimientos Productos</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="insumos-tab" data-toggle="tab" href="#insumos" role="tab" aria-controls="insumos"
                    aria-selected="false">Movimientos Insumos</a>
            </li>
        @endcan
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="movimientos" role="tabpanel" aria-labelledby="movimientos-tab">
            <div class="container-fluid py-3 tab-cont">
                <div class="row py-3">
                    <div class="col-md-12 d-flex justify-content-end pb-2">
                        <a class="btn btn-succes bg-hfrut" href="{{ route('stock.envase-index') }}">Agregar
                            movimiento de stock
                            envase</a>
                    </div>
                </div>
                <table class='table table-bordered' id='tablaEnvases'>
                    <thead class="bg-hfrut">
                        <th scope="col">Proveedor</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">Tipo Movimiento</th>
                        <th scope="col">Descripción</th>
                        <th scope="col" class="text-center" style="width:10em;">Opciones</th>
                    </thead>

                </table>
            </div>
        </div>
        @can('stock-general')
            <div class="tab-pane fade" id="productos" role="tabpanel" aria-labelledby="productos-tab">
                <div class="container-fluid py-3 tab-cont">
                    <div class="row py-3">
                        <div class="col-md-12 d-flex justify-content-end pb-2">
                            <button class="btn btn-succes bg-hfrut" data-target="#modalProductos" data-toggle="modal">Agregar
                                movimiento de stock
                                producto</button>
                        </div>
                    </div>
                    <table class='table table-bordered' id='tablaProductos'>
                        <thead class="bg-hfrut">
                            <th scope="col">Nombre</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Tipo Movimiento</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Descripción</th>
                            <th scope="col" style="" class="text-right">Opciones</th>
                        </thead>

                    </table>
                </div>
            </div>
            <div class="tab-pane fade" id="insumos" role="tabpanel" aria-labelledby="insumos-tab">
                <div class="container-fluid py-3 tab-cont">
                    <div class="row py-3">
                        <div class="col-md-12 d-flex justify-content-end pb-2">
                            <button class="btn btn-succes bg-hfrut" data-target="#modalInsumos" data-toggle="modal">Agregar
                                movimiento de stock
                                insumo</button>
                        </div>
                    </div>
                    <table class='table table-bordered' id='tablaInsumos'>
                        <thead class="bg-hfrut">
                            <th scope="col">Nombre</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Tipo Movimiento</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Descripción</th>
                            <th scope="col" style="" class="text-right">Opciones</th>
                        </thead>

                    </table>
                </div>
            </div>
        @endcan
    </div>
    {{-- NOTE Modal Stock Producto --}}
    <div class="modal fade" id="modalProductos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pl-3" id="">Agregar cantidad productos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="formProductos">
                        <span id="resultFormProductos"></span>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Productos</label>
                                <select name="producto" id="select-productos" class="form-control">
                                    @foreach ($productos as $producto)
                                        <option value="{{ $producto->id }}"
                                            {{ old('producto') == $producto->id ? 'selected' : '' }}>
                                            @php
                                                switch ($producto->tipo) {
                                                    case '0':
                                                        $tipoProducto = 'Caja, ' . $producto->kg . ' kg(s)';
                                                        break;
                                                    case '1':
                                                        $tipoProducto = 'Bolsa, ' . $producto->kg . ' kg(s)';
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            @endphp
                                            {{ $producto->nombreCompleto . ' - ' . $tipoProducto }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Fecha (*)</label>
                                <input type="date" value="{{ date('Y-m-d', strtotime(now())) }}"
                                    max="{{ date('Y-m-d', strtotime(now())) }}" name=" fecha" class="form-control"
                                    placeholder="" aria-describedby="helpId" id="fechaProducto">
                                <span class="invalid-feedback d-block" role="alert" id="error-fechaProducto">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Cantidad (*)</label>
                                <input type="number" name="cantidad" class="form-control" placeholder=""
                                    aria-describedby="helpId" id="cantidadProducto">
                                <span class="invalid-feedback d-block" role="alert" id="error-cantidadProducto">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Descripcion (*)</label>
                                <textarea rows="4" type="text" name="descripcion" class="form-control" placeholder=""
                                    aria-describedby="helpId" id="descripcionProducto"></textarea>
                                <span class="invalid-feedback d-block" role="alert" id="error-descripcionProducto">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnMovimientoStockProductos">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    {{-- NOTE Modal Stock Insumo --}}
    <div class="modal fade" id="modalInsumos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pl-3" id="">Restar cantidad insumos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="formInsumos">
                        <span id="resultFormInsumos"></span>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Productos</label>
                                <select name="insumo" id="select-insumos" class="form-control">
                                    @foreach ($insumos as $insumo)
                                        <option value="{{ $insumo->id }}"
                                            {{ old('insumo') == $insumo->id ? 'selected' : '' }}>
                                            {{ $insumo->nombre }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Fecha (*)</label>
                                <input type="date" value="{{ date('Y-m-d', strtotime(now())) }}"
                                    max="{{ date('Y-m-d', strtotime(now())) }}" name="fecha" class="form-control"
                                    placeholder="" aria-describedby="helpId" id="fechaInsumo">
                                <span class="invalid-feedback d-block" role="alert" id="error-fechaInsumo">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Cantidad (*)</label>
                                <input type="number" name="cantidad" class="form-control" placeholder=""
                                    aria-describedby="helpId" id="cantidadInsumo">
                                <span class="invalid-feedback d-block" role="alert" id="error-cantidadInsumo">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Descripcion (*)</label>
                                <textarea rows="4" type="text" name="descripcion" class="form-control" placeholder=""
                                    aria-describedby="helpId" id="descripcionInsumo"></textarea>
                                <span class="invalid-feedback d-block" role="alert" id="error-descripcionInsumo">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnMovimientoStockInsumos">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    {{-- NOTE Modal detalle movimiento envases --}}
    <div class="modal fade" id="modalDetalleMovimientoEnvase" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pl-3" id="">Detalle movimiento de envase</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped text-center">
                        <thead>
                            <tr>
                                <th scope="col">Envase</th>
                                <th scope="col">Cantidad</th>
                            </tr>
                        </thead>
                        <tbody id="bodyTablaEnvases">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnMovimientoStockInsumos">Guardar</button>
                </div>
            </div>
        </div>
    </div>

@stop



@section('js')
    <script>
        $(document).ready(function() {

            $('#select-productos').select2({
                dropdownParent: $('#modalProductos')
            });
            $('#select-insumos').select2({
                dropdownParent: $('#modalInsumos')
            });

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            //TODO Datatable Insumos
            tablaInsumos = $('#tablaInsumos').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: false,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('stock.dt-insumos') }}",
                columns: [{
                        data: 'nombre',
                    },
                    {
                        data: 'fecha',
                    },
                    {
                        data: 'tipoMovimiento',
                    },
                    {
                        data: 'cantidad',
                    },
                    {
                        data: 'descripcion',
                    },
                    {
                        class: 'text-right',
                        data: 'opciones',
                    },
                    {
                        data: 'created_at',
                        visible: false
                    }
                ],
                order: [6, 'desc']
            });

            //TODO Datatable Productos
            tablaProductos = $('#tablaProductos').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: false,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('stock.dt-productos') }}",
                columns: [{
                        class: 'variante',
                        data: 'nombre',
                    },
                    {
                        data: 'fecha',
                    },
                    {
                        data: 'tipoMovimiento',
                    },
                    {
                        data: 'cantidad',
                    },
                    {
                        data: 'descripcion',
                    },
                    {
                        class: 'text-right',
                        data: 'opciones',
                    },
                    {
                        data: 'created_at',
                        visible: false
                    }
                ],
                order: [6, 'desc']
            });
            //TODO Datatable Envases
            tablaEnvases = $('#tablaEnvases').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: false,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('stock.dt-envases') }}",
                columns: [{
                        class: 'variante',
                        data: 'nombreCompleto',
                    },
                    {
                        data: 'fecha',
                    },
                    {
                        data: 'tipoMovimiento',
                    },
                    {
                        data: 'descripcion',
                    },
                    {
                        class: 'text-right',
                        data: 'opciones',
                    },
                    {
                        data: 'created_at',
                        visible: false
                    }
                ],
                order: [5, 'desc']
            });

            //SECTION Modal Producto

            //EVENTO Esconder
            $('#modalProductos').on('hide.bs.modal', function(e) {
                $('#cantidadProducto').val('');
                $('#descripcionProducto').val('');
                $('#resultFormProductos').empty();
            });

            //EVENTO Ejecutar movimiento stock
            $('#btnMovimientoStockProductos').click(function(e) {
                e.preventDefault();
                var error = 0;
                if (!$('#cantidadProducto').val()) {
                    invalidFeedback('#cantidadProducto', '#error-cantidadProducto',
                        'El campo cantidad no puede esar vacio')
                    error += 1
                } else if (0 > $('#cantidadProducto').val()) {
                    invalidFeedback('#cantidadProducto', '#error-cantidadProducto',
                        'El campo cantidad no puede ser menor que 0')
                    error += 1
                } else {
                    removeFeedback('#cantidadProducto', '#error-cantidadProducto');
                }

                if (!$('#descripcionProducto').val()) {
                    invalidFeedback('#descripcionProducto', '#error-descripcionProducto',
                        'El campo descripcion no puede esar vacio')
                    error += 1
                } else {
                    removeFeedback('#descripcionProducto', '#error-descripcionProducto');
                }

                if (error == 0) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('stock.producto') }}",
                        data: $('#formProductos').serialize() + "&_token={{ csrf_token() }}",
                        dataType: "json",
                        success: function(response) {
                            if (response.success) {
                                Swal.fire({
                                    toast: true,
                                    icon: 'success',
                                    title: response.success,
                                    position: 'top-right',
                                    showConfirmButton: false,
                                    timer: 2300,
                                });
                                tablaProductos.ajax.reload();
                                $('#modalProductos').trigger('click');
                            }

                            if (response.error) {
                                html =
                                    '<div class = "alert alert-danger pb-0 px-0" role = "alert" id="alertaErrores"><ul>';
                                for (var i = 0; i < response.error.length; i++) {
                                    html += '<li>' + response.error[i] + '</li>';
                                }
                                html += '</ul></div>';
                                $('#resultFormProductos').html(html);
                            }
                        }
                    });
                }
            });
            //!SECTION

            //SECTION Modal Insumo
            //EVENTO Esconder
            $('#modalCantidad').on('hide.bs.modal', function(e) {
                $('#cantidadInsumo').val('');
                $('#descripcionInsumo').val('');
                $('#resultFormCantidad').empty();
            });

            //EVENTO Ejecutar movimiento stock
            $('#btnMovimientoStockInsumos').click(function(e) {
                e.preventDefault();
                var error = 0;
                if (!$('#cantidadInsumo').val()) {
                    invalidFeedback('#cantidadInsumo', '#error-cantidadInsumo',
                        'El campo cantidad no puede esar vacio')
                    error += 1
                } else if (0 > $('#cantidadInsumo').val()) {
                    invalidFeedback('#cantidadInsumo', '#error-cantidadInsumo',
                        'El campo cantidad no puede ser menor que 0')
                    error += 1
                } else {
                    removeFeedback('#cantidadInsumo', '#error-cantidadInsumo');
                }

                if (!$('#descripcionInsumo').val()) {
                    invalidFeedback('#descripcionInsumo', '#error-descripcionInsumo',
                        'El campo descripcion no puede esar vacio')
                    error += 1
                } else {
                    removeFeedback('#descripcionInsumo', '#error-descripcionInsumo');
                }

                if (error == 0) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('stock.insumo') }}",
                        data: $('#formInsumos').serialize() + "&_token={{ csrf_token() }}",
                        dataType: "json",
                        success: function(response) {
                            if (response.success) {
                                Swal.fire({
                                    toast: true,
                                    icon: 'success',
                                    title: response.success,
                                    position: 'top-right',
                                    showConfirmButton: false,
                                    timer: 2300,
                                });
                                tablaInsumos.ajax.reload();
                                $('#modalInsumos').trigger('click');
                            }

                            if (response.error) {
                                html =
                                    '<div class = "alert alert-danger pb-0 px-0" role = "alert" id="alertaErrores"><ul>';
                                for (var i = 0; i < response.error.length; i++) {
                                    html += '<li>' + response.error[i] + '</li>';
                                }
                                html += '</ul></div>';
                                $('#resultFormInsumos').html(html);
                            }
                        }
                    });
                }
            });

            //!SECTION


            //EVENTO Eliminar movimiento envase
            $(document).on('click', '.btnEliminarMovimientoEnvase', function() {
                var id = $(this).data('id');
                Swal.fire({
                    title: '¿Desea dar de baja este movimiento?',
                    text: '',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si',
                    cancelButtonText: 'No',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "PUT",
                            url: "/control-stock/borrar-movimiento/" + id,
                            data: "_token={{ csrf_token() }}",
                            dataType: "json",
                            success: function(response) {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 2300,
                                    timerProgressBar: true,
                                })
                                Toast.fire({
                                    icon: 'success',
                                    title: response.success
                                })
                                tablaEnvases.ajax.reload();
                            }
                        });
                    }
                })
            });

            //EVENTO Eliminar movimiento producto/insumo
            $(document).on('click', '.btnEliminarMovimientoProductoInsumo', function() {
                var id = $(this).data('id');
                console.log(id)
                Swal.fire({
                    title: '¿Desea dar de baja este movimiento?',
                    text: '',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si',
                    cancelButtonText: 'No',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "PUT",
                            url: "/control-stock/cancelar-movimiento/" + id,
                            data: "_token={{ csrf_token() }}",
                            dataType: "json",
                            success: function(response) {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 2300,
                                    timerProgressBar: true,
                                })
                                Toast.fire({
                                    icon: 'success',
                                    title: response[0].success
                                })
                                //Significa que es un producto de una variante
                                if (response[1].es_variante == 0) {
                                    tablaProductos.ajax.reload();
                                } else {
                                    tablaInsumos.ajax.reload();
                                }
                            }
                        });
                    }
                })
            });

            //EVENTO Mostrar modal detalle movimiento envase
            $('#modalDetalleMovimientoEnvase').on('show.bs.modal', function(e) {
                var button = $(e.relatedTarget);
                var idmovimiento = button.data('id')
                $.get("/control-stock/detalle-movimiento/" + idmovimiento,
                    function(data) {
                        for (let i = 0; i < data.length; i++) {
                            const movimiento = data[i];
                            var html = '<tr><td>' + movimiento.envase + '</td>' +
                                '<td>' + movimiento.cantidad + '</td></tr>';
                            $('#bodyTablaEnvases').append(html);
                        }
                    },
                    "json"
                );
            });
            //EVENTO Esconder modal detalle movimiento envase
            $('#modalDetalleMovimientoEnvase').on('hide.bs.modal', function(e) {
                $('#bodyTablaEnvases').empty();
            });
            //SECTION Validaciones
            //==================== FUNCION ERROR DE VALIDACION ============================
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }
            //!SECTION
        });
    </script>
@stop
