@extends('adminlte::page')

@section('title', 'Movimiento Envase')


@section('content_header')
    <h1>Movimiento envases</h1>
@stop

@section('content')
    <form action="" id="formEnvase">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header pb-0">
                        <h3 class="card-title"><b>Datos estáticos</b></h3>
                    </div>
                    <div class="card-body pb-2">
                        <div class="row">


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Fecha (*)</label>
                                    <input type="date" value="{{ date('Y-m-d', strtotime(now())) }}"
                                        max="{{ date('Y-m-d', strtotime(now())) }}" name="fecha" class="form-control"
                                        placeholder="" aria-describedby="helpId" id="fechaEnvase">
                                    <span class="invalid-feedback d-block" role="alert" id="error-fechaEnvase">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Proveedor (*)</label>
                                    <select name="proveedor" id="select-proveedores" class="form-control">
                                        @foreach ($proveedoresMP as $proveedor)
                                            <option value="{{ $proveedor->id }}"
                                                {{ old('proveedor') == $proveedor->id ? 'selected' : '' }}>
                                                {{ $proveedor->nombreCompleto }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="tipo" class="col-form-label">Tipo (*)</label>
                                    <div class="btn-group btn-group-toggle d-block" data-toggle="buttons">
                                        {{-- //IS = Insumo --}}
                                        <label class="btn btn-outline-primary active">
                                            <input type="radio" name="es_entregado" value="0" checked id="btnInsumoCrear">
                                            Entrega
                                        </label>
                                        {{-- //SV = Servicio --}}
                                        <label class="btn btn-outline-primary">
                                            <input type="radio" name="es_entregado" value="1" id="btnServicioCrear">
                                            Recepción
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Descripcion</label>
                                    <textarea rows="4" type="text" name="descripcion" class="form-control" placeholder=""
                                        aria-describedby="helpId" id="descripcionEnvase"></textarea>
                                    <span class="invalid-feedback d-block" role="alert" id="error-descripcionEnvase">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header pb-0">
                        <h3 class="card-title"><b>Datos dinámicos</b></h3>
                    </div>
                    <div class="card-body pb-2">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Envases (*)</label>
                                    <select name="envase" id="select-envases" class="form-control">
                                        @foreach ($envases as $envase)
                                            <option value="{{ $envase->id }}"
                                                {{ old('envase') == $envase->id ? 'selected' : '' }}>
                                                {{ $envase->nombre }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="invalid-feedback d-block" role="alert" id="error-select-envases">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Cantidad (*)</label>
                                    <input type="number" name="cantidad" class="form-control" placeholder=""
                                        aria-describedby="helpId" id="cantidadEnvase">
                                    <span class="invalid-feedback d-block" role="alert" id="error-cantidadEnvase">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="" class="">ㅤ</label>
                                    <button class="btn btn-primary form-control" id="btnAgregarEnvase"><i
                                            class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-striped table-sm d-none pl-2" id="tablaEnvases">
                                    <thead>
                                        <tr>
                                            <th scope="col">Envase</th>
                                            <th scope="col">Cantidad</th>
                                            <th scope="col">Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody id="bodyTablaEnvases">

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 d-flex justify-content-end">
                <a type="button" class="btn btn-secondary mr-3 btn-lg" href="{{ route('stock.index') }}">Volver</a>
                <button type="button" class="btn btn-primary btn-lg" id="btnMovimientoStockEnvase">Guardar</button>
            </div>
        </div>
    </form>
@stop


@section('js')
    <script>
        $(document).ready(function() {
            $('#select-proveedores').select2();
            $('#select-envases').select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });


            //EVENTO Agregar Envase 
            $('#btnAgregarEnvase').click(function(e) {
                e.preventDefault();
                var error = 0;
                if (!$('#cantidadEnvase').val()) {
                    invalidFeedback('#cantidadEnvase', '#error-cantidadEnvase',
                        'El campo cantidad no puede esar vacio')
                    error += 1
                } else if (0 > $('#cantidadEnvase').val()) {
                    invalidFeedback('#cantidadEnvase', '#error-cantidadEnvase',
                        'El campo cantidad no puede ser menor que 0')
                    error += 1
                } else {
                    removeFeedback('#cantidadEnvase', '#error-cantidadEnvase');
                }
                if (!$('#select-envases').val()) {
                    invalidFeedback('#select-envases', '#error-select-envases',
                        'El campo envase no puede esar vacio')
                    error += 1
                } else {
                    removeFeedback('#select-envases', '#error-select-envases');
                }

                if (error == 0) {
                    var envase = $("#select-envases option:selected").text();
                    var idEnvase = $("#select-envases option:selected").val();
                    var cantidad = $('#cantidadEnvase').val();
                    var row = '<tr>' +
                        '<td>' + envase + '<input type="hidden" name="id_envase[]" value="' +
                        idEnvase +
                        '"></td>' +
                        '<td>' + cantidad +
                        '<input type="hidden" name="cantidad[]" value="' + cantidad +
                        '"></td>' +
                        '<td><span class="badge badge-danger btnEliminarDeTabla" role="button">Eliminar</span></td>' +
                        '</tr>';
                    $('#tablaEnvases').removeClass('d-none');

                    $('#bodyTablaEnvases').append(row);
                    $('#cantidadEnvase').val('');
                    $('#cantidadEnvase').focus();
                }
            });

            //EVENTO Guardar Movimiento 
            $('#btnMovimientoStockEnvase').click(function(e) {
                e.preventDefault();
                var rowCount = $('#tablaEnvases tr').length;
                if (rowCount == 1) {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 4500,
                        timerProgressBar: true,
                    })
                    Toast.fire({
                        icon: 'error',
                        title: 'Necesita agregar al menos un envase y su cantidad en el apartado "Datos dinámicos"'
                    })

                    return
                }
                $.ajax({
                    type: "POST",
                    url: "{{ route('stock.envase') }}",
                    data: $('#formEnvase').serialize() + "&_token={{ csrf_token() }}",
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success +
                                    '. Espere unos instantes',
                                position: 'top-right',
                                timerProgressBar: true,
                                showConfirmButton: false,
                                timer: 2300,
                            }).then(function() {
                                window.location.href =
                                    "{{ route('stock.index') }}";
                            });
                        }
                        if (data.error) {
                            html =
                                '<ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul>';

                            Swal.fire({
                                title: 'Error',
                                icon: 'error',
                                html: html,
                                showCloseButton: false,
                                showCancelButton: false,
                                focusConfirm: true,
                                confirmButtonText: 'OK',
                                confirmButtonAriaLabel: '',
                            })
                        }
                    }
                });
            });

            //EVENTO Boton eliminar en table 
            $(document).on('click', '.btnEliminarDeTabla', function() {
                $(this).closest("tr").remove();
                var rowCount = $('#tablaEnvases tr').length;
                if (rowCount == 1) {
                    $('#tablaEnvases').addClass('d-none');
                }
            });

            //SECTION Validaciones
            //==================== FUNCION ERROR DE VALIDACION ============================
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }
            //!SECTION

        });
    </script>
@stop
