@extends('adminlte::page')

@section('title', 'Historial Compra')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="invoice p-3 mb-3">
                <div class="row">
                    <div class="col-12">
                        <h4>
                            <i class="fas fa-globe"></i> HFRUT
                            <small class="float-right">Fecha:
                                {{ date('d-m-Y', strtotime($maestroCompra->fecha)) }}</small>
                        </h4>
                    </div>
                </div>
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col my-3">
                        ID Compra: <strong>#{{ $maestroCompra->id }}</strong> <br>
                        Proveedor: <strong>{{ $maestroCompra->proveedor->nombreCompleto }}</strong><br>
                        Detalle: <strong> {{ $maestroCompra->detalle }} </strong> <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped text-center">
                            <thead>
                                <tr>
                                    <th>Cantidad</th>
                                    <th>Nombre</th>
                                    <th>Precio</th>
                                    <th>Total Parcial</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($detalleCompra as $detalle)
                                    <tr>
                                        <td>{{ $detalle->cantidad }}</td>
                                        <td>{{ $detalle->indumentaria->nombre . ', ' . $detalle->indumentaria->modelo }}
                                        </td>
                                        <td>${{ number_format($detalle->precioUnitario, 2, ',', '.') }}</td>
                                        <td>$ {{ number_format($detalle->totalParcial, 2, ',', '.') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                    </div>
                    <div class="col-6">
                        <h2 class="text-right pr-3 my-3">Total: ${{ number_format($maestroCompra->total, 2, ',', '.') }}
                        </h2>
                    </div>
                </div>
                <div class="row no-print">
                    <div class="col-12">

                        <a href="{{ redirect()->back()->with('from', 'ID')->getTargetUrl() }}"
                            class="btn btn-danger">Volver</a>



                        <a href="{{ route('compra.pdf', ['idcompra' => $maestroCompra->id, 'tipoCompra' => 'ID']) }}"
                            target="_blank" class="btn btn-primary float-right" style="margin-right: 5px;">
                            <i class="fas fa-download"></i> Generar PDF
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop



@section('js')

@stop
