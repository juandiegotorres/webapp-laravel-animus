@extends('adminlte::page')

@section('title', 'Nuevo ingreso de materia prima')

@section('css')
    <style>
        hr {
            margin-top: 0.5rem !important;
            margin-bottom: 0.5rem !important;
        }

    </style>
@endsection


@section('content_header')
    <div class="container-fluid px-4">
        <div class="row mb-1">
            <h1>Nuevo ingreso de materia prima</h1>
        </div>
        <div class="row d-flex justify-content-between align-items-center">
            <h5>Proveedor: {{ $proveedor->nombreCompleto }} <span class="text-muted"> - CUIT:
                    {{ $proveedor->cuit }}
                </span></h5>
            <div class="col-md-2">
                <input type="date" class="form-control" id="fechaElegida" value="{{ now()->format('Y-m-d') }}"
                    max="{{ now()->format('Y-m-d') }}" min="{{ now()->subDays(7)->format('Y-m-d') }}">
            </div>
        </div>
    </div>
    <hr>
@stop

@section('content')
    <div class="container-fluid" id='app'>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group mb-0">
                    <label for="materiasPrima" class="col-form-label">Materia prima</label>
                    {{-- LLeno el select con las materias primas de la db --}}
                    <select name="materiasPrima" class="form-control" id="materiasPrima" style="width:100%!important;">
                        @foreach ($materiasPrimas as $materiaPrima)
                            <option value="{{ $materiaPrima->id }}">
                                {{ $materiaPrima->fruta . ', ' . $materiaPrima->variedad }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-1 ">
                <div class="form-group mb-0">
                    <label for="btn" class="col-form-label">ㅤ</label>
                    <a class="btn btn-success form-control " name="btn" data-toggle="modal"
                        data-target="#modalMateriaPrimaAgregar" id="btnModalAgregar"> <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group mb-0">
                    <label for="envase" class="col-form-label">Envase</label>
                    {{-- LLeno el select con las materias primas de la db --}}
                    <select name="envase" class="form-control" id="envases" style="width:100%!important;">
                        @foreach ($envases as $envase)
                            <option value="{{ $envase->id }}" data-peso="{{ $envase->peso }}">
                                {{ $envase->nombre }}</option>
                        @endforeach
                    </select>
                    <small id="pesoLabel" class="text-muted"></small>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group mb-0">
                    <label for="cantidadEnvase" class="col-form-label">Cant. Envases</label>
                    {{-- LLeno el select con las materias primas de la db --}}
                    <input type="number" name="cantidadEnvase" class="form-control" id="cantidadEnvase">
                    <span class="invalid-feedback" role="alert" id="error-cantidadenvase">
                        <strong></strong>
                    </span>
                </div>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-md-2">
                <div class="form-group mb-0">
                    <label for="kgTara" class="col-form-label">Kg Tara</label>
                    {{-- LLeno el select con las materias primas de la db --}}
                    <input type="number" name="kgTara" class="form-control" id="kgTara" readonly>
                    <span class="invalid-feedback" role="alert" id="error-kgtara">
                        <strong></strong>
                    </span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group mb-0">
                    <label for="kgBruto" class="col-form-label">Kg Bruto</label>
                    {{-- LLeno el select con las materias primas de la db --}}
                    <input type="number" name="kgBruto" class="form-control" id="kgBruto">
                    <span class="invalid-feedback" role="alert" id="error-kgbruto">
                        <strong></strong>
                    </span>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group mb-0">
                    <label for="kgNeto" class="col-form-label">Kg Neto</label>
                    {{-- LLeno el select con las materias primas de la db --}}
                    <input type="number" name="kgNeto" class="form-control" id="kgNeto" readonly>
                    <span class="invalid-feedback" role="alert" id="error-kgneto">
                        <strong></strong>
                    </span>
                </div>
            </div>



            <div class="col-md-2">
                <div class="form-group mb-0">
                    <label for="" class="col-form-label">ㅤ</label>
                    {{-- LLeno el select con las materias primas de la db --}}
                    <button data-toggle="modal" data-target="#modalDurazno" class="btn btn-success form-control"
                        id="btnDurazno">Tamaño y cantidades</button>
                    {{-- <small id="tamanoDurazno" class="text-muted">Sin tamaño ni cantidad especificada</small> --}}
                </div>
            </div>
            <div class="col-md-2  offset-2">
                <div class="form-group mb-0">
                    <label for="indumentaria" class="col-form-label">ㅤ</label>
                    <button class="btn btn-success bg-hfrut form-control " id="agregar">Agregar</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12 table-responsive">
                <form action="" method="POST" id="formEntrega">
                    @csrf
                    <input type="hidden" name="fecha" id="fechaEntrega" value="">
                    {{-- ID del proveedor que trae la materia prima --}}
                    <input type="hidden" name="idProveedor" id="idProveedor" value="{{ $proveedor->id }}">
                    <table class="table table-striped" id="tablaCompras">
                        <thead>
                            <tr>
                                <th>Materia prima</th>
                                <th>Envase</th>
                                <th>Cant Envases</th>
                                <th>Kg Tara</th>
                                <th>Kg Bruto</th>
                                <th>Kg Neto</th>
                                {{-- <th style="width:10px">Eliminar</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            <tr class='text-center' id='vacio'>
                                <td colspan="6">Agregue una materia prima para registrar su ingreso</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="form-check mt-4">
                        <input class="form-check-input" type="checkbox" value="" id="chbObservaciones">
                        <div class="d-flex">
                            <label class="form-check-label mr-5" for="chbObservaciones">
                                Agregar observaciones de ingreso
                            </label>
                            <div class="badge badge-secondary p-2 d-none">
                                Tamaño durazno <i class="fa fa-times ml-3" aria-hidden="true" id="cancelarExtra"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mb-0 d-none" id="observaciones">
                                <label for="observaciones" class="col-form-label">Observaciones:</label>
                                <textarea name="observaciones" id="" cols="30" rows="2" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row justify-content-end px-3 mb-3">
                        <div id="tamanosUnidades">

                        </div>

                        <a href="{{ route('ingresoMP.index') }}" class="btn btn-danger btn-lg mr-2">
                            Cancelar
                        </a>
                        <button class="btn btn-success btn-lg" id="finalizarIngreso" type="button" disabled>
                            Finalizar ingreso
                        </button>
                    </div>
                </form>
            </div>
        </div>
        {{-- MODAL AGREGAR MATERIA PRIMA --}}
        <div class="modal fade" id="modalMateriaPrimaAgregar" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <h5 class="modal-title" id="labelInsumoServicioAgregar">Agregar nueva materia prima
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="post" id="formMateriaPrima">
                        <div class="modal-body">
                            <span id="formResult"></span>
                            <div class="form-group">
                                <label for="fruta" class="col-form-label">Nombre de la Fruta (*)</label>
                                <input type="text" name="fruta" class="form-control" id="fruta" autocomplete="off"
                                    placeholder="Fruta..." autofocus required>
                            </div>
                            <div class="form-group">
                                <label for="variedad" class="col-form-label">Variedad (*)</label>
                                <input type="text" name="variedad" class="form-control" id="variedad" autocomplete="off"
                                    placeholder="Variedad..." autofocus required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-success" id="btnAgregarMateriaPrima">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalDurazno" tabindex="-1" aria-labelledby="Modal Tamaño durazno"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">Tamaños y Cantidades</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span id="errorDuraznoCiruela"></span>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="chbDurazno"
                                value="option1" checked>
                            <label class="form-check-label" for="exampleRadios1">
                                Tamaño
                            </label>
                        </div>
                        <div id="contenedorDurazno">
                            <div class="row px-3 py-1">
                                <div class="col-md-12 ">
                                    <label for="" class="col-form-label">Tamaño chico</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">%</span>
                                        </div>
                                        <input type="number" class="form-control" id="tamanoChico">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="" class="col-form-label">Tamaño mediano</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">%</span>
                                        </div>
                                        <input type="number" class="form-control" id="tamanoMediano">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="" class="col-form-label">Tamaño grande</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">%</span>
                                        </div>
                                        <input type="number" class="form-control" id="tamanoGrande">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="chbCiruela"
                                value="option1">
                            <label class="form-check-label" for="exampleRadios1">
                                Cantidad
                            </label>
                        </div>
                        <div id="contenedorCiruela">
                            <div class="row px-3 py-1">
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label for="unidadesKg" class="col-form-label">Unidades por kg (Ciruela)</label>
                                        {{-- LLeno el select con las materias primas de la db --}}
                                        <input type="number" name="unidadesKg" class="form-control" id="unidadesKg">
                                        <span class="invalid-feedback" role="alert" id="error-unidadesKg">
                                            <strong></strong>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="btnGuardarTamano">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    @stop


    @section('js')
        <script>
            $(document).ready(function() {
                $('.sr-only').trigger('click');
                var cantidadInsumo = 0;
                var cantidadExcedida = '';
                var pesoEnvase = 0;
                var tamanoChico = 0;
                var tamanoMediano = 0;
                var tamanoGrande = 0;

                $("#contenedorCiruela *").prop('disabled', true);

                // $('#modalEmpleados').modal('show');
                $('#empleados').select2();
                $('#materiasPrima').select2();

                pesoEnvase = $('#envases').find(':selected').data('peso');
                $('#pesoLabel').text('Peso del envase: ' + pesoEnvase + ' kg');
                //=============== EVENTO SE CIERRA EL MODAL ========================

                $('#modalIndumentariaAgregarEditar').on('hide.bs.modal', function(event) {
                    $("#modalIndumentariaAgregarEditar :input").each(function(index) {
                        $(this).val('');
                    });
                })

                $('#envases').change(function(e) {
                    e.preventDefault();
                    pesoEnvase = $(this).find(':selected').data('peso');
                    $('#pesoLabel').text('Peso del envase: ' + pesoEnvase + ' kg');
                    var kgTara = (parseFloat($('#cantidadEnvase').val()) * parseFloat(pesoEnvase));
                    $('#kgTara').val(kgTara);
                    var kgNeto = (parseFloat($('#kgBruto').val())) - parseFloat($('#kgTara').val());
                    $('#kgNeto').val(kgNeto);
                });

                //====================== EVENTO CLICK DEL CHECKBOX PARA AGREGAR UN DETALLE A LA ENTREGA ===================
                $('#chbObservaciones').click(function(e) {
                    if ($(this).is(":checked")) {
                        $('#observaciones').removeClass('d-none');
                    } else {
                        $('#observaciones').addClass('d-none');
                    }

                });

                //===================== MODAL AGREGAR INSUMO SERVICIO ================================
                $('#modalIndumentariaAgregarEditar').on('shown.bs.modal', function(event) {
                    $('#nombre').focus();
                })

                $('#modalIndumentariaAgregarEditar').on('hide.bs.modal', function(event) {
                    $('#nombre').val('');
                    $('#cantidad').val('');
                    $('#resultadoFormAgregar').empty();
                })

                $('#modalDurazno').on('hide.bs.modal', function(event) {
                    $('#errorDuraznoCiruela').empty();
                    $('#tamanoChico').val('');
                    $('#tamanoMediano').val('');
                    $('#tamanoGrande').val('');
                    $('#unidadesKg').val('');
                })

                //===================== CHECKBOXES DENTRO DEL MODAL ================================
                $("#chbDurazno").change(function() {
                    if (this.checked) {
                        $("#contenedorCiruela *").attr('disabled', true);
                        $("#contenedorDurazno *").attr('disabled', false);
                        $('#errorDuraznoCiruela').empty();
                    }
                });

                $("#chbCiruela").change(function() {
                    if (this.checked) {
                        $("#contenedorDurazno *").attr('disabled', true);
                        $("#contenedorCiruela *").attr('disabled', false);
                        $('#errorDuraznoCiruela').empty();
                    }
                });

                //===================== CLICK BOTON GUARDAR EL TAMAÑO DE FRUTA ======================
                $('#btnGuardarTamano').click(function(e) {
                    e.preventDefault();
                    if ($('#chbDurazno').is(':checked')) {
                        //Si es NaN(el input esta vacio) Asigno un 0
                        tamanoChico = isNaN(parseFloat($('#tamanoChico').val())) ? 0 : parseFloat($(
                                '#tamanoChico')
                            .val());
                        tamanoMediano = isNaN(parseFloat($('#tamanoMediano').val())) ? 0 : parseFloat($(
                            '#tamanoMediano').val());
                        tamanoGrande = isNaN(parseFloat($('#tamanoGrande').val())) ? 0 : parseFloat($(
                            '#tamanoGrande').val());

                        if ((tamanoChico + tamanoMediano + tamanoGrande) != 100) {
                            var htmlError =
                                '<div class="alert alert-danger text-center" role="alert">La suma de todos los tamaños debe ser 100%</div>'
                            $('#errorDuraznoCiruela').html(htmlError);
                        } else {
                            $('.badge').html('Tamaños durazno -> Chico: ' + tamanoChico + '% | Mediano: ' +
                                tamanoMediano +
                                '% | Grande: ' + tamanoGrande +
                                '% <i class="fa fa-times ml-3" aria-hidden="true"></i>');
                            $('#modalDurazno').trigger('click');
                            //Agrego los campos al formulario para enviarlos en el request
                            $('#tamanosUnidades').html(
                                '<input type="hidden" name="tamanoChico" value="' + tamanoChico + '">' +
                                '<input type="hidden" name="tamanoMediano" value="' + tamanoMediano + '">' +
                                '<input type="hidden" name="tamanoGrande" value="' + tamanoGrande + '">' +
                                '<input type="hidden" name="unidadesKg" value="">'
                            );

                            // $('.badge').html(
                            //     'Tamaño de durazno <i class="fa fa-times ml-3" aria-hidden="true"></i>'
                            // );

                            $('.badge').removeClass('d-none');
                        }
                    } else {
                        var unidadesKg = parseFloat($('#unidadesKg').val());

                        var error = 0;
                        if (!$('#unidadesKg').val()) {
                            var htmlError =
                                '<div class="alert alert-danger text-center" role="alert">El campo unidades por kg no puede estar vacio</div>'
                            error += 1;
                        } else if (0 > $('#unidadesKg').val()) {
                            var htmlError =
                                '<div class="alert alert-danger text-center" role="alert">El campo unidades por kg no puede ser menor que 0</div>'
                            error += 1;
                        }
                        if (error == 0) {
                            // $('#tamanoDurazno').html('Unidades por kg ciruela: ' + unidadesKg);

                            $('#tamanosUnidades').html(
                                '<input type="hidden" name="tamanoChico" value="">' +
                                '<input type="hidden" name="tamanoMediano" value="">' +
                                '<input type="hidden" name="tamanoGrande" value="">' +
                                '<input type="hidden" name="unidadesKg" value="' + unidadesKg + '">'
                            );
                            $('#modalDurazno').trigger('click');

                            $('.badge').removeClass('d-none');
                            $('.badge').html(
                                'Unidades por kg: ' +
                                unidadesKg + ' <i class="fa fa-times ml-3" aria-hidden="true"></i>'
                            );
                        } else {
                            $('#errorDuraznoCiruela').html(htmlError);
                        }

                    }
                });

                //===================== CLICK EN LA X DENTRO DEL BADGE PARA CANCELAR EL TAMAÑO DE FRUTA ======================

                $('.badge').on('click', 'i', function(e) {
                    $('#tamanosUnidades').empty();
                    $('#tamanoDurazno').html('Sin tamaño ni cantidad especificada');
                    $('.badge').addClass('d-none');
                });

                //========================= FUNCION PARA COMPROBAR CAMPOS =========================
                //Funcion para comprobar que los campos cantidad y precio no esten vacios a la hora de agregar una compra
                //Paso como parametro el nombre del campo (cantidad o precio)
                function comprobarCampos(campo, nombre) {
                    //Primero defino una variable con la cantidad de caracteres del campo pero le saco los espacios
                    //para verificar que el usuario no haya introducido espacios y este contando como caracteres
                    var length = $.trim($('#' + campo).val()).length;

                    //Si length es igual a 0 significa que el campo esta vacio
                    if (length == 0) {
                        //Hago aparecer la alerta de error y le asigno el mensaje de error
                        $('#' + campo).addClass('is-invalid');
                        $('#error-' + nombre).addClass('d-block');
                        $('#error-' + nombre).children().text('El campo ' + nombre + ' no puede estar vacio');
                        //Retorno falso para que no se continue con el procedimiento de compra
                        return false;
                        //Ahora compruebo que no se haya introducido un numero negativo
                    } else if ($('#' + campo).val() < 0) {
                        //Si se introdujo un numero negativo hago aparecer el mensaje de error
                        $('#error-' + nombre).addClass('d-block');
                        $('#error-' + nombre).children().text(nombre + ' no puede ser menor que 0');
                        //Retorno falso para no continuar con el procedimiento de compra
                        return false
                    }
                    //Si todo esta ok se retorna verdadero y se continua con la compra
                    return true;
                }
                //======================== EVENTO CLICK BOTON AGREGAR A TABLA ==============================           
                $('#agregar').click(function(e) {
                    var error = 0;
                    console.log()
                    if (!$('#cantidadEnvase').val()) {
                        invalidFeedback('#cantidadEnvase', '#error-cantidadenvase',
                            'Debe especificar una cantidad');
                        error += 1;
                    } else if (0 > $('#cantidadEnvases').val()) {
                        invalidFeedback('#cantidadEnvase', '#error-cantidadenvase',
                            'Cantidad no puede ser menor que 0');
                        error += 1;
                    } else {
                        removeFeedback('#cantidadEnvase', '#error-cantidadenvase')
                    }
                    if (!$('#kgBruto').val()) {
                        invalidFeedback('#kgBruto', '#error-kgbruto',
                            'Debe especificar una cantidad');
                        error += 1;
                    } else if (0 > $('#kgBruto').val()) {
                        invalidFeedback('#kgBruto', '#error-kgbruto',
                            'Kg bruto no puede ser menor que 0');
                        error += 1;
                    } else if (parseFloat($('#kgTara').val()) > parseFloat($('#kgBruto').val())) {
                        invalidFeedback('#kgBruto', '#error-kgbruto',
                            'Kg bruto no puede ser menor kg tara');
                        error += 1;
                    } else {
                        removeFeedback('#kgBruto', '#error-kgbruto')
                    }

                    if (error == 0) {
                        $('#vacio').addClass('d-none');
                        agregarATabla();
                        $(this).attr('disabled', true);
                        $('#finalizarIngreso').attr('disabled', false);
                    }
                });


                //====================== FUNCION AGREGAR UN INSUMO AL "CARRITO" ======================
                function agregarATabla() {

                    //Guardo todos los valores de los inputs anteriores en una respectiva variable para mantener
                    //el orden en el proceso siguiente
                    var nombreMateriaPrima = $('#materiasPrima option:selected').text();
                    var idMateriaPrima = $('#materiasPrima').val();
                    var nombreEnvase = $('#envases option:selected').text();
                    var idEnvase = $('#envases').val()
                    var cantidadEnvase = $('#cantidadEnvase').val();
                    var kgTara = $('#kgTara').val();
                    var kgBruto = $('#kgBruto').val();
                    var kgNeto = $('#kgNeto').val();
                    var unidadesKg = $('#unidadesKg').val();

                    if (!idMateriaPrima) {
                        Swal.fire(
                            'Debe elegir una materia prima',
                            '',
                            'error'
                        )
                        return
                    } else if (!idEnvase) {
                        Swal.fire(
                            'Debe elegir un envase',
                            '',
                            'error'
                        )
                        return
                    }

                    //Defino una variable y la lleno con codigo HTML, este codigo representa una fila en la tabla que ya
                    //tengo creada, asigno a cada campo de la fila un inpud de tipo hidden para poder enviar los datos
                    //en el formulario y asi posteriormente guardarlos.                    
                    //Se definen los nombres de los inputs con un [] para que se puedan enviar varios datos en un solo array
                    //y luego recorrerlos en el controlador

                    row = "<tr>" +
                        //Campo materia prima en la fila, muestro el nombre pero en el input va el valor del id
                        `<td>` + nombreMateriaPrima +
                        `<input type='hidden' name='materiaPrima'value='` + idMateriaPrima + `'></td>` +
                        `<td>` + nombreEnvase + `<input type='hidden' name='idEnvase'value='` + idEnvase + `'></td>` +
                        `<td>` + cantidadEnvase + `<input type='hidden' name='cantidadEnvase'value='` + cantidadEnvase +
                        `'></td>` +
                        `<td>` + kgTara + `<input type='hidden' name='kgTara'value='` + kgTara + `'></td>` +
                        `<td>` + kgBruto + `<input type='hidden' name='kgBruto'value='` + kgBruto + `'></td>` +
                        `<td>` + kgNeto + `<input type='hidden' name='kgNeto'value='` + kgNeto + `'></td>` +
                        `<td><input type='hidden' name='unidadesKg'value='` + unidadesKg + `'></td>` +
                        //Boton para eliminar una fila si es necesario
                        // `<td id="DeleteButton"><a href="#" class="badge badge-danger" >Eliminar X</a></td>` +
                        `</tr>`;

                    //Guardo el cuerpo de la tabla en una variable
                    tableBody = $("table tbody");
                    //Aca agrego la fila que creamos anteriormente
                    tableBody.append(row);
                    //Vacio los inputs
                    $('#cantidad_').val('');
                }


                //========================== EVENTOS KEYUP DE CANTIDAD ENVASES ==========================

                $('#cantidadEnvase').keyup(function(e) {
                    var kgTara = (parseFloat($('#cantidadEnvase').val()) * parseFloat(pesoEnvase));
                    $('#kgTara').val(kgTara);
                    var kgNeto = (parseFloat($('#kgBruto').val())) - parseFloat($('#kgTara').val());
                    $('#kgNeto').val(kgNeto);
                });

                $('#kgBruto').keyup(function(e) {
                    var kgNeto = (parseFloat($(this).val()) - parseFloat($('#kgTara').val()));
                    $('#kgNeto').val(kgNeto);
                });

                //======================== EVENTO CLICK ELIMINAR COMPRA =========================================
                $("#tablaCompras").on("click", "#DeleteButton", function() {


                    //Si la tabla tiene 3 tr, que en este caso no tiene ninguna pero jq lo tomas asi
                    //Significa que ya no hay nada en el "carro" por lo que vuelo a mostrar la fila que dice agregue una materia prima
                    if ($('#tablaCompras tr').length == 3) {
                        $('#vacio').removeClass('d-none');
                    }

                    //Borra la fila
                    $(this).closest("tr").remove();
                });

                //========================== FINALIZAR COMPRA ======================================
                $('#finalizarIngreso').click(function(e) {
                    e.preventDefault();
                    $('#fechaEntrega').val($('#fechaElegida').val());
                    var form = $(this).closest("form");
                    var name = $(this).data("name");
                    //Muestro un mensaje de confirmacion 
                    Swal.fire({
                            title: `¿Desea registrar este ingreso?`,
                            text: "",
                            icon: "question",
                            showCancelButton: true,
                            confirmButtonText: 'Si',
                            confirmButtonColor: '#29643e',
                            cancelButtonText: 'No',
                        })
                        .then((response) => {
                            if (response.isConfirmed) {
                                $.ajax({
                                    url: "{{ route('ingresoMP.store') }}",
                                    method: 'POST',
                                    data: $('#formEntrega').serialize() +
                                        "&_token={{ csrf_token() }}",
                                    dataType: 'json',
                                    success: function(data) {
                                        if (data.error) {
                                            console.log(data.error)
                                            Swal.fire({
                                                icon: 'warning',
                                                title: 'Oops...',
                                                text: data.error,
                                            })
                                        }
                                        if (data.success) {
                                            Swal.fire({
                                                toast: true,
                                                icon: 'success',
                                                title: data.success +
                                                    '. Espere unos instantes',
                                                position: 'top-right',
                                                showConfirmButton: false,
                                                timer: 2000,
                                            }).then(function() {
                                                window.location.href =
                                                    "{{ route('ingresoMP.index') }}";
                                            });
                                        }
                                    }
                                })
                            }
                        });
                });

                $('#btnAgregarMateriaPrima').on('click', function(event) {
                    //Evito que se recargue la pagina
                    event.preventDefault();
                    //Envio la peticion del tipo POST a traves de ajax
                    $.ajax({
                        url: "{{ route('purchase-raw-material.storeFromPurchase') }}",
                        method: 'POST',
                        data: $('#formMateriaPrima').serialize() + "&_token={{ csrf_token() }}",
                        dataType: 'json',
                        success: function(data) {
                            var html = '';
                            if (data.errors) {
                                //Si hay algun error en la validacion hago un elemento html con la clase alert-danger
                                //recorro los errores y los voy agregando al html. Despues los muestro
                                html =
                                    '<div class = "alert alert-danger pb-0" role = "alert" id="alertaErrores"><ul>';
                                for (var i = 0; i < data.errors.length; i++) {
                                    html += '<li>' + data.errors[i] + '</li>';
                                }
                                html += '</ul></div>';
                                $('#formResult').html(html);
                            }
                            if (data.success) {
                                //Si se agrego correctamente el insumo servicio vacio el select
                                $('#materiasPrima').empty();
                                //Obtengo todos los insumos - servicios nuevamente
                                $.get("/materias-primas-json", function(data) {
                                    var materiasPrimas = data;
                                    //Los recorro y los voy agregando dentro del select
                                    for (var i = 0; i < materiasPrimas.length; i++) {
                                        $('#materiasPrima').append('<option value=' +
                                            materiasPrimas[i].id + '>' +
                                            materiasPrimas[i].fruta + ', ' +
                                            materiasPrimas[i].variedad +
                                            '</option>');
                                    }
                                    //Selecciono la ultima opcion (La que se agrego ultimamente)
                                    $('#materiasPrima option:last').attr('selected',
                                        'selected');
                                    //Muestro una alerta diciendo que la operacion se completo con exito
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: 'Materia prima agregada con éxito',
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                    //Escondo el modal
                                    $('#modalMateriaPrimaAgregar').trigger('click');
                                    $('#fruta').val('');
                                    $('#variedad').val('');
                                });
                            }
                        }
                    })
                });
                //==================== FUNCION ERROR DE VALIDACION ============================
                function invalidFeedback(input, labelInvalid, text) {
                    $(input).addClass('is-invalid');
                    $(labelInvalid).children().text(text);
                }
                //==================== FUNCION SACAR ERROR DE VALIDACION ============================
                //Revierte la funcion de invalidFeedback()
                function removeFeedback(input, labelInvalid) {
                    $(input).removeClass('is-invalid');
                    $(labelInvalid).children().text('');
                }
            });
        </script>
    @stop
