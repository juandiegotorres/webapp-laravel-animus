@extends('adminlte::page')

@section('title', 'Ingreso de materia prima')

@section('content_header')
    <div class="container-fluid">
        <a class="float-right btn btn-success bg-hfrut" href="{{ route('ingresoMP.create') }}" data-toggle='modal'
            data-target='#modalProveedoresMP'> <i class="fa fa-plus mr-2" aria-hidden="true"></i>
            Agregar
        </a>
        <a class="float-right btn btn-primary mr-3" data-toggle='modal' data-target='#modalFiltro'><i
                class="fa fa-filter mr-2" aria-hidden="true"></i>
            Filtrar
        </a>
        <h1>Ingreso de materia prima</h1>
    </div>
@stop

@section('content')
    {{-- MODAL FILTRO --}}
    <div class="modal fade" id="modalFiltro" tabindex="-1" role="dialog" aria-labelledby="modalFiltro"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Filtros</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span id="formResult"></span>
                    <div class="row">
                        <div class="col-md-12 d-flex">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fechaInicio">Fecha de Inicio <button
                                            class="fa fa-question-circle btn p-0 m-0" data-toggle="tooltip"
                                            data-placement="bottom"
                                            title="Si no se elige una fecha de inicio, no se filtrará por fecha. Solo por proveedor"></button></label>
                                    <input type="date" name="fechaInicio" class="form-control" id="fechaInicio" required
                                        max='2099-12-31' min='1900-01-01'>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fechaInicio">Fecha de Fin</label>
                                    <input type="date" name="fechaFin" class="form-control"
                                        value="{{ Carbon\Carbon::today()->format('Y-m-d') }}" id="fechaFin"
                                        max='2099-12-31' min='1900-01-01' required>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="form-check ">
                                {{-- <input class="form-check-input" type="checkbox" value="" id="chbProveedores"> --}}
                                {{-- <label class="form-check-label" for="flexCheckDefault">
                                         <strong> Proveedor</strong>
                                     </label> --}}
                            </div>
                            <div class="form-group mx-3">
                                <label for="proveedores" class="col-form-label">Proveedor</label>
                                <select name="proveedores" id="proveedoresFiltro" class="form-control">
                                    @if ($proveedores->count() == 0)
                                        <option value="">Sin proveedores disponibles</option>
                                    @endif
                                    <option value="">Seleccione un proveedor</option>
                                    @foreach ($proveedores as $proveedor)
                                        <option value="{{ $proveedor->id }}" data-cuit="{{ $proveedor->cuit }}"
                                            data-telefono="{{ $proveedor->telefono }}"
                                            data-direccion="{{ $proveedor->direccion }}">
                                            {{ $proveedor->nombreCompleto }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id='matPrimaOinsServicio'>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-primary" data-tipo="" id="btnFiltrar">Buscar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalProveedoresIS" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title" id="modalTituloIS">Proveedores</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="proveedores" class="col-form-label">Seleccione el proveedor:</label>
                                <select name="proveedores" id="proveedoresIS" class="form-control"></select>
                                <span class="invalid-feedback" role="alert" id="errorAgregarIS">
                                    <strong>Error</strong>
                                </span>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-form-label">CUIT:</label>
                                <input type="text" class="form-control" readonly id="cuitIS">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-form-label">Teléfono:</label>
                                <input type="text" class="form-control" readonly id="telefonoIS">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="col-form-label">Dirección:</label>
                                <input type="text" class="form-control" readonly id="direccionIS">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <a href="" class="btn btn-success" id="btnComprarIS">Proceder con la compra </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    <div class="row justify-content-end">
                        <div class="col-md-4 align-self-end pb-3 text-right">
                            <span class="badge badge-secondary p-2 d-none" id="badgeFiltro">
                                <i class="fas fa-times pl-3"></i>
                            </span>
                        </div>
                    </div>
                    <table class="table table-bordered table-rounded" id="tablaIngresos">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Proveedor</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Materia Prima</th>
                                <th scope="col">KG Neto</th>
                                <th scope="col">Descartado % </th>
                                <th scope="col" class="text-center" style="width: 20%;">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>

    @can('ingresoMP.create')
        <div class="modal fade" id="modalProveedoresMP" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <h5 class="modal-title" id="modalTituloMP">Proveedores de materia prima</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="proveedores" class="col-form-label">Seleccione el proveedor:</label>
                                    <select name="proveedores" id="proveedores" class="form-control">

                                        @if ($proveedores->count() == 0)
                                            <option value="">Sin proveedores disponibles</option>
                                        @endif
                                        @foreach ($proveedores as $proveedor)
                                            <option value="{{ $proveedor->id }}" data-cuit="{{ $proveedor->cuit }}"
                                                data-telefono="{{ $proveedor->telefono }}"
                                                data-direccion="{{ $proveedor->direccion }}">
                                                {{ $proveedor->nombreCompleto }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-form-label">CUIT:</label>
                                    <input type="text" class="form-control" readonly id="cuitMP"
                                        value="{{ $proveedores->count() >= 1 ? $proveedores->first()->cuit : '' }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Teléfono:</label>
                                    <input type="text" class="form-control" readonly id="telefonoMP"
                                        value="{{ $proveedores->count() >= 1 ? $proveedores->first()->telefono : '' }}">
                                </div>
                            </div>
                        </div>
                        <div class=" row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Dirección:</label>
                                    <input type="text" class="form-control" readonly id="direccionMP"
                                        value="{{ $proveedores->count() >= 1 ? $proveedores->first()->direccion : '' }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <a href="{{ route('ingresoMP.create', ['idProveedor' => $proveedores->count() >= 1 ? $proveedores->first()->id : null]) }}"
                            class="btn btn-success" id="btnIngresoMP">Proceder con el ingreso </a>
                    </div>
                </div>
            </div>
        </div>
    @endcan
@stop

@section('css')
    <style>
        .boton:focus {
            outline: none !important;
            box-shadow: none !important;
        }

        .boton {
            box-shadow: none !important
        }

    </style>
@stop

@section('js')
    <script>
        $(document).ready(function() {
            var tablaIngresos = '';
            $('#proveedores').select2();

            $('#proveedoresFiltro').select2({
                dropdownParent: $("#modalFiltro"),
            });

            $('body').tooltip({
                selector: '[data-toggle="tooltip"]'
            });

            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });

            $('[data-toggle="tooltip"]').on('click', function() {
                $(this).tooltip('hide')
            });

            $(document).on('click', '.boton', function(e) {
                $('[data-toggle="tooltip"]').tooltip('hide')

            });



            //Hacer focus en la barra de busqueda cuando se abre el select de bancos
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });
            //======================== EVENTO CUANDO CAMBIA SELECT PROVEEDORES DENTRO DEL MODAL ==================
            $('#proveedores').change(function(e) {
                var selected = $('#proveedores').find('option:selected');
                var cuit = selected.data('cuit');
                var telefono = selected.data('telefono');
                var direccion = decodeURIComponent(selected.data('direccion'));
                $('#cuitMP').val(cuit);
                $('#telefonoMP').val(telefono);
                $('#direccionMP').val(direccion);
                var idProv = $('#proveedores').val();
                var url = '/ingreso/materia-prima/crear/' + idProv;
                $('#btnIngresoMP').attr('href', url);
            });

            //============================== BTN ELIMINAR INGRESO ================================
            $(document).on('click', '.btnEliminarIngreso', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea dar de baja este ingreso?",
                    // text: "Una vez eliminada, no se puede recuperar",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "put",
                            url: "/ingreso/materia-prima/eliminar/" + id,
                            data: "_token={{ csrf_token() }}",
                            dataType: "json",
                            success: function(response) {
                                if (response.success) {
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: response.success,
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });

                                    tablaIngresos.ajax.reload();

                                } else if (response.error) {
                                    Swal.fire({
                                        toast: false,
                                        icon: 'error',
                                        title: response.error,
                                        position: 'center',
                                        showConfirmButton: true,
                                    });
                                }
                            }
                        });
                    }
                });
            });

            //==================== EVENTO CLICK PARA FILTRAR ============================
            $('#btnFiltrar').click(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{{ route('ingresoMP.filtrar') }}",
                    data: {
                        fechaInicio: $('#fechaInicio').val(),
                        fechaFin: $('#fechaFin').val(),
                        proveedor: $('#proveedoresFiltro').val(),
                        _token: "{{ csrf_token() }}",
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.error) {

                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#formResult').html(html);
                            return
                        } else {
                            tablaIngresos.clear().rows.add(data.data).draw();
                            $('#badgeFiltro').removeClass('d-none');
                            var fechaInicio = new Date($('#fechaInicio').val())
                            var fechaFin = new Date($('#fechaFin').val())
                            var nombreProveedor = $('#proveedoresFiltro :selected').text();
                            if ($('#proveedoresFiltro').val() == "") {
                                $('#badgeFiltro').html('Filtrando entre ' + fecha.format(
                                        fechaInicio) +
                                    ' y ' + fecha.format(
                                        fechaFin) +
                                    '<i class="fas fa-times pl-3"></i>');
                            } else if ($('#fechaInicio').val() == "") {
                                $('#badgeFiltro').html('Filtrando por proveedor: ' +
                                    nombreProveedor + '<i class="fas fa-times pl-3"></i>');
                            } else {
                                $('#badgeFiltro').html('Filtrando entre ' + fecha.format(
                                        fechaInicio) + ' y ' +
                                    fecha.format(
                                        fechaFin) +
                                    '. Proveedor ' +
                                    nombreProveedor + '<i class="fas fa-times pl-3"></i>');
                            }
                            $('#modalFiltro').trigger('click');
                        }
                    }
                });
            });
            //====================== EVENTO CLICK EN LA X DEL TAG ==============================
            //Cuando hago click en la X del filtro me vuelve a mostrar todos los registros nuevamente
            $('#badgeFiltro').on('click', 'i', function(e) {
                tablaIngresos.ajax.reload();
                $('#badgeFiltro').addClass('d-none');
                $('#fechaInicio').val('')
            });


            var fecha = new Intl.DateTimeFormat("es-AR");

            //==============EVENTO CUANDO SE ESCONDE EL MODAL ==========================
            $("#modalFiltro").on("hide.bs.modal", function(event) {
                //Borro si hay algun error
                $('#formResult').children().remove();
                $('#proveedoresFiltro option:eq(0)').prop('selected', true);
                $('#proveedoresFiltro').trigger('change.select2');
                $('#fechaInicio').empty();
            });

            //======================= DATATABLE =============================
            tablaIngresos = $('#tablaIngresos').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                bFilter: true,
                bLengthChange: true,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "/ingreso/materia-prima/dt",
                columns: [{
                        data: 'nombreCompleto'
                    },
                    {
                        data: 'fecha'
                    },
                    {
                        data: 'materiaPrima'
                    },
                    {
                        data: 'kgNeto',
                    },
                    {
                        data: 'cantidadDescartada',
                    },
                    {
                        data: 'opciones'
                    },
                    {
                        data: 'created_at',
                        visible: false,
                    }
                ],
                order: [6, 'desc'],
            });

        });
    </script>
@stop
