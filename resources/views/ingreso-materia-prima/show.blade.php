@extends('adminlte::page')

@section('title', 'Historial Compra')

@section('content_header')
@stop

@section('content')
    <div class="row mt-3">
        <div class="col-12">

            <div class="invoice p-3 mb-3">
                <div class="row">
                    <div class="col-12">
                        <h4>
                            <i class="fas fa-globe"></i> HFRUT
                            <small class="float-right">Fecha:
                                {{ date('d-m-y', strtotime($rawMaterialEntry->fecha)) }}</small>
                        </h4>
                    </div>
                </div>
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col my-3">
                        ID Compra: <strong>#{{ $rawMaterialEntry->id }}</strong> <br>
                        Proveedor: <a
                            href="{{ route('providers.show', ['provider' => $rawMaterialEntry->provider_id]) }}"><strong>{{ $rawMaterialEntry->proveedor->nombreCompleto }}</strong></a><br>
                        Observaciones:
                        {{ $rawMaterialEntry->observaciones == null ? 'Sin observaciones' : $rawMaterialEntry->observaciones }}
                    </div>
                </div>
                <h5 class="ml-2"><b>Materia prima</b></h5>
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Materia Prima</th>
                                    <th>KG Tara</th>
                                    <th>KG Brutos</th>
                                    <th>KG Netos</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $rawMaterialEntry->materiaPrima->fruta }},
                                        {{ $rawMaterialEntry->materiaPrima->variedad }}
                                    </td>
                                    <td>{{ $rawMaterialEntry->kgTara }} kg</td>
                                    <td>{{ $rawMaterialEntry->kgBruto }} kg</td>
                                    <td>{{ $rawMaterialEntry->kgNeto }} kg</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <h5 class="mt-4 ml-2"><b>Envases</b></h5>
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped text-center">
                            <thead>
                                <tr>
                                    <th>Envase</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($rawMaterialEntry->envase as $detalle)
                                    <tr>
                                        <td>{{ $detalle->envase->nombre }}</td>
                                        <td>{{ $detalle->cantidadEnvase }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row px-2 mt-5">
                    <div class="col-md-6">
                        @if ($rawMaterialEntry->unidadesKg == null)
                            <h5><b>Tamaño de durazno</b></h5>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Tamaño chico: {{ $rawMaterialEntry->tamanoChico }} %
                                </li>
                                <li class="list-group-item">Tamaño mediano: {{ $rawMaterialEntry->tamanoMediano }}
                                    %</li>
                                <li class="list-group-item">Tamaño grande: {{ $rawMaterialEntry->tamanoGrande }} %
                                </li>
                            </ul>
                        @elseif ($rawMaterialEntry->tamanoChico == null)
                            <h5><b>Unidades por kg: {{ $rawMaterialEntry->unidadesKg }}</b></h5>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                    </div>
                    <div class="col-6">
                        <h2 class="text-right pr-3 my-3">Total: {{ $rawMaterialEntry->kgNeto }} kg</h2>
                    </div>
                </div>
                <div class="row no-print">
                    <div class="col-12">
                        <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-danger">Volver</a>

                        {{-- <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-dark float-right"><i
                                class="fas fa-print"></i> Imprimir</a> --}}
                        {{-- <h1>{{ $rawMaterialEntry->id }}</h1> --}}
                        <a href="{{ route('ingreso-fruta.pdf', ['ingreso' => $rawMaterialEntry->id]) }}" target="_blank"
                            class="btn btn-primary float-right" style="margin-right: 5px;">
                            <i class="fas fa-download"></i> Generar PDF
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop



@section('js')

@stop
