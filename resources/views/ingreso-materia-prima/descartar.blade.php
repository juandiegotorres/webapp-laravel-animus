@extends('adminlte::page')

@section('title', 'Historial Compra')

@section('content_header')
@stop

@section('content')
    <div class="row mt-3">
        <div class="col-12">

            <div class="invoice p-3 mb-3">
                <div class="row">
                    <div class="col-12">
                        <h4>
                            Descartar de fruta
                            <small class="float-right">Fecha de hoy: {{ date('d-m-Y', strtotime(now())) }}</small>

                        </h4>
                        <h5 class="mt-4 mb-1"><u>Información del ingreso de materia prima:</u></h5>
                    </div>
                </div>
                <div class="row invoice-info">
                    <div class="col-sm-4 mb-4">
                        ID Compra: <strong>#{{ $rawMaterialEntry->id }}</strong> <br>
                        Proveedor: <a
                            href="{{ route('providers.show', ['provider' => $rawMaterialEntry->provider_id]) }}"><strong>{{ $rawMaterialEntry->proveedor->nombreCompleto }}</strong></a><br>
                        Fecha de ingreso: {{ date('d-m-y', strtotime($rawMaterialEntry->fecha)) }} <br>
                        Observaciones:
                        {{ $rawMaterialEntry->observaciones == null ? 'Sin observaciones' : $rawMaterialEntry->observaciones }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="resultadoForm">

                    </div>
                </div>
                <div class="row">
                    <div class="col-12 table-responsive">
                        @if ($historialDescarte->count() > 0)
                            <table class="table table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>Materia Prima</th>
                                        <th>Cantidad</th>
                                        <th>Cantidad a descartar (%)</th>
                                        <th>Observaciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- {{ $totalKg = 0 }} --}}
                                    <tr>
                                        <td colspan="4">
                                            <h5>
                                                No se pueden ejecutar mas descartes, elimine el actual para poder cambiar el
                                                %
                                                de descarte.
                                            </h5>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        @else
                            <table class="table table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>Materia Prima</th>
                                        <th>Cantidad</th>
                                        <th>Cantidad a descartar (%)</th>
                                        <th>Observaciones</th>
                                    </tr>
                                </thead>
                                <form action="" id="formDescarte">
                                    <tbody>
                                        {{-- {{ $totalKg = 0 }} --}}
                                        <tr>
                                            <td>{{ $rawMaterialEntry->materiaPrima->fruta }},
                                                {{ $rawMaterialEntry->materiaPrima->variedad }}

                                                <input type="hidden" name="id_ingreso"
                                                    value="{{ Crypt::encrypt($rawMaterialEntry->id) }}">
                                            </td>
                                            <td>{{ $rawMaterialEntry->kgNeto }} kg
                                                <input type="hidden" name="cantidad_inicial"
                                                    value="{{ $rawMaterialEntry->kgNeto }}">
                                            </td>
                                            <td><input type="number" class="form-control" name="porcentajeDescarte"
                                                    value="">
                                            </td>
                                            <td><input type="text" class="form-control" name="observaciones" value="">
                                            </td>
                                        </tr>
                                    </tbody>
                                </form>

                            </table>
                            <div class="row mt-4">
                                <div class="col-12 d-flex justify-content-end">
                                    <a href="{{ redirect()->back()->getTargetUrl() }}"
                                        class="btn btn-danger mr-2">Volver</a>
                                    <a class="btn btn-orange mr-2" data-id="{{ $rawMaterialEntry->id }}"
                                        id="btnDescartar">Ejecutar
                                        descarte</a>

                                </div>
                            </div>
                        @endif
                    </div>
                    @if ($historialDescarte->count() > 0)
                        <div class="col-md-12 table-responsive mt-3">
                            <h4>Historial de descarte de este ingreso</h4>
                            <table class="table table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>Materia Prima</th>
                                        <th>Porcentaje descartado</th>
                                        <th>Cantidad descartada</th>
                                        <th>Fecha de descarte</th>
                                        <th>Usuario</th>
                                        <th>Observaciones</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($historialDescarte as $detalle)
                                        <tr>
                                            <td>{{ $detalle->ingreso->materiaPrima->fruta }},
                                                {{ $detalle->ingreso->materiaPrima->variedad }}
                                            </td>
                                            <td>{{ $detalle->porcentaje }} %</td>
                                            <td>{{ $detalle->cantidadKg }} kg</td>
                                            <td>{{ date('d-m-Y', strtotime($detalle->fechaMovimiento)) }}
                                            </td>
                                            <td>{{ $detalle->usuario->username }}</td>
                                            <td>{{ $detalle->observaciones }}</td>
                                            <td><a data-id="{{ $detalle->id }}"
                                                    data-idproveedor="{{ $rawMaterialEntry->provider_id }}"
                                                    data-idingreso="{{ $rawMaterialEntry->id }}"
                                                    class="btn btn-danger btn-sm btnEliminar"><i class="fa fa-trash"
                                                        aria-hidden="true"></i></a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
@stop



@section('js')
    <script>
        $(document).ready(function() {

            $('#btnDescartar').on('click', function(event) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea ejecutar este descarte?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "post",
                            url: "/ingreso/materia-prima/descartar/" + id,
                            data: $('#formDescarte').serialize() +
                                "&_token={{ csrf_token() }}",
                            dataType: "json",
                            success: function(data) {
                                if (data.error) {
                                    html =
                                        '<div class="alert alert-danger alert-dismissible fade show" role="alert"><ul>';
                                    for (var i = 0; i < data.error.length; i++) {
                                        html += '<li>' + data.error[i] + '</li>';
                                    }
                                    html +=
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></ul></div>';
                                    $('#resultadoForm').html(html);
                                }
                                if (data.success) {
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: data.success +
                                            '. Espere unos instantes',
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2000,
                                    }).then(function() {
                                        window.location.href =
                                            "{{ route('ingresoMP.index') }}";
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $(document).on('click', '.btnEliminar', function(e) {
                var id = $(this).data('id');
                var idingreso = $(this).data('idingreso');
                Swal.fire({
                    title: "¿Desea eliminar este descarte?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post('/ingreso/materia-prima/eliminar/descarte/' + id + '/' +
                                    idingreso, {
                                        _method: "delete"
                                    })
                                .then(respuesta => {
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: respuesta.data.success,
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                            window.location.reload(true);
                            // location.reload(forceGet);
                        }
                    }
                });
            });

            function comprobarCampos() {
                var contador = 0;
                $("#formDescarte input[type=number]").each(function() {
                    if (this.value == 0) {
                        contador += 1;
                    }
                });
                if (contador == 2) {
                    return false
                } else {
                    return true
                }
            }
        });
    </script>
@stop
