@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="container-fluid px-4">
        <div class="row mb-1">
            <h1>Nuevo ingreso de materia prima</h1>
        </div>
        <div class="row d-flex justify-content-between align-items-center">
            <h5>Proveedor: {{ $proveedor->nombreCompleto }} <span class="text-muted"> - CUIT:
                    {{ $proveedor->cuit }}
                </span></h5>
            <div class="col-md-3">
                <input type="date" class="form-control" id="fechaElegida" value="{{ now()->format('Y-m-d') }}"
                    max="{{ now()->format('Y-m-d') }}" min="{{ now()->subDays(7)->format('Y-m-d') }}">
            </div>
        </div>
    </div>
@stop

@section('css')
    <style>
        .badge-danger {
            cursor: pointer !important;
        }

    </style>
@endsection

@section('content')
    <form action="" class="row" id="formEntrega">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title mb-0"><b>Seleccionar envases</b></h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label for="">Envase (*)</label>
                                <select name="envases" id="envases" class="form-control">
                                    @foreach ($envases as $envase)
                                        <option value="{{ $envase->id }}" data-peso="{{ $envase->peso }}">
                                            {{ $envase->nombre }}</option>
                                    @endforeach
                                </select>
                                <small id="pesoLabel" class="text-muted"></small>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Cantidad (*)</label>
                                <input type="number" name="cantidadEnvase" id="cantidadEnvase" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">ㅤ</label>
                                <button class="btn btn-primary" id="btnAgregarEnvase"><i class="fa fa-plus"
                                        aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-sm">
                                <thead class="table-active">
                                    <tr>
                                        <th scope="col">Envase</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col">KG</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="tablaEnvases">
                                </tbody>
                            </table>
                            <span class="invalid-feedback d-block" role="alert" id="error-cantidadenvase">
                                <strong></strong>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title mb-0"><b>Seleccionar materia prima</b></h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label for="">Materias primas (*)</label>
                                <select name="materiasPrima" id="materiasPrima" class="form-control">
                                    @foreach ($materiasPrimas as $materiaPrima)
                                        <option value="{{ $materiaPrima->id }}">
                                            {{ $materiaPrima->fruta . ', ' . $materiaPrima->variedad }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="kgBruto">Kg Bruto</label>
                                {{-- LLeno el select con las materias primas de la db --}}
                                <input type="number" name="kgBruto" class="form-control" id="kgBruto">
                                <span class="invalid-feedback" role="alert" id="error-kgbruto">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kgTara">Kg Tara</label>
                                {{-- LLeno el select con las materias primas de la db --}}
                                <input type="number" name="kgTara" class="form-control" id="kgTara" readonly>
                                <span class="invalid-feedback" role="alert" id="error-kgtara">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-0">
                                <label for="kgNeto">Kg Neto</label>
                                {{-- LLeno el select con las materias primas de la db --}}
                                <input type="number" name="kgNeto" class="form-control" id="kgNeto" readonly>
                                <span class="invalid-feedback" role="alert" id="error-kgneto">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="badge badge-secondary p-2 d-none" id="badge">
                                Tamaño durazno <i class="fa fa-times ml-3" aria-hidden="true" id="cancelarExtra"></i>
                            </div>
                        </div>
                        <div class="col-md-12" id="tamanosUnidades">
                        </div>
                        <div class="col-md-12 mt-3">
                            <a data-toggle="modal" data-target="#modalDurazno" class="btn btn-primary w-100"
                                id="btnDurazno">Tamaños y cantidades</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-check mt-4">
                <input class="form-check-input" type="checkbox" value="" id="chbObservaciones">
                <div class="d-flex">
                    <label class="form-check-label mr-5" for="chbObservaciones">
                        Agregar observaciones de ingreso
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group mb-0 d-none" id="observaciones">
                        <label for="observaciones" class="col-form-label">Observaciones:</label>
                        <textarea name="observaciones" id="" cols="30" rows="2" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row justify-content-end px-3 mb-3">
                <input type="hidden" name="fecha" id="fechaEntrega" value="">
                <input type="hidden" name="idProveedor" id="idProveedor" value="{{ $proveedor->id }}">
                <a href="{{ route('ingresoMP.index') }}" class="btn btn-danger mr-2">
                    Cancelar
                </a>
                <button class="btn btn-success" id="finalizarIngreso" type="button">
                    Finalizar ingreso
                </button>
            </div>
        </div>
    </form>
    {{-- MODAL TAMAÑOS Y CANTIDADES --}}
    <div class="modal fade" id="modalDurazno" tabindex="-1" aria-labelledby="Modal Tamaño durazno" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Tamaños y Cantidades</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span id="errorDuraznoCiruela"></span>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="chbDurazno" value="option1"
                            checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Tamaño
                        </label>
                    </div>
                    <div id="contenedorDurazno">
                        <div class="row px-3 py-1">
                            <div class="col-md-12 ">
                                <label for="" class="col-form-label">Tamaño chico</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">%</span>
                                    </div>
                                    <input type="number" class="form-control" id="tamanoChico">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="" class="col-form-label">Tamaño mediano</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">%</span>
                                    </div>
                                    <input type="number" class="form-control" id="tamanoMediano">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="" class="col-form-label">Tamaño grande</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">%</span>
                                    </div>
                                    <input type="number" class="form-control" id="tamanoGrande">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="chbCiruela" value="option1">
                        <label class="form-check-label" for="exampleRadios1">
                            Cantidad
                        </label>
                    </div>
                    <div id="contenedorCiruela">
                        <div class="row px-3 py-1">
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label for="unidadesKg" class="col-form-label">Unidades por kg (Ciruela)</label>
                                    {{-- LLeno el select con las materias primas de la db --}}
                                    <input type="number" name="unidadesKg" class="form-control" id="unidadesKg">
                                    <span class="invalid-feedback" role="alert" id="error-unidadesKg">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnGuardarTamano">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {
            var cantidadInsumo = 0;
            var cantidadExcedida = '';
            var pesoEnvase = 0;
            var tamanoChico = 0;
            var tamanoMediano = 0;
            var tamanoGrande = 0;

            //ANCHOR Select2 materia prima
            $('#materiasPrima').select2();
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });


            pesoEnvase = $('#envases').find(':selected').data('peso');
            $('#pesoLabel').text('Peso del envase: ' + pesoEnvase + ' kg');

            //SECTION Moldal Tamaños y cantidades
            $("#contenedorCiruela *").prop('disabled', true);

            //EVENTO Modal Hide
            $('#modalDurazno').on('hide.bs.modal', function(event) {
                $('#errorDuraznoCiruela').empty();
                $('#tamanoChico').val('');
                $('#tamanoMediano').val('');
                $('#tamanoGrande').val('');
                $('#unidadesKg').val('');
            })

            //EVENTO Checkboxes
            $("#chbDurazno").change(function() {
                if (this.checked) {
                    $("#contenedorCiruela *").attr('disabled', true);
                    $("#contenedorDurazno *").attr('disabled', false);
                    $('#errorDuraznoCiruela').empty();
                }
            });

            $("#chbCiruela").change(function() {
                if (this.checked) {
                    $("#contenedorDurazno *").attr('disabled', true);
                    $("#contenedorCiruela *").attr('disabled', false);
                    $('#errorDuraznoCiruela').empty();
                }
            });

            //EVENTO Guardar tamaños y cantidades
            $('#btnGuardarTamano').click(function(e) {
                e.preventDefault();
                if ($('#chbDurazno').is(':checked')) {
                    //Si es NaN(el input esta vacio) Asigno un 0
                    tamanoChico = isNaN(parseFloat($('#tamanoChico').val())) ? 0 : parseFloat($(
                            '#tamanoChico')
                        .val());
                    tamanoMediano = isNaN(parseFloat($('#tamanoMediano').val())) ? 0 : parseFloat($(
                        '#tamanoMediano').val());
                    tamanoGrande = isNaN(parseFloat($('#tamanoGrande').val())) ? 0 : parseFloat($(
                        '#tamanoGrande').val());

                    if ((tamanoChico + tamanoMediano + tamanoGrande) != 100) {
                        var htmlError =
                            '<div class="alert alert-danger text-center" role="alert">La suma de todos los tamaños debe ser 100%</div>'
                        $('#errorDuraznoCiruela').html(htmlError);
                    } else {
                        $('#badge').html('Tamaños durazno -> Chico: ' + tamanoChico + '% | Mediano: ' +
                            tamanoMediano +
                            '% | Grande: ' + tamanoGrande +
                            '% <i class="fa fa-times ml-3" aria-hidden="true"></i>');
                        $('#modalDurazno').trigger('click');
                        //Agrego los campos al formulario para enviarlos en el request
                        $('#tamanosUnidades').html(
                            '<input type="hidden" name="tamanoChico" value="' + tamanoChico + '">' +
                            '<input type="hidden" name="tamanoMediano" value="' + tamanoMediano + '">' +
                            '<input type="hidden" name="tamanoGrande" value="' + tamanoGrande + '">' +
                            '<input type="hidden" name="unidadesKg" value="">'
                        );

                        $('#badge').removeClass('d-none');
                    }
                } else {
                    var unidadesKg = parseFloat($('#unidadesKg').val());

                    var error = 0;
                    if (!$('#unidadesKg').val()) {
                        var htmlError =
                            '<div class="alert alert-danger text-center" role="alert">El campo unidades por kg no puede estar vacio</div>'
                        error += 1;
                    } else if (0 > $('#unidadesKg').val()) {
                        var htmlError =
                            '<div class="alert alert-danger text-center" role="alert">El campo unidades por kg no puede ser menor que 0</div>'
                        error += 1;
                    }
                    if (error == 0) {
                        $('#tamanosUnidades').html(
                            '<input type="hidden" name="tamanoChico" value="">' +
                            '<input type="hidden" name="tamanoMediano" value="">' +
                            '<input type="hidden" name="tamanoGrande" value="">' +
                            '<input type="hidden" name="unidadesKg" value="' + unidadesKg + '">'
                        );
                        $('#modalDurazno').trigger('click');

                        $('#badge').removeClass('d-none');
                        $('#badge').html(
                            'Unidades por kg: ' +
                            unidadesKg + ' <i class="fa fa-times ml-3" aria-hidden="true"></i>'
                        );
                    } else {
                        $('#errorDuraznoCiruela').html(htmlError);
                    }
                }
            });

            $('#badge').on('click', 'i', function(e) {
                $('#tamanosUnidades').empty();
                $('#tamanoDurazno').html('Sin tamaño ni cantidad especificada');
                $('#badge').addClass('d-none');
            });
            //!SECTION

            //SECTION Agregar Envase
            //EVENTO Agregar Envase
            $('#btnAgregarEnvase').click(function(e) {
                e.preventDefault();
                if (!$('#cantidadEnvase').val()) {
                    invalidFeedback('#cantidadEnvase', '#error-cantidadenvase',
                        'El campo cantidad no puede estar vacio')
                    return;
                }

                if ($('#cantidadEnvase').val() <= 0) {
                    invalidFeedback('#cantidadEnvase', '#error-cantidadenvase',
                        'El campo cantidad no puede ser igual o menor que 0')
                    return;
                }

                removeFeedback('#cantidadEnvase', '#error-cantidadenvase')

                // Envase
                // Cantidad
                // KG
                //Eliminar
                let nombreEnvase = $('#envases option:selected').text();
                let idEnvase = $('#envases').val();
                let cantidad = $('#cantidadEnvase').val();
                let pesoEnvase = $('#envases option:selected').data('peso');
                let pesoTotal = parseInt(cantidad) * parseFloat(pesoEnvase);

                $('#tablaEnvases').append(
                    '<tr>' +
                    '<td>' + nombreEnvase + '<input type="hidden" name="id_envase[]" value="' +
                    idEnvase + '"></td>' +
                    '<td>' + cantidad + '<input type="hidden" name="cantidad_envase[]" value="' +
                    cantidad + '"></td>' +
                    '<td id="peso">' + pesoTotal + '</td>' +
                    '<td class="text-right mr-2"><span class="badge badge-danger eliminarEnvase">Eliminar</span></td>' +
                    '</tr>'
                );
                calc_envases();
                calc_kgNeto();
                $('#cantidadEnvase').val('');
                $('#cantidadEnvase').focus();
            });
            //!SECTION


            //EVENTO Change Select Envases
            $('#envases').change(function(e) {
                e.preventDefault();
                pesoEnvase = $(this).find(':selected').data('peso');
                $('#pesoLabel').text('Peso del envase: ' + pesoEnvase + ' kg');
            });

            //SECTION KeyUp

            //EVENTO KeyUP Kg Neto
            $('#kgBruto').keyup(function(e) {
                calc_kgNeto();
            });

            //!SECTION

            $(document).on('click', '.eliminarEnvase', function(e) {
                e.preventDefault();
                $(this).closest("tr").remove();
                calc_envases();
                calc_kgNeto();
            });

            //EVENTO Checkbox agregar detalle
            $('#chbObservaciones').click(function(e) {
                if ($(this).is(":checked")) {
                    $('#observaciones').removeClass('d-none');
                } else {
                    $('#observaciones').addClass('d-none');
                }

            });

            //EVENTO Finalizar ingreso
            $('#finalizarIngreso').click(function(e) {
                e.preventDefault();
                let error = 0;
                if ($('#tablaEnvases tr').length == 0) {
                    invalidFeedback('#cantidadEnvase', '#error-cantidadenvase',
                        'Debe agregar al menos un envase con el ingreso. Presione el boton "+"');
                    error += 1;
                } else {
                    removeFeedback('#cantidadEnvase', '#error-cantidadenvase')
                }

                if (!$('#kgBruto').val()) {
                    invalidFeedback('#kgBruto', '#error-kgbruto',
                        'Debe especificar una cantidad');
                    error += 1;
                } else if (0 > $('#kgBruto').val()) {
                    invalidFeedback('#kgBruto', '#error-kgbruto',
                        'Kg bruto no puede ser menor que 0');
                    error += 1;
                } else if (parseFloat($('#kgTara').val()) > parseFloat($('#kgBruto').val())) {
                    invalidFeedback('#kgBruto', '#error-kgbruto',
                        'Kg bruto no puede ser menor kg tara');
                    error += 1;
                } else {
                    removeFeedback('#kgBruto', '#error-kgbruto')
                }
                if (error == 0) {
                    $('#fechaEntrega').val($('#fechaElegida').val());
                    var form = $(this).closest("form");
                    var name = $(this).data("name");
                    //Muestro un mensaje de confirmacion 
                    Swal.fire({
                            title: `¿Desea registrar este ingreso?`,
                            text: "",
                            icon: "question",
                            showCancelButton: true,
                            confirmButtonText: 'Si',
                            confirmButtonColor: '#29643e',
                            cancelButtonText: 'No',
                        })
                        .then((response) => {
                            if (response.isConfirmed) {
                                $.ajax({
                                    url: "{{ route('ingresoMP.store') }}",
                                    method: 'POST',
                                    data: $('#formEntrega').serialize() +
                                        "&_token={{ csrf_token() }}",
                                    dataType: 'json',
                                    success: function(data) {
                                        if (data.error) {
                                            console.log(data.error)
                                            Swal.fire({
                                                icon: 'warning',
                                                title: 'Oops...',
                                                text: data.error,
                                            })
                                        }
                                        if (data.success) {
                                            Swal.fire({
                                                toast: true,
                                                icon: 'success',
                                                title: data.success +
                                                    '. Espere unos instantes',
                                                position: 'top-right',
                                                showConfirmButton: false,
                                                timer: 2000,
                                            }).then(function() {
                                                window.location.href =
                                                    "{{ route('ingresoMP.index') }}";
                                            });
                                        }
                                    }
                                })
                            }
                        });
                }
            });

            //NOTE FUnction sumar peso envases 
            function calc_envases() {
                var total = 0;
                $("tr #peso").each(function(index, value) {
                    currentRow = parseFloat($(this).text());
                    total += currentRow
                });
                $('#kgTara').val(total);
            }

            //NOTE Function calcular peso neto 
            function calc_kgNeto() {
                var kgNeto = (parseFloat($('#kgBruto').val()) - parseFloat($('#kgTara').val()));
                $('#kgNeto').val(kgNeto);
            }

            //==================== FUNCION ERROR DE VALIDACION ============================
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }

            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });
        });
    </script>
@stop
