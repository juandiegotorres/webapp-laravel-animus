<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lista de precios</title>
    <style>
        @page {
            margin: 60px 40px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 14px;
        }

        table {
            width: 100%;
        }

        header {
            position: fixed;
            top: -40px;
            left: 0px;
            right: 0px;
            height: 105px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        .img {
            width: 100%;
            height: 100%;
        }

        .tablaDetalle {
            width: 100%;
            border: 1px solid #000;
            border-spacing: -1px;
        }

        thead {
            border-bottom: 1px solid #000;
        }

        tbody {
            text-align: left;
        }

        tr {
            padding: 0;
        }

        h1 {
            margin: 0 !important;
        }

        h4 {
            margin: 0;
            margin-bottom: 20px;
        }

        .tablaDetalle td {
            border: 1px solid black;
            padding: 4px;
        }

        .tablaDetalle th {
            border: 1px solid black;
            font-size: 11px;
            padding: 8px;
        }

        footer {
            position: fixed;
            bottom: -40px;
            left: 0px;
            right: 0px;
            height: 60px;

            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

    </style>
</head>

<body>
    <header>
        <img src="img/cabecera_lista_precio_1.jpg" class="img" alt="">
    </header>
    <h4 style="margin-top:80px;">VIGENTE DEL: {{ date('d/m/Y', strtotime($detalleLista->first()->fechaVigencia)) }}
    </h4>

    <table class="tablaDetalle">
        <thead>
            <tr style="background: #DFDFDF; border: none !important;">
                <th colspan="4" style="font-size: 15px;">LISTA DE PRECIOS</th>
            </tr>
        </thead>
        <thead>
            <tr>
                <th>PRODUCTO | DETALLE</th>
                <th>CANTIDAD</th>
                <th>PRECIO X KG</th>
                <th>PRECIO X CAJA</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($detalleLista as $detalle)
                <tr style="font-size: 11px;">
                    <td>{{ $detalle->nombreProducto }}</td>
                    <td>
                        @php
                            switch ($detalle->tipo) {
                                case '0':
                                    echo 'Caja, ' . $detalle->kg . ' kg(s)';
                                    break;
                                case '1':
                                    echo 'Bolsa, ' . $detalle->kg . ' kg(s)';
                                    break;
                                default:
                                    break;
                            }
                        @endphp
                    </td>
                    <td>$ {{ number_format($detalle->precioKg, 2, ',', '.') }}</td>
                    <td>$ {{ number_format($detalle->precioCaja, 2, ',', '.') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <footer>
        <h5 style="font-weight: 400; margin-bottom: 0px;"><b>Generado el dia:</b>
            {{ date('d/m/y', strtotime(now())) }} | <b>Impresión de
                Original y Duplicado</b>
        </h5>
        <p style="margin-top: 0px; font-size: 10px;">Los precios detallados son más flete y más I.V.A. - Precios
            mayristas
            sujetos a
            disponibilidad de stock. -
            Condición de entrega sujeto a forma de pago convenida.</p>
    </footer>
</body>


</html>
