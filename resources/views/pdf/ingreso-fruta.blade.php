<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ingreso de fruta</title>
    <style>
        @page {
            margin: 60px -20px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 10px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 80px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        main {
            position: absolute;
            margin: 15px 40px 0px 40px;
        }

        .img {
            width: 100%;
            height: 100%;
        }

        .header {
            width: 100%;
            background-color: #EDEDED;
            border: 1px solid black;
            font-weight: bold;
        }

        .header:first-child {
            color: #000;
            text-align: center;
            font-size: 12px;
            padding: 5px 3px;
        }

        table {
            width: 100%;
            /* border: 1px solid black; */
            padding: 0px;
            /* border-collapse: collapse; */
            border-spacing: -1px;
        }

        td {
            border: 1px solid black;
            padding: 6px 0px 6px 9px;
        }

        .subtabla {
            width: 100%;
            border: none;
            margin: 0;
            padding: 0;
        }

        .xd {
            margin-top: 20px;
        }

        .invisible2 {
            border: none;
            padding: 0;
        }

        .footer {
            position: fixed;
            bottom: -20px;
            left: 45px;
            right: 40px;
            height: 60px;

            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

    </style>
</head>

<body>
    <header>
        <img src="img/cabecera_ingreso_pdf.jpg" class="img" alt="">
    </header>
    <main>
        <table>
            <tr>
                <td style="border: none; padding: 0px; padding-right: 10px;">
                    <table>
                        <tr>
                            <td colspan="5" style="border:none; padding: 0px; padding-bottom: 10px; padding-left: 5px;">
                                <table class="subtabla">
                                    <tr>
                                        <td style="padding-bottom: 7px;" class="invisible2"><b>VALE N°: </b>
                                            {{ $ingreso->id }}</td>
                                        <td style="padding-bottom: 7px;" class="invisible2"></td>
                                        <td style="padding-bottom: 7px;text-align: right; padding-right: 6px;"
                                            class="invisible2">
                                            <b>FECHA:</b>
                                            {{ date('d/m/y', strtotime($ingreso->fecha)) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="invisible2"><b>Nombre: </b>{{ $proveedor->nombreCompleto }}</td>
                                        <td class="invisible2"><b>Domicilio: </b> {{ $proveedor->domicilio }}</td>
                                        <td class="invisible2"><b>Teléfono</b>{{ $proveedor->telefono }}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="header" colspan="5">COMPROBANTE INGRESO FRUTA</td>
                        </tr>
                        <tr>
                            <td style="padding: 3px; width: 10%;">
                                ENVASE
                            </td>
                            <td style="padding: 3px; width: 10%;">
                                CANTIDAD
                            </td>
                            <td style="padding: 3px; width:35%">
                                DETALLE PRODUCTO
                            </td>
                            <td style="width: 45%;">
                                KG
                            </td>

                        </tr>
                        <tr>
                            <td valign="top" colspan="2" style="padding: 0px;">
                                <table class="subtabla">
                                    @foreach ($envases as $envase)
                                        <tr style="background: #FFF;">
                                            <td style="padding: 4px; width: 50%;">{{ $envase->nombre }}</td>
                                            <td style="padding: 4px; width: 50%;">{{ $envase->cantidadEnvase }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>

                            <td valign="top" style="background: #EDEDED; padding: 0px;">
                                <table class="subtabla">
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">
                                            {{ $ingreso->materiaPrima->fruta . ', ' . $ingreso->materiaPrima->variedad }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="padding: 0px;">
                                <table class="subtabla">
                                    <tr>
                                        <td style="width: 37%;">{{ $ingreso->kgBruto }}</td>
                                        <td style="width: 37%;">KG BRUTO</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 37%;">{{ $ingreso->kgTara }}</td>
                                        <td style="width: 37%;">KG TARA</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 37%;">{{ $ingreso->kgNeto }}</td>
                                        <td style="width: 37%;">KG NETO</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="background: #EDEDED;">
                            <td style="padding-top: 5px;"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style="background: #EDEDED;">
                            <td></td>
                            <td></td>
                            <td style="padding: 0px;">
                                <table class="subtabla">
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">
                                            {{ $ingreso->tamanoChico == null ? '-' : $ingreso->tamanoChico }}
                                        </td>
                                    </tr>
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">
                                            {{ $ingreso->tamanoMediano == null ? '-' : $ingreso->tamanoMediano }}
                                        </td>
                                    </tr>
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">
                                            {{ $ingreso->tamanoGrande == null ? '-' : $ingreso->tamanoGrande }}
                                        </td>
                                    </tr>
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">
                                            {{ $ingreso->unidadesKg == null ? '-' : $ingreso->unidadesKg }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="padding: 0px;">
                                <table class="subtabla">
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">% CHICO</td>
                                    </tr>
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">% MEDIANO</td>
                                    </tr>
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">% GRANDE</td>
                                    </tr>
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">UNIDADES X KG.</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <table class="xd">
                            <tr>
                                <th class="header" colspan="4">INAPTO</th>
                            </tr>
                            <tr>
                                <td><b>PRODUCTO</b></td>
                                <td><b>%</b></td>
                                <td><b>FECHA</b></td>
                                <td><b>DETALLE</b></td>
                            </tr>
                            @if ($historialDescarte->count() > 0)
                                <tr>
                                    <td>{{ $historialDescarte->first()->fruta . ', ' . $historialDescarte->first()->variedad }}
                                    </td>
                                    <td>{{ $historialDescarte->sum('porcentaje') }}</td>
                                    <td>{{ date('d-m-Y', strtotime($historialDescarte->first()->fechaMovimiento)) }}
                                    </td>
                                    <td>{{ $historialDescarte->first()->observaciones }}
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="4" style="text-align: center;">SIN FRUTA INAPTA</td>
                                </tr>
                            @endif
                        </table>
                    </div>

                </td>
                <td style="border: none; padding: 0px; padding-left: 10px;">
                    <table>
                        <tr>
                            <td colspan="5" style="border:none; padding: 0px; padding-bottom: 10px; padding-left: 5px;">
                                <table class="subtabla">
                                    <tr>
                                        <td style="padding-bottom: 7px;" class="invisible2"><b>VALE N°: </b>
                                            {{ $ingreso->id }}</td>
                                        <td style="padding-bottom: 7px;" class="invisible2"></td>
                                        <td style="padding-bottom: 7px;text-align: right; padding-right: 6px;"
                                            class="invisible2">
                                            <b>FECHA:</b>
                                            {{ date('d/m/y', strtotime($ingreso->fecha)) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="invisible2"><b>Nombre: </b>{{ $proveedor->nombreCompleto }}
                                        </td>
                                        <td class="invisible2"><b>Domicilio: </b> {{ $proveedor->domicilio }}
                                        </td>
                                        <td class="invisible2"><b>Teléfono</b>{{ $proveedor->telefono }}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="header" colspan="5">COMPROBANTE INGRESO FRUTA</td>
                        </tr>
                        <tr>
                            <td style="padding: 3px; width: 10%;">
                                ENVASE
                            </td>
                            <td style="padding: 3px; width: 10%;">
                                CANTIDAD
                            </td>
                            <td style="padding: 3px; width:35%">
                                DETALLE PRODUCTO
                            </td>
                            <td style="width: 45%;">
                                KG
                            </td>

                        </tr>
                        <tr>
                            <td valign="top" colspan="2" style="padding: 0px;">
                                <table class="subtabla">
                                    @foreach ($envases as $envase)
                                        <tr style="background: #FFF;">
                                            <td style="padding: 4px; width: 50%;">{{ $envase->nombre }}</td>
                                            <td style="padding: 4px; width: 50%;">{{ $envase->cantidadEnvase }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>

                            <td valign="top" style="background: #EDEDED; padding: 0px;">
                                <table class="subtabla">
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">
                                            {{ $ingreso->materiaPrima->fruta . ', ' . $ingreso->materiaPrima->variedad }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="padding: 0px;">
                                <table class="subtabla">
                                    <tr>
                                        <td style="width: 37%;">{{ $ingreso->kgBruto }}</td>
                                        <td style="width: 37%;">KG BRUTO</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 37%;">{{ $ingreso->kgTara }}</td>
                                        <td style="width: 37%;">KG TARA</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 37%;">{{ $ingreso->kgNeto }}</td>
                                        <td style="width: 37%;">KG NETO</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="background: #EDEDED;">
                            <td style="padding-top: 5px;"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style="background: #EDEDED;">
                            <td></td>
                            <td></td>
                            <td style="padding: 0px;">
                                <table class="subtabla">
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">
                                            {{ $ingreso->tamanoChico == null ? '-' : $ingreso->tamanoChico }}
                                        </td>
                                    </tr>
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">
                                            {{ $ingreso->tamanoMediano == null ? '-' : $ingreso->tamanoMediano }}
                                        </td>
                                    </tr>
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">
                                            {{ $ingreso->tamanoGrande == null ? '-' : $ingreso->tamanoGrande }}
                                        </td>
                                    </tr>
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">
                                            {{ $ingreso->unidadesKg == null ? '-' : $ingreso->unidadesKg }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="padding: 0px;">
                                <table class="subtabla">
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">% CHICO</td>
                                    </tr>
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">% MEDIANO</td>
                                    </tr>
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">% GRANDE</td>
                                    </tr>
                                    <tr style="background: #FFF;">
                                        <td style="padding: 4px;">UNIDADES X KG.</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <table class="xd">
                            <tr>
                                <th class="header" colspan="4">INAPTO</th>
                            </tr>
                            <tr>
                                <td><b>PRODUCTO</b></td>
                                <td><b>%</b></td>
                                <td><b>FECHA</b></td>
                                <td><b>DETALLE</b></td>
                            </tr>
                            @if ($historialDescarte->count() > 0)
                                <tr>
                                    <td>{{ $historialDescarte->first()->fruta . ', ' . $historialDescarte->first()->variedad }}
                                    </td>
                                    <td>{{ $historialDescarte->sum('porcentaje') }}</td>
                                    <td>{{ date('d-m-Y', strtotime($historialDescarte->first()->fechaMovimiento)) }}
                                    </td>
                                    <td>{{ $historialDescarte->first()->observaciones }}
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="4" style="text-align: center;">SIN FRUTA INAPTA</td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </td>
            </tr>
        </table>

        <table class="footer">
            <tr>
                <td style="border: none; padding: 0px; padding-right: 10px;">
                    <table>
                        <tr>
                            <td class="invisible2" colspan="2">
                                <b style="font-size: 12px;">OBSERVACIONES:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2" colspan="2">
                                {{ $ingreso->observaciones }}
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2" style="padding-top: 20px;">
                                <b style="font-size: 12px;"></b>
                            </td>
                            <td class="invisible2">

                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2">
                            </td>
                            <td class="invisible2" style="text-align: center;">
                                -----------------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2">
                            </td>
                            <td class="invisible2" style="text-align: center;">
                                FIRMA PROVEEDOR
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="border: none; padding: 0px; padding-left: 10px;">
                    <table>
                        <tr>
                            <td class="invisible2" colspan="2">
                                <b style="font-size: 12px;">OBSERVACIONES:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2" colspan="2">
                                {{ $ingreso->observaciones }}
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2" style="padding-top: 20px;">
                                <b style="font-size: 12px;"></b>
                            </td>
                            <td class="invisible2">

                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2">
                            </td>
                            <td class="invisible2" style="text-align: center;">
                                -----------------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2">
                            </td>
                            <td class="invisible2" style="text-align: center;">
                                FIRMA PROVEEDOR
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>


    </main>

</body>


</html>
