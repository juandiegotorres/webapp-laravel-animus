<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Entrega indumentaria</title>
    <style>
        @page {
            margin: 60px -10px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 12px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 80px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        main {
            position: absolute;
            margin: -10px 40px 0px 40px;
        }

        .img {
            width: 100%;
            height: 100%;
        }

        .header {
            width: 100%;
            background-color: #29643e;
            border: 1px solid black;
        }

        .header:first-child {
            color: white;
            text-align: center;
            font-size: 16px;
            padding: 10px 3px;
        }


        table {
            width: 100%;
            /* border: 1px solid black; */
            padding: 0px;
            /* border-collapse: collapse; */
            border-spacing: -1px;
        }

        td {
            border: 1px solid black;
            padding: 6px 0px 6px 9px;
        }

        .subtabla {
            width: 100%;
            border: none;
            margin: 0;
        }

        .invisible2 {
            margin-top: -20px;
            border: none;
            padding: 0;
            font-size: 12px;
        }

        footer {
            position: fixed;
            bottom: -10px;
            left: 0px;
            right: 0px;
            height: 60px;

            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

    </style>
</head>

<body>
    <header>
        <h5 style="float: right; margin-right: 45px;">Resolución 299/11. Anexo I</h5>
    </header>
    <main>
        <table>
            <tr>
                <th class="header" colspan="3">ENTREGA DE ROPA DE TRABAJO Y ELEMENTOS DE PROTECCIÓN PERSONAL</th>
            </tr>
            <tr>
                <td style="width: 75%;"><b>Razón social: </b>Achetoni Oscar y Digiambatista Fanny S.H.
                </td>
                <td style="width: 25%; margin-right:10px;"><b>CUIT: </b>30-71102728-5</td>
            </tr>
            <tr>
                <td colspan="3" style="padding: 0px;">
                    <table class="subtabla">
                        <tr>
                            <td style="border:none; border-right: 1px solid black; width: 37%;"><b>Dirección:
                                </b>Moreno y Pública S/N</td>
                            <td style="border:none; border-right: 1px solid black; width: 37%;"><b>Localidad:
                                </b>Moreno y Pública S/N</td>
                            <td style="border:none; width: 26%;"><b>Provincia:
                                </b>Mendoza</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 50%;"><b>Nombre y Apellido Trabajador: </b>{{ $empleado->nombreCompleto }}
                </td>
                <td style="width: 50%;"><b>DNI: </b>{{ $empleado->dni }}</td>
            </tr>
            <tr>
                <td style="width: 50%;"><b>Puesto del Trabajador:</b> {{ $empleado->cargo->nombre }}</td>
                <td style="width: 50%;"><b>Fecha de emisión: </b> {{ date('d-m-Y', strtotime($entrega->fecha)) }}</td>
            </tr>
            <tr>
                <table style="font-size: 11px;">
                    <tr>
                        <td><b>PRODUCTO</b></td>
                        <td><b>TIPO/MODELO</b></td>
                        <td><b>MARCA</b></td>
                        <td><b>CERTIFICACIÓN (SI/NO)</b></td>
                        <td><b>CANTIDAD</b></td>
                    </tr>
                    @foreach ($detalleEntrega as $detalle)
                        <tr>
                            <td>{{ $detalle->nombre }}</td>
                            <td>{{ $detalle->tipo_modelo }}</td>
                            <td>{{ $detalle->modelo }}</td>
                            <td>{{ $detalle->esCertificada }}</td>
                            <td>{{ $detalle->cantidad }}</td>
                        </tr>
                    @endforeach
                </table>
            </tr>
        </table>
    </main>
    <footer>
        <h4 style="margin-left: 45px; margin-bottom: 0px;">INFORMACIÓN ADICIONAL:</h4>
        <p style="margin-left: 45px;  margin-top: 0px;">{{ $entrega->detalle }}</p>
    </footer>
</body>

</html>
