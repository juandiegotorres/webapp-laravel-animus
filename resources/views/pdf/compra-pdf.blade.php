<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Compra</title>
    <style>
        @page {
            margin: 60px 15px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 12px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 80px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        main {
            position: absolute;
            margin: 30px 40px 0px 40px;
        }

        .img {
            width: 100%;
            height: 100%;
        }

        .header {
            width: 100%;
            background-color: #29643e;
            border: 1px solid black;
        }

        .header:first-child {
            color: #fff;
            text-align: center;
            font-size: 16px;
            padding: 10px 3px;
        }

        table {
            width: 100%;
            /* border: 1px solid black; */
            padding: 0px;
            /* border-collapse: collapse; */
            border-spacing: -1px;
        }

        td {
            border: 1px solid black;
            padding: 6px 0px 6px 9px;
        }

        .subtabla {
            width: 100%;
            border: none;
            margin: 0;
        }

        .p {
            padding: 13px;
        }

        .invisible {
            border: none;
            padding: 6px 0px 6px 9px;
            font-size: 12px;
        }

        .invisible2 {
            margin-top: -20px;
            border: none;
            padding: 0;
            font-size: 12px;
        }

        footer {
            position: fixed;
            bottom: -10px;
            left: 0px;
            right: 0px;
            height: 60px;

            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

    </style>
</head>

<body>
    <header>
        <img src="img/cabecera_pdf.jpg" class="img" alt="">
    </header>
    <main style="width: 90% !important;">
        <table style="width: 100% !important;">
            <tr>
                <th class="header" colspan="3">RECIBO COMPRA</th>
            </tr>
            <tr>
                <td colspan="2" style="width: 75%;"><b>Razón social: </b>Achetoni Oscar y Digiambatista Fanny S.H.
                </td>
                <td style="width: 25%; margin-right:10px;"><b>CUIT: </b>30-71102728-5</td>
            </tr>
            <tr>
                <td colspan="3" style="padding: 0px;">
                    <table class="subtabla">
                        <tr>
                            <td style="border:none; border-right: 1px solid black; width: 37%;"><b>Dirección:
                                </b>Moreno y Pública S/N</td>
                            <td style="border:none; border-right: 1px solid black; width: 37%;"><b>Localidad:
                                </b>Moreno y Pública S/N</td>
                            <td style="border:none; width: 26%;"><b>Provincia:
                                </b>Mendoza</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding: 0px;">
                    <table class="subtabla">
                        <tr>
                            <td style="border:none; border-right: 1px solid black; width: 37%;"><b>Proveedor:
                                </b>{{ $proveedor->nombreCompleto }}
                            </td>
                            <td style="border:none; border-right: 1px solid black; width: 25%;"><b>CUIT:
                                </b>{{ $proveedor->cuit }}
                            </td>
                            <td style="border:none; width: 38%;"><b>Localidad prov.:
                                </b>{{ $proveedor->localidad->localidad }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 50%;"><b>Teléfono proveedor:</b> {{ $proveedor->telefono }}</td>
                <td style="width: 50%;"><b>Fecha de emisión remito: </b>
                    {{ date('d-m-Y', strtotime($compra->first()->fecha)) }}</td>
            </tr>
            <tr>
                <td colspan="3" style="padding: 0px;">
                    <table>
                        <tr>
                            <td><b>CANTIDAD</b></td>
                            <td><b>DETALLE|PRODUCTO</b></td>
                            <td><b>PRECIO UNITARIO</b></td>
                            <td><b>IMPORTE</b></td>
                        </tr>
                        @foreach ($detalleCompra as $detalle)
                            <tr>
                                <td>{{ $detalle->cantidad }}</td>
                                <td>
                                    @switch($compra->tipoCompra)
                                        @case('MP')
                                            {{ $detalle->materiaPrima->fruta . ', ' . $detalle->materiaPrima->variedad }}
                                        @break

                                        @case('IS')
                                            {{ $detalle->insumo->nombre }}
                                        @break

                                        @case('SV')
                                            {{ $detalle->servicio->nombre }}
                                        @break

                                        @case('ID')
                                            {{ $detalle->indumentaria->nombre }}
                                        @break

                                        @default
                                    @endswitch
                                </td>
                                <td>$ {{ number_format($detalle->precioUnitario, 2, ',', '.') }}</td>
                                <td>$ {{ number_format($detalle->totalParcial, 2, ',', '.') }}</td>
                            </tr>
                        @endforeach
                        @for ($i = 0; $i < 2; $i++)
                            <tr>
                                <td class="p"></td>
                                <td class="p"></td>
                                <td class="p"></td>
                                <td class="p"></td>
                            </tr>
                        @endfor
                        <tr>
                            <td colspan="4" style="padding: 0px; text-align: right; border:none;">
                                <table style="margin-top: 1px;">
                                    <tr>
                                        <td class="invisible" style="width: 20%"></td>
                                        <td class="invisible" style="width: 20%"></td>
                                        <td class="invisible" style="width: 20%"></td>
                                        <td
                                            style="width: 30%; text-align: right; padding-right: 20px; font-size: 14px;  border-right:none">
                                            <b>IVA:
                                            </b> $
                                            {{ number_format($compra->first()->iva, 2, ',', '.') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="invisible" style="width: 20%"></td>
                                        <td class="invisible" style="width: 20%"></td>
                                        <td class="invisible" style="width: 20%"></td>
                                        <td
                                            style="width: 30%; text-align: right; padding-right: 20px; font-size: 14px; border-bottom:none; border-right:none">
                                            <b>TOTAL:
                                            </b> $
                                            {{ number_format($compra->first()->total, 2, ',', '.') }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </main>
</body>

</html>
