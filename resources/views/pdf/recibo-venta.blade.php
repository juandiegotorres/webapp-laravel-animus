<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recibo venta</title>
    <style>
        @page {
            margin: 60px 15px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 14px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 160px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        main {
            position: absolute;
            margin: 30px 40px 0px 40px;
        }

        .img {
            width: 100%;
            height: 100%;
        }

        .header {
            width: 100%;
            background-color: #29643e;
            border: 1px solid black;
        }

        .header:first-child {
            color: white;
            text-align: center;
            font-size: 16px;
            padding: 10px 3px;
        }

        .tabla1 {
            margin-top: 80px;
            width: 100%;
            /* border: 1px solid black; */
            padding: 0px;
            /* border-collapse: collapse; */
            border-spacing: -1px;
        }

        .tabla2 {
            marign-top: 60px;
            width: 100%;
            /* border: 1px solid black; */
            padding: 0px;
            /* border-collapse: collapse; */
            border-spacing: -1px;
        }

        .invisible {
            border: none;
            padding: 6px 0px 6px 9px;
            font-size: 12px;
        }

        .invisible2 {
            margin-top: -20px;
            border: none;
            padding: 0;
            font-size: 10px;
        }

        .visible {
            border: 1px solid black;
            padding: 6px 0px 6px 9px;
        }


        .subtabla {
            width: 100%;
            border: none;
            margin: 0;
        }

        .p {
            padding: 13px;
        }

        .tr {
            text-align: right !important;
        }

        .total {
            padding: 6px 0px 6px 9px;
            border-left: 1px solid black;
            border-bottom: 1px solid black;
            border-right: 1px solid black;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 60px;

            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

    </style>
</head>

<body>
    <header>
        <img src="img/cabecera_venta.jpg" class="img" alt="">
    </header>
    <main>
        <div>
            <table class="tabla1">
                <tr>
                    <td class="invisible"><b>Señor/a | Empresa: </b> {{ $cliente->nombreCompleto }}</td>
                    <td class="invisible"><b>Localidad: </b> {{ $cliente->localidad->localidad }}</td>
                    <td class="invisible"><b>Domicilio: </b> {{ $cliente->direccion }}</td>
                </tr>
                <tr>
                    <td class="invisible"><b>Cuit: </b> {{ $cliente->cuit }}</td>
                    <td class="invisible"><b>Teléfono: </b> {{ $cliente->telefono }}</td>
                    <td class="invisible"></td>
                </tr>
            </table>
        </div>
        <div style="margin-top:20px;">
            <table class="tabla2">
                <tr>
                    <th class="visible" style="text-align: center;">Cantidad</th>
                    <th class="visible">Producto|Detalle</th>
                    <th class="visible" style="text-align: center;">Importe</th>
                </tr>
                @foreach ($detalleVenta as $detalle)
                    <tr>
                        <td class="visible">{{ $detalle->cantidad }} </td>
                        <td class="visible">@php
                            $nombreCompleto = $detalle->variante->producto->nombreCompleto;
                            switch ($detalle->variante->tipo) {
                                case 0:
                                    $nombreCompleto .= ' - Caja ,' . $detalle->variante->kg . ' kg(s)';
                                    break;
                            
                                default:
                                    $nombreCompleto .= ' - Bolsa ,' . $detalle->variante->kg . ' kg(s)';
                                    break;
                            }
                            echo $nombreCompleto;
                        @endphp</td>
                        <td class="visible">$ {{ number_format($detalle->subtotal, 2, ',', '.') }}</td>
                    </tr>
                @endforeach

                <tr>
                    <td></td>
                    <td></td>
                    <td class="total"><strong>TOTAL:</strong> $
                        {{ number_format($venta->total, 2, ',', '.') }} </td>
                </tr>
            </table>
        </div>
    </main>
    <footer>
        <table>
            <tr>
                <td class="invisible2">
                    <b>Generado el dia: </b>{{ date('d-m-Y', strtotime(now())) }}
                    <b>|
                        Impresión de Original y Duplicado</b>
                </td>

            </tr>
            <tr>
                <td class="invisible2"><b>DEFENSA AL CONSUMIDOR MENDOZA 0800-222-6678 </b></td>
            </tr>
            <tr>
                <td class="invisible2" style="float: right;"> www.hfrut.com.ar | venta@hfrut.com.ar
                </td>
            </tr>
        </table>
    </footer>
</body>

</html>
