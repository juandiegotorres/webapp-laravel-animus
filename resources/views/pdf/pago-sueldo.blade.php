<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pago de sueldo</title>
    <style>
        @page {
            margin: -20px -20px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 10px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 10px;
            right: 0px;
            height: 75px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        main {
            margin: 30px 40px 0px 40px;
        }

        .img {
            width: 100%;
            height: 100%;
        }

        .header {
            width: 100%;
            background-color: #29643e;
            border: 1px solid black;
        }

        .titulo {
            color: #fff;
            text-align: center;
            font-size: 12px;
            padding: 5px 3px;
        }

        table {
            width: 100%;
            /* border: 1px solid black; */
            padding: 0px;
            /* border-collapse: collapse; */
            border-spacing: -1px;
        }

        td {
            border: 1px solid black;
            padding: 6px 0px 6px 9px;
        }

        .subtabla {
            width: 100%;
            border: none;
            margin: 0;
            padding: 0;
        }

        .xd {
            margin-top: 20px;
        }

        .invisible2 {
            border: none;
            padding: 0;
        }

        footer {
            position: fixed;
            bottom: -10px;
            left: 0px;
            right: 0px;
            height: 60px;

            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

        .float-left {
            float: left !important;
            margin-right: 10px;
        }

        .float-right {
            float: right !important;
            margin-left: 10px;
        }

        .footer {
            position: fixed;
            bottom: 60px;
            left: 0px;
            right: 0px;
            height: 30px;
        }

    </style>
</head>

<body>

    <main>
        <table>
            <tr>
                <td style="border: none; padding: 0px; padding-right: 10px;">
                    <table style="margin-top: 15px;">
                        <tr>
                            <th class="header titulo" colspan="3">DETALLE PAGO</th>
                        </tr>
                        <tr>
                            <th colspan="3" style="padding: 5px; border: 1px solid black;">CONTROL INTERNO</th>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding: 0;">
                                <table class="subtabla">
                                    <tr>
                                        <td style="width: 75%"><b>Nombre y Apellido trabajador: </b>
                                            {{ $empleado->nombreCompleto }}</td>
                                        <td style="width: 25%"><b>DNI: </b> {{ $empleado->dni }}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px; width: 25%; text-align: center;">
                                <b>FECHA PAGO</b>
                            </td>
                            <td style="padding: 3px; width: 55%;">
                                <b>CONCEPTO</b>
                            </td>
                            <td style="padding: 3px; width:20%">
                                <b>IMPORTE</b>
                            </td>
                        </tr>
                        <tr>
                            {{-- @foreach ($collection as $item) --}}
                            <td>{{ date('d/m/y', strtotime($pagoSueldo->fecha)) }}</td>
                            <td>{{ $pagoSueldo->concepto }}</td>
                            <td>$ {{ number_format($pagoSueldo->total, 2, ',', '.') }}</td>
                            {{-- @endforeach --}}
                        </tr>
                        <tr>
                            <table class="subtabla" style="width: 100%">
                                <tr>
                                    <td class="invisible2" style="width: 20%; padding: 14px;"></td>
                                    <td class="invisible2"
                                        style="width: 55%; padding-top: 5px; padding-right: 5px; text-align: right;">
                                        TOTAL</td>
                                    <td class="invisible2"
                                        style="width: 25%; padding-top: 5px; padding-right: 5px; text-align: left; padding-left: 9px;">
                                        $ {{ number_format($pagoSueldo->total, 2, ',', '.') }}</td>
                                </tr>

                            </table>
                        </tr>
                    </table>
                </td>
                <td style="border: none; padding: 0px; padding-left: 10px;">
                    <table style="margin-top: 15px;">
                        <tr>
                            <th class="header titulo" colspan="3">DETALLE PAGO</th>
                        </tr>
                        <tr>
                            <th colspan="3" style="padding: 5px; border: 1px solid black;">CONTROL INTERNO</th>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding: 0;">
                                <table class="subtabla">
                                    <tr>
                                        <td style="width: 75%"><b>Nombre y Apellido trabajador: </b>
                                            {{ $empleado->nombreCompleto }}</td>
                                        <td style="width: 25%"><b>DNI: </b> {{ $empleado->dni }}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px; width: 25%; text-align: center;">
                                <b>FECHA PAGO</b>
                            </td>
                            <td style="padding: 3px; width: 55%;">
                                <b>CONCEPTO</b>
                            </td>
                            <td style="padding: 3px; width:20%">
                                <b>IMPORTE</b>
                            </td>
                        </tr>
                        <tr>
                            {{-- @foreach ($collection as $item) --}}
                            <td>{{ date('d/m/y', strtotime($pagoSueldo->fecha)) }}</td>
                            <td>{{ $pagoSueldo->concepto }}</td>
                            <td>$ {{ number_format($pagoSueldo->total, 2, ',', '.') }}</td>
                            {{-- @endforeach --}}
                        </tr>
                        <tr>
                            <table class="subtabla" style="width: 100%">
                                <tr>
                                    <td class="invisible2" style="width: 20%; padding: 14px;"></td>
                                    <td class="invisible2"
                                        style="width: 55%; padding-top: 5px; padding-right: 5px; text-align: right;">
                                        TOTAL</td>
                                    <td class="invisible2"
                                        style="width: 25%; padding-top: 5px; padding-right: 5px; text-align: left; padding-left: 9px;">
                                        $ {{ number_format($pagoSueldo->total, 2, ',', '.') }}</td>
                                </tr>

                            </table>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table class="footer">
            <tr>
                <td style="border: none; padding: 0px; padding-right: 10px;">
                    <table class="subtabla" style="width: 100%;">
                        <tr>
                            <td class="invisible2" width="50%"></td>
                            <td class="invisible2" style="text-align: center">
                                -----------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2" width="50%"></td>
                            <td class="invisible2" style="text-align: center">FIRMA TRABAJADOR</td>
                        </tr>
                        <tr>
                            <td class="invisible2" colspan="2"
                                style="text-align: center; padding-top: 20px; font-weight: bold; font-size: 14px;">NO
                                VÁLIDO
                                COMO RECIBO DE
                                SUELDO</td>
                        </tr>
                    </table>
                </td>
                <td style="border: none; padding: 0px; padding-left: 10px;">
                    <table class="subtabla" style="width: 100%;">
                        <tr>
                            <td class="invisible2" width="50%"></td>
                            <td class="invisible2" style="text-align: center">
                                -----------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2" width="50%"></td>
                            <td class="invisible2" style="text-align: center">FIRMA TRABAJADOR</td>
                        </tr>
                        <tr>
                            <td class="invisible2" colspan="2"
                                style="text-align: center; padding-top: 20px; font-weight: bold; font-size: 14px;">NO
                                VÁLIDO
                                COMO RECIBO DE
                                SUELDO</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </main>

</body>

</html>
