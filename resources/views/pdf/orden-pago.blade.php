<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Orden de pago</title>
    <style>
        @page {
            margin: 60px -20px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 10px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 10px;
            right: 0px;
            height: 75px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        main {
            position: absolute;
            margin: 30px 40px 0px 40px;
        }

        .img {
            width: 100%;
            height: 100%;
        }

        .header {
            width: 100%;
            background-color: #EDEDED;
            border: 1px solid black;
        }

        .header:first-child {
            color: #000;
            text-align: center;
            font-size: 12px;
            padding: 5px 3px;
        }

        table {
            width: 50%;
            /* border: 1px solid black; */
            padding: 0px;
            /* border-collapse: collapse; */
            border-spacing: -1px;
        }

        td {
            border: 1px solid black;
            padding: 6px 0px 6px 9px;
        }

        .subtabla {
            width: 100%;
            border: none;
            margin: 0;
            padding: 0;
        }

        .xd {
            margin-top: 20px;
        }

        .invisible2 {
            border: none;
            padding: 0;
        }

        footer {
            position: fixed;
            bottom: -10px;
            left: 0px;
            right: 0px;
            height: 60px;

            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

        .float-left {
            float: left !important;
            margin-right: 20px;
        }

        .float-right {
            float: right !important;
        }

        .footer {
            position: fixed;
            bottom: -25px;
            left: 40px;
            right: 0px;
            height: 30px;
        }

    </style>
</head>

<body>
    <header>
        <img src="img/cabecera_liquidacion.jpg" class="img" alt="">
    </header>
    <main>
        <div class="float-left">
            <table class="subtabla" style="width: 50%">
                <tr>
                    <td class="invisible2"><b>ORDEN DE PAGO N°</b> {{ 'nro ORDEN' }}</td>
                    <td class="invisible2" style="text-align: right; padding-right: 10px;"><b>FECHA:<b>
                                {{ '00/00/00' }}</td>
                </tr>
                <tr>
                    <td class="invisible2">
                        <b>Nombre:</b> {{ 'aca nombre' }}
                    </td>
                    <td class="invisible2" style="text-align: right; padding-right: 10px;"><b>Concepto:</b>
                        {{ 'aca concepto' }}</td>
                </tr>
            </table>
            <table style="margin-top: 15px;">
                <tr>
                    <th class="header" colspan="4">ORDEN DE PAGO</th>
                </tr>
                <tr>
                    <td style="padding: 3px; width: 55%;">
                        DETALLE
                    </td>
                    <td style="padding: 3px; width: 25%; text-align: center;">
                        NÚMERO CHEQUE
                    </td>
                    <td style="padding: 3px; width:20%">
                        IMPORTE
                    </td>
                </tr>
                <tr>
                    {{-- @foreach ($collection as $item) --}}
                    <td>{{ 'detalle' }}</td>
                    <td>{{ 'cheque' }}</td>
                    <td>{{ 'importe' }}</td>
                    {{-- @endforeach --}}
                </tr>
                <tr>
                    <table class="subtabla" style="width: 50%">
                        <tr>
                            <td class="invisible2" style="width: 55%"></td>
                            <td style="width: 25%; padding-top: 5px;" class="invisible2">
                                TOTAL</td>
                            <td class="invisible2"
                                style="width: 20%; padding-top: 5px; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                {{ 'aca TOTAL' }}</td>
                        </tr>
                        <tr>
                            <td class="invisible2" style="width: 55%"><b>OBSERVACIONES:</b></td>
                            <td style="width: 25%; padding-top: 5px;" class="invisible2">
                                IVA</td>
                            <td class="invisible2"
                                style="width: 20%; padding-top: 5px; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                {{ 'aca IVA' }}</td>
                        </tr>
                        <tr>
                            <td class="invisible2" style="width: 55%"></td>
                            <td style="width: 25%; padding-top: 5px;" class="invisible2">
                                SALDO</td>
                            <td class="invisible2"
                                style="width: 20%; padding-top: 5px; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                {{ 'aca SALDO' }}</td>
                        </tr>
                    </table>
                </tr>
            </table>
            <div class="footer">
                <table class="subtabla" style="width: 51%;">
                    <tr>
                        <td class="invisible2" width="50%"></td>
                        <td class="invisible2" style="text-align: center">-----------------------------------------
                        </td>
                    </tr>
                    <tr>
                        <td class="invisible2" width="50%"></td>
                        <td class="invisible2" style="text-align: center">FIRMA PROVEEDOR</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="float-right">
            <table class="subtabla" style="width: 50%">
                <tr>
                    <td class="invisible2"><b>ORDEN DE PAGO N°</b> {{ 'nro ORDEN' }}</td>
                    <td class="invisible2" style="text-align: right; padding-right: 10px;"><b>FECHA:<b>
                                {{ '00/00/00' }}</td>
                </tr>
                <tr>
                    <td class="invisible2">
                        <b>Nombre:</b> {{ 'aca nombre' }}
                    </td>
                    <td class="invisible2" style="text-align: right; padding-right: 10px;"><b>Concepto:</b>
                        {{ 'aca concepto' }}</td>
                </tr>
            </table>
            <table style="margin-top: 15px;">
                <tr>
                    <th class="header" colspan="4">ORDEN DE PAGO</th>
                </tr>
                <tr>
                    <td style="padding: 3px; width: 55%;">
                        DETALLE
                    </td>
                    <td style="padding: 3px; width: 25%; text-align: center;">
                        NÚMERO CHEQUE
                    </td>
                    <td style="padding: 3px; width:20%">
                        IMPORTE
                    </td>
                </tr>
                <tr>
                    {{-- @foreach ($collection as $item) --}}
                    <td>{{ 'detalle' }}</td>
                    <td>{{ 'cheque' }}</td>
                    <td>{{ 'importe' }}</td>
                    {{-- @endforeach --}}
                </tr>
                <tr>
                    <table class="subtabla" style="width: 50%">
                        <tr>
                            <td class="invisible2" style="width: 55%"></td>
                            <td style="width: 25%; padding-top: 5px;" class="invisible2">
                                TOTAL</td>
                            <td class="invisible2"
                                style="width: 20%; padding-top: 5px; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                {{ 'aca TOTAL' }}</td>
                        </tr>
                        <tr>
                            <td class="invisible2" style="width: 55%"><b>OBSERVACIONES:</b></td>
                            <td style="width: 25%; padding-top: 5px;" class="invisible2">
                                IVA</td>
                            <td class="invisible2"
                                style="width: 20%; padding-top: 5px; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                {{ 'aca IVA' }}</td>
                        </tr>
                        <tr>
                            <td class="invisible2" style="width: 55%"></td>
                            <td style="width: 25%; padding-top: 5px;" class="invisible2">
                                SALDO</td>
                            <td class="invisible2"
                                style="width: 20%; padding-top: 5px; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                {{ 'aca SALDO' }}</td>
                        </tr>
                    </table>
                </tr>
            </table>
            <div class="footer">
                <table class="subtabla" style="width: 51%;">
                    <tr>
                        <td class="invisible2" width="50%"></td>
                        <td class="invisible2" style="text-align: center">-----------------------------------------
                        </td>
                    </tr>
                    <tr>
                        <td class="invisible2" width="50%"></td>
                        <td class="invisible2" style="text-align: center">FIRMA PROVEEDOR</td>
                    </tr>
                </table>
            </div>
        </div>
    </main>

</body>

</html>
