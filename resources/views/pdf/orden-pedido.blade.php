<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Orden de pedido</title>
    <style>
        @page {
            margin: 60px -20px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 10px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 75px;
            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        main {
            margin: 0px 40px 0px 40px;
        }

        .img {
            width: 100%;
            height: 75%;
        }

        .header {
            width: 100%;
            background-color: #EDEDED;
            border: 1px solid black;
        }

        .header:first-child {
            color: #000;
            text-align: center;
            font-size: 12px;
            padding: 5px 3px;
        }

        table {
            width: 100%;
            /* border: 1px solid black; */
            padding: 0px;
            /* border-collapse: collapse; */
            border-spacing: -1px;
        }

        td {
            border: 1px solid black;
            padding: 6px 0px 6px 9px;
        }

        .subtabla {
            width: 100%;
            border: none;
            margin: 0;
            padding: 0;
        }

        .xd {
            margin-top: 20px;
        }

        .invisible2 {
            border: none;
            padding: 0 !important;
            font-size: 9px;
        }

        .footer {
            position: fixed;
            bottom: -80px;
            left: 40px;
            right: 0px;
            height: 60px;
            font-size: 8px !important;
            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

        .float-left {
            float: left !important;
            margin-right: 20px;
        }

        .float-right {
            float: right !important;
        }

    </style>
</head>

<body>
    <header>
        <img src="img/cabecera_orden_pedido.jpg" class="img" alt="">
    </header>
    <main>
        <table>
            <tr>
                <td style="border: none; padding: 0px; padding-right: 10px;">
                    <table style="">
                        <tr>
                            <td class="invisible2">
                                <b>Señor/a | Empresa: </b> {{ $cliente->nombreCompleto }}
                            </td>

                            <td class="invisible2">
                                <b>ORDEN DE PEDIDO N°: </b> {{ $venta->id }}
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2" style="padding-top:5px;">
                                <b>CUIT:</b> {{ $cliente->cuit }}
                            </td>

                            <td class="invisible2" style="padding-top:5px;">
                                <b>FECHA EMISIÓN: </b> {{ date('d/m/y', strtotime($venta->fecha)) }}
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2" style="padding-top:5px;">
                                <b>Teléfono: </b> {{ $cliente->telefono }}
                            </td>
                            <td class="invisible2" style="padding-top:5px;">
                                <b>Localidad: </b> {{ $cliente->localidad->localidad }}
                            </td>
                        </tr>
                    </table>
                    <table style="margin-top: 10px;">
                        <thead>
                            <tr>
                                <th class="header" colspan="4" style="padding: 5px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding: 2px; width: 10%; font-size: 9px;">
                                    <b>CANTIDAD</b>
                                </td>
                                <td style="padding: 2px; width: 45%; font-size: 9px;">
                                    <b>PRODUCTO | DETALLE </b>
                                </td>
                                <td style="padding: 2px; width:20%; font-size: 9px;">
                                    <b>PRECIO UNITARIO</b>
                                </td>
                                <td style="width: 25%; font-size: 9px;">
                                    <b>IMPORTE</b>
                                </td>
                            </tr>

                            @foreach ($detalleVenta as $detalle)
                                <tr>
                                    <td style="padding: 0px; padding-left: 3px; font-size: 9px ">
                                        {{ $detalle->cantidad }}</td>
                                    <td style="padding: 0px; padding-left: 3px; font-size: 9px ">@php
                                        $nombreCompleto = $detalle->variante->producto->nombreCompleto;
                                        switch ($detalle->variante->tipo) {
                                            case 0:
                                                $nombreCompleto .= ' - Caja ,' . $detalle->variante->kg . ' kg(s)';
                                                break;
                                        
                                            default:
                                                $nombreCompleto .= ' - Bolsa ,' . $detalle->variante->kg . ' kg(s)';
                                                break;
                                        }
                                        echo $nombreCompleto;
                                    @endphp</td>
                                    <td style="padding: 0px; padding-left: 3px; font-size: 9px ">$
                                        {{ number_format($detalle->precio_producto, 2, ',', '.') }}</td>
                                    <td style="padding: 0px; padding-left: 3px; font-size: 9px ">$
                                        {{ number_format($detalle->subtotal, 2, ',', '.') }}
                                    </td>
                                </tr>
                            @endforeach

                            <tr style="text-align: right;">
                                <td class="invisible2"></td>
                                <td class="invisible2"></td>
                                <td class="invisible2" colspan="2"
                                    style="padding-top: 7px !important; padding-right: 3px !important;"><b>TOTAL:
                                    </b>${{ number_format($venta->total, 2, ',', '.') }}</td>
                            </tr>
                            <tr style="text-align: right;">
                                <td class="invisible2"></td>
                                <td class="invisible2"></td>
                                <td class="invisible2" colspan="2"
                                    style="padding-top: 5px; padding-right: 3px !important;"><b>IVA:
                                    </b>${{ number_format($venta->iva, 2, ',', '.') }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td style="border: none; padding: 0px; padding-left: 10px;">
                    <table style="">
                        <tr>
                            <td class="invisible2">
                                <b>Señor/a | Empresa: </b> {{ $cliente->nombreCompleto }}
                            </td>

                            <td class="invisible2">
                                <b>ORDEN DE PEDIDO N°: </b> {{ $venta->id }}
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2" style="padding-top:5px;">
                                <b>CUIT:</b> {{ $cliente->cuit }}
                            </td>

                            <td class="invisible2" style="padding-top:5px;">
                                <b>FECHA EMISIÓN: </b> {{ date('d/m/y', strtotime($venta->fecha)) }}
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2" style="padding-top:5px;">
                                <b>Teléfono: </b> {{ $cliente->telefono }}
                            </td>
                            <td class="invisible2" style="padding-top:5px;">
                                <b>Localidad: </b> {{ $cliente->localidad->localidad }}
                            </td>
                        </tr>
                    </table>
                    <table style="margin-top: 10px;">
                        <thead>
                            <tr>
                                <th class="header" colspan="4" style="padding: 5px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding: 2px; width: 10%; font-size: 9px;">
                                    <b>CANTIDAD</b>
                                </td>
                                <td style="padding: 2px; width: 45%; font-size: 9px;">
                                    <b>PRODUCTO | DETALLE </b>
                                </td>
                                <td style="padding: 2px; width:20%; font-size: 9px;">
                                    <b>PRECIO UNITARIO</b>
                                </td>
                                <td style="width: 25%; font-size: 9px;">
                                    <b>IMPORTE</b>
                                </td>
                            </tr>

                            @foreach ($detalleVenta as $detalle)
                                <tr>
                                    <td style="padding: 0px; padding-left: 3px; font-size: 9px ">
                                        {{ $detalle->cantidad }}</td>
                                    <td style="padding: 0px; padding-left: 3px; font-size: 9px ">@php
                                        $nombreCompleto = $detalle->variante->producto->nombreCompleto;
                                        switch ($detalle->variante->tipo) {
                                            case 0:
                                                $nombreCompleto .= ' - Caja ,' . $detalle->variante->kg . ' kg(s)';
                                                break;
                                        
                                            default:
                                                $nombreCompleto .= ' - Bolsa ,' . $detalle->variante->kg . ' kg(s)';
                                                break;
                                        }
                                        echo $nombreCompleto;
                                    @endphp</td>
                                    <td style="padding: 0px; padding-left: 3px; font-size: 9px ">$
                                        {{ number_format($detalle->precio_producto, 2, ',', '.') }}</td>
                                    <td style="padding: 0px; padding-left: 3px; font-size: 9px ">$
                                        {{ number_format($detalle->subtotal, 2, ',', '.') }}
                                    </td>
                                </tr>
                            @endforeach

                            <tr style="text-align: right;">
                                <td class="invisible2"></td>
                                <td class="invisible2"></td>
                                <td class="invisible2" colspan="2"
                                    style="padding-top: 7px !important; padding-right: 3px !important;"><b>TOTAL:
                                    </b>${{ number_format($venta->total, 2, ',', '.') }}</td>
                            </tr>
                            <tr style="text-align: right;">
                                <td class="invisible2"></td>
                                <td class="invisible2"></td>
                                <td class="invisible2" colspan="2"
                                    style="padding-top: 5px; padding-right: 3px !important;"><b>IVA:
                                    </b>${{ number_format($venta->iva, 2, ',', '.') }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>



        <div class="footer">
            <table>
                <tr>
                    <td style="border: none; padding: 0px; padding-right: 10px;">
                        <table class="subtabla" style="width: 90%">
                            <tr>
                                <td colspan="2" class="invisible2"><b>Impresión de original y duplicado</b></td>
                            </tr>
                            <tr>
                                <td class="invisible2"><b>DEFENSA AL CONSUMIDOR MENDOZA 0800-222-6678</b></td>
                                <td class="invisible2" style="text-align: right;">www.hfrut.com.ar |
                                    venta@hfrut.com.ar</td>
                            </tr>
                        </table>
                    </td>
                    <td style="border: none; padding: 0px; padding-left: 10px;">
                        <table class="subtabla" style="width: 90%">
                            <tr>
                                <td colspan="2" class="invisible2"><b>Impresión de original y duplicado</b></td>
                            </tr>
                            <tr>
                                <td class="invisible2"><b>DEFENSA AL CONSUMIDOR MENDOZA 0800-222-6678</b></td>
                                <td class="invisible2" style="text-align: right;">www.hfrut.com.ar |
                                    venta@hfrut.com.ar</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </div>
        </div>

    </main>

</body>

</html>
