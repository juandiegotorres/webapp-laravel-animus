<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liquidacion</title>
    <style>
        @page {
            margin: 60px -20px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 10px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 10px;
            right: 0px;
            height: 75px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        main {
            margin: 30px 40px 0px 40px;
        }

        .img {
            width: 100%;
            height: 100%;
        }

        .header {
            width: 100%;
            background-color: #EDEDED;
            border: 1px solid black;
        }

        .header:first-child {
            color: #000;
            text-align: center;
            font-size: 12px;
            padding: 5px 3px;
        }

        table {
            width: 100%;
            /* border: 1px solid black; */
            padding: 0px;
            /* border-collapse: collapse; */
            border-spacing: -1px;
        }

        td {
            border: 1px solid black;
            padding: 6px 0px 6px 9px;
        }

        .subtabla {
            width: 100%;
            border: none;
            margin: 0;
            padding: 0;
        }

        .xd {
            margin-top: 20px;
        }

        .invisible2 {
            border: none;
            padding: 0;
        }

        footer {
            position: fixed;
            bottom: -10px;
            left: 0px;
            right: 0px;
            height: 60px;

            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

        .float-left {
            float: left !important;
            margin-right: 20px;
        }

        .float-right {
            float: right !important;
        }

        .footer {
            position: fixed;
            bottom: -25px;
            left: 40px;
            right: 0px;
            height: 30px;
        }

    </style>
</head>

<body>
    <header>
        <img src="img/cabecera_liquidacion.jpg" class="img" alt="">
    </header>
    <main>
        <table>
            <tr>
                <td style="border: none; padding: 0px; padding-right: 10px;">
                    <table class="subtabla" style="width: 100%">
                        <tr>
                            <td class="invisible2"><b>LIQUIDACION DE PAGO N°</b> {{ $liquidaciones[0]->id }}</td>
                            <td class="invisible2" style="text-align: right; padding-right: 10px;"><b>FECHA:<b>
                                        {{ date('d/m/Y', strtotime($liquidaciones[0]->fechaDeLiquidacion)) }}</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="invisible2">
                                <b>Nombre:</b> {{ $liquidaciones[0]->proveedor->nombreCompleto }}
                            </td>
                        </tr>
                    </table>
                    <table style="margin-top: 15px;">
                        <tr>
                            <th class="header" colspan="4">LIQUIDACIÓN</th>
                        </tr>
                        <tr>
                            <td style="padding: 3px; width: 20%;">
                                CANTIDAD
                            </td>
                            <td style="padding: 3px; width: 40%;">
                                FRUTA VARIEDAD
                            </td>
                            <td style="padding: 3px; width:20%">
                                PRECIO KG
                            </td>
                            <td style="width: 20%;">
                                IMPORTE
                            </td>
                        </tr>
                        @foreach ($compras as $compra)
                            <tr>
                                <td style="padding: 0px; text-align: center;">{{ $compra->cantidad }}</td>
                                <td style="padding: 0px; text-align: center;">
                                    {{ $compra->fruta . ', ' . $compra->variedad }}</td>
                                <td style="padding: 0px; text-align: center;">
                                    ${{ number_format($compra->precioUnitario, 2, ',', '.') }}</td>
                                <td style="padding: 0px; text-align: center;">
                                    ${{ number_format($compra->totalParcial, 2, ',', '.') }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4" style="border: none">
                                <table class="subtabla" style="width: 100%">
                                    <tr>
                                        <td colspan="2" class="invisible2" style="width: 60%"></td>
                                        {{-- SUBTOTAL --}}
                                        <td style="width: 20%; padding-top: 5px;" class="invisible2">
                                            SUBTOTAL</td>
                                        <td class="invisible2"
                                            style="width: 20%; text-align: center !important; padding-top: 5px; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                            ${{ number_format($totalCompras, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        {{-- CUOTAS E IMPORTE --}}
                                        <td colspan="2" class="invisible2" style="padding-right: 35px;">
                                            <table style="padding-left: 2px;">
                                                <tr>
                                                    <td style="padding:0px; text-align: center !important;">CUOTAS</td>
                                                    <td style="padding:0px; text-align: center !important;">
                                                        {{ $liquidaciones[0]->cantidad_cuotas }}</td>
                                                </tr>
                                                <tr style="text-aling: center !important;">
                                                    <td style="padding:0px; text-align: center !important;">IMPORTE</td>
                                                    <td style="padding:0px; text-align: center !important; ">$
                                                        {{ number_format($totalCompras - $totalPagos, 2, ',', '.') }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        {{-- ENTREGADO --}}
                                        <td style="width: 20%;" class="invisible2">
                                            ENTREGADO</td>
                                        <td class="invisible2"
                                            style="width: 20%; text-align: center !important; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                            ${{ number_format($totalPagos, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        {{-- TOTAL NETO --}}
                                        <td colspan="2" class="invisible2" style="width: 60%"></td>
                                        <td style="width: 20%;" class="invisible2">
                                            TOTAL NETO</td>
                                        <td class="invisible2"
                                            style="width: 20%; text-align: center !important; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                            ${{ number_format($totalCompras - $totalPagos, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        {{-- IVA --}}
                                        <td colspan="2" class="invisible2" style="width: 60%; padding-right: 8px;">
                                            <b>OBSERVACIONES:</b>
                                            {{ $liquidaciones[0]->observaciones }}
                                        </td>
                                        <td style="width: 20%; padding-top: 5px;" class="invisible2">
                                            IVA</td>
                                        <td class="invisible2"
                                            style="width: 20%; text-align: center !important; padding-top: 5px; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                            ${{ number_format($totalIva, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="invisible2" style="width: 60%"></td>
                                        {{-- TOTAL --}}
                                        <td style="width: 20%; padding-top: 5px;" class="invisible2">
                                            TOTAL</td>
                                        <td class="invisible2"
                                            style="width: 20%; text-align: center !important; padding-top: 5px; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                            ${{ number_format($totalIva + ($totalCompras - $totalPagos), 2, ',', '.') }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="border: none; padding: 0px; padding-left: 10px;">
                    <table class="subtabla" style="width: 100%">
                        <tr>
                            <td class="invisible2"><b>LIQUIDACION DE PAGO N°</b> {{ $liquidaciones[0]->id }}</td>
                            <td class="invisible2" style="text-align: right; padding-right: 10px;"><b>FECHA:<b>
                                        {{ date('d/m/Y', strtotime($liquidaciones[0]->fechaDeLiquidacion)) }}</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="invisible2">
                                <b>Nombre:</b> {{ $liquidaciones[0]->proveedor->nombreCompleto }}
                            </td>
                        </tr>
                    </table>
                    <table style="margin-top: 15px;">
                        <tr>
                            <th class="header" colspan="4">LIQUIDACIÓN</th>
                        </tr>
                        <tr>
                            <td style="padding: 3px; width: 20%;">
                                CANTIDAD
                            </td>
                            <td style="padding: 3px; width: 40%;">
                                FRUTA VARIEDAD
                            </td>
                            <td style="padding: 3px; width:20%">
                                PRECIO KG
                            </td>
                            <td style="width: 20%;">
                                IMPORTE
                            </td>
                        </tr>
                        @foreach ($compras as $compra)
                            <tr>
                                <td style="padding: 0px; text-align: center;">{{ $compra->cantidad }}</td>
                                <td style="padding: 0px; text-align: center;">
                                    {{ $compra->fruta . ', ' . $compra->variedad }}</td>
                                <td style="padding: 0px; text-align: center;">
                                    ${{ number_format($compra->precioUnitario, 2, ',', '.') }}</td>
                                <td style="padding: 0px; text-align: center;">
                                    ${{ number_format($compra->totalParcial, 2, ',', '.') }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4" style="border: none">
                                <table class="subtabla" style="width: 100%">
                                    <tr>
                                        <td colspan="2" class="invisible2" style="width: 60%"></td>
                                        {{-- SUBTOTAL --}}
                                        <td style="width: 20%; padding-top: 5px;" class="invisible2">
                                            SUBTOTAL</td>
                                        <td class="invisible2"
                                            style="width: 20%; text-align: center !important; padding-top: 5px; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                            ${{ number_format($totalCompras, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        {{-- CUOTAS E IMPORTE --}}
                                        <td colspan="2" class="invisible2" style="padding-right: 35px;">
                                            <table style="padding-left: 2px;">
                                                <tr>
                                                    <td style="padding:0px; text-align: center !important;">CUOTAS</td>
                                                    <td style="padding:0px; text-align: center !important;">
                                                        {{ $liquidaciones[0]->cantidad_cuotas }}</td>
                                                </tr>
                                                <tr style="text-aling: center !important;">
                                                    <td style="padding:0px; text-align: center !important;">IMPORTE</td>
                                                    <td style="padding:0px; text-align: center !important; ">$
                                                        {{ number_format($totalCompras - $totalPagos, 2, ',', '.') }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        {{-- ENTREGADO --}}
                                        <td style="width: 20%;" class="invisible2">
                                            ENTREGADO</td>
                                        <td class="invisible2"
                                            style="width: 20%; text-align: center !important; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                            ${{ number_format($totalPagos, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        {{-- TOTAL NETO --}}
                                        <td colspan="2" class="invisible2" style="width: 60%"></td>
                                        <td style="width: 20%;" class="invisible2">
                                            TOTAL NETO</td>
                                        <td class="invisible2"
                                            style="width: 20%; text-align: center !important; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                            ${{ number_format($totalCompras - $totalPagos, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        {{-- IVA --}}
                                        <td colspan="2" class="invisible2" style="width: 60%; padding-right: 8px;">
                                            <b>OBSERVACIONES:</b>
                                            {{ $liquidaciones[0]->observaciones }}
                                        </td>
                                        <td style="width: 20%; padding-top: 5px;" class="invisible2">
                                            IVA</td>
                                        <td class="invisible2"
                                            style="width: 20%; text-align: center !important; padding-top: 5px; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                            ${{ number_format($totalIva, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="invisible2" style="width: 60%"></td>
                                        {{-- TOTAL --}}
                                        <td style="width: 20%; padding-top: 5px;" class="invisible2">
                                            TOTAL</td>
                                        <td class="invisible2"
                                            style="width: 20%; text-align: center !important; padding-top: 5px; padding-left: 7px; border-left: 1px solid #000; border-right: 1px solid #000">
                                            ${{ number_format($totalIva + ($totalCompras - $totalPagos), 2, ',', '.') }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table class="footer">
            <tr>
                <td style="border: none; padding: 0px; padding-right: 10px;">
                    <table class="subtabla" style="width: 100%;">
                        <tr>
                            <td class="invisible2" width="50%"></td>
                            <td class="invisible2" style="text-align: center">
                                -----------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2" width="50%"></td>
                            <td class="invisible2" style="text-align: center">FIRMA PROVEEDOR</td>
                        </tr>
                    </table>
                </td>
                <td style="border: none; padding: 0px; padding-left: 10px;">
                    <table class="subtabla" style="width: 100%;">
                        <tr>
                            <td class="invisible2" width="50%"></td>
                            <td class="invisible2" style="text-align: center">
                                -----------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td class="invisible2" width="50%"></td>
                            <td class="invisible2" style="text-align: center">FIRMA PROVEEDOR</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </main>

</body>

</html>
