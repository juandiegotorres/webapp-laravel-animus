<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Balance impositivo</title>
    <style>
        @page {
            margin: 40px 25px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 14px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 160px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        table {
            width: 100%;
            font-size: 12px;
            border: 1px solid #000;
            border-spacing: -1px;
        }

        table td {
            border: 1px solid black;
            padding: 4px;
        }

        .sinBordes {
            border: none !important;
        }

        footer {
            position: fixed;
            bottom: -50px;
            left: 0px;
            right: 0px;
            height: 60px;

            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

    </style>
</head>

<body>
    <table style="border: none;">
        <tbody>
            <tr>
                <td class="sinBordes"><b>PERIODO:
                        @php
                            if (isset($mesPeriodo['mes'])) {
                                \Carbon\Carbon::setLocale('es');
                                $fecha = DateTime::createFromFormat('!m', $mesPeriodo['mes']);
                                echo ucfirst(\Carbon\Carbon::parse($fecha)->translatedFormat('F')) . ' ' . now()->year;
                            } else {
                                echo date('d/m/Y', strtotime($mesPeriodo['desde'])) . ' - ' . date('d/m/Y', strtotime($mesPeriodo['hasta']));
                            }
                        @endphp
                    </b></td>
                <td class="sinBordes" style="text-align: right;"><b>FECHA DE EMISION:
                        {{ date('d/m/Y', strtotime(now())) }}</b></td>
            </tr>
        </tbody>
    </table>

    <table style="margin-top: 30px;">
        <thead>
            <tr>
                <th colspan="5" style="background: #DFDFDF; text-align: center; padding: 7px; font-size: 18px;">BALANCE
                    IMPOSITIVO</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><b>N° DE FACTURA</b></td>
                <td><b>FECHA DE EMISIÓN</b></td>
                <td><b>EMITIDA/RECIBIDA</b></td>
                <td><b>TOTAL NETO</b></td>
                <td><b>IVA</b></td>
            </tr>
            @foreach ($facturas as $factura)
                <tr>
                    <td>{{ $factura->nroFactura }}</td>
                    <td>{{ date('d/m/Y', strtotime($factura->fechaEmision)) }}</td>
                    <td>{{ ucfirst($factura->emitida_recibida) }}</td>
                    <td>$ {{ number_format($factura->total, 2, ',', '.') }}</td>
                    <td>$ {{ number_format($factura->IVA, 2, ',', '.') }}</td>
                </tr>
            @endforeach

        </tbody>
    </table>

    <footer>
        <table style="border: none; font-size: 16px;">
            <tr>
                <td class="sinBordes">BALANCE IVA: $ {{ number_format($balanceIva, 2, ',', '.') }} </td>
                <td class="sinBordes" style="text-align: right;">BALANCE TOTALES: $
                    {{ number_format($balanceTotal, 2, ',', '.') }}</td>
            </tr>
        </table>
    </footer>
</body>

</html>
