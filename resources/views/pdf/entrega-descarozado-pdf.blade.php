<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Entrega descarozado</title>
    <style>
        @page {
            margin: 60px 15px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 14px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 80px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        main {
            position: absolute;
            margin: 30px 40px 0px 40px;
        }

        .img {
            width: 100%;
            height: 100%;
        }

        .header {
            width: 100%;
            background-color: #29643e;
            border: 1px solid black;
        }

        .header:first-child {
            color: #fff;
            text-align: center;
            font-size: 16px;
            padding: 10px 3px;
        }

        table {
            width: 100%;
            /* border: 1px solid black; */
            padding: 0px;
            /* border-collapse: collapse; */
            border-spacing: -1px;
        }

        td {
            border: 1px solid black;
            padding: 6px 0px 6px 9px;
        }

        .subtabla {
            width: 100%;
            border: none;
            margin: 0;
        }

        .p {
            padding: 13px;
        }

        .invisible2 {
            margin-top: -20px;
            border: none;
            padding: 0;
            font-size: 12px;
        }

        footer {
            position: fixed;
            bottom: -10px;
            left: 0px;
            right: 0px;
            height: 60px;

            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

    </style>
</head>

<body>
    <header>
        <img src="img/cabecera_pdf.jpg" class="img" alt="">
    </header>
    <main>
        <table>
            <tr>
                <th class="header" colspan="3">REMITO ENTREGA DESCAROZADO</th>
            </tr>
            <tr>
                <td style="width: 75%;"><b>Razón social: </b>Achetoni Oscar y Digiambatista Fanny S.H.
                </td>
                <td style="width: 25%; margin-right:10px;"><b>CUIT: </b>30-71102728-5</td>
            </tr>
            <tr>
                <td colspan="3" style="padding: 0px;">
                    <table class="subtabla">
                        <tr>
                            <td style="border:none; border-right: 1px solid black; width: 37%;"><b>Dirección:
                                </b>Moreno y Pública S/N</td>
                            <td style="border:none; border-right: 1px solid black; width: 37%;"><b>Localidad:
                                </b>Moreno y Pública S/N</td>
                            <td style="border:none; width: 26%;"><b>Provincia:
                                </b>Mendoza</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 50%;"><b>Nombre y Apellido Trabajador:
                    </b>{{ $detalleDescarozado->first()->nombreCompleto }}
                </td>
                <td style="width: 50%;"><b>DNI: </b>{{ $detalleDescarozado->first()->dni }}</td>
            </tr>
            <tr>
                <td style="width: 50%;"><b>Puesto del Trabajador:</b> {{ $detalleDescarozado->first()->nombreCargo }}
                </td>
                <td style="width: 50%;"><b>Fecha de emisión: </b>
                    {{ date('d-m-Y', strtotime($detalleDescarozado->first()->fecha)) }}</td>
            </tr>
            {{-- <tr>
                <td colspan="3" style="padding: 0px;">
                    <table class="subtabla">
                        <tr>
                            <td style="border:none; border-right: 1px solid black; width: 37%;"><b>Mes:
                                </b>{{ $sueldo->first()->fechaFin }}</td>
                            <td style="border:none; border-right: 1px solid black; width: 37%;"><b>Horas trabajadas:
                                </b>{{ $sueldo->first()->horas }}</td>
                            <td style="border:none; width: 26%;"><b>Valor Hora:
                                </b>{{ $sueldo->first()->precioHora }}</td>
                        </tr>
                    </table>
                </td>
            </tr> --}}
            <tr>
                <table>
                    <tr>
                        <td><b>FECHA ENTREGA</b></td>
                        <td><b>PRODUCTO</b></td>
                        <td><b>CANTIDAD</b></td>
                    </tr>
                    <tr>
                        <td>{{ date('d-m-Y', strtotime($detalleDescarozado->first()->fecha)) }}</td>
                        <td>{{ $detalleDescarozado->first()->fruta . ', ' . $detalleDescarozado->first()->variedad }}
                        </td>
                        <td>{{ $detalleDescarozado->first()->cantidadKg }} kgs</td>
                    </tr>
                    @for ($i = 0; $i < 3; $i++)
                        <tr>
                            <td class="p"></td>
                            <td class="p"></td>
                            <td class="p"></td>
                        </tr>
                    @endfor
                </table>
            </tr>
        </table>
    </main>
    <footer>
        <table>
            <tr>
                <td class="invisible2">
                    <div style="text-align: center;">
                        <h4>______________________________</h4>
                        <h4>FIRMA ADMINISTRACIÓN</h4>
                    </div>
                </td>
                <td class="invisible2">
                    <div style="text-align: center;">
                        <h4>______________________________</h4>
                        <h4>FIRMA TRABAJADOR
                        </h4>
                    </div>
                </td>
            </tr>
        </table>
    </footer>
</body>

</html>
