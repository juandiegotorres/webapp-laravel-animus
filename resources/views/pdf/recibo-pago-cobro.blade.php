<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recibo pago</title>
    <style>
        @page {
            margin: 60px -20px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 10px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 10px;
            right: -5px;
            height: 80px;
            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        main {
            margin: 30px 40px 0px 40px;
        }

        .img {
            width: 100%;
            height: 100%;
        }

        .header {
            width: 100%;
            background-color: #EDEDED;
            border: 1px solid black;
        }

        .titulo {
            color: #000;
            text-align: center;
            font-size: 13px;
            padding: 7px 3px;
        }

        table {
            width: 100%;
            /* border: 1px solid black; */
            padding: 0px;
            /* border-collapse: collapse; */
            border-spacing: -1px;
        }

        td {
            border: 1px solid black;
            padding: 6px 0px 6px 9px;
        }

        .subtabla {
            width: 100%;
            border: none;
            margin: 0;
        }

        .p {
            padding: 13px;
        }

        .invisible2 {
            margin-top: -20px;
            border: none;
            padding: 0;
            font-size: 12px;
        }

        footer {
            position: fixed;
            bottom: -40px;
            left: 40px;
            right: 40px;
            height: 60px;
            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

    </style>
</head>

<body>
    <header>
        <img src="img/cabecera_liquidacion.jpg" class="img" alt="">
    </header>
    <main>
        <table>
            <tr>
                <td style="border: none; padding: 0px; padding-right: 10px; width: 50%;">
                    <table class="subtabla" style="width: 100%; font-size: 10px !important;">
                        <tr>
                            <td class="invisible2"><b>RECIBO DE PAGO N°</b> {{ $cobro->id }}</td>
                            <td class="invisible2" style="text-align: right; padding-right: 10px;"><b>FECHA:</b>
                                {{ date('d/m/y', strtotime($cobro->fecha)) }}</td>
                        </tr>
                        <tr>
                            <td class="invisible2">
                                <b>Nombre:</b> {{ $cobro->cliente->nombreCompleto }}
                            </td>
                            <td class="invisible2" style="text-align: right; padding-right: 10px;">
                                {{-- <b>Concepto:</b> {{ 'aca concepto' }} --}}
                            </td>
                        </tr>
                    </table>
                    <table style="margin-top: 10px;">
                        <tr>
                            <th class="header titulo" colspan="3">RECIBO DE PAGO</th>
                        </tr>

                        <tr>
                            <table>
                                <tr>
                                    <td><b>MÉTODO DE PAGO</b></td>
                                    <td><b>NRO CHEQUE</b></td>
                                    <td><b>TOTAL</b></td>
                                </tr>
                                @foreach ($detalleCobro as $detalle)
                                    <tr>
                                        <td>{{ $detalle->metodoPago }}</td>
                                        <td>{{ $detalle->nroCheque }}</td>
                                        <td>$ {{ number_format($detalle->monto, 2, ',', '.') }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3" style="text-align: right; padding-right: 20px; border: none;">
                                        <b>TOTAL: </b>
                                        ${{ number_format($cobro->montoTotal, 2, ',', '.') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: right; padding-right: 20px; border: none;">
                                        <b>IVA: </b>
                                        ${{ number_format($cobro->iva, 2, ',', '.') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: right; padding-right: 20px; border: none;">
                                        <b>SALDO: </b>
                                        ${{ number_format($balance[0]->balance, 2, ',', '.') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: left; border: none;">
                                        <b>OBSERVACIONES: </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: left; border: none;">
                                        {{ $cobro->observaciones }}
                                    </td>
                                </tr>
                            </table>
                        </tr>
                    </table>
                </td>
                <td style="border: none; padding: 0px; padding-left: 10px; width: 50%;">
                    <table class="subtabla" style="width: 100%; font-size: 10px !important;">
                        <tr>
                            <td class="invisible2"><b>RECIBO DE PAGO N°</b> {{ $cobro->id }}</td>
                            <td class="invisible2" style="text-align: right; padding-right: 10px;"><b>FECHA:</b>
                                {{ date('d/m/y', strtotime($cobro->fecha)) }}</td>
                        </tr>
                        <tr>
                            <td class="invisible2">
                                <b>Nombre:</b> {{ $cobro->cliente->nombreCompleto }}
                            </td>
                            <td class="invisible2" style="text-align: right; padding-right: 10px;">
                                {{-- <b>Concepto:</b> {{ 'aca concepto' }} --}}
                            </td>
                        </tr>
                    </table>
                    <table style="margin-top: 10px;">
                        <tr>
                            <th class="header titulo" colspan="3">RECIBO DE PAGO</th>
                        </tr>

                        <tr>
                            <table>
                                <tr>
                                    <td><b>MÉTODO DE PAGO</b></td>
                                    <td><b>NRO CHEQUE</b></td>
                                    <td><b>TOTAL</b></td>
                                </tr>
                                @foreach ($detalleCobro as $detalle)
                                    <tr>
                                        <td>{{ $detalle->metodoPago }}</td>
                                        <td>{{ $detalle->nroCheque }}</td>
                                        <td>$ {{ number_format($detalle->monto, 2, ',', '.') }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3" style="text-align: right; padding-right: 20px; border: none;">
                                        <b>TOTAL: </b>
                                        ${{ number_format($cobro->montoTotal, 2, ',', '.') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: right; padding-right: 20px; border: none;">
                                        <b>IVA: </b>
                                        ${{ number_format($cobro->iva, 2, ',', '.') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: right; padding-right: 20px; border: none;">
                                        <b>SALDO: </b>
                                        ${{ number_format($balance[0]->balance, 2, ',', '.') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: left; border: none;">
                                        <b>OBSERVACIONES: </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: left; border: none;">
                                        {{ $cobro->observaciones }}
                                    </td>
                                </tr>
                            </table>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </main>
    <footer>
        <table>
            <tr>
                <td style="border: none;padding: 0px; padding-right: 10px; font-size: 10px !important;">
                    <table>
                        <tr>
                            <td class="invisible2" style="width: 50%;">

                            </td>
                            <td class="invisible2" style="width: 50%;">
                                <div style="text-align: center;">
                                    <h4>______________________________</h4>
                                    <h4 style="font-weight: regular;">FIRMA PROVEEDOR</h4>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="border: none; padding: 0px; padding-left: 10px; font-size: 10px !important;">
                    <table>
                        <tr>
                            <td class="invisible2" style="width: 50%;">

                            </td>
                            <td class="invisible2" style="width: 50%;">
                                <div style="text-align: center;">
                                    <h4>______________________________</h4>
                                    <h4 style="font-weight: regular;">FIRMA PROVEEDOR</h4>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </footer>
</body>

</html>
