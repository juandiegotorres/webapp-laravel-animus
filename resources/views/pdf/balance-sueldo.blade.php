<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Balance sueldo</title>
    <style>
        @page {
            margin: 60px 40px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 14px;
        }

        table {
            width: 100%;
        }

        header {
            position: fixed;
            top: -40px;
            left: 0px;
            right: 0px;
            height: 105px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        .img {
            width: 100%;
            height: 100%;
        }

        .tablaDetalle {
            width: 100%;
            border-spacing: -1px;
        }

        thead {
            border-bottom: 1px solid #000;
        }

        tbody {
            text-align: left;
        }

        tr {
            padding: 0;
        }

        h1 {
            margin: 0 !important;
        }

        h4 {
            margin: 0;
            margin-bottom: 20px;
        }

        .tablaDetalle td {
            border: 1px solid black;
            padding: 4px;
        }

        .tablaDetalle th {
            border: 1px solid black;
            font-size: 11px;
            padding: 8px;
        }

    </style>
</head>

<body>
    <h4 style="">Fecha de emisión: {{ date('d/m/Y', strtotime(now())) }}
    </h4>
    <h5 style="margin-bottom: 0px;"><b>Nombre: </b> {{ $empleado['nombre'] }}</h5>
    <h5 style="margin-top: 0px;"><b>Teléfono: </b>
        {{ $empleado['telefono'] == null ? 'No especificado' : $empleado['telefono'] }}</h5>

    <table class="tablaDetalle">
        <thead>
            <tr>
                <td colspan="5" style="background: #DFDFDF; border: none !important; padding: 12px;"></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><b>FECHA</b></td>
                <td><b>TIPO DE MOVIMIENTO</b></td>
                <td><b>HABER</b></td>
                <td><b>DEBE</b></td>
                <td><b>TOTAL</b></td>
            </tr>

            @foreach ($balancesClean as $balance)
                <tr style="font-size: 11px;">
                    <td>{{ date('d/m/y', strtotime($balance['fecha'])) }}</td>
                    <td>{{ $balance['tipoMovimiento'] }}</td>
                    <td>$ {{ number_format($balance['haber'], 2, ',', '.') }}</td>
                    <td>$ {{ number_format($balance['debe'], 2, ',', '.') }}</td>
                    <td>$ {{ number_format($balance['total'], 2, ',', '.') }}</td>
                </tr>
            @endforeach
            <tr style="border: none !important;">
                <td colspan="4" style="border: none !important;text-align: right; font-size: 15px;">TOTAL</td>
                <td style="text-align: right; font-size: 15px;">$
                    {{ number_format($balancesClean[count($balancesClean) - 1]['total'], 2, ',', '.') }}
                </td>
            </tr>
        </tbody>
    </table>

</body>


</html>
