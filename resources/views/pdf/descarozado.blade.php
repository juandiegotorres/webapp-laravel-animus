<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Descarozado</title>
    <style>
        @page {
            margin: -10px -20px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 10px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 80px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            text-align: center;
            /* line-height: 35px; */
        }

        main {
            margin: 30px 40px 0px 40px;
        }

        .img {
            width: 100%;
            height: 100%;
        }

        .header {
            width: 100%;
            background-color: #29643e;
            border: 1px solid black;
            color: #fff;
        }

        .titulo {
            text-align: center;
            font-size: 12px;
            padding: 5px 3px;
        }

        table {
            width: 100%;
            /* border: 1px solid black; */
            padding: 0px;
            /* border-collapse: collapse; */
            border-spacing: -1px;
        }

        td {
            border: 1px solid black;
            padding: 6px 0px 6px 9px;
        }

        .subtabla {
            width: 100%;
            border: none;
            margin: 0;
            padding: 0;
        }

        .xd {
            margin-top: 20px;
        }

        .invisible2 {
            border: none;
            padding: 0;
        }

        footer {
            position: fixed;
            bottom: -10px;
            left: 0px;
            right: 0px;
            height: 60px;

            /** Extra personal styles **/
            background-color: white;
            color: black;
        }

        .float-left {
            float: left !important;
            margin-right: 20px;
        }

        .float-right {
            float: right !important;
        }

        .footer {
            position: fixed;
            bottom: 60px;
            left: 40px;
            right: 40px;
            height: 30px;
        }

    </style>
</head>

<body>
    <main>
        <table>
            <tr>
                <td style="border: none; padding: 0px; padding-right: 10px;">
                    <table>
                        <tr>
                            <th class="header titulo" colspan="2">DETALLE DESCAROZADO</th>
                        </tr>
                        <tr>
                            <td style="padding: 3px; width: 75%;">
                                Nombre y Apellido trabajador: {{ $detalleDescarozado->first()->nombreCompleto }}
                            </td>
                            <td style="width: 25%;">
                                DNI: {{ $detalleDescarozado->first()->dni }}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 0px;">
                                <table class="subtabla">
                                    <tr style="background: #FFF;">
                                        <td style="padding: 3px; width: 50%;">
                                            Puesto: {{ $detalleDescarozado->first()->nombreCargo }}
                                        </td>
                                        <td style="width: 50%;">
                                            Fecha de emisión:
                                            {{ date('d/m/y', strtotime($detalleDescarozado->first()->fecha)) }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 0px;">
                                <table class="subtabla">
                                    <tr style="background: #FFF;">
                                        <td style="padding: 12px; width: 60%;">
                                        </td>
                                        <td style="width: 40%;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 0px;">
                                <table class="subtabla">
                                    <thead>
                                        <tr>
                                            <th style="padding: 5px;border-right: 1px solid black;">FECHA DE ENTREGA
                                            </th>
                                            <th style="border-right: 1px solid black;">PRODUCTO</th>
                                            <th>CANTIDAD</th>
                                        </tr>
                                    </thead>
                                    <tr style="background: #FFF;">
                                        <td>
                                            {{ date('d/m/y', strtotime($detalleDescarozado->first()->fecha)) }}
                                        </td>
                                        <td>
                                            {{ $detalleDescarozado->first()->fruta . ', ' . $detalleDescarozado->first()->variedad }}
                                        </td>
                                        <td>
                                            {{ $detalleDescarozado->first()->cantidadKg }}
                                        </td>
                                    </tr>
                                    @for ($i = 0; $i < 10; $i++)
                                        <tr style="background: #FFF;">
                                            <td style="padding: 10px; "></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endfor
                                </table>
                            </td>
                        </tr>
                    </table>

                </td>
                <td style="border: none; padding: 0px; padding-left: 10px;">
                    <table>
                        <tr>
                            <th class="header titulo" colspan="2">DETALLE DESCAROZADO</th>
                        </tr>
                        <tr>
                            <td style="padding: 3px; width: 75%;">
                                Nombre y Apellido trabajador: {{ $detalleDescarozado->first()->nombreCompleto }}
                            </td>
                            <td style="width: 25%;">
                                DNI: {{ $detalleDescarozado->first()->dni }}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 0px;">
                                <table class="subtabla">
                                    <tr style="background: #FFF;">
                                        <td style="padding: 3px; width: 50%;">
                                            Puesto: {{ $detalleDescarozado->first()->nombreCargo }}
                                        </td>
                                        <td style="width: 50%;">
                                            Fecha de emisión:
                                            {{ date('d/m/y', strtotime($detalleDescarozado->first()->fecha)) }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 0px;">
                                <table class="subtabla">
                                    <tr style="background: #FFF;">
                                        <td style="padding: 12px; width: 60%;">
                                        </td>
                                        <td style="width: 40%;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 0px;">
                                <table class="subtabla">
                                    <thead>
                                        <tr>
                                            <th style="padding: 5px;border-right: 1px solid black;">FECHA DE ENTREGA
                                            </th>
                                            <th style="border-right: 1px solid black;">PRODUCTO</th>
                                            <th>CANTIDAD</th>
                                        </tr>
                                    </thead>
                                    <tr style="background: #FFF;">
                                        <td>
                                            {{ date('d/m/y', strtotime($detalleDescarozado->first()->fecha)) }}
                                        </td>
                                        <td>
                                            {{ $detalleDescarozado->first()->fruta . ', ' . $detalleDescarozado->first()->variedad }}
                                        </td>
                                        <td>
                                            {{ $detalleDescarozado->first()->cantidadKg }}
                                        </td>
                                    </tr>
                                    @for ($i = 0; $i < 10; $i++)
                                        <tr style="background: #FFF;">
                                            <td style="padding: 10px; "></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endfor
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="footer">
            <table>
                <tr>
                    <td style="border: none; padding: 0px; padding-right: 10px;">
                        <table>
                            <tr>
                                <td class="invisible2" style="width: 100%">
                                </td>
                                <td class="invisible2" style="text-align: center;">
                                    -----------------------------------------------
                                </td>
                            </tr>
                            <tr>
                                <td class="invisible2">
                                </td>
                                <td class="invisible2" style="text-align: center;">
                                    FIRMA TRABAJADOR/A
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="invisible2"
                                    style="text-align: center; padding-top: 25px; font-weight:bold; font-size: 14px;">
                                    COMPROBANTE DE
                                    CONTROL INTERNO</td>
                            </tr>
                        </table>
                    </td>
                    <td style="border: none; padding: 0px; padding-left: 10px;">
                        <table>
                            <tr>
                                <td class="invisible2" style="width: 100%">
                                </td>
                                <td class="invisible2" style="text-align: center;">
                                    -----------------------------------------------
                                </td>
                            </tr>
                            <tr>
                                <td class="invisible2">
                                </td>
                                <td class="invisible2" style="text-align: center;">
                                    FIRMA TRABAJADOR/A
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="invisible2"
                                    style="text-align: center; padding-top: 25px; font-weight:bold; font-size: 14px;">
                                    COMPROBANTE DE
                                    CONTROL INTERNO</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


        </div>
    </main>

</body>

</html>
