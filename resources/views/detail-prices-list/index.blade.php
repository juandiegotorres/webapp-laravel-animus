@extends('adminlte::page')

@section('title', 'Editar lista de precios')

@section('content_header')
    <div class=" col-md-12 d-flex justify-content-between pl-2 ">
        <button type="button" class="btn btn-danger" onclick="history.back();"><i class=" fas fa-arrow-left"></i></button>
        <h1 class="text-center ml-5">{{ $listaPrecios->nombre }}</h1>
        <a href="{{ route('lista-precio.pdf', ['priceList' => $listaPrecios->id]) }}" target="_blank"
            class="btn btn-warning ">Imprimir lista <i class="fa fa-print ml-2"></i></a>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12" id="resultForm">
        </div>
    </div>
    <form action="" id="formProductoListaPrecio">
        {{-- ======================== ID LISTA PRECIOS ========================= --}}
        <input type="hidden" id="price_list_id" value="{{ $listaPrecios->id }}" name="price_list_id">
        <div class="row mx-2 pt-2 bg-white rounded border">
            <div class="col-md-3 ">
                <div class="form-group">
                    <label for="" class="form-col-label">Producto</label>
                    <select type="text" class="form-control" name="product_id" id="product_id">
                        @foreach ($productos as $producto)
                            <option value="{{ $producto->id }}">{{ $producto->nombreCompleto }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3 ">
                <div class="form-group">
                    <label for="" class="form-col-label">Variante(s)</label>
                    <select type="text" class="form-control" name="variante_id" id="variantes">

                    </select>
                </div>
            </div>
            <div class="col-md-2 ">
                <div class="form-group">
                    <label for="" class="form-col-label">Precio x KG</label>
                    <input type="number" class="form-control" name="precioKg" id="precioKg">
                    {{-- <div id="validationServerUsernameFeedback" class="invalid-feedback">
                        Please choose a username.
                    </div> --}}
                </div>
            </div>

            <div class="col-md-2 ">
                <div class="form-group">
                    <label for="" class="form-col-label">Precio x Caja</label>
                    <input type="number" class="form-control" name="precioCaja" id="precioCaja">
                </div>
            </div>
            <div class="col-md-2 ">
                <div class="form-group">
                    <label for="" class="form-col-label">ㅤ</label>
                    <button class="form-control btn btn-primary" id="btnNuevoPrecioProducto">Agregar precio</button>
                </div>
            </div>
        </div>
    </form>
    <div class="row px-2 mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    {{-- TABLA --}}
                    <table class="table table-bordered bg-white mt-3" id="tablaListaPrecios">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Producto</th>
                                <th scope="col">Variante</th>
                                <th scope="col">Precio x KG</th>
                                <th scope="col">Precio x Caja</th>
                                <th scope="col" style="width:200px;">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>

    {{-- MODAL MODIFICAR PRECIO --}}
    <div class="modal fade" id="modalModificarPrecio" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="labelListaPrecio">Modificar precio
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="" method="" id="formModificarPrecio" autocomplete="off">
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h5 class="text-center font-weight-bold bg-primary rounded p-1" id="nombreProducto"></h5>
                            </div>
                        </div>
                        <span id="resultadoFormMoficiarPrecio"></span>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="precioKg" class="col-form-label">Precio x KG (*)</label>
                                <input type="number" class="form-control" name="precioKg" id="precioKgMod"
                                    placeholder="Precio..." autofocus required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="precioCaja" class="col-form-label">Precio x Caja (*)</label>
                                <input type="number" name="precioCaja" class="form-control" id="precioCajaMod"
                                    placeholder="Precio..." required>
                            </div>
                        </div>

                        <input type="hidden" id="idDetalleListaPrecio">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-success" id="btnModificarPrecio">Modificar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')

    <script>
        $(document).ready(function() {
            cargarVariantes($('#product_id').val());

            $('.sr-only').trigger('click');
            idListaPrecio = $('#price_list_id').val();

            $('#product_id').select2();

            //EVENTO Select Productos 
            $('#product_id').change(function(e) {
                e.preventDefault();
                cargarVariantes($(this).val());
            });

            //EVENTO Agregar precio
            $('#btnNuevoPrecioProducto').on('click', function(e) {
                e.preventDefault();
                if ($('#product_id').val() == null) {
                    Swal.fire(
                        'Seleccione un producto',
                        '',
                        'error'
                    );
                    return
                }
                $.ajax({
                    url: "/producto-lista-precio",
                    method: "POST",
                    //Envio los datos del formulario
                    data: $('#formProductoListaPrecio').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        //Si devuelvo repetido desde el back significa que el producto ya tiene un precio, por lo que pregunto 
                        //si desea actualizarlo
                        if (data.repetido) {
                            Swal.fire({
                                    title: `Precio repetido`,
                                    text: "Este producto ya tiene un precio establecido. ¿Desea actualizarlo?",
                                    icon: "question",
                                    showCancelButton: true,
                                    confirmButtonText: 'Si',
                                    confirmButtonColor: '#29643e',
                                    cancelButtonText: 'No',
                                })
                                .then((response) => {
                                    if (response.isConfirmed) {
                                        //Actualizar precio
                                        $.ajax({
                                            //Envio la url con el parametro data.idDetalle[0].id que es el id que me devolvio el back
                                            //para poder identificar que precio es el que tengo que actualizar
                                            //Este parametro me lo devuelve cuando intento agregar un precio a un producto que ya tiene
                                            url: "/modificar-precio-producto/" +
                                                data.idDetalle[0].id,
                                            method: "PUT",
                                            //Envio los datos del formulario, con los valores de los inputs y el token
                                            data: {
                                                precioKg: $('#precioKg').val(),
                                                precioCaja: $('#precioCaja')
                                                    .val(),
                                                kgCaja: $('#kgCaja').val(),
                                                _token: "{{ csrf_token() }}"
                                            },
                                            dataType: 'json',
                                            success: function(data) {
                                                if (data.error) {
                                                    html =
                                                        '<div class="alert alert-danger alert-dismissible fade show mx-2" role="alert"><ul>';
                                                    for (var i = 0; i < data
                                                        .error.length; i++) {
                                                        html += '<li>' + data
                                                            .error[i] + '</li>';
                                                    }
                                                    html +=
                                                        '</ul> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                                                    $('#resultForm').html(html);
                                                }
                                                if (data.success) {
                                                    //Si todo esta bien muestro una alerta 
                                                    Swal.fire({
                                                        toast: true,
                                                        icon: 'success',
                                                        title: data
                                                            .success,
                                                        position: 'top-right',
                                                        showConfirmButton: false,
                                                        timer: 2300,
                                                    });
                                                    //Limpio los inputs
                                                    $('#precioKg').val('');
                                                    $('#precioCaja').val('');
                                                    $('#kgCaja').val('');
                                                    $('#product_id').focus();
                                                    $('#resultForm').empty();
                                                    //Recargo la tabla
                                                    tablaListaPrecios.ajax
                                                        .reload();
                                                }
                                            }
                                        })
                                    }
                                });
                        }
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class="alert alert-danger alert-dismissible fade show mx-2" role="alert"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html +=
                                '</ul> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                            $('#resultForm').html(html);
                        }
                        if (data.success) {
                            // Si todo esta bien muestro una alerta
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });

                            //Limpio los inputs
                            $('#precioKg').val('');
                            $('#precioCaja').val('');
                            $('#kgCaja').val('');
                            $('#product_id').focus();
                            $('#resultForm').empty();

                            tablaListaPrecios.ajax.reload();
                        }
                    }
                })
            });

            //EVENTO Modal editar precio
            //Cuando se muestra el modal para modificar
            $("#modalModificarPrecio").on("show.bs.modal", function(event) {

                var button = $(event.relatedTarget);
                var variante = button.parent().parent().siblings('.variante').text();

                //Paso los datos a travez de variables data dentro del boton (HTML), al igual que el id
                var nombreProducto = button.data("nombre");
                var precioKg = button.data("preciokg");
                var precioCaja = button.data("precio_caja");
                var idDetalleListaPrecio = button.data("id");
                var modal = $(this);
                // //Le asigno a un input tipo hidden el ID de la lista de precio a modificar para poder enviarlo en el request
                $("#idDetalleListaPrecio").val(idDetalleListaPrecio);
                //Asigno los valores a los inputs
                $('#nombreProducto').text('Producto: ' + nombreProducto + ', ' + variante);
                $('#precioKgMod').val(precioKg);
                $('#precioCajaMod').val(precioCaja);
            });

            $("#modalModificarPrecio").on("shown.bs.modal", function(event) {
                $('#precioKgMod').focus();
            });

            $('#modalModificarPrecio').on('hide.bs.modal', function(event) {
                $('#resultadoFormMoficiarPrecio').empty();
            })


            //======================== ENVIAR FORMULARIO MODIFICAR LISTA PRECIO ===================================

            $('#btnModificarPrecio').on('click', function(e) {
                e.preventDefault();
                var id = $('#idDetalleListaPrecio').val();
                $.ajax({
                    url: "/modificar-precio-producto/" + id,
                    method: "PUT",
                    //Envio los datos del formulario
                    data: $('#formModificarPrecio').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoFormMoficiarPrecio').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });
                            tablaListaPrecios.ajax.reload();
                            $('#modalModificarPrecio').trigger('click');
                        }
                    }
                })
            });


            //======================= FUNCION PARA MODIFICAR EL PRECIO =======================================

            //======================== ELIMINAR PRODUCTO ===================================
            $(document).on('click', '.btnEliminarPrecio', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea eliminar el precio de este producto?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post('/eliminar-precio-producto/' + id, {
                                    _method: "delete"
                                })
                                .then(respuesta => {
                                    tablaListaPrecios.ajax.reload();
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: "Precio eliminado",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                    }
                });
            });

            //================================ DATATABLE =======================================
            tablaListaPrecios = $('#tablaListaPrecios').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: true,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "/dtDetalleListaPrecios/" + idListaPrecio,
                columns: [{
                        data: 'nombreProducto',
                    },
                    {
                        class: 'variante',
                        data: 'variante',
                    },
                    {
                        data: 'precioKg',
                        render: function(precioKg) {
                            return formatear.format(precioKg);
                        }
                    },
                    {
                        data: 'precioCaja',
                        render: function(precioCaja) {
                            return formatear.format(precioCaja);
                        }
                    },
                    {
                        data: 'opciones',
                    },
                    {
                        data: 'created_at',
                        visible: false,
                    },
                ],
                order: [5, 'desc'],

            });

            //NOTE Funcion Cargar Variantes 

            function cargarVariantes(id) {
                $.get("/obtenerVariantes/" + id,
                    function(data) {
                        let opcionesVariantes = '';
                        let tipoProducto = '';
                        for (let i = 0; i < data.length; i++) {
                            const variante = data[i];
                            tipoProducto = saberTipoProducto(variante.tipo);
                            opcionesVariantes += '<option value="' + variante.id + '">' + tipoProducto +
                                ', ' + variante.kg +
                                ' kg(s)</option>'
                        }
                        $('#variantes').html(opcionesVariantes);
                    },
                    "json"
                );
            }

            //NOTE Funcion saber tipoProducto 
            function saberTipoProducto(tipo) {
                switch (tipo) {
                    case "0":
                        return 'Caja'
                        break;
                    case "1":
                        return 'Bolsa'
                        break;
                    default:
                        break;
                }
            }
            //Funcion para formatear numeros a formato de dinero
            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });
        });
    </script>
@stop
