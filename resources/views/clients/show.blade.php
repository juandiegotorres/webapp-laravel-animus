@extends('adminlte::page')

@section('title', 'Cliente')

@section('content_header')
@stop

@section('content')

    <div class="col-md-9 pt-3">
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title"><strong>Cliente:</strong> {{ $client->nombreCompleto }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row mb-2">
                    <div class="col-md-4">
                        <strong><i class="fas fa-id-card mr-2"></i>DNI</strong>

                        <p class="text-muted mt-1">
                            {{ $client->dni == null ? 'No especificado' : $client->dni }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-4 offset-2">
                        <strong><i class="fas fa-balance-scale mr-2"></i>CUIT</strong>

                        <p class="text-muted mt-1">
                            {{ $client->cuit == null ? 'No especificado' : $client->cuit }}
                        </p>

                        <hr>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-4">
                        <strong><i class="fas fa-map-marked-alt mr-2"></i>Dirección</strong>

                        <p class="text-muted mt-1">
                            {{ $client->direccion == null ? 'No especificado' : $client->direccion }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-4 offset-2">
                        <strong><i class="fas fa-phone-alt mr-2"></i>Teléfono</strong>

                        <p class="text-muted mt-1">
                            {{ $client->telefono == null ? 'No especificado' : $client->telefono }}
                        </p>

                        <hr>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-4">
                        <strong><i class="fas fa-map-marker-alt mr-2"></i> Provincia y Localidad</strong>

                        <p class="text-muted mt-1">
                            {{ $provinciaLocalidad->first()->provincia . ', ' . $provinciaLocalidad->first()->localidad }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-4 offset-2">
                        <strong><i class="fas fa-truck mr-2"></i>Transporte</strong>

                        <p class="text-muted mt-1">
                            {{ $client->transporte }}
                        </p>

                        <hr>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-4">
                        <strong><i class="fas fa-envelope mr-2"></i>Email</strong>

                        <p class="text-muted mt-1">
                            {{ $client->email == null ? 'No especificado' : $client->email }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-4 offset-2">
                        <strong id="" class="cbu"> <i class="fas fa-dollar-sign mr-2"></i>CBU</strong>

                        <p class="text-muted mt-1 d-flex" id='cbu'>
                            {{ $client->cbu == null ? 'No especificado' : $client->cbu }}
                            {!! $client->cbu == null ? '' : '<a class="btn btn-outline-dark btn-sm ml-3" id="copiar" data-toggle="tooltip" data-placement="top" title="Copiar"> <i class="fas fa-copy"></i> </a>' !!}
                        </p>

                        <hr>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-4">
                        <strong><i class="fas fa-tag"></i> Lista de precios</strong>

                        <p class="text-muted mt-1">
                            {{ $listaPrecio->first()->nombre }}
                        </p>

                        <hr>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ route('clients.index') }}" class="btn btn-danger mr-2">Volver</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('js')
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip()

            $('#copiar').click(function(e) {
                e.preventDefault();
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val($('#cbu').text().replace(/ /g, '')).select();
                document.execCommand("copy");
                $temp.remove();
                $(this).attr('data-original-title', "¡Copiado!").tooltip('show');
            });

            $('#copiar').mouseleave(function() {
                $(this).attr('data-original-title', "Copiar");
            });

        })
    </script>

@stop
