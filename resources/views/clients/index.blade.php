@extends('adminlte::page')

@section('plugins.Sweetalert2', true)

@section('title', 'Clientes')

@section('content_header')
    @can('clients.create')
        <a href="{{ route('clients.create') }}" class="float-right pr-2">
            <button class="btn btn-success bg-hfrut">
                Agregar nuevo cliente
            </button>
        </a>
    @endcan
    <p class="pl-2 h2">Clientes</p>
@stop


@section('content')

    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: 'success',
                        title: '{{ session('status') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    <div class="card" id='app'>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-bordered" id="clientes">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope=" col">
                                    Nombre</th>
                                <th scope="col">CUIT / CUIL</th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Email</th>
                                {{-- <th scope="col">Localidad</th>
                        <th scope="col">Cuil</th> --}}
                                <th scope="col" style="width:15px">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($clients as $client)
                                <tr>
                                    <td>{{ $client->nombreCompleto }}</td>
                                    <td>{{ $client->cuit }}</td>
                                    <td>{{ $client->telefono }}</td>
                                    <td>{{ $client->email }}</td>

                                    <td>
                                        <div class="d-flex justify-content-end">
                                            @can('clients.show')
                                                <a href="{{ route('clients.show', ['client' => $client->id]) }}">
                                                    <button class="btn btn-success btn-sm mr-2">
                                                        <i class="fa fa-info-circle"></i>
                                                    </button>
                                                </a>
                                            @endcan
                                            @can('clients.edit')
                                                <a href="{{ route('clients.edit', ['client' => $client->id]) }}">
                                                    <button class="btn btn-primary btn-sm mr-2">
                                                        <i class="fa fa-pen"></i>
                                                    </button>
                                                </a>
                                            @endcan
                                            @can('clients.delete')
                                                <dar-baja id="{{ $client->id }}" ruta="clientes" entidad="cliente">
                                                </dar-baja>

                                            @endcan
                                            {{-- <a class="btn btn-danger" id="borrar"><i class="fa fa-trash"></i></a> --}}

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>



@endsection

@section('js')
    <script>
        $(document).ready(function() {

            $('#clientes').DataTable({
                responsive: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
            });


        });
    </script>

    {{-- <script>
    Swal.fire(
        'Good job!',
        'You clicked the button!',
        'success'
    )
</script> --}}
@stop
