@extends('adminlte::page')

@section('title', 'Nuevo Cliente')

@section('plugins.Select2', true)

@section('css')
    {{-- Tuve que importar axios desde el cdn porque al cargarlo localmente da problemas con el plugin de select2 --}}
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
@stop

@section('content_header')
    <p class="pl-4 h2">Agregar nuevo cliente</p>
    @php
    $valoresViejos = old();
    @endphp
@stop

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10">

                <div class="card card-primary border-top">
                    {!! Form::open(['route' => 'clients.store', 'class' => 'p-4']) !!}
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="radio" name="personaEmpresa" id="chbPersona"
                                    value="persona" checked>
                                <label class="form-check-label" for="persona">
                                    <b>Persona</b>
                                </label>
                            </div>
                            <hr class="m-0">
                            <div class="row mt-2">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('nombre', 'Nombre (*)') !!}
                                        {!! Form::text('nombre', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Nombre...', 'id' => 'nombre']) !!}

                                        @error('nombre')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('apellido', 'Apellido (*)') !!}
                                        {!! Form::text('apellido', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Apellido...', 'id' => 'apellido']) !!}

                                        @error('apellido')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-4">
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="radio" name="personaEmpresa" id="chbEmpresa"
                                    value="empresa">
                                <label class="form-check-label" for="empresa">
                                    <b>Empresa</b>
                                </label>
                            </div>
                            <hr class="m-0">
                            <div class="form-group col-md-6 pl-0 mt-2">
                                {!! Form::label('razonSocial', 'Razón Social (*)', ['class' => 'text-muted']) !!}
                                {!! Form::text('razonSocial', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Razón social...', 'disabled', 'id' => 'razonSocial']) !!}

                                @error('razonSocial')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('cuit', 'CUIT / CUIL') !!}
                                {!! Form::number('cuit', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'CUIT...']) !!}
                                @error('cuit')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('dni', 'DNI') !!}
                                {!! Form::number('dni', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'DNI...']) !!}
                                @error('dni')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('telefono', 'Teléfono') !!}
                                {!! Form::number('telefono', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Teléfono...', 'onkeydown' => '(event.keyCode !== 69 || event.keyCode !== 189) ? return false : return true']) !!}
                                @error('telefono')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('email', 'Email') !!}
                                {!! Form::text('email', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Email...']) !!}
                                @error('email')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('cbu', 'CBU') !!}
                                {!! Form::number('cbu', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'CBU...']) !!}
                                @error('cbu')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('direccion', 'Dirección') !!}
                                {!! Form::text('direccion', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Dirección...']) !!}
                                @error('direccion')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('transporte', 'Transporte') !!}
                                {!! Form::text('transporte', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Transporte...']) !!}
                                @error('transporte')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('price_list_id', 'Lista de precios (*)') !!}
                                {!! Form::select('price_list_id', $listasPrecios->pluck('nombre', 'id'), 14, ['id' => 'listasPrecios', 'class' => 's-example-basic-single js-states form-control', 'placeholder' => 'Seleccione la lista de precios']) !!}
                                @error('price_list_id')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('provincia', 'Provincia (*)') !!}
                                {!! Form::select('provincia', $provincias->pluck('provincia', 'id'), old('provincia'), ['id' => 'provincias', 'class' => 's-example-basic-single js-states form-control', 'placeholder' => 'Seleccione la provincia']) !!}
                                @error('provincia')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('idLocalidad', 'Localidad (*)') !!}
                                {{-- , 'placeholder' => 'Seleccione la provincia para cargar las localidades' --}}
                                {!! Form::select('idLocalidad', $localidadesMza->pluck('localidad', 'id'), old('idLocalidad'), ['id' => 'idLocalidad', 'class' => 'form-control']) !!}
                                @error('idLocalidad')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <a href="{{ route('clients.index') }}" class="btn btn-danger mr-2">
                                Cancelar
                            </a>
                            {!! Form::submit('Agregar cliente', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        $(document).ready(function() {
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            const valoresViejos = {!! json_encode($valoresViejos) !!};
            const errores = {!! json_encode($errors->toArray()) !!};

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            if (valoresViejos.personaEmpresa == 'empresa') {
                habilitarEmpresa();
            } else {
                habilitarPersona();
            }

            if (valoresViejos.length != 0) {

                //Traigo nuevamente todas las localidades de la provincia que estaba seleccioanda antes de que se enviara el formulario y fallara
                axios.post(`/localidades/${valoresViejos.provincia}`).then((r) => {
                    var localidades = r.data;
                    for (var i = 0; i < localidades.length; i++) {
                        //Si el id de la localidad en la posicion i conince con el valor de la localidad seleccionada anteriormente le asigno el atributo selected
                        var seleccionada = localidades[i].id == valoresViejos.idLocalidad ? 'selected' : '';
                        $('#idLocalidad').append('<option value="' + localidades[i].id + '" ' +
                            seleccionada + ' >' +
                            localidades[i].localidad +
                            '</option>');
                    }

                });
                //Pregunto si la variable tipoProveedor existe
                if (valoresViejos.tipoProveedor) {
                    //Recorro con un foreach
                    valoresViejos.tipoProveedor.forEach((tipo) => {
                        //Los vuelvo a chequear
                        $('#' + tipo).attr('checked', true);
                    });
                }
            }

            function habilitarPersona() {
                $('#chbPersona').attr('checked', true);
                $('#chbEmpresa').attr('checked', false);
                $('#nombre').attr('disabled', false);
                $('#nombre').prev().removeClass('text-muted');
                $('#apellido').attr('disabled', false);
                $('#apellido').prev().removeClass('text-muted');
                $('#razonSocial').attr('disabled', true);
                $('#razonSocial').prev().addClass('text-muted');
            }

            function habilitarEmpresa() {
                $('#chbPersona').attr('checked', false);
                $('#chbEmpresa').attr('checked', true);
                $('#razonSocial').attr('disabled', false);
                $('#razonSocial').prev().removeClass('text-muted');
                $('#nombre').attr('disabled', true);
                $('#nombre').prev().addClass('text-muted');
                $('#apellido').attr('disabled', true);
                $('#apellido').prev().addClass('text-muted');
            }

            $('#provincias').select2();
            $('#idLocalidad').select2();
            $('#provincias').change(function() {
                $('#idLocalidad').empty();
                var id_provincia = $('#provincias').val();
                axios.post(`/localidades/${id_provincia}`).then((r) => {
                    var localidades = r.data;
                    for (var i = 0; i < localidades.length; i++) {
                        $('#idLocalidad').append('<option value=' + localidades[i].id + '>' +
                            localidades[i].localidad +
                            '</option>');
                    }
                    //Si la provincia elegida es mendoza (id=14) automaticamente selecciono Gran Alvear (id=1443)
                    //para ahorrarle tiempo al usuario

                    if (id_provincia == 14) {
                        $('#idLocalidad').val(1443);
                    }
                });
            });

            $('#chbPersona').change(function(e) {
                e.preventDefault();
                if ($(this).prop('checked')) {
                    console.log('chequeado');
                    $('#nombre').attr('disabled', false);
                    $('#nombre').prev().removeClass('text-muted');
                    $('#apellido').attr('disabled', false);
                    $('#apellido').prev().removeClass('text-muted');
                    $('#razonSocial').attr('disabled', true);
                    $('#razonSocial').prev().addClass('text-muted');

                }
            });

            $('#chbEmpresa').change(function(e) {
                e.preventDefault();
                if ($(this).prop('checked')) {
                    $('#razonSocial').attr('disabled', false);
                    $('#razonSocial').prev().removeClass('text-muted');
                    $('#nombre').attr('disabled', true);
                    $('#nombre').prev().addClass('text-muted');
                    $('#apellido').attr('disabled', true);
                    $('#apellido').prev().addClass('text-muted');
                }
            });

        });
    </script>

@stop
