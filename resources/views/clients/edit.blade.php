@extends('adminlte::page')

@section('title', 'Nuevo Cliente')

@section('content_header')
    <p class="pl-4 h2">Modificar cliente</p>
@stop

@section('content')
    @if (session('status'))
        <script>
            Swal.fire({
                icon: 'success',
                title: '{{ session('status') }}',
            })
        </script>
    @endif
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10">

                <div class="card card-primary border-top">
                    {{-- clients.store --}}
                    {!! Form::model($client, ['route' => ['clients.update', $client], 'method' => 'put', 'id' => 'my-form', 'class' => 'p-4']) !!}
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="radio" name="personaEmpresa" id="chbPersona"
                                    value="persona">
                                <label class="form-check-label" for="persona">
                                    <b>Persona</b>
                                </label>
                            </div>
                            <hr class="m-0">
                            <div class="row mt-2">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('nombre', 'Nombre (*)') !!}
                                        {!! Form::text('nombre', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Nombre...', 'id' => 'nombre']) !!}

                                        @error('nombre')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('apellido', 'Apellido (*)') !!}
                                        {!! Form::text('apellido', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Apellido...', 'id' => 'apellido']) !!}

                                        @error('apellido')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-4">
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="radio" name="personaEmpresa" id="chbEmpresa"
                                    value="empresa">
                                <label class="form-check-label" for="empresa">
                                    <b>Empresa</b>
                                </label>
                            </div>
                            <hr class="m-0">
                            <div class="form-group col-md-6 pl-0 mt-2">
                                {!! Form::label('razonSocial', 'Razón Social (*)', ['class' => 'text-muted']) !!}
                                {!! Form::text('razonSocial', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Razón social...', 'id' => 'razonSocial']) !!}
                                @error('razonSocial')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('cuit', 'CUIT') !!}
                                {!! Form::number('cuit', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'CUIT...']) !!}
                                @error('cuit')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('dni', 'DNI') !!}
                                {!! Form::number('dni', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'DNI...']) !!}
                                @error('dni')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('telefono', 'Teléfono') !!}
                                {!! Form::number('telefono', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Teléfono...']) !!}
                                @error('telefono')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('email', 'Email') !!}
                                {!! Form::text('email', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Email...']) !!}
                                @error('email')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('cbu', 'CBU') !!}
                                {!! Form::number('cbu', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'CBU...']) !!}
                                @error('cbu')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('direccion', 'Dirección') !!}
                                {!! Form::text('direccion', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Dirección...']) !!}
                                @error('direccion')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('transporte', 'Transporte') !!}
                                @if ($client->transporte == 'No definido')
                                    {!! Form::text('transporte', '', ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Transporte...']) !!}
                                @else
                                    {!! Form::text('transporte', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Transporte...']) !!}
                                @endif
                                @error('transporte')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>




                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('price_list_id', 'Lista de precios (*)') !!}
                                {!! Form::select('price_list_id', $listasPrecios->pluck('nombre', 'id'), $client->price_list_id, ['id' => 'listasPrecios', 'class' => 's-example-basic-single js-states form-control', 'placeholder' => 'Seleccione la lista de precios']) !!}
                                @error('price_list_id')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('provincia', 'Provincias (*)') !!}
                                {!! Form::select('provincia', $provincias->pluck('provincia', 'id'), $localidades->first()->id_provincia, ['id' => 'provincias', 'class' => 'form-control', 'placeholder' => 'Seleccione la provincia']) !!}
                                @error('provincia')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('idLocalidad', 'Localidades (*)') !!}
                                {{-- 'placeholder' => 'Seleccione la provincia para poder elegir la localidad' --}}
                                {!! Form::select('idLocalidad', $localidades->pluck('localidad', 'id'), $client->idLocalidad, ['id' => 'idLocalidad', 'class' => 'form-control']) !!}
                                @error('idLocalidad')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                    </div>


                    <div class="row mt-3">
                        <div class="col-md-12">
                            <a href="{{ route('clients.index') }}" class="btn btn-danger mr-2">Cancelar</a>
                            {!! Form::submit('Modificar cliente', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            const errores = {!! json_encode($errors->toArray()) !!};
            const cliente = @json($client);
            if (errores.length == 0) {
                if (cliente.razonSocial) {
                    habilitarEmpresa();
                } else {
                    habilitarPersona();
                }
            } else {
                if (errores.nombre || errores.apellido) {
                    habilitarPersona();
                } else if (errores.razonSocial) {
                    habilitarEmpresa();
                }
            }

            function habilitarPersona() {
                $('#chbPersona').attr('checked', true);
                $('#chbEmpresa').attr('checked', false);
                $('#nombre').attr('disabled', false);
                $('#nombre').prev().removeClass('text-muted');
                $('#apellido').attr('disabled', false);
                $('#apellido').prev().removeClass('text-muted');
                $('#razonSocial').attr('disabled', true);
                $('#razonSocial').prev().addClass('text-muted');
            }

            function habilitarEmpresa() {
                $('#chbPersona').attr('checked', false);
                $('#chbEmpresa').attr('checked', true);
                $('#razonSocial').attr('disabled', false);
                $('#razonSocial').prev().removeClass('text-muted');
                $('#nombre').attr('disabled', true);
                $('#nombre').prev().addClass('text-muted');
                $('#apellido').attr('disabled', true);
                $('#apellido').prev().addClass('text-muted');
            }
            //Esta linea de codigo sirve para pasar una variable de blade a jquery y convertirla a JSON, en este caso estoy pasando 
            //la variable $localidades donde esta almacenado el id de la localidad del cliente para poder setear el valor en el selected
            //donde se muestran las localidades


            // var localidadActual = {!! json_encode($localidades->toArray(), JSON_HEX_TAG) !!};

            //Paso por parametros el id de la provincia para poder cargar las localidades en el select
            // obtenerLocalidades(localidadActual[0].id_provincia);

            //Seteo el valor en el select de localidades
            // console.log(localidadActual[0].localidad)
            // $('#idLocalidad').val(1558)

            //Cuando en el select de pronvincias se cambia el valor, cargo las localidades de la respectiva provinvcia
            $('#provincias').change(function() {
                $('#idLocalidad').empty();
                var id_provincia = $('#provincias').val();
                obtenerLocalidades(id_provincia);
            });

            // $('#probando').click(function() {
            //     $('#idLocalidad').val(1558);
            // })

            // function localidadElegida() {

            // }

            //Funcion que obtiene las localidades de determinada provincia desde la base de datos y llena el select de localidades
            function obtenerLocalidades(idProv) {
                axios.post(`/localidades/${idProv}`).then((r) => {
                    var localidades = r.data;
                    for (var i = 0; i < localidades.length; i++) {
                        $('#idLocalidad').append('<option value=' + localidades[i].id + '>' +
                            localidades[i].localidad +
                            '</option>');
                    }
                });

            }

            // console.log(errores);
            $('#chbPersona').change(function(e) {
                e.preventDefault();
                if ($(this).prop('checked')) {
                    console.log('chequeado');
                    $('#nombre').attr('disabled', false);
                    $('#nombre').prev().removeClass('text-muted');
                    $('#apellido').attr('disabled', false);
                    $('#apellido').prev().removeClass('text-muted');
                    $('#razonSocial').attr('disabled', true);
                    $('#razonSocial').prev().addClass('text-muted');

                }
            });

            $('#chbEmpresa').change(function(e) {
                e.preventDefault();
                if ($(this).prop('checked')) {
                    $('#razonSocial').attr('disabled', false);
                    $('#razonSocial').prev().removeClass('text-muted');
                    $('#nombre').attr('disabled', true);
                    $('#nombre').prev().addClass('text-muted');
                    $('#apellido').attr('disabled', true);
                    $('#apellido').prev().addClass('text-muted');
                }
            });
        });
    </script>
@stop
