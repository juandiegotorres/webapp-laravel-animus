@extends('adminlte::page')

@section('title', 'Dashboard')

{{-- @section('css')
    <style>
    </style>
@endsection --}}

@section('content_header')
    @if ($preciosProductos->count() != 0)
        <div class="px-3">
            <div class="row mb-1">
                <h1>Vender</h1>
            </div>
            <div class="row d-flex justify-content-between">
                <h5>Cliente: <span class="text-muted">{{ $cliente->nombreCompleto }}
                        - CUIT: {{ $cliente->cuit }}
                    </span>
                </h5>
                <div class="col-md-3">
                    <input type="date" class="form-control" id="fechaElegida" value="{{ now()->format('Y-m-d') }}"
                        max="{{ now()->format('Y-m-d') }}" min="{{ now()->subDays(7)->format('Y-m-d') }}">
                </div>
            </div>
        </div>
    @endif

@stop

@section('content')
    @if (!empty(session()->get('status')))
        @push('js')
            <script>
                window.location.hash = '#';
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        type: '{{ session('status.type') }}',
                        title: 'Oops...',
                        text: '{{ session('status.message') }}',
                    })
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    @if ($preciosProductos->count() == 0)
        <div class="row justify-content-center mt-4">
            <h4>No hay ningun producto agregado, intente <a href="{{ route('products.index') }}">agregando uno</a></h4>
        </div>
    @else
        <div class="row justify-content-between">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="producto" class="col-form-label">Productos</label>
                    {{-- LLeno el select con los productos db --}}
                    <select name="producto" class="form-control" id="productos">
                        @foreach ($preciosProductos as $precioProducto)
                            @if ($precioProducto->estaBorrado != 1)
                                <option value="{{ $precioProducto->idVariante }}"
                                    {{ old('producto') == $precioProducto->idVariante ? 'selected' : '' }}
                                    data-precioun="{{ $precioProducto->precioKg == null ? 0 : $precioProducto->precioKg }}"
                                    data-preciocaja="{{ $precioProducto->precioCaja == null ? 0 : $precioProducto->precioCaja }}">

                                    @php
                                        switch ($precioProducto->tipo) {
                                            case '0':
                                                $tipoProducto = 'Caja, ' . $precioProducto->kg . ' kg(s)';
                                                break;
                                            case '1':
                                                $tipoProducto = 'Bolsa, ' . $precioProducto->kg . ' kg(s)';
                                                break;
                                            default:
                                                break;
                                        }
                                    @endphp

                                    @if ($precioProducto->precioKg == null || $precioProducto->precioCaja == null)
                                        {{ $precioProducto->nombreCompleto . ' - ' . $tipoProducto . ' (𝙋𝙧𝙚𝙘𝙞𝙤 𝙣𝙤 𝙖𝙨𝙞𝙜𝙣𝙖𝙙𝙤)' }}
                                    @else
                                        {{ $precioProducto->nombreCompleto . ' - ' . $tipoProducto }}
                                    @endif
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                {{-- <div class="form-group d-flex align-items-center mt-3 ml-2">
                <button class="btn btn-success mb-1"><i class="fa fa-plus"></i></button>
            </div> --}}
            </div>
            <div class="col-md-2">
                <div class="form-group flex-fill">
                    <label for="cantidad" class="col-form-label">Cantidad</label>
                    <input type="number" name="cantidad" class="form-control " id="cantidad">
                    <span class="invalid-feedback" role="alert" id="error-cantidad">
                        <strong>Error</strong>
                    </span>
                </div>
            </div>
            <div class="col-md-2 d-flex">
                <div class="form-group flex-fill">
                    <label for="precio" class="col-form-label">Precio</label>
                    <select name="precio" class="form-control" id="precio">
                        <option value="0">Precio unitario</option>
                        <option value="1">Precio por caja</option>
                        <option value="2">Precio personalizado</option>
                    </select>
                    <small class="text-muted"><b id="infoPrecio">Precio:
                            ${{ number_format($preciosProductos->first()->precioKg, 2, ',', '.') }}</b></small>
                    <span class="invalid-feedback" role="alert" id="error-precio">
                        <strong>Error</strong>
                    </span>
                </div>

            </div>
            <div class="col-md-4 d-flex">
                <div class="form-group flex-fill">
                    <label for="totalParcial" class="col-form-label">Total Parcial</label>
                    <input type="number" name="totalParcial" class="form-control" readonly id="totalParcial">
                    <span class="invalid-feedback" role="alert" id="error-totalParcial">
                        <strong>Cantidad no puede estar vacio</strong>
                    </span>
                </div>

                <div class="form-group d-flex align-items-center mt-3 ml-2">
                    <button class="btn btn-success bg-hfrut d-block mb-1" id="agregar">Agregar</button>
                </div>
            </div>
            <hr>
        </div>
        <div class="col-12 px-0">
            <form action="" method="POST" id="formVenta">
                {{-- ID del cliente al que se le va a realizar la venta --}}
                <input type="hidden" name="idCliente" value={{ $cliente->id }}>
                <table class="table table-striped" id="tablaVenta">
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                            <th>Subtotal</th>
                            <th style="width:10px">Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class='text-center' id='vacio'>
                            <td colspan="5">Agregue un producto para vender</td>
                        </tr>
                    </tbody>
                </table>

                <hr>
                <div class="row justify-content-end px-3 ">
                    <div class="form-group pr-2">
                        <input type="number" name="iva" class="form-control text-right" placeholder="IVA" id="iva">
                        <span class="invalid-feedback w-100 text-right" role="alert" id="error-iva">
                            <strong>Error</strong>
                        </span>
                    </div>
                </div>
                <hr class="m-0">
                <div class="row justify-content-between px-3 mt-3">
                    <button class="btn btn-warning" id="finalizarVenta">
                        Finalizar venta
                    </button>

                    <div class="text-right col-md-3">
                        <h3 id="totalVenta">Total $0.00</h3>
                    </div>
                    {{-- Total de la compra --}}
                    <input type="hidden" name='total' id="total">
                </div>
            </form>
        </div>
        {{-- MODAL CAMBIAR PRECIO --}}
        <div class="modal" tabindex="-1" id="modalCambiarPrecio">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Precio personalizado</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Precio</label>
                                <input type="number" name="" id="precioPersonalizado" class="form-control"
                                    aria-describedby="helpId" required>
                                <span class="invalid-feedback" role="alert" id="error-precio-personalizado">
                                    <strong>Error</strong>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-success" id="guardarPrecioPersonalizado">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    @endif


@stop


@section('js')
    <script>
        $(document).ready(function() {
            $('.sr-only').trigger('click');
            //======== SELECT2===========================
            $('#productos').select2();
            //Hacer focus en la barra de busqueda cuando se abre el select de bancos
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });


            //Guarda la lista de productos y su respectivo precios en la lista de precios
            const preciosProductos = @json($preciosProductos);
            //Array que contiene dos variables => Precio unitario, precio por caja
            var precioProductoElegido;
            //Variable que va a contener el valor del precio elegido. El valor del precio unitario, el valor del precio por caja
            //O El valor personalizado que se introdujo en el modal
            var precioElegido = 0;
            //Defino una variable donde se va a almacenar el total de todas las compras y un
            var total = 0;
            var contador = 0;

            //Asingo el precio del producto elegido al primer registro del array de datos, ya que los necesito para
            //poder llenar el label que esta debajo del select de precios. Luego cuando se cambie de producto, se actualizaran los valores
            precioProductoElegido = {
                'precioUnidad': preciosProductos[0].precioKg,
                'precioCaja': preciosProductos[0].precioCaja
            };
            precioElegido = preciosProductos[0].precioKg;


            //============================== FUNCIONES ==============================
            //Funcion para guardar un precio cuando se aprieta el boton guardar en el modal o se aprieta la tecla enter
            function guardarPrecio() {
                if (!$('#precioPersonalizado').val()) {
                    $('#error-precio-personalizado').addClass('d-block');
                    $('#error-precio-personalizado').children().text('Debe agregar un precio');
                } else {
                    precioElegido = $('#precioPersonalizado').val();
                    calcularPrecio();
                    $('#modalCambiarPrecio').trigger('click');
                    $('#infoPrecio').text('Precio: ' + formatear.format(precioElegido));
                }
            }

            //Funcion para comprobar campos vacios
            function comprobarCampos(campo) {
                //Primero defino una variable con la cantidad de caracteres del campo pero le saco los espacios
                //para verificar que el usuario no haya introducido espacios y este contando como caracteres
                var length = $.trim($('#' + campo).val()).length;

                //Si length es igual a 0 significa que el campo esta vacio
                if (length == 0) {
                    //Hago aparecer la alerta de error y le asigno el mensaje de error
                    $('#error-' + campo).addClass('d-block');
                    $('#error-' + campo).children().text('El campo ' + campo + ' no puede estar vacio');
                    //Retorno falso para que no se continue con el procedimiento de compra
                    return false;
                    //Ahora compruebo que no se haya introducido un numero negativo
                } else if ($('#' + campo).val() < 0) {
                    //Si se introdujo un numero negativo hago aparecer el mensaje de error
                    $('#error-' + campo).addClass('d-block');
                    $('#error-' + campo).children().text(campo + ' no puede ser menor que 0');
                    //Retorno falso para no continuar con el procedimiento de compra
                    return false
                }
                //Si todo esta ok se retorna verdadero y se continua con la compra
                return true;
            }


            function calcularPrecio() {
                var totalParcial = ($('#cantidad').val() * precioElegido);
                $('#totalParcial').val(totalParcial);
            }

            //==================== EVENTOS MODAL ======================================
            $("#modalCambiarPrecio").on("shown.bs.modal", function(event) {
                $('#precioPersonalizado').focus();
            });
            $("#modalCambiarPrecio").on("hide.bs.modal", function(event) {
                $('#precioPersonalizado').val('');
                $('#error-precio-personalizado').removeClass('d-block');
            });


            //=============== EVENTO BOTON GUARDAR DENTRO DEL MODAL ====================
            $('#guardarPrecioPersonalizado').click(function(e) {
                e.preventDefault();
                guardarPrecio();
            });

            //=============== EVENTO SI SE PRESIONA TECLA ENTER DENTRO DEL INPUT DEL MODAL ================
            $('#precioPersonalizado').keyup(function(e) {
                if (e.keyCode == 13) {
                    guardarPrecio();
                }
            });

            //============== EVENTO CAMBIA DE VALOR EL SELECT DE PRODUCTOS =====================
            $('#productos').change(function(e) {
                e.preventDefault();

                //Guardo los precios en una variable
                precioProductoElegido = {
                    'precioUnidad': $(this).find(':selected').data('precioun'),
                    'precioCaja': $(this).find(':selected').data('preciocaja')
                };


                //Vuelvo a poner el valor del select precios en la primera opcion
                $('#precio').val(0);

                $('#cantidad').val('');

                precioElegido = precioProductoElegido.precioUnidad;

                //Pongo el valor del producto en la etiqueta
                $('#infoPrecio').text('Precio: ' + formatear.format(precioProductoElegido.precioUnidad));

                // console.log($("#productos").prop('selectedIndex'));
                $('#cantidad').focus();

            });

            //============== EVENTO CAMBIA DE VALOR EL SELECT DE PRECIOS =====================
            $('#precio').on('change', function() {
                // Si el valor elegido es 0, significa que es precio unitario por lo que lo indico en el label 
                //y lo guardo en la variable precio elegido

                // Si el valor elegido es 1, significa que es precio por caja por lo que lo indico en el label 
                //y lo guardo en la variable precio elegido

                // Si el valor elegido es 2, significa que es precio personalizado por lo que abro el modal para
                //que introduzca el precio
                switch ($(this).val()) {
                    case '0':
                        $('#infoPrecio').text('Precio: ' + formatear.format(precioProductoElegido
                            .precioUnidad));
                        precioElegido = precioProductoElegido.precioUnidad;
                        calcularPrecio();
                        break;
                    case '1':
                        $('#infoPrecio').text('Precio: ' + formatear.format(precioProductoElegido
                            .precioCaja));
                        precioElegido = precioProductoElegido.precioCaja;
                        calcularPrecio();
                        break;
                    default:
                        $('#modalCambiarPrecio').modal();
                        break;
                }
            });

            $('#cantidad').keyup(function(e) {
                calcularPrecio();
                if (e.keyCode == 13) {
                    $('#agregar').trigger('click');
                }
            });

            $("#cantidad").keypress(function(e) {
                if ($(this).val().length > 5) {
                    e.preventDefault();
                }
            })

            //==============================Accion click del boton para agregar compra============================
            $('#agregar').click(function(e) {
                $('#vacio').addClass('d-none');
                //Pregunto si ambos campos me retornan verdadero, y si es asi continuo con el procedimiento de compra
                if (comprobarCampos('cantidad', 'cantidad') == true) {

                    //Borro los mensajes de error si es que existen
                    if ($('#error-cantidad').hasClass('d-block')) {
                        $('#error-cantidad').toggleClass('d-block');
                    };

                    if (precioElegido == 0 || precioElegido == null) {
                        Swal.fire(
                            'Error',
                            'El precio del producto no puede ser $0.00. Agregue un precio personalizado',
                            'error'
                        )
                    } else {

                        //Guardo todos los valores de los inputs anteriores en una respectiva variable para mantener
                        //el orden en el proceso siguiente
                        var nombreProducto = $('#productos option:selected').text();
                        var idVariante = $('#productos').val();
                        var cantidad = $('#cantidad').val();
                        // var precioUnitario = $('#precio').val();
                        var totalParcial = $('#totalParcial').val();
                        //Si el nombre del producto tiene la cadena "precio no asignado" lo cambio por un espacio vacio
                        var nombreProductoClean = nombreProducto.replace(
                            '(𝙋𝙧𝙚𝙘𝙞𝙤 𝙣𝙤 𝙖𝙨𝙞𝙜𝙣𝙖𝙙𝙤)', '');

                        //tipoPrecio => 0 = precioUnitario || 1= precioCaja || 2 = precioPersonalizado
                        var tipoPrecio = $('#precio').val();


                        //Defino una variable y la lleno con codigo HTML, este codigo representa una fila en la tabla que ya
                        //tengo creada, asigno a cada campo de la fila un inpud de tipo hidden para poder enviar los datos
                        //en el formulario y asi posteriormente guardarlos.                    
                        //Se definen los nombres de los inputs con un [] para que se puedan enviar varios datos en un solo array
                        //y luego recorrerlos en el controlador

                        row = "<tr>" +
                            //Campo materia prima en la fila, muestro el nombre pero en el input va el valor del id
                            `<td>` + nombreProductoClean +
                            `<input type='hidden' name='idVariante[]'value='` +
                            idVariante + `'></td>` +
                            //Campo cantidad en la fila
                            `<td>` + cantidad + `<input type='hidden' name='cantidad[]' value='` +
                            cantidad + `'></td>` +
                            //Campo precio unitario en la fila
                            `<td>` + formatear.format(precioElegido) +
                            `<input type='hidden' name='precioProducto[]' value='` +
                            precioElegido + `'><input type='hidden' name='tipoPrecio[]' value='` +
                            tipoPrecio + `'></td>` +
                            //Campo precio parcial en la fila
                            `<td id="subtotal">` + formatear.format(totalParcial) +
                            `<input  type='hidden' name='subtotal[]' value='` +
                            totalParcial + `'></td>` +
                            //Boton para eliminar una fila si es necesario
                            `<td id="DeleteButton"><a href="#" class="badge badge-danger" >Eliminar X</a></td>` +

                            `</tr>`;

                        //Guardo el cuerpo de la tabla en una variable
                        tableBody = $("table tbody");
                        //Aca agrego la fila que creamos anteriormente
                        tableBody.append(row);
                        //Guardo el total parcial de la compra agregada en una variable para luego hacer calculos
                        totalParcial = $('#totalParcial').val();
                        //En la variable global 'total' voy acumulando todos los totales parciales
                        //Parse float identifica las variables como numeros para poder sumarlas, de otra forma las concatena
                        total = parseFloat(total) + parseFloat(totalParcial);
                        //Asigno el valor del total al h5 para mostrarselo al usuario
                        $('#totalVenta').text('Total: ' + formatear.format(total));
                        //Asigno tambien al input de tipo hidden dentro del formulario el valor para cuando se envie
                        $('#total').val(total);
                        //Vacio los inputs
                        $('#cantidad').val('');

                        //Vuelvo a poner el select de precios en la primera opcion ||precio unitario||
                        $('#precio').val(0);
                        //para establecer el precio elegido. Busco en el array de productos y precios segun la posicion, para
                        //esto tomo la propiedad index del select de productos que me va a dar la posicion exacta. Y luego busco el precio x kg
                        precioElegido = preciosProductos[$("#productos").prop('selectedIndex')].precioKg;


                        //Pongo el valor del producto en la etiqueta
                        $('#infoPrecio').text('Precio: ' + formatear.format(precioElegido));

                        $('#totalParcial').val('');
                    };
                }

            });

            //======================== EVENTO CLICK ELIMINAR DE LA TABLA ============================
            $("#tablaVenta").on("click", "#DeleteButton", function() {
                //Obtiene el valor del total parcial de la fila que se esta borrando
                var subtotal = $(this).closest('td').siblings('td#subtotal').children().val();
                //Si la tabla tiene 3 tr, que en este caso no tiene ninguna fila, pero jq lo tomas asi
                //Significa que ya no hay nada en el "carro" por lo que vuelo a mostrar la fila que dice agregue una un producto para vender
                if ($('#tablaVenta tr').length == 3) {
                    $('#vacio').removeClass('d-none');
                }
                //Se lo resta a la variable del total a pagar
                total = parseFloat(total) - parseFloat(subtotal);
                //Actualiza el h3 donde se muestra el total
                $('#totalVenta').text('Total: ' + formatear.format(total));
                //Borra la fila
                $(this).closest("tr").remove();
            });

            $('#finalizarVenta').on('click', function(e) {
                e.preventDefault();
                if (!$('#iva').val()) {
                    invalidFeedback('#iva', '#error-iva', 'Debe especificar el IVA');
                    return
                }
                Swal.fire({
                    title: '¿Desea realizar esta venta?',
                    text: "",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si!',
                    cancelButtonText: 'No',
                }).then((result) => {
                    if (result.isConfirmed) {
                        removeFeedback('#iva', '#error-iva');
                        realizarCompra();
                    }
                })
            });

            function realizarCompra() {
                $.ajax({
                    type: "POST",
                    url: "{{ route('vender.store') }}",
                    data: $('#formVenta').serialize() +
                        "&_token={{ csrf_token() }}" +
                        "&fecha=" + $('#fechaElegida').val(),
                    dataType: "json",
                    success: function(data) {
                        if (data.error) {
                            Swal.fire(
                                data.error,
                                '',
                                'error'
                            )
                        }
                        if (data.success) {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                                timerProgressBar: true,
                            })

                            Toast.fire({
                                icon: 'success',
                                title: 'Venta realizada con éxito. Espere unos segundos'
                            }).then((result) => {
                                window.location.href =
                                    "{{ route('historial-ventas.index') }}";
                            })

                        }
                    }
                });
            }

            //==================== FUNCION ERROR DE VALIDACION ============================
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }

            //Funcion para formatear numeros a formato de dinero
            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });
        });
    </script>
@stop
