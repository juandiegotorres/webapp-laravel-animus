@extends('adminlte::page')

@section('title', 'Seleccionar Cliente')

@section('content_header')
    <h1>Seleccionar cliente para {{ session()->get('desdeCobros') == false ? 'vender' : 'cobrar' }}</h1>
@stop

@section('content')
    <div class="row row-eq-height mt-2">
        <div class="col-lg-4 col-md-6 col-sm-6 ">
            <div class="card">
                @if (session()->get('desdeOrden') == true)
                    <form action="{{ route('orden-venta.create') }}" method="post">
                    @else
                        <form action="{{ route('vender.index') }}" method="post">
                @endif
                @csrf
                <div class="card-body pb-2">
                    <div class="form-group">
                        <label for="cliente">Clientes</label>
                        <select name="cliente" id="clientes" class="form-control">

                        </select>
                    </div>
                    @if (session()->get('desdeCobros') == false)
                        <div class="form-group">
                            <label for="">Lista de precios</label>
                            <select name="listaPrecios" id="listasPrecios" class="form-control">
                                @foreach ($listasPrecios as $listaPrecios)
                                    <option value="{{ $listaPrecios->id }}"
                                        data-fecha={{ $listaPrecios->fechaVigencia }}>
                                        {{ $listaPrecios->nombre }}</option>
                                @endforeach
                            </select>
                            <small id="fechaVigencia" class="text-muted pl-1">Fecha vigencia</small>
                        </div>
                    @endif
                </div>
                <div class="col-md-12 px-3 pt-0 pb-3">
                    @if (session()->get('desdeCobros') == false)
                        <button type="submit" class="btn btn-success bg-hfrut btn-block">Comenzar la venta</button>
                    @else
                        <a href="" id="btnCobrar" class="btn btn-success bg-hfrut btn-block">Cobrar a este cliente</a>
                    @endif
                </div>
                </form>
            </div>

        </div>

        <div class="col-md-5">
            <div class="card">
                <div class="card-header bg-secondary">
                    <h5 class="mb-0" id="txtNombreCliente">Nombre cliente</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""><i class="fas fa-id-card mr-2"></i>DNI</label>
                                <input type="text" name="" id="txtDNI" class="form-control" placeholder=""
                                    aria-describedby="" readonly>
                            </div>
                            <div class="form-group">
                                <label for=""><i class="fas fa-phone-alt mr-2"></i>Teléfono</label>
                                <input type="text" name="" id="txtTelefono" class="form-control" placeholder=""
                                    aria-describedby="" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""><i class="fas fa-balance-scale mr-2"></i>CUIT</label>
                                <input type="text" name="" id="txtCuit" class="form-control" placeholder=""
                                    aria-describedby="" readonly>
                            </div>
                            <div class="form-group">
                                <label for=""><i class="fas fa-map-marker-alt mr-2"></i>Provincia, Localidad</label>
                                <input type="text" name="" id="txtProvinciaLoc" class="form-control" placeholder=""
                                    aria-describedby="" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for=""><i class="fas fa-map-marked-alt mr-2"></i>Direccion</label>
                                <input type="text" name="" id="txtDireccion" class="form-control" placeholder=""
                                    aria-describedby="" readonly>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {

            var clientes = [];

            function llenarClientes() {
                $.get("/obtener-clientes",
                    function(data) {
                        clientes = data;
                        const lista = data;
                        for (var i = 0; i < lista.length; i++) {
                            $('#clientes').append('<option value=' + lista[i].id + '>' +
                                lista[i].nombreCompleto +
                                '</option>');
                        }
                        cargarDatoCliente(lista[0]);
                        $('#btnCobrar').attr('href', '/cobros/create/' + lista[0].id);
                    }
                );
            }

            function cargarDatoCliente(cliente) {
                $('#txtNombreCliente').text(cliente.nombreCompleto);
                $('#txtDNI').val(cliente.dni);
                $('#txtCuit').val(cliente.cuit);
                $('#txtTelefono').val(cliente.telefono);
                $('#txtProvinciaLoc').val(cliente.provincia + ', ' + cliente
                    .localidad);
                cliente.direccion == null ? direccion = 'No especificado' : direccion = cliente.direccion;
                $('#txtDireccion').val(direccion);
                $('#listasPrecios').val(cliente.price_list_id);
                $('#fechaVigencia').html('Fecha de vigencia: ' + new Date(cliente.fechaVigencia).toLocaleDateString(
                    "fr-FR"));
            }

            llenarClientes();

            $('#clientes').change(function(e) {
                e.preventDefault();
                //Recorro el array de clintes que solicite al principio del documento y busco el que coincida con el id seleccionado
                //Luego lo guardo en una variable y lleno los inputs
                var clienteSeleccionado = $.grep(clientes, function(e) {
                    return e.id == $('#clientes').val();
                });

                cargarDatoCliente(clienteSeleccionado[0]);

                $('#btnCobrar').attr('href', '/cobros/create/' + clienteSeleccionado[0].id);

            });


            $('#clientes').select2();

            //Hacer focus en la barra de busqueda cuando se abre el select de bancos
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });
        });
    </script>
@stop
