@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="px-3">
        <div class="row mb-1">
            <h1>Vender</h1>
        </div>
        <div class="row d-flex justify-content-between">
            <h5>Cliente: <span class="text-muted">{{ $orden->cliente->nombreCompleto }}
                    - CUIT: {{ $orden->cliente->cuit }}
                </span>
            </h5>
            <div class="col-md-3">
                <input type="date" class="form-control" id="fechaElegida" value="{{ now()->format('Y-m-d') }}"
                    max="{{ now()->format('Y-m-d') }}" min="{{ now()->subDays(7)->format('Y-m-d') }}">
            </div>
        </div>
    </div>

@stop

@section('content')
    <div class="row mt-0">
        <div class="col-md-12">
            <div id="resultForm">

            </div>
        </div>
    </div>
    <div class="row px-3">
        <div class="col-12 px-0">
            <form action="" method="POST" id="formVenta">
                {{-- ID del cliente al que se le va a realizar la venta --}}
                <input type="hidden" name="idCliente" value={{ $orden->client_id }}>
                <input type="hidden" name="id_orden" value={{ $orden->id }}>
                <table class="table table-striped" id="tablaVenta">
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orden->detalleOrden as $detalle)
                            <tr>
                                @php
                                    $variante = $detalle->variante;
                                    switch ($variante->tipo) {
                                        case '0':
                                            $tipoProducto = 'Caja, ' . $variante->kg . ' kg(s)';
                                            break;
                                        case '1':
                                            $tipoProducto = 'Bolsa, ' . $variante->kg . ' kg(s)';
                                            break;
                                        default:
                                            break;
                                    }
                                @endphp
                                <td>{{ $detalle->variante->producto->nombreCompleto . ' - ' . $tipoProducto }}
                                    <input type="hidden" name="id_variante[]" value="{{ $detalle->id_variante }}">
                                </td>
                                <td><input type="number" name="cantidad[]" class="form-control cantidad"
                                        value="{{ $detalle->cantidad }}">
                                </td>
                                <td><input type="number" name="precio[]" class="form-control precio"
                                        value="{{ $detalle->precio }}"></td>
                                <td><input type="number" class="form-control total"
                                        value="{{ $detalle->cantidad * $detalle->precio }}" readonly></td>
                            </tr>
                        @endforeach
                        {{-- ACA PRODUCTOS --}}
                        {{-- ACA PRODUCTOS --}}
                        {{-- ACA PRODUCTOS --}}
                        {{-- ACA PRODUCTOS --}}
                        {{-- <tr class='text-center' id='vacio'>
                            <td colspan="5">Agregue un producto para vender</td>
                        </tr> --}}
                    </tbody>
                </table>

                <hr>
                <div class="row justify-content-end px-3 ">
                    <div class="form-group pr-2">
                        <input type="number" name="iva" class="form-control text-right" placeholder="IVA" id="iva">
                        <span class="invalid-feedback w-100 text-right" role="alert" id="error-iva">
                            <strong>Error</strong>
                        </span>
                    </div>
                </div>
                <hr class="m-0">
                <div class="row justify-content-between px-3 mt-3">
                    <button class="btn btn-warning" id="finalizarVenta">
                        Finalizar venta
                    </button>

                    <div class="text-right col-md-3">
                        <h3 id="totalVenta">Total $0.00</h3>
                    </div>
                    {{-- Total de la compra --}}
                    <input type="hidden" name='total' id="total">
                </div>
            </form>
        </div>
    @stop


    @section('js')
        <script>
            $(document).ready(function() {
                // $('.sr-only').trigger('click');
                //======== SELECT2===========================
                $('#productos').select2();
                //Hacer focus en la barra de busqueda cuando se abre el select de bancos
                $(document).on('select2:open', () => {
                    document.querySelector('.select2-search__field').focus();
                });


                //Funcion para comprobar campos vacios
                function comprobarCampos(campo) {
                    //Primero defino una variable con la cantidad de caracteres del campo pero le saco los espacios
                    //para verificar que el usuario no haya introducido espacios y este contando como caracteres
                    var length = $.trim($('#' + campo).val()).length;

                    //Si length es igual a 0 significa que el campo esta vacio
                    if (length == 0) {
                        //Hago aparecer la alerta de error y le asigno el mensaje de error
                        $('#error-' + campo).addClass('d-block');
                        $('#error-' + campo).children().text('El campo ' + campo + ' no puede estar vacio');
                        //Retorno falso para que no se continue con el procedimiento de compra
                        return false;
                        //Ahora compruebo que no se haya introducido un numero negativo
                    } else if ($('#' + campo).val() < 0) {
                        //Si se introdujo un numero negativo hago aparecer el mensaje de error
                        $('#error-' + campo).addClass('d-block');
                        $('#error-' + campo).children().text(campo + ' no puede ser menor que 0');
                        //Retorno falso para no continuar con el procedimiento de compra
                        return false
                    }
                    //Si todo esta ok se retorna verdadero y se continua con la compra
                    return true;
                }




                //EVENTO Finalizar Venta
                $('#finalizarVenta').on('click', function(e) {
                    e.preventDefault();
                    if (!$('#iva').val()) {
                        invalidFeedback('#iva', '#error-iva', 'Debe especificar el IVA');
                        return
                    }

                    removeFeedback('#iva', '#error-iva');

                    if (!comprobar_campos()) {
                        Swal.fire(
                            'Error',
                            'No puede haber campos vacios o con valores menores/iguales a 0',
                            'error'
                        )
                        return;
                    }

                    Swal.fire({
                        title: '¿Desea realizar esta venta?',
                        text: "",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Si!',
                        cancelButtonText: 'No',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            realizarCompra();
                        }
                    })
                });

                function realizarCompra() {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('vender-desde-orden.store') }}",
                        data: $('#formVenta').serialize() +
                            "&_token={{ csrf_token() }}" +
                            "&fecha=" + $('#fechaElegida').val(),
                        dataType: "json",
                        success: function(data) {
                            if (data.error) {
                                html =
                                    '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                                for (var i = 0; i < response.error.length; i++) {
                                    html += '<li>' + response.error[i] + '</li>';
                                }
                                html += '</ul></div>';
                                $('#resultForm').html(html);
                            }
                            if (data.success) {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-right',
                                    showConfirmButton: false,
                                    timer: 2300,
                                    timerProgressBar: true,
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: 'Venta realizada con éxito. Espere unos segundos'
                                }).then((result) => {
                                    window.location.href =
                                        "{{ route('historial-ventas.index') }}";
                                })

                            }
                        }
                    });
                }

                //EVENTO KEYUP DE CANTIDAD Y PRECIO PARA CALCULAR EL SUBTOTAL
                $('.cantidad').keyup(function(e) {
                    var cantidad = $(this).val();
                    var precio = $(this).closest('td').next('td').find('.precio').val();
                    var subtotal = cantidad * precio;
                    $(this).closest('td').next('td').next('td').find('.total').val(subtotal);
                    calc_total();
                });

                $('.precio').keyup(function(e) {
                    var precio = $(this).val();
                    var cantidad = $(this).closest('td').prev('td').find('.cantidad').val();
                    var subtotal = cantidad * precio;
                    $(this).closest('td').next('td').find('.total').val(subtotal);
                    calc_total();
                });

                //EVENTO calcular total
                function calc_total() {
                    var total = 0;
                    $(".total").each(function() {
                        if ($(this).val() == '') {
                            total += 0;
                        } else {
                            total += parseFloat($(this).val());
                        }
                    });
                    $('#totalVenta').text(formatear.format(total));
                    $('#total').val(total);
                }
                //EVENTO validar campos
                function comprobar_campos() {
                    let respuesta = true
                    $(".total").each(function(key, value) {
                        if (!$(this).val() || $(this).val() <= 0) {
                            respuesta = false;
                        }
                    });
                    return respuesta;
                }

                //==================== FUNCION ERROR DE VALIDACION ============================
                function invalidFeedback(input, labelInvalid, text) {
                    $(input).addClass('is-invalid');
                    $(labelInvalid).children().text(text);
                }
                //==================== FUNCION SACAR ERROR DE VALIDACION ============================
                //Revierte la funcion de invalidFeedback()
                function removeFeedback(input, labelInvalid) {
                    $(input).removeClass('is-invalid');
                    $(labelInvalid).children().text('');
                }

                //Funcion para formatear numeros a formato de dinero
                var formatear = new Intl.NumberFormat('de-DE', {
                    style: 'currency',
                    currency: 'USD',
                });
            });
        </script>
    @stop
