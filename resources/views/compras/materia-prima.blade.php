@extends('adminlte::page')

@section('title', 'Dashboard')
@section('plugins.Sweetalert2', true)

@section('css')
    <style>
        .content-header {
            padding-bottom: 0px !important;
        }

        hr {
            margin-top: 0.5rem !important;
            margin-bottom: 0.5rem !important;
        }

        .boton-margen {
            margin-top: 2.3rem;
            margin-right: 1rem;
            margin-left: -1rem;
        }

    </style>

@endsection

@section('content_header')
    <div class="px-4">
        <div class="row mb-1">
            <h1>Compra de materia prima</h1>
        </div>
        <div class="row d-flex justify-content-between">
            <h5>Proveedor: <span
                    class="text-muted">{{ session()->get('desdeIngreso') == false? $proveedor->first()->nombreCompleto: $rawMaterialEntry->proveedor->nombreCompleto }}
                    - CUIT:
                    {{ session()->get('desdeIngreso') == false ? $proveedor->first()->cuit : $rawMaterialEntry->proveedor->cuit }}</span>
            </h5>
            <div class="col-md-3">
                <input type="date" class="form-control" id="fechaElegida" value="{{ now()->format('Y-m-d') }}"
                    max="{{ now()->format('Y-m-d') }}" min="{{ now()->subDays(7)->format('Y-m-d') }}">
            </div>
        </div>
    </div>
    <hr>
@stop


@section('content')
    @if (!empty(session()->get('status')))
        @push('js')
            <script>
                window.location.hash = '#';
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        type: '{{ session('status.type') }}',
                        title: 'Oops...',
                        text: '{{ session('status.message') }}',
                    })
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    <div class="modal fade" id="modalMateriaPrimaAgregar" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title" id="labelInsumoServicioAgregar">Agregar nueva materia prima
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" id="formMateriaPrima">
                    @csrf
                    <div class="modal-body">
                        <span id="formResult"></span>
                        <div class="form-group">
                            <label for="fruta" class="col-form-label">Nombre de la Fruta (*)</label>
                            <input type="text" name="fruta" class="form-control" id="fruta" autocomplete="off"
                                placeholder="Fruta..." autofocus required>
                        </div>
                        <div class="form-group">
                            <label for="variedad" class="col-form-label">Variedad (*)</label>
                            <input type="text" name="variedad" class="form-control" id="variedad" autocomplete="off"
                                placeholder="Variedad..." autofocus required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row justify-content-between">
        <div class="col-md-3">
            <div class="form-group">
                <label for="materia-prima" class="col-form-label">Materia Prima</label>
                {{-- LLeno el select con las materias primas de la db --}}
                <select name="materia-prima" class="form-control" id="materia-prima">
                    @foreach ($materiasPrimas as $materiaPrima)
                        <option value="{{ $materiaPrima->id }}"
                            {{ old('materia-prima') == $materiaPrima->id ? 'selected' : '' }}>
                            {{ $materiaPrima->fruta }}, {{ $materiaPrima->variedad }}
                        </option>
                    @endforeach
                </select>

            </div>
        </div>
        <div class="col-md-1 d-flex align-items-end pb-3 pl-0">

            <a class="btn btn-success mr-0" data-toggle="modal" data-target="#modalMateriaPrimaAgregar"
                id="btnModalAgregar"> <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="cantidad" class="col-form-label">Cantidad</label>
                <input type="number" name="cantidad" class="form-control" id="cantidad">
                <span class="invalid-feedback" role="alert" id="error-cantidad">
                    <strong>Error</strong>
                </span>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="precio" class="col-form-label">Precio</label>
                <input type="number" name="precio" class="form-control" id="precio">
                <span class="invalid-feedback" role="alert" id="error-precio">
                    <strong>Error</strong>
                </span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="totalParcial" class="col-form-label">Total Parcial</label>
                <input type="number" name="totalParcial" class="form-control" readonly id="totalParcial">
                <span class="invalid-feedback" role="alert" id="error-totalParcial">
                    <strong>Cantidad no puede estar vacio</strong>
                </span>
            </div>
        </div>
        <div class="col-md-1 ">
            <div class="form-group">
                <button class="btn btn-success boton-margen" id="agregar">Agregar</button>
            </div>
        </div>
        <hr>
    </div>
    @if ($faltantesFacturar->count() > 0)
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info" role="alert">
                    <p class="mb-0">Frutas por facturar a este proveedor:</p>
                    <ul class="mb-0">
                        @foreach ($faltantesFacturar as $faltante)
                            <li>Fruta: <strong>{{ $faltante->fruta . ', ' . $faltante->variedad }}</strong> -
                                Faltante
                                por facturar: <strong>{{ $faltante->cantidadKg }} kgs</strong></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    <div class="col-12 px-0">
        <form action="" method="POST" id="formCompraMateriaPrima">
            {{-- ID del proveedor al que se le va a realizar la compra --}}
            <input type="hidden" name="idProveedor" value="{{ $proveedor->id }}">
            <table class="table table-striped" id="tablaCompras">
                <thead>
                    <tr>
                        <th>Materia Prima</th>
                        <th>Cantidad</th>
                        <th>Precio * Kg</th>
                        <th>Subtotal</th>
                        <th style="width:10px">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class='text-center' id='vacio'>
                        <td colspan="5">Agregue una materia prima</td>
                    </tr>
                </tbody>
            </table>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="chbDetalle">
                <label class="form-check-label" for="chbDetalle">
                    Agregar detalle de compra
                </label>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group mb-0 pr-2 d-none" id="detalle">
                        <label for="detalle" class="col-form-label">Detalles de la compra:</label>
                        <textarea name="detalle" id="" cols="30" rows="2" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row justify-content-end px-3 ">
                <div class="form-group pr-2">
                    <input type="number" name="iva" class="form-control text-right" placeholder="IVA" id="iva">
                    <span class="invalid-feedback w-100 text-right" role="alert" id="error-iva">
                        <strong>Error</strong>
                    </span>
                </div>
            </div>
            <hr>
            <div class="row justify-content-between px-3">
                <button class="btn btn-warning" id="finalizarCompra" type="submit">
                    Finalizar compra
                </button>
                <div class="col-md-3 text-right">
                    <h3 id="totalCompra">Total</h3>
                </div>
                {{-- Total de la compra --}}
                <input type="hidden" name='total' id="total">
            </div>
        </form>

    </div>

@stop



@section('js')

    <script>
        $(document).ready(function() {
            //Defino una variable donde se va a almacenar el total de todas las compras y un
            var total = 0;
            var contador = 0;

            $('#materia-prima').select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            //===================== AGREGAR MATERIA PRIMA==================================
            //Accion para agregar matearia-prima
            $('#formMateriaPrima').on('submit', function(event) {
                //Evito que se recargue la pagina
                event.preventDefault();
                //Envio la peticion del tipo POST a traves de ajax
                $.ajax({
                    url: "{{ route('purchase-raw-material.storeFromPurchase') }}",
                    method: 'POST',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(data) {
                        var html = '';
                        if (data.errors) {
                            //Si hay algun error en la validacion hago un elemento html con la clase alert-danger
                            //recorro los errores y los voy agregando al html. Despues los muestro
                            html =
                                '<div class = "alert alert-danger pb-0" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.errors.length; i++) {
                                html += '<li>' + data.errors[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#formResult').html(html);
                        }
                        if (data.success) {
                            //Si se agrego correctamente el insumo servicio vacio el select
                            $('#materia-prima').empty();
                            //Obtengo todos los insumos - servicios nuevamente
                            $.get("/materias-primas-json", function(data) {
                                var materiasPrimas = data;
                                //Los recorro y los voy agregando dentro del select
                                for (var i = 0; i < materiasPrimas.length; i++) {
                                    $('#materia-prima').append('<option value=' +
                                        materiasPrimas[i].id + '>' +
                                        materiasPrimas[i].fruta + ', ' +
                                        materiasPrimas[i].variedad +
                                        '</option>');
                                }
                                //Selecciono la ultima opcion (La que se agrego ultimamente)
                                $('#materia-prima option:last').attr('selected',
                                    'selected');
                                //Muestro una alerta diciendo que la operacion se completo con exito
                                Swal.fire({
                                    toast: true,
                                    icon: 'success',
                                    title: 'Materia prima agregada con éxito',
                                    position: 'top-right',
                                    showConfirmButton: false,
                                    timer: 2300,
                                });
                                //Escondo el modal
                                $('#modalMateriaPrimaAgregar').modal('hide');
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $('#fruta').val('');
                                $('#variedad').val('');
                            });
                        }
                    }
                })
            });

            //==================== EVENTOS MODAL ======================================
            $("#modalMateriaPrimaAgregar").on("shown.bs.modal", function(event) {
                $('#fruta').focus();
            });
            $("#modalMateriaPrimaAgregar").on("hide.bs.modal", function(event) {
                $('#fruta').val('');
                $('#variedad').val('');
                $('#alertaErrores').remove();
            });

            //========================== FINALIZAR COMPRA ======================================
            $('#finalizarCompra').click(function(e) {
                // var form = $(this).closest("form");
                var name = $(this).data("name");
                event.preventDefault();
                if (!$('#iva').val()) {
                    invalidFeedback('#iva', '#error-iva', 'Debe especificar el IVA');
                    return
                } else if (0 > $('#iva').val()) {
                    invalidFeedback('#iva', '#error-iva', 'Introduzca un IVA válido');
                    return
                }
                Swal.fire({
                        title: `¿Desea realizar esta compra?`,
                        text: "",
                        icon: "question",
                        showCancelButton: true,
                        confirmButtonText: 'Si',
                        confirmButtonColor: '#29643e',
                        cancelButtonText: 'No',
                    })
                    .then((response) => {
                        if (response.isConfirmed) {
                            removeFeedback('#iva', '#error-iva');
                            $.ajax({
                                type: "POST",
                                url: "{{ route('purchase-raw-material.store') }}",
                                data: $('#formCompraMateriaPrima').serialize() +
                                    "&_token={{ csrf_token() }}",
                                dataType: "json",
                                success: function(data) {
                                    if (data.error) {
                                        Swal.fire({
                                            icon: 'warning',
                                            title: 'Oops...',
                                            text: data.error,
                                        })
                                    }
                                    if (data.success) {
                                        Swal.fire({
                                            toast: true,
                                            icon: 'success',
                                            title: data.success +
                                                '. Espere unos instantes',
                                            position: 'top-right',
                                            showConfirmButton: false,
                                            timer: 2000,
                                        }).then(function() {
                                            window.location.href =
                                                "{{ route('purchase-history.index') }}";
                                        });
                                    }
                                }
                            });
                        }
                    });
            });

            //Funcion para comprobar que los campos cantidad y precio no esten vacios a la hora de agregar una compra
            //Paso como parametro el nombre del campo (cantidad o precio)
            function comprobarCampos(campo) {
                //Primero defino una variable con la cantidad de caracteres del campo pero le saco los espacios
                //para verificar que el usuario no haya introducido espacios y este contando como caracteres
                var length = $.trim($('#' + campo).val()).length;

                //Si length es igual a 0 significa que el campo esta vacio
                if (length == 0) {
                    //Hago aparecer la alerta de error y le asigno el mensaje de error
                    $('#error-' + campo).addClass('d-block');
                    $('#error-' + campo).children().text('El campo ' + campo + ' no puede estar vacio');
                    //Retorno falso para que no se continue con el procedimiento de compra
                    return false;
                    //Ahora compruebo que no se haya introducido un numero negativo
                } else if ($('#' + campo).val() < 0) {
                    //Si se introdujo un numero negativo hago aparecer el mensaje de error
                    $('#error-' + campo).addClass('d-block');
                    $('#error-' + campo).children().text(campo + ' no puede ser menor que 0');
                    //Retorno falso para no continuar con el procedimiento de compra
                    return false
                }
                //Si todo esta ok se retorna verdadero y se continua con la compra
                return true;
            }


            //Accion cliock del boton para agregar compra
            $('#agregar').click(function(e) {
                $('#vacio').addClass('d-none');
                //Pregunto si ambos campos me retornan verdadero, y si es asi continuo con el procedimiento de compra
                if (comprobarCampos('cantidad', 'cantidad') == true && comprobarCampos('precio',
                        'precio') == true) {
                    //Borro los mensajes de error si es que existen
                    if ($('#error-cantidad').hasClass('d-block')) {
                        $('#error-cantidad').toggleClass('d-block');
                    };
                    if ($('#error-precio').hasClass('d-block')) {
                        $('#error-precio').toggleClass('d-block');
                    };

                    //Guardo todos los valores de los inputs anteriores en una respectiva variable para mantener
                    //el orden en el proceso siguiente
                    var nombreMatPrima = $('#materia-prima option:selected').text();
                    var idMatPrima = $('#materia-prima').val();
                    var cantidad = $('#cantidad').val();
                    var precioUnitario = $('#precio').val();
                    var totalParcialMatP = $('#totalParcial').val();

                    //Defino una variable y la lleno con codigo HTML, este codigo representa una fila en la tabla que ya
                    //tengo creada, asigno a cada campo de la fila un inpud de tipo hidden para poder enviar los datos
                    //en el formulario y asi posteriormente guardarlos.                    
                    //Se definen los nombres de los inputs con un [] para que se puedan enviar varios datos en un solo array
                    //y luego recorrerlos en el controlador

                    row = "<tr>" +
                        //Campo materia prima en la fila, muestro el nombre pero en el input va el valor del id
                        `<td>` + nombreMatPrima + `<input type='hidden' name='materia-prima[]'value='` +
                        idMatPrima + `'></td>` +
                        //Campo cantidad en la fila
                        `<td>` + cantidad + `<input type='hidden' name='cantidad[]' value='` +
                        cantidad + `'></td>` +
                        //Campo precio unitario en la fila
                        `<td>` + formatear.format(precioUnitario) +
                        `<input type='hidden' name='precioUnitario[]' value='` +
                        precioUnitario + `'></td>` +
                        //Campo precio parcial en la fila
                        `<td id="subtotal">` + formatear.format(totalParcialMatP) +
                        `<input type='hidden' name='totalParcial[]' value='` +
                        totalParcialMatP + `'></td>` +
                        //Boton para eliminar una fila si es necesario
                        `<td id="DeleteButton"><a href="#" class="badge badge-danger" >Eliminar X</a></td>` +

                        `</tr>`;

                    //Guardo el cuerpo de la tabla en una variable
                    tableBody = $("table tbody");
                    //Aca agrego la fila que creamos anteriormente
                    tableBody.append(row);
                    //Guardo el total parcial de la compra agregada en una variable para luego hacer calculos
                    totalParcial = $('#totalParcial').val();
                    //En la variable global 'total' voy acumulando todos los totales parciales
                    //Parse float identifica las variables como numeros para poder sumarlas, de otra forma las concatena
                    total = parseFloat(total) + parseFloat(totalParcial);
                    //Asigno el valor del total al h5 para mostrarselo al usuario
                    $('#totalCompra').text('Total: ' + formatear.format(total));
                    //Asigno tambien al input de tipo hidden dentro del formulario el valor para cuando se envie
                    $('#total').val(total);
                    //Vacio los inputs
                    $('#cantidad').val('');
                    $('#precio').val('');
                    $('#totalParcial').val('');
                };
            });

            //Las dos funciones siguientes son para que se calcule automaticamente el total parcial multiplicando la cantidad
            //y el precio
            $('#precio').keyup(function(e) {
                var totalParcial = ($('#cantidad').val() * $('#precio').val());
                $('#totalParcial').val(totalParcial);
            });
            $('#cantidad').keyup(function(e) {
                var totalParcial = ($('#cantidad').val() * $('#precio').val());
                $('#totalParcial').val(totalParcial);
            });

            //Cuando se da click en el boton eliminar fila
            $("#tablaCompras").on("click", "#DeleteButton", function() {
                //Obtiene el valor del total parcial de la fila que se esta borrando
                var subtotal = $(this).closest('td').siblings('td#subtotal').children().val();
                //Si la tabla tiene 3 tr, que en este caso no tiene ninguna pero jq lo tomas asi
                //Significa que ya no hay nada en el "carro" por lo que vuelo a mostrar la fila que dice agregue una materia prima
                if ($('#tablaCompras tr').length == 3) {
                    $('#vacio').removeClass('d-none');
                }
                //Se lo resta a la variable del total a pagar
                total = parseFloat(total) - parseFloat(subtotal);
                //Actualiza el h3 donde se muestra el total
                $('#totalCompra').text('Total: ' + formatear.format(total));
                //Borra la fila
                $(this).closest("tr").remove();
            });

            //========================== DESDE INGRESO ===================================
            //===================== EVENTO KEYUP DE CANTIDAD Y PRECIO PARA CALCULAR EL SUBTOTAL ===============================
            $('.cantidadAFacturar').keyup(function(e) {
                var cantidad = $(this).val();
                var precioKg = $(this).closest('td').next('td').find('.precioKg').val();
                var subtotal = cantidad * precioKg;
                $(this).closest('td').next('td').next('td').find('.subtotalIngreso').val(subtotal);
            });

            $('.precioKg').keyup(function(e) {
                var precioKg = $(this).val();
                var cantidad = $(this).closest('td').prev('td').find('.cantidadAFacturar').val();
                var subtotal = cantidad * precioKg;
                $(this).closest('td').next('td').find('.subtotalIngreso').val(subtotal);
            });

            //===================== EVENTO FINALIZAR COMPRA ====================================
            $('#finalizarCompraDesdeIngreso').on('click', function(e) {
                e.preventDefault();
                if (!$('#iva').val()) {
                    invalidFeedback('#iva', '#error-iva', 'Debe especificar el IVA');
                    return
                } else if (0 > $('#iva').val()) {
                    invalidFeedback('#iva', '#error-iva', 'Introduzca un IVA válido');
                    return
                }
                if (comprobarInputs() >= 1) {
                    Swal.fire({
                        title: 'Hay campos sin completar',
                        text: "¿Desea continuar de todas formas?",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Si!',
                        cancelButtonText: 'No',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            realizarCompra();
                        }
                    })
                } else {
                    Swal.fire({
                        title: '¿Desea realizar esta compra?',
                        text: "",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Si!',
                        cancelButtonText: 'No',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            removeFeedback('#iva', '#error-iva');
                            realizarCompra();
                        }
                    })
                }

            });

            //=================== FUNCTION ENVIAR PETICION AJAX PARA CREAR LA COMPRA ===============================

            function realizarCompra() {
                $.ajax({
                    type: "POST",
                    url: "{{ route('compraMP-desdeIngreso.store') }}",
                    data: $('#formCompraDesdeIngreso').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: "json",
                    success: function(data) {
                        if (data.error) {
                            console.log('%cindex.blade.php line:517 error',
                                'color: #007acc;', data.error);
                            html =
                                '<div class="alert alert-danger alert-dismissible fade show" role="alert"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html +=
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></ul></div>';
                            $('#resultFormDesdeIngreso').html(html);
                        }
                        if (data.success) {
                            window.location.href =
                                "{{ route('purchase-history.index') }}";
                        }
                    }
                });
            }

            function comprobarInputs() {
                var contador = 0;
                $("#formCompraDesdeIngreso input[type=number]").each(function() {
                    if (!$(this).val()) {
                        contador += 1;
                    }
                });
                return contador;
            }

            $('#chbDetalle').click(function(e) {
                if ($(this).is(":checked")) {
                    $('#detalle').removeClass('d-none');
                } else {
                    $('#detalle').addClass('d-none');
                }

            });

            //==================== FUNCION ERROR DE VALIDACION ============================
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }

            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });
        })
    </script>
@stop
