@extends('adminlte::page')

@section('title', 'Entrega de indumentaria')

@section('css')
    <style>
        .content-header {
            padding-bottom: 0px !important;
        }

        hr {
            margin-top: 0.5rem !important;
            margin-bottom: 0.5rem !important;
        }

        .boton-margen {
            margin-top: 2.3rem;
            margin-right: 1rem;
            margin-left: -1rem;
        }

    </style>
@stop

@section('content_header')
    <div class="container-fluid px-4">
        <div class="row mb-1">
            <h1>Entrega de indumentaria</h1>
        </div>
        <div class="row d-flex justify-content-between align-items-center">
            <h5>Empleado: {{ $empleado->first()->nombreCompleto }} <span class="text-muted"> - DNI:
                    {{ $empleado->first()->dni }}
                </span></h5>
            <div class="col-md-2">
                <input type="date" class="form-control" id="fechaElegida" value="{{ now()->format('Y-m-d') }}"
                    max="{{ now()->format('Y-m-d') }}" min="{{ now()->subDays(7)->format('Y-m-d') }}">
            </div>
        </div>
    </div>
    <hr>
@stop

@section('content')

    {{-- MODAL PARA AGREGAR INDUMENTARIA --}}
    <div class="modal fade" id="modalIndumentariaAgregarEditar" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title" id="labelIndumentariaAgregar">Agregar nueva indumentaria
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="" method="" id="formAgregarIndumentaria">
                    <div class="modal-body">
                        <span id="resultadoFormIndumentaria"></span>
                        <div class="form-group">
                            <label for="nombre" class="col-form-label">Nombre (*)</label>
                            <input type="text" class="form-control" name="nombre" id="nombreIndumentaria"
                                placeholder="Nombre..." autofocus required autocomplete="off">

                        </div>
                        <div class="form-group">
                            <label for="tipo_modelo" class="col-form-label">Tipo (*)</label>
                            <input type="text" class="form-control" name="tipo_modelo" id="tipo_modelo"
                                placeholder="Tipo..." required autocomplete="off">

                        </div>
                        <div class="form-group">
                            <label for="modelo" class="col-form-label">Modelo (*)</label>
                            <input type="text" class="form-control" name="modelo" id="modelo" placeholder="Modelo..."
                                required autocomplete="off">

                        </div>
                        <div class="form-group">
                            <label for="cantidad" class="col-form-label">Cantidad (*)</label>
                            <input type="number" name="cantidad" class="form-control" placeholder="Cantidad..."
                                id="cantidadIndumentaria" required autocomplete="off" autocomplete="off">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-success" id="btnAgregarIndumentaria">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class="container-fluid" id='app'>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group mb-0">
                    <label for="indumentarias" class="col-form-label">Indumentaria</label>
                    {{-- LLeno el select con las materias primas de la db --}}
                    <select name="indumentarias" class="form-control" id="indumentarias" style="width:100%!important;">
                        @foreach ($indumentarias as $indumentaria)
                            <option value="{{ $indumentaria->id }}" data-cantidad="{{ $indumentaria->cantidad }}"
                                data-tipo={{ $indumentaria->tipo_modelo }} data-modelo={{ $indumentaria->modelo }}>
                                {{ $indumentaria->nombre }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-1 ">
                <div class="form-group mb-0">
                    <label for="indumentaria" class="col-form-label">ㅤ</label>
                    <a class="btn btn-success form-control " name="indumentaria" data-toggle="modal"
                        data-target="#modalIndumentariaAgregarEditar" id="btnModalAgregar"> <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group mb-0">
                    <label for="cantidad" class="col-form-label">Cantidad</label>
                    <input type="number" name="cantidad" class="form-control" id="cantidad_">
                    <span class="invalid-feedback" role="alert" id="error-cantidad">
                        <strong></strong>
                    </span>
                    <span class="valid-feedback text-info" role="alert" id="infoCantidad">
                        <strong>Quedan 22 unidades</strong>
                    </span>
                </div>
            </div>
            <div class="col-md-1 mr-5">
                <label for="indumentaria" class="col-form-label">Certificacion</label>
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-outline-secondary active">
                        <input type="radio" name="options" id="certificacionSi"> Si
                    </label>
                    <label class="btn btn-outline-secondary">
                        <input type="radio" name="options" id="certificacionNo" checked> No
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group mb-0">
                    <label for="indumentaria" class="col-form-label">ㅤ</label>
                    <button class="btn btn-success bg-hfrut form-control " id="agregar">Agregar</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12 table-responsive">
                <form action="" method="POST" id="formEntrega">
                    @csrf
                    <input type="hidden" name="fecha" id="fechaCompra" value="">
                    {{-- ID del empleado al que se le va a entregar indumentarias --}}
                    <input type="hidden" name="idEmpleado" id="idEmpleado" value=" {{ $empleado->first()->id }}">
                    <table class="table table-striped" id="tablaCompras">
                        <thead class="">
                            <tr>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Modelo</th>
                                <th>Certificación</th>
                                <th>Cantidad</th>
                                <th style="width:10px">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class='text-center' id='vacio'>
                                <td colspan="6">Agregue un indumentaria para su respectiva entrega</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="chbDetalle">
                        <label class="form-check-label" for="chbDetalle">
                            Agregar detalle de entrega
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mb-0 d-none" id="detalle">
                                <label for="detalle" class="col-form-label">Detalles de la entrega:</label>
                                <textarea name="detalle" id="" cols="30" rows="2" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row justify-content-end px-3 mb-3">
                        <button class="btn btn-warning btn-lg" id="finalizarEntrega" type="button">
                            Finalizar Entrega
                        </button>
                    </div>
                </form>
            </div>
        </div>
    @stop


    @section('js')
        <script>
            $(document).ready(function() {

                var cantidadInsumo = 0;
                var cantidadExcedida = '';

                // $('#modalEmpleados').modal('show');
                $('#empleados').select2();
                $('#indumentarias').select2();


                obtenerCantidad();
                //=============== EVENTO SE MUESTRA EL MODAL ========================
                $('#modalEmpleados').on('shown.bs.modal', function(event) {
                    //Lleno el select de empleados
                    axios.get('/obtener-empleados-json').then((r) => {
                        var empleados = r.data;
                        for (var i = 0; i < empleados.length; i++) {
                            $('#empleados').append('<option value=' + empleados[i].id +
                                ' data-dni=' + empleados[i].dni + ' data-cargo=' + empleados[i]
                                .nombre + '>' +
                                empleados[i].nombreCompleto +
                                '</option>');
                        }
                        //Seteo los valores de los inputs por si 2 o mas empleados tienen el mismo nobre para poder diferenciarlos
                        $('#dni').val(empleados[0].dni);
                        $('#cargo').val(empleados[0].nombre);
                    });
                });


                //=============== EVENTO SE CIERRA EL MODAL ========================
                $('#modalEmpleados').on('hide.bs.modal', function(event) {
                    //Vacio el select de empleados
                    $('#empleados').empty();
                });

                $('#modalIndumentariaAgregarEditar').on('hide.bs.modal', function(event) {
                    $("#modalIndumentariaAgregarEditar :input").each(function(index) {
                        $(this).val('');
                    });
                })

                //=============== EVENTO CAMBIO SELECT DE INSUMOS ========================
                $('#indumentarias').on('select2:select', function(e) {
                    //Vacio el input cantidad
                    $('#cantidad_').val('');
                    //Vuelvo a setear la variable global que me dice si el usuario esta tratando de entregar mas cantidad de indumentarias 
                    //de los que tiene disponibles en el inventario
                    cantidadExcedida = false;
                    obtenerCantidad();
                });

                //============== FUNCION QUE MUESTRA LA CANTIDAD RESTANTE DE UN INSUMO DEBAJO DEL INPUT CANTIDAD ================
                function obtenerCantidad() {
                    //Obtiene la cantidad a traves del atributo data dentro del option
                    var opcionSeleccionada = $('#indumentarias').find('option:selected');
                    cantidadInsumo = opcionSeleccionada.data('cantidad')

                    //Muestra el label debajo del input cantidad
                    $('#infoCantidad').addClass('d-block');
                    $('#infoCantidad').children().text('Quedan ' + cantidadInsumo + ' unidad(es)');
                    $('#infoCantidad').addClass('text-info').removeClass('text-danger');
                    $('#cantidad_').removeClass('is-invalid');
                }

                //=================== EVENTO CAMBIA EL SELECT DE EMPLEADOS DENTRO DEL MODAL ====================
                $('#empleados').change(function(e) {
                    var selected = $(this).find('option:selected');
                    var dni = selected.data('dni');
                    var cargo = selected.data('cargo');
                    $('#dni').val(dni);
                    $('#cargo').val(cargo);
                });

                //====================== EVENTO CLICK DEL CHECKBOX PARA AGREGAR UN DETALLE A LA ENTREGA ===================
                $('#chbDetalle').click(function(e) {
                    if ($(this).is(":checked")) {
                        $('#detalle').removeClass('d-none');
                    } else {
                        $('#detalle').addClass('d-none');
                    }

                });

                //===================== MODAL AGREGAR INSUMO SERVICIO ================================
                $('#modalIndumentariaAgregarEditar').on('shown.bs.modal', function(event) {
                    $('#nombre').focus();
                })

                $('#modalIndumentariaAgregarEditar').on('hide.bs.modal', function(event) {
                    $('#nombre').val('');
                    $('#cantidad').val('');
                    $('#resultadoFormAgregar').empty();
                })

                //===================== AGREGAR INSUMO - SERVICIO ==================================
                //Accion para agregar un indumentaria - servicio
                $('#btnAgregarIndumentaria').on('click', function(e) {
                    e.preventDefault();

                    //El boton agregar envia los datos del formulario automaticamente
                    $.ajax({
                        url: "{{ route('indumentary.store') }}",
                        method: "POST",
                        //Envio los datos del formulario
                        data: $('#formAgregarIndumentaria').serialize() +
                            "&_token={{ csrf_token() }}",
                        dataType: 'json',
                        success: function(data) {
                            if (data.error) {
                                //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                                //al que le agrego automaticamente el html
                                html =
                                    '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                                for (var i = 0; i < data.error.length; i++) {
                                    html += '<li>' + data.error[i] + '</li>';
                                }
                                html += '</ul></div>';
                                $('#resultadoFormAgregar').html(html);
                            }
                            if (data.success) {
                                // Si todo esta bien muestro una alerta 
                                $('#indumentarias').empty();

                                $.get("/indumentarias-json", function(data) {
                                    var indumentarias = data;
                                    //Los recorro y los voy agregando dentro del select
                                    for (var i = 0; i < indumentarias.length; i++) {
                                        $('#indumentarias').append('<option value=' +
                                            indumentarias[i].id + "' data-cantidad=" +
                                            indumentarias[i].cantidad + ' data-tipo=' +
                                            indumentarias[i].tipo_modelo +
                                            ' data-modelo=' + indumentarias[i].modelo +
                                            ' >' +
                                            indumentarias[i].nombre + '</option>');
                                    }
                                    $('#indumentarias option:last').attr('selected',
                                        'selected');

                                    obtenerCantidad();
                                });

                                //Muestro una alerta diciendo que la operacion se completo con exito
                                Swal.fire({
                                    toast: true,
                                    icon: 'success',
                                    title: data.success,
                                    position: 'top-right',
                                    showConfirmButton: false,
                                    timer: 2300,
                                });
                                //Escondo el modal
                                $('#modalIndumentariaAgregarEditar').trigger('click');
                            }

                        }
                    })
                });

                //========================= FUNCION PARA COMPROBAR CAMPOS =========================
                //Funcion para comprobar que los campos cantidad y precio no esten vacios a la hora de agregar una compra
                //Paso como parametro el nombre del campo (cantidad o precio)
                function comprobarCampos(campo, nombre) {
                    //Primero defino una variable con la cantidad de caracteres del campo pero le saco los espacios
                    //para verificar que el usuario no haya introducido espacios y este contando como caracteres
                    var length = $.trim($('#' + campo).val()).length;

                    //Si length es igual a 0 significa que el campo esta vacio
                    if (length == 0) {
                        //Hago aparecer la alerta de error y le asigno el mensaje de error
                        $('#' + campo).addClass('is-invalid');
                        $('#error-' + nombre).addClass('d-block');
                        $('#error-' + nombre).children().text('El campo ' + nombre + ' no puede estar vacio');
                        //Retorno falso para que no se continue con el procedimiento de compra
                        return false;
                        //Ahora compruebo que no se haya introducido un numero negativo
                    } else if ($('#' + campo).val() < 0) {
                        //Si se introdujo un numero negativo hago aparecer el mensaje de error
                        $('#error-' + nombre).addClass('d-block');
                        $('#error-' + nombre).children().text(nombre + ' no puede ser menor que 0');
                        //Retorno falso para no continuar con el procedimiento de compra
                        return false
                    }
                    //Si todo esta ok se retorna verdadero y se continua con la compra
                    return true;
                }

                //====================== FUNCION AGREGAR UN INSUMO AL "CARRITO" ======================
                function agregarATabla() {

                    //Borro los mensajes de error si es que existen
                    if ($('#error-cantidad').hasClass('d-block')) {
                        $('#error-cantidad').toggleClass('d-block');
                        $('#error-cantidad').children().empty();
                    };

                    //Guardo todos los valores de los inputs anteriores en una respectiva variable para mantener
                    //el orden en el proceso siguiente
                    var nombreInsumo = $('#indumentarias option:selected').text();
                    var idInsumo = $('#indumentarias').val();
                    var cantidad = $('#indumentarias option:selected').data('cantidad');
                    var tipo_modelo = $('#indumentarias option:selected').data('tipo');
                    var modelo = $('#indumentarias option:selected').data('modelo');
                    var certificacion = $('#certificacionSi').prop('checked') == true ? 'SI' : 'NO';
                    var cantidadSeleccionada = $('#cantidad_').val();
                    var excedente = cantidad - cantidadSeleccionada;

                    if (excedente < 0) {
                        $('#indumentarias option:selected').data('cantidad', 0);
                    } else {
                        $('#indumentarias option:selected').data('cantidad', cantidad -
                            cantidadSeleccionada);
                        excedente = 0;
                    }

                    obtenerCantidad();


                    //Defino una variable y la lleno con codigo HTML, este codigo representa una fila en la tabla que ya
                    //tengo creada, asigno a cada campo de la fila un inpud de tipo hidden para poder enviar los datos
                    //en el formulario y asi posteriormente guardarlos.                    
                    //Se definen los nombres de los inputs con un [] para que se puedan enviar varios datos en un solo array
                    //y luego recorrerlos en el controlador

                    row = "<tr>" +
                        //Campo materia prima en la fila, muestro el nombre pero en el input va el valor del id
                        `<td>` + nombreInsumo +
                        `<input type='hidden' name='indumentarias[]'value='` +
                        idInsumo + `'></td>` +
                        `<td id='idInsumoTabla' class='d-none'>` + idInsumo + `</td>` +
                        `<td>` + tipo_modelo + `</td>` +
                        `<td>` + modelo + `</td>` +
                        `<td>
                        <input type="hidden" value='` + certificacion + `' name="certificacion[]">` + certificacion + `
                    </td> ` +
                        //Agrego una columna en la tabla que me va a decir si el cliente cargo mas indumentarias de los que tiene en su inventario
                        //que cantidad fue la que se excedio, por si en algun momento llegara a eliminar la fila poder volver a poner el valor original
                        //y no uno erroneo
                        `<td id='excedenteCantidad' class='d-none'>` + excedente +
                        `</td>` +
                        //Campo cantidad en la fila
                        `<td id='cantidadTabla'>` + cantidadSeleccionada +
                        `<input type='hidden' name='cantidad[]' value='` +
                        cantidadSeleccionada + `'></td>` +
                        //Boton para eliminar una fila si es necesario
                        `<td id="DeleteButton"><a href="#" class="badge badge-danger" >Eliminar X</a></td>` +
                        `</tr>`;

                    //Guardo el cuerpo de la tabla en una variable
                    tableBody = $("table tbody");
                    //Aca agrego la fila que creamos anteriormente
                    tableBody.append(row);
                    //Vacio los inputs
                    $('#cantidad_').val('');
                }

                //======================== EVENTO CLICK BOTON AGREGAR COMPRA ==============================           
                $('#agregar').click(function(e) {

                    if (comprobarCampos('cantidad_', 'cantidad') == true) {
                        $('#vacio').addClass('d-none');
                        //Si el usuario se excedio en la cantidad se le muestra un mensaje de advertencia
                        if (cantidadExcedida == true) {
                            Swal.fire({
                                title: 'Cantidad excedida',
                                text: "Está entregando una cantidad de indumentarias que no tiene en su inventario, esto generará una incongruencia en la base de datos. ¿Desea continuar de todas formas?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Si, continuar',
                                cancelButtonText: 'Cancelar'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    agregarATabla();
                                } else {
                                    return;
                                }
                            })
                        } else {
                            agregarATabla();
                        }
                    };

                });

                //========================== EVENTOS KEYUP DE CANTIDAD ==========================

                $('#cantidad_').keyup(function(e) {
                    if ($(this).val() > cantidadInsumo) {
                        $('#cantidad_').addClass('is-invalid');
                        $('#infoCantidad').removeClass('text-info').addClass('text-danger');
                        cantidadExcedida = true;
                    } else {
                        $('#error-cantidad').removeClass('d-block');
                        $('#error-cantidad').children().empty();
                        $('#cantidad_').removeClass('is-invalid');
                        $('#infoCantidad').removeClass('text-danger').addClass('text-info');
                        cantidadExcedida = false;
                    }
                });

                //======================== EVENTO CLICK ELIMINAR COMPRA =========================================
                $("#tablaCompras").on("click", "#DeleteButton", function() {
                    //Obtiene el valor del id del indumentaria. Este esta guardado en una columna en la tabla que no se esta mostrando
                    var id = $(this).closest('td').siblings('td#idInsumoTabla').text();

                    //Luego de eso obtengo la cantidad que debo volver a reintegrar al select
                    var cantidadQuitar = $(this).closest('td').siblings('td#cantidadTabla').text();

                    //Este campo lo utilizo solo para saber si el usuario introdujo una cantidad mayor de la que hay en stock
                    //De esa forma si el usuario quiere volver hacia atras puedo saber cuanto era la cantidad excedida y poder
                    //volver a poner la cantidad que estaba originariamente
                    var cantidadExcedente = $(this).closest('td').siblings('td#excedenteCantidad').text();

                    //Obtengo la cantidad actual que tiene el select mediante el atributo data
                    // var cantidadActual = $('#indumentarias option[value=' + id + ']').data('cantidad');
                    var cantidadActual = $("#indumentarias").select2().find('option[value=' + id + ']').data(
                        "cantidad");

                    var totalCantidad = parseFloat(cantidadActual) + (parseFloat(cantidadQuitar) + parseFloat(
                        cantidadExcedente));


                    //Asigno al atributo data el nuevo valor de la cantidad
                    $("#indumentarias").select2().find('option[value=' + id + ']').data('cantidad',
                        totalCantidad)

                    //Seteo el label que va debajo del input con la nueva cantidad
                    obtenerCantidad();

                    //Si la tabla tiene 3 tr, que en este caso no tiene ninguna pero jq lo tomas asi
                    //Significa que ya no hay nada en el "carro" por lo que vuelo a mostrar la fila que dice agregue una materia prima
                    if ($('#tablaCompras tr').length == 3) {
                        $('#vacio').removeClass('d-none');
                    }

                    //Borra la fila
                    $(this).closest("tr").remove();
                });

                //========================== FINALIZAR COMPRA ======================================
                $('#finalizarEntrega').click(function(e) {
                    e.preventDefault();
                    $('#fechaCompra').val($('#fechaElegida').val());
                    var form = $(this).closest("form");
                    var name = $(this).data("name");
                    //Muestro un mensaje de confirmacion 
                    Swal.fire({
                            title: `¿Desea realizar esta entrega de indumentaria?`,
                            text: "",
                            icon: "question",
                            showCancelButton: true,
                            confirmButtonText: 'Si',
                            confirmButtonColor: '#29643e',
                            cancelButtonText: 'No',
                        })
                        .then((response) => {
                            if (response.isConfirmed) {
                                $.ajax({
                                    url: "{{ route('indumentary-distribution.store') }}",
                                    method: 'POST',
                                    data: $('#formEntrega').serialize(),
                                    dataType: 'json',
                                    success: function(data) {
                                        if (data.error) {
                                            console.log(data.error)
                                            Swal.fire({
                                                icon: 'warning',
                                                title: 'Oops...',
                                                text: data.error,
                                            })
                                        }
                                        if (data.success) {
                                            Swal.fire({
                                                toast: true,
                                                icon: 'success',
                                                title: data.success +
                                                    '. Espere unos instantes',
                                                position: 'top-right',
                                                showConfirmButton: false,
                                                timer: 2000,
                                            }).then(function() {
                                                window.location.href =
                                                    "{{ route('distribution-history.index') }}";
                                            });
                                        }
                                    }
                                })
                            }
                        });
                });
            });
        </script>
    @stop
