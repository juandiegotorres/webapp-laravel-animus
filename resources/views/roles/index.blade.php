@extends('adminlte::page')

@section('title', 'Dashboard')

@section('plugins.Sweetalert2', true)

@section('content_header')
    <a href="" class="float-right pr-2" data-toggle="modal" data-target="#modalNuevoRol">
        <button class="btn btn-success bg-hfrut">
            Agregar nuevo rol
        </button>
    </a>
    <h1 class="pl-2">Roles</h1>
@stop

@section('content')
    <div class="row px-2">
        @if (session('info'))
            @push('js')
                <script>
                    var popupId = "{{ uniqid() }}";
                    if (!sessionStorage.getItem('shown-' + popupId)) {
                        Swal.fire({
                            toast: true,
                            type: 'success',
                            title: '{{ session('info') }}',
                            position: 'top-right',
                            showConfirmButton: false,
                            timer: 2300,
                        });
                    }
                    sessionStorage.setItem('shown-' + popupId, '1');
                </script>
            @endpush
        @endif
        {{-- MODAL AGREGAR ROL --}}
        <div class="modal fade" id="modalNuevoRol" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">Rol</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <span id="resultadoForm"></span>
                                <form action="" id="formRol">
                                    <div class="form-group">
                                        <label for="name">Nombre: </label>
                                        <input type="text" class="form-control" name="name" id="name"
                                            aria-describedby="helpId" placeholder="Nombre del rol..." required>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="" id="idRol">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="btnAgregarRol">Agregar</button>
                        <button type="button" class="btn btn-primary" style="display: none;"
                            id="btnModificarRol">Modificar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    {{-- TABLA --}}
                    <table class="table table-bordered table-rounded" id="tablaRoles">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Nombre</th>
                                <th scope="col" style="width: 40%">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>

    </div>
@stop

@section('css')
@stop

@section('js')
    <script>
        $(document).ready(function() {
            //======================= AGREGAR ROL ================================
            $('#btnAgregarRol').on('click', function(e) {
                e.preventDefault();
                $.ajax({
                    type: "post",
                    url: "{{ route('roles.store') }}",
                    data: $('#formRol').serialize() + "&_token={{ csrf_token() }}",
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success +
                                    '¡Ahora debes asignarle permisos!',
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2500,
                            });

                            tablaRoles.ajax.reload();

                            $('#modalNuevoRol').trigger('click');
                        }
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoForm').html(html);
                        }
                    }
                });
            });

            //======================= MODIFICAR ROL ================================
            $('#btnModificarRol').on('click', function(e) {
                e.preventDefault();
                $.ajax({
                    type: "put",
                    url: "/rol/modificar/" + $('#idRol').val(),
                    data: $('#formRol').serialize() + "&_token={{ csrf_token() }}",
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2500,
                            });

                            tablaRoles.ajax.reload();

                            $('#modalNuevoRol').trigger('click');
                        }
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoForm').html(html);
                        }
                    }
                });
            });

            //============================== EVENTOS MODAL ===============================
            $('#modalNuevoRol').on('hide.bs.modal', function(event) {
                $('#name').val('');
                $('#resultadoForm').empty('');
            })

            $('#modalNuevoRol').on('shown.bs.modal', function(event) {
                $('#name').focus();
            })

            $('#modalNuevoRol').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                //Pregunto el id desde donde se disparo el modal, si tiene el id 'editarRol' significa que 
                //debo editar. De otra forma el modal se esa llamando desde el boton agregar
                if (button.attr('id') == 'editarRol') {
                    //Paso los datos a travez de variables data dentro del boton (HTML), al igual que el id
                    var nombre = button.data("name");
                    var idRol = button.data("id");

                    // //Le asigno a un input tipo hidden el ID del producto a modificar para poder enviarlo en el request
                    $("#idRol").val(idRol);
                    //Asigno los valores a los inputs
                    $('#name').val(nombre);
                    $('#btnAgregarRol').hide();
                    $('#btnModificarRol').show();
                } else {
                    $('#btnModificarRol').hide();
                    $('#btnAgregarRol').show();
                }
            });

            //============================== ELIMINAR ROL ===============================
            $(document).on('click', '.btnEliminar', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea eliminar definitivamente este rol?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            $.ajax({
                                type: "put",
                                url: "/rol/eliminar/" + id,
                                data: "_token={{ csrf_token() }}",
                                dataType: "json",
                                success: function(data) {
                                    if (data.success) {
                                        Swal.fire({
                                            toast: true,
                                            icon: 'success',
                                            title: "Rol eliminado",
                                            position: 'top-right',
                                            showConfirmButton: false,
                                            timer: 2300,
                                        });

                                        tablaRoles.ajax.reload();
                                    }
                                }
                            });
                        }
                    }
                });
            })

            //================= DATATABLE =====================
            tablaRoles = $('#tablaRoles').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: false,
                bLengthChange: false,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('roles.dt') }}",
                columns: [{
                        data: 'name'
                    },
                    {
                        data: 'opciones'
                    },
                ],
            });
        });
    </script>
@stop
