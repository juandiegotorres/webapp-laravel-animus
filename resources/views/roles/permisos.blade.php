@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 class="pl-2">Permisos para el rol: <strong>{{ ucfirst($rol->name) }}<strong></h1>
@stop

@section('content')
    <div class="container-fluid">
        <h4 class="mb-2">Seleccione los permisos a continuación</h4>
        <hr>

        {{-- @foreach ($permisos as $permiso)
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="roles[]" id="" value="checkedValue"
                                checked>
                            {{ $permiso->description }}
                        </label>
                    </div>
                @endforeach --}}
        {{-- {{ $permisos->chunk(2) }} --}}
        {{-- {{ $permisos }} --}}
        {!! Form::model($rol, ['route' => ['permisos.update', $rol], 'method' => 'put']) !!}
        <div class="row">
            @foreach ($permisos->chunk(3) as $permisoz)
                @foreach ($permisoz as $permiso)
                    <div class="col-md-4">
                        <label for="">
                            {!! Form::checkbox('permissions[]', $permiso->id, null, ['class' => 'mr-1']) !!}
                            {{ $permiso->description }}
                        </label>
                    </div>
                @endforeach
            @endforeach
        </div>
        <div class="col-md-12 text-right mt-2 mb-4">
            {!! Form::submit('Guardar permisos', ['class' => 'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
    </div>
    </div>

@stop

@section('css')
@stop

@section('js')
    <script>

    </script>
@stop
