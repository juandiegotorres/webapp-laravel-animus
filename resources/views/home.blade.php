@extends('adminlte::page')

@section('plugins.Chartjs', true)

@section('title', 'Dashboard')

@section('css')
    <script defer src="{{ mix('js/app.js') }}"></script>

    <link rel="stylesheet" href="js/fullcalendar/main.css">

    <style>
        .fc-event-title {
            cursor: pointer !important;
        }

    </style>
@endsection

@section('content')
    <div class="container">

        {{-- CARDS --}}
        <div class="row mt-4">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h5 class="mb-4"><b>$
                                {{ number_format($totalVentas - $totalCompras['totalCompras'], 2, ',', '.') }}</b></h5>

                        <p class="m-0">Balance ventas - compras</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{ route('purchase-history.index') }}" class="small-box-footer p-1">Ver ventas <i
                            class="fas fa-arrow-circle-right ml-2"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h5 class="mb-4"><b>$ {{ number_format($totalVentas, 2, ',', '.') }}</b></h5>

                        <p class="m-0">Total ventas</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('purchase-history.index') }}" class="small-box-footer p-1">Ver ventas <i
                            class="fas fa-arrow-circle-right ml-2"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h5 class="mb-4"><b>$ {{ number_format($totalCobros, 2, ',', '.') }}</b></h5>

                        <p class="m-0">Total cobros</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{ route('cobros.index') }}" class="small-box-footer p-1">Ver cobros <i
                            class="fas fa-arrow-circle-right ml-2"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h5 class="mb-4"><b>$
                                {{ number_format($totalCompras['totalCompras'], 2, ',', '.') }}</b></h5>

                        <p class="m-0">Total compras</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    {{-- <a href="{{ route('purchase-history.index') }}" class="small-box-footer">Ver compras <i
                            class="fas fa-arrow-circle-right ml-2"></i></a> --}}
                    <div class="btn-group w-100">
                        <button type="button" class="btn btn-danger p-1"
                            style="background-color: rgba(0,0,0,.1) !important;">Detalles</button>
                        <button type="button" class="btn btn-danger dropdown-toggle dropdown-icon p-1"
                            style="background-color: rgba(0,0,0,.1) !important;" data-toggle="dropdown">
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item">Compra indumentaria: $
                                {{ number_format($totalCompras['totalIndumentaria'], 2, ',', '.') }}</a>
                            <a class="dropdown-item">Compra materia prima: $
                                {{ number_format($totalCompras['totalMatPrima'], 2, ',', '.') }}</a>
                            <a class="dropdown-item">Compra insumos: $
                                {{ number_format($totalCompras['totalInsumos'], 2, ',', '.') }}</a>
                            <a class="dropdown-item">Compra servicios: $
                                {{ number_format($totalCompras['totalServicios'], 2, ',', '.') }}</a>
                            <a class="dropdown-item">Sueldos: $
                                {{ number_format($totalCompras['totalSueldos'], 2, ',', '.') }}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./col -->
        </div>
        {{-- GRAFICO COMPRAS --}}
        <div class="row px-3">
            <div class="card card-success w-100">
                <div class="card-header">
                    <h3 class="card-title">Compras del año {{ now()->year }}</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="stackedBarChart"
                            style="min-height: 400px; height: 250px; max-height: 400px; max-width: 100%;"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
        {{-- GRAFICO VENTAS --}}
        <div class="row px-3 justify-content-center">
            <div class="card card-success w-100">
                <div class="card-header">
                    <h3 class="card-title">Ventas del año {{ now()->year }}</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="barChart"
                            style="min-height: 400px; height: 250px; max-height: 400px; max-width: 100%;"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
        {{-- CALENDARIO CHEQUES --}}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-success w-100">
                    <div class="card-body">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- MODAL CHEQUES --}}
    <div class="modal fade" id="modalCheque" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-hfrut">
                    <h4 class="modal-title" id="nombre">Cheque</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <strong><i class="fas fa-money-check-alt mr-2"></i>N° de cheque</strong>
                            <p class="text-muted mt-1" id="nroCheque">
                            </p>
                            <hr>
                        </div>
                        <div class="col-md-6 ">
                            <strong><i class="fas fa-money-bill-wave mr-2"></i>Importe</strong>
                            <h5 class="text-muted mt-1" id="importe">
                            </h5>
                            <hr>
                        </div>
                        {{-- SI ES EMITIDO --}}
                        <div class="col-md-6 d-none" id="siEsEmitido">
                            <strong><i class="fas fa-calendar-alt mr-2"></i>Fecha de emisión</strong>
                            <p class="text-muted mt-1" id="fechaEmision">
                            </p>
                            <hr>
                        </div>
                        {{-- SI ES RECIBIDO --}}
                        <div class="col-md-6 d-none" id="siEsRecibido">
                            <strong><i class="fas fa-calendar-alt mr-2"></i>Fecha de recepción</strong>
                            <p class="text-muted mt-1" id="fechaRecepcion">
                            </p>
                            <hr>
                        </div>
                        {{-- SI ES RECIBIDO -> FECHA COBRO SINO FECHA PAGO --}}
                        <div class="col-md-6">
                            <strong id="nombreSegun"><i class="fas fa-calendar-alt mr-2"></i></strong>
                            <p class="text-muted mt-1" id="fechaCobro">
                            </p>
                            <hr>
                        </div>
                        {{-- SI ES RECIBIDO --}}
                        <div class="col-md-6 d-none" id="siEsRecibido1">
                            <strong><i class="fas fa-user-tie mr-2"></i>Titular</strong>
                            <p class="text-muted mt-1" id="titular">
                            </p>
                            <hr>
                        </div>
                        <div class="col-md-6 d-none" id="siEsRecibido2">
                            <strong><i class="fas fa-people-carry mr-2"></i>Entregado por</strong>
                            <p class="text-muted mt-1" id="entregadoPor">
                            </p>
                            <hr>
                        </div>
                        {{-- FIN SI ES RECIBIDO --}}
                        <div class="col-md-6">
                            <strong><i class="fas fa-people-arrows mr-2"></i>Entregado a</strong>
                            <p class="text-muted mt-1" id="entregadoA">
                            </p>
                            <hr>
                        </div>
                        <div class="col-md-6">
                            <strong><i class="fas fa-university mr-2"></i>Banco</strong>
                            <p class="text-muted mt-1" id="banco">
                            </p>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    @stop

    @section('js')
        <script src="js/fullcalendar/locales-all.js"></script>
        <script src="js/fullcalendar/main.js"></script>
        <script>
            $(document).ready(function() {

                let dataVentas = [];
                let dataCompras = [];
                let dataCalendario = [];


                function mostrarCheque(id) {
                    $.get("/cheques/mostrar-json/" + id,
                        function(data) {
                            const cheque = data;
                            if (cheque.tipoCheque == 'recibido') {
                                $('#nombre').text('Cheque ' + cheque.tipoCheque.toUpperCase());
                                $('#siEsRecibido').removeClass('d-none');
                                $('#siEsRecibido1').removeClass('d-none');
                                $('#siEsRecibido2').removeClass('d-none');
                                $('#siEsEmitido').addClass('d-none');
                                $('#nombreSegun').html('<i class="fas fa-calendar-alt mr-2"></i>Fecha de Cobro');
                                $('#nroCheque').text(cheque.nroCheque)
                                $('#importe').text(formatear.format(cheque.importe));
                                var fechaEmision = new Date(cheque.fechaEmision);
                                var fechaRecepcion = new Date(cheque.fechaRecepcion);
                                var fechaCobro = new Date(cheque.fechaCobro);
                                var fecha = new Intl.DateTimeFormat("es-AR");

                                cheque.fechaEmision == null ? $('#fechaEmision').text('No especificado') : $(
                                    '#fechaEmision').text(fecha.format(fechaEmision))

                                cheque.fechaRecepcion == null ? $('#fechaRecepcion').text('No especificado') : $(
                                    '#fechaRecepcion').text(fecha.format(fechaRecepcion))

                                $('#fechaCobro').text(fecha.format(fechaCobro))

                                cheque.titular == null ? $('#titular').text('No especificado') : $(
                                    '#titular').text(cheque.titular)

                                cheque.entregadoPor == null ? $('#entregadoPor').text('No especificado') : $(
                                    '#entregadoPor').text(cheque.entregadoPor)

                                cheque.entregadoA == null ? $('#entregadoA').text('No especificado') : $(
                                    '#entregadoA').text(cheque.entregadoA)

                                $('#banco').text(cheque.nombreBanco)
                            } else {
                                $('#nombre').text('Cheque ' + cheque.tipoCheque.toUpperCase());
                                $('#siEsRecibido').addClass('d-none');
                                $('#siEsRecibido1').addClass('d-none');
                                $('#siEsRecibido2').addClass('d-none');
                                $('#siEsEmitido').removeClass('d-none');
                                $('#nombreSegun').html('<i class="fas fa-calendar-alt mr-2"></i>Fecha de Pago');
                                $('#nroCheque').text(cheque.nroCheque)
                                $('#importe').text(formatear.format(cheque.importe));

                                var fechaEmision = new Date(cheque.fechaEmision);
                                var fechaRecepcion = new Date(cheque.fechaRecepcion);
                                var fechaCobro = new Date(cheque.fechaCobro);
                                var fecha = new Intl.DateTimeFormat("es-AR");

                                cheque.fechaEmision == null ? $('#fechaEmision').text('No especificado') : $(
                                    '#fechaEmision').text(fecha.format(fechaEmision))

                                cheque.fechaRecepcion == null ? $('#fechaRecepcion').text('No especificado') : $(
                                    '#fechaRecepcion').text(fecha.format(fechaRecepcion))

                                $('#fechaCobro').text(fecha.format(fechaCobro))

                                cheque.titular == null ? $('#titular').text('No especificado') : $(
                                    '#titular').text(cheque.titular)

                                cheque.entregadoPor == null ? $('#entregadoPor').text('No especificado') : $(
                                    '#entregadoPor').text(cheque.entregadoPor)

                                cheque.entregadoA == null ? $('#entregadoA').text('No especificado') : $(
                                    '#entregadoA').text(cheque.entregadoA)

                                $('#banco').text(cheque.nombreBanco)
                            }

                            $('#modalCheque').modal('show');
                        },
                        "json"
                    );

                }



                //GRAFICO VENTAS
                $.get("{{ route('grafico.ventas') }}",
                    function(data) {
                        dataVentas = data;
                    },
                    "json"
                ).then(
                    function() {
                        var areaChartData = {
                            labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto',
                                'Septiempre',
                                'Octubre',
                                'Noviembre', 'Diciembre'
                            ],
                            datasets: [{
                                label: 'VENTAS',
                                backgroundColor: 'rgba(60,141,188,0.9)',
                                borderColor: 'rgba(60,141,188,0.8)',
                                pointRadius: false,
                                pointColor: '#3b8bba',
                                pointStrokeColor: 'rgba(60,141,188,1)',
                                pointHighlightStroke: 'rgba(60,141,188,1)',
                                data: dataVentas
                            }, ]
                        }


                        var barChartCanvas = $('#barChart').get(0).getContext('2d')
                        var barChartData = $.extend(false, {}, areaChartData)
                        var temp0 = areaChartData.datasets[0]
                        // barChartData.datasets[1] = temp0

                        var barChartOptions = {
                            display: true,
                            responsive: true,
                            maintainAspectRatio: false,
                            datasetFill: false,
                            scales: {
                                xAxes: [{
                                    barPercentage: 0.6
                                }],

                            },
                            // animation: {
                            //     duration: 1,
                            //     onComplete: function() {
                            //         var chartInstance = this.chart,
                            //             ctx = chartInstance.ctx;
                            //         ctx.font = Chart.helpers.fontString(Chart.defaults.global
                            //             .defaultFontSize, Chart
                            //             .defaults.global
                            //             .defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            //         ctx.textAlign = 'center';
                            //         ctx.textBaseline = 'bottom';

                            //         this.data.datasets.forEach(function(dataset, i) {
                            //             var meta = chartInstance.controller.getDatasetMeta(i);
                            //             meta.data.forEach(function(bar, index) {
                            //                 var data = '$' + dataset.data[index];
                            //                 ctx.fillText(data, bar._model.x, bar._model.y -
                            //                     5);
                            //             });
                            //         });
                            //     }
                            // }

                        }

                        new Chart(barChartCanvas, {
                            type: 'bar',
                            data: barChartData,
                            options: barChartOptions,

                        })
                    }
                );
                //GRAFICO COMPRAS
                $.get("{{ route('grafico.compras') }}",
                    function(data) {
                        dataCompras = data;
                    },
                    "json"
                ).then(() => {
                    var areaChartDataCompras = {
                        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto',
                            'Septiempre',
                            'Octubre',
                            'Noviembre', 'Diciembre'
                        ],
                        datasets: [{
                                label: 'Compras Materia Prima',
                                backgroundColor: 'rgba(101,213,49,0.9)',
                                borderColor: 'rgba(101,213,49,0.8)',
                                pointRadius: false,
                                pointColor: '#58A82D',
                                pointStrokeColor: 'rgba(101,213,49,1)',
                                pointHighlightStroke: 'rgba(101,213,49,1)',
                                data: dataCompras[0].matPrima
                            },
                            {
                                label: 'Compra de Insumos',
                                backgroundColor: 'rgba(49,213,203, 1)',
                                borderColor: 'rgba(49,213,203, 1)',
                                pointRadius: false,
                                pointColor: 'rgba(49,213,203, 1)',
                                pointStrokeColor: '#39CDD7',
                                pointHighlightFill: '#fff',
                                pointHighlightStroke: 'rgba(220,220,220,1)',
                                data: dataCompras[0].insumos
                            }, {
                                label: 'Compra de Servicios',
                                backgroundColor: 'rgba(213,201,49, 1)',
                                borderColor: 'rgba(213,201,49, 1)',
                                pointRadius: false,
                                pointColor: 'rgba(213,201,49, 1)',
                                pointStrokeColor: '#c1c7d1',
                                pointHighlightFill: '#fff',
                                pointHighlightStroke: 'rgba(220,220,220,1)',
                                data: dataCompras[0].servicios
                            }, {
                                label: 'Compra de Indumentaria',
                                backgroundColor: 'rgba(213,49,126, 1)',
                                borderColor: 'rgba(213,49,126, 1)',
                                pointRadius: false,
                                pointColor: 'rgba(213,49,126, 1)',
                                pointStrokeColor: '#c32a43',
                                pointHighlightFill: '#fff',
                                pointHighlightStroke: 'rgba(220,220,220,1)',
                                data: dataCompras[0].indumentaria
                            },
                        ]
                    }


                    var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
                    var barChartData2 = $.extend(false, {}, areaChartDataCompras)
                    var stackedBarChartData = $.extend(true, {}, barChartData2)

                    var stackedBarChartOptions = {
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            xAxes: [{
                                stacked: true,
                                barPercentage: 0.6
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        }
                    }

                    new Chart(stackedBarChartCanvas, {
                        type: 'bar',
                        data: areaChartDataCompras,
                        options: stackedBarChartOptions
                    })


                });
                //CALENDARIO CHEQUES
                $.get("{{ route('calendario.cheques') }}",
                    function(data) {
                        dataCalendario = data;
                    },
                    "json"
                ).then(() => {
                    /* initialize the external events
                     -----------------------------------------------------------------*/
                    function ini_events(ele) {
                        ele.each(function() {

                            // create an Event Object (https://fullcalendar.io/docs/event-object)
                            // it doesn't need to have a start or end
                            var eventObject = {
                                title: $.trim($(this)
                                    .text()) // use the element's text as the event title
                            }

                            // store the Event Object in the DOM element so we can get to it later
                            $(this).data('eventObject', eventObject)

                            // make the event draggable using jQuery UI
                            $(this).draggable({
                                zIndex: 1070,
                                revert: true, // will cause the event to go back to its
                                revertDuration: 0 //  original position after the drag
                            })

                        })
                    }

                    ini_events($('#external-events div.external-event'))

                    var Calendar = FullCalendar.Calendar;

                    var calendarEl = document.getElementById('calendar');

                    var calendar = new Calendar(calendarEl, {
                        locale: 'es',
                        contentHeight: 400,
                        headerToolbar: {
                            left: 'prev,next',
                            center: 'title',
                            right: ''
                        },
                        themeSystem: 'bootstrap',
                        events: dataCalendario, //Cheques desde la base de datos
                        editable: false,
                        eventClick: function(info) {
                            mostrarCheque(info.event.id);
                        }
                    });

                    calendar.render();


                });

                var formatear = new Intl.NumberFormat('de-DE', {
                    style: 'currency',
                    currency: 'USD',
                });

            });
        </script>
    @endsection
