@extends('adminlte::page')

@section('plugins.Sweetalert2', true)

@section('title', 'Listas de precios')

@section('content_header')
    <a class="float-right pr-2">
        <button class="btn btn-success bg-hfrut" data-toggle="modal" data-target="#modalListaAgregarEditar">
            Crear nueva lista de precios
        </button>
    </a>
    <p class="pl-2 h2">Listas de precios</p>
@stop

@section('content')
    {{-- MODAL PARA AGREGAR-MODIFICAR LISTA PRECIOS --}}
    <div class="modal fade" id="modalListaAgregarEditar" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="labelListaPrecio">Crear nueva lista de precios
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="" method="" id="formAgregarListaPrecio" autocomplete="off">
                    <div class="modal-body">
                        <span id="resultadoFormListaPrecio"></span>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre" class="col-form-label">Nombre de la lista de precios (*)</label>
                                <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre..."
                                    autofocus required>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fechaVigencia" class="col-form-label">Fecha Vigencia (*)</label>
                                <input type="date" name="fechaVigencia" class="form-control" id="fechaVigencia" required>
                            </div>
                        </div>

                        <input type="hidden" id="idListaPrecio">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-success" id="btnAgregarListaPrecio">Agregar</button>
                        <button class="btn btn-success d-none" id="btnEditarListaPrecio">Editar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    {{-- TABLA --}}
                    <table class="table table-bordered table-rounded" id="tablaListaPrecios">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Nombre</th>
                                <th scope="col">Fecha Vigencia</th>
                                <th scope="col" style="width:30%;">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script>
        $(document).ready(function() {

            $('body').tooltip({
                selector: '[data-toggle="tooltip"]'
            });

            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });

            $('[data-toggle="tooltip"]').on('click', function() {
                $(this).tooltip('hide')
            });

            $(document).on('click', '.boton', function(e) {
                $('[data-toggle="tooltip"]').tooltip('hide')

            });


            var tablaListaPrecios = '';
            //======================== EVENTO SE ESCONDE EL MODAL ====================================
            $('#modalListaAgregarEditar').on('hide.bs.modal', function(event) {
                $('#resultadoFormListaPrecio').empty();
                $("#modalListaAgregarEditar :input").each(function(index) {
                    $(this).val('');
                });
            })

            //======================== ENVIAR FORMULARIO AGREGAR LISTA PRECIOS ===================================

            $('#btnAgregarListaPrecio').on('click', function(e) {
                e.preventDefault();
                //El boton agregar envia los datos del formulario automaticamente
                $.ajax({
                    url: "{{ route('price-lists.store') }}",
                    method: "POST",
                    //Envio los datos del formulario
                    data: $('#formAgregarListaPrecio').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoFormListaPrecio').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });

                            tablaListaPrecios.ajax.reload();
                            //Cierro el modal
                            $('#modalListaAgregarEditar').trigger('click');
                        }
                    }
                })
            });

            //================ EVENTO CUANDO SE MUESTRA EL MOPDAL PARA EDITAR LISTA PRECIO ===================
            //Cuando se muestra el modal para modificar
            $("#modalListaAgregarEditar").on("show.bs.modal", function(event) {
                var button = $(event.relatedTarget);
                //Pregunto el id desde donde se disparo el modal, si tiene el id 'editarLista' significa que 
                //debo editar. De otra forma el modal se esa llamando desde el boton agregar
                if (button.attr('id') == 'editarLista') {
                    //Paso los datos a travez de variables data dentro del boton (HTML), al igual que el id
                    var nombre = button.data("nombre");
                    var fechaVigencia = button.data("fecha_vigencia");
                    var idListaPrecio = button.data("id");
                    var modal = $(this);
                    // //Le asigno a un input tipo hidden el ID de la lista de precio a modificar para poder enviarlo en el request
                    $("#idListaPrecio").val(idListaPrecio);
                    //Asigno los valores a los inputs
                    $('#nombre').val(nombre);
                    $('#fechaVigencia').val(fechaVigencia);
                    $('#btnAgregarListaPrecio').addClass('d-none');
                    $('#btnEditarListaPrecio').removeClass('d-none');
                } else {
                    $('#btnEditarListaPrecio').addClass('d-none');
                    $('#btnAgregarListaPrecio').removeClass('d-none');
                }
            });

            //======================== ENVIAR FORMULARIO MODIFICAR LISTA PRECIO ===================================

            $('#btnEditarListaPrecio').on('click', function(e) {
                e.preventDefault();
                var id = $('#idListaPrecio').val();
                //El boton agregar envia los datos del formulario automaticamente
                $.ajax({
                    url: "/listas-precios/update/" + id,
                    method: "PUT",
                    //Envio los datos del formulario
                    data: $('#formAgregarListaPrecio').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoFormListaPrecio').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });
                            tablaListaPrecios.ajax.reload();
                            //Cierro el modal
                            $('#modalListaAgregarEditar').trigger('click');
                        }
                    }
                })
            });

            //======================== ELIMINAR PRODUCTO ===================================
            $(document).on('click', '.btnEliminarListaPrecio', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea dar de baja esta lista de precios?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post('/listas-precios/delete/' + id, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    tablaListaPrecios.ajax.reload();
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: "Lista de precios dada de baja",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                    }
                });
            });

            //================================ DATATABLE =======================================
            tablaListaPrecios = $('#tablaListaPrecios').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: true,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "/dtListaPrecios",
                columns: [{
                        data: 'nombre',
                    },
                    {
                        data: 'fechaVigencia'
                    },
                    {
                        data: 'opciones',
                    },
                    {
                        data: 'created_at',
                        visible: false,
                    },
                ],
                order: [3, 'desc'],

            });




        });
    </script>
@stop
