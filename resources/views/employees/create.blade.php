@extends('adminlte::page')

@section('title', 'Nuevo empleado')

@section('content_header')
    <p class="pl-4 h2">Agregar nuevo empleado</p>
@stop

@section('content')

    <div class="container-fluid" id="app">
        <div class="row">
            <div class="col-md-10">

                <div class="card card-primary border-top">

                    {{-- MODAL PARA AGREGAR CARGO --}}
                    <div class="modal fade" id="modalCargos" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">

                                    <h5 class="modal-title" id="modalCargosLabel">Agregar nuevo cargo</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="" method="POST" id="formCrearCargo">
                                    @csrf
                                    <div class="modal-body">
                                        <span id="formResult"></span>
                                        <div class="form-group">
                                            <label for="nombreCargo" class="col-form-label">Nombre del cargo:</label>
                                            <input name="nombreCargo" type="text" autocomplete="off" id="nombreCargo"
                                                autofocus="off" required class="form-control"
                                                placeholder="Nombre del cargo">
                                            <input type="hidden" name='desde' value='desdeEmpleado'>
                                            <span class="invalid-feedback" role="alert" id="errorAgregar">
                                                <strong>Error</strong>
                                            </span>

                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        <button type="submit" class="btn btn-success" id="btnAgregarCargo">Agregar
                                            cargo</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    {{-- MODAL PARA EDITAR CARGO --}}
                    <div class="modal fade" id="modalCargosEditar" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalCargosLabelEditar">Editar cargo</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="" method="POST" id="formEditarCargo">
                                    @method('PUT')
                                    @csrf
                                    <div class="modal-body">
                                        <span id="formResultEditar"></span>
                                        <div class="form-group">
                                            <label for="nombreCargoEditar" class="col-form-label">Nombre del cargo:</label>
                                            <input name="nombreCargoEditar" type="text" id="nombreCargoEditar"
                                                autocomplete="off" class="form-control" required>
                                            <input type="hidden" name='desde' value='desdeEmpleado'>
                                            <span class="invalid-feedback" role="alert" id="errorModificar">
                                                <strong></strong>
                                            </span>

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" name="id" value="" id="idCargo">
                                        {{-- <input type="hidden" id="idCargo" name="id" /> --}}
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Cerrar
                                        </button>
                                        <button type="submit" class="btn btn-success" id="btnModificarCargo">Modificar
                                            cargo</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    {!! Form::open(['route' => 'employees.store', 'class' => 'p-4']) !!}
                    <div class="row">
                        <div class="col-md-12 mb-2 pl-3">
                            <strong><i class="fas fa-user-lock mr-2"></i> Datos personales</strong>
                            <hr>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('nombre', 'Nombre (*)') !!}
                                {!! Form::text('nombre', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Nombre...']) !!}

                                @error('nombre')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('apellido', 'Apellido (*)') !!}
                                {!! Form::text('apellido', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Apellido...']) !!}

                                @error('apellido')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('dni', 'DNI') !!}
                                {!! Form::number('dni', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'DNI...']) !!}
                                @error('dni')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('cuil', 'CUIL') !!}
                                {!! Form::number('cuil', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'CUIL...']) !!}
                                @error('cuil')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('telefono', 'Teléfono') !!}
                                {!! Form::number('telefono', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Teléfono...']) !!}
                                @error('telefono')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('fechaNacimiento', 'Fecha de nacimiento') !!}
                                {!! Form::date('fechaNacimiento', null, ['autocomplete' => 'off', 'max' => '2099-12-31', 'min' => '1900-01-01', 'class' => 'form-control', 'placeholder' => 'Teléfono...']) !!}
                                @error('fechaNacimiento')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('direccion', 'Dirección') !!}
                                {!! Form::text('direccion', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Dirección...']) !!}
                                @error('direccion')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('tramiteDocumento', 'Número de trámite del documento') !!}
                                {!! Form::number('tramiteDocumento', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Número de trámite...']) !!}
                                @error('tramiteDocumento')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('cbu', 'CBU') !!}
                                {!! Form::number('cbu', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'CBU...']) !!}
                                @error('cbu')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('sexo', 'Sexo') !!}

                                <div class="btn-group btn-group-toggle d-block" data-toggle="buttons">
                                    <label class="btn btn-outline-secondary active">
                                        <input type="radio" name="sexo" id="btnFemenino" value="F"> Femenino
                                    </label>
                                    <label class="btn btn-outline-secondary">
                                        <input type="radio" name="sexo" id="btnMasculino" value="M"> Masculino
                                    </label>
                                    <label class="btn btn-outline-secondary">
                                        <input type="radio" name="sexo" id="btnX" value="X"> X
                                    </label>
                                </div>

                                @error('sexo')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-12 mb-2 pl-3">
                            <strong><i class="fas fa-briefcase mr-2"></i> Datos laborales</strong>
                            <hr>
                        </div>
                        <div class="col-md-4" style="padding-right: 0px !important;">
                            <div class="form-group">
                                {!! Form::label('charge_id', 'Cargo (*)') !!}
                                <select name="charge_id" class="form-control @error('charge_id') is-invalid @enderror"
                                    id="selectCargos">
                                    @foreach ($cargos as $cargo)
                                        <option value="{{ $cargo->id }}"
                                            {{ old('charge_id') == $cargo->id ? 'selected' : '' }}>
                                            {{ $cargo->nombre }}
                                        </option>
                                    @endforeach
                                </select>
                                {{-- {!! Form::select('charge_id', $cargos->pluck('nombre', 'id'), null, ['id' => 'selectCargos', 'class' => 'form-control', 'placeholder' => 'Seleccione el cargo']) !!} --}}

                                @error('charge_id')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror


                            </div>
                        </div>

                        <div class="col-md-2 mr-0">
                            <div class="form-group">
                                <label for="">ㅤ</label>
                                <div class="d-flex">
                                    <a class="btn btn-primary mx-2 form-control flex-fill" data-toggle="modal"
                                        data-target="#modalCargosEditar" id="btnModalEditar">
                                        <i class=" fa fa-pen"></i>
                                        <a class="btn btn-success mx-2 form-control flex-fill" data-toggle="modal"
                                            data-target="#modalCargos" id="btnModalAgregar">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </a>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('fechaActividadLaboral', 'Fecha de actividad laboral') !!}
                                {!! Form::date('fechaActividadLaboral', null, ['autocomplete' => 'off', 'max' => '2099-12-31', 'min' => '1900-01-01', 'class' => 'form-control', 'placeholder' => 'Teléfono...']) !!}
                                @error('fechaActividadLaboral')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-12">
                            <a href="{{ route('employees.index') }}" class="btn btn-danger mr-2">Cancelar</a>
                            {!! Form::submit('Agregar empleado', ['class' => 'btn btn-success', 'id' => 'agregarEmpleado']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    @endsection

    @section('js')


        <script>
            $(document).ready(function() {

                //Defino un array para poder guardar los cambios cuando sea enviado el formulario de guardar empleado
                //Ya que si envio formularios dentro de la vista para agregar un cargo se va a recargar la pagina y se van 
                //a perder los datos ya cargados
                var cambios = [];
                var idCargo = '';

                $('#agregarEmpleado').click(function(e) {
                    $('#cambioCargos').val(cambios);
                });


                $('#formCrearCargo').on('submit', function(event) {
                    //Evito que se recargue la pagina
                    event.preventDefault();
                    //Envio la peticion del tipo POST a traves de ajax
                    $.ajax({
                        url: "{{ route('charges.store') }}",
                        method: 'POST',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function(data) {
                            var html = '';
                            if (data.errors) {
                                //Si hay algun error en la validacion hago un elemento html con la clase alert-danger
                                //recorro los errores y los voy agregando al html. Despues los muestro
                                html =
                                    '<div class = "alert alert-danger pb-0" role = "alert" id="alertaErrores"><ul>';
                                for (var i = 0; i < data.errors.length; i++) {
                                    html += '<li>' + data.errors[i] + '</li>';
                                }
                                html += '</ul></div>';
                                $('#formResult').html(html);
                            }
                            if (data.success) {
                                //Si se agrego correctamente el insumo servicio vacio el select
                                $('#selectCargos').empty();
                                //Obtengo todos los insumos - servicios nuevamente
                                $.get("/empleados/cargos/json", function(data) {
                                    var cargos = data;
                                    //Los recorro y los voy agregando dentro del select
                                    for (var i = 0; i < cargos.length; i++) {
                                        $('#selectCargos').append('<option value=' +
                                            cargos[i].id + '>' +
                                            cargos[i].nombre +
                                            '</option>');
                                    }
                                    //Selecciono la ultima opcion (La que se agrego ultimamente)
                                    $('#selectCargos option:last').attr('selected',
                                        'selected');
                                    //Muestro una alerta diciendo que la operacion se completo con exito
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: 'Cargo agregado con éxito',
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                    //Escondo el modal
                                    $('#modalCargos').trigger('click');
                                    $('#nombreCargo').val('');
                                });
                            }
                        }
                    })
                });

                $('#formEditarCargo').on('submit', function(event) {
                    //Evito que se recargue la pagina
                    event.preventDefault();
                    //Envio la peticion del tipo POST a traves de ajax
                    $.ajax({
                        url: "{{ route('charges.update') }}",
                        method: 'PUT',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function(data) {
                            var html = '';
                            if (data.errors) {
                                //Si hay algun error en la validacion hago un elemento html con la clase alert-danger
                                //recorro los errores y los voy agregando al html. Despues los muestro
                                html =
                                    '<div class = "alert alert-danger pb-0" role = "alert" id="alertaErrores"><ul>';
                                for (var i = 0; i < data.errors.length; i++) {
                                    html += '<li>' + data.errors[i] + '</li>';
                                }
                                html += '</ul></div>';
                                $('#formResultEditar').html(html);
                            }
                            if (data.success) {
                                //Si se agrego correctamente el insumo servicio vacio el select
                                $('#selectCargos').empty();
                                //Obtengo todos los insumos - servicios nuevamente
                                $.get("/empleados/cargos/json", function(data) {
                                    var cargos = data;
                                    //Los recorro y los voy agregando dentro del select
                                    for (var i = 0; i < cargos.length; i++) {
                                        $('#selectCargos').append('<option value=' +
                                            cargos[i].id + '>' +
                                            cargos[i].nombre +
                                            '</option>');
                                    }
                                    //Selecciono la ultima opcion (La que se agrego ultimamente)
                                    $('#selectCargos option[value=' + idCargo + ']')
                                        .attr('selected',
                                            'selected');
                                    //Muestro una alerta diciendo que la operacion se completo con exito
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: 'Cargo editado con éxito',
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                    //Escondo el modal
                                    $('#modalCargosEditar').trigger('click');

                                });
                            }
                        }
                    })
                });

                $("#modalCargos").on("hide.bs.modal", function(event) {
                    //Borro si hay algun error
                    $('#formResult').empty();
                    $('#nombreCargo').val('');
                });

                $("#modalCargosEditar").on("hide.bs.modal", function(event) {
                    //Borro si hay algun error
                    $('#formResultEditar').empty();
                    $('#nombreCargoEditar').val('');
                });


                //Evento para cuando agrega un cargo
                // $('#btnAgregarCargo').click(function(e) {
                //     //Ontengo el largo de caracteres dentro del input nombre sin espacios
                //     var length = $.trim($("#nombreCargo").val()).length;
                //     //Si el input esta vacio
                //     if (length == 0) {
                //         //Muestro el mensaje de error
                //         $('#errorAgregar').toggleClass('d-block');
                //         $('#errorAgregar').children().text(
                //             'El campo nombre no puede estar vacio');
                //         //Si es menor que 3 caracteres muestro el mensaje de error tambien 
                //         //Si ya esta siendo mostrado solo cambio el texto de dentro
                //     } else if (length < 3) {
                //         if ($('#errorAgregar').hasClass('d-block')) {
                //             $('#errorAgregar').children().text(
                //                 'El campo nombre no puede contener menos de 3 caracteres');
                //         } else {
                //             $('#errorAgregar').toggleClass('d-block');
                //             $('#errorAgregar').children().text(
                //                 'El campo nombre no puede contener menos de 3 caracteres');
                //         }
                //     } else {
                //         //Guardo el nombre del cargo en una variable
                //         var nombre = $('#nombreCargo').val();

                //         //Agrego el metodo y el nombre a un array para agregarlos a la base de datos
                //         cambios.push(['A', '', nombre]);

                //         //Agrego el nombre al select de cargos                        
                //         $('#selectCargos').append('<option>' +
                //             nombre + '</option>');
                //         //Lo selecciono
                //         $('#selectCargos').val($('#selectCargos option:last').val());

                //         //Escondo el modal
                //         $('#modalCargos').modal('hide');
                //         $('body').removeClass('modal-open');
                //         $('.modal-backdrop').remove();
                //     }
                // });

                $('#btnModalAgregar').click(function(e) {
                    $('#nombreCargo').val('');

                });

                // $('#btnModificarCargo').click(function(e) {
                //     e.preventDefault();
                //     var length = $.trim($("#nombreCargoEditar").val()).length;
                //     //Si el input esta vacio
                //     if (length == 0) {
                //         //Muestro el mensaje de error
                //         $('#errorModificar').toggleClass('d-block');
                //         $('#errorModificar').children().text(
                //             'El campo nombre no puede estar vacio');
                //         //Si es menor que 3 caracteres muestro el mensaje de error tambien 
                //         //Si ya esta siendo mostrado solo cambio el texto de dentro
                //     } else if (length < 3) {
                //         if ($('#errorModificar').hasClass('d-block')) {
                //             $('#errorModificar').children().text(
                //                 'El campo nombre no puede contener menos de 3 caracteres');
                //         } else {
                //             $('#errorModificar').toggleClass('d-block');
                //             $('#errorModificar').children().text(
                //                 'El campo nombre no puede contener menos de 3 caracteres');
                //         }
                //     } else {
                //         //Guardo el nombre del cargo en una variable
                //         var nombre = $('#nombreCargoEditar').val();
                //         var idCargo = $('#idCargo').val();

                //         if ($.isNumeric(idCargo)) {
                //             //Agrego el metodo, le id y el nombre a un array para agregarlos a la base de datos
                //             cambios.push(['M', idCargo, nombre]);
                //         } else {
                //             //Si el idCargo no es numerico significa que es un id que he agregado recientemente por lo que no asigno
                //             //ningun id
                //             cambios.push(['M', '', nombre]);;
                //         }

                //         //Edito select de cargos con el nombre nuevo                        
                //         $('#selectCargos option[value=' + idCargo + ']').text(nombre);

                //         //Escondo el modal
                //         $('#modalCargosEditar').modal('hide');
                //         $('body').removeClass('modal-open');
                //         $('.modal-backdrop').remove();
                //     }
                // });

                //Cuando aprieto el boton para modificar el cargo, cargo automaticamente el nombre del cargo seleccionado dentro
                //del modal
                $('#btnModalEditar').click(function(e) {
                    e.preventDefault();
                    var cargoAEditar = $('#selectCargos option:selected').text();
                    idCargo = $('#selectCargos').val();
                    var cargoSinEspacios = $.trim(cargoAEditar);
                    $('#nombreCargoEditar').val(cargoSinEspacios);
                    $('#idCargo').val(idCargo);
                });

            });
        </script>

    @stop
