@extends('adminlte::page')

@section('plugins.Sweetalert2', true)

@section('title', 'Empleados')

@section('content_header')
    <a href="{{ route('employees.create') }}" class="float-right pr-2">
        <button class="btn btn-success bg-hfrut">
            Agregar nuevo empleado
        </button>
    </a>
    <p class="pl-2 h2">Empleados</p>
@stop

@section('content')

    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: 'success',
                        title: '{{ session('status') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif

    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    <table class="table table-bordered table-rounded" id="empleados">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Nombre</th>
                                <th scope="col">DNI</th>
                                <th scope="col">Telefono</th>
                                <th scope="col">Cargo</th>
                                {{-- <th scope="col">Localidad</th>
                        <th scope="col">Cuil</th> --}}
                                <th scope="col" style="width:15px">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($employees as $employee)
                                <tr>
                                    <td>{{ $employee->nombreCompleto }}</td>
                                    <td>{{ $employee->dni }}</td>
                                    <td>{{ $employee->telefono }}</td>
                                    <td>{{ $employee->cargo->nombre }}</td>

                                    <td>
                                        <div class="d-flex justify-content-end">
                                            @can('employees.show')
                                                <a href="{{ route('employees.show', ['employee' => $employee->id]) }}">
                                                    <button class="btn btn-success btn-sm mr-2">
                                                        <i class="fa fa-info-circle"></i>
                                                    </button>
                                                </a>
                                            @endcan
                                            @can('employees.edit')
                                                <a href="{{ route('employees.edit', ['employee' => $employee->id]) }}">
                                                    <button class="btn btn-primary btn-sm mr-2">
                                                        <i class="fa fa-pen"></i>
                                                    </button>
                                                </a>
                                            @endcan

                                            @can('employees.delete')
                                                <dar-baja id="{{ $employee->id }}" ruta="empleados" entidad="empleado">
                                                </dar-baja>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script>
        $(document).ready(function() {


            $('#empleados').DataTable({
                responsive: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
            });
        });
    </script>

    {{-- <script>
    Swal.fire(
        'Good job!',
        'You clicked the button!',
        'success'
    )
</script> --}}
@stop
