@extends('adminlte::page')

@section('title', 'Empleado')

@section('content_header')
@stop

@section('content')

    <div class="col-md-9 pt-3">
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title"><strong>Empleado:</strong> {{ $employee->nombreCompleto }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row mb-2">
                    <div class="col-md-12 mb-3">
                        <strong class="h5"><i class="fas fa-user-lock mr-2"></i> Datos personales</strong>
                        <hr>
                    </div>
                    <div class="col-md-6">
                        <strong><i class="fas fa-id-card mr-2"></i>DNI</strong>

                        <p class="text-muted mt-1">
                            {{ $employee->dni == null ? 'No especificado' : $employee->dni }}
                        </p>

                        <hr>
                    </div>

                    <div class="col-md-6 ">
                        <strong><i class="fas fa-id-card-alt mr-2"></i>CUIL</strong>

                        <p class="text-muted mt-1">
                            {{ $employee->cuil == null ? 'No especificado' : $employee->cuil }}
                        </p>

                        <hr>
                    </div>

                    <div class="col-md-6 ">
                        <strong><i class="fas fa-fingerprint mr-2"></i>Número de trámite</strong>

                        <p class="text-muted mt-1">
                            {{ $employee->tramiteDocumento == null ? 'No especificado' : $employee->tramiteDocumento }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-6">
                        <strong><i class="fas fa-birthday-cake mr-2"></i>Fecha de nacimiento</strong>

                        <p class="text-muted mt-1">
                            {{ $employee->fechaNacimiento == null? 'No especificada': date('d-m-Y', strtotime($employee->fechaNacimiento)) }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-6 ">
                        <strong><i class="fas fa-venus-mars mr-2"></i>Sexo</strong>
                        <p class="text-muted mt-1">
                            @if ($employee->sexo == 'M')
                                Masculino
                            @elseif($employee->sexo == 'F')
                                Femenino
                            @else
                                No definido
                            @endif
                        </p>
                        <hr>
                    </div>
                    <div class="col-md-6">
                        <strong><i class="fas fa-map-marked-alt mr-2"></i>Dirección</strong>

                        <p class="text-muted mt-1">
                            {{ $employee->direccion == null ? 'No especificado' : $employee->direccion }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-6 ">
                        <strong><i class="fas fa-phone-alt mr-2"></i>Teléfono</strong>

                        <p class="text-muted mt-1">
                            {{ $employee->telefono == null ? 'No especificado' : $employee->telefono }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-6">
                        <strong><i class="fas fa-dollar-sign mr-2"></i>CBU</strong>

                        <p class="text-muted mt-1 d-flex" id='cbu'>
                            {{ $employee->cbu == null ? 'No especificado' : $employee->cbu }}
                            {!! $employee->cbu == null ? '' : '<a class="btn btn-outline-dark btn-sm ml-3" id="copiar" data-toggle="tooltip" data-placement="top" title="Copiar"> <i class="fas fa-copy"></i> </a>' !!}
                        </p>

                        <hr>
                    </div>
                </div>
                <div class="row mt-4 mb-2">
                    <div class="col-md-12 mb-3">
                        <strong class="h5"><i class="fas fa-briefcase mr-2"></i> Datos laborales</strong>
                        <hr>
                    </div>
                    <div class="col-md-6">
                        <strong><i class="fas fa-user-cog mr-2"></i>Cargo</strong>

                        <p class="text-muted mt-1">
                            {{ $employee->cargo->nombre }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-6 ">
                        <strong><i class="fas fa-calendar-alt mr-2"></i>Fecha de actividad laboral</strong>

                        <p class="text-muted mt-1">
                            {{ $employee->fechaActividadLaboral == null? 'No especificada': date('d-m-Y', strtotime($employee->fechaActividadLaboral)) }}
                        </p>

                        <hr>
                    </div>

                </div>
                <div class="row mb-2">

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ route('employees.index') }}" class="btn btn-danger mr-2">Volver</a>
                    </div>
                </div>

                {{-- <strong><i class="far fa-file-alt mr-1"></i> Notas</strong>

                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.
                </p> --}}
            </div>
        </div>

    </div>
@stop

@section('js')
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip()

            $('#copiar').click(function(e) {
                e.preventDefault();
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val($('#cbu').text().replace(/ /g, '')).select();
                document.execCommand("copy");
                $temp.remove();
                $(this).attr('data-original-title', "¡Copiado!").tooltip('show');
            });

            $('#copiar').mouseleave(function() {
                $(this).attr('data-original-title', "Copiar");
            });

        })
    </script>

@stop
