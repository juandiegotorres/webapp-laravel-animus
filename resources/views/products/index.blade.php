@extends('adminlte::page')

@section('plugins.Sweetalert2', true)

@section('title', 'Productos')

@section('content_header')
    @can('products.create')
        <a class="float-right pr-2">
            <button class="btn btn-success bg-hfrut" data-toggle="modal" data-target="#modalProductoAgregarEditar">
                Agregar nuevo producto
            </button>
        </a>
    @endcan

    <p class="pl-2 h2">Productos</p>
@stop

@section('content')
    {{-- MODAL PARA AGREGAR-MODIFICAR PRODUCTO --}}
    @can('products.create')
        <div class="modal fade" id="modalProductoAgregarEditar" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg modal-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <h5 class="modal-title" id="labelProducto">Agregar nuevo producto
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form action="" method="" id="formAgregarProducto" autocomplete="off">
                        <div class="modal-body px-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <span id="resultadoFormProducto"></span>
                                </div>
                                <div class="col-md-7">
                                    <div class=" form-group">
                                        <label for="nombre" class="col-form-label">Nombre (*)</label>
                                        <input type="text" class="form-control" name="nombreCompleto" id="nombreCompleto"
                                            placeholder="Nombre..." autofocus required>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="tipoProducto" class="col-form-label">Tipo (*)</label>
                                        <select name="tipoProducto" class="form-control" id="tiposProducto">
                                            <option value="0">Caja</option>
                                            <option value="1">Bolsa</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Peso en KG (*)</label>
                                        <input type="number" name="" class="form-control" placeholder="Peso en kg..." id="kg"
                                            required>
                                        <span class="invalid-feedback d-block" role="alert" id="error-kg">
                                            <strong></strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cantidad" class="col-form-label">Cantidad (*)</label>
                                        <input type="number" name="" class="form-control" placeholder="Cantidad..."
                                            id="cantidadProducto">
                                        <span class="invalid-feedback d-block" role="alert" id="error-cantidadProducto">
                                            <strong></strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">ㅤ</label>
                                        <button class="btn btn-primary" id="btnAgregarVariante"><i class="fa fa-plus"
                                                aria-hidden="true"></i></button>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <table class="table table-striped d-none" id="tablaVariantes">
                                        <thead>
                                            <tr>
                                                <th scope="col">Tipo</th>
                                                <th scope="col">Peso en KG</th>
                                                <th scope="col">Cantidad</th>
                                            </tr>
                                        </thead>
                                        <tbody id="bodyTablaVariantes">

                                        </tbody>

                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="descripcion" class="col-form-label">Descripción</label>
                                        <textarea type="number" name="descripcion" class="form-control"
                                            placeholder="Descripción opcional..." id="descripcionProducto" required
                                            rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button class="btn btn-success" id="btnAgregarProducto">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan

    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    {{-- TABLA --}}
                    <table class="table table-bordered table-rounded" id="tablaProductos">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Nombre</th>
                                <th scope="col">Descripcion</th>
                                <th scope="col" style="width: 20%;">Variantes</th>
                                <th scope="col" style="width:15%;">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>

    {{-- NOTE Modal mostrar variantes --}}
    <div class="modal fade" id="modalMostrarVariantes" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg modal-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title" id="labelVariante">Variantes del producto: <b></b>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="col-md-12">
                        <table class="table table-striped text-center" id="">
                            <thead>
                                <tr>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Peso en KG</th>
                                    <th scope="col">Cantidad</th>
                                </tr>
                            </thead>
                            <tbody id="tablaMostrarVariantes">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    {{-- NOTE Modal editar producto --}}
    <div class="modal fade" id="modalEditarProducto" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title" id="">Editar producto
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="" method="" id="formEditarProducto" autocomplete="off">
                    <div class="modal-body">
                        <div class="col-md-12">
                            <span id="resultadoFormEditarProducto"></span>
                        </div>
                        <div class="col-md-12">
                            <div class=" form-group">
                                <label for="nombre" class="col-form-label">Nombre (*)</label>
                                <input type="text" class="form-control" name="nombreCompleto" id="nombreEditar"
                                    placeholder="Nombre..." autofocus required>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="descripcion" class="col-form-label">Descripción</label>
                                <textarea type="number" name="descripcion" class="form-control"
                                    placeholder="Descripción opcional..." id="descripcionEditar" required
                                    rows="5"></textarea>
                            </div>
                            <input type="hidden" id="idProducto">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-success" id="btnEditarProducto">Editar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- NOTE Modal editar variante --}}
    <div class="modal fade" id="modalEditarVariante" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title" id="">Editar producto
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="" method="" id="formEditarVariante" autocomplete="off">
                    <div class="modal-body">
                        <div class="col-md-12">
                            <span id="resultadoFormEditarVariante"></span>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tipoProducto" class="col-form-label">Tipo (*)</label>
                                <select name="tipoProducto" class="form-control" id="tiposProductoEditar">
                                    <option value="0">Caja</option>
                                    <option value="1">Bolsa</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="kg" class="col-form-label">Peso en KG (*)</label>
                                <input type="number" name="kg" class="form-control" placeholder="Peso en kg..."
                                    id="kgEditar" required>
                                <span class="invalid-feedback d-block" role="alert" id="error-kg-editar">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="cantidad" class="col-form-label">Cantidad (*)</label>
                                <input type="number" name="cantidad" class="form-control" placeholder="Cantidad..."
                                    id="cantidadProductoEditar" required>
                                <span class="invalid-feedback d-block" role="alert" id="error-cantidadProducto-editar">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                        <input type="hidden" id="idVariante">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-success" id="btnEditarVariante">Editar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            var id_producto;
            var tablaProductos = '';

            //EVENTO MODAL SE ESCONDE
            $('#modalProductoAgregarEditar').on('hide.bs.modal', function(event) {
                $('#resultadoFormProducto').empty();
                $("#modalProductoAgregarEditar :input").each(function(index) {
                    if ($(this).attr('id') != 'tiposProducto') {
                        $(this).val('');
                    }
                });
                $('#tablaVariantes').addClass('d-none');
                $('#bodyTablaVariantes').empty();

            })

            //EVENTO ENVIAR FORMULARIO AGREGAR PRODUCTO ===================================

            $('#btnAgregarProducto').on('click', function(e) {
                e.preventDefault();
                //El boton agregar envia los datos del formulario automaticamente
                $.ajax({
                    url: "{{ route('products.store') }}",
                    method: "POST",
                    //Envio los datos del formulario
                    data: $('#formAgregarProducto').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoFormProducto').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });

                            tablaProductos.ajax.reload();
                            //Cierro el modal
                            $('#modalProductoAgregarEditar').trigger('click');
                        }
                    }
                })
            });

            //EVENTO Mostrar modal editar producto
            //Cuando se muestra el modal para modificar
            $("#modalEditarProducto").on("show.bs.modal", function(event) {
                var button = $(event.relatedTarget);
                var nombre = button.data("nombre");
                var descripcion = button.data("descripcion");
                var idProducto = button.data("id");

                var modal = $(this);
                // //Le asigno a un input tipo hidden el ID del producto a modificar para poder enviarlo en el request
                $("#idProducto").val(idProducto);
                //Asigno los valores a los inputs
                $('#nombreEditar').val(nombre);
                $('#descripcionEditar').val(descripcion);
            });

            //EVENTO Editar producto
            $('#btnEditarProducto').on('click', function(e) {
                e.preventDefault();
                var id = $('#idProducto').val();
                //El boton agregar envia los datos del formulario automaticamente
                $.ajax({
                    url: "/productos/" + id,
                    method: "PUT",
                    //Envio los datos del formulario
                    data: $('#formEditarProducto').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoFormEditarProducto').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });
                            tablaProductos.ajax.reload();
                            //Cierro el modal
                            $('#modalEditarProducto').trigger('click');
                        }
                    }
                })
            });

            //EVENTO Eliminar Producto
            $(document).on('click', '.btnEliminarProducto', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea dar de baja este producto?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post('/productos/delete/' + id, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    tablaProductos.ajax.reload();
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: "Producto dado de baja",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                    }
                });
            });

            //SECTION Variantes

            //EVENTO Mostrar modal variantes
            $(document).on('click', '.btnMostrarVariantes', function(e) {
                id_producto = $(this).data('id');
                cargarVariantes(id_producto);
                $('#modalMostrarVariantes').modal('show');
            });

            //EVENTO Boton agregar variante
            $('#btnAgregarVariante').click(function(e) {
                e.preventDefault();
                var error = 0;
                if (!$('#kg').val()) {
                    invalidFeedback('#kg', '#error-kg', 'El campo peso no puede esar vacio')
                    error += 1
                } else if (0 > $('#kg').val()) {
                    invalidFeedback('#kg', '#error-kg',
                        'El campo peso no puede ser menor que 0')
                    error += 1
                } else {
                    removeFeedback('#kg', '#error-kg');
                }

                if (!$('#cantidadProducto').val()) {
                    invalidFeedback('#cantidadProducto', '#error-cantidadProducto',
                        'El campo cantidad no puede esar vacio')
                    error += 1
                } else if (0 > $('#cantidadProducto').val()) {
                    invalidFeedback('#cantidadProducto', '#error-cantidadProducto',
                        'El campo cantidad no puede ser menor que 0')
                    error += 1
                } else {
                    removeFeedback('#cantidadProducto', '#error-cantidadProducto');
                }

                if (error == 0) {
                    var tipoProducto = $("#tiposProducto option:selected").text();
                    var idTipoProducto = $("#tiposProducto option:selected").val();
                    var pesoKG = $('#kg').val();
                    var cantidad = $('#cantidadProducto').val();
                    var row = '<tr>' +
                        '<td>' + tipoProducto + '<input type="hidden" name="tipo[]" value="' +
                        idTipoProducto +
                        '"></td>' +
                        '<td>' + pesoKG + '<input type="hidden" name="kg[]" value="' + pesoKG +
                        '"></td>' +
                        '<td>' + cantidad +
                        '<input type="hidden" name="cantidad[]" value="' + cantidad +
                        '"></td>';
                    $('#tablaVariantes').removeClass('d-none');

                    $('#bodyTablaVariantes').append(row);
                    $('#kg').val('');
                    $('#cantidadProducto').val('');
                    $('#kg').focus();
                }
            });

            //EVENTO Esconder modal variantes
            $('#modalMostrarVariantes').on('hide.bs.modal', function(e) {});

            //EVENTO Mostrar modal editar variante
            $('#modalEditarVariante').on('show.bs.modal', function(e) {
                var button = $(e.relatedTarget)
                var idvariante = button.data('id')
                var tipoProducto = button.data('tipo')
                var kgProducto = button.data('kg')
                var cantidad = button.data('cantidad')

                $('#tiposProductoEditar').val(tipoProducto);
                $('#kgEditar').val(kgProducto);
                $('#cantidadProductoEditar').val(cantidad);
                $('#idVariante').val(idvariante);

                $('#modalMostrarVariantes').modal('hide');

            });
            $('#modalEditarVariante').on('hide.bs.modal', function(e) {
                $('#modalMostrarVariantes').modal('show');
            });
            $('#modalEditarVariante').on('hide.bs.modal', function(e) {
                $('#resultadoFormEditarVariante').empty();
            });

            //EVENTO Editar variante
            $('#btnEditarVariante').click(function(e) {
                e.preventDefault();
                var idvariante = $('#idVariante').val();

                $.ajax({
                    url: "/productos/modificar-variante/" + idvariante,
                    method: "PUT",
                    //Envio los datos del formulario
                    data: $('#formEditarVariante').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoFormEditarVariante').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });

                            cargarVariantes(id_producto);
                            $('#modalEditarVariante').trigger('click');
                            $('#modalMostrarVariantes').modal('show');
                            // tablaProductos.ajax.reload();
                            //Cierro el modal
                        }
                    }
                })
            });

            //EVENTO Eliminar Variante
            $(document).on('click', '.btnEliminarVariante', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea dar de baja esta variante?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post('/productos/eliminar-variante/' + id, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: "Variante dada de baja",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                    cargarVariantes(id_producto);
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                    }
                });
            });

            //!SECTION


            //SECTION DATATABLE 
            tablaProductos = $('#tablaProductos').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: false,
                bLengthChange: false,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "/dtproductos",
                columns: [{
                        data: 'nombreCompleto',
                    },
                    {
                        data: 'descripcion',
                        render: function(descripcion) {
                            if (descripcion == '') {
                                return 'Sin descripción';
                            } else {
                                return descripcion;
                            }
                        }
                    },
                    {
                        data: 'variantes',
                    },
                    {
                        data: 'opciones',
                    },
                    {
                        data: 'created_at',
                        visible: false,
                    },
                ],
                order: [4, 'desc'],

            });
            //!SECTION

            //NOTE Funcion cargar variantes 
            function cargarVariantes(id) {
                $.get("/productos/mostrar-variantes/" + id,
                    function(data) {
                        const variantes = data;
                        var body = '';
                        $('#labelVariante').children().text(variantes[0].nombreCompleto);

                        variantes.forEach(variante => {
                            var tipoProducto = '';

                            switch (variante.tipo) {
                                case "0":
                                    tipoProducto = 'Caja'
                                    break;
                                case "1":
                                    tipoProducto = 'Bolsa'
                                    break;
                                default:
                                    break;
                            }

                            var row = '<tr>' +
                                '<td>' + tipoProducto + '</td>' +
                                '<td>' + variante.kg + ' kgs </td>' +
                                '<td>' + variante.cantidad + '</td>' +
                                '<td><a class="btn btn-primary btn-sm mr-2 btnEditarVariante" data-id="' +
                                variante
                                .id + '" data-tipo="' + variante
                                .tipo + '" data-cantidad="' + variante.cantidad +
                                '" data-kg="' + variante.kg +
                                '" data-toggle="modal" data-target="#modalEditarVariante"><i class="fa fa-pen"></i></a><a data-id="' +
                                variante.id +
                                '" class="btn btn-danger btn-sm btnEliminarVariante"><i class="fa fa-trash" aria-hidden="true"></i></a></td>' +
                                '</tr>'


                            body += row;
                        });
                        $('#tablaMostrarVariantes').html(body);
                    },
                    "json"
                );
            }

            //SECTION funciones validacion
            //==================== FUNCION ERROR DE VALIDACION ============================
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }
            //!SECTION
        });
    </script>
@stop
