@extends('adminlte::page')

@section('title', 'Cheque')

@section('content_header')

@stop


@section('content')

    <div class="col-md-9 pt-3">
        <div class="card card-success">
            <div class="card-header d-flex justify-content-center">
                <h3 class="mb-0">Cheque <strong> {{ strtoupper($cheque->tipoCheque) }} </strong> </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row mb-2 ">
                    <div class="col-md-6">
                        <strong><i class="fas fa-money-check-alt mr-2"></i>N° de cheque</strong>

                        <p class="text-muted mt-1" id="nroCheque">
                            {{ $cheque->nroCheque }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-6 ">
                        <strong><i class="fas fa-money-bill-wave mr-2"></i>Importe</strong>

                        <h5 class="text-muted mt-1" id="importe">
                            $ {{ number_format($cheque->importe, 2, ',', '.') }}
                        </h5>

                        <hr>
                    </div>
                    @if ($cheque->tipoCheque == 'emitido')
                        <div class="col-md-6 ">
                            <strong><i class="fas fa-calendar-alt mr-2"></i>Fecha de emisión</strong>

                            <p class="text-muted mt-1" id="fechaEmision">
                                {{ date('d-m-Y', strtotime($cheque->fechaEmision)) }}
                            </p>

                            <hr>
                        </div>
                    @elseif ($cheque->tipoCheque == 'recibido')
                        <div class="col-md-6 ">
                            <strong><i class="fas fa-calendar-alt mr-2"></i>Fecha de recepción</strong>

                            <p class="text-muted mt-1" id="fechaRecepcion">
                                {{ date('d-m-Y', strtotime($cheque->fechaRecepcion)) }}
                            </p>

                            <hr>
                        </div>
                    @endif
                    <div class="col-md-6">
                        <strong><i
                                class="fas fa-calendar-alt mr-2"></i>{{ $cheque->tipoCheque == 'recibido' ? 'Fecha de cobro' : 'Fecha de pago' }}</strong>

                        <p class="text-muted mt-1" id="fechaCobro">
                            {{ date('d-m-Y', strtotime($cheque->fechaCobro)) }}
                        </p>

                        <hr>
                    </div>
                    @if ($cheque->tipoCheque == 'recibido')
                        <div class="col-md-6">
                            <strong><i class="fas fa-user-tie mr-2"></i>Titular</strong>

                            <p class="text-muted mt-1" id="titular">
                                {{ $cheque->titular }}
                            </p>

                            <hr>
                        </div>
                        <div class="col-md-6 ">
                            <strong><i class="fas fa-people-carry mr-2"></i>Entregado por</strong>
                            <p class="text-muted mt-1" id="entregadoPor">
                                {{ $cheque->entregadoPor }}
                            </p>
                            <hr>
                        </div>
                    @endif
                    <div class="col-md-6">
                        <strong><i class="fas fa-people-arrows mr-2"></i>Entregado a</strong>

                        <p class="text-muted mt-1" id="entregadoA">
                            {{ $cheque->entregadoA == null ? 'No especificado' : $cheque->entregadoA }}
                        </p>

                        <hr>
                    </div>

                    <div class="col-md-6">
                        <strong><i class="fas fa-university mr-2"></i>Banco</strong>

                        <p class="text-muted mt-1" id="banco">
                            {{ $cheque->banco->nombre }}
                        </p>

                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ $cheque->tipoCheque == 'recibido'
                            ? redirect()->back()->with('from', 'recibido')->getTargetUrl()
                            : redirect()->back()->getTargetUrl() }}"
                            class="btn btn-danger mr-2">Volver</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('js')
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip()

            $('#copiar').click(function(e) {
                e.preventDefault();
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val($('#cbu').text()).select();
                document.execCommand("copy");
                $temp.remove();
                $(this).attr('data-original-title', "¡Copiado!").tooltip('show');
            });

            $('#copiar').mouseleave(function() {
                $(this).attr('data-original-title', "Copiar");
            });

        })
    </script>

@stop
