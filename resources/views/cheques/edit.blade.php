@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<p class="pl-4 h2">Editar cheque</p>
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-primary border-top">
                <div class="row justify-content-center px-4 mt-3">
                    <div class="col-md-12">
                        @if (session()->has('dirty'))
                        <div class="alert alert-danger mb-0" role="alert">
                            Debe modificar al menos un dato para poder guardar los cambios.
                        </div>
                        @endif
                    </div>
                </div>
                {!! Form::model($cheque, ['route' => ['cheques.update', $cheque], 'method' => 'put', 'class' => 'p-4',
                'autocomplete' => 'off']) !!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('nroCheque', 'Número de cheque (*)') !!}
                            {!! Form::number('nroCheque', null, ['autocomplete' => 'off', 'class' => 'form-control',
                            'placeholder' => 'Número de cheque ...']) !!}

                            @error('nroCheque')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    @if ($cheque->tipoCheque == 'emitido')
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('fechaEmision', 'Fecha de emisión (*)') !!}
                            {!! Form::date('fechaEmision', null, ['class' => 'form-control']) !!}
                            @error('fechaEmision')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    @elseif ($cheque->tipoCheque == 'recibido')
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('fechaRecepcion', 'Fecha de recepción (*)') !!}
                            {!! Form::date('fechaRecepcion', null, ['class' => 'form-control']) !!}
                            @error('fechaRecepcion')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    @endif
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('fechaCobro', $cheque->tipoCheque == 'recibido' ? 'Fecha de cobro (*)' :
                            'Fecha de pago (*)') !!}
                            {!! Form::date('fechaCobro', null, ['class' => 'form-control']) !!}
                            @error('fechaCobro')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    @if ($cheque->tipoCheque == 'recibido')
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('titular', 'Titular (*)') !!}
                            {!! Form::text('titular', null, ['autocomplete' => 'off', 'class' => 'form-control',
                            'placeholder' => 'Titular ...']) !!}

                            @error('titular')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('entregadoPor', 'Recibido de:') !!}
                            {!! Form::text('entregadoPor', null, ['class' => 'form-control', 'placeholder' => 'Recibido
                            de: ...']) !!}
                            @error('entregadoPor')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    @endif
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('entregadoA', 'Entregado a:') !!}
                            {!! Form::text('entregadoA', null, ['autocomplete' => 'off', 'class' => 'form-control',
                            'placeholder' => 'Entregado a: ...']) !!}
                            @error('entregadoA')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('importe', 'Importe (*)') !!}
                            {!! Form::number('importe', null, ['autocomplete' => 'off', 'class' => 'form-control',
                            'placeholder' => 'Importe...']) !!}
                            @error('importe')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('bank_id', 'Banco (*)') !!}
                            {!! Form::select('bank_id', $bancos->pluck('nombre', 'id'), $cheque->bank_id,
                            ['autocomplete' => 'off', 'class' => 'form-control']) !!}
                            @error('bank_id')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <a href="" class="btn btn-sucess"></a>
                    </div>
                </div>
                {!! Form::hidden('tipoCheque', $cheque->tipoCheque) !!}
                <div class="row mt-3">
                    <div class="col-md-12">
                        <a href="{{ $cheque->tipoCheque == 'recibido'
                                ? redirect()->back()->with('from', 'recibido')->getTargetUrl()
                                : redirect()->back()->getTargetUrl() }}" class="btn btn-danger mr-2">
                            Cancelar
                        </a>
                        {!! Form::submit('Modificar cheque', ['class' => 'btn btn-success']) !!}
                    </div>
                </div>

                {!! Form::close() !!}


            </div>
        </div>
    </div>
</div>

@stop


@section('js')

@stop