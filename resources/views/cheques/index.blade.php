@extends('adminlte::page')

@section('plugins.Sweetalert2', true)

@section('title', 'Cheques')

@section('content_header')
    @can('cheques.create')
        <a class="float-right pr-2" href="{{ route('cheques.create', ['emitidoRecibido' => 'recibido']) }}">
            <button class="btn btn-success bg-hfrut" data-toggle="modal" data-target="#modalListaAgregarEditar">
                Agregar cheque recibido
            </button>
        </a>
        <a class="float-right pr-2" href="{{ route('cheques.create', ['emitidoRecibido' => 'emitido']) }}">
            <button class="btn btn-success bg-hfrut" data-toggle="modal" data-target="#modalListaAgregarEditar">
                Agregar cheque emitido
            </button>
        </a>
    @endcan

    <p class="pl-2 h2">Cheques</p>
@stop

@section('content')
    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: 'success',
                        title: '{{ session('status') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link {{ Session::get('from') ? '' : 'active' }}" id="cheques-emitidos" data-toggle="tab"
                href="#chequesEmitidos" role="tab" aria-controls="chequesEmitidos"
                {{ Session::get('from') ? 'aria-selected="false"' : 'aria-selected="true"' }}>Cheques
                emitidos</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link {{ Session::get('from') == 'recibido' ? 'active' : '' }}" id="cheques-recibidos"
                data-toggle="tab" href="#chequesRecibidos" role="tab" aria-controls="profile"
                {{ Session::get('from') ? 'aria-selected="true"' : 'aria-selected="false"' }}>Cheques
                recibidos</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade {{ Session::get('from') ? '' : 'show active' }}" id="chequesEmitidos" role="tabpanel"
            aria-labelledby="cheques-emitidos">
            <div class="container-fluid py-3 tab-cont">
                <div class="row justify-content-between ">
                    <div class="col-md-2 align-self-end pb-3">
                        <span class="badge badge-secondary p-2 d-none" id="badgeMP">
                            <i class="fas fa-times pl-3"></i>
                        </span>
                    </div>
                </div>
                <table class="table table-bordered table-rounded" id="tablaChequesEmitidos">
                    <thead class="bg-hfrut">
                        <tr class="fuente-header">
                            <th scope="col">N° de cheque</th>
                            <th scope="col">Fecha de pago</th>
                            <th scope="col">Importe</th>
                            <th scope="col">Estado</th>
                            <th scope="col" style="width:15px">Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
            {{-- TABLA --}}



        </div>
        <div class="tab-pane fade {{ Session::get('from') == 'recibido' ? 'show active' : '' }}" id="chequesRecibidos"
            role="tabpanel" aria-labelledby="cheques-recibidos">
            <div class="container-fluid py-3 tab-cont">
                <div class="row justify-content-between ">
                    <div class="col-md-2 align-self-end pb-3">
                        <span class="badge badge-secondary p-2 d-none" id="badgeMP">
                            <i class="fas fa-times pl-3"></i>
                        </span>
                    </div>
                </div>
                <table class="table table-bordered table-rounded" id="tablaChequesRecibidos">
                    <thead class="bg-hfrut">
                        <tr class="fuente-header">
                            <th scope="col">N° de cheque</th>
                            <th scope="col">Entregado por</th>
                            <th scope="col">Entregado a</th>
                            <th scope="col">Fecha de cobro</th>
                            <th scope="col">Importe</th>
                            <th scope="col">Estado</th>
                            <th scope="col" style="width:15px">Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
    </div>
    </div>
    <div class="modal fade" id="modalMostrarCheque" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Cheque</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pl-5 pt-4">
                    <div class="row mb-2 ">
                        <div class="col-md-4">
                            <strong><i class="fas fa-money-check-alt mr-2"></i>N° de cheque</strong>

                            <p class="text-muted mt-1" id="nroCheque">
                            </p>

                            <hr>
                        </div>
                        <div class="col-md-4 offset-2">
                            <strong><i class="fas fa-calendar-alt mr-2"></i>Fecha de emisión</strong>

                            <p class="text-muted mt-1" id="fechaEmision">
                            </p>

                            <hr>
                        </div>

                    </div>
                    <div class="row mb-2">
                        <div class="col-md-4">
                            <strong><i class="fas fa-calendar-alt mr-2"></i>Fecha de cobro</strong>

                            <p class="text-muted mt-1" id="fechaCobro">
                            </p>

                            <hr>
                        </div>
                        <div class="col-md-4 offset-2">
                            <strong><i class="fas fa-calendar-alt mr-2"></i>Fecha de recepción</strong>

                            <p class="text-muted mt-1" id="fechaRecepcion">
                            </p>

                            <hr>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-4">
                            <strong><i class="fas fa-user-tie mr-2"></i>Titular</strong>

                            <p class="text-muted mt-1" id="titular">
                            </p>

                            <hr>
                        </div>

                        <div class="col-md-4 offset-2">
                            <strong><i class="fas fa-people-carry mr-2"></i>Recibido de</strong>
                            <p class="text-muted mt-1" id="entregadoPor">
                            </p>
                            <hr>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-4">
                            <strong><i class="fas fa-people-arrows mr-2"></i>Entregado A</strong>

                            <p class="text-muted mt-1" id="entregadoA">
                            </p>

                            <hr>
                        </div>
                        <div class="col-md-4 offset-2">
                            <strong><i class="fas fa-dollar-sign mr-2"></i>Importe</strong>

                            <p class="text-muted mt-1" id="importe">
                            </p>

                            <hr>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-4">
                            <strong><i class="fas fa-university mr-2"></i>Banco</strong>

                            <p class="text-muted mt-1" id="banco">
                            </p>

                            <hr>
                        </div>
                        <div class="col-md-4 offset-2">
                            <strong><i class="fas fa-coins mr-2"></i>Tipo de cheque</strong>

                            <p class="text-muted mt-1 text-capitalize" id="tipoCheque">
                            </p>

                            <hr>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalCambiarEstado" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Cambiar estado del cheque</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span id="resultFormEstado"></span>
                    <form action="" id="formCambiarEstado">
                        <div class="form-group">
                            <label for="estado" class="form-col-lable">Estado</label>
                            <select class="form-control" name="estado" id="selectEstado">
                                <option value="0">Cobrado / Pagado</option>
                                <option value="1">Agregado</option>
                                <option value="2">Entregado</option>
                                <option value="-1">Rechazado</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="btnCambiarEstado">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        var idCheque;

        $(document).ready(function() {

            //======================= MODAL MOSTRAR CHEQUE ===================
            $("#modalMostrarCheque").on("show.bs.modal", function(event) {
                var id = $(event.relatedTarget).data('id');
                $.get("/cheques/mostrar/" + id, function(data) {
                    $('#nroCheque').text(data.nroCheque);
                    $('#titular').text(data.titular);
                    $('#fechaEmision').text(data.fechaEmision);
                    $('#fechaRecepcion').text(data.fechaRecepcion);
                    $('#fechaCobro').text(data.fechaCobro);
                    if (data.entregadoPor == null) {
                        $('#entregadoPor').text('No especificado');
                    } else {
                        $('#entregadoPor').text(data.entregadoPor)
                    }
                    if (data.entregadoA == null) {
                        $('#entregadoA').text('No especificado');
                    } else {
                        $('#entregadoA').text(data.entregadoA);
                    }
                    $('#importe').text(formatear.format(data.importe));
                    $('#banco').text(data.nombre);
                    $('#tipoCheque').text(data.tipoCheque);
                });
            });

            //======================== ELIMINAR CHEQUE ===================================
            $(document).on('click', '.btnEliminarCheque', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea dar de baja este cheque?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post('/cheques/eliminar/' + id, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    tablaChequesEmitidos.ajax.reload();
                                    tablaChequesRecibidos.ajax.reload();
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: "Cheque dado de baja",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                    }
                });
            });

            //==================== EVENTOS MODAL CAMBIAR ESTADO ===================
            $('#modalCambiarEstado').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                var idEstadoCheque = (button.data('estado'));
                $('#selectEstado option:eq(' + idEstadoCheque + ')').prop('selected', true)
                //Seteo el id del cheque en una variable global
                idCheque = (button.data('id'));
            })

            $('#modalCambiarEstado').on('hide.bs.modal', function(event) {
                //Seteo el id del cheque en una variable global
                $('#resultFormEstado').empty('');
            })

            //==================== CAMBIAR ESTADO CHEQUE ===================
            $('#btnCambiarEstado').on('click', function() {
                $.ajax({
                    type: "PUT",
                    url: "/cheques/cambiar-estado/" + idCheque,
                    data: $('#formCambiarEstado').serialize() + "&_token={{ csrf_token() }}",
                    dataType: "json",
                    success: function(data) {
                        if (data.error) {
                            //Recorro los errores y desp agrego el html al DOM
                            html =
                                '<div class = "alert alert-danger pb-0" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultFormEstado').html(html);
                        }

                        if (data.success) {
                            //Alerta
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });
                            //Recargo la tabla
                            tablaChequesEmitidos.ajax.reload();
                            tablaChequesRecibidos.ajax.reload();

                            $('#modalCambiarEstado').trigger('click');
                        }
                    }
                });
            });
            //==================== DATATABLE ===========================
            tablaChequesEmitidos = $('#tablaChequesEmitidos').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: true,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "/dtcheques/emitido",
                columns: [{
                        data: 'nroCheque'
                    },
                    {
                        data: 'fechaCobro'
                    },
                    {
                        data: 'importe',
                        render: function(importe) {
                            return formatear.format(importe);
                        }
                    },
                    {
                        data: 'cambiarEstado'
                    },
                    {
                        data: 'opciones'
                    },
                ]
            });
            //==================== DATATABLE ===========================
            tablaChequesRecibidos = $('#tablaChequesRecibidos').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: true,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "/dtcheques/recibido",
                columns: [{
                        data: 'nroCheque'
                    },
                    {
                        data: 'entregadoPor',
                        render: function(entregadoPor) {
                            if (entregadoPor == null) {
                                return 'No especificado';
                            } else {
                                return entregadoPor
                            }
                        }
                    },
                    {
                        data: 'entregadoA',
                        render: function(entregadoA) {
                            if (entregadoA == null) {
                                return 'No especificado';
                            } else {
                                return entregadoA
                            }
                        }
                    },
                    {
                        data: 'fechaCobro'
                    },
                    {
                        data: 'importe',
                        render: function(importe) {
                            return formatear.format(importe);
                        }
                    },
                    {
                        data: 'cambiarEstado'
                    },
                    {
                        data: 'opciones'
                    },
                ]
            });

            //Funcion para formatear numeros a formato de dinero
            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });
        });
    </script>
@stop
