@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <p class="pl-4 h2">Agregar nuevo cheque</p>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card card-primary border-top">
                    {!! Form::open(['route' => 'cheques.store', 'class' => 'p-4', 'autocomplete' => 'off']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('nroCheque', 'Número de cheque (*)') !!}
                                {!! Form::number('nroCheque', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Número de cheque ...']) !!}

                                @error('nroCheque')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        @if ($emitidoRecibido == 'emitido')
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('fechaEmision', 'Fecha de emisión (*)') !!}
                                    {!! Form::date('fechaEmision', null, ['class' => 'form-control']) !!}
                                    @error('fechaEmision')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        @elseif ($emitidoRecibido == 'recibido')
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('fechaRecepcion', 'Fecha de recepción (*)') !!}
                                    {!! Form::date('fechaRecepcion', null, ['class' => 'form-control']) !!}
                                    @error('fechaRecepcion')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        @endif
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('fechaCobro', $emitidoRecibido == 'recibido' ? 'Fecha de cobro (*)' : 'Fecha de pago (*)') !!}
                                {!! Form::date('fechaCobro', null, ['class' => 'form-control']) !!}
                                @error('fechaCobro')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        @if ($emitidoRecibido == 'recibido')
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('titular', 'Titular (*)') !!}
                                    {!! Form::text('titular', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Titular ...']) !!}

                                    @error('titular')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('entregadoPor', 'Recibido de:') !!}
                                    {!! Form::text('entregadoPor', null, ['class' => 'form-control', 'placeholder' => 'Recibido de: ...']) !!}
                                    @error('entregadoPor')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        @endif

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('entregadoA', 'Entregado a:') !!}
                                {!! Form::text('entregadoA', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Entregado a: ...']) !!}
                                @error('entregadoA')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('importe', 'Importe (*)') !!}
                                {!! Form::number('importe', null, ['step' => '0.01', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Importe...']) !!}
                                @error('importe')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('bank_id', 'Banco (*)') !!}
                                <div class="d-flex">
                                    {!! Form::select('bank_id', $bancos->pluck('nombre', 'id'), null, ['autocomplete' => 'off', 'class' => 'form-control pr-3']) !!}
                                    <a class="btn btn-success btn-sm ml-2 mr-1" data-toggle="modal"
                                        data-target="#modalBancos" id="btnAgregarBanco"> <i class="fa fa-plus pt-2"></i>
                                    </a>
                                    <a class="btn btn-primary btn-sm mr-1" data-toggle="modal" data-target="#modalBancos"
                                        id="btnEditarBanco">
                                        <i class=" fa fa-pen pt-2"></i>
                                    </a>
                                </div>
                                @error('bank_id')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>

                    </div>
                    {!! Form::hidden('tipoCheque', $emitidoRecibido) !!}
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <a href="{{ route('cheques.index') }}" class="btn btn-danger mr-2">
                                Cancelar
                            </a>
                            {!! Form::submit('Agregar cheque', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalBancos" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="formAgregarEditarBanco" autocomplete="off">
                    <div id="agregarBanco">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <span id="resultFormAgregar"></span>
                                    <div class="form-group">
                                        <label for="nombre">Nombre</label>
                                        <input type="text" name="nombre" id="nombreBanco" class="form-control"
                                            placeholder="Nombre..." aria-describedby="helpId">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"
                                id="btnCerrarModal">Cerrar</button>
                            <button type="button" class="btn btn-primary" id="btnGuardarBanco">Agregar</button>
                            <button type="button" class="btn btn-primary" id="btnGuardarEditarBanco">Editar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {

            $('#bank_id').select2();
            //Hacer focus en la barra de busqueda cuando se abre el select de bancos
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            $('#modalBancos').on('hide.bs.modal', function() {
                $('#resultFormAgregar').empty();
                $('#nombreBanco').val('');
            });

            $('#modalBancos').on('shown.bs.modal', function() {
                $('#nombreBanco').focus();
            });
            $('#modalBancos').on('show.bs.modal', function(event) {
                var idBoton = $(event.relatedTarget).prop('id');
                if (idBoton == "btnEditarBanco") {
                    $('#btnGuardarBanco').hide();
                    $('#btnGuardarEditarBanco').show();
                    $('#nombreBanco').val($('#bank_id option:selected').text());
                } else {
                    $('#btnGuardarBanco').show();
                    $('#btnGuardarEditarBanco').hide();
                }
            });

            //================ EVENTO CLICK BOTON GUARDAR BANCO ==========================
            $('#btnGuardarBanco').on('click', function() {
                $.ajax({
                    url: '/bancos',
                    method: 'post',
                    data: $('#formAgregarEditarBanco').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultFormAgregar').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });

                            cargarBancos();

                            $('#btnCerrarModal').trigger('click');
                            $('div.modal-backdrop').remove();
                        }
                    }
                })
            });

            //================ EVENTO CLICK BOTON EDITAR BANCO ==========================
            $('#btnGuardarEditarBanco').on('click', function() {
                var idBanco = $('#bank_id option:selected').val();
                $.ajax({
                    url: '/bancos/modificar/' + idBanco,
                    method: 'put',
                    data: $('#formAgregarEditarBanco').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultFormAgregar').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });

                            cargarBancos(idBanco);
                            console.log('%ccreate.blade.php line:315 idBanco',
                                'color: #007acc;', idBanco);
                            $('#bank_id option[value=' + idBanco + ']').prop('selected', true);

                            $('#btnCerrarModal').trigger('click');
                            $('div.modal-backdrop').remove();
                        }
                    }
                })
            });


            //======================== FUNCION CARGAR BANCOS AL SELECT ===========================
            function cargarBancos(id = 'texto') {
                $('#bank_id').empty();
                $.ajax({
                    url: '/bancos',
                    method: 'get',
                    dataType: 'json',
                    success: function(data) {
                        var bancos = data;
                        //Los recorro y los voy agregando dentro del select
                        for (var i = 0; i < bancos.length; i++) {
                            $('#bank_id').append('<option value=' +
                                bancos[i].id + '>' +
                                bancos[i].nombre +
                                '</option>');
                        }
                        //Si id es nan significa que es un texto por lo que solo necesito seleccionar el ultimo elemento del select
                        if (isNaN(id)) {
                            $('#bank_id option:last').attr('selected', 'selected');
                        } else {
                            //Si el id es un numero lo establezco en el select
                            $('#bank_id option[value=' + id + ']').prop('selected', true);
                        }
                    }
                })


            }
        });
    </script>
@stop
