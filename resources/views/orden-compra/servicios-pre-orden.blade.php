@extends('adminlte::page')

@section('title', 'Dashboard')
@section('plugins.Sweetalert2', true)

@section('css')
    <style>
        .content-header {
            padding-bottom: 0px !important;
        }

        hr {
            margin-top: 0.5rem !important;
            margin-bottom: 0.5rem !important;
        }

        .boton-margen {
            margin-top: 2.3rem;
            margin-right: 1rem;
            margin-left: -1rem;
        }

    </style>

@endsection

@section('content_header')
    <div class="px-4">
        <div class="row mb-1">
            <h1>Orden de compra de servicios</h1>
        </div>

        <div class="row d-flex justify-content-between align-items-center">
            <h5>Proveedor: <span class="text-muted">{{ $proveedor->first()->nombreCompleto }} - CUIT:
                    {{ $proveedor->first()->cuit }}</span></h5>
            <div class="col-md-3">
                <input type="date" class="form-control" id="fechaElegida" value="{{ now()->format('Y-m-d') }}"
                    max="{{ now()->format('Y-m-d') }}" min="{{ now()->subDays(7)->format('Y-m-d') }}">
            </div>
            {{-- <h5>Fecha: <span class="text-muted">{{ now()->format('d-m-Y') }}</span></h5> --}}
        </div>
    </div>
    <hr>
@stop


@section('content')
    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                window.location.hash = '#';
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        type: '{{ session('status.type') }}',
                        title: 'Oops...',
                        text: '{{ session('status.message') }}',
                    })
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    <div class="container-fluid" id='app'>
        {{-- MODAL PARA AGREGAR INSUMO - SERVICIO --}}
        <div class="modal fade" id="modalInsumoServicioAgregar" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <h5 class="modal-title" id="labelInsumoServicioAgregar">Agregar nuevo servicio
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form action="" method="POST" id="formAgregarIS">
                        @csrf
                        <div class="modal-body">
                            <span id="resultadoFormAgregar"></span>
                            <div class="form-group">
                                <label for="nombre" class="col-form-label">Nombre (*)</label>
                                <input type="text" class="form-control" name="nombre" id="nombre"
                                    placeholder="Nombre del insumo / servicio..." autofocus required>

                            </div>
                            <input type="hidden" name="tipo" value="SV" id="btnServicioCrear">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button class="btn btn-success" id="btnAgregarIS">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- CONTENIDO --}}
        <div class="row">
            <div class="col-md-4 d-flex">
                <div class="form-group mb-0 mr-2 flex-fill">
                    <label for="insumo-servicio" class="col-form-label">Servicio</label>
                    {{-- LLeno el select con las materias primas de la db --}}
                    <select name="insumo-servicio" class="form-control" id="insumo-servicio"
                        style="width:100%!important;">
                        @foreach ($servicios as $servicio)
                            <option value="{{ $servicio->id }}">
                                {{ $servicio->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group mb-0">
                    <label for="insumo" class="col-form-label">ㅤ</label>
                    <a class="btn btn-success form-control " name=" insumo" data-toggle="modal"
                        data-target="#modalInsumoServicioAgregar" id="btnModalAgregar"> <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group mb-0 ">
                    <label for="agregar" class="col-form-label">ㅤ</label>
                    <a name="agregar" class="btn btn-success bg-hfrut form-control" id="agregar">Agregar</a>
                </div>
            </div>
            {{-- <div class="col-md-1 ">
            </div> --}}
        </div>
        <hr>
        <div class="row">
            <div class="col-12 table-responsive">
                <form action="{{ route('orden-compra.storeServicio') }}" method="POST">
                    @csrf
                    {{-- ID del proveedor al que se le va a realizar la compra --}}
                    <input type="hidden" name="fecha" id="fechaCompra" value="">
                    <input type="hidden" name="idProveedor" value="{{ $proveedor->id }}">
                    <table class="table table-striped" id="tablaCompras">
                        <thead>
                            <tr>
                                <th>Servicio</th>
                                <th style="width: 60%;"></th>
                                <th style="width:10px">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class='text-center' id='vacio'>
                                <td colspan="6">Agregue un servicio</td>
                            </tr>
                            <input type="hidden" value="SV" name="tipoOrdenCompra">
                        </tbody>
                    </table>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="chbDetalle" checked>
                        <label class="form-check-label" for="chbDetalle">
                            Agregar detalle de orden compra
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mb-0" id="detalle">
                                <label for="detalle" class="col-form-label">Detalles de la orden de compra:</label>
                                <textarea name="detalle" id="" cols="30" rows="2" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row justify-content-between px-3 mb-3">
                        <button class="btn btn-warning" id="finalizarCompra" type="submit">
                            Finalizar orden de compra
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@stop



@section('js')

    <script>
        $(document).ready(function() {
            //Defino una variable donde se va a almacenar el total de todas las compras y un
            var total = 0;
            var contador = 0;
            var cantidadInsumo = 0;
            var excedidoCantidad = false;

            $('#insumo-servicio').select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            $('#insumo-servicio').on('select2:select', function(e) {
                var opcionSeleccionada = $(this).find('option:selected');
                cantidadInsumo = opcionSeleccionada.data('cantidad')
                if (cantidadInsumo == null) {
                    $("#cantidad_").prop('disabled', true);
                    $("#cantidad_").val('1');
                    // $('#infoCantidad').removeClass('d-block');
                } else {
                    $("#cantidad_").prop('disabled', false);
                    $("#cantidad_").val('');
                    // $('#infoCantidad').addClass('d-block');
                    // $('#infoCantidad').children().text('Quedan ' + cantidadInsumo + ' unidad(es)');
                }
            });



            $('#chbDetalle').click(function(e) {
                if ($(this).is(":checked")) {
                    $('#detalle').removeClass('d-none');
                } else {
                    $('#detalle').addClass('d-none');
                }

            });

            //===================== MODAL AGREGAR INSUMO SERVICIO ================================
            $('#modalInsumoServicioAgregar').on('shown.bs.modal', function(event) {
                $('#nombre').focus();
            })

            $('#modalInsumoServicioAgregar').on('hide.bs.modal', function(event) {
                $('#nombre').val('');
                $('#cantidad').val('');
                $('#resultadoFormAgregar').empty();
            })


            //===================== AGREGAR INSUMO - SERVICIO ==================================
            //Accion para agregar un insumo - servicio
            $('#btnAgregarIS').on('click', function(e) {
                e.preventDefault();
                //El boton agregar envia los datos del formulario automaticamente
                $.ajax({
                    url: "{{ route('supplie-service.store') }}",
                    method: "POST",
                    //Envio los datos del formulario
                    data: $('#formAgregarIS').serialize(),
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoFormAgregar').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            $('#insumo-servicio').empty();

                            $.get("/servicios-json", function(data) {
                                var servicios = data;
                                //Los recorro y los voy agregando dentro del select
                                for (var i = 0; i < servicios.length; i++) {
                                    $('#insumo-servicio').append('<option value=' +
                                        servicios[i].id + '>' +
                                        servicios[i].nombre + '</option>');
                                }

                                if ($('#btnServicioCrear').attr('checked', true)) {
                                    $('#insumo-servicio option:last').attr('selected',
                                        'selected');
                                }
                            });

                            //Muestro una alerta diciendo que la operacion se completo con exito
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });
                            //Escondo el modal
                            $('#modalInsumoServicioAgregar').trigger('click');
                        }

                    }
                })
            });

            //======================= EVENTO QUITAR O MOSTRAR INPUT CANTIDAD =====================================

            $('#btnServicioCrear').click(function() {
                $('#divCantidad').fadeOut();
            });

            $('#btnInsumoCrear').click(function() {
                $('#divCantidad').fadeIn();
            });



            //========================== FINALIZAR COMPRA ======================================
            $('#finalizarCompra').click(function(e) {
                e.preventDefault();
                $('#fechaCompra').val($('#fechaElegida').val());
                var form = $(this).closest("form");
                var name = $(this).data("name");
                Swal.fire({
                        title: `¿Desea realizar esta orden de compra?`,
                        text: "",
                        icon: "question",
                        showCancelButton: true,
                        confirmButtonText: 'Si',
                        confirmButtonColor: '#29643e',
                        cancelButtonText: 'No',
                    })
                    .then((response) => {
                        if (response.isConfirmed) {
                            form.submit();
                        }
                    });
            });


            //========================= FUNCION PARA COMPROBAR CAMPOS =========================
            //Funcion para comprobar que los campos cantidad y precio no esten vacios a la hora de agregar una compra
            //Paso como parametro el nombre del campo (cantidad o precio)
            function comprobarCampos(campo, nombre) {
                //Primero defino una variable con la cantidad de caracteres del campo pero le saco los espacios
                //para verificar que el usuario no haya introducido espacios y este contando como caracteres
                var length = $.trim($('#' + campo).val()).length;

                //Si length es igual a 0 significa que el campo esta vacio
                if (length == 0) {
                    //Hago aparecer la alerta de error y le asigno el mensaje de error
                    $('#error-' + nombre).addClass('d-block');
                    $('#error-' + nombre).children().text('El campo total no puede estar vacio');
                    //Retorno falso para que no se continue con el procedimiento de compra
                    return false;
                    //Ahora compruebo que no se haya introducido un numero negativo
                } else if ($('#' + campo).val() <= 0) {
                    //Si se introdujo un numero negativo hago aparecer el mensaje de error
                    $('#error-' + nombre).addClass('d-block');
                    $('#error-' + nombre).children().text('Total no puede ser menor que 0');
                    //Retorno falso para no continuar con el procedimiento de compra
                    return false
                }
                //Si todo esta ok se retorna verdadero y se continua con la compra
                return true;
            }

            //======================== EVENTO CLICK BOTON AGREGAR COMPRA ==============================           
            $('#agregar').click(function(e) {

                if ($('#insumo-servicio').val()) {
                    $('#vacio').addClass('d-none');

                    //Guardo todos los valores de los inputs anteriores en una respectiva variable para mantener
                    //el orden en el proceso siguiente
                    var nombInsumoServicio = $('#insumo-servicio option:selected').text();
                    var idInsServ = $('#insumo-servicio').val();
                    //Defino una variable y la lleno con codigo HTML, este codigo representa una fila en la tabla que ya
                    //tengo creada, asigno a cada campo de la fila un inpud de tipo hidden para poder enviar los datos
                    //en el formulario y asi posteriormente guardarlos.                    
                    //Se definen los nombres de los inputs con un [] para que se puedan enviar varios datos en un solo array
                    //y luego recorrerlos en el controlador

                    row = "<tr>" +
                        //Campo materia prima en la fila, muestro el nombre pero en el input va el valor del id
                        `<td>` + nombInsumoServicio +
                        `<input type='hidden' name='insumo-servicio[]'value='` +
                        idInsServ + `'></td>` +
                        '<td></td>' +
                        //Boton para eliminar una fila si es necesario
                        `<td id="DeleteButton"><a href="#" class="badge badge-danger" >Eliminar X</a></td>` +
                        `</tr>`;

                    //Guardo el cuerpo de la tabla en una variable
                    tableBody = $("table tbody");
                    //Aca agrego la fila que creamos anteriormente
                    tableBody.append(row);
                    //Guardo el total parcial de la compra agregada en una variable para luego hacer calculos
                    $('#cantidad_').val('');
                };
            });


            //======================== EVENTO CLICK ELIMINAR COMPRA =========================================
            //Cuando se da click en el boton eliminar fila
            $("#tablaCompras").on("click", "#DeleteButton", function() {
                //Significa que ya no hay nada en el "carro" por lo que vuelo a mostrar la fila que dice agregue una materia prima
                if ($('#tablaCompras tr').length == 3) {
                    $('#vacio').removeClass('d-none');
                }
                //Borra la fila
                $(this).closest("tr").remove();
            });

            //==================== FUNCION ERROR DE VALIDACION ============================
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }

            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });
        })
    </script>
@stop
