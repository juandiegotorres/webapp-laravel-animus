@extends('adminlte::page')

@section('title', 'Historial Compra')

@section('content_header')
    <h1>Orden de compra</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="invoice p-3 mb-3">
                <div class="row">
                    <div class="col-12">
                        <h4>
                            <i class="fas fa-globe"></i> HFRUT
                            <small class="float-right">Fecha:
                                {{ date('d-m-Y', strtotime($orden->fecha)) }}</small>
                        </h4>
                    </div>
                </div>
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col my-3">
                        ID Orden de compra: <strong>#{{ $orden->id }}</strong> <br>
                        Proveedor: <strong>{{ $orden->proveedor->nombreCompleto }}</strong><br>
                        Detalle: <strong> {{ $orden->detalle }} </strong> <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped text-center">
                            <thead>
                                <tr>
                                    <th>Cantidad</th>
                                    <th>Nombre</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($detalles as $detalle)
                                    <tr>
                                        <td>{{ $detalle->cantidad }}</td>
                                        <td>{{ $detalle->nombre }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row no-print">
                    <div class="col-12">
                        <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-danger float-right">Volver</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop



@section('js')

@stop
