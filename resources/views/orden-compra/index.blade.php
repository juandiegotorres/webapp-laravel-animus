@extends('adminlte::page')

@section('title', 'Orden Compra')

@section('content_header')
    <h1 class="text-center">Seleccionar Pre Orden de Compra</h1>
@stop


@section('css')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <style>
        .card {
            box-shadow: 0 0 0px;
            background-color: #F4F6F9;
            border: 0px;
        }

        .card-footer {
            border-top: 0px;
            background-color: #F4F6F9;
        }

        .imagen-compra-card {
            padding: 3rem;
            border: 4px solid rgb(41, 100, 62);
            border-radius: 20px;
        }

    </style>
@endsection

@section('content')
    <div class="container flex-column d-flex aling-items-center mt-5">
        <div class="row w-100 d-flex mb-5 justify-content-center">
            <div class="col-3">
                <div class="card">
                    <div class="card-body d-flex justify-content-center">
                        <img src="{{ asset('img/people-carry-solid.png') }}" alt="" width="150" class="imagen-compra-card">
                    </div>
                    <div class="card-footer text-center">
                        <button class="btn btn-login" id="compraInsumos" data-toggle="modal"
                            data-target="#modalProveedores">Insumos</button>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card">
                    <div class="card-body d-flex justify-content-center">
                        <img src="{{ asset('img/tshirt-solid.png') }}" alt="" width="150" class="imagen-compra-card">
                    </div>
                    <div class="card-footer text-center">
                        <button class="btn btn-login" id="compraIndumentarias" data-toggle="modal"
                            data-target="#modalProveedores">Indumentarias</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <button class="btn btn-danger" onclick="window.location.replace('/pre-orden-compra')"><i
                    class="fa fa-arrow-left mr-2" aria-hidden="true"></i>
                Volver</button>
        </div>

        <div class="modal fade" id="modalProveedores" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <h5 class="modal-title" id="modalTitulo">Proveedores</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="proveedores" class="col-form-label">Seleccione el proveedor:</label>
                                    <select name="proveedores" id="proveedores" class="form-control"></select>

                                    <span class="invalid-feedback" role="alert" id="errorAgregar">
                                        <strong>Error</strong>
                                    </span>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-form-label">CUIT:</label>
                                    <input type="text" class="form-control" readonly id="cuit">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Teléfono:</label>
                                    <input type="text" class="form-control" readonly id="telefono">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Dirección:</label>
                                    <input type="text" class="form-control" readonly id="direccion">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <a href="" class="btn btn-success" id="btnComprar">Proceder con la compra </a>
                    </div>
                    <div class="col-md-10 text-center py-4 offset-1 d-none" id="sinProveedores">
                        <h3>No se encontraron proveedores para esta seccion, intente <a
                                href="{{ route('providers.create') }}">agregando uno</a></h3>
                        <button type="button" class="btn btn-secondary mt-2" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(document).ready(function() {
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            $('#proveedores').select2({
                width: '100%',
            });
            $('#proveedoresIS').select2({
                width: '100%',
            });

            function cargarProveedores(tipoProveedor, tituloModal) {
                $('#modalTitulo').text(tituloModal);
                axios.get('/proveedores-tipo/' + tipoProveedor).then((r) => {
                    const proveedores = r.data;
                    if (proveedores.length != 0) {
                        $('.modal-body').removeClass('d-none');
                        $('.modal-footer').removeClass('d-none');
                        $('#sinProveedores').addClass('d-none');
                        for (var i = 0; i < proveedores.length; i++) {
                            $('#proveedores').append('<option value=' + proveedores[i].id +
                                ' data-cuit=' + proveedores[i].cuit + ' data-telefono=' +
                                proveedores[i].telefono + ' data-direccion=' + encodeURIComponent(
                                    proveedores[i]
                                    .direccion) + ' data-tipoproveedor=' + tipoProveedor + '>' +
                                proveedores[i].nombreCompleto +
                                '</option>');
                        }
                        $('#cuit').val(proveedores[0].cuit);
                        $('#telefono').val(proveedores[0].telefono);
                        $('#direccion').val(proveedores[0].direccion);
                        cargarHREF(tipoProveedor);
                    } else {
                        $('.modal-body').addClass('d-none');
                        $('.modal-footer').addClass('d-none');
                        $('#sinProveedores').removeClass('d-none');
                    }
                });
            }

            function cargarHREF(tipoCompra) {
                var idProv = $('#proveedores').val();
                var url = '/pre-orden-compra/' + tipoCompra + '/' + idProv;
                $('#btnComprar').attr('href', url);
            }



            $('#compraInsumos').click(function(e) {
                e.preventDefault();
                cargarProveedores('IS', 'Proveedores de insumos')
            });

            $('#compraServicios').click(function(e) {
                e.preventDefault();
                cargarProveedores('SV', 'Proveedores de servicios')
            });

            $('#compraIndumentarias').click(function(e) {
                e.preventDefault();
                cargarProveedores('ID', 'Proveedores de indumentarias')
            });


            $('#proveedores').change(function(e) {
                var selected = $(this).find('option:selected');
                var cuit = selected.data('cuit');
                var telefono = selected.data('telefono');
                var direccion = decodeURIComponent(selected.data('direccion'));
                var tipoCompraProveedor = selected.data('tipoproveedor');
                $('#cuit').val(cuit);
                $('#telefono').val(telefono);
                $('#direccion').val(direccion);
                cargarHREF(tipoCompraProveedor);
            });

            $("#modalProveedores").on("hide.bs.modal", function(event) {
                $('#proveedores').empty();
            });



        });
    </script>
@stop
