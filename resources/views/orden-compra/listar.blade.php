@extends('adminlte::page')

@section('title', 'Dashboard')

@section('css')
    <style>
        .boton:focus {
            outline: none !important;
            box-shadow: none !important;
        }

        .boton {
            box-shadow: none !important
        }

    </style>
@endsection

@section('content_header')
    <div class="float-right pr-2">
        <a href="{{ route('orden-compra.index') }}" class="btn btn-success bg-hfrut">
            Crear orden de compra
        </a>
    </div>
    <h1>Ordenes de compra</h1>
@stop

@section('content')
    <div class="modal fade" id="modalFiltro" tabindex="-1" role="dialog" aria-labelledby="modalFiltro"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Filtros</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span id="formResult"></span>
                    <div class="row">
                        <div class="col-md-12 d-flex">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fechaInicio">Fecha de Inicio <button
                                            class="fa fa-question-circle btn p-0 m-0" data-toggle="tooltip"
                                            data-placement="bottom"
                                            title="Si no se elige una fecha de inicio, no se filtrará por fecha. Solo por proveedor"></button></label>
                                    <input type="date" name="fechaInicio" class="form-control" id="fechaInicio" required
                                        max='2099-12-31' min='1900-01-01'>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fechaFin>Fecha de Fin</label>
                                            <input type=" date" name="fechaFin" class="form-control"
                                        value="{{ Carbon\Carbon::today()->format('Y-m-d') }}" id="fechaFin"
                                        max='2099-12-31' min='1900-01-01' required>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="form-check ">
                            </div>
                            <div class="form-group mx-3">
                                <label for="proveedores" class="col-form-label">Proveedor</label>
                                <select name="proveedores" id="proveedores" class="form-control">
                                    @if ($proveedores->count() == 0)
                                        <option value="">No hay proveedores disponibles</option>
                                    @else
                                        <option value="">Seleccione un proveedor</option>
                                        @foreach ($proveedores as $proveedor)
                                            <option value="{{ $proveedor->id }}">{{ $proveedor->nombreCompleto }}
                                            </option>
                                        @endforeach
                                    @endif

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id='matPrimaOinsServicio'>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-primary" data-tipo="" id="btnFiltrar">Buscar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    <div class="row justify-content-between ">
                        <div class="col-md-2 align-self-end pb-3">
                            <span class="badge badge-secondary p-2 d-none" id="badgeFiltro">
                                <i class="fas fa-times pl-3"></i>
                            </span>
                        </div>
                        <div class="col-md-2 align-self-end pb-3">
                            <button type="button" class="btn btn-primary btn-block " data-toggle="modal"
                                data-target="#modalFiltro" data-tipo='MP'>Filtros
                            </button>
                        </div>
                    </div>
                    {{-- TABLA --}}
                    <table class="table table-bordered table-rounded" id="tablaOrdenes">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Proveedor</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Tipo de Orden</th>
                                <th scope="col">Detalles</th>
                                <th scope="col" style="width: 23%;">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {
            //Select proveedores dentro del modal filtro
            $('#proveedores').select2({
                dropdownParent: $("#modalFiltro"),
            });

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            //EVENTO Tooltip
            $('body').tooltip({
                selector: '[data-toggle="tooltip"]'
            });

            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });

            $('[data-toggle="tooltip"]').on('click', function() {
                $(this).tooltip('hide')
            });

            $(document).on('click', '.boton', function(e) {
                $('[data-toggle="tooltip"]').tooltip('hide')

            });

            //EVENTO Eliminar Orden
            $(document).on('click', '.btnEliminarOrden', function() {
                let idOrden = $(this).data('id');
                Swal.fire({
                    title: '¿Desea cancelar esta orden?',
                    text: '',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, cancelar orden',
                    cancelButtonText: 'No',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "PUT",
                            url: "/pre-orden-compra/delete/" + idOrden,
                            data: "_token={{ csrf_token() }}",
                            dataType: "json",
                            success: function(response) {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 2300,
                                    timerProgressBar: true,
                                })
                                if (response.success) {
                                    tablaOrdenes.ajax.reload();
                                    Toast.fire({
                                        icon: 'success',
                                        title: response.success
                                    })
                                } else {
                                    tablaOrdenes.ajax.reload();
                                    Toast.fire({
                                        icon: 'error',
                                        title: response.error
                                    })
                                }
                            }
                        });
                    }
                })
            });

            //SECTION Datatable
            tablaOrdenes = $('#tablaOrdenes').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                bFilter: true,
                bLengthChange: true,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('orden-compra.dt') }}",
                columns: [{
                        data: 'nombreCompleto'
                    },
                    {
                        data: 'fecha'
                    },
                    {
                        data: 'tipoOrden'
                    },
                    {
                        data: 'detalle',

                    },
                    {
                        class: 'text-right',
                        data: 'opciones'
                    },
                    {
                        data: 'created_at',
                        visible: false
                    },
                ],
                order: [5, 'desc']
            });
            //!SECTION

            //SECTION Filtrar

            // EVENTO Click filtrar 
            $('#btnFiltrar').click(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{{ route('orden-compra.filtrar') }}",
                    data: {
                        fechaInicio: $('#fechaInicio').val(),
                        fechaFin: $('#fechaFin').val(),
                        empleado: $('#proveedores').val(),
                        _token: "{{ csrf_token() }}",
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.error) {

                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#formResult').html(html);
                            return
                        } else {
                            tablaOrdenes.clear().rows.add(data.data).draw();
                            $('#badgeFiltro').removeClass('d-none');
                            var fechaInicio = new Date($('#fechaInicio').val())
                            var fechaFin = new Date($('#fechaFin').val())
                            var nombreProveedor = $('#proveedores :selected').text();
                            if ($('#proveedores').val() == "") {
                                $('#badgeFiltro').html('Filtrando entre ' + fecha.format(
                                        fechaInicio) +
                                    ' y ' + fecha.format(
                                        fechaFin) +
                                    '<i class="fas fa-times pl-3"></i>');
                            } else if ($('#fechaInicio').val() == "") {
                                $('#badgeFiltro').html('Filtrando por proveedor: ' +
                                    nombreProveedor + '<i class="fas fa-times pl-3"></i>');
                            } else {
                                $('#badgeFiltro').html('Filtrando entre ' + fecha.format(
                                        fechaInicio) + ' y ' +
                                    fecha.format(
                                        fechaFin) +
                                    '. Proveedor ' +
                                    nombreProveedor + '<i class="fas fa-times pl-3"></i>');
                            }
                            $('#modalFiltro').trigger('click');
                        }
                    }
                });
            });
            //EVENTO Click remover filtro
            //Cuando hago click en la X del filtro me vuelve a mostrar todos los registros nuevamente
            $('#badgeFiltro').on('click', 'i', function(e) {
                tablaOrdenes.ajax.reload();
                $('#badgeFiltro').addClass('d-none');
                $('#fechaInicio').val('')
            });


            var fecha = new Intl.DateTimeFormat("es-AR");

            //EVENTO Esconde modal filtro
            $("#modalFiltro").on("hide.bs.modal", function(event) {
                //Borro si hay algun error
                $('#formResult').children().remove();
                $('#proveedores option:eq(0)').prop('selected', true);
                $('#proveedores').trigger('change.select2');
                $('#fechaInicio').empty();
            });


            //!SECTION
        });
    </script>
@stop
