@extends('adminlte::page')
@section('plugins.Sweetalert2', true)
@section('title', 'Liquidaciones')

@section('content_header')
    <div class="d-flex justify-content-between">
        <h1>Liquidaciones</h1>
        <a href="{{ route('liquidaciones.create') }}" class="btn btn-success bg-hfrut">Liquidar a proveedor</a>
    </div>
@stop

@section('content')
    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: 'success',
                        title: '{{ session('status') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    <table class="table table-bordered table-rounded" id="tablaLiquidaciones">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Proveedor</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Cuotas</th>
                                <th scope="col">Observaciones</th>
                                <th scope="col" style="width: 20%;">Opciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(document).ready(function() {

            $(document).on('click', '.btnEliminarLiquidacion', function(e) {
                let id = $(this).data('id');
                Swal.fire({
                    title: '¿Desea dar de baja esta liquidación?',
                    text: '',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, dar de baja',
                    cancelButtonText: 'No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "PUT",
                            url: "/liquidaciones/delete/" + id,
                            data: "_token={{ csrf_token() }}",
                            dataType: "json",
                            success: function(data) {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 2300,
                                    timerProgressBar: true,
                                })
                                if (data.success) {
                                    tablaLiquidaciones.ajax.reload();
                                    Toast.fire({
                                        icon: 'success',
                                        title: data.success
                                    })
                                } else {
                                    Toast.fire({
                                        icon: 'error',
                                        title: data.error
                                    })
                                }
                            }
                        });
                    }
                })
            });

            //SECTION Datatable
            tablaLiquidaciones = $('#tablaLiquidaciones').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: true,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('liquidaciones.dt') }}",
                columns: [{
                        data: 'nombreCompleto'
                    },
                    {
                        data: 'fechaDeLiquidacion'
                    },
                    {
                        data: 'cantidad_cuotas'
                    },
                    {
                        data: 'observaciones',
                        render: function(observaciones) {
                            if (observaciones == null || observaciones == '') {
                                return 'No especificado';
                            } else {
                                return observaciones;
                            }
                        }
                    },
                    {
                        class: 'text-right',
                        data: 'opciones',
                    },
                    {
                        data: 'created_at',
                        visible: false,
                    },
                ],
                order: [5, 'desc']

            });
            //!SECTION
        });
    </script>
@stop
