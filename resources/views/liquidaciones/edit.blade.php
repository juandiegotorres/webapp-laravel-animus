@extends('adminlte::page')

@section('title', 'Liquidar a proveedor')

@section('content_header')
    {{-- <h1>Dashboard</h1> --}}
@stop

@section('content')

    <form action="{{ route('liquidaciones.update', ['liquidacion' => $liquidacion->id]) }}" method="POST">
        @method('PUT')
        @csrf
        <div class="row mt-4">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header pb-0">
                        <h3 class="card-title"><b>Liquidar a proveedor</b></h3>
                    </div>
                    <div class="card-body pb-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="proveedor">Proveedor (*)</label>
                                    <select type="text" name="proveedor" id="proveedor" class="form-control">
                                        @foreach ($proveedores as $proveedor)
                                            <option value="{{ $proveedor->id }}"
                                                {{ old('proveedor', $liquidacion->provider_id) == $proveedor->id ? 'selected' : '' }}>
                                                {{ $proveedor->nombreCompleto }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('proveedor')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fecha">Fecha (*)</label>
                                    <input type="date" name="fecha" id="fecha" class="form-control"
                                        value="{{ date('Y-m-d', strtotime(old('fecha', $liquidacion->fechaDeLiquidacion))) }}">
                                    @error('fecha')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Cantidad de cuotas (*)</label>
                                    <input type="number" name="cantidad_cuotas" id="cantidad_cuotas" class="form-control"
                                        value="{{ old('cantidad_cuotas', $liquidacion->cantidad_cuotas) }}">
                                    @error('cantidad_cuotas')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="observaciones">Observaciones</label>
                                    <textarea type="text" name="observaciones" id=""
                                        class="form-control">{{ old('observaciones', $liquidacion->observaciones) }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12 d-flex justify-content-end mt-2">
                                <button type="submit" class="btn btn-primary" id="btnGuardar">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop


@section('js')
    <script>
        $(document).ready(function() {
            $('#proveedor').select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

        });
    </script>
@stop
