@extends('adminlte::page')

@section('title', 'Liquidar a proveedor')

@section('content_header')
    @if ($fechas->count() == 2)
        <h4 class="mb-0 pb-0">Liquidacion a {{ $fechas[0]->proveedor->nombreCompleto }} desde
            {{ date('d-m-Y', strtotime($fechas[1]->fechaDeLiquidacion)) }} hasta
            {{ date('d-m-Y', strtotime($fechas[0]->fechaDeLiquidacion)) }}</h4>
    @else
        <h4 class="mb-0 pb-0">Liquidacion a {{ $fechas[0]->proveedor->nombreCompleto }} hasta la fecha
            {{ date('d-m-Y', strtotime($fechas[0]->fechaDeLiquidacion)) }}</h4>
    @endif
@stop

@section('css')
    <style>
        table {
            display: flex;
            flex-flow: column;
            width: 100%;
        }

        thead {
            position: sticky !important;
            top: 0 !important;
            background: white;
        }

        th {
            padding: 8px 10px !important;
            background: rgb(224, 224, 224);
            border: 1px solid rgb(165, 165, 165) !important;
        }

        td {
            border: 1px solid rgb(165, 165, 165) !important;
        }

        tbody {
            flex: 1 1 auto;
            display: block;
            overflow-y: auto;
            overflow-x: hidden;
        }

        tr {
            width: 100%;
            display: table;
            table-layout: fixed;
        }

    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-7">
            <div class="card">
                <div class="card-header bg-secondary pb-0">
                    <h3 class="card-title"><b>Compras</b></h3>
                </div>
                <div class="card-body py-1">
                    <div class="row" style="height: 55vh; max-height: 55vh;overflow: scroll; overflow-x: hidden;">
                        <table class="table table-bordered table-sm">
                            <thead class="">
                                <tr class="fuente-header">
                                    <th scope="col" class="header">Materia Prima</th>
                                    <th scope="col" class="header">Fecha</th>
                                    <th scope="col" class="header">Cantidad</th>
                                    <th scope="col" class="header">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $totalCompras = 0;
                                @endphp
                                @foreach ($compras as $compra)
                                    <tr>
                                        <td>{{ $compra->fruta . ', ' . $compra->variedad }}</td>
                                        <td>{{ date('d-m-Y', strtotime($compra->fecha)) }}</td>
                                        <td>{{ $compra->cantidad }}</td>
                                        <td>${{ number_format($compra->totalParcial, 2, ',', '.') }}</td>
                                    </tr>
                                    @php
                                        $totalCompras += $compra->total;
                                    @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex flex-column">
                        <h5 class="my-1 text-right">Total IVA: <b>${{ number_format($totalIvaCompras, 2, ',', '.') }}</b>
                        </h5>
                        <h5 class="mb-0 text-right">Total Compras:
                            <b>${{ number_format($totalCompras, 2, ',', '.') }}</b>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="card">
                <div class="card-header bg-secondary pb-0">
                    <h3 class="card-title"><b>Pagos</b></h3>
                </div>
                <div class="card-body py-1">
                    <div class="row"
                        style="height: 55vh; max-height: 55vh;overflow: scroll; overflow-x: hidden;">
                        <table class="table table-bordered table-sm">
                            <thead class="">
                                <tr class="fuente-header">
                                    <th scope="col" class="header">Fecha</th>
                                    <th scope="col" class="header">Monto</th>
                                </tr>
                            </thead>
                            @php
                                $totalIvaPagos = 0;
                                $totalPagos = 0;
                            @endphp
                            <tbody>
                                @foreach ($pagos as $pago)
                                    <tr>
                                        <td>{{ date('d-m-Y', strtotime($pago->fecha)) }}</td>
                                        <td>$ {{ number_format($pago->montoTotal, 2, ',', '.') }}</td>
                                    </tr>
                                    @php
                                        $totalIvaPagos += $pago->iva;
                                        $totalPagos += $pago->montoTotal;
                                    @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex flex-column">
                        <h5 class="my-1 text-right">Total IVA: <b>${{ number_format($totalIvaPagos, 2, ',', '.') }}</b>
                        </h5>
                        <h5 class="mb-0 text-right">Total Pagos: <b>${{ number_format($totalPagos, 2, ',', '.') }}</b>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-end px-3">
        <a href="{{ route('liquidacion.pdf', ['liquidacion' => $fechas[0]->id]) }}" target="_blank"
            class="btn btn-warning"><i class="fa fa-print mr-2" aria-hidden="true"></i> Imprimir liquidación</a>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {
            $('#proveedor').select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

        });
    </script>
@stop
