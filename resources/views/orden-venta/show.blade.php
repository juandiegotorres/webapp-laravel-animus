@extends('adminlte::page')

@section('title', 'Ver orden de venta')

@section('content_header')
    <h1>Orden de venta</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="invoice p-3 mb-3">
                <div class="row">
                    <div class="col-12">
                        <h4>
                            <i class="fas fa-globe"></i> HFRUT
                            <small class="float-right">Fecha:
                                {{ date('d-m-Y', strtotime($orden->fecha)) }}</small>
                        </h4>
                    </div>
                </div>
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col my-3">
                        ID Orden de venta: <strong>#{{ $orden->id }}</strong> <br>
                        Cliente: <strong>{{ $orden->cliente->nombreCompleto }}</strong><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped text-center">
                            <thead>
                                <tr>
                                    <th>Variante Producto</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($detalles as $detalle)
                                    <tr>
                                        @php
                                            switch ($detalle->tipo) {
                                                case '0':
                                                    $tipoProducto = 'Caja, ' . $detalle->kg . ' kg(s)';
                                                    break;
                                                case '1':
                                                    $tipoProducto = 'Bolsa, ' . $detalle->kg . ' kg(s)';
                                                    break;
                                                default:
                                                    break;
                                            }
                                        @endphp
                                        <td>{{ $detalle->nombreCompleto . ' - ' . $tipoProducto }}
                                        </td>
                                        <td>{{ $detalle->cantidad }}</td>
                                        <td>${{ number_format($detalle->precio, 2, ',', '.') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                    </div>
                    <div class="col-6">
                        <h2 class="text-right pr-3 my-3">Total: ${{ number_format($orden->total, 2, ',', '.') }}</h2>
                    </div>
                </div>
                <div class="row no-print">
                    <div class="col-12">

                        <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-danger float-right">Volver</a>


                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
