@extends('adminlte::page')

@section('title', 'Nuevo Cliente')

@section('plugins.Select2', true)

@section('content_header')
    <p class="pl-4 h2">Pago de sueldos</p>
@stop

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">

                <div class="card card-primary border-top">
                    {!! Form::model($salaryPayment, ['route' => ['salary-payments.update', $salaryPayment], 'method' => 'put', 'class' => 'p-4']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('employee_id', 'Empleado (*)') !!}
                                {!! Form::select('employee_id', $empleados->pluck('nombreCompleto', 'id'), null, ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'empleados']) !!}

                                @error('employee_id')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('fecha', 'Fecha (*)') !!}
                                {!! Form::date('fecha', null, ['class' => 'form-control']) !!}
                                @error('fecha')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('total', 'Total a pagar (*)') !!}
                                {!! Form::number('total', null, ['autocomplete' => 'off', 'id' => 'txtTotal', 'class' => 'form-control', 'placeholder' => 'Total...']) !!}
                                @error('total')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('concepto', 'Concepto (*)') !!}
                                {!! Form::textarea('concepto', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Concepto...', 'rows' => '3']) !!}
                                @error('concepto')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>




                    <div class="row mt-3">
                        <div class="col-md-12">
                            <a href="{{ route('salaries.index') }}" class="btn btn-danger mr-2">
                                Cancelar
                            </a>
                            {!! Form::submit('Agregar pago de sueldo', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        $(document).ready(function() {

            $('#empleados').select2();

        })
    </script>

@stop
