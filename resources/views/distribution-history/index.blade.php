@extends('adminlte::page')

@section('title', 'Historial de entregas')

@section('css')
    <style>
        ul {
            margin-bottom: 0 !important;
        }

    </style>
@stop
@section('content_header')
    <div class="row justify-content-between px-4">
        <h1>Historial de entregas</h1>
        <a class="btn btn-success bg-hfrut" data-toggle="modal" data-target="#modalEmpleados" id="btnHacerEntrega">Hacer
            entrega de indumentaria</a>
    </div>
@stop

@section('content')

    {{-- MODAL MOSTRAR EMPLEADOS --}}
    <div class="modal fade" id="modalEmpleados" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title">Seleccione un empleado</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="empleados" class="col-form-label">Empleado:</label>
                                <select name="empleados" id="empleados" class="form-control"
                                    style="width:100%!important;">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-form-label">DNI:</label>
                                <input type="text" class="form-control" readonly id="dni">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-form-label">Cargo:</label>
                                <input type="text" class="form-control" readonly id="cargo">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <a class="btn btn-success" id="btnSeleccionar">Seleccionar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <input name="" class="form-control" id="nombreEmpleado" disabled></input>
            </div>
            <div class="col-md-4">
                <button class="btn btn-success bg-hfrut mr-2" data-toggle="modal" data-target="#modalEmpleados"
                    id="btnFiltroEmpleado">Seleccionar
                    empleado</button>

                <button class="btn btn-danger" data-toggle="tooltip" data-placement="right"
                    title="Eliminar filtrado por empleado" id="btnEliminarFiltro" disabled><i
                        class="fa fas fa-times-circle"></i></button>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered" id="tablaHistorialDistribucion">
                    <thead class="bg-hfrut">

                        <tr>
                            <th>Fecha</th>
                            <th>Insumos - Cantidad</th>
                            <th style="width: 40%;">Detalle</th>
                            <th>Empleado</th>
                            <th style="width: 8em !important;">Opciones</th>
                            {{-- <th style="width:10px">Eliminar</th> --}}
                        </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div>
@stop




@section('js')
    <script>
        $(document).ready(function() {

            $('[data-toggle="tooltip"]').tooltip()
            $('#empleados').select2();

            var nuevaEntrega = false;

            $('#empleados').change(function(e) {
                var selected = $(this).find('option:selected');
                var dni = selected.data('dni');
                var cargo = selected.data('cargo');
                $('#dni').val(dni);
                $('#cargo').val(cargo);
                // $('#btnComprarIS').attr('href', url);
            });

            $(document).on('click', '.btnEliminar', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea eliminar esta entrega?",
                    // text: "Una vez eliminada, no se puede recuperar",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post(`/historial-entrega-indumentaria/eliminar/` + id, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    tablaHistorialDistribucion.ajax.reload();
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: "Entrega eliminada",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                    }
                });
            });

            //======================CREACION DE DATATABLE ======================
            var tablaHistorialDistribucion = $('#tablaHistorialDistribucion').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                bFilter: false,
                bLengthChange: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: {
                    url: '/historial-entrega-indumentaria/empleado/',
                },
                columns: [{
                        data: 'fecha',
                    },
                    {
                        data: 'indumentariaCantidad',
                    },
                    {
                        data: 'detalle',
                    },
                    {
                        data: 'empleado',
                        visible: true
                    },
                    {
                        data: 'eliminar',
                        class: 'text-center'
                    }
                ],
                order: [
                    [0, "desc"]
                ]
            });;


            //================== PETICION PARA LLENAR SELECT EMPLEADOS ===================
            axios.get('/obtener-empleados-json').then((r) => {
                var empleados = r.data;
                for (var i = 0; i < empleados.length; i++) {
                    $('#empleados').append('<option value=' + empleados[i].id +
                        ' data-dni=' + empleados[i].dni + ' data-cargo=' + empleados[i]
                        .nombre + '>' +
                        empleados[i].nombreCompleto +
                        '</option>');
                }
                //Cargo los valores en los inputs del modal por si hay 2 o mas empleados con un mismo nombre
                //puedan diferenciarse por el DNI
                $('#dni').val(empleados[0].dni);
                $('#cargo').val(empleados[0].nombre);
            });

            //================== EVENTO CUANDO SE ABRE EL MODAL ===================
            $("#modalEmpleados").on("show.bs.modal", function(event) {
                //Capturo el boton desde donde se abrio el modal
                var button = $(event.relatedTarget);
                //Lo idenfitico mediante un if y si es el boton para hacer una nueva entrega seteo la variable global en true
                if (button.attr('id') == 'btnHacerEntrega') {
                    nuevaEntrega = true;
                } else {
                    nuevaEntrega = false;
                }
            });

            //============= EVENTO CLICK BOTON ELIMINAR FILTRO EMPLEADO ===============
            $('#btnEliminarFiltro').click(function(e) {
                e.preventDefault();
                $('#nombreEmpleado').val('');
                //Muestro la columna de la tabla que me muesta el nombre del empleado
                tablaHistorialDistribucion.column(2).visible(true);
                //LLeno la tabla con los datos nuevos
                tablaHistorialDistribucion.ajax.reload();
                //Escondo el tooltip del boton y lo deshabilito
                $(this).tooltip('hide');
                $(this).attr("disabled", true);
            });

            //============== EVENTO BOTON PARA CONFIRMAR DENTRO DEL MODAL ======================
            $('#btnSeleccionar').click(function(e) {
                e.preventDefault();
                //Obtengo los datos y los guardo en una variable respectivamente
                var nombreEmpleado = $('#empleados option:selected').text();
                var idEmpleado = $('#empleados').val();
                var dni = $('#dni').val();

                //Pregunto el valor de la variable global, si es true redirigo a la pantalla para hacer la entrega
                //Sino filtro la tabla
                if (nuevaEntrega == true) {
                    window.location.href = "/entregar-indumentaria/" + idEmpleado;
                } else {
                    $('#nombreEmpleado').val(nombreEmpleado + ', DNI: ' + dni);

                    cargarTabla(idEmpleado);

                    $('#btnEliminarFiltro').tooltip('hide');
                    $('#btnEliminarFiltro').attr("disabled", false);

                    $('#modalEmpleados').trigger('click');
                }

            });

            //======================FUNCION CARGAR TABLA SEGUN EMPLEADO ======================
            function cargarTabla($id) {
                $.get('/historial-entrega-indumentaria/empleado/' + $id,
                    function(data) {
                        if (data.recordsTotal == 0) {
                            tablaHistorialDistribucion.clear().draw();
                            Swal.fire(
                                'Sin registros',
                                'No se le ha hecho una entrega de indumentaria a este empleado aún.',
                                'warning'
                            )
                        } else {
                            tablaHistorialDistribucion.clear().rows.add(data.data).draw();
                            tablaHistorialDistribucion.column(2).visible(false);
                        }
                    },
                    "json"
                );
            }
        })
    </script>
@stop
