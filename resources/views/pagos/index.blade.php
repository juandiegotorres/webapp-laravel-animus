@extends('adminlte::page')

@section('plugins.Sweetalert2', true)

@section('title', 'Dashboard')

@section('content_header')
    <div class="float-right pr-2">
        <a class="btn btn-success bg-hfrut" data-toggle="modal" data-target="#modalProveedores">
            Agregar pago a proveedor
        </a>
    </div>
    <h1 class="pl-2">Pagos</h1>
@stop


@section('content')
    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: 'success',
                        title: '{{ session('status') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    {{-- TABLA --}}
                    <div class="row px-2">
                        <div class="col-md-4 xd">
                            <label for="proveedores">Filtrar por proveedor</label>
                            <select name="proveedores" id="filtroProveedores" class="form-control filter-select">
                                <option value="0">Ninguno</option>
                                @foreach ($proveedores as $proveedor)
                                    <option value="{{ $proveedor->id }}">{{ $proveedor->nombreCompleto }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-1 mt-2 pl-0">
                            <button class="btn btn-danger flex-fill mt-4 mb-0" data-toggle="tooltip" data-placement="right"
                                title="Eliminar filtrado por proveedor" id="btnEliminarFiltroProveedor" disabled><i
                                    class="fa fas fa-times-circle"></i></button>
                        </div>
                    </div>
                    <hr>
                    <table class="table table-bordered table-rounded" id="tablaPagos">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Proveedor</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">IVA</th>
                                <th scope="col">Monto total</th>
                                <th scope="col">Método de pago</th>
                                <th scope="col">Observaciones</th>
                                <th scope="col" style="width:15px">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>
    {{-- MODAL PROVEEDORES --}}
    <div class="modal fade" id="modalProveedores" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title" id="modalTitulo">Proveedores</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="proveedores" class="col-form-label">Seleccione el proveedor:</label>
                                <select name="proveedores" id="proveedores" class="form-control">
                                </select>
                                <span class="invalid-feedback" role="alert" id="errorAgregar">
                                    <strong>Error</strong>
                                </span>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-form-label">CUIT:</label>
                                <input type="text" class="form-control" readonly id="cuit">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-form-label">Teléfono:</label>
                                <input type="text" class="form-control" readonly id="telefono">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="col-form-label">Dirección:</label>
                                <input type="text" class="form-control" readonly id="direccion">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <a href="" class="btn btn-success" id="btnComprar">Proceder con el pago </a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalMetodosPago" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title" id="modalTitulo">Metodos de pago usados:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Metodo de pago</th>
                                        <th>Monto</th>
                                        <th>Nro Cheque</th>
                                    </tr>
                                </thead>
                                <tbody id="tablaMetodoPago">

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {
            $('#filtroProveedores').select2();


            //==================== LLENAR SELECT DEL MODAL CON LOS PROVEEDORES ===================
            axios.get('/proveedores-json').then((r) => {
                var proveedores = r.data;
                for (var i = 0; i < proveedores.length; i++) {
                    $('#proveedores').append('<option value=' + proveedores[i].id +
                        ' data-cuit=' + proveedores[i].cuit + ' data-telefono=' +
                        proveedores[i].telefono + ' data-direccion=' + encodeURIComponent(
                            proveedores[i]
                            .direccion) + '>' +
                        proveedores[i].nombreCompleto +
                        '</option>');
                }
                $('#cuit').val(proveedores[0].cuit);
                $('#telefono').val(proveedores[0].telefono);
                $('#direccion').val(proveedores[0].direccion);
                var idProv = $('#proveedores').val();
                var url = '/pagos/create/' + idProv;
                $('#btnComprar').attr('href', url);
            });

            //Hacer focus en la barra de busqueda cuando se abre el select de bancos
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            $('#proveedores').select2({
                width: '100%',
            });
            //========================EVENTO MODAL VER METODOS DE PAGO =====================================
            $('#modalMetodosPago').on('show.bs.modal', function(e) {
                var button = $(e.relatedTarget)
                var idPago = button.data('id')

                $.get("/pagos/obtener-metodos-pago/" + idPago,
                    function(data) {
                        var html = "";
                        data.forEach(pago => {
                            var cheque = '';
                            pago.nroCheque == null ? cheque = "" : cheque = pago.nroCheque;
                            html += "<tr><td>" + pago.nombre + "</td>" +
                                "<td>" + formatear.format(pago.monto) + "</td>" +
                                "<td>" + cheque + "</td></tr>"
                        });
                        $('#tablaMetodoPago').html(html);
                        console.log(html);
                    },
                    "json"
                );
            })

            $('#modalMetodosPago').on('hide.bs.modal', function(e) {
                $('#tablaMetodoPago').empty();
            })


            //============== EVENTO CADA VEZ QUE SE CAMBIA DE PROVEEDOR EN EL SELECT ===============
            $('#proveedores').change(function(e) {
                var selected = $(this).find('option:selected');
                var cuit = selected.data('cuit');
                var telefono = selected.data('telefono');
                var direccion = decodeURIComponent(selected.data('direccion'));
                $('#cuit').val(cuit);
                $('#telefono').val(telefono);
                $('#direccion').val(direccion);
                var idProv = $('#proveedores').val();
                var url = '/pagos/create/' + idProv;
                $('#btnComprar').attr('href', url);
            });


            //========================= EVENTO CAMBIA EL SELECT DE CLIENTES ======================
            $('#filtroProveedores').change(function(e) {
                tablaPagos.ajax.reload();
                $('#btnEliminarFiltroProveedor').prop('disabled', false);
            });


            //======================= CLICK BOTON ELIMINAR FILTRO
            $('#btnEliminarFiltroProveedor').click(function(e) {
                e.preventDefault();
                $('#filtroProveedores').val(0).change();
                $(this).prop('disabled', true);
            });

            //======================== ELIMINAR FACTURA ===================================
            $(document).on('click', '.btnEliminarPago', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea dar de baja esta pago?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post('/pagos/eliminar/' + id, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    tablaPagos.ajax.reload();
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: "Pago dada de baja",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                            tablaPagos.ajax.reload();
                        }
                    }
                });
            });

            //==================== DATATABLE ===========================
            tablaPagos = $('#tablaPagos').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: true,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: {
                    "url": "{{ route('pagos.dt') }}",
                    "data": function(d) {
                        d.idproveedor = $('#filtroProveedores').val();
                        d._token = "{{ csrf_token() }}";
                    },
                    "type": "POST",
                    "dataType": "json"
                },
                columns: [{
                        data: 'nombreCompleto'
                    },
                    {
                        data: 'fecha'
                    },
                    {
                        data: 'iva'
                    },
                    {
                        data: 'montoTotal'
                    },
                    {
                        data: 'metodoPago',
                    },
                    {
                        data: 'observaciones',
                        render: function(observaciones) {
                            if (observaciones == null) {
                                return 'Sin observaciones';
                            } else {
                                return observaciones;
                            }
                        }
                    },
                    {
                        data: 'opciones',
                    },
                    {
                        data: 'created_at',
                        visible: false
                    },
                ],
                order: [6, 'desc']
            });

            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });
        });
    </script>
@stop
