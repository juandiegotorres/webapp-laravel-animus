@extends('adminlte::page')

@section('title', 'Agregar Pago')

@section('content_header')
    <h1 class="pl-3">Agregar pago a proveedor: </h1>
    <h5 class="pl-3 mb-0 pb-0">{{ $proveedor->nombreCompleto . ' - CUIT: ' . $proveedor->cuit }}</h5>
@stop

@section('content')
    <div class="col-md-10 col-sm-10 col-lg-10">
        <div class="card">
            <div class="card-body" id="app">
                <div class="col-md-12" id="resultadoForm"></div>
                {!! Form::open(['class' => '', 'id' => 'formPago']) !!}
                {!! Form::hidden('provider_id', $proveedor->id) !!}
                @error('provider_id')
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('fecha', 'Fecha (*)', ['class' => 'col-form-label']) !!}
                            {!! Form::date('fecha', now(), ['class' => 'form-control']) !!}
                            @error('fecha')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('metodoPago', 'Método de pago (*)', ['class' => 'col-form-label']) !!}
                            {!! Form::select('metodoPago', $metodosPago->pluck('nombre', 'id'), null, ['class' => 'form-control', 'id' => 'metodosPago']) !!}
                            @error('metodoPago')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('montoTotal', 'Monto (*)', ['class' => 'col-form-label']) !!}
                            {!! Form::number('', null, ['class' => 'form-control', 'placeholder' => 'Monto...', 'id' => 'montoPago']) !!}
                            <span class="invalid-feedback d-block" role="alert" id="error-montopago">
                                <strong></strong>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="" class="col-form-label">ㅤ</label>
                            <a id="agregarMetodoPago" class="btn btn-primary form-control">Agregar</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped d-none">
                            <thead>
                                <tr>
                                    <th scope="col">Método Pago</th>
                                    <th scope="col">Nro Cheque (opcional)</th>
                                    <th scope="col">Monto</th>
                                </tr>
                            </thead>
                            <tbody id="tablaPagos">

                            </tbody>
                            <tfoot class="mt-3">
                                <tr class="text-right">
                                    <td colspan="3">
                                        <h4 id="total"></h4>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="iva">IVA</label>
                            <input type="number" name="iva" id="iva" class="form-control" placeholder="IVA...">
                        </div>
                    </div>
                    <div class="col-md-12">
                        {!! Form::label('observaciones', 'Observaciones', ['class' => 'col-form-label']) !!}
                        {!! Form::textarea('observaciones', null, ['class' => 'form-control', 'rows' => '2']) !!}
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 d-flex justify-content-end">
                        <a class="btn btn-danger" onclick="window.history.back();">Volver</a>
                        <button class="btn btn-success ml-2" id="btnGuardar">Guardar</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- ======= MODAL CHEQUES ========== --}}
    <div class="modal fade" id="modalCheques" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cheques</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Nro Cheque</th>
                                <th scope="col">Tipo Cheque</th>
                                <th scope="col">Importe</th>
                                <th scope="col">Fecha de creación</th>
                                <th scope="col" class="text-center">Seleccionar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cheques as $cheque)
                                <tr>
                                    <td class="nroCheque">{{ $cheque->nroCheque }}</td>
                                    <td class="tipoCheque">{{ ucfirst($cheque->tipoCheque) }}</td>
                                    <td class="importe">$ {{ number_format($cheque->importe, 2, ',', '.') }}<input
                                            type='hidden' value="{{ $cheque->importe }}"></td>
                                    <td>{{ date('d/m/y', strtotime($cheque->created_at)) }}</td>
                                    <td class="text-center"><button class="btn btn-success btn-sm btnSeleccionarCheque"
                                            data-id="{{ $cheque->id }}">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </button></td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {
            var total = 0;
            //============== EVENTO QUE ABRE EL MODAL DE CHEQUES CUANDO SE SELECCIONA EN EL SELECT =====================
            $('#metodosPago').change(function(e) {
                e.preventDefault();
                //Significa que seleccione un cheque
                if ($("#metodosPago option:selected").val() == 3) {
                    $('#modalCheques').modal('show');
                }
                $('#montoPago').focus();
            });

            //EVENTO Hide 
            $('#modalSeleccionarCheque').on('hide.bs.modal', function() {
                $("#metodosPago").val($("#metodosPago option:first").val())
            });

            //=============== EVENTO CLICK EN EL BOTON DENTRO DEL MODAL PARA SELCCIONAR EL CHEQUE ===================
            $(document).on('click', '.btnSeleccionarCheque', function(e) {
                e.preventDefault();
                var idcheque = $(this).data('id');
                var nroCheque = $(this).closest('td').siblings('td.nroCheque').text();
                var importe = $(this).closest('td').siblings('td.importe').children().val();
                $(this).attr('disabled', true);
                agregarPagoTabla(nroCheque, idcheque, importe);
                $('#modalCheques').trigger('click');
            });

            //================== AGREGAR UN METODO DE PAGO CON SU MONTO RESPECTIVO ==================================
            $('#agregarMetodoPago').click(function(e) {
                e.preventDefault();
                var error = 0;
                if (!$('#montoPago').val()) {
                    invalidFeedback('#montoPago', '#error-montopago', 'El campo monto no puede esar vacio')
                    error += 1
                } else if (0 > $('#montoPago').val()) {
                    invalidFeedback('#montoPago', '#error-montopago',
                        'El campo monto no puede ser menor que 0')
                    error += 1
                } else {
                    removeFeedback('#montoPago', '#error-montopago');
                }

                if (error == 0) {
                    agregarPagoTabla('', '', $('#montoPago').val())
                }
            });
            //====================== FUNCION QUE AGREGA EL HTML A LA TABLA ========================================
            function agregarPagoTabla(nroCheque, idCheque, importe) {
                var metodoPago = $("#metodosPago option:selected").text();
                var idMetodoPago = $("#metodosPago option:selected").val();
                var row = '<tr>' +
                    '<td>' + metodoPago + '<input type="hidden" name="id_metodo_pago[]" value="' + idMetodoPago +
                    '"></td>' +
                    '<td>' + nroCheque + '<input type="hidden" name="id_cheque[]" value="' + idCheque +
                    '"></td>' +
                    '<td>' + formatear.format(importe) + '<input type="hidden" name="importe[]" value="' + importe +
                    '"></td>';
                $('table').removeClass('d-none');
                total += parseInt(importe);
                $('#total').text('Total: ' + formatear.format(total));
                $('#tablaPagos').append(row);
                $('#montoPago').val('');
                $("#metodosPago").val($("#metodosPago option:first").val());
                $('#montoPago').focus();
            }


            //====================== GUARDAR PAGO A PROVEEDOR ===========================================
            $('#btnGuardar').click(function(e) {
                e.preventDefault();
                var cantidadFilas = $('#tablaPagos tr').length;
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 4200,
                    timerProgressBar: true,
                })

                if (cantidadFilas == 0) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Debe agregar al menos un metodo de pago'
                    })
                    return;
                } else if (!$('#iva').val()) {
                    Toast.fire({
                        icon: 'error',
                        title: 'El campo IVA no puede estar vacio'
                    })
                    return;
                } else if ($('#iva').val() < 0) {
                    Toast.fire({
                        icon: 'error',
                        title: 'El campo IVA no puede ser menor que 0'
                    })
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: "{{ route('pagos.store') }}",
                    data: $('#formPago').serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.error) {
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 my-2" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoForm').html(html);
                        }

                        if (data.success) {
                            document.location.href = "{{ route('pagos.index') }}";
                        }
                    }
                });
            });

            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });


            //==================== FUNCION ERROR DE VALIDACION ============================
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }

        });
    </script>
@stop
