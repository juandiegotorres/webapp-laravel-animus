@extends('layouts.app')

@section('content')
    <div class="contenedor-centrado">
        <div class="col-md-4">
            <div class="card">

                <div class="card-body v-padding">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row justify-content-center">


                            <div class="col-md-10">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-hfrut" id="basic-addon1"><i
                                                class="fas fa-user"></i></span>
                                    </div>
                                    <input id="name" type="text"
                                        class="border-hfrut form-control @error('name') is-invalid @enderror" name="name"
                                        value="{{ old('name') }}" required autocomplete="name" autofocus
                                        placeholder="Nombre">

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>


                            </div>
                        </div>

                        <div class="form-group row justify-content-center">

                            <div class="col-md-10">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-hfrut" id="basic-addon1"><i
                                                class="fas fa-envelope"></i></span>
                                    </div>
                                    <input id="email" type="email"
                                        class="border-hfrut form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" required autocomplete="email"
                                        placeholder="Correo electrónico">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-10">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-hfrut" id="basic-addon1"><i
                                                class="fas fa-key"></i></span>
                                    </div>
                                    <input id="password" type="password"
                                        class="border-hfrut form-control @error('password') is-invalid @enderror"
                                        name="password" required autocomplete="new-password" placeholder="Contraseña">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                            </div>
                        </div>

                        <div class="form-group row justify-content-center">

                            <div class="col-md-10">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-hfrut" id="basic-addon1"><i
                                                class="fas fa-key"></i></span>
                                    </div>
                                    <input id="password-confirm" type="password" class="border-hfrut form-control"
                                        name="password_confirmation" required autocomplete="new-password"
                                        placeholder="Confirme su contraseña">
                                </div>

                            </div>
                        </div>

                        <div class="form-group row pt-4 justify-content-center">
                            <div class="col-md-8 ">
                                <button type="submit" class="btn btn-block btn-login">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
