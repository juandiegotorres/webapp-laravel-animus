@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="px-4">
        <div class="row mb-1">
            <h1>Compra desde orden</h1>
        </div>

        <div class="row d-flex justify-content-between align-items-center">
            <h5 class="mb-0 pt-2">Proveedor: <span class="text-muted">{{ $orden->proveedor->nombreCompleto }} -
                    CUIT:
                    {{ $orden->proveedor->cuit }}</span></h5>
            <div class="col-md-3">
                <input type="date" class="form-control" id="fechaElegida" value="{{ now()->format('Y-m-d') }}"
                    max="{{ now()->format('Y-m-d') }}" min="{{ now()->subDays(7)->format('Y-m-d') }}">
            </div>
        </div>
    </div>
    <hr class="mb-0">
@stop

@section('content')
    @if ($orden->tipoOrden == 'IS')
        <form action="{{ route('purchase-supplie.store-from-orden') }}" method="POST" id="formOrden">
        @else
            <form action="{{ route('purchase-indumentarie.store-from-orden') }}" method="POST" id="formOrden">
    @endif
    <div class="row mt-0">
        <div class="col-md-12">
            <div id="resultForm">

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @csrf
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col" style="width: 25%;">Nombre</th>
                        <th scope="col" style="width: 20%;">Cantidad</th>
                        <th scope="col" style="width: 25%;">Precio</th>
                        <th scope="col" style="width: 30%;" class="totales">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($detalles as $detalle)
                        <tr>
                            <td>{{ $detalle->nombre }} <input type="hidden" name="id_objeto[]"
                                    value="{{ $detalle->id_objeto }}"></td>
                            <td><input type="number" class="cantidad form-control" name="cantidad[]"
                                    value="{{ $detalle->cantidad }}"></td>
                            <td><input type="number" class="precio form-control" name="precio[]"></td>
                            <td><input type="number" class="total form-control" readonly id=""></td>
                        </tr>
                    @endforeach
                </tbody>
                <input type="hidden" name="id_orden" value="{{ $orden->id }}">
                <input type="hidden" name="fecha" id="fechaCompra" value="">
                <input type="hidden" name="tipo_orden" value="{{ $orden->tipoOrden }}">
                <input type="hidden" name="id_proveedor" value="{{ $orden->provider_id }}">
            </table>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="chbDetalle">
                <label class="form-check-label" for="chbDetalle">
                    Agregar detalle de compra
                </label>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group mb-0 pr-2 d-none" id="detalle">
                        <label for="detalle" class="col-form-label">Detalles de la compra:</label>
                        <textarea name="detalle" id="" cols="30" rows="2" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row justify-content-end px-3 ">
                <div class="form-group pr-2">
                    <input type="number" name="iva" class="form-control text-right" placeholder="IVA" id="iva">
                    <span class="invalid-feedback w-100 text-right" role="alert" id="error-iva">
                        <strong>Error</strong>
                    </span>
                </div>
            </div>
            <hr>
            <div class="row justify-content-between px-3 mb-3">
                <button class="btn btn-warning" id="finalizarCompra" type="submit">
                    Finalizar compra
                </button>
                <div class="col-md-3 text-right">
                    <input type="hidden" name="total" id="montoTotal">
                    <h3>Total <b id="total"></b></h3>
                </div>
            </div>
        </div>
        </form>
    @stop


    @section('js')
        <script>
            $(document).ready(function() {
                //EVENTO Detalle compras
                $('#chbDetalle').click(function(e) {
                    if ($(this).is(":checked")) {
                        $('#detalle').removeClass('d-none');
                    } else {
                        $('#detalle').addClass('d-none');
                    }
                });


                //EVENTO KEYUP DE CANTIDAD Y PRECIO PARA CALCULAR EL SUBTOTAL
                $('.cantidad').keyup(function(e) {
                    var cantidad = $(this).val();
                    var precio = $(this).closest('td').next('td').find('.precio').val();
                    var subtotal = cantidad * precio;
                    $(this).closest('td').next('td').next('td').find('.total').val(subtotal);
                    calc_total();
                });

                $('.precio').keyup(function(e) {
                    var precio = $(this).val();
                    var cantidad = $(this).closest('td').prev('td').find('.cantidad').val();
                    var subtotal = cantidad * precio;
                    $(this).closest('td').next('td').find('.total').val(subtotal);
                    calc_total();
                });

                //EVENTO Finalizar compra

                $('#finalizarCompra').click(function(e) {
                    e.preventDefault();
                    if (!$('#iva').val()) {
                        invalidFeedback('#iva', '#error-iva', 'Debe especificar el IVA');
                        return
                    } else if (0 > $('#iva').val()) {
                        invalidFeedback('#iva', '#error-iva', 'Introduzca un IVA válido');
                        return
                    }
                    removeFeedback('#iva', '#error-iva');
                    if (!comprobar_campos()) {
                        Swal.fire(
                            'Error',
                            'No puede haber campos vacios o con valores menores/iguales a 0',
                            'error'
                        )
                        return;
                    }
                    $('#fechaCompra').val($('#fechaElegida').val());
                    var form = $(this).closest("form");
                    var name = $(this).data("name");
                    Swal.fire({
                            title: `¿Desea realizar esta compra?`,
                            text: "",
                            icon: "question",
                            showCancelButton: true,
                            confirmButtonText: 'Si',
                            confirmButtonColor: '#29643e',
                            cancelButtonText: 'No',
                        })
                        .then((response) => {
                            if (response.isConfirmed) {
                                let url_ = $('#formOrden').attr('action');
                                $.ajax({
                                    type: "POST",
                                    url: url_,
                                    data: $('#formOrden').serialize() +
                                        "&_token={{ csrf_token() }}",
                                    dataType: "json",
                                    success: function(response) {
                                        const Toast = Swal.mixin({
                                            toast: true,
                                            position: 'top-end',
                                            showConfirmButton: false,
                                            timer: 2300,
                                            timerProgressBar: true,
                                        })

                                        if (response.success) {
                                            Toast.fire({
                                                icon: 'success',
                                                title: response.success +
                                                    '. Espere unos segundos'
                                            }).then(() => {
                                                window.location.href =
                                                    "{{ route('orden-compra.listar') }}";
                                            })
                                        }

                                        if (response.error) {
                                            html =
                                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                                            for (var i = 0; i < response.error.length; i++) {
                                                html += '<li>' + response.error[i] + '</li>';
                                            }
                                            html += '</ul></div>';
                                            $('#resultForm').html(html);
                                        }
                                    }
                                });
                            }
                        });
                });



                //EVENTO calcular total
                function calc_total() {
                    var total = 0;
                    $(".total").each(function() {
                        if ($(this).val() == '') {
                            total += 0;
                        } else {
                            total += parseFloat($(this).val());
                        }
                    });
                    $('#total').text(formatear.format(total));
                    $('#montoTotal').val(total);
                }
                //EVENTO validar campos
                function comprobar_campos() {
                    let respuesta = true
                    $(".total").each(function(key, value) {
                        if (!$(this).val() || $(this).val() <= 0) {
                            respuesta = false;
                        }
                    });
                    return respuesta;
                }

                var formatear = new Intl.NumberFormat('de-DE', {
                    style: 'currency',
                    currency: 'USD',
                });

                //==================== FUNCION ERROR DE VALIDACION ============================
                function invalidFeedback(input, labelInvalid, text) {
                    $(input).addClass('is-invalid');
                    $(labelInvalid).children().text(text);
                }
                //==================== FUNCION SACAR ERROR DE VALIDACION ============================
                //Revierte la funcion de invalidFeedback()
                function removeFeedback(input, labelInvalid) {
                    $(input).removeClass('is-invalid');
                    $(labelInvalid).children().text('');
                }

            });
        </script>
    @stop
