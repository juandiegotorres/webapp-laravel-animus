@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="d-flex justify-content-between">
        <h1 class="pl-3">Balance de compras-pagos a proveedores</h1>
        <button class="btn btn-warning" disabled id="btnImprimir"><i class="fa fa-print mr-2" aria-hidden="true"></i> Imprimir
            balance en
            pantalla</button>
        <a class="d-none" target="_blank" href="" id="btnPDF"></a>
    </div>
@stop


@section('content')
    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: 'error',
                        title: '{{ session('status') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    {{-- MODAL MOSTRAR PROVEEDORES --}}

    <div class="modal fade" id="modalProveedores" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title" id="modalTitulo">Proveedores</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="proveedores" class="col-form-label">Seleccione el proveedor:</label>
                                <select name="proveedores" id="proveedores" class="form-control">
                                    <span class="invalid-feedback" role="alert" id="errorAgregar">
                                        <strong>Error</strong>
                                    </span>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-form-label">CUIT:</label>
                                <input type="text" class="form-control" readonly id="cuit">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-form-label">Teléfono:</label>
                                <input type="text" class="form-control" readonly id="telefono">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="col-form-label">Dirección:</label>
                                <input type="text" class="form-control" readonly id="direccion">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <a href="" class="btn btn-success" id="btnSeleccionar">Seleccionar</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <input name="" data-idproveedor="0" class="form-control" id="nombreProveedor" disabled
                    placeholder="Seleccione un proveedor para mostrar el balance.."></input>
            </div>
            <div class="col-md-3 d-flex">
                <button class="btn btn-success bg-hfrut mr-2 flex-fill" data-toggle="modal" data-target="#modalProveedores"
                    id="btnFiltroEmpleado">Seleccionar
                    proveedor</button>

                <button class="btn btn-danger flex-fill" data-toggle="tooltip" data-placement="right"
                    title="Eliminar filtrado por proveedor" id="btnEliminarFiltroProveedor" disabled><i
                        class="fa fas fa-times-circle"></i></button>
            </div>

        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <div class="form-group">
                    <input type="date" class="form-control" id="fechaInicio" max='2099-12-31' min='1900-01-01' disabled>
                    <div id="validacionFechaInicio" class="invalid-feedback">
                        <strong></strong>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="date" class="form-control" id="fechaFin" value="{{ now()->format('Y-m-d') }}" disabled>
                    <div id="validacionFechaFin" class="invalid-feedback">
                        <strong></strong>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group d-flex">
                    <button class="btn btn-primary flex-fill mr-2" id="btnFiltrarFecha" disabled>Buscar</button>
                    <button class="btn btn-danger flex-fill" data-toggle="tooltip" data-placement="right"
                        title="Eliminar filtrado por fecha" id="btnEliminarFiltroFecha" disabled><i
                            class="fa fas fa-times-circle"></i></button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-2">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered" id="tablaBalanceCompraPago">
                                    <thead class="bg-hfrut">
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {
            var total = 0;

            //==================== LLENAR SELECT DEL MODAL CON LOS PROVEEDORES ===================
            axios.get('/proveedores-json').then((r) => {
                var proveedores = r.data;
                for (var i = 0; i < proveedores.length; i++) {
                    $('#proveedores').append('<option value=' + proveedores[i].id +
                        ' data-cuit=' + proveedores[i].cuit + ' data-telefono=' +
                        proveedores[i].telefono + ' data-direccion=' + encodeURIComponent(
                            proveedores[i]
                            .direccion) + '>' +
                        proveedores[i].nombreCompleto +
                        '</option>');
                }
                $('#cuit').val(proveedores[0].cuit);
                $('#telefono').val(proveedores[0].telefono);
                $('#direccion').val(proveedores[0].direccion);
                var idProv = $('#proveedores').val();
                var url = '/pagos/create/' + idProv;
                $('#btnComprar').attr('href', url);
            });

            //Hacer focus en la barra de busqueda cuando se abre el select de bancos
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            $('#proveedores').select2({
                width: '100%',
            });


            //============== EVENTO CADA VEZ QUE SE CAMBIA DE PROVEEDOR EN EL SELECT ===============
            $('#proveedores').change(function(e) {
                var selected = $(this).find('option:selected');
                var cuit = selected.data('cuit');
                var telefono = selected.data('telefono');
                var direccion = decodeURIComponent(selected.data('direccion'));
                $('#cuit').val(cuit);
                $('#telefono').val(telefono);
                $('#direccion').val(direccion);
                var idProv = $('#proveedores').val();
                var url = '/pagos/create/' + idProv;
                $('#btnComprar').attr('href', url);
            });

            function habilitarFiltros(habilitar) {
                //Obtengo el valor de la fecha
                var miFecha = new Date();
                //La formateo
                var fechaFormateada = miFecha.toISOString().substr(0, 10)
                //Asigno el valor al input
                $('#fechaFin').val(fechaFormateada);
                $('#fechaInicio').val('');
                if (habilitar == true) {
                    //Habilito los inputs de la fecha y el boton para buscar
                    $('#fechaInicio').prop('disabled', false);
                    $('#fechaFin').prop('disabled', false);
                    $('#btnFiltrarFecha').prop('disabled', false);
                } else {
                    //Deshabilito los inputs de la fecha y el boton para buscar
                    $('#fechaInicio').prop('disabled', true);
                    $('#fechaFin').prop('disabled', true);
                    $('#btnFiltrarFecha').prop('disabled', true);
                    $('#btnEliminarFiltroFecha').prop('disabled', true);
                }
            }

            //============== EVENTO ENTER DENTRO DEL SELECT2 ======================
            $(document).on('keydown', '.select2-search__field', function(e) {
                if (e.keyCode === 13) {
                    $('#btnSeleccionar').trigger('click');
                }
            });

            //============== EVENTO BOTON PARA CONFIRMAR DENTRO DEL MODAL ======================
            $('#btnSeleccionar').click(function(e) {
                    e.preventDefault();
                    //Obtengo los datos y los guardo en una variable respectivamente
                    var nombreProveedor = $('#proveedores option:selected').text();
                    var idProveedor = $('#proveedores').val();
                    var cuit = $('#cuit').val();
                    total = 0;
                    if (idProveedor == null) {
                        Swal.fire(
                            'Error',
                            'No hay ningun proveedor seleccionado.',
                            'error'
                        )
                    } else {
                        //LLeno el input de empleado para saber los datos de quien estoy viendo
                        $('#nombreProveedor').val(nombreProveedor + ', CUIT: ' + cuit);
                        //Asigno el id al atributo data para a la hora de imprimir saber que datos tengo que imprimir
                        $('#nombreProveedor').attr('data-idproveedor', idProveedor);

                        $('#btnImprimir').attr("disabled", false);
                        //Cargo la tabla con el respectivo empleado
                        cargarTabla(idProveedor);

                        //Escondo el tooltip del boton eliminar filtro
                        $('#btnEliminarFiltroProveedor').tooltip('hide');
                        $('#btnEliminarFiltroProveedor').attr("disabled", false);


                        $('#btnEliminarFiltroFecha').tooltip('hide');
                        $('#btnEliminarFiltroFecha').attr("disabled", true);

                        //Cierro el modal
                        $('#modalProveedores').trigger('click');
                    }
                }

            );

            //============= EVENTO CLICK BOTON ELIMINAR FILTRO PROVEEDOR ===============
            $('#btnEliminarFiltroProveedor').click(function(e) {
                e.preventDefault();
                $('#nombreProveedor').val('');
                //LLeno la tabla con los datos nuevos
                tablaBalanceCompraPago.ajax.url("{{ route('balance-proveedor.dt') }}").load();
                //Escondo el tooltip del boton y lo deshabilito
                $(this).tooltip('hide');
                $(this).attr("disabled", true);
                $('#btnImprimir').attr("disabled", true);
                habilitarFiltros(false);
                neutralFeedback('#fechaInicio', '#validacionFechaInicio', '');
                neutralFeedback('#fechaFin', '#validacionFechaFin', '');
            });

            //======================FUNCION CARGAR TABLA SEGUN EMPLEADO ======================
            function cargarTabla($id) {
                total = 0;
                $.get('/balance-compras-pagos/dt/' + $id,
                    function(data) {
                        if (data.recordsTotal == 0) {
                            tablaBalanceCompraPago.clear().draw();
                            Swal.fire(
                                'Sin registros',
                                'No hay registro de compras, ni de pagos a este proveedor.',
                                'warning'
                            )
                        } else {
                            tablaBalanceCompraPago.clear().rows.add(data.data).draw();
                            habilitarFiltros(true)
                        }
                    },
                    "json"
                );
            }

            //EVENTO (FILTRAR FECHAS)
            $('#btnFiltrarFecha').click(function(e) {
                e.preventDefault();
                total = 0;
                //Obtengo los valores de los inputs y el select y los guardo en sus respectivas variables
                var fechaInicio = $('#fechaInicio').val();
                var fechaFin = $('#fechaFin').val();
                var idProveedor = $('#proveedores').val();


                //Validacion para saber si la fecha no esta vacia
                if (!Date.parse(fechaInicio)) {
                    invalidFeedback('#fechaInicio', '#validacionFechaInicio',
                        'La fecha de inicio no puede estar vacia');
                    return;
                } else if (fechaInicio > fechaFin) {
                    invalidFeedback('#fechaInicio', '#validacionFechaInicio',
                        'La fecha de inicio no puede ser mayor que la de fin');
                    return;
                } else if (fechaInicio > '2099-12-31' || fechaInicio < '1900-01-01') {
                    invalidFeedback('#fechaInicio', '#validacionFechaInicio',
                        'Introduzca una fecha válida');
                    return;
                } else if (fechaFin > '2099-12-31' || fechaFin < '1900-01-01') {
                    invalidFeedback('#fechaFin', '#validacionFechaFin',
                        'Introduzca una fecha válida');
                    return;
                    //Validacion para saber si la fecha de fin no esta vacia
                } else if (!Date.parse(fechaFin)) {
                    invalidFeedback('#fechaFin', '#validacionFechaFin',
                        'La fecha de inicio no puede estar vacia');
                    return;
                }

                //Recargo la tabla con los datos filtrados

                tablaBalanceCompraPago.ajax.url('/balance-compras-pagos/filtro/proveedor=' +
                    idProveedor +
                    '&fechaInicio=' + fechaInicio + '&fechaFin=' + fechaFin).load();

                $('#btnEliminarFiltroFecha').prop('disabled', false);

                //Saco los mensajes de error si existen
                removeFeedback('#fechaInicio', '#validacionFechaInicio');
                removeFeedback('#fechaFin', '#validacionFechaFin');
            });

            //======================CLICK BOTON ELIMINAR FILTRO FECHAS ===============================
            $('#btnEliminarFiltroFecha').click(function(e) {
                e.preventDefault();
                $('#fechaInicio').val('');
                //Obtengo el valor de la fecha
                var miFecha = new Date();
                //La formateo
                var fechaFormateada = miFecha.toISOString().substr(0, 10)
                //Asigno el valor al input
                $('#fechaFin').val(fechaFormateada);

                var idProveedor = $('#proveedores').val();
                //LLeno la tabla con los datos nuevos
                cargarTabla(idProveedor);
                //Escondo el tooltip del boton y lo deshabilito
                $(this).tooltip('hide');
                $(this).attr("disabled", true);
                //Si hay menssaje de error lo escondo
                neutralFeedback('#fechaInicio', '#validacionFechaInicio', '');
                neutralFeedback('#fechaFin', '#validacionFechaFin', '');
            });

            //==================== FUNCION ERROR DE VALIDACION ============================
            //Funcion para agregar el el error debado del input que recibe como parametro el input al que se le va a agregar
            //el error, e label que va a contener el texto, y el texto en si
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== BORRAR ERROR VALIDACION =================================
            function neutralFeedback(input, labelInvalid, text) {
                $(input).hasClass('is-invalid') ? $(input).removeClass('is-invalid') : '';
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }

            //Funcion para formatear numeros a formato de dinero
            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });


            $('body').tooltip({
                selector: '[data-toggle="tooltip"]'
            });

            //EVENTO Imprimir balance
            $('#btnImprimir').click(function(e) {
                e.preventDefault();
                var ruta =
                    "{{ route('balance-compras-pagos.pdf', ['idproveedor' => ':id','fechaFin' => ':fFin','fechaInicio' => ':fInc']) }}";
                ruta = ruta.replace(':id', $('#nombreProveedor').attr('data-idproveedor'));
                //Significa que ya he aplicado el filtro, de otra forma se tomarian los datos sin haber ningun filtro
                if ($('#btnEliminarFiltroFecha').prop('disabled') == false) {
                    ruta = ruta.replace(':fFin', $('#fechaFin').val());
                    ruta = ruta.replace(':fInc', $('#fechaInicio').val());
                } else {
                    var d = new Date();
                    var strDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                    ruta = ruta.replace(':fFin', strDate);
                }
                $('#btnPDF').attr('href', ruta);
                $('#btnPDF')[0].click();


            });

            var fecha = new Intl.DateTimeFormat("es-AR");

            //=========================== DATATABLE =================================

            var tablaBalanceCompraPago = $('#tablaBalanceCompraPago').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                deferRender: true,
                bLengthChange: true,
                lengthMenu: [
                    [-1, 5, 10, 25, 100],
                    ["Todos", 5, 10, 25, 50]
                ],
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                //hago la peticion a esta url ya que no me devuelve nada y puedo inicializar el datatable sin nungun dato
                //luego cuando se seleccione un empleado le envio el parametro
                ajax: {
                    url: "{{ route('balance-proveedor.dt') }}",
                },
                columns: [{
                        data: 'fecha',
                        title: 'Fecha'
                    },
                    {
                        data: 'tipoCompra',
                        title: 'Tipo de movimiento',
                        render: function(tipoMovimiento) {
                            switch (tipoMovimiento) {
                                //CIS = Compra de insumos y servicios
                                case 'CI':
                                    return 'Compra de insumo'
                                    break;
                                case 'CS':
                                    return 'Compra de servicio'
                                    break;
                                case 'CID':
                                    return 'Compra de indumentaria'
                                    break;
                                    //CMP = Compra de materia prima
                                case 'CMP':
                                    return 'Compra de materia prima'
                                    break;
                                    //PP = Pago a proveedor
                                case 'PP':
                                    return 'Pago a proveedor'
                                    break;
                            }
                        }
                    },
                    {
                        data: 'haber',
                        title: 'Haber',
                        render: function(haber) {
                            if (haber == '' || haber == null) {
                                return '';
                            } else {
                                //Funcion para formatear el dinero con el signo $ y las comas
                                return formatear.format(haber);
                            }
                        }
                    },
                    {
                        data: 'debe',
                        title: 'Debe',
                        render: function(debe) {
                            if (debe == '' || debe == null) {
                                return '';
                            } else {
                                //Funcion para formatear el dinero con el signo $ y las comas
                                return formatear.format(debe);
                            }
                        }
                    },
                    {
                        data: null,
                        title: "Total",
                        targets: 6,
                        //Voy acumulando los sueldos (debe) y voy restando los pagos de sueldo (haber) por parte
                        //del empleado
                        render: function(data, type) {
                            //Luego de cargar el datatable mas de una vez esta columna se renderizaba dos veces y se producia un
                            //total erroneo, por lo que agregando este if con el parametro display se soluciono. Ty stackoverflow
                            if (type === 'display') {
                                total += (parseFloat(Number(
                                    data.haber)) - parseFloat(Number(data.debe)));
                            }
                            return formatear.format(total);
                        }
                    },
                    {
                        data: 'created_at',
                        visible: false,

                    },
                ],
                order: [5, 'desc'],
            });

        });
    </script>
@stop
