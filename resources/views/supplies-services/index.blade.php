@extends('adminlte::page')

@section('plugins.Sweetalert2', true)

@section('title', 'Insumos - Servicios')

@section('content_header')
    @can('supplie-service.create')
        <a class="float-right pr-2">
            <button class="btn btn-success bg-hfrut" data-toggle="modal" data-target="#modalInsumoServicioAgregar">
                Agregar nuevo insumo / servicio
            </button>
            <button class="btn btn-success bg-hfrut" data-toggle="modal" data-target="#modalIndumentariaAgregarEditar">
                Agregar nueva indumentaria
            </button>
        </a>
    @endcan
    <p class="pl-2 h2">Insumos - Servicios - Indumentarias</p>

@stop


@section('content')

    <div id="app">
        @can('supplie-service.create')
            {{-- MODAL PARA AGREGAR INSUMO - SERVICIO --}}
            <div class="modal fade" id="modalInsumoServicioAgregar" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">

                            <h5 class="modal-title" id="labelInsumoServicioAgregar">Agregar nuevo insumo /
                                servicio
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <form action="" method="POST" id="formAgregarIS">
                            @csrf
                            <div class="modal-body">
                                <span id="resultadoFormAgregar"></span>
                                <div class="form-group">
                                    <label for="nombre" class="col-form-label">Nombre (*)</label>
                                    <input type="text" class="form-control" name="nombre" id="nombre"
                                        placeholder="Nombre del insumo / servicio..." required>

                                </div>
                                <div class="form-group" id="divCantidad">
                                    <label for="cantidad" class="col-form-label">Cantidad</label>
                                    <input type="number" name="cantidad" class="form-control" placeholder="Cantidad..."
                                        id="cantidad" required autocomplete="off">
                                </div>
                                <div class="form-group d-block">
                                    <label for="tipo" class="col-form-label">Tipo (*)</label>
                                    <div class="btn-group btn-group-toggle d-block" data-toggle="buttons">
                                        {{-- //IS = Insumo --}}
                                        <label class="btn btn-outline-secondary active">
                                            <input type="radio" name="tipo" value="IS" checked id="btnInsumoCrear">
                                            Insumo
                                        </label>
                                        {{-- //SV = Servicio --}}
                                        <label class="btn btn-outline-secondary">
                                            <input type="radio" name="tipo" value="SV" id="btnServicioCrear">
                                            Servicio
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button class="btn btn-success" id="btnAgregarIS">Agregar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            {{-- MODAL PARA AGREGAR INDUMENTARIA --}}
            <div class="modal fade" id="modalIndumentariaAgregarEditar" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">

                            <h5 class="modal-title" id="labelIndumentariaAgregar">Agregar nueva indumentaria
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <form action="" method="" id="formAgregarIndumentaria">
                            <div class="modal-body">
                                <span id="resultadoFormIndumentaria"></span>
                                <div class="form-group">
                                    <label for="nombre" class="col-form-label">Nombre (*)</label>
                                    <input type="text" class="form-control" name="nombre" id="nombreIndumentaria"
                                        placeholder="Nombre..." autofocus required autocomplete="off">

                                </div>
                                <div class="form-group">
                                    <label for="tipo_modelo" class="col-form-label">Tipo (*)</label>
                                    <input type="text" class="form-control" name="tipo_modelo" id="tipo_modelo"
                                        placeholder="Tipo..." required autocomplete="off">

                                </div>
                                <div class="form-group">
                                    <label for="modelo" class="col-form-label">Modelo (*)</label>
                                    <input type="text" class="form-control" name="modelo" id="modelo" placeholder="Modelo..."
                                        required autocomplete="off">

                                </div>
                                <div class="form-group">
                                    <label for="cantidad" class="col-form-label">Cantidad (*)</label>
                                    <input type="number" name="cantidad" class="form-control" placeholder="Cantidad..."
                                        id="cantidadIndumentaria" required autocomplete="off" autocomplete="off">
                                </div>
                                <input type="hidden" id="idIndumentaria">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button class="btn btn-success" id="btnAgregarIndumentaria">Agregar</button>
                                <button class="btn btn-success d-none" id="btnEditarIndumentaria">Editar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan
        @can('supplie-service.edit')
            {{-- MODAL PARA EDITAR INSUMO - SERVICIO --}}
            <div class="modal fade" id="modalInsumoServicioEditar" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="labelInsumoServicioEditar">Editar insumo / servicio</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="" method="POST" id="formEditarInsumoServicio">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <span id="resultadoFormEditar"></span>
                                <div class="form-group">
                                    <label for="nombre" class="col-form-label">Nombre (*)</label>
                                    <input type="text" class="form-control" name="nombre" id="nombreEditar"
                                        placeholder="Nombre del insumo / servicio..." autofocus required>
                                </div>
                                <div class="form-group" id="divCantidadEditar">
                                    <label for="cantidad" class="col-form-label">Cantidad</label>
                                    <input type="number" name="cantidad" class="form-control" placeholder="Cantidad..."
                                        id="cantidadEditar" required autocomplete="off">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="id" id="idInsumoServicio">
                                <input type="hidden" name="InsOServ" id="InsOServ">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    Cerrar
                                </button>
                                <button class="btn btn-success" id="btnModificar">Modificar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan


        <ul class="nav nav-tabs mr-1" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link {{ Session::get('from') ? '' : 'active' }}" id="insumos-tab" data-toggle="tab"
                    href="#insumos" role="tab" aria-controls="insumos"
                    {{ Session::get('from') ? '' : 'aria-selected="true"' }}>Insumos</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link {{ Session::get('from') ? 'active' : '' }}" id="servicios-tab" data-toggle="tab"
                    href="#servicios" role="tab" aria-controls="servicios"
                    {{ Session::get('from') ? 'aria-selected="true"' : '' }}>Servicios</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link {{ Session::get('from') ? 'active' : '' }}" id="indumentaria-tab" data-toggle="tab"
                    href="#indumentaria" role="tab" aria-controls="indumentaria"
                    {{ Session::get('from') ? 'aria-selected="true"' : '' }}>Indumentaria</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade {{ Session::get('from') ? '' : 'show active' }}" id="insumos" role="tabpanel"
                aria-labelledby="insumos-tab">

                <div class="container-fluid py-3 tab-cont">

                    <table class='table table-bordered' id='tablaInsumos'>
                        <thead class="bg-hfrut">
                            <th scope="col">Nombre</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col" class="text-center" style="width:12em;">Opciones</th>
                        </thead>

                    </table>
                </div>
            </div>
            <div class="tab-pane fade {{ Session::get('from') ? 'show active' : '' }}" id="servicios" role="tabpanel"
                aria-labelledby="servicios-tab">
                <div class="container-fluid py-3 tab-cont">
                    <table class='table table-bordered' id="tablaServicios">
                        <thead class="bg-hfrut">
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col" class="text-center" style="width:20em;">Opciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade {{ Session::get('from') ? 'show active' : '' }}" id="indumentaria" role="tabpanel"
                aria-labelledby="indumentaria-tab">
                <div class="container-fluid py-3 tab-cont">
                    <table class='table table-bordered' id="tablaIndumentaria">
                        <thead class="bg-hfrut">
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Modelo</th>
                                <th scope="col">Cantidad</th>
                                <th scope="col" class="text-center" style="width:10em;">Opciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')

    <script>
        $(document).ready(function() {

            var tablaInsumos = "";
            var tablaServicios = "";
            var tablaIndumentaria = "";

            function capitalize(word) {
                return word[0].toUpperCase() + word.slice(1);
            }

            //============================ EVENTO CLICK BOTON ELIMINAR ============================
            $(document).on('click', '.btnEliminar', function(e) {
                var id = $(this).data('id');
                var url = $(this).data('url');
                Swal.fire({
                    title: "¿Desea dar de baja este " + url + "?",
                    // text: "Una vez eliminada, no se puede recuperar",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post(`/` + url + `/eliminar/` + id, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    if (url == 'insumo') {
                                        tablaInsumos.ajax.reload();
                                    } else {
                                        tablaServicios.ajax.reload();
                                    }
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: capitalize(url) + " dado de baja",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                    }
                });
            });

            //============================ EVENTOS MODALES AGREGAR EDITAR ============================

            $('#modalInsumoServicioAgregar').on('shown.bs.modal', function(event) {
                $('#nombre').focus();
            })

            $('#modalInsumoServicioAgregar').on('hide.bs.modal', function(event) {
                $('#nombre').val('');
                $('#cantidad').val('');
                $('#resultadoFormAgregar').empty();
            })

            $('#modalInsumoServicioEditar').on('hide.bs.modal', function(event) {
                $('#nombre').val('');
                $('#resultadoFormEditar').empty();
            })

            $("#modalInsumoServicioEditar").on("shown.bs.modal", function(event) {
                //Una vez abierto el modal hago foco sobre el input
                $("#nombreEditar").focus();
            });

            $('#modalIndumentariaAgregarEditar').on('hide.bs.modal', function(event) {
                $('#resultadoFormIndumentaria').empty();
                $("#modalIndumentariaAgregarEditar :input").each(function(index) {
                    $(this).val('');
                });
            })

            //======================= ENVENTO CLICK BOTON MODIFICAR INSUMO _SERVICIO ============================
            $('#btnModificar').on('click', function(e) {
                if ($('#servicios-tab').attr('aria-selected', 'true')) {
                    tabServicios = true;
                } else {
                    tabServicios = false;
                }
                e.preventDefault();

                $.ajax({
                    url: "{{ route('supplie-service.update') }}",
                    method: 'PUT',
                    data: $('#formEditarInsumoServicio').serialize(),
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoFormEditar').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });
                            //Si se creo un servicio significa que el boton servicio esta chequeado por lo que lo pregunto
                            //y si es correcto recargo la tabla y disparo el evento de hacer click sobre la pestaña de servicios
                            if (tabServicios == true) {
                                tablaServicios.ajax.reload();
                                $('#servicios-tab').trigger('click');
                            } else {
                                tablaInsumos.ajax.reload();
                            }
                            //Cierro el modal
                            $('#modalInsumoServicioEditar').trigger('click');
                        }
                    }
                })
            });


            //================ EVENTO CUANDO SE MUESTRA EL MOPDAL PARA EDITAR INSSUMO SERVICIO ===================
            //Cuando se muestra el modal para modificar
            $("#modalInsumoServicioEditar").on("show.bs.modal", function(event) {
                var button = $(event.relatedTarget);
                //Paso el nombre del cargo por variables data dentro del boton (HTML), al igual que el id
                var nombre = button.data("nombre");
                var cantidad = button.data("cantidad");
                var idInsumoServicio = button.data("id");
                var modal = $(this);
                //Le asigno a un input tipo hidden el ID del cargo a modificar para poder enviarlo en el request
                $("#idInsumoServicio").val(idInsumoServicio);
                //Lleno el input con el nombre del cargo a modificar
                // modal.find(".modal-body input").val(nombreCargo);
                $('#nombreEditar').val(nombre);

                //Si cantidad es null significa que voy a editar un servicio
                if (cantidad == null) {
                    $('#divCantidadEditar').addClass('d-none');
                    //Establezco en un input de tipo hidden si lo que voy a modificar es insumo o servicio para poder verificarlo en el back
                    $('#InsOServ').val('servicio');
                } else {
                    if ($('#divCantidadEditar').hasClass('d-none')) {
                        $('#divCantidadEditar').removeClass('d-none')
                    }
                    $('#cantidadEditar').val(cantidad);
                    //Establezco en un input de tipo hidden si lo que voy a modificar es insumo o servicio para poder verificarlo en el back
                    $('#InsOServ').val('insumo');
                }
            });


            //======================= EVENTO QUITAR O MOSTRAR INPUT CANTIDAD =====================================

            $('#btnServicioCrear').click(function() {
                $('#divCantidad').fadeOut();
            });

            $('#btnInsumoCrear').click(function() {
                $('#divCantidad').fadeIn();
            });

            //======================== ENVIAR FORMULARIO AGREGAR INSUMO O SERVICIO ===================================

            $('#btnAgregarIS').on('click', function(e) {
                e.preventDefault();
                //El boton agregar envia los datos del formulario automaticamente
                $.ajax({
                    url: "{{ route('supplie-service.store') }}",
                    method: "POST",
                    //Envio los datos del formulario
                    data: $('#formAgregarIS').serialize(),
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoFormAgregar').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });
                            //Si se creo un servicio significa que el boton servicio esta chequeado por lo que lo pregunto
                            //y si es correcto recargo la tabla y disparo el evento de hacer click sobre la pestaña de servicios
                            if ($('#btnServicioCrear').parent().hasClass('active')) {
                                tablaServicios.ajax.reload();
                                $('#servicios-tab').trigger('click');
                            }
                            //Lo mismo para los insumos
                            if ($('#btnInsumoCrear').parent().hasClass('active')) {
                                tablaInsumos.ajax.reload();
                                $('#insumos-tab').trigger('click');
                            }
                            //Cierro el modal
                            $('#modalInsumoServicioAgregar').trigger('click');
                        }
                    }
                })
            });

            //======================== ENVIAR FORMULARIO AGREGAR INDUMENTARIA ===================================

            $('#btnAgregarIndumentaria').on('click', function(e) {
                e.preventDefault();
                //El boton agregar envia los datos del formulario automaticamente
                $.ajax({
                    url: "{{ route('indumentary.store') }}",
                    method: "POST",
                    //Envio los datos del formulario
                    data: $('#formAgregarIndumentaria').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoFormIndumentaria').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });
                            $('.nav-tabs a[href="#indumentaria"]').tab('show');
                            tablaIndumentaria.ajax.reload();
                            //Cierro el modal
                            $('#modalIndumentariaAgregarEditar').trigger('click');
                        }
                    }
                })
            });

            //================ EVENTO CUANDO SE MUESTRA EL MOPDAL PARA EDITAR INDUMENTARIA ===================
            //Cuando se muestra el modal para modificar
            $("#modalIndumentariaAgregarEditar").on("show.bs.modal", function(event) {
                var button = $(event.relatedTarget);
                //Pregunto el id desde donde se disparo el modal, si tiene el id 'editarIndumentaria' significa que 
                //debo editar. De otra forma el modal se esa llamando desde el boton agregar
                if (button.attr('id') == 'editarIndumentaria') {
                    //Paso los datos a travez de variables data dentro del boton (HTML), al igual que el id
                    var nombre = button.data("nombre");
                    var tipo = button.data("tipo");
                    var modelo = button.data("modelo");
                    var cantidad = button.data("cantidad");
                    var idIndumentaria = button.data("id");

                    var modal = $(this);
                    // //Le asigno a un input tipo hidden el ID del cargo a modificar para poder enviarlo en el request
                    $("#idIndumentaria").val(idIndumentaria);
                    //Asigno los valores a los inputs
                    $('#nombreIndumentaria').val(nombre);
                    $('#tipo_modelo').val(tipo);
                    $('#modelo').val(modelo);
                    $('#cantidadIndumentaria').val(cantidad);
                    $('#btnAgregarIndumentaria').addClass('d-none');
                    $('#btnEditarIndumentaria').removeClass('d-none');
                } else {
                    $('#btnEditarIndumentaria').addClass('d-none');
                    $('#btnAgregarIndumentaria').removeClass('d-none');
                }
            });

            //======================== ENVIAR FORMULARIO MODIFICAR INDUMENTARIA ===================================

            $('#btnEditarIndumentaria').on('click', function(e) {
                e.preventDefault();
                var id = $('#idIndumentaria').val();
                //El boton agregar envia los datos del formulario automaticamente
                $.ajax({
                    url: "/indumentaria/" + id,
                    method: "PUT",
                    //Envio los datos del formulario
                    data: $('#formAgregarIndumentaria').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoFormIndumentaria').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });
                            $('.nav-tabs a[href="#indumentaria"]').tab('show');
                            tablaIndumentaria.ajax.reload();
                            //Cierro el modal
                            $('#modalIndumentariaAgregarEditar').trigger('click');
                        }
                    }
                })
            });

            //======================== ELIMINAR INDUMENTARIA ===================================
            $(document).on('click', '.btnEliminarIndumentaria', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea dar de baja esta indumentaria?",
                    // text: "Una vez eliminada, no se puede recuperar",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post('/indumentaria/delete/' + id, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    tablaIndumentaria.ajax.reload();
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: "Indumentaria dada de baja",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                    }
                });
            });

            //============== DATATABLES DE INSUMOS Y SERVICIOS ===========================

            tablaInsumos = $('#tablaInsumos').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: false,
                bLengthChange: false,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('dt.insumos') }}",
                columns: [{
                        data: 'nombre',
                    },
                    {
                        data: 'cantidad',
                    },
                    {
                        data: 'opciones',
                    },
                    {
                        data: 'created_at',
                        visible: false
                    }
                ],
                order: [3, 'desc']
            });

            tablaServicios = $('#tablaServicios').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: false,
                bLengthChange: false,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('dt.servicios') }}",
                columns: [{
                        data: 'nombre',
                    },
                    {
                        data: 'opciones',
                        // className: 'text-center'
                    },
                    {
                        data: 'created_at',
                        visible: false
                    }
                ],
                order: [2, 'desc']

            });

            tablaIndumentaria = $('#tablaIndumentaria').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: false,
                bLengthChange: false,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "/indumentaria",
                columns: [{
                        data: 'nombre',
                    },
                    {
                        data: 'tipo_modelo'
                    },
                    {
                        data: 'modelo'
                    },
                    {
                        data: 'cantidad'
                    },
                    {
                        data: 'opciones',
                        // className: 'text-center'
                    },
                    {
                        data: 'created_at',
                        visible: false
                    },

                ],
                order: [5, 'desc'],

            });

        });
    </script>

@stop
