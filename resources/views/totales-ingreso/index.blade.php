@extends('adminlte::page')

@section('title', 'Totales de ingresos')

@section('content_header')
    <h1 class="pl-3">Totales de ingresos</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" id="resultForm">

            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Fecha de inicio:</label>
                    <input type="date" class="form-control" id="fechaInicio" max='2099-12-31' min='1900-01-01'>
                    <div id="validacionFechaInicio" class="invalid-feedback">
                        <strong></strong>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Fecha de fin:</label>
                    <input type="date" class="form-control" id="fechaFin" value="{{ now()->format('Y-m-d') }}"
                        max="{{ now()->format('Y-m-d') }}">
                    <div id="validacionFechaFin" class="invalid-feedback">
                        <strong></strong>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="proveedor">Proveedores</label>
                    <select name="proveedores" id="proveedores" class="form-control">
                        <option value="">Seleccione un proveedor</option>
                        @foreach ($proveedores as $proveedor)
                            <option value="{{ $proveedor->id }}">{{ $proveedor->nombreCompleto }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group ">
                    <label for="" class="">ㅤ</label>
                    <button class="btn btn-primary flex-fill mr-2 form-control" id="btnFiltrarFecha">Buscar</button>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group ">
                    <label for="" class="">ㅤ</label>
                    <button class="btn btn-danger flex-fill form-control" data-toggle="tooltip" data-placement="right"
                        title="Eliminar filtrado por fecha" id="btnEliminarFiltroFecha" disabled><i
                            class="fa fas fa-times-circle"></i></button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-2">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered" id="tablaTotales">
                                    <thead class="bg-hfrut">
                                        <tr>
                                            <th>Materia prima</th>
                                            <th>Suma total de kilogramos</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {


            $('#proveedores').select2();

            //Hacer focus en la barra de busqueda cuando se abre el select de bancos
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });


            //Filtrar
            $('#btnFiltrarFecha').click(function(e) {
                e.preventDefault();
                total = 0;
                //Obtengo los valores de los inputs y el select y los guardo en sus respectivas variables
                var fechaInicio = $('#fechaInicio').val();
                var fechaFin = $('#fechaFin').val();
                var proveedor = $('#proveedores').val();
                //Validacion para saber si la fecha no esta vacia
                if (!Date.parse(fechaInicio) && !$('#proveedores').val()) {
                    html =
                        '<div class="alert alert-danger alert-dismissible fade show" role="alert">El campo proveedor es obligatorio cuando fecha inicio no está presente o viceversa.' +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    $('#resultForm').html(html);
                    return;
                } else if (Date.parse(fechaInicio) && Date.parse(fechaFin)) {
                    if (fechaInicio > fechaFin) {
                        invalidFeedback('#fechaInicio', '#validacionFechaInicio',
                            'La fecha de inicio no puede ser mayor que la de fin');
                        return;
                    } else if (fechaInicio > '2099-12-31' || fechaInicio < '1900-01-01') {
                        invalidFeedback('#fechaInicio', '#validacionFechaInicio',
                            'Introduzca una fecha válida');
                        return;
                    } else if (fechaFin > '2099-12-31' || fechaFin < '1900-01-01') {
                        invalidFeedback('#fechaFin', '#validacionFechaFin',
                            'Introduzca una fecha válida');
                        return;
                        //Validacion para saber si la fecha de fin no esta vacia
                    } else if (!Date.parse(fechaFin)) {
                        invalidFeedback('#fechaFin', '#validacionFechaFin',
                            'La fecha de inicio no puede estar vacia');
                        return;
                    }
                }

                $.ajax({
                    type: "POST",
                    url: "{{ route('totales-ingreso.filtrar') }}",
                    data: {
                        fechaInicio: fechaInicio,
                        fechaFin: fechaFin,
                        proveedor: proveedor,
                        _token: "{{ csrf_token() }}",
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response.error) {
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < response.error.length; i++) {
                                html += '<li>' + response.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultForm').html(html);
                            return;
                        }

                        tablaTotales.clear().rows.add(response.data).draw();
                        let html =
                            '<div class="alert alert-dark alert-dismissible fade show" role="alert">';

                        var fechaInicio = new Date($('#fechaInicio').val())
                        var fechaFin = new Date($('#fechaFin').val())
                        var nombreProveedor = $('#proveedores :selected').text();
                        if ($('#proveedores').val() == "") {
                            html += 'Filtrando entre ' + fecha.format(
                                    fechaInicio) +
                                ' y ' + fecha.format(
                                    fechaFin);
                        } else if ($('#fechaInicio').val() == "") {
                            html += 'Filtrando por proveedor: ' +
                                nombreProveedor;
                        } else {
                            html += 'Filtrando entre ' + fecha.format(
                                    fechaInicio) + ' y ' +
                                fecha.format(
                                    fechaFin) +
                                '. Proveedor ' +
                                nombreProveedor;
                        }

                        html +=
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '</button></div>';

                        $('#resultForm').html(html);

                        $('#btnFiltrarFecha').prop('disabled', true);
                    }
                });


                $('#btnEliminarFiltroFecha').prop('disabled', false);

                //Saco los mensajes de error si existen
                removeFeedback('#fechaInicio', '#validacionFechaInicio');
                removeFeedback('#fechaFin', '#validacionFechaFin');
            });

            function habilitarBusqueda() {
                if ($('#btnFiltrarFecha').is(':disabled')) {
                    $('#btnFiltrarFecha').prop('disabled', false);
                }
            }

            $('#fechaInicio').change(function(e) {
                e.preventDefault();
                habilitarBusqueda();
            });

            $('#fechaFin').change(function(e) {
                e.preventDefault();
                habilitarBusqueda();
            });

            $('#proveedores').change(function(e) {
                e.preventDefault();
                habilitarBusqueda();
            });

            $('#btnEliminarFiltroFecha').click(function(e) {
                e.preventDefault();
                $('#fechaInicio').val('');
                //Obtengo el valor de la fecha
                var miFecha = new Date();
                //La formateo
                var fechaFormateada = miFecha.toISOString().substr(0, 10)
                //Asigno el valor al input
                $('#fechaFin').val(fechaFormateada);

                tablaTotales.clear().draw();

                //Escondo el tooltip del boton y lo deshabilito
                $(this).tooltip('hide');
                $(this).attr("disabled", true);
                //Si hay menssaje de error lo escondo
                removeFeedback('#fechaInicio', '#validacionFechaInicio');
                removeFeedback('#fechaFin', '#validacionFechaFin');

                $('#proveedores option:eq(0)').prop('selected', true);
                $('#proveedores').trigger('change.select2');
                $('#resultForm').empty();
                habilitarBusqueda();
            });

            $(document).on('click', '.close', function() {
                $('#btnEliminarFiltroFecha').trigger('click');
            });

            tablaTotales = $('#tablaTotales').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                deferRender: true,
                bLengthChange: true,
                lengthMenu: [
                    [-1, 5, 10, 25, 100],
                    ["Todos", 5, 10, 25, 50]
                ],
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                columns: [{
                        data: 'materiaPrima',
                    },
                    {
                        data: 'total'
                    }
                ],
            });
            var fecha = new Intl.DateTimeFormat("es-AR");

            //==================== FUNCION ERROR DE VALIDACION ============================
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }
        });
    </script>
@stop
