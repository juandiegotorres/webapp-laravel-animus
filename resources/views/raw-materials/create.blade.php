@extends('adminlte::page')

@section('title', 'Nuevo Cliente')

@section('content_header')
    <p class="pl-4 h2">Agregar nueva materia prima</p>
@stop

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">

                <div class="card card-primary border-top">
                    {!! Form::open(['route' => 'raw-material.store', 'class' => 'p-4']) !!}
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('fruta', 'Nombre de la fruta (*)') !!}
                                {!! Form::text('fruta', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Nombre de la fruta...']) !!}

                                @error('fruta')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('variedad', 'Variedad de la fruta (*)') !!}
                                {!! Form::number('variedad', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Variedad de la fruta...']) !!}
                                @error('variedad')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>






                    <div class="row mt-3">
                        <div class="col-md-12">
                            <a href="{{ route('clients.index') }}" class="btn btn-danger mr-2">
                                Cancelar
                            </a>
                            {!! Form::submit('Agregar cliente', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')


    <script>
        $(document).ready(function() {
            $('#provincias').change(function() {
                $('#idLocalidad').empty();
                var id_provincia = $('#provincias').val();
                axios.post(`/localidades/${id_provincia}`).then((r) => {
                    var localidades = r.data;
                    for (var i = 0; i < localidades.length; i++) {
                        $('#idLocalidad').append('<option value=' + localidades[i].id + '>' +
                            localidades[i].localidad +
                            '</option>');
                    }
                    //Si la provincia elegida es mendoza (id=14) automaticamente selecciono Gran Alvear (id=1443)
                    //para ahorrarle tiempo al usuario
                    if (id_provincia == 14) {
                        $('#idLocalidad').val(1443);
                    }
                });
            });
        });
    </script>


    {{-- <script>
        Swal.fire(
            'Good job!',
            'You clicked the button!',
            'success'
        )
    </script> --}}
@stop
