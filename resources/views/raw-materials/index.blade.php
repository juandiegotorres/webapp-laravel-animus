@extends('adminlte::page')

@section('plugins.Sweetalert2', true)

@section('title', 'Clientes')

@section('content_header')
    @can('raw-material.create')
        <a class="float-right pr-2">
            <button class="btn btn-success bg-hfrut" data-toggle="modal" data-target="#modalMateriaPrimaAgregar">
                Agregar nueva materia prima
            </button>
        </a>
    @endcan
    <p class="pl-2 h2">Materias Primas</p>
@stop


@section('content')

    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                window.location.hash = '#';
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: 'success',
                        title: '{{ session('status') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif

    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    @can('raw-material.create')
                        {{-- MODAL PARA AGREGAR MATERIA PRIMA --}}
                        <div class="modal fade" id="modalMateriaPrimaAgregar" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">

                                        <h5 class="modal-title" id="labelMateriaPrimaAgregar">Agregar nueva materia prima
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    {!! Form::open(['route' => ['raw-material.store', '#create'], 'id' => 'formMateriaPrimaAgregar']) !!}
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="fruta" class="col-form-label">Nombre de la Fruta (*)</label>
                                            {!! Form::text('fruta', null, ['autocomplete' => 'off', 'id' => 'fruta', 'autofocus', 'required', 'class' => 'form-control', 'placeholder' => 'Fruta...']) !!}
                                            @error('fruta')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="variedad" class="col-form-label">Variedad (*)</label>
                                            {!! Form::text('variedad', null, ['autocomplete' => 'off', 'id' => 'variedad', 'autofocus', 'required', 'class' => 'form-control', 'placeholder' => 'Variedad...']) !!}
                                            @error('variedad')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        {{-- <button type="button" class="btn btn-primary">Agregar cargo</button> --}}
                                        {!! Form::submit('Agregar materia prima', ['class' => 'btn btn-success']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    @endcan
                    @can('raw-material.edit')
                        {{-- MODAL PARA AGREGAR MATERIA PRIMA --}}
                        <div class="modal fade" id="modalMateriaPrimaEditar" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="labelMateriaPrimaEditar">Editar cargo</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    {!! Form::open(['route' => ['raw-material.update', '#edit']]) !!}
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="fruta" class="col-form-label">Nombre de la Fruta (*)</label>
                                            {!! Form::text('fruta', null, ['id' => 'frutaEditar', 'autocomplete' => 'off', 'class' => 'form-control', 'required']) !!}
                                            @error('fruta')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="variedad" class="col-form-label">Variedad (*)</label>
                                            {!! Form::text('variedad', null, ['id' => 'variedadEditar', 'autocomplete' => 'off', 'class' => 'form-control', 'required']) !!}
                                            @error('variedad')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        {!! Form::hidden('id', null, ['id' => 'idMateriaPrima', 'type' => 'hidden']) !!}
                                        {{-- <input type="hidden" id="idCargo" name="id" /> --}}
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Cerrar
                                        </button>
                                        {!! Form::submit('Modificar materia prima', ['class' => 'btn btn-success']) !!}

                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    @endcan

                    <table class="table table-bordered table-rounded" id="materiasPrimas">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Fruta</th>
                                <th scope="col">Variedad</th>
                                <th scope="col" style="width:15px">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($materiasPrimas as $materiaPrima)
                                <tr>
                                    <td>{{ $materiaPrima->fruta }}</td>
                                    <td>{{ $materiaPrima->variedad }}</td>
                                    <td>
                                        <div class="d-flex justify-content-end">
                                            @can('raw-material.edit')
                                                <a data-toggle="modal" data-target="#modalMateriaPrimaEditar"
                                                    data-fruta="{{ $materiaPrima->fruta }}"
                                                    data-variedad="{{ $materiaPrima->variedad }}"
                                                    data-id="{{ $materiaPrima->id }}">
                                                    <button class=" btn btn-primary btn-sm mr-2">
                                                        <i class="fa fa-pen"></i>
                                                    </button>
                                                </a>
                                            @endcan
                                            @can('raw-material.delete')
                                                <dar-baja id="{{ $materiaPrima->id }}" ruta="materias-primas"
                                                    entidad="materia prima">
                                                </dar-baja>
                                            @endcan

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script>
        $(document).ready(function() {

            //AGREGAR MATERIA PRIMA
            if (window.location.hash === '#create') {
                $('#modalMateriaPrimaAgregar').modal('show');
            }

            $('#modalMateriaPrimaAgregar').on('show.bs.modal', function(event) {
                window.location.hash = '#create';
            })

            $('#modalMateriaPrimaAgregar').on('shown.bs.modal', function(event) {
                $('#fruta').focus();
            })

            $('#modalMateriaPrimaAgregar').on('hide.bs.modal', function(event) {
                window.location.hash = '#';
                $('#fruta').val('');
                //Deshabilito el mensaje de error si es que hay al cerrar el modal, de todas formas se vuelve a mostrar si
                //es necesario cuando enviamos el formulario
                var modal = $(this);
                modal.find('.invalid-feedback strong').addClass('d-none');
            })


            //MODIFICAR MATERIA PRIMA

            //Si la URL posee el hash #edit se va a abrir nuevamente para que el cliente pueda modificar los datos erroneos
            //Esto solo se va a ejecutar si hay un error de validacion
            if (window.location.hash === "#edit") {
                $("#modalMateriaPrimaEditar").modal("show");
            }

            //Cuando se muestra el modal para modificar
            $("#modalMateriaPrimaEditar").on("show.bs.modal", function(event) {
                //Pongo le hash #edit para poder abrir nuevamente el modal si hay un error de validacion
                window.location.hash = "#edit";
                var button = $(event.relatedTarget);
                //Paso el nombre del cargo por variables data dentro del boton (HTML), al igual que el id
                var fruta = button.data("fruta");
                var variedad = button.data("variedad");
                var idMateriaPrima = button.data("id");
                var modal = $(this);
                //Le asigno a un input tipo hidden el ID del cargo a modificar para poder enviarlo en el request
                $("#idMateriaPrima").val(idMateriaPrima);
                //Lleno el input con el nombre del cargo a modificar
                // modal.find(".modal-body input").val(nombreCargo);
                $('#frutaEditar').val(fruta);
                $('#variedadEditar').val(variedad);
            });

            $("#modalMateriaPrimaEditar").on("shown.bs.modal", function(event) {
                //Una vez abierto el modal hago foco sobre el input
                $("#frutaEditar").focus();

            });

            $("#modalMateriaPrimaEditar").on("hide.bs.modal", function(event) {
                //Cuando el modal es cerrado o escondido saco el hash #edit y vacio el input
                window.location.hash = "#";
                $('#frutaEditar').val('');
                $('#variedadEditar').val('');
                var modal = $(this);
                //Deshabilito el mensaje de error si es que hay al cerrar el modal, de todas formas se vuelve a mostrar si
                //es necesario cuando enviamos el formulario
                modal.find('.invalid-feedback strong').addClass('d-none');

            });



            //DATATABLE
            $('#materiasPrimas').DataTable({
                responsive: true,
                autoWidth: false,

                "language": {
                    "lengthMenu": "Mostrar " +
                        `<select class = "custom-select custom-select-sm form-control form-control-sm">
                        <option value = '10'>10</option>
                        <option value = '25'>25</option>
                        <option value = '50'>50</option>
                        <option value = '100'>100</option>
                        <option value = '-1'>Todos</option>
                    </select>` +
                        " registros por página",
                    "zeroRecords": "Nada encontrado - disculpa",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtrado de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior",
                    }
                }
            });

        });
    </script>

@stop
