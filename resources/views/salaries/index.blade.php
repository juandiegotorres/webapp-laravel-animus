@extends('adminlte::page')

@section('title', 'Sueldos')

@section('plugins.Sweetalert2', true)

@section('content_header')
    <div class="container-fluid">
        @can('salaries.create')
            <a href="{{ route('salaries.create') }}" class="float-right pr-2">
                <button class="float-right btn btn-success bg-hfrut">
                    Agregar sueldo
                </button>
                <a class="float-right btn btn-primary mr-3" data-toggle='modal' data-target='#modalFiltro'><i
                        class="fa fa-filter mr-2" aria-hidden="true"></i>
                    Filtrar
                </a>
            </a>
        @endcan
        <h1 class="ml-3">Sueldos</h1>
    </div>
@stop

@section('content')
    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: 'success',
                        title: '{{ session('status') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    {{-- MODAL FILTRO --}}
    <div class="modal fade" id="modalFiltro" tabindex="-1" role="dialog" aria-labelledby="modalFiltro"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Filtros</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span id="formResult"></span>
                    <div class="row">
                        <div class="col-md-12 d-flex">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fechaInicio">Fecha de Inicio <button
                                            class="fa fa-question-circle btn p-0 m-0" data-toggle="tooltip"
                                            data-placement="bottom"
                                            title="Si no se elige una fecha de inicio, no se filtrará por fecha. Solo por proveedor"></button></label>
                                    <input type="date" name="fechaInicio" class="form-control" id="fechaInicio" required
                                        max='2099-12-31' min='1900-01-01'>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fechaInicio">Fecha de Fin</label>
                                    <input type="date" name="fechaFin" class="form-control"
                                        value="{{ Carbon\Carbon::today()->format('Y-m-d') }}" id="fechaFin"
                                        max='2099-12-31' min='1900-01-01' required>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="form-group mx-3">
                                <label for="empleados" class="col-form-label">Empleado</label>
                                <select name="empleados" id="empleadosFiltro" class="form-control">
                                    @if ($empleados->count() == 0)
                                        <option value="">Sin empleados disponibles</option>
                                    @endif
                                    <option value="">Seleccione un empleado</option>
                                    @foreach ($empleados as $empleado)
                                        <option value="{{ $empleado->id }}">
                                            {{ $empleado->nombreCompleto }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-primary" data-tipo="" id="btnFiltrar">Buscar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="card mt-2">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row justify-content-end">
                        <div class="col-md-4 align-self-end pb-3 text-right">
                            <span class="badge badge-secondary p-2 d-none" id="badgeFiltro">
                                <i class="fas fa-times pl-3"></i>
                            </span>
                        </div>
                    </div>
                    <table class="table table-bordered" id="tablaSueldos">
                        <thead class="bg-hfrut">
                            <tr>
                                <th>Empleado</th>
                                <th>Precio(Hora)</th>
                                <th>Horas</th>
                                <th>Total</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Fin</th>
                                <th style="width: 8em !important;">Observaciones</th>
                                <th style="width: 8em !important;">Opciones</th>
                                {{-- <th style="width:10px">Eliminar</th> --}}
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {
            //SECTION Filtrar
            $('#empleadosFiltro').select2({
                dropdownParent: $("#modalFiltro"),
            });


            // EVENTO Click filtrar 
            $('#btnFiltrar').click(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{{ route('salaries.filtrar') }}",
                    data: {
                        fechaInicio: $('#fechaInicio').val(),
                        fechaFin: $('#fechaFin').val(),
                        empleado: $('#empleadosFiltro').val(),
                        _token: "{{ csrf_token() }}",
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.error) {

                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#formResult').html(html);
                            return
                        } else {
                            tablaSueldos.clear().rows.add(data.data).draw();
                            $('#badgeFiltro').removeClass('d-none');
                            var fechaInicio = new Date($('#fechaInicio').val())
                            var fechaFin = new Date($('#fechaFin').val())
                            var nombreEmpleado = $('#empleadosFiltro :selected').text();
                            if ($('#empleadosFiltro').val() == "") {
                                $('#badgeFiltro').html('Filtrando entre ' + fecha.format(
                                        fechaInicio) +
                                    ' y ' + fecha.format(
                                        fechaFin) +
                                    '<i class="fas fa-times pl-3"></i>');
                            } else if ($('#fechaInicio').val() == "") {
                                $('#badgeFiltro').html('Filtrando por empleado: ' +
                                    nombreEmpleado + '<i class="fas fa-times pl-3"></i>');
                            } else {
                                $('#badgeFiltro').html('Filtrando entre ' + fecha.format(
                                        fechaInicio) + ' y ' +
                                    fecha.format(
                                        fechaFin) +
                                    '. Empleado ' +
                                    nombreEmpleado + '<i class="fas fa-times pl-3"></i>');
                            }
                            $('#modalFiltro').trigger('click');
                        }
                    }
                });
            });
            //EVENTO Click remover filtro
            //Cuando hago click en la X del filtro me vuelve a mostrar todos los registros nuevamente
            $('#badgeFiltro').on('click', 'i', function(e) {
                tablaSueldos.ajax.reload();
                $('#badgeFiltro').addClass('d-none');
                $('#fechaInicio').val('')
            });


            var fecha = new Intl.DateTimeFormat("es-AR");

            //EVENTO Esconde modal filtro
            $("#modalFiltro").on("hide.bs.modal", function(event) {
                //Borro si hay algun error
                $('#formResult').children().remove();
                $('#empleadosFiltro option:eq(0)').prop('selected', true);
                $('#empleadosFiltro').trigger('change.select2');
                $('#fechaInicio').empty();
            });


            //!SECTION

            $(document).on('click', '.btnEliminar', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea dar de baja este sueldo?",
                    // text: "Una vez eliminada, no se puede recuperar",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post(`/sueldos/` + id + `/eliminar`, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    tablaSueldos.ajax.reload()
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: "Sueldo dado de baja",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                    }
                });
            });

            var tablaSueldos = $('#tablaSueldos').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                bFilter: true,
                bLengthChange: true,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: {
                    url: '/sueldos/dt',
                },
                columns: [{
                        data: 'empleado',
                    },
                    {
                        data: 'precioHora',
                        render: function(precioHora) {
                            return formatear.format(precioHora);
                        }
                    },
                    {
                        data: 'horas',
                    },
                    {
                        data: 'total',
                        render: function(total) {
                            return formatear.format(total);
                        }
                    },
                    {
                        data: 'fechaInicio',
                    },
                    {
                        data: 'fechaFin',
                    },
                    {
                        data: 'observaciones',
                    },
                    {
                        data: 'eliminar',
                    },
                    {
                        data: 'created_at',
                        visible: false,
                    },

                ],
                order: [
                    [8, "desc"]
                ]
            });

            //Funcion para formatear numeros a formato de dinero
            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });

            $('body').tooltip({
                selector: '[data-toggle="tooltip"]'
            });
        })
    </script>
@stop
