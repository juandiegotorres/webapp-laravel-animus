@extends('adminlte::page')

@section('title', 'Nuevo Cliente')

@section('plugins.Select2', true)

@section('content_header')
    <p class="pl-4 h2">Sueldo</p>
@stop

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">

                <div class="card card-primary border-top">
                    {!! Form::open(['route' => 'salaries.store', 'class' => 'p-4']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('employee_id', 'Empleado (*)') !!}
                                {!! Form::select('employee_id', $empleados->pluck('nombreCompleto', 'id'), null, ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'empleados']) !!}

                                @error('employee_id')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('horas', 'Horas trabajadas (*)') !!}
                                {!! Form::number('horas', null, ['autocomplete' => 'off', 'id' => 'txtHoras', 'class' => 'form-control', 'placeholder' => 'Cantidad de horas trabajadas...']) !!}
                                @error('horas')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('precioHora', 'Precio por Hora (*)') !!}
                                {!! Form::number('precioHora', null, ['autocomplete' => 'off', 'id' => 'txtPrecioHora', 'class' => 'form-control', 'placeholder' => 'Precio por hora...']) !!}
                                @error('precioHora')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('total', 'Total (*)') !!}
                                {!! Form::number('total', null, ['autocomplete' => 'off', 'id' => 'txtTotal', 'class' => 'form-control', 'readonly']) !!}
                                @error('total')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                    </div>

                    @php
                        $fechaActual = now()->format('Y-m-d');
                        $fechaAnterior = now()
                            ->subMonth()
                            ->format('Y-m-d');
                    @endphp
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('fechaInicio', 'Fecha Inicio (*)') !!}
                                {!! Form::date('fechaInicio', $fechaAnterior, ['class' => 'form-control']) !!}
                                @error('fechaInicio')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('fechaFin', 'Fecha Fin (*)') !!}
                                {!! Form::date('fechaFin', $fechaActual, ['class' => 'form-control']) !!}
                                @error('fechaFin')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('observaciones', 'Observaciones') !!}
                                {!! Form::textarea('observaciones', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Observaciones...', 'rows' => '3']) !!}
                                @error('observaciones')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>




                    <div class="row mt-3">
                        <div class="col-md-12">
                            <a href="{{ route('salaries.index') }}" class="btn btn-danger mr-2">
                                Cancelar
                            </a>
                            {!! Form::submit('Agregar sueldo', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        $(document).ready(function() {

            $('#empleados').select2();

            var totalParcial = 0;
            $('#txtHoras').keyup(function(e) {
                totalParcial = ($('#txtHoras').val() * $('#txtPrecioHora').val());
                $('#txtTotal').val(totalParcial);
            });
            $('#txtPrecioHora').keyup(function(e) {
                totalParcial = ($('#txtHoras').val() * $('#txtPrecioHora').val());
                $('#txtTotal').val(totalParcial);
            });

        })
    </script>

@stop
