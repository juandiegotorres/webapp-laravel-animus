@extends('adminlte::page')

@section('title', 'Dashboard')

@section('plugins.Sweetalert2', true)

@section('content_header')
    <div class="float-right pr-2">
        <a href="{{ route('seleccionar.cliente.cobros') }}" class="btn btn-success bg-hfrut">
            Agregar cobro a cliente
        </a>
    </div>
    <h1 class="pl-2">Cobros</h1>
@stop

@section('content')
    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: 'success',
                        title: '{{ session('status') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    {{-- TABLA --}}
                    <div class="row px-2">
                        <div class="col-md-4 xd">
                            <label for="clientes">Filtrar por cliente</label>
                            <select name="clientes" id="filtroClientes" class="form-control filter-select">
                                <option value="0">Ninguno</option>
                                @foreach ($clientes as $cliente)
                                    <option value="{{ $cliente->id }}">{{ $cliente->nombreCompleto }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-1 mt-2 pl-0">
                            <button class="btn btn-danger flex-fill mt-4 mb-0" data-toggle="tooltip" data-placement="right"
                                title="Eliminar filtrado por cliente" id="btnEliminarFiltroCliente" disabled><i
                                    class="fa fas fa-times-circle"></i></button>
                        </div>
                    </div>
                    <hr>
                    <table class="table table-bordered table-rounded" id="tablaCobros">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Cliente</th>
                                <th scope="col" style="width:10%;">Fecha</th>
                                <th scope="col">IVA</th>
                                <th scope="col">Monto total</th>
                                <th scope="col" style="width:16%;">Método de pago</th>
                                <th scope="col">Observaciones</th>
                                <th scope="col" style="width:15px">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalMetodosPago" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title" id="modalTitulo">Metodos de pago usados:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Metodo de pago</th>
                                        <th>Monto</th>
                                        <th>Nro Cheque</th>
                                    </tr>
                                </thead>
                                <tbody id="tablaMetodoPago">

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {
            $('#filtroClientes').select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            //EVENTO MODAL VER METODOS DE PAGO 
            $('#modalMetodosPago').on('show.bs.modal', function(e) {
                var button = $(e.relatedTarget)
                var idcobro = button.data('id')

                $.get("/cobros/obtener-metodos-pago/" + idcobro,
                    function(data) {
                        var html = "";
                        data.forEach(pago => {
                            var cheque = '';
                            pago.nroCheque == null ? cheque = "" : cheque = pago.nroCheque;
                            html += "<tr><td>" + pago.nombre + "</td>" +
                                "<td>" + formatear.format(pago.monto) + "</td>" +
                                "<td>" + cheque + "</td></tr>"
                        });
                        $('#tablaMetodoPago').html(html);
                        console.log(html);
                    },
                    "json"
                );
            })

            $('#modalMetodosPago').on('hide.bs.modal', function(e) {
                $('#tablaMetodoPago').empty();
            })

            //======================== ELIMINAR COBRO ===================================
            $(document).on('click', '.btnEliminarCobro', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea dar de baja este cobro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post('/cobros/eliminar/' + id, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    tablaCobros.ajax.reload();
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: "Cobro dada de baja",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                    }
                });
            });
            //========================= EVENTO CAMBIA EL SELECT DE CLIENTES ======================
            $('#filtroClientes').change(function(e) {
                tablaCobros.ajax.reload();
                $('#btnEliminarFiltroCliente').prop('disabled', false);
            });


            //======================= CLICK BOTON ELIMINAR FILTRO
            $('#btnEliminarFiltroCliente').click(function(e) {
                e.preventDefault();
                $('#filtroClientes').val(0).change();
                $(this).prop('disabled', true);
            });
            //==================== DATATABLE ===========================
            tablaCobros = $('#tablaCobros').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: true,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: {
                    "url": "{{ route('cobros.dt') }}",
                    "data": function(d) {
                        d.idcliente = $('#filtroClientes').val();
                        d._token = "{{ csrf_token() }}";
                    },
                    "type": "POST",
                    "dataType": "json"
                },
                columns: [{
                        data: 'nombreCompleto'
                    },
                    {
                        data: 'fecha'
                    },
                    {
                        data: 'iva'
                    },
                    {
                        data: 'montoTotal'
                    },
                    {
                        data: 'metodoPago',
                    },
                    {
                        data: 'observaciones',
                        render: function(observaciones) {
                            if (observaciones == null) {
                                return 'Sin observaciones';
                            } else {
                                return observaciones;
                            }
                        }

                    },
                    {
                        data: 'opciones'
                    },
                    {
                        data: 'created_at',
                        visible: false
                    },
                ],
                order: [1, 'desc']
            });

            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });
        });
    </script>
@stop
