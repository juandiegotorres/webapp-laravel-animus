@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar cobro</h1>
@stop

@section('content')
    <div class="col-md-8 col-sm-10 col-lg-8">
        <div class="card">
            <div class="card-body" id="app">
                {!! Form::model($cobro, ['route' => ['cobros.update', $cobro], 'method' => 'put', 'class' => '']) !!}
                {!! Form::hidden('provider_id', $cobro->cliente->id) !!}
                @error('provider_id')
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('fecha', 'Fecha (*)', ['class' => 'col-form-label']) !!}
                            {!! Form::date('fecha', $cobro->fecha, ['class' => 'form-control']) !!}
                            @error('fecha')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('montoTotal', 'Monto (*)', ['class' => 'col-form-label']) !!}
                            {!! Form::number('montoTotal', null, ['class' => 'form-control', 'placeholder' => 'Monto total del cobro...']) !!}
                            @error('montoTotal')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('metodoPago', 'Método de pago (*)', ['class' => 'col-form-label']) !!}
                            {!! Form::select('metodoPago', ['Efectivo' => 'Efectivo', 'Transferencia' => 'Transferencia', 'Cheque' => 'Cheque', 'Tarjeta de crédito' => 'Tarjeta de crédito', 'Tarjeta de débito' => 'Tarjeta de débito'], null, ['class' => 'form-control']) !!}
                            @error('metodoPago')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::label('observaciones', 'Observaciones', ['class' => 'col-form-label']) !!}
                        {!! Form::textarea('observaciones', null, ['class' => 'form-control', 'rows' => '2']) !!}
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 d-flex justify-content-end">
                        <a class="btn btn-danger" onclick="window.history.back();">Volver</a>
                        <button type="submit" class="btn btn-success ml-2">Guardar</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@stop
