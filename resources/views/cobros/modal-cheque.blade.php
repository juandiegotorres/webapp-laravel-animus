<div class="modal fade" id="modalCrearCheque" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear cheque</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'cheques.store', 'class' => 'p-4', 'autocomplete' => 'off', 'id' => 'formCheque']) !!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('nroCheque', 'Número de cheque (*)') !!}
                            {!! Form::number('nroCheque', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Número de cheque ...']) !!}
                            <span class="invalid-feedback d-block" role="alert" id="error-nroCheque">
                                <strong></strong>
                            </span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('fechaRecepcion', 'Fecha de recepción (*)') !!}
                            {!! Form::date('fechaRecepcion', null, ['class' => 'form-control']) !!}
                            <span class="invalid-feedback d-block" role="alert" id="error-fechaRecepcion">
                                <strong></strong>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('fechaCobro', 'Fecha de cobro (*)') !!}
                            {!! Form::date('fechaCobro', null, ['class' => 'form-control']) !!}
                            <span class="invalid-feedback d-block" role="alert" id="error-fechaCobro">
                                <strong></strong>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('titular', 'Titular (*)') !!}
                            {!! Form::text('titular', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Titular ...']) !!}
                            <span class="invalid-feedback d-block" role="alert" id="error-titular">
                                <strong></strong>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('entregadoPor', 'Recibido de:') !!}
                            {!! Form::text('entregadoPor', null, ['class' => 'form-control', 'placeholder' => 'Recibido de: ...']) !!}
                            <span class="invalid-feedback d-block" role="alert" id="error-entregadoPor">
                                <strong></strong>
                            </span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('entregadoA', 'Entregado a:') !!}
                            {!! Form::text('entregadoA', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Entregado a: ...']) !!}
                            <span class="invalid-feedback d-block" role="alert" id="error-entregadoA">
                                <strong></strong>
                            </span>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('importe', 'Importe (*)') !!}
                            {!! Form::number('importe', null, ['step' => '0.01', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Importe...']) !!}
                            <span class="invalid-feedback d-block" role="alert" id="error-importe">
                                <strong></strong>
                            </span>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('bank_id', 'Banco (*)') !!}
                            <div class="d-flex">
                                {!! Form::select('bank_id', $bancos->pluck('nombre', 'id'), null, ['autocomplete' => 'off', 'class' => 'form-control pr-3']) !!}
                                <a class="btn btn-success btn-sm ml-2 mr-1" data-toggle="modal"
                                    data-target="#modalBancos" id="btnAgregarBanco"> <i class="fa fa-plus pt-2"></i>
                                </a>
                                <a class="btn btn-primary btn-sm mr-1" data-toggle="modal" data-target="#modalBancos"
                                    id="btnEditarBanco">
                                    <i class=" fa fa-pen pt-2"></i>
                                </a>
                            </div>
                            <span class="invalid-feedback d-block" role="alert" id="error-bank_id">
                                <strong></strong>
                            </span>
                        </div>


                    </div>
                    {!! Form::hidden('tipoCheque', 'recibido') !!}
                    {!! Form::hidden('desdeCobros', 'si') !!}

                </div>
                <div class="row mt-3 ">
                    <div class="col-md-12 d-flex justify-content-end">
                        <a class="btn btn-danger mr-2" id="cerrar">
                            Cancelar
                        </a>
                        <button class="btn btn-success" id="agregarCheque">Agregar cheque</button>

                    </div>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal fade" id="modalBancos" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="formAgregarEditarBanco" autocomplete="off">
                <div id="agregarBanco">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <span id="resultFormAgregar"></span>
                                <div class="form-group">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" name="nombre" id="nombreBanco" class="form-control"
                                        placeholder="Nombre..." aria-describedby="helpId">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"
                            id="btnCerrarModal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="btnGuardarBanco">Agregar</button>
                        <button type="button" class="btn btn-primary" id="btnGuardarEditarBanco">Editar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
