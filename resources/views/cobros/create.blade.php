@extends('adminlte::page')

@section('title', 'Agregar cobro')

@section('content_header')
    <div class="px-3">
        <h1 class="">Agregar cobro a cliente: </h1>
        <h5 class=" mb-0 pb-0">{{ $client->nombreCompleto . ' - CUIT: ' . $client->cuit }}</h5>
    </div>
@stop

@section('css')
    <style>
        .card {
            box-shadow: 0 0 0px;
            background-color: #F4F6F9;
            border: 0px;
        }

        .card-footer {
            border-top: 0px;
            background-color: #F4F6F9;
        }

        .imagen-compra-card {
            padding: 3rem;
            border: 4px solid rgb(41, 100, 62);
            border-radius: 20px;
        }

    </style>
@endsection

@section('content')
    <div class="col-md-10 col-sm-10 col-lg-10">
        <div class="card">
            <div class="card-body" id="app">
                <div class="col-md-12" id="resultadoForm"></div>
                {!! Form::open(['class' => '', 'id' => 'formCobro']) !!}
                {!! Form::hidden('client_id', $client->id) !!}
                @error('client_id')
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('fecha', 'Fecha (*)', ['class' => 'col-form-label']) !!}
                            {!! Form::date('fecha', now(), ['class' => 'form-control']) !!}
                            @error('fecha')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('metodoPago', 'Método de pago (*)', ['class' => 'col-form-label']) !!}
                            {!! Form::select('metodoPago', $metodosPago->pluck('nombre', 'id'), null, ['class' => 'form-control', 'id' => 'metodosPago']) !!}
                            @error('metodoPago')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('montoTotal', 'Monto (*)', ['class' => 'col-form-label']) !!}
                            {!! Form::number('montoTotal', null, ['class' => 'form-control', 'placeholder' => 'Monto...', 'id' => 'montoPago']) !!}
                            @error('montoTotal')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="" class="col-form-label">ㅤ</label>
                            <a id="agregarMetodoPago" class="btn btn-primary form-control">Agregar</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped d-none">
                            <thead>
                                <tr>
                                    <th scope="col">Método Pago</th>
                                    <th scope="col">Nro Cheque (opcional)</th>
                                    <th scope="col">Monto</th>
                                </tr>
                            </thead>
                            <tbody id="tablaPagos">

                            </tbody>
                            <tfoot class="mt-3">
                                <tr class="text-right">
                                    <td colspan="3">
                                        <h4 id="total"></h4>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="iva">IVA</label>
                            <input type="number" name="iva" id="iva" class="form-control" placeholder="IVA...">
                        </div>
                    </div>
                    <div class="col-md-12">
                        {!! Form::label('observaciones', 'Observaciones', ['class' => 'col-form-label']) !!}
                        {!! Form::textarea('observaciones', null, ['class' => 'form-control', 'rows' => '2']) !!}
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 d-flex justify-content-end">
                        <a class="btn btn-danger" onclick="window.location.replace('/cobros');">Cancelar</a>
                        <button class="btn btn-success ml-2" id="btnGuardar">Guardar</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        {{-- ====== MODAL PARA CREAR UN CHEQUE ====== --}}
        @include('cobros.modal-cheque')
        {{-- ====== MODAL PARA ELEGIR SI CREAR O SELECCIONAR CHEQUE ====== --}}
        <div class="modal fade" tabindex="-1" id="modalSeleccionarCheque" data-backdrop="static" data-keyboard="false"
            tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            id="cerrarModalCheque">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body d-flex justify-content-center">
                                        <img src="{{ asset('img/plus-circle-solid.png') }}" alt="" width="150"
                                            class="imagen-compra-card">
                                    </div>
                                    <div class="card-footer text-center">
                                        <button class="btn btn-login" id="btnCrearCheque">Crear cheque</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body d-flex justify-content-center">
                                        <img src="{{ asset('img/list-ul-solid.png') }}" alt="" width="150"
                                            class="imagen-compra-card">
                                    </div>
                                    <div class="card-footer text-center">
                                        <button class="btn btn-login" id="btnSeleccionarCheque">Seleccionar cheque</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"
                            id="cerrarModalCheque1">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- ====== MODAL PARA SELECCIONAR CHEQUE ====== --}}
        <div class="modal fade" id="modalCheques" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Cheques</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Nro Cheque</th>
                                    <th scope="col">Tipo Cheque</th>
                                    <th scope="col">Importe</th>
                                    <th scope="col">Fecha de creación</th>
                                    <th scope="col" class="text-center">Seleccionar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cheques as $cheque)
                                    <tr>
                                        <td class="nroCheque">{{ $cheque->nroCheque }}</td>
                                        <td class="tipoCheque">{{ ucfirst($cheque->tipoCheque) }}</td>
                                        <td class="importe">$
                                            {{ number_format($cheque->importe, 2, ',', '.') }}<input type='hidden'
                                                value="{{ $cheque->importe }}"></td>
                                        <td>{{ date('d/m/y', strtotime($cheque->created_at)) }}</td>
                                        <td class="text-center"><button
                                                class="btn btn-success btn-sm btnSeleccionarCheque"
                                                data-id="{{ $cheque->id }}">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            </button></td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {
            var total = 0;
            //SECTION Cobros
            //EVENTO QUE ABRE EL MODAL DE CHEQUES CUANDO SE SELECCIONA EN EL SELECT
            $('#metodosPago').change(function(e) {
                e.preventDefault();
                //Significa que seleccione un cheque
                if ($("#metodosPago option:selected").val() == 3) {
                    $('#modalSeleccionarCheque').modal('show');
                }
                $('#montoPago').focus();
            });

            //EVENTO Elegir crear cheque

            $('#btnCrearCheque').click(function(e) {
                e.preventDefault();
                $('#modalSeleccionarCheque').modal('hide');
                $('#modalCrearCheque').modal('show');
            });

            //EVENTO Elegir seleccionar cheque

            $('#btnSeleccionarCheque').click(function(e) {
                e.preventDefault();
                $('#modalSeleccionarCheque').modal('hide');
                $('#modalCheques').modal('show');
            });


            //EVENTO CLICK EN EL BOTON DENTRO DEL MODAL PARA SELCCIONAR EL CHEQUE 
            $(document).on('click', '.btnSeleccionarCheque', function(e) {
                e.preventDefault();
                var idcheque = $(this).data('id');
                var nroCheque = $(this).closest('td').siblings('td.nroCheque').text();
                var importe = $(this).closest('td').siblings('td.importe').children().val();
                $(this).attr('disabled', true);
                agregarPagoTabla(nroCheque, idcheque, importe);
                $("#metodosPago").val($("#metodosPago option:first").val());

                $('#modalCheques').trigger('click');
            });

            //EVENTO AGREGAR UN METODO DE PAGO CON SU MONTO RESPECTIVO 
            $('#agregarMetodoPago').click(function(e) {
                e.preventDefault();
                var error = 0;
                if (!$('#montoPago').val()) {
                    invalidFeedback('#montoPago', '#error-montopago', 'El campo monto no puede esar vacio')
                    error += 1
                } else if (0 > $('#montoPago').val()) {
                    invalidFeedback('#montoPago', '#error-montopago',
                        'El campo monto no puede ser menor que 0')
                    error += 1
                } else {
                    removeFeedback('#montoPago', '#error-montopago');
                }

                if (error == 0) {
                    agregarPagoTabla('', '', $('#montoPago').val())
                }
            });
            //====================== FUNCION QUE AGREGA EL HTML A LA TABLA ========================================
            function agregarPagoTabla(nroCheque, idCheque, importe) {
                var metodoPago = $("#metodosPago option:selected").text();
                var idMetodoPago = $("#metodosPago option:selected").val();
                var row = '<tr>' +
                    '<td>' + metodoPago + '<input type="hidden" name="id_metodo_pago[]" value="' + idMetodoPago +
                    '"></td>' +
                    '<td>' + nroCheque + '<input type="hidden" name="id_cheque[]" value="' + idCheque +
                    '"></td>' +
                    '<td>' + formatear.format(importe) + '<input type="hidden" name="importe[]" value="' + importe +
                    '"></td>';
                $('table').removeClass('d-none');
                console.log(importe);
                total += parseInt(importe);
                $('#total').text('Total: ' + formatear.format(total));
                $('#tablaPagos').append(row);
                $('#montoPago').val('');
                $("#metodosPago").val($("#metodosPago option:first").val());
                $('#montoPago').focus();
            }


            //EVENTO GUARDAR COBRO A CLIENTE 

            $('#btnGuardar').click(function(e) {
                e.preventDefault();
                var cantidadFilas = $('#tablaPagos tr').length;
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 4200,
                    timerProgressBar: true,
                })

                if (cantidadFilas == 0) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Debe agregar al menos un metodo de pago'
                    })
                    return;
                } else if (!$('#iva').val()) {
                    Toast.fire({
                        icon: 'error',
                        title: 'El campo IVA no puede estar vacio'
                    })
                    return;
                } else if ($('#iva').val() < 0) {
                    Toast.fire({
                        icon: 'error',
                        title: 'El campo IVA no puede ser menor que 0'
                    })
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "{{ route('cobros.store') }}",
                    data: $('#formCobro').serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.error) {
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 my-2" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoForm').html(html);
                        }

                        if (data.success) {
                            document.location.href = "{{ route('cobros.index') }}";
                        }
                    }
                });
            });

            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });


            //==================== FUNCION ERROR DE VALIDACION ============================
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }
            //!SECTION
            //SECTION MODAL CHEQUES

            $('#bank_id').select2();
            //Hacer focus en la barra de busqueda cuando se abre el select de bancos
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            //EVENTO Hide 
            $('#modalCheques').on('hide.bs.modal', function() {
                $("#metodosPago").val($("#metodosPago option:first").val())
            });

            //EVENTO Modal Bancos
            $('#modalBancos').on('hide.bs.modal', function() {
                $('#resultFormAgregar').empty();
                $('#nombreBanco').val('');
            });

            $('#modalBancos').on('shown.bs.modal', function() {
                $('#nombreBanco').focus();
            });
            $('#modalBancos').on('show.bs.modal', function(event) {
                var idBoton = $(event.relatedTarget).prop('id');
                if (idBoton == "btnEditarBanco") {
                    $('#btnGuardarBanco').hide();
                    $('#btnGuardarEditarBanco').show();
                    $('#nombreBanco').val($('#bank_id option:selected').text());
                } else {
                    $('#btnGuardarBanco').show();
                    $('#btnGuardarEditarBanco').hide();
                }
            });

            //EVENTO Guardar Banco

            $('#btnGuardarBanco').on('click', function() {
                $.ajax({
                    url: '/bancos',
                    method: 'post',
                    data: $('#formAgregarEditarBanco').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultFormAgregar').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });

                            cargarBancos();

                            $('#btnCerrarModal').trigger('click');
                            $('div.modal-backdrop').remove();
                        }
                    }
                })
            });

            //EVENTO EDITAR BANCO 

            $('#btnGuardarEditarBanco').on('click', function() {
                var idBanco = $('#bank_id option:selected').val();
                $.ajax({
                    url: '/bancos/modificar/' + idBanco,
                    method: 'put',
                    data: $('#formAgregarEditarBanco').serialize() +
                        "&_token={{ csrf_token() }}",
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                            //al que le agrego automaticamente el html
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultFormAgregar').html(html);
                        }
                        if (data.success) {
                            //Si todo esta bien muestro una alerta 
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });

                            cargarBancos(idBanco);
                            console.log('%ccreate.blade.php line:315 idBanco',
                                'color: #007acc;', idBanco);
                            $('#bank_id option[value=' + idBanco + ']').prop('selected', true);

                            $('#btnCerrarModal').trigger('click');
                            $('div.modal-backdrop').remove();
                        }
                    }
                })
            });

            //EVENTO Agregar cheque

            $('#agregarCheque').click(function(e) {
                e.preventDefault();
                //Borro todos los mensajes de error
                $('form#formCheque :input').each(function(key, val) {
                    var input = $(this);
                    var idinput = "#" + input.attr('id')
                    var mensajeError = "#" + input.next().attr(
                        'id') //Obtengo el id del span que muestra el error
                    removeFeedback(idinput, mensajeError) //Borro el mensaje de error
                })

                $.ajax({
                    type: "post",
                    url: "{{ route('cheques.store') }}",
                    data: $('#formCheque').serialize() + "&_desdeCobros='SI'",
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 2300,
                                timerProgressBar: true,
                            })
                            Toast.fire({
                                icon: 'success',
                                title: data.success[0]
                            })
                        }
                        const cheque = data.success[1][0];

                        agregarPagoTabla(cheque.nroCheque, cheque.id, cheque.importe)
                        $('#modalCrearCheque').trigger('click');
                    },
                    error: function(data) {
                        var errors = data.responseJSON.errors;
                        $.each(errors, (key, value) => {
                            var labelError = "#error-" + key;
                            invalidFeedback(key, labelError, value[0])
                        });
                    }
                });
            });

            //FUNCION CARGAR BANCOS AL SELECT 
            function cargarBancos(id = 'texto') {
                $('#bank_id').empty();
                $.ajax({
                    url: '/bancos',
                    method: 'get',
                    dataType: 'json',
                    success: function(data) {
                        var bancos = data;
                        //Los recorro y los voy agregando dentro del select
                        for (var i = 0; i < bancos.length; i++) {
                            $('#bank_id').append('<option value=' +
                                bancos[i].id + '>' +
                                bancos[i].nombre +
                                '</option>');
                        }
                        //Si id es nan significa que es un texto por lo que solo necesito seleccionar el ultimo elemento del select
                        if (isNaN(id)) {
                            $('#bank_id option:last').attr('selected', 'selected');
                        } else {
                            //Si el id es un numero lo establezco en el select
                            $('#bank_id option[value=' + id + ']').prop('selected', true);
                        }
                    }
                })
            }

            //EVENTO Click cerrar modal seleccionar metodo cheque
            $('#cerrarModalCheque').click(function(e) {
                e.preventDefault();
                $("#metodosPago").val($("#metodosPago option:first").val())
            });
            $('#cerrarModalCheque1').click(function(e) {
                e.preventDefault();
                $("#metodosPago").val($("#metodosPago option:first").val())
            });

            //!SECTION
        });
    </script>
@stop
