@extends('adminlte::page')

@section('plugins.Sweetalert2', true)

@section('title', 'Proveedores')

@section('content_header')
    <a class="float-right pr-2">
        <button class="btn btn-success bg-hfrut" data-toggle="modal" data-target="#modalCargos">
            Agregar nuevo cargo
        </button>
    </a>

    <p class="pl-2 h2">Cargos</p>
@stop

@section('content')
    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                window.location.hash = '#';
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: 'success',
                        title: '{{ session('status') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">

                    {{-- MODAL PARA AGREGAR CARGO --}}
                    <div class="modal fade" id="modalCargos" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">

                                    <h5 class="modal-title" id="modalCargosLabel">Agregar nuevo cargo</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                {!! Form::open(['route' => ['charges.store', '#create'], 'id' => 'formCargos']) !!}
                                <div class="modal-body">

                                    <div class="form-group">
                                        <label for="nombreCargo" class="col-form-label">Nombre del cargo:</label>
                                        {!! Form::text('nombreCargo', null, ['autocomplete' => 'off', 'id' => 'nombreCargo', 'autofocus', 'required', 'class' => 'form-control', 'placeholder' => 'Nombre del cargo...']) !!}
                                        @error('nombreCargo')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    {{-- <button type="button" class="btn btn-primary">Agregar cargo</button> --}}
                                    {!! Form::submit('Agregar cargo', ['class' => 'btn btn-success']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                    {{-- MODAL PARA EDITAR CARGO --}}
                    <div class="modal fade" id="modalCargosEditar" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalCargosLabelEditar">Editar cargo</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                {!! Form::open(['route' => ['charges.update', '#edit']]) !!}
                                @method('PUT')
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="nombreCargoEditar" class="col-form-label">Nombre del cargo:</label>
                                        {!! Form::text('nombreCargoEditar', null, ['id' => 'nombreCargoEditar', 'autocomplete' => 'off', 'class' => 'form-control', 'required']) !!}
                                        @error('nombreCargoEditar')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    {!! Form::hidden('id', null, ['id' => 'idCargo', 'type' => 'hidden']) !!}
                                    {{-- <input type="hidden" id="idCargo" name="id" /> --}}
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        Cerrar
                                    </button>
                                    {!! Form::submit('Modificar cargo', ['class' => 'btn btn-success']) !!}

                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>


                    {{-- TABLA --}}
                    <table class="table table-bordered table-rounded" id="cargos">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Cargo</th>
                                <th scope="col" style="width:15px">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($charges as $charge)
                                <tr class="data-row">
                                    <td>{{ $charge->nombre }}</td>
                                    <td>
                                        <div class="d-flex justify-content-end">

                                            <a data-toggle="modal" data-target="#modalCargosEditar"
                                                data-nombrecargo="{{ $charge->nombre }}" data-id="{{ $charge->id }}">
                                                <button class=" btn btn-primary btn-sm mr-2">
                                                    <i class="fa fa-pen"></i>
                                                </button>
                                            </a>
                                            <dar-baja id="{{ $charge->id }}" ruta="cargos" entidad="cargo">
                                            </dar-baja>

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script>
        $(document).ready(function() {

            //AGREGAR CARGO

            //Si la URL posee el hash #edit se va a abrir nuevamente para que el cliente pueda modificar los datos erroneos
            //Esto solo se va a ejecutar si hay un error de validacion
            if (window.location.hash === '#create') {
                $('#modalCargos').modal('show');
            }

            $('#modalCargos').on('show.bs.modal', function(event) {
                window.location.hash = '#create';
            })

            $('#modalCargos').on('shown.bs.modal', function(event) {
                $('#nombreCargo').focus();
            })

            $('#modalCargos').on('hide.bs.modal', function(event) {
                window.location.hash = '#';
                $('#nombreCargo').val('');
                //Deshabilito el mensaje de error si es que hay al cerrar el modal, de todas formas se vuelve a mostrar si
                //es necesario cuando enviamos el formulario
                var modal = $(this);
                modal.find('.invalid-feedback strong').addClass('d-none');
            })

            //MODIFICAR CARGO

            //Si la URL posee el hash #edit se va a abrir nuevamente para que el cliente pueda modificar los datos erroneos
            //Esto solo se va a ejecutar si hay un error de validacion
            if (window.location.hash === "#edit") {
                $("#modalCargosEditar").modal("show");
            }

            //Cuando se muestra el modal para modificar
            $("#modalCargosEditar").on("show.bs.modal", function(event) {
                //Pongo le hash #edit para poder abrir nuevamente el modal si hay un error de validacion
                window.location.hash = "#edit";
                var button = $(event.relatedTarget);
                //Paso el nombre del cargo por variables data dentro del boton (HTML), al igual que el id
                var nombreCargo = button.data("nombrecargo");
                var idCargo = button.data("id");
                var modal = $(this);
                //Le asigno a un input tipo hidden el ID del cargo a modificar para poder enviarlo en el request
                $("#idCargo").val(idCargo);
                //Lleno el input con el nombre del cargo a modificar
                modal.find(".modal-body input").val(nombreCargo);
            });

            $("#modalCargosEditar").on("shown.bs.modal", function(event) {
                //Una vez abierto el modal hago foco sobre el input
                $("#nombreCargoEditar").focus();

            });

            $("#modalCargosEditar").on("hide.bs.modal", function(event) {
                //Cuando el modal es cerrado o escondido saco el hash #edit y vacio el input
                window.location.hash = "#";
                $('#nombreCargoEditar').val('');
                var modal = $(this);
                //Deshabilito el mensaje de error si es que hay al cerrar el modal, de todas formas se vuelve a mostrar si
                //es necesario cuando enviamos el formulario
                modal.find('.invalid-feedback strong').addClass('d-none');

            });

            //DATATABLE
            $('#cargos').DataTable({
                responsive: true,
                autoWidth: false,

                "language": {
                    "lengthMenu": "Mostrar " +
                        `<select class = "custom-select custom-select-sm form-control form-control-sm">
                        <option value = '10'>10</option>
                        <option value = '25'>25</option>
                        <option value = '50'>50</option>
                        <option value = '100'>100</option>
                        <option value = '-1'>Todos</option>
                    </select>` +
                        " registros por página",
                    "zeroRecords": "Nada encontrado - disculpa",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtrado de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior",
                    }
                }
            });
        });
    </script>

@stop
