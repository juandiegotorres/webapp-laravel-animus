@extends('adminlte::page')

@section('title', 'Envases')

@section('content_header')
    <div class="row justify-content-between px-4">
        <h1>Envases</h1>
        <button class="btn btn-hfrut bg-hfrut" data-toggle="modal" data-target="#modalEnvaseAgregarEditar">Agregar
            envase</button>
    </div>
@stop

@section('content')
    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    {{-- TABLA --}}
                    <table class="table table-bordered table-rounded" id="tablaEnvases">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Envase</th>
                                <th scope="col">Peso</th>
                                <th scope="col">Cantidad</th>
                                <th scope="col" style="width: 30%">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>

    {{-- MODAL PARA AGREGAR ENVASE --}}
    <div class="modal fade" id="modalEnvaseAgregarEditar" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title" id="labelEnvaseAgregar">Agregar nuevo envase
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="formAgregarEnvase">
                    <span id="resultadoFormEnvase"></span>
                    <div class="modal-body pt-1">
                        <div class="form-group">
                            <label for="nombre" class="col-form-label">Nombre (*)</label>
                            <input type="text" class="form-control" name="nombre" id="nombreEnvase"
                                placeholder="Nombre..." autofocus required autocomplete="off">
                            <span class="invalid-feedback" role="alert" id="error-nombre">
                                <strong>Error</strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="peso" class="col-form-label">Peso en kg (*)</label>
                            <input type="number" class="form-control" step="any" name="peso" id="peso"
                                placeholder="Peso en KG..." required autocomplete="off">
                            <span class="invalid-feedback" role="alert" id="error-peso">
                                <strong>Error</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="cantidad" class="col-form-label">Cantidad (*)</label>
                            <input type="text" class="form-control" name="cantidad" id="cantidad"
                                placeholder="Cantidad..." required autocomplete="off">
                            <span class="invalid-feedback" role="alert" id="error-cantidad">
                                <strong>Error</strong>
                            </span>
                        </div>

                        <input type="hidden" id="idEnvase">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-success" id="btnAgregarEnvase">Agregar</button>
                        <button class="btn btn-success d-none" id="btnEditarEnvase">Editar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(document).ready(function() {

            //================ EVENTO CUANDO SE MUESTRA EL MOPDAL PARA EDITAR Envase ===================
            //Cuando se muestra el modal para modificar
            $("#modalEnvaseAgregarEditar").on("show.bs.modal", function(event) {
                var button = $(event.relatedTarget);
                //Pregunto el id desde donde se disparo el modal, si tiene el id 'editarEnvase' significa que 
                //debo editar. De otra forma el modal se esa llamando desde el boton agregar
                console.log(button.attr('id'));

                if (button.attr('id') == 'editarEnvase') {
                    //Paso los datos a travez de variables data dentro del boton (HTML), al igual que el id
                    var idEnvase = button.data("id");
                    var nombre = button.data("nombre");
                    var peso = button.data("peso");
                    var cantidad = button.data("cantidad");

                    // //Le asigno a un input tipo hidden el ID del cargo a modificar para poder enviarlo en el request
                    $("#idEnvase").val(idEnvase);
                    //Asigno los valores a los inputs
                    $('#nombreEnvase').val(nombre);
                    $('#peso').val(peso);
                    $('#cantidad').val(cantidad);
                    $('#btnAgregarEnvase').addClass('d-none');
                    $('#btnEditarEnvase').removeClass('d-none');
                } else {
                    $('#btnEditarEnvase').addClass('d-none');
                    $('#btnAgregarEnvase').removeClass('d-none');
                }
            });

            $('#btnAgregarEnvase').click(function(e) {
                e.preventDefault();
                var error = 0;
                if (!$('#nombreEnvase').val()) {
                    invalidFeedback('#nombreEnvase', '#error-nombre', 'Este campo no puede estar vacio')
                    error += 1;
                } else {
                    removeFeedback('#nombreEnvase', '#error-nombre')
                }

                if (!$('#peso').val()) {
                    invalidFeedback('#peso', '#error-peso', 'Este campo no puede estar vacio')
                    error += 1;
                } else {
                    removeFeedback('#peso', '#error-peso')
                }

                if (!$('#cantidad').val()) {
                    invalidFeedback('#cantidad', '#error-cantidad', 'Este campo no puede estar vacio')
                    error += 1;
                } else {
                    removeFeedback('#cantidad', '#error-cantidad')
                }

                if (error == 0) {

                    //El boton agregar envia los datos del formulario automaticamente
                    $.ajax({
                        url: "{{ route('envases.store') }}",
                        method: "POST",
                        //Envio los datos del formulario
                        data: $('#formAgregarEnvase').serialize() +
                            "&_token={{ csrf_token() }}",
                        dataType: 'json',
                        success: function(data) {
                            if (data.error) {
                                //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                                //al que le agrego automaticamente el html
                                html =
                                    '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                                for (var i = 0; i < data.error.length; i++) {
                                    html += '<li>' + data.error[i] + '</li>';
                                }
                                html += '</ul></div>';
                                $('#resultadoFormEnvase').html(html);
                            }
                            if (data.success) {
                                //Si todo esta bien muestro una alerta 
                                Swal.fire({
                                    toast: true,
                                    icon: 'success',
                                    title: data.success,
                                    position: 'top-right',
                                    showConfirmButton: false,
                                    timer: 2300,
                                });

                                //Cierro el modal
                                $('#modalEnvaseAgregarEditar').trigger('click');

                                tablaEnvases.ajax.reload();
                            }
                        }
                    })
                } else {
                    return
                }
            });

            //======================== ENVIAR FORMULARIO MODIFICAR ENVASE ===================================

            $('#btnEditarEnvase').on('click', function(e) {
                e.preventDefault();
                var id = $('#idEnvase').val();
                var error = 0;

                if (!$('#nombreEnvase').val()) {
                    invalidFeedback('#nombreEnvase', '#error-nombre', 'Este campo no puede estar vacio')
                    error += 1;
                } else {
                    removeFeedback('#nombreEnvase', '#error-nombre')
                }

                if (!$('#peso').val()) {
                    invalidFeedback('#peso', '#error-peso', 'Este campo no puede estar vacio')
                    error += 1;
                } else {
                    removeFeedback('#peso', '#error-peso')
                }

                if (!$('#cantidad').val()) {
                    invalidFeedback('#cantidad', '#error-cantidad', 'Este campo no puede estar vacio')
                    error += 1;
                } else {
                    removeFeedback('#cantidad', '#error-cantidad')
                }



                if (error == 0) {

                    $.ajax({
                        url: "/envases/update/" + id,
                        method: "PUT",
                        //Envio los datos del formulario
                        data: $('#formAgregarEnvase').serialize() +
                            "&_token={{ csrf_token() }}",
                        dataType: 'json',
                        success: function(data) {
                            if (data.error) {
                                //Si hay algun error los recorro y los muestro sobre el modal, para esto tengo un span con el id='resultadoFormAgregar'
                                //al que le agrego automaticamente el html
                                html =
                                    '<div class = "alert alert-danger pb-0 px-0 mx-3 my-2" role = "alert" id="alertaErrores"><ul>';
                                for (var i = 0; i < data.error.length; i++) {
                                    html += '<li>' + data.error[i] + '</li>';
                                }
                                html += '</ul></div>';
                                $('#resultadoFormEnvase').html(html);
                            }
                            if (data.success) {
                                //Si todo esta bien muestro una alerta 
                                Swal.fire({
                                    toast: true,
                                    icon: 'success',
                                    title: data.success,
                                    position: 'top-right',
                                    showConfirmButton: false,
                                    timer: 2300,
                                });
                                tablaEnvases.ajax.reload();
                                //Cierro el modal
                                $('#modalEnvaseAgregarEditar').trigger('click');
                            }
                        }
                    })
                } else {
                    return
                }
            });

            //======================== ELIMINAR ENVASE ===================================
            $(document).on('click', '.btnEliminarEnvase', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea dar de baja este envase?",
                    // text: "Una vez eliminada, no se puede recuperar",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post('/envases/delete/' + id, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    tablaEnvases.ajax.reload();
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: "Envase dado de baja",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                    }
                });
            });

            //===================== MODAL AGREGAR INSUMO SERVICIO ================================
            $('#modalEnvaseAgregarEditar').on('shown.bs.modal', function(event) {
                $('#nombreEnvase').focus();
            })

            $('#modalEnvaseAgregarEditar').on('hide.bs.modal', function(event) {
                $('#nombreEnvase').val('');
                $('#peso').val('');
                $('#cantidad').val('');
                $('#resultadoFormEnvase').empty();
                removeFeedback('#nombreEnvase', '#error-nombre')
                removeFeedback('#peso', '#error-peso')
                removeFeedback('#cantidad', '#error-cantidad')
            })


            //==================== DATATABLE ===========================
            tablaEnvases = $('#tablaEnvases').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: false,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "/envases/datatable",
                columns: [{
                        data: 'nombre'
                    },
                    {
                        data: 'peso'
                    },
                    {
                        data: 'cantidad'
                    },
                    {
                        data: 'opciones'
                    },
                    {
                        data: 'created_at',
                        visible: false
                    },
                ],
                order: [3, 'desc']
            });

            //==================== FUNCION ERROR DE VALIDACION ============================
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }
        });
    </script>
@stop
