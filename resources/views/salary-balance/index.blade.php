@extends('adminlte::page')

@section('title', 'Sueldos')

@section('plugins.Sweetalert2', true)

@section('content_header')
    <div class="d-flex justify-content-between">
        <h1 class="ml-3">Balance de sueldo</h1>
        <button class="btn btn-warning" disabled id="btnImprimir"><i class="fa fa-print mr-2" aria-hidden="true"></i> Imprimir
            balance en
            pantalla</button>
        <a class="d-none" target="_blank" href="" id="btnPDF"></a>
    </div>
@stop

@section('content')
    {{-- MODAL MOSTRAR EMPLEADOS --}}
    <div class="modal fade" id="modalEmpleados" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title">Seleccione un empleado</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="empleados" class="col-form-label">Empleado:</label>
                                <select name="empleados" id="empleados" class="form-control"
                                    style="width:100%!important;">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-form-label">DNI:</label>
                                <input type="text" class="form-control" readonly id="dni">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-form-label">Cargo:</label>
                                <input type="text" class="form-control" readonly id="cargo">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <a class="btn btn-success" id="btnSeleccionar">Seleccionar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <input name="" data-idempleado="" class="form-control" id="nombreEmpleado" disabled
                    placeholder="Seleccione un empleado para mostrar el balance.."></input>
            </div>
            <div class="col-md-3 d-flex">
                <button class="btn btn-success bg-hfrut mr-2 flex-fill" data-toggle="modal" data-target="#modalEmpleados"
                    id="btnFiltroEmpleado">Seleccionar
                    empleado</button>

                <button class="btn btn-danger flex-fill" data-toggle="tooltip" data-placement="right"
                    title="Eliminar filtrado por empleado" id="btnEliminarFiltroEmpleado" disabled><i
                        class="fa fas fa-times-circle"></i></button>
            </div>

        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <div class="form-group">
                    <input type="date" class="form-control" id="fechaInicio" max='2099-12-31' min='1900-01-01' disabled>
                    <div id="validacionFechaInicio" class="invalid-feedback">
                        <strong></strong>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="date" class="form-control" id="fechaFin" value="{{ now()->format('Y-m-d') }}" disabled>
                    <div id="validacionFechaFin" class="invalid-feedback">
                        <strong></strong>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group d-flex">
                    <button class="btn btn-primary flex-fill mr-2" id="btnFiltrarFecha" disabled>Buscar</button>
                    <button class="btn btn-danger flex-fill" data-toggle="tooltip" data-placement="right"
                        title="Eliminar filtrado por fecha" id="btnEliminarFiltroFecha" disabled><i
                            class="fa fas fa-times-circle"></i></button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-2">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered" id="tablaBalanceSueldo">
                                    <thead class="bg-hfrut">
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {

            function habilitarFiltros(habilitar) {
                //Obtengo el valor de la fecha
                var miFecha = new Date();
                //La formateo
                var fechaFormateada = miFecha.toISOString().substr(0, 10)
                //Asigno el valor al input
                $('#fechaFin').val(fechaFormateada);
                $('#fechaInicio').val('');
                if (habilitar == true) {
                    //Habilito los inputs de la fecha y el boton para buscar
                    $('#fechaInicio').prop('disabled', false);
                    $('#fechaFin').prop('disabled', false);
                    $('#btnFiltrarFecha').prop('disabled', false);
                } else {
                    //Deshabilito los inputs de la fecha y el boton para buscar
                    $('#fechaInicio').prop('disabled', true);
                    $('#fechaFin').prop('disabled', true);
                    $('#btnFiltrarFecha').prop('disabled', true);
                    $('#btnEliminarFiltroFecha').prop('disabled', true);
                }
            }


            $('#empleados').select2();

            var total = 0;

            //SECTION DATATABLE 

            var tablaBalanceSueldo = $('#tablaBalanceSueldo').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                deferRender: true,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                //hago la peticion a esta url ya que no me devuelve nada y puedo inicializar el datatable sin nungun dato
                //luego cuando se seleccione un empleado le envio el parametro
                ajax: {
                    url: "/balance-sueldos/dt/empleado/",
                },
                columns: [{
                        data: 'empleado',
                        title: 'Empleado'
                    },
                    {
                        data: 'fecha',
                        title: 'Fecha',
                    },
                    {
                        data: 'tipoMovimiento',
                        title: 'Tipo de movimiento',
                        render: function(tipoMovimiento) {
                            if (tipoMovimiento == 'D') {
                                return 'Sueldo'
                            } else {
                                return 'Pago sueldo'
                            }
                        }
                    },
                    {
                        data: 'haber',
                        title: 'Haber',
                        render: function(haber) {
                            if (haber == '') {
                                return '';
                            } else {
                                //Funcion para formatear el dinero con el signo $ y las comas
                                return formatear.format(haber);
                            }
                        }
                    },
                    {
                        data: 'debe',
                        title: 'Debe',
                        render: function(debe) {
                            if (debe == '') {
                                return '';
                            } else {
                                //Funcion para formatear el dinero con el signo $ y las comas
                                return formatear.format(debe);
                            }
                        }
                    },
                    {
                        data: null,
                        title: "Total",
                        targets: 6,
                        //Voy acumulando los sueldos (debe) y voy restando los pagos de sueldo (haber) por parte
                        //del empleado
                        render: function(data, type) {
                            //Luego de cargar el datatable mas de una vez esta columna se renderizaba dos veces y se producia un
                            //total erroneo, por lo que agregando este if con el parametro display se soluciono. Ty stackoverflow
                            if (type === 'display') {
                                total += parseFloat(Number(data.debe)) - parseFloat(Number(
                                    data.haber));
                            }
                            return formatear.format(total);
                        }
                    },
                    {
                        data: 'created_at',
                        visible: false,

                    },
                ],
                order: [1, 'asc'],
            });

            //!SECTION


            //EVENTO LLENAR SELECT EMPLEADOS 
            //Peticion que me devulve un json con todos los empleados y los carga dentro del select del modal
            axios.get('/obtener-empleados-json').then((r) => {
                var empleados = r.data;
                for (var i = 0; i < empleados.length; i++) {
                    $('#empleados').append('<option value=' + empleados[i].id +
                        ' data-dni=' + empleados[i].dni + ' data-cargo=' + empleados[
                            i]
                        .nombre + '>' +
                        empleados[i].nombreCompleto +
                        '</option>');
                }
                //Cargo los valores en los inputs del modal por si hay 2 o mas empleados con un mismo nombre
                //puedan diferenciarse por el DNI
                $('#dni').val(empleados[0].dni);
                $('#cargo').val(empleados[0].nombre);
            });

            //============== EVENTO BOTON PARA CONFIRMAR DENTRO DEL MODAL ======================
            $('#btnSeleccionar').click(function(e) {
                    e.preventDefault();
                    //Obtengo los datos y los guardo en una variable respectivamente
                    var idEmpleado = $('#empleados').val();
                    var nombreEmpleado = $('#empleados option:selected').text();
                    var dni = $('#dni').val();
                    total = 0;
                    if (idEmpleado == null) {
                        Swal.fire(
                            'Error',
                            'No hay ningun empleado seleccionado.',
                            'error'
                        )
                    } else {
                        //LLeno el input de empleado para saber los datos de quien estoy viendo
                        $('#nombreEmpleado').val(nombreEmpleado + ', DNI: ' + dni);
                        $('#nombreEmpleado').attr('data-idempleado', idEmpleado);

                        $('#btnImprimir').attr("disabled", false);
                        //Cargo la tabla con el respectivo empleado
                        cargarTabla(idEmpleado);

                        //Escondo el tooltip del boton eliminar filtro
                        $('#btnEliminarFiltroEmpleado').tooltip('hide');
                        $('#btnEliminarFiltroEmpleado').attr("disabled", false);


                        $('#btnEliminarFiltroFecha').tooltip('hide');
                        $('#btnEliminarFiltroFecha').attr("disabled", true);

                        //Cierro el modal
                        $('#modalEmpleados').trigger('click');
                    }
                }

            );

            //EVENTO Imprimir balance
            $('#btnImprimir').click(function(e) {
                e.preventDefault();
                var ruta =
                    "{{ route('balance-sueldo.pdf', ['idempleado' => ':id', 'fechaFin' => ':fFin', 'fechaInicio' => ':fInc']) }}";
                ruta = ruta.replace(':id', $('#nombreEmpleado').attr('data-idempleado'));
                //Significa que ya he aplicado el filtro, de otra forma se tomarian los datos sin haber ningun filtro
                if ($('#btnEliminarFiltroFecha').prop('disabled') == false) {
                    ruta = ruta.replace(':fFin', $('#fechaFin').val());
                    ruta = ruta.replace(':fInc', $('#fechaInicio').val());
                } else {
                    var d = new Date();
                    var strDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                    ruta = ruta.replace(':fFin', strDate);
                }
                $('#btnPDF').attr('href', ruta);
                $('#btnPDF')[0].click();


            });

            var fecha = new Intl.DateTimeFormat("es-AR");

            //============= EVENTO CLICK BOTON ELIMINAR FILTRO EMPLEADO ===============
            $('#btnEliminarFiltroEmpleado').click(function(e) {
                e.preventDefault();
                $('#nombreEmpleado').val('');
                //LLeno la tabla con los datos nuevos
                tablaBalanceSueldo.ajax.url('/balance-sueldos/dt/empleado/').load();
                //Escondo el tooltip del boton y lo deshabilito
                $(this).tooltip('hide');
                $(this).attr("disabled", true);
                $('#btnImprimir').attr("disabled", true);
                habilitarFiltros(false);
            });

            //EVENTO FUNCION CARGAR TABLA SEGUN EMPLEADO
            function cargarTabla($id) {
                total = 0;
                // tablaBalanceSueldo.ajax.url('/balance-sueldos/dt/empleado/' + $id).load();
                //Pregunto si hay algun registro en la tabla, si hay habilito los filtros de fecha, sino los deshabilito

                $.get('/balance-sueldos/dt/empleado/' + $id,
                    function(data) {
                        if (data.recordsTotal == 0) {
                            tablaBalanceSueldo.clear().draw();
                            Swal.fire(
                                'Sin registros',
                                'No hay registro de sueldos, ni de pago de sueldos para este empleado.',
                                'warning'
                            )
                            $('#btnImprimir').attr("disabled", true);
                        } else {
                            tablaBalanceSueldo.clear().rows.add(data.data).draw();
                            habilitarFiltros(true)
                        }
                    },
                    "json"
                );
            }

            //======================CLICK BOTON BUSCAR (FILTRAR FECHAS) ===============================
            $('#btnFiltrarFecha').click(function(e) {
                e.preventDefault();
                total = 0;
                //Obtengo los valores de los inputs y el select y los guardo en sus respectivas variables
                var fechaInicio = $('#fechaInicio').val();
                var fechaFin = $('#fechaFin').val();
                var idEmpleado = $('#empleados').val();


                //Validacion para saber si la fecha no esta vacia
                if (!Date.parse(fechaInicio)) {
                    invalidFeedback('#fechaInicio', '#validacionFechaInicio',
                        'La fecha de inicio no puede estar vacia');
                    return;
                } else if (fechaInicio > fechaFin) {
                    invalidFeedback('#fechaInicio', '#validacionFechaInicio',
                        'La fecha de inicio no puede ser mayor que la de fin');
                    return;
                } else if (fechaInicio > '2099-12-31' || fechaInicio < '1900-01-01') {
                    invalidFeedback('#fechaInicio', '#validacionFechaInicio',
                        'Introduzca una fecha válida');
                    return;
                } else if (fechaFin > '2099-12-31' || fechaFin < '1900-01-01') {
                    invalidFeedback('#fechaFin', '#validacionFechaFin',
                        'Introduzca una fecha válida');
                    return;
                    //Validacion para saber si la fecha de fin no esta vacia
                } else if (!Date.parse(fechaFin)) {
                    invalidFeedback('#fechaFin', '#validacionFechaFin',
                        'La fecha de inicio no puede estar vacia');
                    return;
                } else if (Date.parse(fechaInicio) === Date.parse(fechaFin)) {
                    invalidFeedback('#fechaInicio', '#validacionFechaInicio',
                        'La fecha de inicio no puede ser igual que la fecha de fin');
                    return;
                }

                //Recargo la tabla con los datos filtrados
                tablaBalanceSueldo.ajax.url('/balance-sueldos/dt/empleado=' + idEmpleado + '&fechaInicio=' +
                    fechaInicio + '&fechaFin=' + fechaFin).load();

                $('#btnEliminarFiltroFecha').prop('disabled', false);

                //Saco los mensajes de error si existen
                removeFeedback('#fechaInicio', '#validacionFechaInicio');
                removeFeedback('#fechaFin', '#validacionFechaFin');
            });

            //======================CLICK BOTON ELIMINAR FILTRO FECHAS ===============================
            $('#btnEliminarFiltroFecha').click(function(e) {
                e.preventDefault();
                $('#fechaInicio').val('');
                //Obtengo el valor de la fecha
                var miFecha = new Date();
                //La formateo
                var fechaFormateada = miFecha.toISOString().substr(0, 10)
                //Asigno el valor al input
                $('#fechaFin').val(fechaFormateada);

                var idEmpleado = $('#empleados').val();
                //LLeno la tabla con los datos nuevos
                cargarTabla(idEmpleado);
                //Escondo el tooltip del boton y lo deshabilito
                $(this).tooltip('hide');
                $(this).attr("disabled", true);
            });

            //==================== FUNCION ERROR DE VALIDACION ============================
            //Funcion para agregar el el error debado del input que recibe como parametro el input al que se le va a agregar
            //el error, e label que va a contener el texto, y el texto en si
            function invalidFeedback(input, labelInvalid, text) {
                $(input).addClass('is-invalid');
                $(labelInvalid).children().text(text);
            }
            //==================== FUNCION SACAR ERROR DE VALIDACION ============================
            //Revierte la funcion de invalidFeedback()
            function removeFeedback(input, labelInvalid) {
                $(input).removeClass('is-invalid');
                $(labelInvalid).children().text('');
            }

            //Funcion para formatear numeros a formato de dinero
            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });


            $('body').tooltip({
                selector: '[data-toggle="tooltip"]'
            });

        });
    </script>
@stop
