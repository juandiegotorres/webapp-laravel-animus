<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Error</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style>
        .xd {
            width: 100%;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-content: center;
        }

        .xd div {
            width: 100%;
            align-self: center;
        }

    </style>
</head>

<body>
    <div class="xd">
        <div class="text-center">
            <h1 class="text-danger">ERROR</h1>
            <h4>{{ $status }}</h4>
        </div>
    </div>
</body>

</html>
