@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="d-flex justify-content-between px-3">
        <h1>Historial de ventas</h1>
        <a href="{{ route('seleccionar.cliente') }}" class="btn btn-succes bg-hfrut"> <i
                class="fa fa-cash-register mr-2"></i>
            Vender</a>
    </div>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-2">
                    <div class="card-body">
                        <div class="row justify-content-between">
                            <div class="col-md-2 align-self-end pb-3">
                                <span class="badge badge-secondary p-2 d-none" id="badgeFiltro">
                                    <i class="fas fa-times pl-3"></i>
                                </span>
                            </div>
                            <div class="col-md-2 align-self-end pb-3">
                                <button type="button" class="btn btn-primary btn-block " data-toggle="modal"
                                    data-target="#modalFiltro" data-tipo="IS">Filtros
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered" id="tablaVentas">
                                    <thead class="bg-hfrut">
                                        <tr>
                                            <th scope="col">Cliente</th>
                                            <th scope="col">Fecha</th>
                                            <th scope="col">IVA</th>
                                            <th scope="col">Total</th>
                                            <th scope="col" style="width: 20%;">Opciones</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalFiltro" tabindex="-1" role="dialog" aria-labelledby="modalFiltro"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Filtros</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span id="formResult"></span>
                    <div class="row">
                        <div class="col-md-12 d-flex">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fechaInicio">Fecha de Inicio <button
                                            class="fa fa-question-circle btn p-0 m-0" data-toggle="tooltip"
                                            data-placement="bottom"
                                            title="Si no se elige una fecha de inicio, no se filtrará por fecha. Solo por proveedor"></button></label>
                                    <input type="date" name="fechaInicio" class="form-control" id="fechaInicio" required
                                        max='2099-12-31' min='1900-01-01'>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fechaInicio">Fecha de Fin</label>
                                    <input type="date" name="fechaFin" class="form-control"
                                        value="{{ Carbon\Carbon::today()->format('Y-m-d') }}" id="fechaFin"
                                        max='2099-12-31' min='1900-01-01' required>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="form-group mx-3">
                                <label for="clientes" class="col-form-label">Clientes</label>
                                <select name="cliente" id="clientes" class="form-control">
                                    <option value="" selected>Seleccione un cliente</option>
                                    @foreach ($clientes as $cliente)
                                        <option value="{{ $cliente->id }}"> {{ $cliente->nombreCompleto }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-primary" data-tipo="" id="btnFiltrar">Buscar</button>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {

            $('[data-toggle="tooltip"]').tooltip()

            $('#clientes').select2({
                dropdownParent: $("#modalFiltro"),
            });

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });


            //==============EVENTO CUANDO SE ESCONDE EL MODAL ==========================
            $("#modalFiltro").on("hide.bs.modal", function(event) {
                //Borro si hay algun error
                $('#formResult').children().remove();
            });


            //==================== EVENTO CLICK PARA FILTRAR ============================
            $('#btnFiltrar').click(function(e) {
                e.preventDefault();

                var tipoCompraAFiltrar = $(this).data('tipo');
                console.log(tipoCompraAFiltrar);
                $.ajax({
                    type: "POST",
                    url: "{{ route('historial-ventas.filtrar') }}",
                    data: {
                        fechaInicio: $('#fechaInicio').val(),
                        fechaFin: $('#fechaFin').val(),
                        cliente: $('#clientes').val(),
                        _token: "{{ csrf_token() }}",
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.error) {
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#formResult').html(html);
                        } else {

                            tablaVentas.clear().rows.add(data.data).draw();

                            $('#badgeFiltro').removeClass('d-none');
                            var fechaInicio = new Date($('#fechaInicio').val())
                            var fechaFin = new Date($('#fechaFin').val())
                            var nombreCliente = $('#clientes :selected').text();
                            if ($('#clientes').val() == "") {
                                $('#badgeFiltro').html('Filtrando entre ' + fecha.format(
                                        fechaInicio) +
                                    ' y ' + fecha.format(
                                        fechaFin) +
                                    '<i class="fas fa-times pl-3"></i>');
                            } else if ($('#fechaInicio').val() == "") {
                                $('#badgeFiltro').html('Filtrando por proveedor: ' +
                                    nombreCliente + '<i class="fas fa-times pl-3"></i>');
                            } else {
                                $('#badgeFiltro').html('Filtrando entre ' + fecha.format(
                                        fechaInicio) + ' y ' +
                                    fecha.format(
                                        fechaFin) +
                                    '. Proveedor ' +
                                    nombreCliente + '<i class="fas fa-times pl-3"></i>');
                            }
                            $('#modalFiltro').trigger('click');
                        }
                    }
                });



            });
            //====================== EVENTO CLICK EN LA X DEL TAG ==============================
            //Cuando hago click en la X del filtro me vuelve a mostrar todos los registros nuevamente
            $('#badgeFiltro').on('click', 'i', function(e) {
                tablaVentas.ajax.reload();
                $('#badgeFiltro').addClass('d-none');
                $('#fechaInicio').val(null);
                $("#clientes option:selected").prop("selected", false);
                $("#clientes option:first").prop("selected", "selected");
                $('#clientes').trigger('change');
            });

            //===================== CANCELAR VENTA =============================
            $(document).on('click', '.btnEliminarVenta', function(e) {
                Swal.fire({
                    title: '¿Esta seguro de que desea cancelar esta venta?',
                    text: '',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, estoy seguro',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var idVenta = $(this).data('id');
                        $.ajax({
                            type: "put",
                            url: "/venta/eliminar/" + idVenta,
                            data: '_token={{ csrf_token() }}',
                            dataType: "json",
                            success: function(response) {
                                tablaVentas.ajax.reload();
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 2300,
                                    timerProgressBar: true,
                                })
                                Toast.fire({
                                    icon: 'success',
                                    title: response.success
                                })
                            }
                        });
                    }
                })
            });

            //==================== DATATABLE ===========================
            var tablaVentas = $('#tablaVentas').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                bFilter: true,
                bLengthChange: true,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: {
                    url: "{{ route('historial-ventas.dt') }}"
                },
                columns: [{
                        data: 'cliente'
                    },
                    {
                        data: 'fecha'
                    },
                    {
                        data: 'iva',
                    },
                    {
                        data: 'total',
                    },
                    {
                        data: 'opciones',
                        class: 'text-right'
                    },
                    {
                        data: 'created_at',
                        visible: false
                    },
                ],
                order: [5, 'desc']
            });


            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });

            var fecha = new Intl.DateTimeFormat("es-AR");
        });
    </script>
@stop
