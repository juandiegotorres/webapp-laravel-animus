@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Venta</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="invoice p-3 mb-3">
                <div class="row">
                    <div class="col-12">
                        <h4>
                            <i class="fas fa-globe"></i> HFRUT
                            <small class="float-right">Fecha:
                                {{ date('d-m-Y', strtotime($venta->fecha)) }}</small>
                        </h4>
                    </div>
                </div>
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col my-3">
                        ID Venta: <strong>#{{ $venta->id }}</strong> <br>
                        Cliente: <strong>{{ $venta->cliente->nombreCompleto }}</strong><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped text-center">
                            <thead>
                                <tr>
                                    <th>Cantidad</th>
                                    <th>Variante Producto</th>
                                    <th>Tipo precio</th>
                                    <th>Precio</th>
                                    <th>Total Parcial</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($detalles as $detalle)
                                    <tr>
                                        <td>{{ $detalle->cantidad }}</td>
                                        @php
                                            switch ($detalle->tipo) {
                                                case '0':
                                                    $tipoProducto = 'Caja, ' . $detalle->kg . ' kg(s)';
                                                    break;
                                                case '1':
                                                    $tipoProducto = 'Bolsa, ' . $detalle->kg . ' kg(s)';
                                                    break;
                                                default:
                                                    break;
                                            }
                                        @endphp
                                        <td>{{ $detalle->nombreCompleto . ' - ' . $tipoProducto }}
                                        </td>
                                        <td>
                                            @php
                                                switch ($detalle->tipo_precio) {
                                                    case 'PKG':
                                                        echo 'Precio por KG';
                                                        break;
                                                    case 'PC':
                                                        echo 'Precio por caja';
                                                        break;
                                                    case 'PP':
                                                        echo 'Precio personalizado';
                                                        break;
                                                }
                                            @endphp
                                        </td>
                                        <td>${{ number_format($detalle->precio_producto, 2, ',', '.') }}</td>
                                        <td>$ {{ number_format($detalle->subtotal, 2, ',', '.') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                    </div>
                    <div class="col-6">
                        <h2 class="text-right pr-3 my-3">Total: ${{ number_format($venta->total, 2, ',', '.') }}</h2>
                    </div>
                </div>
                <div class="row no-print">
                    <div class="col-12">

                        <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-danger">Volver</a>

                        {{-- <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-dark float-right"><i
                                class="fas fa-print"></i> Imprimir</a> --}}

                        <a href="{{ route('venta.pdf', ['venta' => $venta->id]) }}" target="_blank"
                            class="btn btn-primary float-right" style="margin-right: 5px;">
                            <i class="fas fa-download"></i> Generar PDF
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
