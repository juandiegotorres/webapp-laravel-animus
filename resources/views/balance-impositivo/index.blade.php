@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="d-flex justify-content-between">
        <h1>Balance impositivo</h1>
        <button class="btn btn-warning" id="btnImprimir"><i class="fa fa-print mr-2" aria-hidden="true"></i> Imprimir
            balance en
            pantalla</button>
        <a class="d-none" target="_blank" href="" id="btnPDF"></a>
    </div>
@stop

@section('css')
    <style>
        .custom-control-input:focus~.custom-control-label::before {
            border-color: #29643e !important;
            box-shadow: 0 0 0 0rem rgba(0, 0, 0, 0) !important;
        }

        .custom-control-input:checked~.custom-control-label::before {
            border-color: #29643e !important;
            background-color: #29643e !important;
        }

        .custom-control-input:active~.custom-control-label::before {
            background-color: #29643e !important;
            border-color: #29643e !important;
        }

        .custom-control-input:focus: not(:checked)~.custom-control-label::before {
            border-color: #29643e !important;
        }

        .borde {
            border-radius: 20px;
            border: 0.13rem solid #29643e;
            background: rgb(241, 241, 241);

        }

        .activo {
            box-shadow: rgba(50, 50, 93, 0.25) 0px 13px 27px -5px, rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;
            transition: 0.3s;
            background: white !important;
        }

        .opacidad {
            opacity: 0.5;
        }

    </style>
@endsection

@section('content')
    <div class="row justify-content-between px-3">
        <div class="col-md-5 borde pt-2 activo" id="contenedor-meses">
            <div class="row pl-4">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="filtroMeses" checked>
                    <label class="custom-control-label" for="filtroMeses"></label>
                </div>
            </div>
            <div class="form-group w-100 p-2" id="input-meses">
                <label for="">Filtrar por mes</label>
                <select class="form-control" name="" id="meses">
                    @for ($i = 1; $i < 13; $i++)
                        @php
                            \Carbon\Carbon::setLocale('es');
                            $fecha = DateTime::createFromFormat('!m', $i);
                        @endphp
                        <option value="{{ $i }}"
                            {{ now()->month < $i ? 'disabled' : (now()->month == $i ? 'selected' : '') }}>
                            {{ ucfirst(\Carbon\Carbon::parse($fecha)->translatedFormat('F')) . ' ' . now()->year }}
                        </option>
                    @endfor

                </select>
            </div>
        </div>

        <div class="col-md-5 borde pt-2" id="contenedor-fechas">
            <div class="row pl-4">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="filtroFechas">
                    <label class="custom-control-label" for="filtroFechas"></label>
                </div>
            </div>
            <div class="row opacidad" id="input-fechas">
                <div class="col-md-12 d-flex justify-content-between">
                    <div class="form-group w-100 p-2">
                        <label for="desde">Desde</label>
                        <input type="date" name="" id="desde" class="form-control"
                            value="{{ now()->subMonth()->format('Y-m-d') }}" disabled>
                    </div>
                    <div class="form-group w-100 p-2">
                        <label for="hasta">Hasta</label>
                        <input type="date" name="" id="hasta" class="form-control" value="{{ now()->format('Y-m-d') }}"
                            disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    {{-- TABLA --}}
                    <table class="table table-bordered table-rounded" id="tablaFacturas">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">N° de Factura</th>
                                <th scope="col">Fecha de emision</th>
                                <th scope="col" style="width: 20%;">Emitida/Recibida</th>
                                <th scope="col">Total Neto</th>
                                <th scope="col">IVA</th>
                            </tr>
                        </thead>
                    </table>

                    <div class="row px-3 mt-4 justify-content-center">
                        <div class="col-md-5 col-lg-4 col-sm-6 borde activo d-flex mx-2 justify-content-center">
                            <h4 class="my-1">Balance IVA: <strong class="ml-2" id="balanceIVA"></strong>
                            </h4>
                            <input type="hidden" id="balanceIVAsinFormato">

                        </div>
                        <div class="col-md-5 col-lg-4 col-sm-6 borde activo d-flex ml-2 justify-content-center">
                            <h4 class="my-1">Balance de totales: <strong class="ml-2"
                                    id="balanceTotal"></strong></h4>
                        </div>
                    </div>
                    <div class="row justify-content-center mt-3">
                        <div class="col-md-3 text-center">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" checked id="chbBalancePositivo">
                                <label class="form-check-label" for="defaultCheck1">
                                    Ver balances en positivo
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function() {
            var tablaFacturas;
            var totalIva;
            var totalTotales;
            var esPositivo = -1;
            var datos;

            function recargarTabla() {
                tablaFacturas.ajax.reload(function(data) {
                    $('#chbBalancePositivo').prop('checked') ? esPositivo = -1 : esPositivo = 1;

                    totalIva = data.balanceIva[0].balanceIva == 0 ? data.balanceIva[0]
                        .balanceIva : data.balanceIva[0].balanceIva * esPositivo;
                    totalTotales = data.balanceTotal[0].balanceTotal == 0 ? data.balanceTotal[
                        0].balanceTotal : data.balanceTotal[0].balanceTotal * esPositivo;

                    $('#balanceIVA').text(formatear.format(totalIva));
                    $('#balanceTotal').text(formatear.format(totalTotales));
                });
            }

            //EVENTO Imprimir balance
            $('#btnImprimir').click(function(e) {
                e.preventDefault();
                var ruta =
                    "{{ route('balance-impositivo.pdf', ['mes' => ':mes','desde' => ':desde','hasta' => ':hasta','verPositivo' => ':positivo']) }}";

                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,

                })

                if ($('#filtroMeses').prop('checked') == true) {
                    let mes = $('#meses').val();
                    ruta = ruta.replace(':mes', mes);
                } else if ($('#filtroFechas').prop('checked') == true) {

                    if ($('#desde').val() == $('#hasta').val()) {
                        console.log('xd');
                        Toast.fire({
                            icon: 'error',
                            title: 'Las fechas no pueden ser iguales'
                        })
                        return
                    } else if ($('#desde').val() > $('#hasta').val()) {
                        Toast.fire({
                            icon: 'error',
                            title: 'La fecha de inicio no puede ser mayor que la de fin'
                        })
                        return
                    }
                    let desde = $('#desde').val();
                    let hasta = $('#hasta').val();
                    ruta = ruta.replace(':desde', desde);
                    ruta = ruta.replace(':hasta', hasta);
                }
                if ($('#chbBalancePositivo').is(':checked') == true) {
                    ruta = ruta.replace(':positivo', 'si');
                } else {
                    ruta = ruta.replace(':positivo', 'no');
                }

                $('#btnPDF').attr('href', ruta);
                $('#btnPDF')[0].click();


            });


            $('#chbBalancePositivo').on('change', function() {
                esPositivo = -1;
                totalIva = totalIva == 0 ? totalIva : (totalIva * esPositivo)
                totalTotales = totalTotales == 0 ? totalTotales : (totalTotales * esPositivo);

                $('#balanceIVA').text(formatear.format(totalIva));
                $('#balanceTotal').text(formatear.format(totalTotales));

            });


            $('#meses').change(function(e) {
                e.preventDefault();
                recargarTabla();
            });

            $('#desde').change(function(e) {
                e.preventDefault();
                if ($(this).val() > $('#hasta').val()) {
                    Swal.fire('La fecha de inicio no puede ser mayor que la de fin', '',
                        'warning')
                } else if ($(this).val() == $('#hasta').val()) {
                    Swal.fire('Las fechas no pueden ser iguales', '', 'warning')
                } else {
                    recargarTabla();
                }
            });

            $('#hasta').change(function(e) {
                e.preventDefault();
                if ($(this).val() < $('#desde').val()) {
                    Swal.fire('La fecha de fin no puede ser menor que la de inicio', '',
                        'warning')
                } else if ($(this).val() == $('#desde').val()) {
                    Swal.fire('Las fechas no pueden ser iguales', '', 'warning')
                } else {
                    recargarTabla();
                }
            });

            $('#filtroFechas').click(function() {
                if ($(this).prop('checked') == true) {
                    /* Clase que da el efecto de deshabilidato a los inputs */
                    $('#input-meses').toggleClass('opacidad');
                    $('#input-fechas').toggleClass('opacidad');

                    /* Clase que agrega el box shadow al col-md-5 y le cambia el color de fonto */
                    $('#contenedor-meses').toggleClass('activo');
                    $('#contenedor-fechas').toggleClass('activo');

                    //Deshabilito el otro checkbox que habilita el otro filtro                    
                    $('#filtroMeses').prop('checked', false);

                    //Deshabilito el select de los meses y habilito el de las fechas
                    $('#meses').prop('disabled', true);

                    $('#desde').prop('disabled', false);
                    $('#hasta').prop('disabled', false);

                    recargarTabla();
                } else {
                    $(this).prop('checked', true)
                }

            });

            $('#filtroMeses').click(function(e) {
                if ($(this).prop('checked') == true) {
                    /* Clase que da el efecto de deshabilidato a los inputs */
                    $('#input-meses').toggleClass('opacidad');
                    $('#input-fechas').toggleClass('opacidad');

                    /* Clase que agrega el box shadow al col-md-5 y le cambia el color de fonto */
                    $('#contenedor-fechas').toggleClass('activo');
                    $('#contenedor-meses').toggleClass('activo');

                    //Deshabilito el otro checkbox que habilita el otro filtro
                    $('#filtroFechas').prop('checked', false);

                    //Deshabilito el select de los meses y habilito el de las fechas
                    $('#meses').prop('disabled', false);

                    $('#desde').prop('disabled', true);
                    $('#hasta').prop('disabled', true);

                    recargarTabla();
                } else {
                    $(this).prop('checked', true)
                }
            });

            tablaFacturas = $('#tablaFacturas').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: false,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: {
                    "url": "{{ route('balance-impositivo.datatable') }}",
                    "data": function(d) {
                        if ($('#filtroMeses').prop('checked') == true) {
                            d.mes = $('#meses').val();
                        } else {
                            d.desde = $('#desde').val();
                            d.hasta = $('#hasta').val();
                        }
                        d._token = "{{ csrf_token() }}";
                    },
                    "type": "POST",
                    "dataType": "json"
                },
                columns: [{
                        data: 'nroFactura'
                    },
                    {
                        data: 'fechaEmision'
                    },
                    {
                        data: 'emitida_recibida',
                        class: 'text-capitalize'
                    },
                    {
                        data: 'totalNeto',
                        render: function(totalNeto) {
                            return formatear.format(totalNeto);
                        }
                    },
                    {
                        data: 'IVA',
                        render: function(IVA) {
                            return formatear.format(IVA);
                        }
                    },
                    {
                        data: 'created_at',
                        visible: false
                    },
                ],
                initComplete: function(data) {
                    // console.log(data);
                    totalIva = data.json.balanceIva[0].balanceIva == 0 ? data.json
                        .balanceIva[0]
                        .balanceIva : data.json.balanceIva[0].balanceIva * esPositivo;
                    totalTotales = data.json.balanceTotal[0].balanceTotal == 0 ? data.json
                        .balanceTotal[
                            0].balanceTotal : data.json.balanceTotal[0].balanceTotal *
                        esPositivo;

                    $('#balanceIVA').text(formatear.format(totalIva));
                    $('#balanceTotal').text(formatear.format(totalTotales));
                },
                order: [5, 'desc']
            });

            //Funcion para formatear numeros a formato de dinero
            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });
        });
    </script>
@stop
