@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 class="pl-3">Entrega para descarozado</h1>
@stop

@section('content')
    <div class="col-md-8 col-sm-10 col-lg-8">
        <div class="card">
            <div class="card-body" id="app">
                <form action="{{ route('entrega-descarozado.update', ['descarozado' => $descarozado->first()->id]) }}"
                    method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="materiaPrima">Materia Prima (*)</label>
                                <select type="text" name="materiaPrima" id="materiasPrimas" class="form-control">
                                    @foreach ($materiasPrimas as $materiaPrima)
                                        <option value="{{ $materiaPrima->id }}"
                                            {{ $materiaPrima->id == $descarozado->first()->raw_material_id ? 'selected' : '' }}>
                                            {{ $materiaPrima->fruta . ', ' . $materiaPrima->variedad }}</option>
                                    @endforeach
                                </select>
                                @error('materiaPrima')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fecha">Fecha (*)</label>
                                <input type="date" name="fecha" class="form-control"
                                    value="{{ old('fecha', $descarozado->first()->fecha) }}">
                                @error('fecha')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cantidadKg">Cantidad KG (*)</label>
                                <input type="number" name="cantidadKg" id="" class="form-control"
                                    value="{{ old('cantidadKg', $descarozado->first()->cantidadKg) }}">
                                @error('cantidadKg')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Observaciones</label>
                                <textarea class="form-control" name="observaciones" id="" rows="1"
                                    value="{{ $descarozado->first()->observaciones }}"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-12 d-flex justify-content-end">
                            <a class="btn btn-danger" onclick="window.history.back();">Volver</a>
                            <button type="submit" class="btn btn-success ml-2">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('css')
@stop

@section('js')
    <script>
        $(document).ready(function() {
            $('#materiasPrimas').select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });
        });
    </script>
@stop
