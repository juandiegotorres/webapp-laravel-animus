@extends('adminlte::page')

@section('title', 'Dashboard')

@section('plugins.Sweetalert2', true)

@section('content_header')
    @can('entrega-descarozado.create')
        <a class="float-right pr-2" data-toggle="modal" data-target="#modalEmpleados">
            <button class="btn btn-success bg-hfrut">
                Agregar nueva entrega para descarozado
            </button>
        </a>
    @endcan
    <h1>Entregas descarozado</h1>
@stop


@section('content')
    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: '{{ session('status.type') }}',
                        title: '{{ session('status.message') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    {{-- MODAL MOSTRAR EMPLEADOS --}}
    <div class="modal fade" id="modalEmpleados" role="dialog">
        <form action="{{ route('entrega-descarozado.create') }}" method="POST" id="formNuevaEntrega">
            @csrf
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Seleccione un empleado</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="empleados" class="col-form-label">Empleado:</label>
                                    <select name="empleados" id="empleados" class="form-control"
                                        style="width:100%!important;">
                                        @if ($empleados->count() == 0)
                                            <option value=""> No hay empleados cargados </option>
                                        @endif
                                        @foreach ($empleados as $empleado)
                                            <option value="{{ $empleado->id }}" data-dni={{ $empleado->dni }}
                                                data-cargo="{{ $empleado->nombre }}">{{ $empleado->nombreCompleto }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-form-label">DNI:</label>
                                    <input type="text" class="form-control" readonly id="dni"
                                        value="{{ $empleados->count() == 0 ? ' ' : $empleados->first()->dni }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Cargo:</label>
                                    <input type="text" class="form-control" readonly id="cargo"
                                        value="{{ $empleados->count() == 0 ? ' ' : $empleados->first()->nombre }}">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button class="btn btn-success" id="btnSeleccionar" type="submit">Seleccionar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{-- MODAL REGISTRAR DEVOLUCION --}}
    <div class="modal fade" id="modalRegistrarDevolucion" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Registrar devolución</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        id="btnCerrarModal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="formDevolucion">
                        <span id="resultForm"></span>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Fecha de devolución (*)</label>
                                    <input type="date" name="fechaDevolucion" id="fechaDevolucion" class="form-control"
                                        value="{{ now()->format('Y-m-d') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Cantidad kg devuelta (*)</label>
                                    <input type="number" name="cantidadDevolucion" id="cantidadDevolucion"
                                        class="form-control">
                                    <small id="helpId" class="text-muted"></small>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Observaciones</label>
                                    <textarea class="form-control" name="observaciones" id="" rows="2"
                                        id="observacionesDevolucion"></textarea>
                                </div>
                            </div>
                            <label for="" class="pl-3">Porcentaje de pérdida</label>
                            <div class="contenedor-porcentaje bg-success" id="contenedorPorcentaje">
                                <span class="porcentaje" id="">0.00%</span>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnRegistrarDevolucion">Registrar devolución</button>
                </div>
            </div>
        </div>
    </div>
    {{-- MODAL MOSTRAR ENTREGA --}}
    <div class="modal fade" id="modalMostrarEntrega" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    {{-- <h5 class="modal-title" id="staticBackdropLabel">Entrega</h5> --}}
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body px-5 pt-4">
                    <div class="d-flex justify-content-between">
                        <h4 class="mb-0">Entrega</h4>
                        <a href="" target="_blank" class="btn btn-primary btn-sm" id="btnPDF">Generar PDF</a>
                    </div>
                    <hr>
                    <div class="row mb-2 ">
                        <div class="col-md-4">
                            <strong><i class="fas fa-money-check-alt mr-2"></i>Empleado</strong>

                            <p class="text-muted mt-1" id="txtEmpleado">
                            </p>

                            <hr>
                        </div>
                        <div class="col-md-4 offset-2">
                            <strong><i class="fas fa-calendar-alt mr-2"></i>Materia prima</strong>

                            <p class="text-muted mt-1" id="txtMateriaPrima">
                            </p>

                            <hr>
                        </div>

                    </div>
                    <div class="row mb-2">
                        <div class="col-md-4">
                            <strong><i class="fas fa-calendar-alt mr-2"></i>Fecha de entrega</strong>

                            <p class="text-muted mt-1" id="txtFechaEntrega">
                            </p>

                            <hr>
                        </div>
                        <div class="col-md-4 offset-2">
                            <strong><i class="fas fa-calendar-alt mr-2"></i>Cantidad KG entrega</strong>

                            <p class="text-muted mt-1" id="txtCantidadKgEntrega">
                            </p>

                            <hr>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-4">
                            <strong><i class="fas fa-user-tie mr-2"></i>Observaciones entrega</strong>

                            <p class="text-muted mt-1" id="txtObservacionesEntrega">
                            </p>

                            <hr>
                        </div>
                    </div>
                    <div class="text-center h5 bg-warning rounded py-2" id="mensajeDevolucion" style="display:none;">
                        Todavia no se ha registrado la devolución de esta entrega.
                    </div>
                    <div id="devolucion">
                        <h4>Devolución</h4>
                        <hr>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                <strong><i class="fas fa-people-arrows mr-2"></i>Cantidad KG devolución</strong>

                                <p class="text-muted mt-1" id="txtKgDevolucion">
                                </p>

                                <hr>
                            </div>
                            <div class="col-md-4 offset-2">
                                <strong><i class="fas fa-dollar-sign mr-2"></i>Fecha de devolución</strong>

                                <p class="text-muted mt-1" id="txtFechaDevolucion">
                                </p>

                                <hr>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                <strong><i class="fas fa-percentage mr-2"></i>Porcentaje de pérdida</strong>

                                <p class="text-muted mt-1" id="txtPorcentajePerdida">
                                </p>

                                <hr>
                            </div>
                            <div class="col-md-4 offset-2">
                                <strong><i class="fas fa-coins mr-2"></i>Osbervaciones devolución</strong>

                                <p class="text-muted mt-1 text-capitalize" id="txtObservacionesDevolucion">
                                </p>

                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    {{-- TABLA --}}
                    <table class="table table-bordered table-rounded" id="tablaEntregas">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Empleado</th>
                                <th scope="col">Materia prima</th>
                                <th scope="col">Cantidad entregada</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Porcentaje pérdida</th>
                                <th scope="col" style="width:15px">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <style>
        .porcentaje {
            font-size: 36px;
        }

        .contenedor-porcentaje {
            width: 100%;
            margin: 0em 1em;
            border-radius: 5px;
            text-align: center;
        }

        .boton:focus {
            outline: none !important;
            box-shadow: none !important;
        }

        .boton {
            box-shadow: none !important
        }

    </style>
@stop

@section('js')
    <script>
        $(document).ready(function() {
            //Esta variable la voy a usar cada vez que abro el modal para registrar una devolucion. Esta variable va a almacenar la Cantidad de kg
            //de la misma fila de donde se llamo el boton
            var cantidadKg;
            var porcentajePerdida;
            var idDescarozado;
            var fechaEntrega;
            const empleados = @json($empleados);
            if (empleados.length == 0) {
                $('#btnSeleccionar').attr('disabled', true);
                $('#formNuevaEntrega').attr('action', '');
            }
            $('#empleados').select2();

            $('body').tooltip({
                selector: '[data-toggle="tooltip"]'
            });

            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });

            $('[data-toggle="tooltip"]').on('click', function() {
                $(this).tooltip('hide')
            });

            $(document).on('click', '.boton', function(e) {
                $('[data-toggle="tooltip"]').tooltip('hide')

            });


            //Hacer focus en la barra de busqueda cuando se abre el select de bancos
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            $('#empleados').change(function(e) {
                var selected = $(this).find('option:selected');
                var dni = selected.data('dni');
                var cargo = selected.data('cargo');
                $('#dni').val(dni);
                $('#cargo').val(cargo);
            });

            $("#modalRegistrarDevolucion").on("show.bs.modal", function(event) {
                cantidadKg = $(event.relatedTarget).data('cantidad');
                idDescarozado = $(event.relatedTarget).data('id');
                fechaEntrega = $(event.relatedTarget).data('fecha');
                $('#helpId').text('Cantidad entregada: ' + cantidadKg + ' kgs');
            });

            $("#modalRegistrarDevolucion").on("hidden.bs.modal", function(event) {
                $('#cantidadDevolucion').val('');
                $('textarea').val('');
                $('.porcentaje').text('0.00' + '%');
                ($('#contenedorPorcentaje').hasClass('bg-danger') == true) ? $('#contenedorPorcentaje')
                    .removeClass('bg-danger').addClass('bg-success'): '';
                $('#resultForm').empty();
            });

            $("#cantidadDevolucion").keyup(function() {
                var resto = cantidadKg - $('#cantidadDevolucion').val();
                porcentajePerdida = (resto * 100) / cantidadKg;
                (porcentajePerdida < 0) ? porcentajePerdida = 0: porcentajePerdida;
                $('.porcentaje').text(porcentajePerdida.toFixed(2) + '%');
                if (porcentajePerdida > 25) {
                    $('#contenedorPorcentaje').removeClass('bg-success').addClass('bg-danger');
                } else {
                    ($('#contenedorPorcentaje').hasClass('bg-danger') == true) ? $('#contenedorPorcentaje')
                        .removeClass('bg-danger').addClass('bg-success'): '';
                }
            });

            //======================== MOSTRAR ENTREGA ===================================
            $(document).on('click', '.btnMostrarEntrega', function(e) {
                var idEntrega = $(this).data('id');

                $.get("/entregas-descarozado/mostrar/" + idEntrega,
                    function(data) {
                        const entrega = data[0];
                        $('#btnPDF').attr('href', '/imprimir-pdf/entrega-descarozado/' + entrega.id);
                        $('#txtEmpleado').text(entrega.nombreCompleto);
                        $('#txtMateriaPrima').text(entrega.fruta + ', ' + entrega.variedad);
                        $('#txtFechaEntrega').text(entrega.fecha);
                        $('#txtCantidadKgEntrega').text(entrega.cantidadKg + ' kgs');
                        $('#txtObservacionesEntrega').text((entrega.observaciones == null) ?
                            'Sin observaciones' : entrega.observaciones);

                        if (entrega.fechaDevolucion == null) {
                            $('#devolucion').hide();
                            $('#mensajeDevolucion').show();
                        } else {
                            $('#devolucion').show();
                            $('#mensajeDevolucion').hide();

                            $('#txtKgDevolucion').text(entrega.cantidadKgRetorno);
                            $('#txtFechaDevolucion').text(entrega.fechaDevolucion);
                            $('#txtPorcentajePerdida').text(entrega.porcentajePerdida);
                            $('#txtObservacionesDevolucion').text((entrega.observacionesDevolucion ==
                                null) ? 'Sin observaciones' : entrega.observacionesDevolucion);
                        }

                        $('#modalMostrarEntrega').modal('show');
                        // console.log('%cindex.blade.php line:354 entrega', 'color: #007acc;', entrega);
                    },
                    "json"
                );
            })

            //======================== ELIMINAR ENTREGA ===================================
            $(document).on('click', '.btnEliminarEntrega', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea dar de baja esta entrega?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post('/entregas-descarozado/eliminar/' + id, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    tablaEntregas.ajax.reload();
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: respuesta.data.success,
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                            tablaEntregas.ajax.reload();
                        }
                    }
                });
            });

            // ============================= EVENTO CLICK REGISTRAR DEVOLUCION =========================
            $('#btnRegistrarDevolucion').on('click', function() {
                if ($('#cantidadDevolucion').val() > cantidadKg) {
                    Swal.fire(
                        'Error',
                        'La cantidad a descartar no puede ser mayor que la cantidad de la entrega',
                        'error'
                    )
                } else if (fechaEntrega > $('#fechaDevolucion').val()) {
                    Swal.fire(
                        'Error',
                        'La fecha de devolución no puede ser menor que la fecha de entrega',
                        'error'
                    )
                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('devolucion-descarozado.create') }}",
                        data: $('#formDevolucion').serialize() + "&porcetajePerdida=" +
                            porcentajePerdida + "&id_entrega_descarozado=" + idDescarozado +
                            "&_token={{ csrf_token() }}",
                        dataType: "json",
                        success: function(data) {
                            if (data.error) {
                                html =
                                    '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                                for (var i = 0; i < data.error.length; i++) {
                                    html += '<li>' + data.error[i] + '</li>';
                                }
                                html += '</ul></div>';
                                $('#resultForm').html(html);
                            }
                            if (data.success) {
                                Swal.fire({
                                    toast: true,
                                    icon: 'success',
                                    title: data.success,
                                    position: 'top-right',
                                    showConfirmButton: false,
                                    timer: 2300,
                                });

                                $('#btnCerrarModal').trigger('click');
                                $('.modal').remove();
                                $('.modal-backdrop').remove();
                                $('body').removeClass("modal-open");

                                tablaEntregas.ajax.reload();
                            }
                        }
                    });
                }

            });



            //==================== DATATABLE ===========================
            tablaEntregas = $('#tablaEntregas').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: false,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('entrega-descarozado.datatable') }}",
                columns: [{
                        data: 'nombreCompleto'
                    },
                    {
                        data: 'materiaPrima'
                    },
                    {
                        //Cantidad entregada
                        data: 'cantidadKg'
                    },
                    {
                        data: 'fecha',
                    },
                    {
                        data: 'porcentajePerdida',

                    },
                    {
                        data: 'opciones'
                    },
                    {
                        data: 'created_at',
                        visible: false
                    },
                ],
                order: [6, 'desc']
            });

        });
    </script>
@stop
