@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="row d-flex justify-content-between align-items-center">
        <div class="col-md-6">
            <h1>Registrar entrega descarozado</h1>
            <h4 class="text-muted">Empleado: {{ $empleadoSeleccionado->nombreCompleto }}</h4>
        </div>
        <div class="col-md-3">
            <label for="fecha">Fecha:</label>
            <input type="date" name="fecha" class="form-control" id="fechaElegida" value="{{ now()->format('Y-m-d') }}"
                max="{{ now()->format('Y-m-d') }}" min="{{ now()->subDays(7)->format('Y-m-d') }}">
        </div>
    </div>
@stop

@section('content')
    <div class="row px-2">

        <div class="col-md-3">
            <div class="form-group">
                <label for="">Materias primas</label>
                <select class="form-control" name="materiasPrimas" id="materiasPrimas">
                    @foreach ($materiasPrimas as $materiaPrima)
                        <option value="{{ $materiaPrima->id }}"
                            {{ old('materia-prima') == $materiaPrima->id ? 'selected' : '' }}>
                            {{ $materiaPrima->fruta . ', ' . $materiaPrima->variedad }}</option>
                    @endforeach
                </select>
                <span class="invalid-feedback" role="alert" id="error-mp">
                    <strong>Error</strong>
                </span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Cantidad KG</label>
                <input type="number" name="" id="cantidad" class="form-control">
                <span class="invalid-feedback" role="alert" id="error-cantidad">
                    <strong>Error</strong>
                </span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Observaciones</label>
                <textarea class="form-control" name="" id="observaciones" rows="1"></textarea>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">ㅤ</label>
                <button class="btn btn-primary form-control" id="agregar">Agregar</button>
            </div>
        </div>
    </div>
    <div class="px-2">
        <hr>
    </div>
    <div class="row">
        <div class="col-12 px-4">
            <form id="formCompraMateriaPrima">
                <table class="table table-striped" id="tablaEntregas">
                    <thead>
                        <tr>
                            <th>Empleado</th>
                            <th>Materia Prima</th>
                            <th>Cantidad (KG)</th>
                            <th>Observaciones</th>
                            <th style="width:10px">Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class='text-center' id='vacio'>
                            <td colspan="5">Agregue una materia prima para su descarozado</td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <div class="row justify-content-between px-3">
                    <button class="btn btn-warning" id="finalizarEntrega">
                        Finalizar entrega
                    </button>
                </div>
            </form>
        </div>
    </div>
    {{-- <hr class="px-4"> --}}
@stop


@section('js')
    <script>
        $(document).ready(function() {
            const empleado = @json($empleadoSeleccionado);

            $('#materiasPrimas').select2();

            //Hacer focus en la barra de busqueda cuando se abre el select de bancos
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            //=========================Accion cliock del boton para agregar entrega ========================
            $('#agregar').click(function(e) {
                console.log($('#materiasPrimas').val());

                if ($('#materiasPrimas').val() == null) {
                    $('#error-mp').addClass('d-block');
                    $('#error-mp').children().text('Debe seleccionar una materia prima');
                    return
                }
                //Si el campo cantidad no esta vacio
                if ($('#cantidad').val()) {

                    $('#vacio').addClass('d-none');
                    //Borro los mensajes de error si es que existen
                    if ($('#error-cantidad').hasClass('d-block')) {
                        $('#error-cantidad').toggleClass('d-block');
                    };
                    if ($('#error-mp').hasClass('d-block')) {
                        $('#error-mp').toggleClass('d-block');
                    };

                    //Guardo todos los valores de los inputs anteriores en una respectiva variable para mantener
                    //el orden en el proceso siguiente
                    var nombreMatPrima = $('#materiasPrimas option:selected').text();
                    var idMatPrima = $('#materiasPrimas').val();
                    var cantidad = $('#cantidad').val();
                    var observaciones = $('#observaciones').val();

                    //Defino una variable y la lleno con codigo HTML, este codigo representa una fila en la tabla que ya
                    //tengo creada, asigno a cada campo de la fila un input de tipo hidden para poder enviar los datos
                    //en el formulario y asi posteriormente guardarlos.                    
                    //Se definen los nombres de los inputs con un [] para que se puedan enviar varios datos en un solo array
                    //y luego recorrerlos en el controlador

                    row = "<tr>" +
                        `<td>` + empleado.nombreCompleto + `</td>` +
                        //Campo materia prima en la fila, muestro el nombre pero en el input va el valor del id
                        `<td>` + nombreMatPrima + `<input type='hidden' name='materia-prima[]'value='` +
                        idMatPrima + `'></td>` +
                        //Campo cantidad en la fila
                        `<td>` + cantidad + `<input type='hidden' name='cantidad[]' value='` +
                        cantidad + `'></td>` +
                        //Campo precio unitario en la fila
                        `<td>` + observaciones + `<input type='hidden' name='observaciones[]' value='` +
                        observaciones + `'></td>` +

                        //Boton para eliminar una fila si es necesario
                        `<td id="DeleteButton"><a href="#" class="badge badge-danger" >Eliminar X</a></td>` +

                        `</tr>`;

                    //Guardo el cuerpo de la tabla en una variable
                    tableBody = $("table tbody");
                    //Aca agrego la fila que creamos anteriormente
                    tableBody.append(row);

                    //Vacio los inputs
                    $('#cantidad').val('');
                    $('#observaciones').val('');

                    $('#materiasPrimas').focus();

                } else {
                    $('#error-cantidad').addClass('d-block');
                    $('#error-cantidad').children().text('El campo cantidad no puede estar vacio');
                }

            });

            //======================= EVENTO CLICK BOTON FINALIZAR ENTREGA ====================
            $('#finalizarEntrega').click(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{{ route('entrega-descarozado.store') }}",
                    data: $('#formCompraMateriaPrima').serialize() +
                        "&_token={{ csrf_token() }}" + "&idEmpleado=" + empleado.id + "&fecha=" +
                        $('#fechaElegida').val(),
                    dataType: "JSON",
                    success: function(data) {
                        if (data.error) {
                            console.log(data.error)
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: data.error,
                            })
                        }
                        if (data.success) {
                            //Muestro una alerta diciendo que la operacion se completo con exito
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: data.success +
                                    '. Espere unos instantes',
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2000,
                            }).then(function() {
                                window.location.href =
                                    "{{ route('entrega-descarozado.index') }}";
                            });
                        }
                    }
                });
            });
            //Cuando se da click en el boton eliminar fila
            $("#tablaEntregas").on("click", "#DeleteButton", function() {
                $(this).closest("tr").remove();
            });
        });
    </script>
@stop
