@extends('adminlte::page')

@section('title', 'Historial de compras')

@section('plugins.Sweetalert2', true)

@section('css')
    <style>
        i {
            cursor: pointer;
        }

    </style>


@stop

@section('content_header')
    <div class="d-flex justify-content-between px-1">
        <h1>Historial de compras</h1>
        <a href="{{ route('purchase.index') }}" class="btn btn-success bg-hfrut"> <i class="fa fa-shopping-cart mr-2"
                aria-hidden="true"></i> Comprar</a>
    </div>
    {{-- <h1>{{ Session::get('from') }}</h1> --}}
@stop

@section('content')
    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                window.location.hash = '#';
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: '{{ session('status.type') }}',
                        title: '{{ session('status.message') }}',
                        text: '',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    })
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    <div id='app'>
        <div class="modal fade" id="modalFiltro" tabindex="-1" role="dialog" aria-labelledby="modalFiltro"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">Filtros</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span id="formResult"></span>
                        <div class="row">
                            <div class="col-md-12 d-flex">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fechaInicio">Fecha de Inicio <button
                                                class="fa fa-question-circle btn p-0 m-0" data-toggle="tooltip"
                                                data-placement="bottom"
                                                title="Si no se elige una fecha de inicio, no se filtrará por fecha. Solo por proveedor"></button></label>
                                        <input type="date" name="fechaInicio" class="form-control" id="fechaInicio"
                                            required max='2099-12-31' min='1900-01-01'>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fechaInicio">Fecha de Fin</label>
                                        <input type="date" name="fechaFin" class="form-control"
                                            value="{{ Carbon\Carbon::today()->format('Y-m-d') }}" id="fechaFin"
                                            max='2099-12-31' min='1900-01-01' required>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="form-check ">
                                    {{-- <input class="form-check-input" type="checkbox" value="" id="chbProveedores"> --}}
                                    {{-- <label class="form-check-label" for="flexCheckDefault">
                                         <strong> Proveedor</strong>
                                     </label> --}}
                                </div>
                                <div class="form-group mx-3">
                                    <label for="proveedores" class="col-form-label">Proveedor</label>
                                    <select name="proveedores" id="proveedores" class="form-control">

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id='matPrimaOinsServicio'>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-primary" data-tipo="" id="btnFiltrar">Buscar</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="session" {{ Session::get('from') ? 'data-session="si"' : '' }}>
            <ul class="nav nav-tabs mr-1" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link {{ Session::get('from') ? '' : 'active' }}" id="materias-primas-tab"
                        data-toggle="tab" href="#materias-primas" role="tab" aria-controls="materias-primas"
                        {{ Session::get('from') ? '' : 'aria-selected="true"' }}>Materias
                        Primas</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link {{ Session::get('from') == 'IS' ? 'active' : '' }}" id="insumos-tab"
                        data-toggle="tab" href="#insumos" role="tab" aria-controls="insumos-servicios"
                        {{ Session::get('from') ? 'aria-selected="true"' : '' }}>Insumos</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link {{ Session::get('from') == 'SV' ? 'active' : '' }}" id="servicios-tab"
                        data-toggle="tab" href="#servicios" role="tab" aria-controls="servicios"
                        {{ Session::get('from') ? 'aria-selected="true"' : '' }}>Servicios</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link {{ Session::get('from') == 'ID' ? 'active' : '' }}" id="indumentarias-tab"
                        data-toggle="tab" href="#indumentarias" role="tab" aria-controls="indumentarias"
                        {{ Session::get('from') ? 'aria-selected="true"' : '' }}>Indumentarias</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade {{ Session::get('from') ? '' : 'show active' }}" id="materias-primas"
                    role="tabpanel" aria-labelledby="materias-primas-tab">

                    <div class="container-fluid py-3 tab-cont">
                        <div class="row justify-content-between ">
                            <div class="col-md-2 align-self-end pb-3">
                                <span class="badge badge-secondary p-2 d-none" id="badgeMP">
                                    <i class="fas fa-times pl-3"></i>
                                </span>
                            </div>
                            <div class="col-md-2 align-self-end pb-3">
                                <button type="button" class="btn btn-primary btn-block " data-toggle="modal"
                                    data-target="#modalFiltro" data-tipo='MP'>Filtros
                                </button>
                            </div>
                        </div>
                        <table class='table table-bordered' id='comprasMP'>
                            <thead class="bg-hfrut">
                                <th scope="col">Fecha</th>
                                <th scope="col">Proveedor</th>
                                <th scope="col">IVA</th>
                                <th scope="col">Total</th>
                                <th scope="col" class="text-center" style="width:120px">Opciones</th>
                            </thead>

                        </table>
                    </div>
                </div>
                <div class="tab-pane fade {{ Session::get('from') == 'IS' ? 'show active' : '' }}" id="insumos"
                    role="tabpanel" aria-labelledby="insumos">
                    <div class="container-fluid py-3 tab-cont">
                        <div class="row justify-content-between">
                            <div class="col-md-2 align-self-end pb-3">
                                <span class="badge badge-secondary p-2 d-none" id="badgeIS">
                                    <i class="fas fa-times pl-3"></i>
                                </span>
                            </div>
                            <div class="col-md-2 align-self-end pb-3">
                                <button type="button" class="btn btn-primary btn-block " data-toggle="modal"
                                    data-target="#modalFiltro" data-tipo="IS">Filtros
                                </button>
                            </div>
                        </div>
                        <table class='table table-bordered' id="tablaInsumos">
                            <thead class="bg-hfrut">
                                <tr>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Proveedor</th>
                                    <th scope="col">IVA</th>
                                    <th scope="col">Total</th>
                                    <th scope="col" class="text-center" style="width:120px">Opciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade {{ Session::get('from') == 'SV' ? 'show active' : '' }}" id="servicios"
                    role="tabpanel" aria-labelledby="servicios-tab">
                    <div class="container-fluid py-3 tab-cont">
                        <div class="row justify-content-between">
                            <div class="col-md-2 align-self-end pb-3">
                                <span class="badge badge-secondary p-2 d-none" id="badgeSV">
                                    <i class="fas fa-times pl-3"></i>
                                </span>
                            </div>
                            <div class="col-md-2 align-self-end pb-3">
                                <button type="button" class="btn btn-primary btn-block " data-toggle="modal"
                                    data-target="#modalFiltro" data-tipo="SV">Filtros
                                </button>
                            </div>
                        </div>
                        <table class='table table-bordered' id="tablaServicios">
                            <thead class="bg-hfrut">
                                <tr>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Proveedor</th>
                                    <th scope="col">IVA</th>
                                    <th scope="col">Total</th>
                                    <th scope="col" class="text-center" style="width:120px">Opciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade {{ Session::get('from') == 'ID' ? 'show active' : '' }}" id="indumentarias"
                    role="tabpanel" aria-labelledby="indumentarias-tab">
                    <div class="container-fluid py-3 tab-cont">
                        <div class="row justify-content-between">
                            <div class="col-md-2 align-self-end pb-3">
                                <span class="badge badge-secondary p-2 d-none" id="badgeID">
                                    <i class="fas fa-times pl-3"></i>
                                </span>
                            </div>
                            <div class="col-md-2 align-self-end pb-3">
                                <button type="button" class="btn btn-primary btn-block " data-toggle="modal"
                                    data-target="#modalFiltro" data-tipo='ID'>Filtros
                                </button>
                            </div>
                        </div>
                        <table class='table table-bordered' id="tablaIndumentarias">
                            <thead class="bg-hfrut">
                                <tr>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Proveedor</th>
                                    <th scope="col">IVA</th>
                                    <th scope="col">Total</th>
                                    <th scope="col" class="text-center" style="width:120px">Opciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop



@section('js')
    <script>
        $(document).ready(function() {
            //Action es la ruta a donde voy a mandar la peticion ajax
            var action = ''
            var tipoCompra = '';
            $('[data-toggle="tooltip"]').tooltip()

            $('#proveedores').select2({
                dropdownParent: $("#modalFiltro"),
            });

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            var tablaComprasMP = $('#comprasMP').DataTable({
                retrieve: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                bFilter: true,
                bLengthChange: true,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('purchase-history.mp') }}",
                columns: [{
                        data: 'fecha',
                    },
                    {
                        data: 'proveedor',
                    },
                    {
                        data: 'iva'
                    },
                    {
                        data: 'total'
                    },
                    {
                        data: 'opciones',
                        className: 'text-center'
                    },
                    {
                        data: 'created_at',
                        visible: false
                    }
                ],
                order: [5, 'desc']
            });


            var tablaInsumos = $('#tablaInsumos').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                bFilter: true,
                bLengthChange: true,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('purchase-history.insumos') }}",
                columns: [{
                        data: 'fecha',
                    },
                    {
                        data: 'proveedor',
                    },
                    {
                        data: 'iva',
                    },
                    {
                        data: 'total',
                    },
                    {
                        data: 'opciones',
                        className: 'text-center'
                    },
                    {
                        data: 'created_at',
                        visible: false
                    }
                ],
                order: [5, 'desc']
            });

            var tablaServicios = $('#tablaServicios').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                bFilter: true,
                bLengthChange: true,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('purchase-history.servicios') }}",
                columns: [{
                        data: 'fecha',
                    },
                    {
                        data: 'proveedor',
                    },
                    {
                        data: 'iva',
                    },
                    {
                        data: 'total',
                    },
                    {
                        data: 'opciones',
                        className: 'text-center'
                    },
                    {
                        data: 'created_at',
                        visible: false
                    }
                ],
                order: [5, 'desc']
            });

            var tablaIndumentarias = $('#tablaIndumentarias').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                bFilter: true,
                bLengthChange: true,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: "{{ route('purchase-history.indumentarias') }}",
                columns: [{
                        data: 'fecha',
                    },
                    {
                        data: 'proveedor',
                    },
                    {
                        data: 'iva',
                    },
                    {
                        data: 'total',
                    },
                    {
                        data: 'opciones',
                        className: 'text-center'
                    },
                    {
                        data: 'created_at',
                        visible: false
                    }
                ],
                order: [5, 'desc']
            });

            //=================== CANCELAR COMPRA =========================
            $(document).on('click', '.btnEliminar', function() {
                var id = $(this).data('id');
                var tipoCompra = $(this).data('tipo');
                console.log(id);
                Swal.fire({
                    title: '¿Está seguro de que desea cancelar esta compra?',
                    text: '',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, estoy seguro',
                    cancelButtonText: 'No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "post",
                            url: "{{ route('cancelar.compra') }}",
                            data: {
                                id: id,
                                tipoCompra: tipoCompra,
                                _token: "{{ csrf_token() }}",
                            },
                            dataType: "json",
                            success: function(response) {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 2400,
                                    timerProgressBar: true,
                                })
                                if (response.success) {
                                    switch (tipoCompra) {
                                        case 'MP':
                                            tablaComprasMP.ajax.reload();
                                            break;
                                        case 'IS':
                                            tablaInsumos.ajax.reload();
                                            break;
                                        case 'SV':
                                            tablaServicios.ajax.reload();
                                            break;
                                        case 'ID':
                                            tablaIndumentarias.ajax.reload();
                                            break;

                                        default:
                                            break;
                                    }
                                    Toast.fire({
                                        icon: 'success',
                                        title: response.success
                                    })
                                } else if (response.error) {
                                    Toast.fire({
                                        icon: 'error',
                                        title: response.error
                                    })
                                }
                            }
                        });
                    }
                })
            });


            //======================= CLICK CHECKBOX PROVEEDORES DENTRO DEL MODAL ==========================
            //  $('#chbProveedores').click(function(e) {
            //      var url_proveedores = '';
            //      //Si no esta chequeado deshabilito el select de proveedores y lo limpio. Si el click es para chequear habilito el select
            //      if ($(this).prop('checked') == true) {
            //          $('#proveedores').prop('disabled', false);
            //          $('#proveedores').empty();
            //      } else {
            //          $('#proveedores').prop('disabled', true);

            //      }
            //      //Verifico si lo que se esta filtrando es materia prima (representado con el valor = 0 [cero])
            //      //o insumo o servicio (representado con el valor = 1 [uno])
            //      //y a partir de eso lleno el select con los proveedores correspondientes
            //      if ($('#matPrimaOinsServicio').val() == 0) {
            //          url_proveedores = '/materias-primas/proveedores-mp'
            //      } else if ($('#matPrimaOinsServicio').val() == 1) {
            //          url_proveedores = '/insumos-servicios/proveedores-is'
            //      }

            //      //Hago la peticion y lleno el select
            //      axios.get(url_proveedores).then((r) => {
            //          var proveedores = r.data;
            //          for (var i = 0; i < proveedores.length; i++) {
            //              $('#proveedores').append('<option value=' + proveedores[i].id +
            //                  ' data-cuit=' + proveedores[i].cuit + ' data-telefono=' +
            //                  proveedores[i].telefono + ' data-direccion=' +
            //                  encodeURIComponent(
            //                      proveedores[i]
            //                      .direccion) + '>' +
            //                  proveedores[i].nombreCompleto +
            //                  '</option>');
            //          }
            //          var idProv = $('#proveedores').val();
            //      });
            //  });
            //==============EVENTO CUANDO SE ESCONDE EL MODAL ==========================
            $("#modalFiltro").on("hide.bs.modal", function(event) {
                //Borro si hay algun error
                $('#formResult').children().remove();
                $('#proveedores').empty();
                $('#fechaInicio').empty();
            });

            $("#modalFiltro").on("show.bs.modal", function(e) {
                var boton = $(e.relatedTarget);
                tipoCompraProveedor = boton.data('tipo');
                $('#btnFiltrar').data('tipo', tipoCompraProveedor);
                //cargar proveedores

                axios.get('/proveedores-tipo/' + tipoCompraProveedor).then((r) => {
                    const proveedores = r.data;
                    if (proveedores.length != 0) {
                        $('#sinProveedores').addClass('d-none');
                        $('#proveedores').append(
                            '<option value="" selected>Seleccione un proveedor</option>');

                        for (var i = 0; i < proveedores.length; i++) {
                            $('#proveedores').append('<option value=' + proveedores[i].id +
                                ' data-cuit=' + proveedores[i].cuit + ' data-telefono=' +
                                proveedores[i].telefono + ' data-direccion=' +
                                encodeURIComponent(
                                    proveedores[i]
                                    .direccion) + ' data-tipoproveedor=' + tipoCompraProveedor +
                                '>' +
                                proveedores[i].nombreCompleto +
                                '</option>');
                        }
                        $('#proveedores').select2('enable');
                    } else {
                        $('#proveedores').append(
                            '<option value="" selected>No hay proveedores para este tipo de compra</option>'
                        );
                        $('#proveedores').select2('enable', false);
                    }
                });
            });
            //  $("#modalFiltro").on("hide.bs.modal", function(e) {
            //      var boton = e.relatedTarget;
            //      tipoCompra = boton.dataset.tipo;
            //      console.log(tipoCompra);
            //  });

            //==================== CERRAR MODAL ================================
            //Funcion para cerrar modal y no tener que repetir codigo
            function cerrarModal() {
                $('#modalFiltro').trigger('click');
            }

            //==================== EVENTO CLICK PARA FILTRAR ============================
            $('#btnFiltrar').click(function(e) {
                e.preventDefault();

                var tipoCompraAFiltrar = $(this).data('tipo');
                $.ajax({
                    type: "POST",
                    url: "{{ route('historial-compras.filtrar') }}",
                    data: {
                        tipoCompra: $(this).data('tipo'),
                        fechaInicio: $('#fechaInicio').val(),
                        fechaFin: $('#fechaFin').val(),
                        proveedor: $('#proveedores').val(),
                        _token: "{{ csrf_token() }}",
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.error) {
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#formResult').html(html);
                        } else {
                            var nombreBadge = '#badge' + tipoCompraAFiltrar;
                            switch (tipoCompraAFiltrar) {
                                case 'MP':
                                    tablaComprasMP.clear().rows.add(data.data).draw();
                                    break;
                                case 'IS':
                                    tablaInsumos.clear().rows.add(data.data).draw();
                                    break;
                                case 'SV':
                                    tablaServicios.clear().rows.add(data.data).draw();
                                    break;
                                case 'ID':
                                    tablaIndumentarias.clear().rows.add(data.data).draw();
                                    break;
                                default:
                                    break;
                            }
                            $(nombreBadge).removeClass('d-none');
                            var fechaInicio = new Date($('#fechaInicio').val())
                            var fechaFin = new Date($('#fechaFin').val())
                            var nombreProveedor = $('#proveedores :selected').text();
                            if ($('#proveedores').val() == "") {
                                $(nombreBadge).html('Filtrando entre ' + fecha.format(
                                        fechaInicio) +
                                    ' y ' + fecha.format(
                                        fechaFin) +
                                    '<i class="fas fa-times pl-3"></i>');
                            } else if ($('#fechaInicio').val() == "") {
                                $(nombreBadge).html('Filtrando por proveedor: ' +
                                    nombreProveedor + '<i class="fas fa-times pl-3"></i>');
                            } else {
                                $(nombreBadge).html('Filtrando entre ' + fecha.format(
                                        fechaInicio) + ' y ' +
                                    fecha.format(
                                        fechaFin) +
                                    '. Proveedor ' +
                                    nombreProveedor + '<i class="fas fa-times pl-3"></i>');
                            }
                            cerrarModal();
                        }
                    }
                });


                //====================== EVENTO CLICK EN LA X DEL TAG ==============================
                //Cuando hago click en la X del filtro me vuelve a mostrar todos los registros nuevamente
                $('#badgeMP').on('click', 'i', function(e) {
                    tablaComprasMP.ajax.reload();
                    $('#badgeMP').addClass('d-none');
                });

                $('#badgeIS').on('click', 'i', function(e) {
                    tablaInsumos.ajax.reload();
                    $('#badgeIS').addClass('d-none');
                });
                $('#badgeSV').on('click', 'i', function(e) {
                    tablaServicios.ajax.reload();
                    $('#badgeSV').addClass('d-none');
                });

                $('#badgeID').on('click', 'i', function(e) {
                    tablaIndumentarias.ajax.reload();
                    $('#badgeID').addClass('d-none');
                });

                var fecha = new Intl.DateTimeFormat("es-AR");
            });
        })
    </script>
@stop
