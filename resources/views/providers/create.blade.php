@extends('adminlte::page')

@section('title', 'Nuevo Proveedor')

@section('plugins.Select2', true)

@section('css')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
@stop


@section('content_header')
    <p class="pl-4 h2">Agregar nuevo proveedor</p>
    @php
    $valoresViejos = old();
    @endphp
@stop

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10">

                <div class="card card-primary border-top">
                    {!! Form::open(['route' => 'providers.store', 'class' => 'p-4']) !!}
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="radio" name="personaEmpresa" id="chbPersona"
                                    value="persona" checked>
                                <label class="form-check-label" for="persona">
                                    <b>Persona</b>
                                </label>
                            </div>
                            <hr class="m-0">
                            <div class="row mt-2">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('nombre', 'Nombre (*)') !!}
                                        {!! Form::text('nombre', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Nombre...', 'id' => 'nombre']) !!}

                                        @error('nombre')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('apellido', 'Apellido (*)') !!}
                                        {!! Form::text('apellido', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Apellido...', 'id' => 'apellido']) !!}

                                        @error('apellido')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-4">
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="radio" name="personaEmpresa" id="chbEmpresa"
                                    value="empresa">
                                <label class="form-check-label" for="empresa">
                                    <b>Empresa</b>
                                </label>
                            </div>
                            <hr class="m-0">
                            <div class="form-group col-md-6 pl-0 mt-2">
                                {!! Form::label('razonSocial', 'Razón Social (*)', ['class' => 'text-muted']) !!}
                                {!! Form::text('razonSocial', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Razón social...', 'disabled', 'id' => 'razonSocial']) !!}

                                @error('razonSocial')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('cuit', 'CUIT / CUIL') !!}
                                {!! Form::number('cuit', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'CUIT...']) !!}
                                @error('cuit')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('dni', 'DNI') !!}
                                {!! Form::number('dni', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'DNI...']) !!}
                                @error('dni')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('telefono', 'Teléfono') !!}
                                {!! Form::number('telefono', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Teléfono...']) !!}
                                @error('telefono')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('email', 'Email ') !!}
                                {!! Form::text('email', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Email...']) !!}
                                @error('email')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('cbu', 'CBU') !!}
                                {!! Form::number('cbu', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'CBU...']) !!}
                                @error('cbu')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('direccion', 'Dirección') !!}
                                {!! Form::text('direccion', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Dirección...']) !!}
                                @error('direccion')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('provincia', 'Provincia (*)') !!}
                                {!! Form::select('provincia', $provincias->pluck('provincia', 'id'), old('provincia'), ['id' => 'provincias', 'class' => 'form-control', 'placeholder' => 'Seleccione la provincia']) !!}
                                @error('provincia')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('idLocalidad', 'Localidad (*)') !!}
                                {!! Form::select('idLocalidad', [], null, ['id' => 'idLocalidad', 'class' => 'form-control', 'placeholder' => 'Seleccione la provincia para cargar las localidades']) !!}
                                @error('idLocalidad')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('tipoProveedor[]', 'Tipo de proveedor (*)') !!}
                                <div class="d-block">
                                    <div class="form-check form-check-inline mr-5">
                                        <input class="form-check-input" name="tipoProveedor[]" type="checkbox" id="MP"
                                            value="MP">
                                        <label class="form-check-label" for="inlineCheckbox1">Materia Prima</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-5">
                                        <input class="form-check-input" name="tipoProveedor[]" type="checkbox" id="IS"
                                            value="IS">
                                        <label class="form-check-label" for="inlineCheckbox2">Insumos</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-5">
                                        <input class="form-check-input" name="tipoProveedor[]" type="checkbox" id="SV"
                                            value="SV">
                                        <label class="form-check-label" for="inlineCheckbox3">Servicios</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-5">
                                        <input class="form-check-input" name="tipoProveedor[]" type="checkbox" id="ID"
                                            value="ID">
                                        <label class="form-check-label" for="inlineCheckbox4">Indumentarias</label>
                                    </div>
                                </div>

                                @error('tipoProveedor')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror


                                {{-- {!! Form::label('tipoProveedor', 'Tipo de proveedor (*)') !!}
                                {!! Form::text('tipoProveedor', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Tipo de proveedor...']) !!}
                                @error('tipoProveedor')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror --}}
                            </div>
                        </div>
                    </div>


                    <div class="row mt-3">
                        <div class="col-md-12">
                            <a href="{{ route('providers.index') }}" class="btn btn-danger mr-2">Cancelar</a>
                            {!! Form::submit('Agregar proveedor', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>

                </div>
                {!! Form::close() !!}


            </div>
        </div>
    </div>
    </div>

@endsection

@section('js')


    <script>
        $(document).ready(function() {

            const valoresViejos = {!! json_encode($valoresViejos) !!};
            const errores = {!! json_encode($errors->toArray()) !!};

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            if (valoresViejos.personaEmpresa == 'empresa') {
                habilitarEmpresa();
            } else {
                habilitarPersona();
            }

            if (valoresViejos.length != 0) {

                //Traigo nuevamente todas las localidades de la provincia que estaba seleccioanda antes de que se enviara el formulario y fallara
                axios.post(`/localidades/${valoresViejos.provincia}`).then((r) => {
                    var localidades = r.data;
                    for (var i = 0; i < localidades.length; i++) {
                        //Si el id de la localidad en la posicion i conince con el valor de la localidad seleccionada anteriormente le asigno el atributo selected
                        var seleccionada = localidades[i].id == valoresViejos.idLocalidad ? 'selected' : '';
                        $('#idLocalidad').append('<option value="' + localidades[i].id + '" ' +
                            seleccionada + ' >' +
                            localidades[i].localidad +
                            '</option>');
                    }

                });
                //Pregunto si la variable tipoProveedor existe
                if (valoresViejos.tipoProveedor) {
                    //Recorro con un foreach
                    valoresViejos.tipoProveedor.forEach((tipo) => {
                        //Los vuelvo a chequear
                        $('#' + tipo).attr('checked', true);
                    });
                }
            }

            $('#provincias').select2();
            $('#idLocalidad').select2();

            $('#provincias').change(function() {
                $('#idLocalidad').empty();
                var id_provincia = $('#provincias').val();
                axios.post(`/localidades/${id_provincia}`).then((r) => {
                    var localidades = r.data;
                    for (var i = 0; i < localidades.length; i++) {
                        $('#idLocalidad').append('<option value=' + localidades[i].id + '>' +
                            localidades[i].localidad +
                            '</option>');
                    }
                    if (id_provincia == 14) {
                        $('#idLocalidad').val(1443);
                    }
                });
            });


            $('#chbPersona').change(function(e) {
                e.preventDefault();
                if ($(this).prop('checked')) {
                    habilitarPersona();
                }
            });

            $('#chbEmpresa').change(function(e) {
                e.preventDefault();
                if ($(this).prop('checked')) {
                    habilitarEmpresa();
                }
            });

            function habilitarPersona() {
                $('#chbPersona').attr('checked', true);
                $('#chbEmpresa').attr('checked', false);
                $('#nombre').attr('disabled', false);
                $('#nombre').prev().removeClass('text-muted');
                $('#apellido').attr('disabled', false);
                $('#apellido').prev().removeClass('text-muted');
                $('#razonSocial').attr('disabled', true);
                $('#razonSocial').prev().addClass('text-muted');
            }

            function habilitarEmpresa() {
                $('#chbPersona').attr('checked', false);
                $('#chbEmpresa').attr('checked', true);
                $('#razonSocial').attr('disabled', false);
                $('#razonSocial').prev().removeClass('text-muted');
                $('#nombre').attr('disabled', true);
                $('#nombre').prev().addClass('text-muted');
                $('#apellido').attr('disabled', true);
                $('#apellido').prev().addClass('text-muted');
            }

        });
    </script>


    {{-- <script>
        Swal.fire(
            'Good job!',
            'You clicked the button!',
            'success'
        )
    </script> --}}
@stop
