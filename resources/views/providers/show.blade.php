@extends('adminlte::page')

@section('title', 'Proveedor')

@section('content_header')
    {{-- <p class="pl-4 h2">Clientes</p> --}}
@stop

@section('content')

    <div class="col-md-9 pt-3">
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title"><strong>Proveedor:</strong> {{ $provider->nombreCompleto }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row mb-2">
                    <div class="col-md-4">
                        <strong><i class="fas fa-balance-scale mr-2"></i>CUIT</strong>

                        <p class="text-muted mt-1">
                            {{ $provider->cuit == null ? 'No especificado' : $provider->cuit }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-4  offset-2">
                        <strong><i class="fas fa-id-card mr-2"></i>DNI</strong>

                        <p class="text-muted mt-1">
                            {{ $provider->dni == null ? 'No especificado' : $provider->dni }}
                        </p>

                        <hr>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-4">
                        <strong><i class="fas fa-map-marked-alt mr-2"></i>Dirección</strong>

                        <p class="text-muted mt-1">
                            {{ $provider->direccion == null ? 'No especificada' : $provider->direccion }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-4 offset-2">
                        <strong><i class="fas fa-phone-alt mr-2"></i>Teléfono</strong>

                        <p class="text-muted mt-1">
                            {{ $provider->telefono == null ? 'No especificado' : $provider->telefono }}
                        </p>

                        <hr>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-4">
                        <strong><i class="fas fa-map-marker-alt mr-2"></i> Provincia y Localidad</strong>

                        <p class="text-muted mt-1">
                            {{ $provinciaLocalidad->first()->provincia . ', ' . $provinciaLocalidad->first()->localidad }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-4 offset-2">
                        <strong><i class="fas fa-truck-moving mr-2"></i>Tipo de proveedor</strong>

                        <p class="text-muted mt-1">
                            @php
                                $arrayTipos = explode('-', $provider->tipoProveedor);
                                $tipos = '';
                                foreach ($arrayTipos as $key => $value) {
                                    switch ($value) {
                                        case 'MP':
                                            $tipos .= ' Materia prima';
                                            break;
                                        case 'IS':
                                            $tipos .= ' Insumos';
                                            break;
                                        case 'SV':
                                            $tipos .= ' Servicios';
                                            break;
                                        case 'ID':
                                            $tipos .= ' Indumentarias';
                                            break;
                                        default:
                                            break;
                                    }
                                    $key == 0 ? ($tipos .= ' - ') : ($tipos .= '');
                                }
                                echo $tipos;
                            @endphp
                        </p>

                        <hr>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-4">
                        <strong><i class="fas fa-envelope mr-2"></i>Email</strong>

                        <p class="text-muted mt-1">
                            {{ $provider->email == null ? 'No especificado' : $provider->email }}
                        </p>

                        <hr>
                    </div>
                    <div class="col-md-4 offset-2">
                        <strong id="" class="cbu"> <i class="fas fa-dollar-sign mr-2"></i>CBU</strong>
                        {{-- <div class="d-flex"> --}}
                        <p class="text-muted mt-1 d-flex" id='cbu'>
                            {{ $provider->cbu == null ? 'No especificado' : $provider->cbu }}
                            @if (!$provider->cbu == null)
                                <a class="btn btn-outline-dark btn-sm ml-3" id="copiar" data-toggle="tooltip"
                                    data-placement="top" title="Copiar"> <i class="fas fa-copy"></i> </a>
                            @endif
                        </p>
                        <hr>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ route('providers.index') }}" class="btn btn-danger mr-2">Volver</a>
                    </div>
                </div>

                {{-- <strong><i class="far fa-file-alt mr-1"></i> Notas</strong>

            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.
            </p> --}}
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
    <script>
        $(document).ready(function() {
            $('#copiar').click(function(e) {
                e.preventDefault();
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val($('#cbu').text()).select();
                document.execCommand("copy");
                $temp.remove();
            });

            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

@stop
