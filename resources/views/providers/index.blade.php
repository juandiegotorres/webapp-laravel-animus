@extends('adminlte::page')

@section('plugins.Sweetalert2', true)

@section('title', 'Proveedores')

@section('content_header')
    @can('providers.create')
        <a href="{{ route('providers.create') }}" class="float-right pr-2">
            <button class="btn btn-success bg-hfrut">
                Agregar nuevo proveedor
            </button>
        </a>
    @endcan
    <p class="pl-2 h2">{{ $title }}</p>
@stop

@section('content')
    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: 'success',
                        title: '{{ session('status') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">

                    <table class="table table-bordered table-rounded" id="proveedores">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">Nombre</th>
                                <th scope="col">CUIT / CUIL</th>
                                <th scope="col">Telefono</th>
                                <th scope="col">Email</th>
                                <th scope="col">Tipo de Proveedor</th>
                                {{-- <th scope="col">Localidad</th>
                        <th scope="col">Cuil</th> --}}
                                <th scope="col" style="width:15px">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($providers as $provider)
                                <tr>
                                    <td>{{ $provider->nombreCompleto }}</td>
                                    <td>{{ $provider->cuit }}</td>
                                    <td>{{ $provider->telefono }}</td>
                                    <td>{{ $provider->email }}</td>
                                    <td>
                                        @php
                                            //Separo el string delimitado por los guiones y lo convierto en un array
                                            $tiposProveedor = explode('-', $provider->tipoProveedor);
                                            //Elimina el ultimo registro del array. Que en este caso es un espacio en blanco
                                            array_pop($tiposProveedor);
                                            
                                            $stringTipos = '';
                                            
                                            foreach ($tiposProveedor as $key => $value) {
                                                switch ($value) {
                                                    case 'MP':
                                                        $key == 0 ? ($stringTipos .= 'Materia prima') : ($stringTipos .= ' - Materia prima');
                                                        break;
                                                    case 'IS':
                                                        $key == 0 ? ($stringTipos .= 'Insumos') : ($stringTipos .= ' - Insumos');
                                                        break;
                                                    case 'SV':
                                                        $key == 0 ? ($stringTipos .= 'Servicios') : ($stringTipos .= ' - Servicios');
                                                        break;
                                                    case 'ID':
                                                        $key == 0 ? ($stringTipos .= 'Indumentarias') : ($stringTipos .= ' - Indumentarias');
                                                        break;
                                                }
                                            }
                                            
                                            echo $stringTipos;
                                            
                                        @endphp
                                    </td>
                                    <td>
                                        <div class="d-flex justify-content-end">
                                            @can('providers.show')
                                                <a href="{{ route('providers.show', ['provider' => $provider->id]) }}">
                                                    <button class="btn btn-success btn-sm mr-2">
                                                        <i class="fa fa-info-circle"></i>
                                                    </button>
                                                </a>
                                            @endcan
                                            @can('providers.edit')
                                                <a href="{{ route('providers.edit', ['provider' => $provider->id]) }}">
                                                    <button class="btn btn-primary btn-sm mr-2">
                                                        <i class="fa fa-pen"></i>
                                                    </button>
                                                </a>
                                            @endcan
                                            @can('providers.delete')
                                                <dar-baja id="{{ $provider->id }}" ruta="proveedores" entidad="proveedor">
                                                </dar-baja>
                                            @endcan

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script>
        $(document).ready(function() {

            $('#exampleModal').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('New message to ' + recipient)
                modal.find('.modal-body input').val(recipient)
            })

            $('#proveedores').DataTable({
                responsive: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
            });
        });
    </script>

    {{-- <script>
    Swal.fire(
        'Good job!',
        'You clicked the button!',
        'success'
    )
</script> --}}
@stop
