@extends('adminlte::page')

@section('title', 'Usuarios')



@section('content_header')
    <div class="float-right pr-2" id="agregarUsuario">

    </div>
    <h1>Usuarios</h1>
@stop

@section('content')
    <div id="app">
        {{-- MODAL PASSWORD --}}
        <div class="modal fade" id="modalPassword" tabindex="-1" role="dialog" aria-labelledby="modalPasswordLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalPasswordLabel">Introduzca su contraseña</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="POST" id='formPassword'>
                        @csrf
                        <div class="modal-body">
                            <div class="alert alert-danger pb-0 d-none" role="alert" id="passIncorrecta">
                                <ul>
                                    <li>Contraseña incorrecta</li>
                                </ul>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="passw" class="col-form-label">Contraseña</label>
                                        <div class="input-group">
                                            <input type="password" name="passw" class="form-control" id="passw"
                                                placeholder="Contraseña..." required>
                                            <div class="input-group-append" id="show_hide_password">
                                                <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary">Aceptar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- MODAL CREAR USUARIO --}}
        <div class="modal fade" id="crearUsuarioModal" tabindex="-1" role="dialog" aria-labelledby="modalPasswordLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">Crear nuevo usuario</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="POST" id='formCrearUsuario'>
                        @csrf
                        <div class="modal-body">
                            <span id="resultadoCrear"></span>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name" class="col-form-label">Nombre de usuario</label>
                                        <input type="text" name="name" class="form-control" placeholder="Nombre..."
                                            required id="nombreUsuario" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="passw" class="col-form-label">Contraseña</label>
                                        <div class="input-group">
                                            <input type="password" name="password" class="form-control"
                                                placeholder="Contraseña..." required id="password"
                                                autocomplete="new-password">
                                            <div class="input-group-append" id="show_hide_password2">
                                                <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- MODAL CAMBIAR CONTRASEÑA --}}
        <div class="modal fade" id="cambiarPassModal" tabindex="-1" role="dialog" aria-labelledby="modalPasswordLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="change_pass_titulo"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="POST" id='formCambiarPass'>
                        @csrf
                        <div class="modal-body">
                            <span id="resultadoCambiarPass"></span>
                            <div class="row">
                                <input type="hidden" name='username' id='username_new_password'>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="new_password" class="col-form-label">Nueva contraseña</label>
                                        <input type="password" name="new_password" class="form-control"
                                            placeholder="Nueva contraseña..." required id="new_password">
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button type="button" class="btn btn-primary" id="btnCambiarPass">Cambiar
                                    contraseña</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- MODAL ASIGNAR ROLES --}}
        <div class="modal fade" id="modalAsignarRoles" tabindex="-1" role="dialog" aria-labelledby="modalPasswordLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">Asignar roles</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="POST" id='formAsignarRoles'>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 px-4" id="bodyRoles">
                                    @foreach ($roles as $rol)
                                        <div class="form-check my-3">
                                            <input class="form-check-input" type="radio" name="rol"
                                                value="{{ $rol->name }}"
                                                id="{{ 'rol' . str_replace(' ', '', $rol->name) }}">
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                {{ ucfirst($rol->name) }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="btnAsignarRoles">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row" id="mensajePass">
                            <div class="col-md-12 text-center">
                                <h4><strong>Debe autenticarse para poder administrar los usuarios.</strong></h4>
                                <button type="button" class="btn btn-primary btn-lg mt-4" data-toggle="modal"
                                    data-target="#modalPassword">Autenticarse</button>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered d-none" id="usuarios">
                            <thead class="bg-hfrut">
                                <tr class="fuente-header">
                                    <th scope="col">
                                        Nombre</th>
                                    <th scope="col" style="width: 45%">
                                        Rol/Roles</th>
                                    <th scope="col" style="width:30%;">
                                        Opciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>



@stop


@section('js')
    <script>
        $(document).ready(function() {
            var tablaUsuarios = '';
            var idUsuario;

            // var rolUsuario = @json($rolUsuario);

            $('#modalPassword').modal('show');

            function mostrarPassword(idInput, idButton) {
                if ($(idInput).attr("type") == "text") {
                    $(idInput).attr('type', 'password');
                    $(idButton + ' i').addClass("fa-eye-slash");
                    $(idButton + ' i').removeClass("fa-eye");
                } else if ($(idInput).attr("type") == "password") {
                    $(idInput).attr('type', 'text');
                    $(idButton + ' i').removeClass("fa-eye-slash");
                    $(idButton + ' i').addClass("fa-eye");
                }
            }

            $('#show_hide_password').click(function(e) {
                e.preventDefault();
                mostrarPassword('#passw', '#show_hide_password');
            });

            $('#show_hide_password2').click(function(e) {
                e.preventDefault();
                mostrarPassword('#password', '#show_hide_password2');
            });

            $('#formPassword').on('submit', function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('users.password-check') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            if ($('#passIncorrecta').hasClass('d-none') == true) {
                                $('#passIncorrecta').removeClass('d-none')
                            }
                        }
                        if (data[0].success) {
                            $('#modalPassword').trigger('click');
                            $('#mensajePass').addClass('d-none');
                            $('#usuarios').removeClass('d-none');

                            var html =
                                '<a href="" class="btn btn-success bg-hfrut" data-toggle="modal" data-target="#crearUsuarioModal">Agregar nuevo usuario</a>';

                            tablaUsuarios = $('#usuarios').DataTable({
                                responsive: true,
                                autoWidth: false,
                                processing: true,
                                serverSide: true,
                                bFilter: false,
                                bLengthChange: false,
                                // bPaginate: false,
                                language: {
                                    url: "{{ asset('language-datatable/es-mx.json') }}"
                                },
                                ajax: "{{ route('users.users') }}",
                                columns: [{
                                        data: 'username',
                                    },
                                    {
                                        data: 'roles',
                                        render: function(roles) {
                                            var html = '<ul>'

                                            var matches = roles.match(
                                                /\[(.*?)\]/);

                                            var todosRoles = matches[1].split(
                                                ',');

                                            for (let i = 0; i < todosRoles
                                                .length; i++) {
                                                if (!todosRoles[i] == '') {
                                                    html += (
                                                        '<li class="text-capitalize">' +
                                                        todosRoles[i]
                                                        .replace(/"/g, '') +
                                                        '</li>');
                                                } else {
                                                    html +=
                                                        '<li>Ninguno. Asigne alguno para que el usuario pueda empezar a utilizar el sistema</li>';
                                                }
                                            }

                                            return html + '</ul>'

                                        }
                                    },
                                    {
                                        data: 'opciones',
                                        className: 'text-right'
                                    },
                                ],
                            });

                            $('#agregarUsuario').html(html);

                            /* Por la variable data tambien vienen los roles, por lo que los asigno a una variable para poder recorrerlos mas facil
                            Los voy agregando como html en una variable y luego los inserto dentro del modal */

                            // var htmlRoles = '';
                            // var roles = data[1].roles;

                            // for (let i = 0; i < roles.length; i++) {
                            //     let checked = '';
                            //     rolUsuario == roles[i] ? checked = 'checked' : checked = ' ';

                            //     htmlRoles +=
                            //         '<div class="form-check mt-3"><label class="form-check-label"><input type="radio" class="form-check-input" name="roles[]"  value="' +
                            //         roles[i] + '">' +
                            //         roles[i] + '</label></div>'
                            // }

                            // $('#bodyRoles').html(htmlRoles);
                        }
                    }
                })
            });

            $('#formCrearUsuario').on('submit', function(e) {
                e.preventDefault()
                $.ajax({
                    url: "{{ route('users.create') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(data) {
                        var html = '';
                        if (data.error) {
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoCrear').html(html);
                        }
                        if (data.success) {
                            tablaUsuarios.ajax.reload()
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: 'Usuario agregado con éxito',
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });
                            $('#crearUsuarioModal').trigger('click');
                        }

                    }
                })
            });

            $('#btnCambiarPass').on('click', function(event) {
                event.preventDefault()
                $.ajax({
                    url: "{{ route('users.change') }}",
                    method: "POST",
                    data: $('#formCambiarPass').serialize(),
                    dataType: 'json',
                    success: function(data) {
                        if (data.error) {
                            html =
                                '<div class = "alert alert-danger pb-0 px-0 mx-3" role = "alert" id="alertaErrores"><ul>';
                            for (var i = 0; i < data.error.length; i++) {
                                html += '<li>' + data.error[i] + '</li>';
                            }
                            html += '</ul></div>';
                            $('#resultadoCambiarPass').html(html);
                        }
                        if (data.success) {
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: 'Contraseña cambiada con éxito',
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            });
                            $('#cambiarPassModal').trigger('click');
                        }
                    }
                });
            });

            $("#cambiarPassModal").on("show.bs.modal", function(event) {
                var button = $(event.relatedTarget);
                //Paso el nombre del cargo por variables data dentro del boton (HTML), al igual que el id
                var username = button.data("username");
                $('#username_new_password').val(username);
                $('#change_pass_titulo').html('Cambiar contraseña. Usuario: <strong>' +
                    username + '</strong>');
                var modal = $(this);
            });

            $("#cambiarPassModal").on("hide.bs.modal", function(event) {
                $('#change_pass_titulo').val('');
                $('#new_password').val('');
                $('#confirm_password').val('');
            });

            $('#crearUsuarioModal').on('hide.bs.modal', function(e) {
                $('#nombreUsuario').val('');
                $('#password').val('');
                $('#resultadoCrear').empty();
            })

            $('#crearUsuarioModal').on('shown.bs.modal', function(e) {
                $('#nombreUsuario').focus();
            })

            $("#cambiarPassModal").on("shown.bs.modal", function(event) {
                $('#new_password').focus();
            });


            $('#modalPassword').on('shown.bs.modal', function(e) {
                $('#passw').focus();
            });

            // $('#modalAsignarRoles').on('shown.bs.modal', function(e) {
            //     var button = $(e.relatedTarget);
            //     idUsuario = button.data('idusuario');
            // });

            $('#modalAsignarRoles').on('hide.bs.modal', function(e) {
                $("#bodyRoles :input").each(function() {
                    $(this).prop('checked', false);
                });
            });

            $('#modalAsignarRoles').on('show.bs.modal', function(e) {
                var button = $(e.relatedTarget);
                idUsuario = button.data('idusuario');
                var rolUsuario = button.data('rol');
                var idRol = '#rol' + rolUsuario.replace(/\s/g, '')
                //Chequeo el input
                $(idRol).prop('checked', true);
            });

            //========= BOTON ENVIAR PETICION PARA ASIGNAR ROLES A USUARIO =========
            $('#btnAsignarRoles').on('click', function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{{ route('users.roles') }}",
                    data: $('#formAsignarRoles').serialize() + '&_token={{ csrf_token() }}' +
                        '&user=' + idUsuario,
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2300,
                            })
                            Toast.fire({
                                icon: 'success',
                                title: data.success,
                            })

                            tablaUsuarios.ajax.reload();

                            $('#modalAsignarRoles').trigger('click');
                        } else {
                            Swal.fire(
                                'Oops...',
                                data.error,
                                'error'
                            )
                        }
                    }
                });
            });





        });
    </script>
@stop
