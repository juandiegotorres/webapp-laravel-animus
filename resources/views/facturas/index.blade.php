@extends('adminlte::page')

@section('plugins.Sweetalert2', true)

@section('title', 'Dashboard')

@section('content_header')
    @can('facturas.create')
        <div class="float-right pr-2">
            <a href="{{ route('facturas.create', ['emitida_recibida' => 'emitida']) }}" class="btn btn-success bg-hfrut">
                Agregar factura emitida
            </a>
            <a href="{{ route('facturas.create', ['emitida_recibida' => 'recibida']) }}" class="btn btn-success bg-hfrut">
                Agregar factura recibida
            </a>
        </div>
    @endcan
    <h1>Facturas</h1>
@stop

@section('content')
    @if (!empty(Session::get('status')))
        @push('js')
            <script>
                var popupId = "{{ uniqid() }}";
                if (!sessionStorage.getItem('shown-' + popupId)) {
                    Swal.fire({
                        toast: true,
                        type: 'success',
                        title: '{{ session('status') }}',
                        position: 'top-right',
                        showConfirmButton: false,
                        timer: 2300,
                    });
                }
                sessionStorage.setItem('shown-' + popupId, '1');
            </script>
        @endpush
    @endif
    <div class="row px-2">
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Filtrar</label>
                <select class="form-control" name="" id="filtroFacturas">
                    <option value="todas">TODAS</option>
                    <option value="emitida">EMITIDAS</option>
                    <option value="recibida">RECIBIDAS</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row px-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" id="app">
                    {{-- TABLA --}}
                    <table class="table table-bordered table-rounded" id="tablaFacturas">
                        <thead class="bg-hfrut">
                            <tr class="fuente-header">
                                <th scope="col">N° de Factura</th>
                                <th scope="col">Fecha de emision</th>
                                <th scope="col">Emitida/Recibida</th>
                                <th scope="col">Emisor/Receptor</th>
                                <th scope="col">Total</th>
                                <th scope="col" style="width:15px">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>

    @can('facturas.show')
        <div class="modal fade" id="modalMostrarFactura" data-backdrop="static" data-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Factura</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body pl-5 pt-4">
                        <div class="row mb-2 ">
                            <div class="col-md-4">
                                <strong><i class="fas fa-money-check-alt mr-2"></i>N° de factura</strong>

                                <p class="text-muted mt-1" id="nroFactura">
                                </p>

                                <hr>
                            </div>
                            <div class="col-md-4 offset-2">
                                <strong><i class="fas fa-calendar-alt mr-2"></i>Fecha de emisión</strong>

                                <p class="text-muted mt-1" id="fechaEmision">
                                </p>

                                <hr>
                            </div>

                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                <strong><i class="fas fa-dollar-sign mr-2"></i>Total Neto</strong>

                                <p class="text-muted mt-1" id="totalNeto">
                                </p>

                                <hr>
                            </div>
                            <div class="col-md-4 offset-2">
                                <strong><i class="fas fa-percentage mr-2"></i>IVA</strong>

                                <p class="text-muted mt-1" id="IVA">
                                </p>

                                <hr>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                <strong><i class="fas fa-file-invoice-dollar mr-2"></i>Tipo de Factura</strong>

                                <p class="text-muted mt-1 text-capitalize" id="tipoFactura">
                                </p>

                                <hr>
                            </div>

                            <div class="col-md-4 offset-2">
                                <strong><i class="fas fa-people-carry mr-2"></i>Emisor/Receptor</strong>
                                <p class="text-muted mt-1" id="emisorReceptor">
                                </p>
                                <hr>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                <strong><i class="fas fa-file-pdf mr-2"></i>PDF de la Factura</strong>

                                <p class="text-muted mt-1" id="pdf">
                                </p>
                                {{-- <img src="/img/pdf.png" alt="" class="img-pdf"> --}}
                                <hr>
                            </div>
                            <div class="col-md-4 offset-2">
                                <strong><i class="fas fa-dollar-sign mr-2"></i>Total</strong>

                                <p class="text-muted mt-1" id="total">
                                </p>

                                <hr>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary">Editar</button>
                    </div>
                </div>
            </div>
        </div>
    @endcan
@stop

@section('js')
    <script>
        $(document).ready(function() {

            var tablaFacturas;

            $('#filtroFacturas').change(function(e) {
                e.preventDefault();
                tablaFacturas.ajax.reload();
            });



            // $('#filtros').click(function(e) {
            //     e.preventDefault();
            //     console.log($('#filtroFacturas').val())
            // });
            //================== MOSTRAR FACTURA =====================
            $("#modalMostrarFactura").on("show.bs.modal", function(event) {
                var id = $(event.relatedTarget).data('id');
                $.get("/facturas/mostrar/" + id, function(data) {
                    $('#nroFactura').text(data.nroFactura);
                    $('#fechaEmision').text(data.fechaEmision);
                    $('#totalNeto').text(formatear.format(data.totalNeto));
                    $('#IVA').text('$' + data.IVA);
                    $('#tipoFactura').text(data.emitida_recibida);
                    if (data.emisor_receptor == null) {
                        $('#emisorReceptor').text('No especificado');
                    } else {
                        $('#emisorReceptor').text(data.emisor_receptor)
                    }
                    if (data.pdf == null) {
                        $('#pdf').text('No adjuntado');
                    } else {
                        // console.log(data.pdf);
                        $('#pdf').html('<a href="storage/' + data.pdf + '" target="_blank">' + data
                            .pdf.replace(
                                'pdf_facturas/', '') +
                            '<i class="fa fa-download ml-2" aria-hidden="true"></i> </a>');
                    }
                    $('#total').text(formatear.format(data.total));
                });
            });


            //======================== ELIMINAR FACTURA ===================================
            $(document).on('click', '.btnEliminarFactura', function(e) {
                var id = $(this).data('id');
                Swal.fire({
                    title: "¿Desea dar de baja esta factura?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, estoy seguro",
                    cancelButtonText: "No"
                }).then((result) => {
                    if (result.value) {
                        if (result.isConfirmed) {
                            axios.post('/facturas/eliminar/' + id, {
                                    _method: "put"
                                })
                                .then(respuesta => {
                                    tablaFacturas.ajax.reload();
                                    Swal.fire({
                                        toast: true,
                                        icon: 'success',
                                        title: "Factura dada de baja",
                                        position: 'top-right',
                                        showConfirmButton: false,
                                        timer: 2300,
                                    });
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                            tablaFacturas.ajax.reload();
                        }
                    }
                });
            });

            //==================== DATATABLE ===========================
            tablaFacturas = $('#tablaFacturas').DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                bFilter: true,
                bLengthChange: false,
                // bPaginate: false,
                language: {
                    url: "{{ asset('language-datatable/es-mx.json') }}"
                },
                ajax: {
                    "url": "/facturas/dt",
                    "data": function(d) {
                        d.filtro = $('#filtroFacturas').val();
                        d._token = "{{ csrf_token() }}";
                    },
                    "type": "POST",
                    "dataType": "json"
                },
                columns: [{
                        data: 'nroFactura'
                    },
                    {
                        data: 'fechaEmision'
                    },
                    {
                        data: 'emitida_recibida'
                    },
                    {
                        data: 'emisor_receptor',
                        render: function(emisor_receptor) {
                            if (emisor_receptor == null) {
                                return 'No especificado'
                            } else {
                                return emisor_receptor;
                            }
                        }
                    },
                    {
                        data: 'total',
                        render: function(total) {
                            return formatear.format(total);
                        }
                    },
                    {
                        data: 'opciones'
                    },
                    {
                        data: 'created_at',
                        visible: false
                    },
                ],
                order: [6, 'desc']
            });


            //Funcion para formatear numeros a formato de dinero
            var formatear = new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
            });
        });
    </script>
@stop
