@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 class="pl-2">Agregar factura {{ $emitida_recibida }}</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => 'facturas.store', 'class' => 'p-4', 'autocomplete' => 'off', 'enctype' => 'multipart/form-data']) !!}
            <div class="row">
                {!! Form::hidden('emitida_recibida', $emitida_recibida, []) !!}
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('nroFactura', 'N° Factura (*)', ['class' => 'form-col-label']) !!}
                        {!! Form::number('nroFactura', null, ['class' => 'form-control', 'placeholder' => 'Número de factura...']) !!}
                        @error('nroFactura')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('fechaEmision', 'Fecha (*)', ['class' => 'form-col-label']) !!}
                        {!! Form::date('fechaEmision', now(), ['class' => 'form-control']) !!}
                        @error('fechaEmision')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('emisor_receptor', $emitida_recibida == 'emitida' ? 'Recibida por' : 'Emitida por', ['class' => 'form-col-label']) !!}
                        {!! Form::text('emisor_receptor', null, ['class' => 'form-control']) !!}
                        @error('emisor_receptor')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="files">Factura en pdf</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="pdf" id="customFile">
                        <label class="custom-file-label" for="customFile">Seleccione el archivo...</label>
                    </div>
                    @error('pdf')
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        {!! Form::label('totalNeto', 'Total neto (*)', ['class' => 'form-col-label']) !!}
                        {!! Form::number('totalNeto', null, ['class' => 'form-control', 'id' => 'totalNeto']) !!}
                        @error('totalNeto')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    {!! Form::label('IVA', 'IVA (*)', ['class' => 'form-col-label']) !!}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">%</span>
                        </div>
                        {!! Form::number('IVA', 21, ['class' => 'form-control', 'id' => 'iva', 'step' => 'any']) !!}
                    </div>
                    @error('IVA')
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="">Total</label>
                            <input type="text" name="total" id="total" value="{{ old('total') }}" class="form-control"
                                placeholder="" aria-describedby="helpId" readonly>
                        </div>

                        {{-- {!! Form::label('total', 'Total (*)', ['class' => 'form-col-label']) !!}
                    {!! Form::number('total', null, ['class' => 'form-control', 'readonly' => true, 'id' => 'total'])
                    !!} --}}
                    </div>
                </div>

            </div>

            <div class="row justify-content-end mt-4">
                <div class="col-md-6 d-flex justify-content-end">
                    <a onclick="window.history.back();" class="btn btn-danger mr-2">Cancelar</a>
                    {!! Form::submit('Agregar factura', ['class' => 'btn btn-success']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop


@section('js')
    <script type="text/javascript" src="{{ asset('js/bootstrap-filestyle.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            //================= EVENTOS KEY UP =========================
            $('#totalNeto').keyup(function(e) {
                var total = calcularIVA($('#totalNeto').val(), $('#iva').val());
                $('#total').val(total);
            });
            $('#iva').keyup(function(e) {
                var total = calcularIVA($('#totalNeto').val(), $('#iva').val());
                $('#total').val(total);
            });

            $(".custom-file-input").on("change", function() {
                var fileName = $(this).val().split("\\").pop();
                $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
            });

            function calcularIVA(precio, iva) {
                var porcentaje = ((precio * iva) / 100);
                var total = parseFloat(precio) + parseFloat(porcentaje);
                return parseFloat(total).toFixed(2);;
            }
        })
    </script>
@stop
