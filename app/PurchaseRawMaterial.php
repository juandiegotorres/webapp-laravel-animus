<?php

namespace App;

use App\Provider;
use App\DetailPurchaseRawMaterial;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class PurchaseRawMaterial extends Model
{
    protected $fillable = ['provider_id', 'iva', 'total', 'detalle', 'fecha', 'estado', 'borrado'];

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    public function detalleCompra()
    {
        return $this->hasMany(DetailPurchaseRawMaterial::class);
    }
    public function proveedor()
    {
        return $this->belongsTo(Provider::class, 'provider_id');
    }


    public function scopeHistorialCompra($query)
    {
        $query->join('providers', 'purchase_raw_materials.provider_id', '=', 'providers.id')
            ->select('purchase_raw_materials.id', 'iva', 'fecha', 'total', 'provider_id', 'nombreCompleto', 'purchase_raw_materials.created_at')
            ->where('purchase_raw_materials.borrado', 0)
            ->orderBy('fecha', 'DESC');
    }

    public function scopeEntreFechas($query, $fechaInicio, $fechaFin)
    {
        $query->whereBetween('fecha', [$fechaInicio, $fechaFin]);
    }

    public function scopeProveedor($query, $idproveedor)
    {
        $query->where('provider_id', $idproveedor);
    }
}
