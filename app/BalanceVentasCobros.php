<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class BalanceVentasCobros extends Model
{
    protected $fillable = [
        'client_id',
        'tipoMovimiento',
        'movimiento_id',
        'debe',
        'haber',
        'fecha',
        'estado',
        'borrado',
    ];

    const VENTA = 'V';
    const COBRO = 'C';

    public static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }
}
