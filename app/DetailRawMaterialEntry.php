<?php

namespace App;

use App\Envase;
use App\RawMaterial;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class DetailRawMaterialEntry extends Model
{

    protected $fillable = [
        'raw_material_entries_id',
        'raw_material_id',
        'cantidadEnvase',
        'envase_id',
        'kgTara',
        'kgBruto',
        'kgNeto',
        'tamanoChico',
        'tamanoMediano',
        'tamanoGrande',
        'unidadesKg',
        'estado',
        'borrado',
    ];

    public function materiaPrima()
    {
        return $this->belongsTo(RawMaterial::class, 'raw_material_id');
    }

    public function envase()
    {
        return $this->belongsTo(Envase::class, 'envase_id');
    }
}
