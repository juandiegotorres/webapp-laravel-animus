<?php

namespace App;

use App\Provider;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class RawMaterialEntry extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'fecha',
        'provider_id',
        'raw_material_id',
        'kgTara',
        'kgBruto',
        'kgNeto',
        'tamanoChico',
        'tamanoMediano',
        'tamanoGrande',
        'unidadesKg',
        'observaciones',
        'estado',
        'borrado',
    ];

    public function materiaPrima()
    {
        return $this->belongsTo(RawMaterial::class, 'raw_material_id');
    }

    public function envase()
    {
        return $this->hasMany(DetailRawMaterialEntry::class, 'raw_material_entries_id');
    }

    public function proveedor()
    {
        return $this->belongsTo(Provider::class, 'provider_id');
    }

    public function movimientos()
    {
        return $this->hasMany(RawMaterialMovement::class, 'id_ingreso');
    }
}
