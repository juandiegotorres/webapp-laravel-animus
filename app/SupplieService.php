<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class SupplieService extends Model
{

    protected $fillable = ['nombre', 'tipo', 'estado', 'borrado'];
}
