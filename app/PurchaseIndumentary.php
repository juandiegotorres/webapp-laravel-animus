<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseIndumentary extends Model
{
    protected $fillable = [
        'provider_id',
        'iva',
        'total',
        'detalle',
        'fecha',
        'estado',
        'borrado'
    ];

    public function detalleCompra()
    {
        return $this->hasMany(DetailPurchaseIndumentary::class, 'purchase_indumentary_id');
    }
    public function proveedor()
    {
        return $this->belongsTo(Provider::class, 'provider_id');
    }
    public function indumentaria()
    {
        return $this->belongsTo(Indumentary::class, 'indumentary_id');
    }

    public function scopeHistorialCompra($query)
    {
        $query->join('providers', 'purchase_indumentaries.provider_id', '=', 'providers.id')
            ->select('purchase_indumentaries.id', 'fecha', 'iva',  'total', 'provider_id', 'nombreCompleto', 'purchase_indumentaries.created_at')
            ->where('purchase_indumentaries.borrado', 0)
            ->orderBy('fecha', 'DESC');
    }
}
