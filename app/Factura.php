<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{

    public static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'nroFactura',
        'fechaEmision',
        'totalNeto',
        'IVA',
        'total',
        'emitida_recibida',
        'emisor_receptor',
        'pdf',
        'estado',
        'borrado'
    ];
}
