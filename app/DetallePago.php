<?php

namespace App;

use App\MetodoPago;
use Illuminate\Database\Eloquent\Model;

class DetallePago extends Model
{
    protected $fillable = [
        'pago_id',
        'metodo_pago_id',
        'cheque_id',
        'monto',
    ];

    public function metodoPago()
    {
        return $this->belongsTo(MetodoPago::class, 'metodo_pago_id');
    }

    public function cheque()
    {
        return $this->hasMany(Cheque::class, 'cheque_id');
    }
}
