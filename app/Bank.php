<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'nombre',
        'estado',
        'borrado'
    ];
}
