<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class Liquidacion extends Model
{
    protected $table = 'liquidaciones';

    protected $fillable = [
        'provider_id',
        'fechaDeLiquidacion',
        'cantidad_cuotas',
        'observaciones',
        'estado',
        'borrado',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    public function proveedor()
    {
        return $this->belongsTo(Provider::class, 'provider_id');
    }
}
