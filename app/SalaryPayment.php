<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class SalaryPayment extends Model
{

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'employee_id',
        'fecha',
        'total',
        'concepto',
        'estado',
        'borrado',
    ];

    public function empleado()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
