<?php

namespace App;

use App\Client;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class OrdenVenta extends Model
{
    protected $fillable = [
        'client_id',
        'fecha',
        'detalle',
        'estado',
        'borrado',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    public function cliente()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function detalleOrden()
    {
        return $this->hasMany(DetalleOrdenVenta::class, 'id_orden_venta');
    }
}
