<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class Envase extends Model
{
    protected $fillable = [
        'nombre',
        'peso',
        'cantidad',
        'estado',
        'borrado'
    ];

    public static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }
}
