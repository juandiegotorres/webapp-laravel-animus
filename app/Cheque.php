<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class Cheque extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'tipoCheque',
        'nroCheque',
        'fechaEmision',
        'fechaCobro',
        'fechaRecepcion',
        'titular',
        'entregadoPor',
        'entregadoA',
        'importe',
        'bank_id',
        'estado',
        'borrado',
    ];

    public function banco()
    {
        return $this->belongsTo(Bank::class, 'bank_id');
    }

    //ESTADOS CHEQUE 
    const CHEQUE_COBRADO_PAGADO = 0;
    const CHEQUE_AGREGADO = 1;
    const CHEQUE_ENTREGADO = 2;
    const CHEQUE_RECHAZADO = -1;
}
