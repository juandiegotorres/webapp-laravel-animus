<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovimientoStockInsumo extends Model
{
    protected $table = ['movimientos_stock_insumo'];
}
