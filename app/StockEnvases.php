<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class StockEnvases extends Model
{
    protected $fillable = [
        'fecha',
        'provider_id',
        'descripcion',
        'es_entregado',
        'estado',
        'borrado',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    public function detalleMovimiento()
    {
        return $this->hasMany(DetalleStockEnvases::class, 'id_stock_envases');
    }

    const ES_ENTREGADO = 0;
    const ES_RECIBIDO = 1;
}
