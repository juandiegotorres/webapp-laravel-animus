<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetodoPago extends Model
{
    protected $fillable = [
        'id',
        'nombre'
    ];

    protected $table = 'metodos_pago';
}
