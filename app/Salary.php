<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'employee_id',
        'fechaInicio',
        'fechaFin',
        'horas',
        'precioHora',
        'total',
        'observaciones',
        'estado',
        'borrado'
    ];

    public function empleado()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
