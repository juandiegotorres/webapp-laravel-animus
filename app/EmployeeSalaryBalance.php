<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class EmployeeSalaryBalance extends Model
{

    const MOVIMIENTO_HABER = 'H';
    const MOVIMIENTO_DEBE = 'D';

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'employee_id',
        'tipoMovimiento',
        'movimiento_id',
        'debe',
        'haber',
        'fecha',
        'estado',
        'borrado'
    ];
}
