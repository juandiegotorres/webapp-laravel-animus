<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class PriceList extends Model
{

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'nombre',
        'fechaVigencia',
        'estado',
        'borrado',
    ];

    public function detalleLista()
    {
        return $this->hasMany(DetailPriceList::class, 'price_list_id');
    }
}
