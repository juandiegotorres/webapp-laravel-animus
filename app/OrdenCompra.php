<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class OrdenCompra extends Model
{

    protected static function booter()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'provider_id',
        'fecha',
        'tipoOrden',
        'detalle',
    ];

    public function proveedor()
    {
        return $this->belongsTo(Provider::class, 'provider_id');
    }
}
