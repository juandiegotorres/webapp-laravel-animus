<?php

namespace App;

use App\Province;
use Illuminate\Database\Eloquent\Model;

class Locality extends Model
{
    public function provincia()
    {
        return $this->belongsTo(Province::class);
    }
}
