<?php

namespace App;

use App\SupplieService;
use App\PurchaseSupplieService;
use Illuminate\Database\Eloquent\Model;

class DetailPurchaseSupplieService extends Model
{
    protected $fillable = ['purchase_supplie_services_id', 'supplie_id', 'service_id', 'cantidad', 'precioUnitario', 'totalParcial', 'estado', 'borrado'];

    public function maestroCompra()
    {
        return $this->belongsTo(PurchaseSupplieService::class, 'purchase_supplie_services_id');
    }

    public function insumo()
    {
        return $this->belongsTo(Supplie::class, 'supplie_id');
    }

    public function servicio()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}
