<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaltanteFacturar extends Model
{
    protected $table = 'faltante_facturar';

    protected $fillable = [
        'provider_id',
        'raw_material_id',
        'cantidadKg',
    ];
}
