<?php

namespace App;

use App\Provider;
use App\DetailPurchaseSupplieService;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class PurchaseSupplieService extends Model
{
    protected $fillable = ['id', 'provider_id', 'tipoCompra', 'iva', 'total', 'detalle', 'fecha', 'estado', 'borrado'];

    public function detalleCompra()
    {
        return $this->hasMany(DetailPurchaseSupplieService::class, 'purchase_supplie_services_id');
    }
    public function proveedor()
    {
        return $this->belongsTo(Provider::class, 'provider_id');
    }
    public function insumo()
    {
        return $this->belongsTo(Supplie::class, 'supplie_id');
    }
    public function servicio()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    public function scopeHistorialCompra($query)
    {
        $query->join('providers', 'purchase_supplie_services.provider_id', '=', 'providers.id')
            ->select('purchase_supplie_services.id', 'fecha', 'iva',  'total', 'provider_id', 'nombreCompleto', 'purchase_supplie_services.created_at')
            ->where('purchase_supplie_services.borrado', 0)
            ->orderBy('fecha', 'DESC');
    }

    public function scopeEntreFechas($query, $fechaInicio, $fechaFin)
    {
        $query->whereBetween('fecha', [$fechaInicio, $fechaFin]);
    }

    public function scopeProveedor($query, $idproveedor)
    {
        $query->where('provider_id', $idproveedor);
    }

    const COMPRA_INSUMO = 'I';
    const COMPRA_SERVICIO = 'S';
}
