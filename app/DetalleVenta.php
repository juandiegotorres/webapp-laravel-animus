<?php

namespace App;

use App\Product;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class DetalleVenta extends Model
{
    protected $fillable = [
        'id_venta',
        'id_variante',
        'cantidad',
        'precio_producto',
        'tipo_precio',
        'subtotal',
    ];


    public function variante()
    {
        return $this->belongsTo(VarianteProducto::class, 'id_variante');
    }

    // protected static function booted()
    // {
    //     static::addGlobalScope(new NoBorradoScope);
    // }
}
