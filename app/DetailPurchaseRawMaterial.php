<?php

namespace App;

use App\PurchaseRawMaterial;
use Illuminate\Database\Eloquent\Model;

class DetailPurchaseRawMaterial extends Model
{
    protected $fillable = ['purchase_raw_material_id', 'raw_material_id', 'cantidad', 'precioUnitario', 'totalParcial', 'estado', 'borrado'];


    public function maestroCompra()
    {
        return $this->belongsTo(PurchaseRawMaterial::class, 'purchase_raw_material_id');
    }

    public function materiaPrima()
    {
        return $this->belongsTo(RawMaterial::class, 'raw_material_id');
    }
}
