<?php

namespace App;

use App\Cheque;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $fillable = [
        'provider_id',
        'fecha',
        'montoTotal',
        'observaciones',
        'metodoPago',
        'iva',
        'estado',
        'borrado',
    ];

    const PAGO_A_PROVEEDOR = 'PP';

    public static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    public function proveedor()
    {
        return $this->belongsTo(Provider::class, 'provider_id');
    }

    public function detallePago()
    {
        return $this->hasMany(DetallePago::class, 'pago_id');
    }
}
