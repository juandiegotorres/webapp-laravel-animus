<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class BalanceCompraPagos extends Model
{
    protected $table = 'balance_compras_pagos';

    const COMPRA_MATERIA_PRIMA = 'CMP';
    const COMPRA_INSUMO = 'CI';
    const COMPRA_SERVICIO = 'CS';
    const COMPRA_INDUMETARIA = 'CID';
    const PAGO_A_PROVEEDOR = 'PP';

    protected $fillable = [
        'provider_id',
        'tipoCompra',
        'movimiento_id',
        'debe',
        'haber',
        'fecha',
        'estado',
        'borrado',
    ];

    public static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }
}
