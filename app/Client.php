<?php

namespace App;

use App\Locality;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'id', 'nombreCompleto', 'dni', 'cuit', 'direccion', 'telefono', 'email',
        'idLocalidad', 'cbu', 'price_list_id', 'transporte', 'estado', 'borrado'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    public static function nombreCompleto($nombre, $apellido)
    {
        return $apellido . ', ' . $nombre;
    }

    public function localidad()
    {
        return $this->belongsTo(Locality::class, 'idLocalidad');
    }
}
