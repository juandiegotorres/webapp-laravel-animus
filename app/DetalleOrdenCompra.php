<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleOrdenCompra extends Model
{
    protected $fillable = [
        'id_objeto',
        'cantidad',
        'id_orden'
    ];
}
