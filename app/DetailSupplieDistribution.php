<?php

namespace App;

use App\Supplie;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class DetailSupplieDistribution extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'supplie_distributions_id',
        'supplie_id',
        'cantidad',
        'estado',
        'borrado',
    ];

    public function insumo()
    {
        return $this->belongsTo(Supplie::class, 'supplie_id');
    }
}
