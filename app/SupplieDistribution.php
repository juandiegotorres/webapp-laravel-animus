<?php

namespace App;

use App\Employee;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class SupplieDistribution extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = ['employee_id', 'fecha', 'detalle', 'estado', 'borrado'];

    public function detalleDistribucion()
    {
        return $this->hasMany(DetailSupplieDistribution::class, 'indumentary_distributions_id');
    }

    public function empleado()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
