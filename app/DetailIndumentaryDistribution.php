<?php

namespace App;

use App\Indumentary;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class DetailIndumentaryDistribution extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'indumentary_distributions_id',
        'indumentary_id',
        'esCertificada',
        'cantidad',
        'estado',
        'borrado',
    ];

    public function insumo()
    {
        return $this->belongsTo(Indumentary::class, 'indumentary_id');
    }
}
