<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    protected $fillable = ['nombre', 'estado', 'borrado'];

    public function empleados()
    {
        return $this->hasMany(Employee::class);
    }
}
