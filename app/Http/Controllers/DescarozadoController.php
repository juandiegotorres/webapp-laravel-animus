<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Descarozado;
use App\RawMaterial;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class DescarozadoController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:entregas-descarozado']);
    }

    public function index()
    {
        $empleados = Employee::join('charges', 'employees.charge_id', '=', 'charges.id')
            ->select('employees.id', 'employees.nombreCompleto', 'dni', 'charges.nombre')
            ->get();

        return view('entrega-descarozado.index')
            ->with('empleados', $empleados);
    }


    public function datatable()
    {
        $descarozado = DB::table('entrega_descarozado')
            ->join('employees', 'entrega_descarozado.employee_id', '=', 'employees.id')
            ->join('raw_materials', 'entrega_descarozado.raw_material_id', '=', 'raw_materials.id')
            ->leftJoin('recepcion_descarozado', 'entrega_descarozado.id', '=', 'recepcion_descarozado.id_entrega_descarozado')
            ->select('entrega_descarozado.*', 'cantidadKgRetorno', 'porcentajePerdida', 'nombreCompleto', 'fruta', 'variedad')
            ->where('entrega_descarozado.borrado', '=', 0)
            ->orderBy('id', 'ASC')
            ->get();

        return DataTables::of($descarozado)
            ->editColumn('cantidadKg', '{{ $cantidadKg }} kgs')
            ->editColumn('fecha', '{{ date("d-m-Y", strtotime($fecha)) }} ')
            ->addColumn('materiaPrima', '{{$fruta . ", " . $variedad}}')
            ->addColumn('opciones', function ($descarozado) {
                $html = '<div class="d-flex justify-content-end">';
                $html .= '<a href="' . route('descarozado.pdf', ['descarozado' => $descarozado->id]) . '" target="_blank" class="btn btn-warning btn-sm mr-2" ><i class="fa fa-print" aria-hidden="true"></i></a>';

                if (auth()->user()->can('entrega-descarozado.show')) {
                    $html .= '<a data-id="' . $descarozado->id . '" class="btn btn-success btn-sm mr-2 btnMostrarEntrega" data-toggle="modal" data-target="#modalMostrarEntrega"> 
                            <i class="fa fa-info-circle"></i>
                        </a>';
                }

                if (auth()->user()->can('entrega-descarozado.edit') &&  $descarozado->porcentajePerdida == null) {
                    $html .=  '<a class="btn btn-primary btn-sm mr-2" href="' . route('entrega-descarozado.edit', ['descarozado' => $descarozado->id]) . '"> 
                            <i class="fa fa-pen"></i>
                        </a>';
                }

                if (auth()->user()->can('entrega-descarozado.delete')) {
                    if ($descarozado->porcentajePerdida == null) {
                        $html .= '<a  data-id="' . $descarozado->id . '" class="btn btn-danger btn-sm mr-2 btnEliminarEntrega">
                            <i class="fa fa-trash"></i>
                        </a>';
                    } else {
                        $html .= '<button class="boton btn btn-secondary btn-sm mr-2" data-toggle="tooltip" data-placement="left" title="No se puede eliminar una entrega que ya tiene una devolucion"><i class="fa fa-trash"></i></button>';
                    }
                }

                return $html;
            })
            ->editColumn('porcentajePerdida', function ($descarozado) {
                if ($descarozado->porcentajePerdida == null) {
                    return '<a data-id="' . $descarozado->id . '" data-fecha="' . $descarozado->fecha  . '" data-cantidad="' . $descarozado->cantidadKg . '" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalRegistrarDevolucion">Registrar devolución</a>';
                } else if ($descarozado->porcentajePerdida > 25) {
                    return '<span class="badge badge-danger p-2">' . $descarozado->porcentajePerdida . '%</span>';
                } else {
                    return '<span class="badge badge-success p-2">' . $descarozado->porcentajePerdida . '%</span>';
                }
            })
            ->rawColumns(['opciones', 'porcentajePerdida'])
            ->make(true);

        // return response()->json($descarozado);
    }


    public function create(Request $request)
    {
        $materiasPrimas = RawMaterial::all();
        $empleadoSeleccionado = Employee::find($request->empleados);

        return view('entrega-descarozado.create')
            ->with('materiasPrimas', $materiasPrimas)
            ->with('empleadoSeleccionado', $empleadoSeleccionado);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!isset($request['cantidad'])) {
            return response()->json(['error' => 'Debe seleccionar al menos una materia prima para entregar al  descarozado']);
        }

        return DB::transaction(function () use ($request) {
            try {
                for ($i = 0; $i < count($request['cantidad']); $i++) {
                    $valores[] = [
                        'employee_id' => $request['idEmpleado'],
                        'raw_material_id' => $request['materia-prima'][$i],
                        'fecha' => $request['fecha'],
                        'cantidadKg' => $request['cantidad'][$i],
                        'observaciones' => $request['observaciones'][$i],
                        'created_at' => now(),
                        'updated_at' => now()
                    ];
                }

                DB::table('entrega_descarozado')->insert($valores);

                return response()->json(['success' => 'Ingreso registrado con éxito']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()]);
            }
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Descarozado  $descarozado
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $descarozado = DB::table('entrega_descarozado')
            ->join('employees', 'entrega_descarozado.employee_id', '=', 'employees.id')
            ->join('raw_materials', 'entrega_descarozado.raw_material_id', '=', 'raw_materials.id')
            ->leftJoin('recepcion_descarozado', 'entrega_descarozado.id', '=', 'recepcion_descarozado.id_entrega_descarozado')
            ->select('entrega_descarozado.*', 'cantidadKgRetorno', 'porcentajePerdida', 'nombreCompleto', 'fruta', 'variedad', 'recepcion_descarozado.observaciones as observacionesDevolucion', 'recepcion_descarozado.fecha AS fechaDevolucion')
            ->where('entrega_descarozado.borrado', '=', 0)
            ->where('entrega_descarozado.id', '=', $id)
            ->get();

        return response()->json($descarozado);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Descarozado  $descarozado
     * @return \Illuminate\Http\Response
     */
    public function edit($descarozado)
    {
        $descarozado = DB::table('entrega_descarozado')
            ->where('id', '=', $descarozado)
            ->get();

        $materiasPrimas = RawMaterial::all();

        return view('entrega-descarozado.edit')
            ->with('descarozado', $descarozado)
            ->with('materiasPrimas', $materiasPrimas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Descarozado  $descarozado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $descarozado)
    {
        // dd($request->all());
        $data = $request->validate([
            'materiaPrima' => 'required',
            'fecha' => 'required|date|date_format:Y-m-d|before:2099-12-31|after:2000-01-01',
            'cantidadKg' => 'required|numeric|digits_between:1,9',
            'observaciones' => 'nullable'
        ]);
        try {
            DB::table('entrega_descarozado')
                ->where('id', '=', $descarozado)
                ->update([
                    'raw_material_id' => $data['materiaPrima'],
                    'fecha' => $data['fecha'],
                    'cantidadKg' => $data['cantidadKg'],
                    'observaciones' => $data['observaciones'],
                    'updated_at' => now(),
                ]);
            $request->session()->flash('status', ['message' => 'Entrega a descarozado modificada con éxito', 'type' => 'success']);
            //
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('status', ['message' => $e->getMessage(), 'type' => 'error']);
        }
        return redirect()->action('DescarozadoController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Descarozado  $descarozado
     * @return \Illuminate\Http\Response
     */
    public function delete($descarozado)
    {
        // return response()->json($descarozado);
        return DB::transaction(function () use ($descarozado) {
            try {
                DB::table('entrega_descarozado')
                    ->whereId($descarozado)
                    ->update(['borrado' => 1]);

                DB::table('recepcion_descarozado')
                    ->where('id_entrega_descarozado', '=', $descarozado)
                    ->delete();

                return response()->json(['success' => 'Entrega dada de baja con éxito']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()]);
            }
        });
    }

    public function devolucionDescarozado(Request $request)
    {
        $rules = array(
            'fechaDevolucion' => 'required|date|date_format:Y-m-d|before:2099-12-31|after:2000-01-01',
            'cantidadDevolucion' => 'required|numeric|digits_between:1,9|gt:0',
            'observaciones' => 'nullable',
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        try {
            DB::table('recepcion_descarozado')->insert([
                'id_entrega_descarozado' => $request['id_entrega_descarozado'],
                'fecha' => $request['fechaDevolucion'],
                'observaciones' => $request['observaciones'],
                'cantidadKgRetorno' => $request['cantidadDevolucion'],
                'porcentajePerdida' => round($request['porcetajePerdida'], 2),
                'created_at' => now(),
                'updated_at' => now()
            ]);

            return response()->json(['success' => 'Devolución registrada con éxito']);
            //
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => [$e->getMessage()]]);
        }
    }
}
