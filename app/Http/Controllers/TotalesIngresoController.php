<?php

namespace App\Http\Controllers;

use App\Provider;
use App\RawMaterialEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class TotalesIngresoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:totales-ingresos']);
    }

    public function index()
    {
        $proveedores = Provider::deMateriaPrima()->get();
        return view('totales-ingreso.index')
            ->with('proveedores', $proveedores);
    }

    public function filtrar(Request $request)
    {
        $fechaInicio = $request['fechaInicio'];
        $fechaFin = $request['fechaFin'];
        $idproveedor = $request['proveedor'];

        $rules = array(
            'fechaInicio' => 'nullable|date|before:fechaFin|after:2000-01-01',
            'fechaFin' => 'required|date|before:2099-12-31|' . $fechaInicio != null ? 'after:fechaInicio' : '',
            'proveedor' => 'nullable|required_without:fechaInicio',
        );

        $error = Validator::make($request->all(), $rules, ['proveedor.required_without' => 'El campo proveedor es obligatorio cuando fecha inicio no está presente o viceversa.']);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        $query = RawMaterialEntry::leftJoin('raw_material_movements', 'raw_material_entries.id', 'raw_material_movements.id_ingreso')
            ->join('raw_materials', 'raw_materials.id', 'raw_material_entries.raw_material_id')
            ->select(DB::raw('sum(kgNeto) -  ifnull(sum(cantidadKg), 0) AS total'), DB::raw("CONCAT(raw_materials.fruta, ', ', raw_materials.variedad) AS materiaPrima"))
            ->groupBy('raw_material_id', 'raw_materials.fruta', 'raw_materials.variedad');

        if ($fechaInicio == null) {
            //Solo filtrar por proveedor
            $totales = $query->where('provider_id', $idproveedor)->get();

            //
        } elseif ($idproveedor == '' || $idproveedor == null) {
            //Solo debo filtrar entre fechas
            $totales = $query->whereBetween('fecha', [$fechaInicio, $fechaFin])->get();
        } else {
            //Debo filtrar entre ambos
            $totales = $query->whereBetween('fecha', [$fechaInicio, $fechaFin])->where('provider_id', $idproveedor)->get();
        }

        return DataTables::of($totales)
            ->editColumn('total', '{{ number_format($total, 2, ",", ".") }}')
            ->make(true);
    }
}
