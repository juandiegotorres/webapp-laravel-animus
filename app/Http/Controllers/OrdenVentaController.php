<?php

namespace App\Http\Controllers;

use App\Client;
use App\Product;
use App\PriceList;
use App\OrdenVenta;
use App\DetalleOrdenVenta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use JeroenNoten\LaravelAdminLte\Components\Tool\Datatable;

class OrdenVentaController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:vender']);
    }

    public function index()
    {
        $clientes = Client::all();
        return view('orden-venta.index')
            ->with('clientes', $clientes);
    }

    function devolverTabla($data)
    {
        return DataTables::of($data)
            ->editColumn('fecha', '{{ date("d-m-Y", strtotime($fecha)) }}')
            ->addColumn('opciones', function ($data) {
                $html = '';
                $html .= '<a href="' . route('orden-venta.show', ['orden' => $data->id]) . '" class="btn btn-success btn-sm mr-2"><i class="fa fa-info-circle" aria-hidden="true"></i></a>';
                //Si el estado es 0 significa que ya converti la orden en una compra
                if ($data->estado == 0) {
                    $html .= '<a class="btn btn-secondary btn-sm mr-2 boton">Venta realizada</a>';
                    $html .= '<button class="boton btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="left" title="No se puede eliminar una orden que ya se convirtió en compra"><i class="fa fa-trash"></i></button>';
                } else {
                    $html .= '<a href="' . route('vender-desde-orden.show', ['orden' => $data->id]) . '" class="btn btn-primary btn-sm mr-2">Vender</a>';
                    $html .= '<a data-id="' . $data->id . '" class="btn btn-sm btn-danger btnEliminarOrden"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                }
                return $html;
            })
            ->rawColumns(['opciones'])
            ->make(true);
    }

    public function datatable()
    {
        $ordenes = OrdenVenta::join('clients', 'clients.id', 'orden_ventas.client_id')
            ->select('orden_ventas.*', 'clients.nombreCompleto')->get();

        return $this->devolverTabla($ordenes);
    }

    public function filtrar(Request $request)
    {
        $fechaInicio = $request['fechaInicio'];
        $fechaFin = $request['fechaFin'];
        $idcliente = $request['cliente'];

        $rules = array(
            'fechaInicio' => 'nullable|date|before:fechaFin|after:2000-01-01',
            'fechaFin' => 'required|date|before:2099-12-31|after:fechaInicio',
            'cliente' => 'nullable|required_without:fechaInicio',
        );

        $error = Validator::make($request->all(), $rules, ['cliente.required_without' => 'El campo cliente es obligatorio cuando fecha inicio no está presente o viceversa.']);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        $query = OrdenVenta::join('clients', 'clients.id', 'orden_ventas.client_id')
            ->select('orden_ventas.*', 'clients.nombreCompleto');

        if ($fechaInicio == null) {
            //Solo filtrar por cliente
            $ordenes = $query->where('client_id', $idcliente)->join('clients', 'clients.id', '=', 'orden_ventas.client_id')->get();
            //  
        } elseif ($idcliente == '') {
            //Solo debo filtrar entre fechas
            $ordenes = $query->whereBetween('fecha', [$fechaInicio, $fechaFin])->join('clients', 'clients.id', '=', 'orden_ventas.client_id')->get();
        } else {
            //Debo filtrar entre ambos
            $ordenes = $query->where('client_id', $idcliente)->whereBetween('fecha', [$fechaInicio, $fechaFin])->join('clients', 'clients.id', '=', 'orden_ventas.client_id')->get();
        }

        return $this->devolverTabla($ordenes);
    }

    public function create(Request $request)
    {
        $cliente = Client::findOrFail($request->cliente);
        $listaPrecio = PriceList::findOrFail($request->listaPrecios);

        $preciosProductos = DB::table('variantes_productos')->where('variantes_productos.borrado', 0)
            ->leftJoin('detail_price_lists', 'variantes_productos.id', '=', 'detail_price_lists.variante_id')
            ->join('products', 'products.id', 'variantes_productos.product_id')
            ->where('detail_price_lists.borrado', 0)
            ->where('price_list_id', $request->listaPrecios)
            ->orWhereNull('price_list_id')
            ->select(
                'variantes_productos.id as idVariante',
                'variantes_productos.product_id',
                'variantes_productos.tipo',
                'variantes_productos.kg',
                'variantes_productos.cantidad',
                'variantes_productos.borrado AS estaBorrado',
                'products.nombreCompleto',
                'detail_price_lists.*'
            )
            ->get();
        // return $preciosProductos;
        $productos = Product::all();
        return view('orden-venta.create')
            ->with('productos', $productos)
            ->with('cliente', $cliente)
            ->with('listaPrecio', $listaPrecio)
            ->with('preciosProductos', $preciosProductos);
    }

    public function show(OrdenVenta $orden)
    {
        $detalles = DB::table('detalle_orden_ventas')
            ->where('id_orden_venta', $orden->id)
            ->join('variantes_productos', 'variantes_productos.id', 'detalle_orden_ventas.id_variante')
            ->join('products', 'products.id', 'variantes_productos.product_id')
            ->select('detalle_orden_ventas.*', 'variantes_productos.tipo', 'variantes_productos.kg', 'products.nombreCompleto')
            ->get();

        return view('orden-venta.show')
            ->with('orden', $orden)
            ->with('detalles', $detalles);
    }

    public function store(Request $request)
    {
        // return response()->json($request->all());

        if (empty($request['idVariante'])) {
            return response()->json(['error' => 'Debe comprar algo']);
            // return redirect()->back();
        }
        //Cuento la cantidad de registros que vienen dentro del array idVariante y lo guardo en una variable
        //en este caso use idProduco, pero puede ser cualquier array que venga del request(cantidad, precioProducto o subtotal)
        $registros = count($request['idVariante']);

        //Elimino los valores duplicados
        list($variantes, $cantidades) = $this->valoresDuplicados($request['idVariante'], $request['cantidad']);
        //Comienzo la transaccion porque voy a efectuar insersiones en dos tablas distintas
        return DB::transaction(function () use ($request, $variantes, $cantidades) {

            try {
                //Primeramente inserto el la compra general
                $orden = OrdenVenta::create([
                    'client_id' => $request['idCliente'],
                    'total' => $request['total'],
                    'fecha' => $request['fecha'],
                ]);


                for ($i = 0; $i < count($variantes); $i++) {
                    $variante = $variantes[$i];
                    $cantidad = $cantidades[$i];
                    $precioProducto = $request['precioProducto'][$i];

                    DetalleOrdenVenta::create([
                        'id_orden_venta' => $orden->id,
                        'id_variante' => $variante,
                        'cantidad' => $cantidad,
                        'precio' => $precioProducto,
                    ]);
                }

                return response()->json(['success' => 'Orden de venta registrada con éxito']);
                // }
            } catch (\Exception $e) {
                //En caso de haber algun error se hace rollback
                DB::rollback();
                return response()->json(['error' => 'Sucedio un error: ' . $e->getMessage()]);
            }
        });
    }


    public function seleccionarCliente()
    {
        $listasPrecios = PriceList::all();

        session()->flash('desdeOrden', true);

        return view('vender.clientes')
            ->with('listasPrecios', $listasPrecios);
    }

    public function delete(OrdenVenta $orden)
    {
        try {
            $orden->borrado = 1;
            $orden->save();
            return response()->json(['success' => 'Orden cancelada con éxito']);
            //
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function valoresDuplicados($ids, $cantidades)
    {
        // Esta funcion surge del problema que desde el front, el usuario haya agregado una materia prima mas de una vez con distintas cantidades a comprar/ingresar respectivamente
        // Por lo que esta funcion lo que hace es recorrer el array y sumar todas las cantidades de las materias primas que estan repetidas
        // Obtengo los valores unicos dentro del array de ids de materias primas, y los valores duplicados
        $valoresUnicos = array_unique($ids);
        $valoresDuplicados = array_diff_assoc($ids, $valoresUnicos);

        $totalDuplicadas = 0;
        $suma = array();

        foreach ($valoresUnicos as $idUnico => $valorUnico) {
            //Agrego al array un registro con el id de la materia prima y la cantidad
            $suma[$valorUnico] = $cantidades[$idUnico];

            //Recorro el array de los valores que estan duplicados
            foreach ($valoresDuplicados as $idDuplicado => $valorDuplicado) {
                //Si se repiten con el array de ids unicos
                if ($valorUnico == $valorDuplicado) {
                    //Si no existe el valor dentro de larray de las sumas  
                    if (isset($suma[$valorDuplicado])) {
                        $suma[$valorUnico] += $cantidades[$idDuplicado];
                    } else {
                        $totalDuplicadas = 0;
                        $totalDuplicadas += $cantidades[$idUnico];
                        $suma[$valorUnico] = $totalDuplicadas += $cantidades[$idDuplicado];
                    }
                }
            }
        }

        //Separo los valores y los vuelvo a asignar a las variables iniciales
        $materiasPrimas = array_keys($suma);
        $cantidades = array_values($suma);

        return array($materiasPrimas, $cantidades);
    }
}
