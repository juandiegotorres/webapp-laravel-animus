<?php

namespace App\Http\Controllers;

use App\Indumentary;
use App\BalanceCompraPagos;
use App\PurchaseIndumentary;
use Illuminate\Http\Request;
use App\DetailPurchaseIndumentary;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PurchaseIndumentaryController extends Controller
{
    public function store(Request $request)
    {
        //Las variables tienen este nombre porque son un copy-paste. Y es complicado cambiar todos los nombres en todos los archivoss
        if (empty($request['insumo-servicio'])) {
            $request->session()->flash('status', ['message' => 'Debes comprar algo', 'type' => 'error']);
            return redirect()->back();
        }
        //Cuento la cantidad de registros que vienen dentro del array insumo-servicio y lo guardo en una variable
        //en este caso use insumo-servicio, pero puede ser cualquier array que venga del request(cantidad, precioUnitario o totalParcial)
        $registros = count($request['insumo-servicio']);

        //Comienzo la transaccion porque voy a efectuar insersiones en dos tablas distintas
        DB::transaction(function () use ($request, $registros) {
            try {

                //Primeramente inserto el la compra general
                $compra = PurchaseIndumentary::create([
                    'provider_id' => $request['idProveedor'],
                    'iva' => $request['iva'],
                    'total' => $request['total'],
                    'detalle' => $request['detalle'],
                    'fecha' => $request['fecha'],
                ]);

                BalanceCompraPagos::create([
                    'provider_id' => $request['idProveedor'],
                    'tipoCompra' => BalanceCompraPagos::COMPRA_INDUMETARIA,
                    'movimiento_id' => $compra->id,
                    'debe' => ($request['total'] + $request['iva']),
                    'fecha' => now(),
                ]);

                //Y ahora empiezo a recorrer el request para obtener las distintas compras que se hicieron al proveedor
                //Y las inserto en el detalle de la compra
                for ($i = 0; $i < $registros; $i++) {
                    $indumentaria = $request['insumo-servicio'][$i];
                    $cantidad = $request['cantidad'][$i];
                    $precioUnitario = $request['precioUnitario'][$i];
                    $totalParcial = $request['totalParcial'][$i];

                    DetailPurchaseIndumentary::create([
                        'purchase_indumentary_id' => $compra->id,
                        'indumentary_id' => $indumentaria,
                        'cantidad' => $cantidad,
                        'precioUnitario' => $precioUnitario,
                        'totalParcial' => $totalParcial,
                    ]);

                    Indumentary::whereId($indumentaria)->increment('cantidad', $cantidad);

                    $request->session()->flash('status', ['message' => 'Compra realizada con éxito', 'type' => 'success']);
                }
            } catch (\Exception $e) {
                //En caso de haber algun erro se hace rollback
                DB::rollback();
                $request->session()->flash('status', ['message' => 'Sucedio un error' . $e->getMessage(), 'type' => 'error']);
            }
        });
        return redirect()->route('purchase-history.index');
    }

    public function storeFromOrden(Request $request)
    {
        // return response()->json($request->all());
        //===================== VALIDACION ==========================
        $arrayPrecios = $request['precio'];
        $arrayCantidades = $request['cantidad'];

        $hayError = 0;

        for ($i = 0; $i < count($arrayPrecios); $i++) {
            if (($arrayPrecios[$i] != null && $arrayCantidades[$i] == null) || ($arrayPrecios[$i] == null && $arrayCantidades[$i] != null)) {
                $hayError += 1;
            }
        }

        if ($hayError >= 1) {
            //Error para mostrar en el sweet alert
            return response()->json(['error' => ['Si se establece una cantidad se debe establecer un precio, o viceversa.']]);
        }

        $mensajesError = [
            'cantidad.*.required' => 'Los campos cantidad no pueden estar vacios',
            'precio.*.required' => 'Los campos precio no pueden estar vacios',
            'cantidad.*.numeric' => 'El campo cantidad debe ser numerico',
            'precio.*.numeric' => 'El campo precio debe ser numerico',
            'cantidad.*.not_in' => 'La cantidad debe ser mayor que 0',
            'precio.*.not_in' => 'El precio debe ser mayor que 0'
        ];
        //Valido los campos
        $error = Validator::make($request->all(), [
            'cantidad.*' => 'numeric|required|min:0|not_in:0',
            'precio.*' => 'numeric|required|min:0|not_in:0'
        ], $mensajesError);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }
        //============== FIN VALIDACION =========================

        return DB::transaction(function () use ($request) {
            try {

                //Primeramente inserto el la compra general
                $compra = PurchaseIndumentary::create([
                    'provider_id' => $request['id_proveedor'],
                    'iva' => $request['iva'],
                    'total' => $request['total'],
                    'detalle' => $request['detalle'],
                    'fecha' => $request['fecha'],
                ]);

                BalanceCompraPagos::create([
                    'provider_id' => $request['id_proveedor'],
                    'tipoCompra' => BalanceCompraPagos::COMPRA_INDUMETARIA,
                    'movimiento_id' => $compra->id,
                    'debe' => $request['total'],
                    'fecha' => now(),
                ]);

                //Y ahora empiezo a recorrer el request para obtener las distintas compras que se hicieron al proveedor
                //Y las inserto en el detalle de la compra
                for ($i = 0; $i < count($request['precio']); $i++) {
                    $indumentaria = $request['id_objeto'][$i];
                    $cantidad = $request['cantidad'][$i];
                    $precioUnitario = $request['precio'][$i];
                    $totalParcial = ($request['precio'][$i] * $request['cantidad'][$i]);

                    DetailPurchaseIndumentary::create([
                        'purchase_indumentary_id' => $compra->id,
                        'indumentary_id' => $indumentaria,
                        'cantidad' => $cantidad,
                        'precioUnitario' => $precioUnitario,
                        'totalParcial' => $totalParcial,
                    ]);

                    Indumentary::whereId($indumentaria)->increment(
                        'cantidad',
                        $cantidad
                    );
                }

                //Cambio el estado de la orden de compra para que no se pueda borrar
                DB::table('orden_compras')->where('id', $request['id_orden'])
                    ->update(['estado' => 0]);

                return response()->json(['success' => 'Compra registrada con éxito']);
                //
            } catch (\Exception $e) {
                //En caso de haber algun error se hace rollback
                DB::rollback();
                return response()->json(['error' => [$e->getMessage()]]);
            }
        });
    }

    public function show(PurchaseIndumentary $purchaseIndumentary)
    {
        $detalleCompra = DetailPurchaseIndumentary::where('purchase_indumentary_id', $purchaseIndumentary->id)->get();
        $maestroCompra = $purchaseIndumentary;
        return view('purchase-indumentary.show')->with('detalleCompra', $detalleCompra)->with('maestroCompra', $maestroCompra);
    }
}
