<?php

namespace App\Http\Controllers;

use App\Provider;
use App\Liquidacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class LiquidacionController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:liquidacion']);
    }

    public function index()
    {
        return view('liquidaciones.index');
    }

    public function datatable()
    {
        $liquidaciones = Liquidacion::join('providers', 'providers.id', 'liquidaciones.provider_id')->select('liquidaciones.*', 'providers.nombreCompleto');

        return DataTables::of($liquidaciones)
            ->addColumn('nombreProveedor', '{{$nombreCompleto}}')
            ->editColumn('fechaDeLiquidacion', '{{ date("d-m-Y", strtotime($fechaDeLiquidacion)) }}')
            ->addColumn('opciones', '<a href="{{ route(\'liquidaciones.show\', [\'liquidacion\' => $id])}}" class="btn btn-success btn-sm mr-1"><i class="fa fa-info-circle" aria-hidden="true"></i></a> 
            <a href="{{ route(\'liquidacion.pdf\', [\'liquidacion\' => $id])}}" class="btn btn-warning btn-sm mr-1"><i class="fa fa-print" aria-hidden="true"></i></a>
            <a href="{{ route(\'liquidaciones.edit\', [\'liquidacion\' => $id])}}" class="btn btn-primary btn-sm mr-1"><i class="fa fa-pen" aria-hidden="true"></i></a>
            <a data-id={{$id}} class="btn btn-danger btn-sm btnEliminarLiquidacion"><i class="fa fa-trash" aria-hidden="true"></i></a>')
            ->rawColumns(['opciones'])
            ->make(true);
    }

    public function create()
    {
        $proveedores = Provider::materiaPrima()->get();
        return view('liquidaciones.create')->with('proveedores', $proveedores);
    }

    public function store(Request $request)
    {
        $request->validate([
            'proveedor' => 'required',
            'fecha' => 'date|required|date_format:Y-m-d|before:2099-12-31|after:2000-01-01',
            'cantidad_cuotas' => 'required|integer|max:99|min:1',
            'observaciones' => 'nullable'
        ]);
        try {
            Liquidacion::create([
                'provider_id' => $request['proveedor'],
                'fechaDeLiquidacion' => $request['fecha'],
                'cantidad_cuotas' => $request['cantidad_cuotas'],
                'observaciones' => $request['observaciones']
            ]);

            $request->session()->flash('status', 'Liquidacion creada con éxito');
            //
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('status', $e->getMessage());
        }

        return redirect()->action('LiquidacionController@index');
    }

    public function show(Liquidacion $liquidacion)
    {
        //Para poder saber las compras y los pagos de la liquidacion debo filtrar entre dos fechas. 
        //Por lo que traigo el registro de la liquidacion a mostrar y un registro anterior tambien para seber la fecha de inicio por la cual empezar a filtrar
        $fechas = Liquidacion::where('provider_id', $liquidacion->provider_id)
            ->where('fechaDeLiquidacion', '<=', $liquidacion->fechaDeLiquidacion)
            ->orderBy('fechaDeLiquidacion', 'desc')
            ->take(2)
            ->get();

        $queryCompras = $compras = DB::table('purchase_raw_materials')->where('purchase_raw_materials.borrado', 0)
            ->where('provider_id', $liquidacion->provider_id)
            ->join('detail_purchase_raw_materials', 'detail_purchase_raw_materials.purchase_raw_material_id', 'purchase_raw_materials.id')
            ->join('raw_materials', 'raw_materials.id', 'detail_purchase_raw_materials.raw_material_id')
            ->select(
                'purchase_raw_materials.iva',
                'purchase_raw_materials.total',
                'purchase_raw_materials.fecha',
                'detail_purchase_raw_materials.precioUnitario',
                'detail_purchase_raw_materials.totalParcial',
                'detail_purchase_raw_materials.cantidad',
                'raw_materials.fruta',
                'raw_materials.variedad'
            );

        $queryPagos = DB::table('pagos')->where('borrado', 0)->where('provider_id', $liquidacion->provider_id);
        $queryIva = DB::table('purchase_raw_materials')->where('borrado', 0)->select(DB::raw('sum(iva) AS suma'));
        /*Si fechas tiene dos registros significa que hay una fecha de inicio y una fecha de fin, sino tengo que traer todas las compras y pagos
        que sean menor que la fecha de la liquidacion actual*/
        if ($fechas->count() == 2) {

            $compras = $queryCompras->whereBetween(DB::raw('date(purchase_raw_materials.fecha)'), [$fechas[1]->fechaDeLiquidacion, $liquidacion->fechaDeLiquidacion])->get();
            $iva = $queryIva->whereBetween(DB::raw('date(fecha)'), [$fechas[1]->fechaDeLiquidacion, $liquidacion->fechaDeLiquidacion])->get();
            $pagos = $queryPagos->whereBetween('pagos.fecha', [$fechas[1]->fechaDeLiquidacion, $liquidacion->fechaDeLiquidacion])->get();
        } else {
            $compras = $queryCompras->where(DB::raw('date(purchase_raw_materials.fecha)'), '<=', $liquidacion->fechaDeLiquidacion)->get();
            $iva = $queryIva->where(DB::raw('date(purchase_raw_materials.fecha)'), '<=', $liquidacion->fechaDeLiquidacion)->get();
            $pagos = $queryPagos->where('pagos.fecha', '<=', $liquidacion->fechaDeLiquidacion)->get();
        }
        $totalIvaCompras = $iva->first()->suma;


        return view('liquidaciones.show')
            ->with('totalIvaCompras', $totalIvaCompras)
            ->with('pagos', $pagos)
            ->with('compras', $compras)
            ->with('fechas', $fechas);
    }

    public function edit(Liquidacion $liquidacion)
    {
        $proveedores = Provider::materiaPrima()->get();

        return view('liquidaciones.edit')
            ->with('proveedores', $proveedores)
            ->with('liquidacion', $liquidacion);
    }

    public function update(Request $request, Liquidacion $liquidacion)
    {
        try {
            $request->validate([
                'proveedor' => 'required',
                'fecha' => 'date|required|date_format:Y-m-d|before:2099-12-31|after:2000-01-01',
                'cantidad_cuotas' => 'required|integer|max:99|min:1',
                'observaciones' => 'nullable'
            ]);

            if ($request->has('proveedor')) {
                $liquidacion->provider_id = $request->proveedor;
            }
            if ($request->has('fecha')) {
                $liquidacion->fechaDeLiquidacion = $request->fecha;
            }
            if ($request->has('cantidad_cuotas')) {
                $liquidacion->cantidad_cuotas = $request->cantidad_cuotas;
            }
            if ($request->has('observaciones')) {
                $liquidacion->observaciones = $request->observaciones;
            }

            $liquidacion->save();

            $request->session()->flash('status', 'Liquidacion modificada con éxito');
            //
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('status', $e->getMessage());
        }

        return redirect()->action('LiquidacionController@index');
    }

    public function delete($id)
    {
        try {
            DB::table('liquidaciones')->where('liquidaciones.id', $id)->update(['borrado' => 1]);
            return response()->json(['success' => 'Liquidacion dada de baja']);
            //
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function calcularBalance(Request $request)
    {
        $fecha = $request['fecha'];
        $idproveedor = $request['proveedor'];

        $fechas = Liquidacion::where('provider_id', $idproveedor)
            ->where('fechaDeLiquidacion', '<=', $fecha)
            ->orderBy('fechaDeLiquidacion', 'desc')
            ->take(2)
            ->get();

        $queryCompras =  DB::table('purchase_raw_materials')->where('purchase_raw_materials.borrado', 0)
            ->where('provider_id', $idproveedor);

        $queryPagos = DB::table('pagos')->where('borrado', 0)->where('provider_id', $idproveedor);

        /*Si fechas tiene dos registros significa que hay una fecha de inicio y una fecha de fin, sino tengo que traer todas las compras y pagos
        que sean menor que la fecha de la liquidacion actual*/
        if ($fechas->count() == 2) {

            $compras = $queryCompras->whereBetween(DB::raw('date(purchase_raw_materials.fecha)'), [$fechas[1]->fechaDeLiquidacion, $fecha])->get();

            $pagos = $queryPagos->whereBetween('pagos.fecha', [$fechas[1]->fechaDeLiquidacion, $fecha])->get();
        } else {
            $compras = $queryCompras->where(DB::raw('date(purchase_raw_materials.fecha)'), '<=', $fecha)->get();

            $pagos = $queryPagos->where('pagos.fecha', '<=', $fecha)->get();
        }
        $totalCompras = $compras->sum('total');
        $totalPagos = $pagos->sum('montoTotal');

        return response()->json([$totalCompras - $totalPagos]);
    }
}
