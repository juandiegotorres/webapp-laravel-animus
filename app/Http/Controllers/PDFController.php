<?php

namespace App\Http\Controllers;

use App\Pago;
use DateTime;
use App\Cobro;
use App\Venta;
use App\Salary;
use App\Factura;
use App\PriceList;
use App\Descarozado;
use App\SalaryPayment;
use App\RawMaterialEntry;
use App\BalanceCompraPagos;
use App\BalanceVentasCobros;
use App\PurchaseIndumentary;
use App\PurchaseRawMaterial;
use App\RawMaterialMovement;
use Illuminate\Http\Request;
use App\DetailRawMaterialEntry;
use App\PurchaseSupplieService;
use App\IndumentaryDistribution;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\DetailIndumentaryDistribution;
use App\Liquidacion;
use Illuminate\Support\Facades\Validator;
// use mikehaertl\wkhtmlto\Pdf;

class PDFController extends Controller
{

    public function pdfSueldo(Salary $sueldo)
    {
        $empleado = $sueldo->empleado;
        $pdf = PDF::loadView('pdf.sueldo', compact('sueldo', 'empleado'));
        $pdf->setPaper('A5', 'landscape');
        return $pdf->stream('sueldo-' . now() . '.pdf');
    }

    public function pdfOrdenPedido()
    {
        $pdf = PDF::loadView('pdf.orden-pedido');
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('orden-pedido-' . now() . '.pdf');
    }

    public function pdfOrdenPago()
    {
        $pdf = PDF::loadView('pdf.orden-pago');
        $pdf->setPaper('A5', 'landscape');
        return $pdf->stream('orden-pago-' . now() . '.pdf');
    }

    public function pdfLiquidacion(Liquidacion $liquidacion)
    {
        //Para poder saber las compras y los pagos de la liquidacion debo filtrar entre dos fechas. 
        //Por lo que traigo el registro de la liquidacion a mostrar y un registro anterior tambien para seber la fecha de inicio por la cual empezar a filtrar
        $liquidaciones = Liquidacion::where('provider_id', $liquidacion->provider_id)
            ->where('fechaDeLiquidacion', '<=', $liquidacion->fechaDeLiquidacion)
            ->orderBy('fechaDeLiquidacion', 'desc')
            ->take(2)
            ->get();

        $queryCompras = $compras = DB::table('purchase_raw_materials')->where('purchase_raw_materials.borrado', 0)
            ->where('provider_id', $liquidacion->provider_id)
            ->join('detail_purchase_raw_materials', 'detail_purchase_raw_materials.purchase_raw_material_id', 'purchase_raw_materials.id')
            ->join('raw_materials', 'raw_materials.id', 'detail_purchase_raw_materials.raw_material_id')
            ->select(
                'purchase_raw_materials.id',
                'purchase_raw_materials.iva',
                'purchase_raw_materials.total',
                'purchase_raw_materials.fecha',
                'detail_purchase_raw_materials.precioUnitario',
                'detail_purchase_raw_materials.totalParcial',
                'detail_purchase_raw_materials.cantidad',
                'raw_materials.fruta',
                'raw_materials.variedad'
            );

        $queryPagos = DB::table('pagos')->where('borrado', 0)->where('provider_id', $liquidacion->provider_id);
        $queryIva = DB::table('purchase_raw_materials')
            ->where('provider_id', $liquidacion->provider_id)
            ->where('borrado', 0)
            ->where('provider_id', $liquidacion->provider_id)->select(DB::raw('sum(iva) AS suma'));

        /*Si fechas tiene dos registros significa que hay una fecha de inicio y una fecha de fin, sino tengo que traer todas las compras y pagos
        que sean menor que la fecha de la liquidacion actual*/
        if ($liquidaciones->count() == 2) {

            $compras = $queryCompras->whereBetween(DB::raw('date(purchase_raw_materials.fecha)'), [$liquidaciones[1]->fechaDeLiquidacion, $liquidacion->fechaDeLiquidacion])->get();
            $iva = $queryIva->whereBetween(DB::raw('date(fecha)'), [$liquidaciones[1]->fechaDeLiquidacion, $liquidacion->fechaDeLiquidacion])->get();
            $totalPagos = $queryPagos->whereBetween('pagos.fecha', [$liquidaciones[1]->fechaDeLiquidacion, $liquidacion->fechaDeLiquidacion])->get()->sum('montoTotal');
        } else {
            $compras = $queryCompras->where(DB::raw('date(purchase_raw_materials.fecha)'), '<=', $liquidacion->fechaDeLiquidacion)->get();
            $iva = $queryIva->where(DB::raw('date(purchase_raw_materials.fecha)'), '<=', $liquidacion->fechaDeLiquidacion)->get();
            $totalPagos = $queryPagos->where('pagos.fecha', '<=', $liquidacion->fechaDeLiquidacion)
                ->sum('montoTotal');
        }

        $totalCompras = $compras->sum('totalParcial');
        $totalIva = $iva->first()->suma;

        $pdf = PDF::loadView('pdf.liquidacion', compact('compras', 'liquidaciones', 'totalCompras', 'totalPagos', 'totalIva'));

        $pdf->setPaper('A5', 'landscape')->setWarnings(false);
        return $pdf->stream('liquidacion' . now() . '.pdf');
    }

    public function pdfDescarozado($descarozado)
    {

        $detalleDescarozado = DB::table('entrega_descarozado')->where('entrega_descarozado.id', $descarozado)
            ->join('raw_materials', 'raw_materials.id', '=', 'entrega_descarozado.raw_material_id')
            ->join('employees', 'employees.id', '=', 'entrega_descarozado.employee_id')
            ->join('charges', 'charges.id', '=', 'employees.charge_id')
            ->select('entrega_descarozado.*', 'employees.nombreCompleto', 'employees.dni', 'charges.nombre AS nombreCargo', 'raw_materials.fruta', 'raw_materials.variedad')
            ->get();

        $pdf = PDF::loadView('pdf.descarozado', compact('detalleDescarozado'));
        $pdf->setPaper('A5', 'landscape');
        return $pdf->stream('descarozado-' . now() . '.pdf');
    }

    function esFechaValida($date, $format = 'Y-m-d')
    {
        $dt = DateTime::createFromFormat($format, $date);
        return $dt && $dt->format($format) === $date;
    }

    public function pdfBalanceImpositivo($mes, $desde, $hasta, $verPositivo)
    {

        if ($mes >= 1 && $mes <= 12) {

            $balanceTotal = DB::SELECT("SELECT (SELECT ifnull(sum(totalNeto), 0) from facturas WHERE month(fechaEmision) = ? and emitida_recibida = 'emitida' and borrado = 0) - (SELECT ifnull(sum(totalNeto), 0) from facturas WHERE month(fechaEmision) = ? and emitida_recibida = 'recibida' and borrado = 0) as 'balanceTotal'", array($mes, $mes));

            $balanceIva = DB::SELECT("SELECT (SELECT ifnull(sum(IVA), 0) from facturas WHERE month(fechaEmision) = ? and emitida_recibida = 'emitida' and borrado = 0) - (SELECT ifnull(sum(IVA), 0) from facturas WHERE month(fechaEmision) = ? and emitida_recibida = 'recibida' and borrado = 0) as 'balanceIva'", array($mes, $mes));

            $facturas = Factura::whereMonth('fechaEmision', '=', $mes)->get();

            if ($verPositivo == 'si') {
                $balanceTotal = $balanceTotal[0]->balanceTotal * (-1);
                $balanceIva = $balanceIva[0]->balanceIva * (-1);
            }

            $mesPeriodo = array(
                'mes' => $mes
            );
        } else if ($this->esFechaValida($desde) && $this->esFechaValida($hasta)) {

            $balanceTotal = DB::SELECT("SELECT (SELECT ifnull(sum(totalNeto), 0) from facturas WHERE fechaEmision BETWEEN ? and ? and emitida_recibida = 'emitida' and borrado = 0) - (SELECT ifnull(sum(totalNeto), 0) from facturas WHERE fechaEmision BETWEEN ? and ? and emitida_recibida = 'recibida' and borrado = 0) as 'balanceTotal'", array($desde, $hasta, $desde, $hasta));

            $balanceIva = DB::SELECT("SELECT (SELECT ifnull(sum(IVA), 0) from facturas WHERE fechaEmision BETWEEN ? and ? and emitida_recibida = 'emitida' and borrado = 0) - (SELECT ifnull(sum(IVA), 0) from facturas WHERE fechaEmision BETWEEN ? and ? and emitida_recibida = 'recibida' and borrado = 0) as 'balanceIva'", array($desde, $hasta, $desde, $hasta));

            $facturas = Factura::whereBetween('fechaEmision', [$desde, $hasta])->get();

            if ($verPositivo == 'si') {
                $balanceTotal = $balanceTotal[0]->balanceTotal * (-1);
                $balanceIva = $balanceIva[0]->balanceIva * (-1);
            }

            $mesPeriodo = array(
                'desde' => $desde,
                'hasta' => $hasta
            );
        } else {
            $status = 'Datos incorrectos';
            return view('error', compact('status'));
        }

        $pdf = PDF::loadView('pdf.balance-impositivo', compact('facturas', 'balanceTotal', 'balanceIva', 'mesPeriodo'));
        $pdf->setPaper('A4');
        return $pdf->stream('balance-impositivo-' . now() . '.pdf');
        // return response()->json($mes . ' ' . $desde . ' ' . $hasta);
    }

    public function pdfBalanceVentasCobros($idcliente, $fechaFin, $fechaInicio)
    {
        try {
            $query = BalanceVentasCobros::join('clients', 'balance_ventas_cobros.client_id', '=', 'clients.id')
                ->select('balance_ventas_cobros.*', 'clients.nombreCompleto', 'clients.telefono');

            if ($fechaInicio == ':fInc') {
                //Solo filtrar por proveedor
                $balances = $query->where('client_id', $idcliente)->get();

                //
            } else {
                //Debo filtrar entre ambos
                $balances = $query->whereBetween('fecha', [$fechaInicio, $fechaFin])->where('client_id', $idcliente)->get();
            }

            if ($balances->count() == 0) {
                session()->flash('status', 'No hay datos para imprimir');
                $status = 'No hay datos para imprimir';
                return view('error', compact('status'));
            }

            $proveedor = array(
                'proveedor' => $balances[0]['nombreCompleto'],
                'telefono' => $balances[0]['telefono'],
            );

            $balancesClean = array();
            $total = 0;
            foreach ($balances as $balance) {
                $total += floatval($balance->haber) - floatval($balance->debe);
                switch ($balance->tipoMovimiento) {
                    case BalanceVentasCobros::VENTA;
                        $tipo = 'Venta';
                        break;
                    case BalanceVentasCobros::COBRO;
                        $tipo = 'Cobro';
                        break;
                    default:
                        $tipo = "No especificado";
                        break;
                }

                $entrada = array(
                    'fecha' => $balance->fecha,
                    'tipoMovimiento' => $tipo,
                    'haber' => $balance->haber,
                    'debe' => $balance->debe,
                    'total' => $total,
                );

                array_push($balancesClean, $entrada);
            }
            $pdf = PDF::loadView('pdf.balance-ventas-cobros', compact('balancesClean', 'proveedor'));
            $pdf->setPaper('A4');
            return $pdf->stream('balance-ventas-cobros-' . now() . '.pdf');
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function pdfBalanceSueldo($idempleado, $fechaFin, $fechaInicio)
    {
        try {
            $query = DB::table('employee_salary_balances')
                ->join('employees', 'employees.id', '=', 'employee_salary_balances.employee_id')
                ->where('employee_salary_balances.borrado', 0)
                ->select('employee_salary_balances.*', 'employees.nombreCompleto', 'employees.telefono');

            if ($fechaInicio == ':fInc') {
                //Solo filtrar por proveedor
                $balances = $query->where('employee_id', $idempleado)->orderBy('created_at', 'DESC')->get();

                //
            } else {
                //Debo filtrar entre ambos
                $balances = $query->whereBetween('fecha', [$fechaInicio, $fechaFin])->where('employee_id', $idempleado)->orderBy('fecha', 'DESC')->get();
            }
            // return $balances;
            if ($balances->count() == 0) {
                session()->flash('status', 'No hay datos para imprimir');
                $status = 'No hay datos para imprimir';
                return view('error', compact('status'));
            }

            $empleado = array(
                'nombre' => $balances[0]->nombreCompleto,
                'telefono' => $balances[0]->telefono,
            );

            $balancesClean = array();
            $total = 0;
            foreach ($balances as $balance) {
                $total += floatval($balance->debe) - floatval($balance->haber);
                switch ($balance->tipoMovimiento) {
                    case 'H';
                        $tipo = 'Sueldo';
                        break;
                    case 'D';
                        $tipo = 'Pago de sueldo';
                        break;
                    default:
                        $tipo = "No especificado";
                        break;
                }

                $entrada = array(
                    'fecha' => $balance->fecha,
                    'tipoMovimiento' => $tipo,
                    'haber' => $balance->haber,
                    'debe' => $balance->debe,
                    'total' => $total,
                );

                array_push($balancesClean, $entrada);
            }
            $pdf = PDF::loadView('pdf.balance-sueldo', compact('balancesClean', 'empleado'));
            $pdf->setPaper('A4');
            return $pdf->stream('balance-sueldo-' . now() . '.pdf');
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }


    public function pdfBalanceComprasPagos($idproveedor, $fechaFin, $fechaInicio)
    {
        try {
            $query = BalanceCompraPagos::join('providers', 'balance_compras_pagos.provider_id', '=', 'providers.id')
                ->select('balance_compras_pagos.*', 'providers.nombreCompleto', 'providers.telefono');
            if ($fechaInicio == ':fInc') {
                //Solo filtrar por proveedor
                $balances = $query->where('provider_id', $idproveedor)->get();

                //
            } else {
                //Debo filtrar entre ambos
                $balances = $query->whereBetween('fecha', [$fechaInicio, $fechaFin])->where('provider_id', $idproveedor)->get();
            }

            if ($balances->count() == 0) {
                session()->flash('status', 'No hay datos para imprimir');
                $status = 'No hay datos para imprimir';
                return view('error', compact('status'));
            }

            $proveedor = array(
                'proveedor' => $balances[0]['nombreCompleto'],
                'telefono' => $balances[0]['telefono'],
            );

            $balancesClean = array();
            $total = 0;
            foreach ($balances as $balance) {
                $total += floatval($balance->haber) - floatval($balance->debe);
                switch ($balance->tipoCompra) {
                    case BalanceCompraPagos::COMPRA_INDUMETARIA;
                        $tipo = 'Compra de indumentaria';
                        break;
                    case BalanceCompraPagos::COMPRA_INSUMO;
                        $tipo = 'Compra de insumo';
                        break;
                    case BalanceCompraPagos::COMPRA_MATERIA_PRIMA;
                        $tipo = 'Compra de materia prima';
                        break;
                    case BalanceCompraPagos::COMPRA_SERVICIO;
                        $tipo = 'Compra de servicio';
                        break;
                    case BalanceCompraPagos::PAGO_A_PROVEEDOR;
                        $tipo = 'Pago a proveedor';
                        break;
                    default:
                        $tipo = "No especificado";
                        break;
                }

                $entrada = array(
                    'fecha' => $balance->fecha,
                    'tipoMovimiento' => $tipo,
                    'haber' => $balance->haber,
                    'debe' => $balance->debe,
                    'total' => $total,
                );

                array_push($balancesClean, $entrada);
            }
            $pdf = PDF::loadView('pdf.balance-compras-pagos', compact('balancesClean', 'proveedor'));
            $pdf->setPaper('A4');
            return $pdf->stream('balance-compras-pagos-' . now() . '.pdf');
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function pdfListaPrecio($priceList)
    {

        $detalleLista = DB::table('price_lists')
            ->where('price_lists.id', $priceList)
            ->join('detail_price_lists', 'detail_price_lists.price_list_id', 'price_lists.id')
            ->join('variantes_productos', 'variantes_productos.id', 'detail_price_lists.variante_id')
            ->join('products', 'products.id', 'variantes_productos.product_id')
            ->select('price_lists.nombre', 'fechaVigencia', 'detail_price_lists.*', 'products.nombreCompleto AS nombreProducto', 'variantes_productos.tipo', 'variantes_productos.kg')
            ->get();


        $pdf = PDF::loadView('pdf.lista-precio-pdf', compact('detalleLista'));
        $pdf->setPaper('A4');
        return $pdf->stream('lista-precio-' . now() . '.pdf');
    }

    public function pdfCompra($idcompra, $tipoCompra)
    {
        switch ($tipoCompra) {
            case 'MP':
                $compra = PurchaseRawMaterial::whereId($idcompra)->get();
                $compra->tipoCompra = 'MP';
                $proveedor = $compra->first()->proveedor;
                $detalleCompra = $compra->first()->detalleCompra;
                break;
            case 'IS':
                $compra = PurchaseSupplieService::find($idcompra)->where('tipoCompra', PurchaseSupplieService::COMPRA_INSUMO)->get();
                $compra->tipoCompra = 'IS';
                $proveedor = $compra->first()->proveedor;
                $detalleCompra = $compra->first()->detalleCompra;
                break;
            case 'SV':
                $compra = PurchaseSupplieService::find($idcompra)->where('tipoCompra', PurchaseSupplieService::COMPRA_SERVICIO)->get();
                $compra->tipoCompra = 'SV';
                $proveedor = $compra->first()->proveedor;
                $detalleCompra = $compra->first()->detalleCompra;
                break;
            case 'ID':
                $compra = PurchaseIndumentary::whereId($idcompra)->get();
                $compra->tipoCompra = 'ID';
                $proveedor = $compra->first()->proveedor;
                $detalleCompra = $compra->first()->detalleCompra;
                break;
            default:
                abort(404);
                break;
        }

        $pdf = PDF::loadView('pdf.compra-pdf', compact('compra', 'detalleCompra', 'proveedor'));
        return $pdf->stream('compra-pdf-' . now() . '.pdf');
    }

    public function pdfEntregaDescarozado($descarozado)
    {

        $detalleDescarozado = DB::table('entrega_descarozado')->where('entrega_descarozado.id', $descarozado)
            ->join('raw_materials', 'raw_materials.id', '=', 'entrega_descarozado.raw_material_id')
            ->join('employees', 'employees.id', '=', 'entrega_descarozado.employee_id')
            ->join('charges', 'charges.id', '=', 'employees.charge_id')
            ->select('entrega_descarozado.*', 'employees.nombreCompleto', 'employees.dni', 'charges.nombre AS nombreCargo', 'raw_materials.fruta', 'raw_materials.variedad')
            ->get();

        $pdf = PDF::loadView('pdf.entrega-descarozado-pdf', compact('detalleDescarozado'));

        return $pdf->stream('entrega-descarozado-' . now() . '.pdf');
    }

    public function pdfEntregaIndumentaria(IndumentaryDistribution $entrega)
    {
        //Entrega indumentaria
        $empleado = $entrega->empleado;
        $detalleEntrega = DetailIndumentaryDistribution::where('indumentary_distributions_id', 1)
            ->join('indumentaries', 'indumentaries.id', '=', 'detail_indumentary_distributions.indumentary_id')
            ->select('detail_indumentary_distributions.*', 'indumentaries.nombre', 'indumentaries.tipo_modelo', 'indumentaries.modelo')
            ->get();
        // return $entrega->detalle;
        $pdf = PDF::loadView('pdf.entrega-indumentaria', compact('detalleEntrega', 'empleado', 'entrega'));
        $pdf->setPaper('A5', 'landscape');
        return $pdf->stream('entrega-indumentaria-' . now() . '.pdf');
    }

    public function pdfVenta(Venta $venta)
    {
        //Venta
        $cliente = $venta->cliente;
        $detalleVenta = $venta->detalleVenta;
        $pdf = PDF::loadView('pdf.orden-pedido', compact('venta', 'cliente', 'detalleVenta'));
        $pdf->setPaper('A5', 'landscape');
        return $pdf->stream('orden-pedido-' . now() . '.pdf');
    }

    public function pdfPagoSueldo(SalaryPayment $pagoSueldo)
    {
        //Pago sueldo
        $empleado = $pagoSueldo->empleado;
        $pdf = PDF::loadView('pdf.pago-sueldo', compact('pagoSueldo', 'empleado'));
        $pdf->setPaper('A5', 'landscape');
        return $pdf->stream('pago-sueldo-' . now() . '.pdf');
    }

    public function pdfReciboPagoProductor(Pago $pago)
    {
        //Recibo pago productor
        $balance = DB::SELECT("SELECT (SELECT ifnull(sum(haber), 0) from  balance_compras_pagos WHERE provider_id = ? and borrado = 0) - (SELECT ifnull(sum(debe), 0) from  balance_compras_pagos WHERE provider_id = ? and borrado = 0) as 'balance'", array($pago->provider_id, $pago->provider_id));
        $proveedor = $pago->proveedor;
        $detallePago = DB::table('detalle_pagos')->where('pago_id', $pago->id)
            ->join('metodos_pago', 'metodos_pago.id', 'detalle_pagos.metodo_pago_id')
            ->leftJoin('cheques', 'cheques.id', 'detalle_pagos.cheque_id')
            ->select('metodos_pago.nombre AS metodoPago', 'cheques.nroCheque', 'detalle_pagos.monto')
            ->get();
        $pdf = PDF::loadView('pdf.recibo-pago', compact('pago', 'proveedor', 'detallePago', 'balance'));
        $pdf->setPaper('A5', 'landscape');
        return $pdf->stream('recibo-pago-' . now() . '.pdf');
    }

    public function pdfReciboPagoCobro(Cobro $cobro)
    {
        //Recibo pago productor
        $balance = DB::SELECT("SELECT (SELECT ifnull(sum(haber), 0) from  balance_ventas_cobros WHERE client_id = ? and borrado = 0) - (SELECT ifnull(sum(debe), 0) from  balance_ventas_cobros WHERE client_id = ? and borrado = 0) as 'balance'", array($cobro->client_id, $cobro->client_id));
        $cliente = $cobro->cliente;
        $detalleCobro = DB::table('detalle_cobros')->where('cobro_id', $cobro->id)
            ->join('metodos_pago', 'metodos_pago.id', 'detalle_cobros.metodo_pago_id')
            ->leftJoin('cheques', 'cheques.id', 'detalle_cobros.cheque_id')
            ->select('metodos_pago.nombre AS metodoPago', 'cheques.nroCheque', 'detalle_cobros.monto')
            ->get();
        $pdf = PDF::loadView('pdf.recibo-pago-cobro', compact('cobro', 'cliente', 'detalleCobro', 'balance'));
        $pdf->setPaper('A5', 'landscape');
        return $pdf->stream('recibo-pago-cobro-' . now() . '.pdf');
    }

    public function pdfIngresoFruta(RawMaterialEntry $ingreso)
    {
        //Ingreso Fruta
        $proveedor = $ingreso->proveedor;
        $envases = DetailRawMaterialEntry::where('raw_material_entries_id', $ingreso->id)
            ->join('envases', 'envases.id', 'detail_raw_material_entries.envase_id')
            ->take(2)->get();
        $historialDescarte = RawMaterialMovement::join('raw_material_entries', 'raw_material_movements.id_ingreso', '=', 'raw_material_entries.id')
            ->join('raw_materials', 'raw_materials.id', 'raw_material_entries.raw_material_id')
            ->where('id_ingreso', '=', $ingreso->id)
            ->select('raw_material_movements.*', 'raw_materials.fruta', 'raw_materials.variedad')->get();

        // return $envases;

        $pdf = PDF::loadView('pdf.ingreso-fruta', compact('ingreso', 'proveedor', 'envases', 'historialDescarte'));
        $pdf->setPaper('A5', 'landscape');
        return $pdf->stream('ingreso-fruta-' . now() . '.pdf');
    }
}
