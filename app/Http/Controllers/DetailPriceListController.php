<?php

namespace App\Http\Controllers;

use App\Product;
use App\PriceList;
use App\DetailPriceList;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class DetailPriceListController extends Controller
{
    public function index(PriceList $priceList)
    {
        if (!auth()->user()->can('detail-price-lists.edit')) {
            abort(403, 'Acción no autorizada.');
        }

        $listaPrecios = $priceList;
        $productos = Product::all();

        return view('detail-prices-list.index')
            ->with('productos', $productos)
            ->with('listaPrecios', $listaPrecios);
    }

    public function datatable($priceList)
    {
        if (!auth()->user()->can('detail-price-lists.edit')) {
            abort(403, 'Acción no autorizada.');
        }

        $precios = DetailPriceList::join('variantes_productos', 'variante_id', '=', 'variantes_productos.id')
            ->join('products', 'products.id', 'variantes_productos.product_id')
            ->select('detail_price_lists.*', 'products.nombreCompleto', 'variantes_productos.tipo', 'variantes_productos.kg')
            ->where('price_list_id', $priceList)
            ->get();

        return DataTables::of($precios)
            ->addColumn('nombreProducto', '{{ $nombreCompleto }}')
            ->addColumn('precioKg', '{{ $precioKg }}')
            ->addColumn('precioCaja', '{{ $precioCaja }}')
            ->addColumn('created_at', '{{ $created_at }}')
            ->addColumn('variante', function ($precios) {
                switch ($precios->tipo) {
                    case '0':
                        return 'Caja, ' . $precios->kg . ' kg(s)';
                        break;
                    case '1':
                        return 'Bolsa, ' . $precios->kg . ' kg(s)';
                        break;
                    default:
                        break;
                }
            })
            ->editColumn('opciones', '<div class="d-flex justify-content-end">
                        <a data-id="{{ $id }}" data-nombre="{{ $nombreProducto }}" data-preciokg="{{ $precioKg }}" data-precio_caja="{{ $precioCaja }}" 
                        data-toggle="modal" data-target="#modalModificarPrecio"> 
                            <button class="btn btn-primary btn-sm mr-2">
                                Modificar precio <i class="fa fa-pen"></i>
                            </button>
                        </a>
                        <button  data-id="{{ $id }}" class="btn btnEliminarPrecio btn-danger btn-sm mr-2">
                            <i class="fa fa-trash"></i>
                        </button>
                  </div> 
                ')
            ->rawColumns(['opciones'])
            ->toJson();
    }

    public function obtenerVariantes($idproducto)
    {
        $variantes = DB::table('variantes_productos')->where('product_id', $idproducto)->get();
        return response()->json($variantes);
    }

    public function store(Request $request)
    {
        // return response()->json($request->all());
        if (!auth()->user()->can('detail-price-lists.edit')) {
            abort(403, 'Acción no autorizada.');
        }

        $rules = array(
            'price_list_id' => 'required',
            'variante_id' => 'required',
            'precioKg' => 'nullable|numeric|digits_between:1,8',
            'precioCaja' => 'nullable|numeric|digits_between:1,8',
        );


        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        $precioUnico = Validator::make($request->all(), array('variante_id' => [
            'required',
            Rule::unique('detail_price_lists')->where(function ($query) use ($request) {
                return $query->where('price_list_id', $request->price_list_id)
                    ->where('variante_id', $request->variante_id);
            }),
        ],));


        if ($precioUnico->fails()) {
            //Si el precio esta repetido significa que lo tengo que actualiar, para eso recupero el id del detalle de precio
            //de la tabla y lo mando al front para poder hacer el update
            $idDetallePrecio = DetailPriceList::where('price_list_id', $request->price_list_id)
                ->where('variante_id', $request->variante_id)
                ->select('id')
                ->get();

            return response()->json(['repetido' => 'Este articulo ya tiene precio', 'idDetalle' => $idDetallePrecio]);
        }

        return DB::transaction(function () use ($request) {
            try {

                $request->precioCaja == null ? $request->precioCaja = 0 : $request->precioCaja;
                $request->precioKg == null ? $request->precioKg = 0 : $request->precioKg;
                DetailPriceList::create($request->all());

                return response()->json(['success' => 'Precio agregado con éxito']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => [$e->getMessage()]]);
            }
        });
    }


    public function update(Request $request, DetailPriceList $detailPriceList)
    {
        if (!auth()->user()->can('detail-price-lists.edit')) {
            abort(403, 'Acción no autorizada.');
        }

        $rules = array(
            'precioKg' => 'nullable|numeric|digits_between:1,6',
            'precioCaja' => 'nullable|numeric|digits_between:1,6',
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        if ($request->has('precioKg')) {
            $detailPriceList->precioKg = $request->precioKg;
        }
        if ($request->has('precioCaja')) {
            $detailPriceList->precioCaja = $request->precioCaja;
        }
        if ($request->has('kgCaja')) {
            $detailPriceList->kgCaja = $request->kgCaja;
        }

        if (!$detailPriceList->isDirty()) {

            return response()->json(['error' => ['Se debe especificar al menos un valor diferente para actualizar']]);
        }

        $detailPriceList->save();

        return response()->json(['success' => 'Precio actualizado con éxito']);
    }

    public function destroy($id)
    {
        if (!auth()->user()->can('detail-price-lists.edit')) {
            abort(403, 'Acción no autorizada.');
        }

        $precio = DetailPriceList::findOrFail($id);
        $precio->delete();
    }
}
