<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(['role:administrador'], ['except' => ['index']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (!auth()->user()->hasRole('administrador')) {
            return redirect()->route('ingresoMP.index');
        }
        $totalVentas = DB::table('ventas')->where('borrado', 0)->sum('total');
        $totalCobros = DB::table('cobros')->where('borrado', 0)->sum('montoTotal');
        $totalIndumentaria = DB::table('purchase_indumentaries')->where('borrado', 0)->sum('total');
        $totalMatPrima = DB::table('purchase_raw_materials')->where('borrado', 0)->sum('total');
        $totalInsumos = DB::table('purchase_supplie_services')->where('borrado', 0)->where('tipoCompra', 'I')->sum('total');
        $totalServicios = DB::table('purchase_supplie_services')->where('borrado', 0)->where('tipoCompra', 'S')->sum('total');
        $totalSueldos = DB::table('salaries')->where('borrado', 0)->sum('total');

        $totalCompras = [
            'totalIndumentaria' => $totalIndumentaria,
            'totalMatPrima' => $totalMatPrima,
            'totalInsumos' => $totalInsumos,
            'totalServicios' => $totalServicios,
            'totalSueldos' => $totalSueldos,
            'totalCompras' => $totalIndumentaria + $totalMatPrima + $totalInsumos + $totalServicios,
            'total' => $totalIndumentaria + $totalMatPrima + $totalInsumos + $totalServicios + $totalSueldos,
        ];
        // return $totalCompras;

        return view('home')
            ->with('totalCompras', $totalCompras)
            ->with('totalCobros', $totalCobros)
            ->with('totalVentas', $totalVentas);
    }

    public function graficoVentas()
    {
        $ventas = DB::table('ventas')->where('borrado', 0)
            ->where(DB::raw('YEAR(fecha)'), '=', Carbon::now())
            ->select(DB::raw('MONTH(fecha) as NroMes, sum(total) as SUMA'))
            ->groupBy(DB::raw('MONTH(fecha)'))
            ->orderBy(DB::raw('MONTH(fecha)'))
            ->get();

        $totales = array();

        for ($i = 0; $i < 12; $i++) {
            if (isset($ventas[$i]->SUMA)) {
                array_push($totales, $ventas[$i]->SUMA);
            } else {
                array_push($totales, 0);
            }
        }

        return response()->json($totales);
    }

    public function graficoCompras()
    {
        $totalIndumentaria = DB::table('purchase_indumentaries')->where('borrado', 0)
            ->where(DB::raw('YEAR(fecha)'), '=', Carbon::now())
            ->select(DB::raw('MONTH(fecha) as NroMes, sum(total) as SUMA'))
            ->groupBy(DB::raw('MONTH(fecha)'))
            ->orderBy(DB::raw('MONTH(fecha)'))
            ->get();

        $totalMatPrima = DB::table('purchase_raw_materials')->where('borrado', 0)
            ->where(DB::raw('YEAR(fecha)'), '=', Carbon::now())
            ->select(DB::raw('MONTH(fecha) as NroMes, sum(total) as SUMA'))
            ->groupBy(DB::raw('MONTH(fecha)'))
            ->orderBy(DB::raw('MONTH(fecha)'))
            ->get();

        $totalInsumos = DB::table('purchase_supplie_services')->where('borrado', 0)->where('tipoCompra', 'I')
            ->where(DB::raw('YEAR(fecha)'), '=', Carbon::now())
            ->select(DB::raw('MONTH(fecha) as NroMes, sum(total) as SUMA'))
            ->groupBy(DB::raw('MONTH(fecha)'))
            ->orderBy(DB::raw('MONTH(fecha)'))
            ->get();

        $totalServicios = DB::table('purchase_supplie_services')->where('borrado', 0)->where('tipoCompra', 'S')
            ->where(DB::raw('YEAR(fecha)'), '=', Carbon::now())
            ->select(DB::raw('MONTH(fecha) as NroMes, sum(total) as SUMA'))
            ->groupBy(DB::raw('MONTH(fecha)'))
            ->orderBy(DB::raw('MONTH(fecha)'))
            ->get();


        $totalesIndumentaria = array();
        $totalesMatPrima = array();
        $totalesInsumos = array();
        $totalesServicios = array();

        for ($i = 0; $i < 12; $i++) {
            if (isset($totalIndumentaria[$i]->SUMA)) {
                array_push($totalesIndumentaria, $totalIndumentaria[$i]->SUMA);
            } else {
                array_push($totalesIndumentaria, 0);
            }

            if (isset($totalMatPrima[$i]->SUMA)) {
                array_push($totalesMatPrima, $totalMatPrima[$i]->SUMA);
            } else {
                array_push($totalesMatPrima, 0);
            }

            if (isset($totalInsumos[$i]->SUMA)) {
                array_push($totalesInsumos, $totalInsumos[$i]->SUMA);
            } else {
                array_push($totalesInsumos, 0);
            }

            if (isset($totalServicios[$i]->SUMA)) {
                array_push($totalesServicios, $totalServicios[$i]->SUMA);
            } else {
                array_push($totalesServicios, 0);
            }
        }

        $totales[] = (object) [
            'indumentaria' => $totalesIndumentaria,
            'matPrima' => $totalesMatPrima,
            'insumos' =>  $totalesInsumos,
            'servicios' => $totalesServicios
        ];

        return response()->json($totales);
    }

    public function calendarioCheques()
    {
        $cheques = DB::table('cheques')->where('borrado', 0)
            ->select('tipoCheque', 'fechaCobro', 'id')
            ->get();
        // return response()->json($cheques);
        $calendario = [];
        foreach ($cheques as $cheque) {
            $array = array(
                'id' => $cheque->id,
                'title' => 'Cheque ' . $cheque->tipoCheque,
                'start' => $cheque->fechaCobro,
                'className' => 'cursor-pointer',
                'backgroundColor' => $cheque->tipoCheque == 'emitido' ? '#f56954' : '#2DAD4C',
                'borderColor' => $cheque->tipoCheque == 'emitido' ? '#f56954' : '#2DAD4C',
                'allDay' => true
            );

            array_push($calendario, $array);
        }

        return response()->json($calendario);
    }
}
