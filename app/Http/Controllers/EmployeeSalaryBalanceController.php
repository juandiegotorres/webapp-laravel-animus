<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;


class EmployeeSalaryBalanceController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:balance-sueldos']);
    }

    public function index()
    {
        if (!auth()->user()->can('salary-balance.index')) {
            abort(403, 'Acción no autorizada.');
        }

        return view('salary-balance.index');
    }

    public function indexDT($employee_id = null)
    {
        if (!auth()->user()->can('salary-balance.index')) {
            abort(403, 'Acción no autorizada.');
        }
        $balance = DB::table('employee_salary_balances')
            ->join('employees', 'employees.id', '=', 'employee_salary_balances.employee_id')
            ->where('employee_id', $employee_id)
            ->where('employee_salary_balances.borrado', 0)
            ->select('employee_salary_balances.*', 'nombreCompleto')->orderBy('created_at', 'DESC')
            ->get();

        // return count($balance);

        return DataTables::of($balance)
            ->addColumn('empleado', '  <a target="_blank" href="{{ route(\'employees.show\', [\'employee\' => $employee_id]) }}"> {{$nombreCompleto}}</a>')
            ->addColumn('tipoMovimiento', '{{ $tipoMovimiento }}')
            ->addColumn('debe', '{{ $debe }}')
            ->addColumn('haber', '{{ $haber }}')
            ->addColumn('fecha', '{{ date("d-m-y", strtotime($fecha)) }}')
            ->rawColumns(['empleado'])
            ->toJson();
    }

    public function filtrarFechas($employee_id = null, $fechaInicio = null, $fechaFin = null)
    {
        if (!auth()->user()->can('salary-balance.index')) {
            abort(403, 'Acción no autorizada.');
        }
        $error = array();
        if ($fechaInicio != null || $fechaFin != null) {
            if ($fechaInicio == 'vacio') {
                $error = [
                    'status' => 'error',
                    'message' => 'La fecha de inicio no puede estar vacia'
                ];
            } elseif ($fechaFin > '2099-12-31' || $fechaInicio < '1900-01-01') {
                $error = [
                    'status' => 'error',
                    'message' => 'Introduzca una fecha válida'
                ];
            } elseif ($fechaInicio == $fechaFin) {
                $error = [
                    'status' => 'error',
                    'message' => 'Las fechas no pueden ser iguales'
                ];
            } elseif ($fechaInicio > $fechaFin) {
                $error = [
                    'status' => 'error',
                    'message' => 'La fecha de inicio no puede ser mayor que la fecha de fin'
                ];
            }
            if (count($error) != 0) {
                return response()->json($error);
                exit();
            }

            $balance = DB::table('employee_salary_balances')
                ->join('employees', 'employees.id', '=', 'employee_salary_balances.employee_id')
                ->where('employee_id', $employee_id)
                ->where('employee_salary_balances.borrado', 0)
                ->whereBetween('fecha', [$fechaInicio, $fechaFin])
                ->select('employee_salary_balances.*', 'nombreCompleto')->orderBy('created_at', 'DESC')
                ->get();


            return DataTables::of($balance)
                ->addColumn('empleado', '  <a target="_blank" href="{{ route(\'employees.show\', [\'employee\' => $employee_id]) }}"> {{$nombreCompleto}}</a>')
                ->addColumn('fecha', '{{ date("d-m-y", strtotime($fecha)) }}')
                ->addColumn('tipoMovimiento', '{{ $tipoMovimiento }}')
                ->addColumn('debe', '{{ $debe }}')
                ->addColumn('haber', '{{ $haber }}')
                ->rawColumns(['empleado'])
                ->toJson();
        }
    }
}
