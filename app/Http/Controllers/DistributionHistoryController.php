<?php

namespace App\Http\Controllers;

use App\DetailIndumentaryDistribution;
use App\Purchase;
use App\Indumentary;
use App\IndumentaryDistribution;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class DistributionHistoryController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('distribution-history.index')) {
            abort(403, 'Acción no autorizada.');
        }

        return view('distribution-history.index');
    }

    public function datatable()
    {

        // return view('distribution-history.index');
    }

    public function obtenerHistorial($idEmpleado = null)
    {
        if (!auth()->user()->can('distribution-history.index')) {
            abort(403, 'Acción no autorizada.');
        }

        if ($idEmpleado == null) {
            $distribuciones = IndumentaryDistribution::orderBy('fecha', 'desc')->get();
        } else {
            $distribuciones = IndumentaryDistribution::where('employee_id', '=', $idEmpleado)
                ->orderBy('fecha', 'desc')->get();
        }

        return DataTables::of($distribuciones)
            ->addColumn('fecha', '{{ $fecha }}')
            ->addColumn('detalle', '{{ $detalle }}')
            ->editColumn(
                'indumentariaCantidad',
                function (IndumentaryDistribution $indumentaryDistribution) {
                    $indumentaria = '';
                    foreach ($indumentaryDistribution->detalleDistribucion as $detalle) {
                        $indumentaria .= '<li>' . $detalle->insumo->nombre . ' - ' . $detalle->cantidad . ' unidad(es)</li>';
                    }
                    return '<ul>' . $indumentaria . '</ul>';
                }
            )
            ->addColumn(
                'empleado',
                function (IndumentaryDistribution $indumentaryDistribution) {
                    return $indumentaryDistribution->empleado->nombreCompleto;
                }
            )
            ->addColumn(
                'eliminar',
                '<a href="{{ route(\'entrega-indumentaria.pdf\', [\'entrega\' => $id]) }}" target="_blank" class="btn btn-warning btn-sm"><i class="fa fa-print"></i></a>
                <a data-id="{{ $id }}" class="btn btn-danger btn-sm btnEliminar">Eliminar <i class="fa fa-trash"></i></a>'
            )
            ->rawColumns(['indumentariaCantidad', 'eliminar'])
            ->toJson();
    }

    public function delete($id)
    {
        if (!auth()->user()->can('distribution-history.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        return DB::transaction(
            function () use ($id) {
                try {
                    //Seteo el registro 'borrado' de la tabla maestro en 1
                    DB::table('indumentary_distributions')->where('id', $id)->update(['borrado' => 1]);

                    //Obtengo todos los registros de la tabla detalle donde aparezca el id de la distribucion de la tabla maestro
                    $detalles = DB::table('detail_indumentary_distributions')
                        ->select('cantidad', 'indumentary_id')
                        ->where('indumentary_distributions_id', $id)
                        ->get();

                    //Recorro cada registro y vuelvo a incrementar la cantidad de los insumos entregados
                    foreach ($detalles as $detalle) {
                        DB::table('indumentaries')->where('id', $detalle->indumentary_id)->increment('cantidad', $detalle->cantidad);
                    }

                    //Seteo todos los registros 'borrado' de la tabla detalle en 1
                    DB::table('detail_indumentary_distributions')->where('indumentary_distributions_id', $id)->update(['borrado' => 1]);

                    return response()->json(['success' => 'Se eliminó correctamente']);
                } catch (\Exception $e) {
                    //En caso de haber algun erro se hace rollback
                    DB::rollback();
                    return response()->json(['error' => 'Error en el catch']);
                    // $request->session()->flash('status', ['message' => 'Sucedio un error' . $e, 'type' => 'error']);
                }
            }
        );
    }
}
