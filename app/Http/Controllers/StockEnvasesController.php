<?php

namespace App\Http\Controllers;

use App\DetalleStockEnvases;
use App\Envase;
use App\Provider;
use App\StockEnvases;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class StockEnvasesController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:stock-envases']);
    }

    public function create()
    {

        $envases = Envase::all();
        $proveedoresMP = Provider::where('tipoProveedor', 'like', '%MP%')->get();

        return view('stock.stock-envases')
            ->with('proveedoresMP', $proveedoresMP)
            ->with('envases', $envases);
    }
    public function datatableEnvase()
    {
        $envases = StockEnvases::join('providers', 'providers.id', 'stock_envases.provider_id')
            ->select('stock_envases.*', 'providers.nombreCompleto')
            ->get();

        return DataTables::of($envases)
            ->editColumn('fecha', '{{ date("d-m-Y", strtotime($fecha)) }}')
            ->addColumn('tipoMovimiento', function ($envases) {
                if ($envases->es_entregado == StockEnvases::ES_ENTREGADO) {
                    return "Entregado";
                } else if ($envases->es_entregado == StockEnvases::ES_RECIBIDO) {
                    return "Recibido";
                }
            })
            ->addColumn('opciones', '<a data-id={{$id}} class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalDetalleMovimientoEnvase">Ver detalles</a>
            <a data-id={{$id}} class="btn btn-danger btn-sm btnEliminarMovimientoEnvase"><i class="fa fa-trash" aria-hidden="true"></i></a>')
            ->rawColumns(['opciones'])
            ->make(true);
    }

    public function detalleMovimiento($idmovimiento)
    {
        $detalles = DB::table('detalle_stock_envases')->where('id_stock_envases', $idmovimiento)
            ->join('envases', 'envases.id', 'detalle_stock_envases.id_envase')
            ->select('detalle_stock_envases.*', 'envases.nombre AS envase')
            ->get();

        return response()->json($detalles);
    }

    public function store(Request $request)
    {
        // return response()->json($request->all());
        $rules = array(
            'proveedor' => 'required',
            'cantidad' => 'required',
            'cantidad.*' => 'required|integer',
            'fecha' => 'required|date|date_format:Y-m-d|before:tomorrow',
            'descripcion' => 'nullable|min:3'
        );

        $error = Validator::make($request->all(), $rules, ['fecha.tomorrow' => 'La fecha debe ser menor o igual a hoy']);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        return DB::transaction(function () use ($request) {
            try {
                $movimiento = StockEnvases::create([
                    'fecha' => $request['fecha'],
                    'provider_id' => $request['proveedor'],
                    'descripcion' => $request['descripcion'],
                    'es_entregado' => $request['es_entregado'],
                ]);

                for ($i = 0; $i < count($request['cantidad']); $i++) {
                    DetalleStockEnvases::create([
                        'id_stock_envases' => $movimiento->id,
                        'id_envase' => $request['id_envase'][$i],
                        'cantidad' => $request['cantidad'][$i]
                    ]);

                    if ($request['es_entregado'] == StockEnvases::ES_ENTREGADO) {
                        DB::table('envases')
                            ->where('id', $request['id_envase'][$i])
                            ->update(['cantidad' => DB::raw('GREATEST(cantidad - ' . $request['cantidad'][$i] . ', 0)')]);
                    } else if ($request['es_entregado'] == StockEnvases::ES_RECIBIDO) {
                        DB::table('envases')->where('id', $request['id_envase'][$i])->increment('cantidad', $request['cantidad'][$i]);
                    }
                }
                return response()->json(['success' => 'Movimiento de envase registrado']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => [$e->getMessage()]]);
            }
        });
    }

    public function borrarMovimiento(StockEnvases $movimiento)
    {
        return DB::transaction(function () use ($movimiento) {
            try {
                $movimiento->update(['borrado' => 1]);
                foreach ($movimiento->detalleMovimiento as $detalle) {
                    if ($movimiento->es_entregado == StockEnvases::ES_ENTREGADO) {
                        DB::table('envases')->where('id', $detalle->id_envase)->increment('cantidad', $detalle->cantidad);
                    } else if ($movimiento->es_entregado == StockEnvases::ES_RECIBIDO) {
                        DB::table('envases')
                            ->where('id', $detalle->id_envase)
                            ->update(['cantidad' => DB::raw('GREATEST(cantidad - ' . $detalle->cantidad . ', 0)')]);
                    }
                }
                return response()->json(['success' => 'Movimiento de envases dado de baja']);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()]);
            }
        });
    }
}
