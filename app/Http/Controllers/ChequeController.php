<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Cheque;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ChequeRequest;
use Yajra\DataTables\Facades\DataTables;

class ChequeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:cheques']);
    }

    public function index()
    {
        if (!auth()->user()->can('cheques.index')) {
            abort(403, 'Acción no autorizada.');
        }
        return view('cheques.index');
    }

    public function datatable($emitidoRecibido)
    {

        if ($emitidoRecibido == 'emitido') {
            $cheques = Cheque::select('id', 'nroCheque', 'tipoCheque', 'fechaCobro', 'titular', 'importe', 'estado')->where('tipoCheque', 'emitido')->get();
        } else if ($emitidoRecibido == 'recibido') {
            $cheques = Cheque::select('id', 'nroCheque', 'entregadoPor', 'entregadoA', 'tipoCheque', 'fechaCobro', 'titular', 'importe', 'estado')->where('tipoCheque', 'recibido')->get();
        } else {
            abort(404);
        }

        return DataTables::of($cheques)
            ->editColumn('tipoCheque', '{{ ucfirst($tipoCheque) }}')
            ->editColumn('fechaCobro', '{{date("d-m-Y", strtotime($fechaCobro))}}')
            ->addColumn('opciones', function ($cheques) {
                $html = '<div class="d-flex justify-content-end">';
                if (auth()->user()->can('cheques.show')) {
                    $html .= '<a href="' . route('cheques.show', ['cheque' => $cheques->id]) . '" class="btn btn-success btn-sm mr-2" > 
                            <i class="fa fa-info-circle"></i>
                        </a>';
                }

                if (auth()->user()->can('cheques.edit')) {
                    $html .=  '<a class="btn btn-primary btn-sm mr-2" href="' . route('cheques.edit', ['cheque' => $cheques->id]) . '"> 
                            <i class="fa fa-pen"></i>
                        </a>';
                }

                if (auth()->user()->can('cheques.delete')) {
                    $html .= '<a  data-id="' . $cheques->id . '" data-tipo="' . $cheques->tipoCheque . '" class="btn btn-danger btn-sm mr-2 btnEliminarCheque">
                            <i class="fa fa-trash"></i>
                        </a>';
                }
                return $html;
            })
            ->addColumn('cambiarEstado', function (Cheque $cheque) use ($emitidoRecibido) {

                if ($cheque->estado == 1) {
                    $estado = 'Agregado';
                } elseif ($cheque->estado == 0) {
                    $emitidoRecibido == 'emitido' ? $estado = 'Pagado' :
                        $estado = 'Cobrado';
                } else if ($cheque->estado == 2) {
                    $estado = 'Entregado';
                } else {
                    $estado = 'Rechazado';
                }
                if (auth()->user()->can('cheques.edit')) {
                    return $estado . '<a data-id="' . $cheque->id . '" data-estado="' . $cheque->estado . '" class="btn btn-primary btn-sm ml-3" data-toggle="modal" data-target="#modalCambiarEstado"><i class="fa fa-pen"></i></a>';
                } else {
                    return $estado;
                }
            })
            ->rawColumns(['opciones', 'cambiarEstado'])
            ->make();
    }


    public function create($emitidoRecibido)
    {
        if ($emitidoRecibido == 'emitido' || $emitidoRecibido == 'recibido') {
            $bancos = Bank::select('nombre', 'id')->get();

            return view('cheques.create')
                ->with('bancos', $bancos)
                ->with('emitidoRecibido', $emitidoRecibido);
        } else {
            abort(404);
        }
    }

    public function store(ChequeRequest $request)
    {
        $cheque = Cheque::create(array_merge($request->all(), ['estado' => 1, 'borrado' => 0]));

        $request->session()->flash('status', 'Cheque creado con éxito');

        if ($request->desdeCobros) {
            return response()->json(['success' => [['Cheque agregado'], [$cheque]]]);
        }

        if ($request->tipoCheque == 'recibido') {
            return redirect()->action('ChequeController@index')->with('from', 'recibido');
        } else {
            return redirect()->action('ChequeController@index');
        }
    }

    public function show(Cheque $cheque)
    {
        return view('cheques.show')
            ->with('cheque', $cheque);
    }

    public function showJson(Cheque $cheque)
    {
        $cheque->nombreBanco = $cheque->banco->nombre;
        return response()->json($cheque);
    }

    public function edit(Cheque $cheque)
    {
        if (!auth()->user()->can('cheques.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        $bancos = Bank::select('nombre', 'id')->get();

        return view('cheques.edit')
            ->with('cheque', $cheque)
            ->with('bancos', $bancos);
    }


    public function update(ChequeRequest $request, Cheque $cheque)
    {
        $cheque->update($request->all());

        $request->session()->flash('status', 'Cheque modificado con éxito');

        if ($cheque->tipoCheque == 'recibido') {
            return redirect()->action('ChequeController@index')->with('from', 'recibido');
        } else {
            return redirect()->action('ChequeController@index');
        }
    }

    public function cambiarEstado(Request $request, Cheque $cheque)
    {
        if (!auth()->user()->can('cheques.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        if ($request->has('estado')) {
            $cheque->estado = $request->estado;
        }
        if (!$cheque->isDirty()) {
            return response()->json(['error' => ['Debe especificar un estado diferente al actual']]);
        }

        $cheque->save();

        return response()->json(['success' => ['Estado actualizado con éxito']]);
    }

    public function delete(Cheque $cheque)
    {
        if (!auth()->user()->can('cheques.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        DB::table('cheques')->whereId($cheque->id)->update(['borrado' => 1]);
    }
}
