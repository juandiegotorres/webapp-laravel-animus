<?php

namespace App\Http\Controllers;

use App\RawMaterial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RawMaterialController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:materias-primas']);
    }

    public function index()
    {
        if (!auth()->user()->can('raw-material.index')) {
            abort(403, 'Acción no autorizada.');
        }
        $materiasPrimas = RawMaterial::where('borrado', 0)->get();

        return view('raw-materials.index')->with('materiasPrimas', $materiasPrimas);
    }

    public function create()
    {
        if (!auth()->user()->can('raw-material.create')) {
            abort(403, 'Acción no autorizada.');
        }
        return view('raw-materials.create');
    }


    public function store(Request $request)
    {
        if (!auth()->user()->can('raw-material.create')) {
            abort(403, 'Acción no autorizada.');
        }
        $data = request()->validate([
            'fruta' => 'required',
            'variedad' => 'required',
        ]);

        $data['estado'] = 1;
        $data['borrado'] = 0;

        RawMaterial::create($data);

        request()->session()->flash('status', 'Materia prima creada con éxito');
        return redirect()->action('RawMaterialController@index');
    }


    public function update(Request $request)
    {
        if (!auth()->user()->can('raw-material.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        $data = request()->validate([
            'fruta' => 'required',
            'variedad' => 'required',
            'id' => 'required'
        ]);

        RawMaterial::where('id', $data['id'])->update(['fruta' => $data['fruta'], 'variedad' => $data['variedad']]);

        $request->session()->flash('status', 'Materia prima modificada con éxito');

        return redirect()->action('RawMaterialController@index');
    }

    public function storeFromPurchase(Request $request)
    {
        if (!auth()->user()->can('raw-material.create')) {
            abort(403, 'Acción no autorizada.');
        }
        $rules = array(
            'fruta' => 'required',
            'variedad' => 'required',
        );

        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $data = array(
            'fruta' => $request['fruta'],
            'variedad' => $request['variedad'],
            'estado' => 1,
            'borrado' => 0,
        );

        RawMaterial::create($data);

        return response()->json(['success' => 'Datos OK']);
    }


    public function delete(RawMaterial $rawMaterial)
    {
        if (!auth()->user()->can('raw-material.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        RawMaterial::whereId($rawMaterial->id)->update(['borrado' => 1]);
    }
}
