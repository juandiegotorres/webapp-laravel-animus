<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:usuarios']);
    }

    public function comprobarUsuario(Request $request)
    {
        $usuario_autenticado = auth()->user()->password;

        if (Hash::check($request['passw'], $usuario_autenticado)) {
            $roles = Role::all()->pluck('name');
            return response()->json([['success' => 'Contraseña correcta'], ['roles' => $roles]]);
        } else {
            return response()->json(['error' => 'Contraseña incorrecta']);
        }
    }

    public function index()
    {
        $rolUsuario = auth()->user()->getRoleNames()->first();
        $roles = Role::all();
        return view('users.password')
            ->with('rolUsuario', $rolUsuario)
            ->with('roles', $roles);
    }

    public function obtenerUsuarios()
    {
        $usuarios = User::where('estado', 1)->get();

        return DataTables::of($usuarios)
            ->addColumn('name', '{{ $username }}')
            ->addColumn('roles', function ($usuarios) {
                return $usuarios->getRoleNames();
            })
            ->addColumn(
                'opciones',
                function ($usuarios) {
                    $html = '';
                    if (auth()->user()->id != $usuarios->id) {
                        $html = '<a data-idusuario="' . $usuarios->id . '" data-rol="' . $usuarios->getRoleNames()->first() . '" data-toggle="modal" data-target="#modalAsignarRoles" class="btn btn-info btn-sm mr-1">Asignar roles<i class="fas fa-user-tag ml-2"></i></a>';
                    }
                    $html .= '<a data-username="' . $usuarios->username . '" data-toggle="modal" data-target="#cambiarPassModal" class="btn btn-warning btn-sm">Cambiar contraseña <i class="fa fa-key ml-2"></i></a><br>';

                    return $html;
                }
            )
            ->rawColumns(['opciones'])
            ->toJson();
    }

    public function create(Request $request)
    {

        $rules = array(
            'name' => 'required|min:3|unique:users,username|alpha_dash',
            'password' => 'required|min:8',
        );

        $messages = array(
            'name.required' => 'Este nombre de usuario no puede estar vacío',
            'name.min' => 'El nombre de usuario debe tener al menos 3 caracteres',
            'name.unique' => 'Este nombre de usuario ya esta en uso',
            'name.alpha_dash' => 'El nombre de usuario no puede contener espacios',
            'password.required' => 'Debe introducir una contraseña',
            'password.min' => 'La contraseña debe tener al menos 8 caracteres',
            'password.regex' => 'La contraseña debe contener al menos una letra mayúscula'
        );

        $error = Validator::make($request->all(), $rules, $messages);
        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        $data = array(
            'username' => $request['name'],
            'password' => Hash::make($request['password']),
        );

        User::create($data);

        return response()->json(['success' => 'Datos OK']);
    }

    public function changePassword(Request $request)
    {
        $rules = array(
            'new_password' => 'required|min:8',
        );

        $messages = array(
            'new_password.required' => 'Debe introducir una contraseña',
            'new_password.min' => 'La contraseña debe tener al menos 8 caracteres',
        );

        $error = Validator::make($request->all(), $rules, $messages);
        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        $data = array(
            'password' => Hash::make($request['new_password']),
        );

        User::where('username', $request['username'])->update(['password' => $data['password']]);
        return response()->json(['success' => 'Datos OK']);
    }

    public function asignarRoles(Request $request)
    {
        try {
            User::findOrFail($request->user)->syncRoles($request->rol);
            return response()->json(['success' => 'Asignación de rol registrada con éxito']);
            //
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function rolesDelUsuario($idUsuario)
    {
        try {
            $roles = User::findOrFail($idUsuario)->getRoleNames();
            return response()->json(['roles' => $roles]);
            //
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
}
