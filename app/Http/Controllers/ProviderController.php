<?php

namespace App\Http\Controllers;

use App\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProviderRequest;

class ProviderController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:proveedores']);
    }

    public function index($tipo = null)
    {
        if (!auth()->user()->can('providers.index')) {
            abort(403, 'Acción no autorizada.');
        }

        if ($tipo) {
            $providers = Provider::deMateriaPrima()->get();
            $title = 'Proveedores de materia prima';
        } else {
            $providers = Provider::where('borrado', 0)->get();
            $title = 'Proveedores';
        }

        return view('providers.index')->with('providers', $providers)->with('title', $title);
    }




    public function create()
    {
        if (!auth()->user()->can('providers.create')) {
            abort(403, 'Acción no autorizada.');
        }

        $provincias = DB::table('provinces')->get();
        return view('providers.create')->with('provincias', $provincias);
    }

    //Hago la validacion dentro de ProviderRequest
    public function store(ProviderRequest $request)
    {
        // dd($request->all());

        if (!auth()->user()->can('providers.create')) {
            abort(403, 'Acción no autorizada.');
        }
        $request['estado'] = 1;
        $request['borrado'] = 0;

        $tipoProveedor = '';

        //Esta propiedad del request es un array con los distintos tipos de proveedores, si el array tiene mas de un registro lo recorro con un for
        //Y concateno un guion  para luego cuando lo traiga de la base de datos poder separarlo y volver a convertirlo en un array
        if (count($request['tipoProveedor']) != 1) {
            for ($i = 0; $i < count($request['tipoProveedor']); $i++) {
                $tipoProveedor .= $request['tipoProveedor'][$i] . '-';
            }
            $request['tipoProveedor'] = $tipoProveedor;
        } else {
            //Si tiene solamente un registro lo guardo en la base de datos de igual manera con un guion para poder convertirlo en un array luego
            $request['tipoProveedor'] = $request['tipoProveedor'][0] . '-';
        }

        $request['nombreCompleto'] =
            $request['personaEmpresa'] == 'persona'
            ? Provider::nombreCompleto($request['nombre'], $request['apellido'])
            : $request['razonSocial'];


        $provider = Provider::create($request->except(['nombre', 'apellido']));

        $request->session()->flash('status', 'Proveedor creado con éxito');
        $tipo = null;
        if (str_contains($provider->tipoProveedor, 'MP')) {
            $tipo = 'materia-prima';
        }
        return redirect()->action('ProviderController@index', [$tipo]);
    }

    public function show(Provider $provider)
    {

        if (!auth()->user()->can('providers.show')) {
            abort(403, 'Acción no autorizada.');
        }

        $provinciaLocalidad = DB::table('localities')
            ->join('provinces', 'localities.id_provincia', '=', 'provinces.id')
            ->where('localities.id', $provider->idLocalidad)
            ->get();

        // dd($provinciaLocalidad);
        return view('providers.show')
            ->with('provider', $provider)
            ->with('provinciaLocalidad', $provinciaLocalidad);
    }


    public function edit(Provider $provider)
    {
        if (!auth()->user()->can('providers.edit')) {
            abort(403, 'Acción no autorizada.');
        }

        $provincias = DB::table('provinces')->get();
        $localidades = DB::table('localities')
            ->select('localities.id', 'localidad', 'id_provincia')
            ->join('provinces', 'localities.id_provincia', '=', 'provinces.id')
            ->whereRaw('provinces.id = (SELECT id_provincia FROM localities Where id =' . $provider->idLocalidad . ')')
            ->get();

        //Separo el string delimitado por los guiones y lo convierto en un array
        $tiposProveedor = explode('-', $provider->tipoProveedor);
        //Elimina el ultimo registro del array. Que en este caso es un espacio en blanco
        array_pop($tiposProveedor);

        $provider->tipoProveedor = $tiposProveedor;

        $apellidoNombre = explode(", ", $provider->nombreCompleto);
        if (count($apellidoNombre) > 1) {
            $provider->apellido = $apellidoNombre[0];
            $provider->nombre = $apellidoNombre[1];
        } else {
            $provider->razonSocial = $apellidoNombre[0];
        }

        return view('providers.edit')
            ->with('provider', $provider)
            ->with('provincias', $provincias)
            ->with('localidades', $localidades);
    }


    public function update(ProviderRequest $request, Provider $provider)
    {
        if (!auth()->user()->can('providers.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        try {
            $tipoProveedor = '';
            //Esta propiedad del request es un array con los distintos tipos de proveedores, si el array tiene mas de un registro lo recorro con un for
            //Y concateno un guion  para luego cuando lo traiga de la base de datos poder separarlo y volver a convertirlo en un array
            if (count($request['tipoProveedor']) != 1) {
                for ($i = 0; $i < count($request['tipoProveedor']); $i++) {
                    $tipoProveedor .=  $request['tipoProveedor'][$i] . '-';
                }
                $request['tipoProveedor'] = $tipoProveedor;
            } else {
                //Si tiene solamente un registro lo guardo en la base de datos de igual manera con un guion para poder convertirlo en un array luego
                $request['tipoProveedor'] = $request['tipoProveedor'][0] . '-';
            }

            $request['nombreCompleto'] =
                $request['personaEmpresa'] == 'persona'
                ? Provider::nombreCompleto($request['nombre'], $request['apellido'])
                : $request['razonSocial'];

            $provider->update($request->except(['nombre', 'apellido']));

            $request->session()->flash('status', 'Proveedor modificado con éxito');

            $tipo = null;
            if (str_contains($provider->tipoProveedor, 'MP')) {
                $tipo = 'materia-prima';
            }
            return redirect()->action('ProviderController@index', [$tipo]);
            //
        } catch (\Exception $e) {

            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function delete(Provider $provider)
    {
        if (!auth()->user()->can('providers.delete')) {
            abort(403, 'Acción no autorizada.');
        }

        DB::table('providers')
            ->where('id', $provider->id)
            ->update(['borrado' => 1]);
    }

    public function listar()
    {
        $proveedores = Provider::all();
        return response()->json($proveedores);
    }

    public function tipoProveedor($tipo)
    {
        $proveedores = Provider::where('tipoProveedor', 'like', '%' . $tipo . '%')->get();
        return response()->json($proveedores);
    }
}
