<?php

namespace App\Http\Controllers;

use Exception;
use App\Provider;
use App\Purchase;
use App\Exceptions;
use App\RawMaterial;
use App\FaltanteFacturar;
use App\RawMaterialEntry;
use App\BalanceCompraPagos;
use App\PurchaseRawMaterial;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\DetailRawMaterialEntry;
use App\DetailPurchaseRawMaterial;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PurchaseRawMaterialController extends Controller
{


    public function index($id_proveedor)
    {
        $materiasPrimas = RawMaterial::where('borrado', 0)->get();
        $proveedor = Provider::whereId($id_proveedor);

        $entradasMateriaPrima = RawMaterialEntry::join('detail_raw_material_entries', 'detail_raw_material_entries.raw_material_entries_id', '=', 'raw_material_entries.id')
            ->join('raw_materials', 'detail_raw_material_entries.raw_material_id', '=', 'raw_materials.id')
            ->where('provider_id', '=', $id_proveedor)
            ->where('faltanteFacturar', '>', 0)
            ->select('faltanteFacturar', 'raw_materials.fruta', 'raw_materials.variedad')
            ->get();

        session()->flash('desdeIngreso', false);

        return view('compras.materia-prima')
            ->with('entradasMateriaPrima', $entradasMateriaPrima)
            ->with('proveedor', $proveedor)
            ->with('materiasPrimas', $materiasPrimas);
    }

    public function indexDesdeIngresoMP(RawMaterialEntry $rawMaterialEntry)
    {

        $detalleIngreso = DetailRawMaterialEntry::where('raw_material_entries_id', '=', $rawMaterialEntry->id)->get();

        session()->flash('desdeIngreso', true);

        return view('compras.materia-prima')
            ->with('detalleIngreso', $detalleIngreso)
            ->with('rawMaterialEntry', $rawMaterialEntry);
    }

    public function store(Request $request)
    {
        // return response()->json($request->all());
        if (!auth()->user()->can('purchase-raw-material.index')) {
            abort(403, 'Acción no autorizada.');
        }

        if (empty($request['materia-prima'])) {
            $request->session()->flash('status', ['message' => 'Debes comprar algo', 'type' => 'error']);
            return redirect()->back();
        }
        //Cuento la cantidad de registros que vienen dentro del array materia-prima y lo guardo en una variable
        //en este caso use materia-prima, pero puede ser cualquier array que venga del request(cantidad, precioUnitario o totalParcial)
        $registros = count($request['materia-prima']);

        //Comienzo la transaccion porque voy a efectuar insersiones en dos tablas distintas
        return DB::transaction(function () use ($request, $registros) {

            try {
                //Primeramente inserto el la compra general
                $compra = PurchaseRawMaterial::create([
                    'provider_id' => $request['idProveedor'],
                    'detalle' => $request['detalle'],
                    'iva' => $request['iva'],
                    'total' => $request['total'],
                    'fecha' => Carbon::now(),
                    'estado' => 1,
                    'borrado' => 0,
                ]);

                BalanceCompraPagos::create([
                    'provider_id' => $request['idProveedor'],
                    'tipoCompra' => BalanceCompraPagos::COMPRA_MATERIA_PRIMA,
                    'movimiento_id' => $compra->id,
                    'debe' => ($request['total'] + $request['iva']),
                    'fecha' => now(),
                ]);

                //Y ahora empiezo a recorrer el request para obtener las distintas compras que se hicieron al proveedor
                //Y las inserto en el detalle de la compra
                for ($i = 0; $i < $registros; $i++) {
                    $materiaPrima = $request['materia-prima'][$i];
                    $cantidad = $request['cantidad'][$i];
                    $precioUnitario = $request['precioUnitario'][$i];
                    $totalParcial = $request['totalParcial'][$i];

                    DetailPurchaseRawMaterial::create([
                        'purchase_raw_material_id' => $compra->id,
                        'raw_material_id' => $materiaPrima,
                        'cantidad' => $cantidad,
                        'precioUnitario' => $precioUnitario,
                        'totalParcial' => $totalParcial,
                        'estado' => 1,
                        'borrado' => 0,
                    ]);

                    if (FaltanteFacturar::where('provider_id', $request['idProveedor'])->where('raw_material_id', $materiaPrima)->exists()) {
                        DB::table('faltante_facturar')
                            ->where('provider_id', $request['idProveedor'])
                            ->where('raw_material_id', $materiaPrima)
                            ->update(['cantidadKg' => DB::raw('GREATEST(cantidadKg - ' . $cantidad . ', 0)')]);
                    }
                }

                //Lo unico que devuelve esta funcion es un error en el catch. Entonces lo asigno a la variable
                // $error = Purchase::restarCantidadAFacturar($request['idProveedor'], $request['materia-prima'], $request['cantidad']);
                //Si la variable existe, es que hay un error por lo que lanzo una excepcion
                // if ($error) {
                //     throw new Exception($error);
                // } else {
                //Si todo esta bien retorno el estado success (exito)
                return response()->json(['success' => 'Compra realizada con éxito']);
                // }
            } catch (\Exception $e) {
                //En caso de haber algun error se hace rollback
                DB::rollback();
                return response()->json(['error' => 'Sucedio un error: ' . $e->getMessage()]);
            }
        });
        // return redirect()->route('purchase.index');
    }

    function soloTieneNulls($input)
    {
        return empty(array_filter($input, function ($a) {
            return $a !== null;
        }));
    }

    public function storeDesdeIngresoMP(Request $request)
    {
        // return response()->json($request->all());
        //===================== VALIDACION ==========================
        $result = $this->soloTieneNulls($request['cantidadAFacturar']);
        if ($result == true) {
            return response()->json(['error'  => ['Debes facturar al menos una materia prima']]);
        }
        $arrayPrecios = $request['precioKg'];
        $arrayCantidades = $request['cantidadAFacturar'];

        $hayError = 0;

        for ($i = 0; $i < count($arrayPrecios); $i++) {
            if (($arrayPrecios[$i] != null && $arrayCantidades[$i] == null) || ($arrayPrecios[$i] == null && $arrayCantidades[$i] != null)) {
                $hayError += 1;
            }
        }

        if ($hayError >= 1) {
            //Error para mostrar en el sweet alert
            return response()->json(['error' => ['Si se establece una cantidad a facturar se debe establecer un precio, o viceversa.']]);
        }

        $mensajesError = [
            'cantidadAFacturar.*.numeric' => 'El campo cantidad a facturar debe ser numerico',
            'precioKg.*.numeric' => 'El campo precio por kg debe ser numerico',
            'cantidadAFacturar.*.not_in' => 'La cantidad a facturar debe ser mayor que 0',
            'precioKg.*.not_in' => 'El precio debe ser mayor que 0'
        ];
        //Valido los campos
        $error = Validator::make($request->all(), [
            'cantidadAFacturar.*' => 'numeric|nullable|min:0|not_in:0',
            'precioKg.*' => 'numeric|nullable|min:0|not_in:0'
        ], $mensajesError);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }
        //==================== FIN VALIDACION ========================


        //Cuento la cantidad de registros que vienen dentro del array cantidadAFacturar y lo guardo en una variable
        //en este caso use cantidadAFacturar, pero puede ser cualquier array que venga del request(cantidad, precioUnitario o totalParcial)
        $registros = count($request['cantidadAFacturar']);

        //Comienzo la transaccion porque voy a efectuar insersiones en dos tablas distintas
        return DB::transaction(function () use ($request, $registros) {

            try {
                $total = 0;
                for ($i = 0; $i < $registros; $i++) {
                    if (
                        $request['cantidadAFacturar'][$i] != null && $request['cantidadAFacturar'][$i] != 0
                        && $request['precioKg'][$i] != null && $request['precioKg'][$i] != 0
                    ) {
                        $cantidad = $request['cantidadAFacturar'][$i];
                        $precioUnitario = $request['precioKg'][$i];
                        $total += ($cantidad * $precioUnitario);
                    }
                }
                //Primeramente inserto el la compra general
                $compra = PurchaseRawMaterial::create([
                    'provider_id' => $request['idProveedor'],
                    'iva' => $request['iva'],
                    'total' => $total,
                    'fecha' => Carbon::now(),
                    'estado' => 1,
                    'borrado' => 0,
                ]);

                BalanceCompraPagos::create([
                    'provider_id' => $request['idProveedor'],
                    'tipoCompra' => BalanceCompraPagos::COMPRA_MATERIA_PRIMA,
                    'movimiento_id' => $compra->id,
                    'debe' => $request['total'],
                    'fecha' => now(),
                ]);

                //Y ahora empiezo a recorrer el request para obtener las distintas compras que se hicieron al proveedor
                //Y las inserto en el detalle de la compra
                for ($i = 0; $i < $registros; $i++) {
                    if ($request['cantidadAFacturar'][$i] != null && $request['precioKg'][$i] != null) {
                        $materiaPrima = $request['materia-prima'][$i];
                        $cantidad = $request['cantidadAFacturar'][$i];
                        $precioUnitario = $request['precioKg'][$i];
                        $totalParcial = ($cantidad * $precioUnitario);

                        DetailPurchaseRawMaterial::create([
                            'purchase_raw_material_id' => $compra->id,
                            'raw_material_id' => $materiaPrima,
                            'cantidad' => $cantidad,
                            'precioUnitario' => $precioUnitario,
                            'totalParcial' => $totalParcial,
                            'estado' => 1,
                            'borrado' => 0,
                        ]);
                    }
                }

                //Pongo el estado en 0 para saber que ese ingreso de materia prima no se puede dar de baja o eliminar ya que tiene una compra asignada
                RawMaterialEntry::whereId($request['id_ingreso'])->update(['estado' => 0]);

                //Lo unico que devuelve esta funcion es un error en el catch. Entonces lo asigno a la variable
                $error = Purchase::restarCantidadAFacturar($request['idProveedor'], $request['materia-prima'], $request['cantidadAFacturar']);
                //Si la variable existe, es que hay un error por lo que lanzo una excepcion
                if ($error) {
                    throw new Exception($error);
                } else {
                    //Si todo esta bien retorno el estado success (exito)
                    return response()->json(['success' => 'Compra realizada con éxito']);
                }

                return response()->json(['success' => 'Compra realizada con éxito']);
            } catch (\Exception $e) {
                //En caso de haber algun erro se hace rollback
                DB::rollback();
                return response()->json(['error' => [$e->getMessage()]]);
            }
        });
    }

    public function show(PurchaseRawMaterial $purchaseRawMaterial)
    {
        if (!auth()->user()->can('purchase-raw-material.show')) {
            abort(403, 'Acción no autorizada.');
        }
        $detalleCompra = DetailPurchaseRawMaterial::where('purchase_raw_material_id', $purchaseRawMaterial->id)->get();
        $maestroCompra = $purchaseRawMaterial;
        return view('purchase-raw-material.show')->with('detalleCompra', $detalleCompra)->with('maestroCompra', $maestroCompra);
    }



    public function proveedoresMateriaPrima()
    {
        $proveedores = DB::table('providers')->where('borrado', 0)->where('tipoProveedor', 'like', '%MP%')->get();
        return response()->json($proveedores);
    }

    public function obtenerMateriasPrimas()
    {
        $materiasPrimas = RawMaterial::where('borrado', 0)->get();
        return response()->json($materiasPrimas);
    }
}
