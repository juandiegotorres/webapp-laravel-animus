<?php

namespace App\Http\Controllers;

use App\Provider;
use App\RawMaterial;
use App\RawMaterialEntry;
use App\RawMaterialMovement;
use Illuminate\Http\Request;
use App\DetailRawMaterialEntry;
use App\Envase;
use App\FaltanteFacturar;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class RawMaterialEntryController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:ingreso-materia-prima']);
    }

    public function index()
    {
        if (!auth()->user()->can('ingresoMP.index')) {
            abort(403, 'Acción no autorizada.');
        }

        $proveedores = Provider::where('tipoProveedor', 'like', '%MP%')->get();

        return view('ingreso-materia-prima.index')
            ->with('proveedores', $proveedores);
    }

    public function create($idProveedor = null)
    {
        $idProveedor == null ? abort(404) : $proveedor = Provider::where('tipoProveedor', 'like', '%MP%')->findOrFail($idProveedor);

        $materiasPrimas = RawMaterial::all();
        $envases = Envase::all();

        return view('ingreso-materia-prima.create')
            ->with('proveedor', $proveedor)
            ->with('envases', $envases)
            ->with('materiasPrimas', $materiasPrimas);
    }

    public function datatable()
    {
        $ingresos = RawMaterialEntry::join('providers', 'raw_material_entries.provider_id', '=', 'providers.id')
            ->join('raw_materials', 'raw_materials.id', 'raw_material_entries.raw_material_id')
            ->select('raw_material_entries.*', 'raw_materials.fruta', 'raw_materials.variedad', 'providers.nombreCompleto')
            ->get();

        return $this->devolverTabla($ingresos);
    }

    public function filtrar(Request $request)
    {
        try {
            $fechaInicio = $request['fechaInicio'];
            $fechaFin = $request['fechaFin'];
            $idproveedor = $request['proveedor'];

            $rules = array(
                'fechaInicio' => 'nullable|date|before:fechaFin|after:2000-01-01',
                'fechaFin' => 'required|date|before:2099-12-31|after:fechaInicio',
                'proveedor' => 'nullable|required_without:fechaInicio',
            );

            $error = Validator::make($request->all(), $rules, ['proveedor.required_without' => 'El campo proveedor es obligatorio cuando fecha inicio no está presente o viceversa.']);

            if ($error->fails()) {
                return response()->json(['error' => $error->errors()->all()]);
            }

            $ingreso = '';
            $query = RawMaterialEntry::join('providers', 'raw_material_entries.provider_id', '=', 'providers.id')
                ->join('raw_materials', 'raw_materials.id', 'raw_material_entries.raw_material_id')
                ->select('raw_material_entries.*', 'raw_materials.fruta', 'raw_materials.variedad', 'providers.nombreCompleto');
            if ($fechaInicio == null) {
                //Solo filtrar por proveedor
                $ingresos = $query->where('provider_id', $idproveedor)->get();

                //
            } elseif ($idproveedor == '') {
                //Solo debo filtrar entre fechas
                $ingresos = $query->whereBetween('fecha', [$fechaInicio, $fechaFin])->get();
            } else {
                //Debo filtrar entre ambos
                $ingresos = $query->whereBetween('fecha', [$fechaInicio, $fechaFin])->where('provider_id', $idproveedor)->get();
            }

            return $this->devolverTabla($ingresos);
            //
        } catch (\Exception $e) {
            return response()->json(['error' => [$e->getMessage()]]);
        }
    }

    public function store(Request $request)
    {
        // return response()->json($request->all());
        $rules = array(
            'fecha' => 'required|date|date_format:Y-m-d|after:2000-01-01',
            'idProveedor' => 'required',
            'materiasPrima' => 'required',
            'kgTara' => 'required|numeric|gt:0',
            'kgBruto' => 'required|numeric|gt:0',
            'kgNeto' => 'required|numeric|gt:0',
            'tamanoChico' => 'nullable|numeric',
            'tamanoMediano' => 'nullable|numeric',
            'tamanoGrande' => 'nullable|numeric',
            'unidadesKg' => 'nullable|numeric',
        );

        $error = Validator::make($request->all(), $rules, ['proveedor.required_without' => 'El campo proveedor es obligatorio cuando fecha inicio no está presente o viceversa.']);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        if (!auth()->user()->can('ingresoMP.create')) {
            abort(403, 'Acción no autorizada.');
        }

        return DB::transaction(function () use ($request) {
            try {
                $ingreso = RawMaterialEntry::create([
                    'fecha' => $request['fecha'],
                    'provider_id' => $request['idProveedor'],
                    'raw_material_id' => $request['materiasPrima'],
                    'kgTara' => $request['kgTara'],
                    'kgBruto' => $request['kgBruto'],
                    'kgNeto' => $request['kgNeto'],
                    'tamanoChico' => $request['tamanoChico'],
                    'tamanoMediano' => $request['tamanoMediano'],
                    'tamanoGrande' => $request['tamanoGrande'],
                    'unidadesKg' => $request['unidadesKg'],
                    'observaciones' => $request['observaciones'],
                ]);

                list($envases, $cantidades) = $this->valoresDuplicados($request['id_envase'], $request['cantidad_envase']);

                for ($i = 0; $i < count($request['cantidad_envase']); $i++) {
                    DetailRawMaterialEntry::create([
                        'raw_material_entries_id' => $ingreso->id,
                        'envase_id' => $envases[$i],
                        'cantidadEnvase' => $cantidades[$i],
                    ]);
                }


                $faltante = FaltanteFacturar::where('provider_id', $request['idProveedor'])->where('raw_material_id', $request['materiasPrima'])->get();

                if (count($faltante) == 0) {
                    //Si no existe lo creo
                    FaltanteFacturar::create([
                        'provider_id' => $request['idProveedor'],
                        'raw_material_id' => $request['materiasPrima'],
                        'cantidadKg' => $request['kgNeto'],
                    ]);
                } else {
                    //Si existe lo actualizo
                    DB::table('faltante_facturar')
                        ->where('provider_id', $request['idProveedor'])
                        ->where('raw_material_id', $request['materiasPrima'])
                        ->increment('cantidadKg', $request['kgNeto']);
                }

                return response()->json(['success' => 'Ingreso registrado con éxito']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()]);
            }
        });
    }

    public function show(RawMaterialEntry $rawMaterialEntry)
    {
        if (!auth()->user()->can('ingresoMP.show')) {
            abort(403, 'Acción no autorizada.');
        }
        try {
            return view('ingreso-materia-prima.show')
                ->with('rawMaterialEntry', $rawMaterialEntry);
            //
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => $e->getMessage()]);
        }
    }


    public function delete(RawMaterialEntry $ingreso)
    {
        // return response()->json($ingreso);
        if (!auth()->user()->can('ingresoMP.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        return DB::transaction(function () use ($ingreso) {
            try {
                $ingreso->borrado = 1;
                $ingreso->save();

                RawMaterialMovement::where('id_ingreso', '=', $ingreso->id)->delete();

                //Recorro cada uno de los detalles del ingreso y voy restando la cantidad de kg a faltante_facturar 
                //ya que se cancelo el ingreso

                DB::table('faltante_facturar')
                    ->where('provider_id', $ingreso->provider_id)
                    ->where('raw_material_id', $ingreso->raw_material_id)
                    ->update(['cantidadKg' => DB::raw('GREATEST(cantidadKg - ' . $ingreso->kgNeto . ', 0)')]);

                //ACA

                return response()->json(['success' => 'Ingreso dado de baja con éxito']);

                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()]);
            }
        });
    }

    public function showDescartar(RawMaterialEntry $rawMaterialEntry)
    {
        if (!auth()->user()->can('ingresoMP.descartar')) {
            abort(403, 'Acción no autorizada.');
        }
        //Obtengo los movimientos (se insertan en el momento que se agrega un ingreso) que son los que me dicen la cantidad ingresada, la cantidad descartada, etc
        $historialDescarte = RawMaterialMovement::where('id_ingreso', $rawMaterialEntry->id)->get();

        //Retorno la vista
        return view('ingreso-materia-prima.descartar')
            // ->with('detalleIngreso', $detalleIngreso)
            ->with('historialDescarte', $historialDescarte)
            ->with('rawMaterialEntry', $rawMaterialEntry);
    }

    function soloTieneNulls($input)
    {
        return empty(array_filter($input, function ($a) {
            return $a !== null;
        }));
    }

    public function storeDescartar(RawMaterialEntry $rawMaterialEntry, Request $request)
    {
        // return response()->json($request->all());
        if (!auth()->user()->can('ingresoMP.descartar')) {
            abort(403, 'Acción no autorizada.');
        }


        $rules = array(
            'id_ingreso' => 'required',
            'porcentajeDescarte' => 'required|numeric|max:99|gt:0',
            'observaciones' => 'nullable'
        );

        $mensajesError = [
            'porcentajeDescarte.required' => 'El campo cantidad a descartar no puede estar vacio',
            'porcentajeDescarte.numeric' => 'El campo cantidad a descartar debe ser numerico',
            'porcentajeDescarte.max' => 'El campo cantidad a descartar no puede ser mayor a 99',
            'porcentajeDescarte.gt' => 'El campo cantidad a descartar debe ser mayor a 0',
        ];

        //Valido los campos

        $error = Validator::make($request->all(), $rules, $mensajesError);


        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }
        //Sumo todos los porcentajes de descartes anteriores para corroborar que lo que se descarte no sea mayor del 100%
        $porcentajeDescartado = $rawMaterialEntry->movimientos->sum('porcentaje');
        $porcentajeTotal = $porcentajeDescartado + $request['porcentajeDescarte'];

        if ($porcentajeTotal > 99) {
            return response()->json(['error' => ['No se puede descartar mas de el 99% de la fruta']]);
        }

        return DB::transaction(function () use ($request, $rawMaterialEntry) {
            try {
                $cantidadKgDescartados = (($request['porcentajeDescarte'] * $request['cantidad_inicial']) / 100);
                RawMaterialMovement::Create([
                    'id_ingreso' => Crypt::decrypt($request['id_ingreso']),
                    'fechaMovimiento' => now(),
                    'porcentaje' => $request['porcentajeDescarte'],
                    'cantidadKg' => $cantidadKgDescartados,
                    'usuarioGalpon' => auth()->user()->id,
                    'observaciones' => $request['observaciones'],
                    'estado' => 1,
                    'borrado' => 0
                ]);

                $rawMaterialEntry->update(['estado' => 0]);

                DB::table('faltante_facturar')
                    ->where('provider_id', $rawMaterialEntry->provider_id)
                    ->where('raw_material_id', $rawMaterialEntry->raw_material_id)
                    ->update(['cantidadKg' => DB::raw('GREATEST(cantidadKg - ' . $cantidadKgDescartados . ', 0)')]);

                return response()->json(['success' => 'Descarte registrado con éxito']);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => [$e->getMessage()]]);
            }
        });
    }

    public function eliminarDescarte(RawMaterialMovement $descarte, RawMaterialEntry $ingreso)
    {
        return DB::transaction(function () use ($descarte, $ingreso) {
            try {
                DB::table('faltante_facturar')
                    ->where('provider_id', $ingreso->provider_id)
                    ->where('raw_material_id', $ingreso->raw_material_id)
                    ->increment('cantidadKg', $descarte->cantidadKg);


                $descarte->delete();

                //Obtengo la cantidad de descartes que se le han hecho 
                $descartes = DB::table('raw_material_movements')->where('id_ingreso', $descarte->id_ingreso)
                    ->where('borrado', 0)
                    ->get();

                //Si los descartes son 0 significa que puedo debo habilitar para poder eliminar el ingreso
                //eso lo hago poniendo el estado en 1
                if (count($descartes) == 0) {
                    Db::table('raw_material_entries')
                        ->whereId($ingreso->id)
                        ->update(['estado' => 1]);
                }

                return response()->json(['success' => 'Descarte eliminado con éxito']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()]);
            }
        });
    }

    public function devolverTabla($data)
    {
        return DataTables::of($data)
            ->editColumn('fecha', '{{date("d-m-Y", strtotime($fecha))}}')
            ->addColumn('materiaPrima', '{{ $fruta . ", " . $variedad }}')
            ->addColumn('kgNeto', '{{ $kgNeto }} kgs')
            ->addColumn('cantidadDescartada', function ($ingreso) {
                return $ingreso->movimientos->sum('porcentaje') . '%';
            })
            ->editColumn('nombreCompleto', '<a target="_blank" href="{{ route(\'providers.show\', [\'provider\' => $provider_id]) }}">{{$nombreCompleto}}</a>')
            ->addColumn(
                'opciones',
                function ($data) {
                    $html = '<div class="text-right">';
                    if (auth()->user()->can('ingresoMP.show')) {
                        $html .= '<a href="' . route('ingresoMP.show', ['rawMaterialEntry' =>  $data->id]) . '" class="btn btn-success btn-sm mr-1">' . ('Ver detalles') . '</a>';
                        $html .=  '<a href="' . route('ingreso-fruta.pdf', ['ingreso' => $data->id]) . '" target="_blank" class="btn btn-warning btn-sm "><i class="fa fa-print"></i></a>';
                    }

                    if (auth()->user()->can('ingresoMP.descartar')) {
                        $html .= '<a href="' . route('ingresoMP.store-descartar', ['rawMaterialEntry' => $data->id]) . '" class="btn btn-orange btn-sm mt-1">Descartar fruta</a> <br>';
                    }

                    if (auth()->user()->can('ingresoMP.facturar')) {
                        $html .= '<a href="' . route('purchase.tipoCompra', ['tipoCompra' => 'MP', 'idProveedor' => $data->provider_id]) . '" class="btn btn-primary mt-1 btn-sm mr-1">Facturar fruta</i></a>';
                    }

                    if (auth()->user()->can('ingresoMP.delete')) {
                        //Si el estado esta en 0 significa que tiene una compra ya hecha a este ingreso por lo cual no se puede eliminar
                        if ($data->estado == 0) {
                            $html .= '<button class="boton btn btn-secondary btn-sm mt-1" data-toggle="tooltip" data-placement="left" title="No se puede eliminar un ingreso si ya se ha descartado fruta"><i class="fa fa-trash"></i></button>';
                        } else {
                            $html .= '<a data-id="' . $data->id . '" class="btn btn-danger btn-sm  btnEliminarIngreso mt-1"><i class="fa fa-trash"></i></a>';
                        }
                    }
                    return $html .= '</div>';
                }
            )
            ->rawColumns(['nombreCompleto', 'opciones'])
            ->make(true);
    }

    public function valoresDuplicados($ids, $cantidades)
    {
        // Esta funcion surge del problema que desde el front, el usuario haya agregado una materia prima mas de una vez con distintas cantidades a comprar/ingresar respectivamente
        // Por lo que esta funcion lo que hace es recorrer el array y sumar todas las cantidades de las materias primas que estan repetidas

        // Obtengo los valores unicos dentro del array de ids de materias primas, y los valores duplicados
        $valoresUnicos = array_unique($ids);
        $valoresDuplicados = array_diff_assoc($ids, $valoresUnicos);

        $totalDuplicadas = 0;
        $suma = array();

        foreach ($valoresUnicos as $idUnico => $valorUnico) {
            //Agrego al array un registro con el id de la materia prima y la cantidad
            $suma[$valorUnico] = $cantidades[$idUnico];

            //Recorro el array de los valores que estan duplicados
            foreach ($valoresDuplicados as $idDuplicado => $valorDuplicado) {
                //Si se repiten con el array de ids unicos
                if ($valorUnico == $valorDuplicado) {
                    //Si no existe el valor dentro de larray de las sumas  
                    if (isset($suma[$valorDuplicado])) {
                        $suma[$valorUnico] += $cantidades[$idDuplicado];
                    } else {
                        $totalDuplicadas = 0;
                        $totalDuplicadas += $cantidades[$idUnico];
                        $suma[$valorUnico] = $totalDuplicadas += $cantidades[$idDuplicado];
                    }
                }
            }
        }

        //Separo los valores y los vuelvo a asignar a las variables iniciales
        $materiasPrimas = array_keys($suma);
        $cantidades = array_values($suma);

        return array($materiasPrimas, $cantidades);
    }
}
