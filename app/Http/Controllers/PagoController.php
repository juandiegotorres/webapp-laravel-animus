<?php

namespace App\Http\Controllers;

use App\Pago;
use App\Cheque;
use App\Provider;
use App\MetodoPago;
use App\DetallePago;
use App\BalanceCompraPagos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class PagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['permission:pagos']);
    }

    public function index()
    {
        $proveedores = Provider::all();
        return view('pagos.index')
            ->with('proveedores', $proveedores);
    }

    public function datatable(Request $request)
    {
        if ($request->idproveedor == 0) {
            $pagos = Pago::join('providers', 'pagos.provider_id', '=', 'providers.id')->select('pagos.*', 'providers.nombreCompleto')->get();
        } else {
            $pagos = Pago::join('providers', 'pagos.provider_id', '=', 'providers.id')
                ->where('provider_id', $request->idproveedor)
                ->select('pagos.*', 'providers.nombreCompleto')
                ->get();
        }

        return DataTables::of($pagos)
            ->editColumn('fecha', '{{ date("d-m-Y", strtotime($fecha))}}')
            ->editColumn('nombreCompleto', '<a target="_blank" href="{{ route(\'providers.show\', [\'provider\' => $provider_id]) }}"> {{$nombreCompleto}}</a>')
            ->editColumn('iva', '{{ "$" .  number_format($iva, 2, ",", ".")  }}')
            ->editColumn('montoTotal', '{{ "$" .  number_format($montoTotal, 2, ",", ".")  }}')
            ->addColumn('metodoPago', '<button data-id="{{$id}}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalMetodosPago">Ver métodos de pago</button>')
            ->addColumn('opciones', function ($pagos) {
                $html = '<div class="d-flex justify-content-end">';

                $html .=  '<a href="' . route('pago.pdf', ['pago' => $pagos->id]) . '" target="_blank" class="btn btn-warning btn-sm mr-2"><i class="fa fa-print"></i></a>';
                // '<a class="btn btn-primary btn-sm mr-2" href="' . route('pagos.edit', ['pago' => $pagos->id]) . '"> 
                //             <i class="fa fa-pen"></i>
                //         </a>';

                $html .= '<a  data-id="' . $pagos->id . '" class="btn btn-danger btn-sm mr-2 btnEliminarPago">
                            <i class="fa fa-trash"></i>
                        </a>';
                return $html;
            })
            ->rawColumns(['nombreCompleto', 'metodoPago', 'opciones'])
            ->make(true);
    }

    public function create(Provider $proveedor)
    {
        $metodosPago = MetodoPago::all();
        $cheques = Cheque::where(['estado' => 1])->orderBy('created_at', 'DESC')->get();

        return view('pagos.create')
            ->with('metodosPago', $metodosPago)
            ->with('cheques', $cheques)
            ->with('proveedor', $proveedor);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request['importe'])) {
            return response()->json(['error' => ['Debes agregar un metodo de pago']]);
        }

        $rules = array(
            'provider_id' => 'required|numeric',
            'fecha' => 'required|date|before:2099-12-31|after:2000-01-01',
            'iva' => 'required|numeric',
            'observaciones' => 'nullable'
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        return DB::transaction(function () use ($request) {

            try {
                $pago = Pago::create([
                    'provider_id' => $request['provider_id'],
                    'fecha' => $request['fecha'],
                    'iva' => $request['iva'],
                    'montoTotal' => array_sum($request['importe']),
                    'observaciones' => $request['observaciones'],
                ]);

                for ($i = 0; $i < count($request['importe']); $i++) {
                    DetallePago::create([
                        'pago_id' => $pago->id,
                        'metodo_pago_id' => $request['id_metodo_pago'][$i],
                        'cheque_id' => $request['id_cheque'][$i],
                        'monto' => $request['importe'][$i],
                    ]);

                    if ($request['id_cheque'][$i] != null || $request['id_cheque'][$i] != '') {
                        DB::table('cheques')->where('id', $request['id_cheque'][$i])->update(['estado' => Cheque::CHEQUE_ENTREGADO]);
                    }
                }

                BalanceCompraPagos::create([
                    'provider_id' => $request['provider_id'],
                    'tipoCompra' => BalanceCompraPagos::PAGO_A_PROVEEDOR,
                    'movimiento_id' => $pago->id,
                    'haber' => $pago->montoTotal,
                    'fecha' => now(),
                ]);

                $request->session()->flash('status', 'Pago agregado con éxito');
                return response()->json(['success' => 'Pago agregado']);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => [$e->getMessage()]]);
            }
        });
    }


    public function edit(Pago $pago)
    {

        return view('pagos.edit')->with('pago', $pago);
    }

    public function update(Request $request, Pago $pago)
    {
        return DB::transaction(function () use ($request, $pago) {
            try {
                $data = $request->validate([
                    'provider_id' => 'required|numeric',
                    'montoTotal' => 'required|numeric|digits_between:1,10',
                    'fecha' => 'required|date|before:2099-12-31|after:2000-01-01',
                    'metodoPago' => 'required',
                    'observaciones' => 'nullable'
                ]);
                //Este if lo hago para saber si se modifico el monto del pago, ya que si se modificó debo actualizarlo tambien en la tabla balance compras pagos
                $request->montoTotal != $pago->montoTotal ? $monto = $request->montoTotal : '';

                $pago->update($data);

                //Si se actualizo el monto actualizo la tabla tambien
                if (isset($monto)) {
                    $balance = BalanceCompraPagos::where('movimiento_id', $pago->id)
                        ->where('tipoCompra', BalanceCompraPagos::PAGO_A_PROVEEDOR);

                    $balance->update([
                        'haber' => $monto,
                    ]);
                }
                return redirect()->action('PagoController@index');
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()]);
            }
        });
    }

    public function metodosPago($idpago)
    {
        $metodosPago = DB::table('detalle_pagos')->where('pago_id', $idpago)
            ->join('metodos_pago', 'metodos_pago.id', 'detalle_pagos.metodo_pago_id')
            ->leftjoin('cheques', 'cheques.id', 'detalle_pagos.cheque_id')
            ->select('detalle_pagos.*', 'metodos_pago.nombre', 'cheques.nroCheque')
            ->get();

        return response()->json($metodosPago);
    }

    public function delete(Pago $pago)
    {
        return DB::transaction(function () use ($pago) {
            try {

                $pago->update(['borrado' => 1]);

                DB::table('balance_compras_pagos')
                    ->where('movimiento_id', '=', $pago->id)
                    ->where('tipoCompra', '=', BalanceCompraPagos::PAGO_A_PROVEEDOR)
                    ->update(['borrado' => 1]);

                foreach ($pago->detallePago as $detalle) {
                    if ($detalle->cheque_id != null) {
                        DB::table('cheques')->where('id', $detalle->cheque_id)->update(['estado' => Cheque::CHEQUE_AGREGADO]);
                    }
                }

                return response()->json(['success' => 'Pago dado de baja con éxito']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()]);
            }
        });
    }
}
