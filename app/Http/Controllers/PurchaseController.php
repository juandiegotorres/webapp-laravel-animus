<?php

namespace App\Http\Controllers;

use App\Indumentary;
use App\Service;
use App\Supplie;
use App\Provider;
use App\Purchase;
use App\RawMaterial;
use App\RawMaterialEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:compras']);
    }
    public function index()
    {
        if (!auth()->user()->can('purchase.index')) {
            abort(403, 'Acción no autorizada.');
        }
        return view('purchases.index');
    }

    public function tipoCompra($tipoCompra, $idProveedor)
    {
        $proveedor = Provider::findOrFail($idProveedor);
        switch ($tipoCompra) {
            case 'MP':

                $materiasPrimas = RawMaterial::all();

                session()->flash('desdeIngreso', false);

                $faltantesFacturar = DB::table('faltante_facturar')
                    ->where('provider_id', $idProveedor)
                    ->where('cantidadKg', '>', 0)
                    ->join('providers', 'providers.id', 'faltante_facturar.provider_id')
                    ->join('raw_materials', 'raw_materials.id', 'faltante_facturar.raw_material_id')
                    ->select('faltante_facturar.*', 'providers.nombreCompleto', 'raw_materials.fruta', 'raw_materials.variedad')
                    ->get();


                return view('compras.materia-prima')
                    ->with('faltantesFacturar', $faltantesFacturar)
                    ->with('proveedor', $proveedor)
                    ->with('materiasPrimas', $materiasPrimas);

                break;


            case 'IS':

                $insumos = Supplie::all();

                return view('compras.insumos')
                    ->with('proveedor', $proveedor)
                    ->with('insumos', $insumos);

                break;
            case 'SV':

                $servicios = Service::all();

                return view('compras.servicios')
                    ->with('proveedor', $proveedor)
                    ->with('servicios', $servicios);
                break;
            case 'ID':
                $indumentarias = Indumentary::all();

                return view('compras.indumentrarias')
                    ->with('proveedor', $proveedor)
                    ->with('indumentarias', $indumentarias);
                break;
            default:
                abort(404);
                break;
        }
    }
}
