<?php

namespace App\Http\Controllers;

use App\MovimientoStockInsumo;
use Illuminate\Http\Request;

class MovimientoStockInsumoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MovimientoStockInsumo  $movimientoStockInsumo
     * @return \Illuminate\Http\Response
     */
    public function show(MovimientoStockInsumo $movimientoStockInsumo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MovimientoStockInsumo  $movimientoStockInsumo
     * @return \Illuminate\Http\Response
     */
    public function edit(MovimientoStockInsumo $movimientoStockInsumo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MovimientoStockInsumo  $movimientoStockInsumo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MovimientoStockInsumo $movimientoStockInsumo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MovimientoStockInsumo  $movimientoStockInsumo
     * @return \Illuminate\Http\Response
     */
    public function destroy(MovimientoStockInsumo $movimientoStockInsumo)
    {
        //
    }
}
