<?php

namespace App\Http\Controllers;

use App\Factura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class BalanceImpositivoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:balance-impositivo']);
    }

    public function index()
    {
        return view('balance-impositivo.index');
    }

    public function datatable(Request $request)
    {
        // return response()->json($request->all());
        if (isset($request->mes)) {

            $balanceTotal = DB::SELECT("SELECT (SELECT ifnull(sum(totalNeto), 0) from facturas WHERE month(fechaEmision) = ? and emitida_recibida = 'emitida' and borrado = 0) - (SELECT ifnull(sum(totalNeto), 0) from facturas WHERE month(fechaEmision) = ? and emitida_recibida = 'recibida' and borrado = 0) as 'balanceTotal'", array($request->mes, $request->mes));

            $balanceIva = DB::SELECT("SELECT (SELECT ifnull(sum(IVA), 0) from facturas WHERE month(fechaEmision) = ? and emitida_recibida = 'emitida' and borrado = 0) - (SELECT ifnull(sum(IVA), 0) from facturas WHERE month(fechaEmision) = ? and emitida_recibida = 'recibida' and borrado = 0) as 'balanceIva'", array($request->mes, $request->mes));

            $facturas = Factura::whereMonth('fechaEmision', '=', $request->mes)->get();
        } else if (isset($request->desde) or isset($request->hasta)) {

            $balanceTotal = DB::SELECT("SELECT (SELECT ifnull(sum(totalNeto), 0) from facturas WHERE fechaEmision BETWEEN ? and ? and emitida_recibida = 'emitida' and borrado = 0) - (SELECT ifnull(sum(totalNeto), 0) from facturas WHERE fechaEmision BETWEEN ? and ? and emitida_recibida = 'recibida' and borrado = 0) as 'balanceTotal'", array($request->desde, $request->hasta, $request->desde, $request->hasta));

            $balanceIva = DB::SELECT("SELECT (SELECT ifnull(sum(IVA), 0) from facturas WHERE fechaEmision BETWEEN ? and ? and emitida_recibida = 'emitida' and borrado = 0) - (SELECT ifnull(sum(IVA), 0) from facturas WHERE fechaEmision BETWEEN ? and ? and emitida_recibida = 'recibida' and borrado = 0) as 'balanceIva'", array($request->desde, $request->hasta, $request->desde, $request->hasta));

            $facturas = Factura::whereBetween('fechaEmision', [$request->desde, $request->hasta])->get();
        }

        return DataTables::of($facturas)
            ->with('balanceTotal', $balanceTotal)
            ->with('balanceIva', $balanceIva)
            ->make(true);

        // return response()->json($facturas);
    }
}
