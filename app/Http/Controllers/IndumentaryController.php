<?php

namespace App\Http\Controllers;

use App\Indumentary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class IndumentaryController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:insumos-servicios-indumentarias']);
    }

    public function index()
    {
        $indumentaria = Indumentary::all();

        return DataTables::of($indumentaria)
            ->addColumn('nombre', '{{ $nombre }}')
            ->addColumn('tipo_modelo', '{{ $tipo_modelo }}')
            ->addColumn('modelo', '{{ $modelo }}')
            ->addColumn('cantidad', '{{ $cantidad }}')
            ->addColumn('opciones', ' <div class="d-flex justify-content-end">
                        <a data-id="{{ $id }}" data-nombre="{{ $nombre }}" data-cantidad="{{ $cantidad }}" 
                        data-tipo={{ $tipo_modelo }} data-modelo = {{ $modelo }} 
                        data-toggle="modal" data-target="#modalIndumentariaAgregarEditar" id="editarIndumentaria" class="btn btn-primary btn-sm mr-2"> 
                                Editar <i class="fa fa-pen"></i>
                        </a>
        
                        <button  data-id="{{ $id }}" class="btn btnEliminarIndumentaria btn-danger btn-sm mr-2">
                            Borrar <i class="fa fa-trash"></i>
                        </button>
                  </div> ')
            ->rawColumns(['opciones'])
            ->toJson();
    }


    public function store(Request $request)
    {
        $rules = array(
            'nombre' => 'required',
            'tipo_modelo' => 'required',
            'modelo' => 'required',
            'cantidad' => 'required|numeric|digits_between:1,6',
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        Indumentary::create(array_merge($request->all(), ['estado' => 1, 'borrado' => 0]));

        return response()->json(['success' => 'Indumentaria agregada con éxito']);
    }

    public function update(Request $request, Indumentary $indumentary)
    {
        $rules = array(
            'nombre' => 'required',
            'tipo_modelo' => 'required',
            'modelo' => 'required',
            'cantidad' => 'required|digits_between:1,6',
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        if ($request->has('nombre')) {
            $indumentary->nombre = $request->nombre;
        }
        if ($request->has('tipo_modelo')) {
            $indumentary->tipo_modelo = $request->tipo_modelo;
        }
        if ($request->has('modelo')) {
            $indumentary->modelo = $request->modelo;
        }
        if ($request->has('cantidad')) {
            $indumentary->cantidad = $request->cantidad;
        }

        if (!$indumentary->isDirty()) {
            return response()->json(['error' => ['Se debe especificar al menos un valor diferente para actualizar']]);
        }

        $indumentary->save();

        return response()->json(['success' => 'Indumentaria modificada con éxito']);
    }

    public function delete(Indumentary $indumentary)
    {
        DB::table('indumentaries')->whereId($indumentary->id)->update(['borrado' => 1]);
    }

    public function indumentariasJson()
    {
        $indumentarias = Indumentary::get();
        return response()->json($indumentarias);
    }
}
