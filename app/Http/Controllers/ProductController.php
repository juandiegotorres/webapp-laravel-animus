<?php

namespace App\Http\Controllers;

use App\Product;
use App\VarianteProducto;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:productos']);
    }

    public function index()
    {
        if (!auth()->user()->can('products.index')) {
            abort(403, 'Acción no autorizada.');
        }
        return view('products.index');
    }

    public function datatable()
    {
        if (!auth()->user()->can('products.index')) {
            abort(403, 'Acción no autorizada.');
        }
        $productos = Product::all();

        return DataTables::of($productos)
            ->addColumn('nombreCompleto', '{{ $nombreCompleto }}')
            ->addColumn('descripcion', '{{ $descripcion }}')
            ->addColumn('variantes', '<button data-id="{{$id}}" class="btn btn-primary btn-sm d-block btnMostrarVariantes" data-toggle="modal" data-target="#modalMostrarVariantes" >Mostrar Variantes</button>')
            ->addColumn('opciones', function ($productos) {
                $html = '<div class="d-flex justify-content-end">';
                if (auth()->user()->can('products.edit')) {
                    $html .= '<a data-id="' . $productos->id . '" data-nombre="' . $productos->nombreCompleto . '" data-cantidad="' . $productos->cantidad . '" 
                            data-descripcion="' . $productos->descripcion . '" data-toggle="modal" data-target="#modalEditarProducto" id="editarProducto"> 
                                <button class="btn btn-primary btn-sm mr-2">
                                    <i class="fa fa-pen"></i>
                                </button>
                            </a>';
                }

                if (auth()->user()->can('products.delete')) {
                    $html .= '<button  data-id="' . $productos->id . '" class="btn btnEliminarProducto btn-danger btn-sm mr-2">
                            <i class="fa fa-trash"></i>
                        </button>';
                }

                return $html .= '</div>';
            })
            ->rawColumns(['opciones', 'variantes'])
            ->toJson();
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('products.create')) {
            abort(403, 'Acción no autorizada.');
        }

        if (!$request->has('cantidad')) {
            return response()->json(['error' => ['Debe agregar al menos una variante, presione el boton "+"']]);
        }

        $rules = array(
            'nombreCompleto' => 'required|min:3',
            'descripcion' => 'nullable'
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }
        return DB::transaction(function () use ($request) {
            try {
                $producto = Product::create([
                    'nombreCompleto' => $request['nombreCompleto'],
                    'descripcion' => $request['descripcion']
                ]);

                for ($i = 0; $i < count($request['cantidad']); $i++) {

                    VarianteProducto::create([
                        'product_id' => $producto->id,
                        'tipo' => $request['tipo'][$i],
                        'kg' => $request['kg'][$i],
                        'cantidad' => $request['cantidad'][$i],
                    ]);
                }
                return response()->json(['success' => 'Producto y variante(s) creados con éxito']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => [$e->getMessage()]]);
            }
        });
    }


    public function update(Request $request, Product $product)
    {
        if (!auth()->user()->can('products.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        // return response()->json($request);
        $rules = array(
            'nombreCompleto' => 'required|min:3',
            'descripcion' => 'nullable'
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        if ($request->has('nombreCompleto')) {
            $product->nombreCompleto = $request->nombreCompleto;
        }
        if ($request->has('cantidad')) {
            $product->cantidad = $request->cantidad;
        }

        $product->descripcion = $request->descripcion;

        if (!$product->isDirty()) {
            return response()->json(['error' => ['Se debe especificar al menos un valor diferente para actualizar']]);
        }

        $product->save();

        return response()->json(['success' => 'Producto modificado con éxito']);
    }

    public function updateVariante(Request $request, VarianteProducto $variante)
    {
        if (!auth()->user()->can('products.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        // return response()->json($request);
        $rules = array(
            'tipoProducto' => 'required',
            'kg' => 'required|numeric|gt:0',
            'cantidad' => 'required|numeric'
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        if ($request->has('tipoProducto')) {
            $variante->tipo = $request->tipoProducto;
        }
        if ($request->has('kg')) {
            $variante->kg = $request->kg;
        }
        if ($request->has('cantidad')) {
            $variante->cantidad = $request->cantidad;
        }

        if (!$variante->isDirty()) {
            return response()->json(['error' => ['Se debe especificar al menos un valor diferente para actualizar']]);
        }

        $variante->save();

        return response()->json(['success' => 'Variante modificada con éxito']);
    }

    public function mostrarVariantes($idproducto)
    {
        $variantes = DB::table('variantes_productos')->where('product_id', $idproducto)->where('variantes_productos.borrado', 0)
            ->join('products', 'products.id', 'variantes_productos.product_id')->select('variantes_productos.*', 'products.nombreCompleto')->get();
        return response()->json($variantes);
    }

    public function delete(Product $product)
    {
        if (!auth()->user()->can('products.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        DB::table('products')->whereId($product->id)->update(['borrado' => 1]);
        DB::table('variantes_productos')->where('product_id', $product->id)->update(['borrado' => 1]);
    }

    public function deleteVariante(VarianteProducto $variante)
    {
        if (!auth()->user()->can('products.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        DB::table('variantes_productos')->where('id', $variante->id)->update(['borrado' => 1]);
    }
}
