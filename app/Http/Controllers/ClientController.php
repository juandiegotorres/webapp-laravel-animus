<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\PriceList;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:administrador']);
    }

    public function index()
    {
        if (!auth()->user()->can('clients.index')) {
            abort(403, 'Acción no autorizada.');
        }

        $clients = Client::all();

        return view('clients.index')
            ->with('clients', $clients);
    }


    public function create()
    {
        if (!auth()->user()->can('clients.create')) {
            abort(403, 'Acción no autorizada.');
        }
        $provincias = DB::table('provinces')->get();
        $localidadesMza = DB::table('localities')->where('id_provincia', 14)->get();
        $listasPrecios = PriceList::select('nombre', 'id')->get();

        return view('clients.create')
            ->with('provincias', $provincias)
            ->with('listasPrecios', $listasPrecios)
            ->with('localidadesMza', $localidadesMza);
    }

    //Hago la validacion dentro de ClientRequest
    public function store(ClientRequest $request)
    {
        // return response()->json($request->all());
        if (!auth()->user()->can('clients.create')) {
            abort(403, 'Acción no autorizada.');
        }
        //Si el usuario no definio el transporte (dejando el campo vacio) automaticamente yo le asigno un valor
        if (empty($request['transporte'])) {
            $request['transporte'] = "No definido";
        }
        $request['estado'] = 1;
        $request['borrado'] = 0;

        $request['nombreCompleto'] =
            $request['personaEmpresa'] == 'persona'
            ? Client::nombreCompleto($request['nombre'], $request['apellido'])
            : $request['razonSocial'];

        Client::create($request->except(['nombre', 'apellido']));

        $request->session()->flash('status', 'Cliente creado con éxito');
        return redirect()->action('ClientController@index');
    }


    public function show(Client $client)
    {
        if (!auth()->user()->can('clients.show')) {
            abort(403, 'Acción no autorizada.');
        }

        $provinciaLocalidad = DB::table('localities')
            ->join('provinces', 'localities.id_provincia', '=', 'provinces.id')
            ->where('localities.id', $client->idLocalidad)
            ->get();

        $listaPrecio = DB::table('price_lists')->select('nombre')->where('id', '=', $client->price_list_id)->get();

        return view('clients.show')
            ->with('client', $client)
            ->with('listaPrecio', $listaPrecio)
            ->with('provinciaLocalidad', $provinciaLocalidad);
    }



    public function edit(Client $client)
    {
        if (!auth()->user()->can('clients.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        $provincias = DB::table('provinces')->get();
        //Consulta anidada para obtener todas las localidades de la provincia de donde ya tiene una localidad el cliente;
        //cliente->idLocalidad, localidad->id_provincia, seleccionar localidades que contenga el id de la provincia
        //del idLocalidad inicial

        $localidades = DB::table('localities')
            ->select('localities.id', 'localidad', 'id_provincia')
            ->join('provinces', 'localities.id_provincia', '=', 'provinces.id')
            ->whereRaw('provinces.id = (SELECT id_provincia FROM localities Where id =' . $client->idLocalidad . ')')
            ->get();

        $listasPrecios = PriceList::select('nombre', 'id')->get();


        $apellidoNombre = explode(", ", $client->nombreCompleto);
        if (count($apellidoNombre) > 1) {
            $client->apellido = $apellidoNombre[0];
            $client->nombre = $apellidoNombre[1];
        } else {
            $client->razonSocial = $apellidoNombre[0];
        }

        return view('clients.edit')
            ->with('client', $client)
            ->with('listasPrecios', $listasPrecios)
            ->with('provincias', $provincias)
            ->with('localidades', $localidades);
    }

    //La validacion se hace desde 'ClientRequest'
    public function update(ClientRequest $request, Client $client)
    {
        if (!auth()->user()->can('clients.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        if (empty($request['transporte'])) {
            $request['transporte'] = "No definido";
        }

        $request['nombreCompleto'] =
            $request['personaEmpresa'] == 'persona'
            ? Client::nombreCompleto($request['nombre'], $request['apellido'])
            : $request['razonSocial'];

        $client->update($request->except(['nombre', 'apellido']));

        $request->session()->flash('status', 'Cliente modificado con éxito');

        return redirect()->action('ClientController@index');
    }

    //Borrado Lógico
    public function delete(Client $client)
    {
        if (!auth()->user()->can('clients.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        DB::table('clients')
            ->where('id', $client->id)
            ->update(['borrado' => 1]);

        return redirect()->action('ClientController@index');
    }


    public function destroy(Client $client)
    {
    }


    //Obtiene las localidades de una determinada provincia 
    public function obtenerLocalidades($id)
    {
        $localidades = DB::table('localities')->where('id_provincia', $id)->get();
        return response()->json($localidades);
    }
}
