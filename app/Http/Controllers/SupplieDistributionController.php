<?php

namespace App\Http\Controllers;

use App\Supplie;
use App\Employee;
use App\SupplieDistribution;
use Illuminate\Http\Request;
use App\DetailSupplieDistribution;
use Illuminate\Support\Facades\DB;
use App\Exceptions\Handler;

class SupplieDistributionController extends Controller
{
    public function index($idEmpleado)
    {

        $empleado = Employee::where('id', $idEmpleado)->get();

        if ($empleado->isEmpty()) {
            return abort(404);
        }

        $insumos = Supplie::get();
        return view('supplie-distribution.index')
            ->with('insumos', $insumos)
            ->with('empleado', $empleado);
    }

    public function obtenerEmpleados()
    {
        $empleados = Employee::join('charges', 'employees.charge_id', '=', 'charges.id')
            ->select('employees.id', 'employees.nombreCompleto', 'dni', 'charges.nombre')
            ->get();

        return response()->json($empleados);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        // return response()->json('aca estoy');
        if (empty($request['insumos'])) {
            return response()->json(['error' => 'Debes entregar algo']);
            // $request->session()->flash('status', ['message' => 'Debes entregar algo', 'type' => 'error']);
            // return redirect()->back();
        }
        //Cuento la cantidad de registros que vienen dentro del array insumo-servicio y lo guardo en una variable
        //en este caso use insumo-servicio, pero puede ser cualquier array que venga del request(cantidad, precioUnitario o totalParcial)
        $registros = count($request['insumos']);

        //Comienzo la transaccion porque voy a efectuar insersiones en dos tablas distintas
        return DB::transaction(function () use ($request, $registros) {
            try {
                //Primeramente inserto el la compra general
                SupplieDistribution::create([
                    'employee_id' => $request['idEmpleado'],
                    'detalle' => $request['detalle'],
                    'fecha' => $request['fecha'],
                    'estado' => 1,
                    'borrado' => 0,
                ]);
                //Obtengo el id de esa compra general recien insertada
                $idEntrega = SupplieDistribution::select('id')->max('id');

                //Y ahora empiezo a recorrer el request para obtener las distintas compras que se hicieron al proveedor
                //Y las inserto en el detalle de la compra
                for ($i = 0; $i < $registros; $i++) {

                    $insumo = $request['insumos'][$i];
                    $cantidad = $request['cantidad'][$i];

                    DetailSupplieDistribution::create([
                        'supplie_distributions_id' => $idEntrega,
                        'supplie_id' => $insumo,
                        'cantidad' => $cantidad,
                        'estado' => 1,
                        'borrado' => 0,
                    ]);

                    //Actualiza el stocke de la base de datos. Greatest sirve para que si la cantidad a restar es mas grande que la
                    //actual se reemplazara con un 0 y no un numero negativo
                    DB::table('supplies')
                        ->where('id', $insumo)
                        ->update(['cantidad' => DB::raw('GREATEST(cantidad - ' . $cantidad . ', 0)')]);

                    // $request->session()->flash('status', ['message' => 'Entrega realizada con éxito', 'type' => 'success']);
                }
                return response()->json(['success' => 'Entrega realizada con éxito']);
            } catch (\Exception $e) {
                //En caso de haber algun erro se hace rollback
                DB::rollback();
                return response()->json(['error' => 'Error en el catch']);
                // $request->session()->flash('status', ['message' => 'Sucedio un error' . $e, 'type' => 'error']);
            }
        });
    }
}
