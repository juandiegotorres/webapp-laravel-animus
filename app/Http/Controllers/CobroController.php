<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Cobro;
use App\Cheque;
use App\Client;
use App\MetodoPago;
use App\DetalleCobro;
use App\BalanceVentasCobros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class CobroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['permission:cobros']);
    }

    public function index()
    {
        $clientes = Client::all();
        return view('cobros.index')->with('clientes', $clientes);
    }

    public function datatable(Request $request)
    {
        if ($request->idcliente == 0) {
            $cobros = Cobro::join('clients', 'cobros.client_id', '=', 'clients.id')->select('cobros.*', 'clients.nombreCompleto')->get();
        } else {
            $cobros = Cobro::join('clients', 'cobros.client_id', '=', 'clients.id')
                ->where('client_id', $request->idcliente)
                ->select('cobros.*', 'clients.nombreCompleto')
                ->get();
        }

        return DataTables::of($cobros)
            ->editColumn('nombreCompleto', '<a target="_blank" href="{{ route(\'clients.show\', [\'client\' => $client_id]) }}"> {{$nombreCompleto}}</a>')
            ->editColumn('montoTotal', '{{  "$ " . $montoTotal }}')
            ->editColumn('fecha', '{{ date("d-m-Y", strtotime($fecha)) }}')
            ->editColumn('iva', '{{ "$" .  number_format($iva, 2, ",", ".")  }}')
            ->addColumn('metodoPago', '<button data-id="{{$id}}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalMetodosPago">Ver métodos de pago</button>')
            ->addColumn('opciones', function ($cobros) {
                $html = '<div class="d-flex justify-content-end">';
                $html .=  '<a href="' . route('cobro.pdf', ['cobro' => $cobros->id]) . '" target="_blank" class="btn btn-warning btn-sm mr-2"><i class="fa fa-print"></i></a>';
                // $html .=  '<a class="btn btn-primary btn-sm mr-2" href="' . route('cobros.edit', ['cobro' => $cobros->id]) . '"> 
                //             <i class="fa fa-pen"></i>
                //         </a>';

                $html .= '<a  data-id="' . $cobros->id . '" class="btn btn-danger btn-sm mr-2 btnEliminarCobro">
                            <i class="fa fa-trash"></i>
                        </a>';

                return $html;
            })
            ->rawColumns(['nombreCompleto', 'metodoPago', 'opciones'])
            ->make(true);
    }

    public function filtroCliente(Request $request)
    {
    }

    public function seleccionarCliente()
    {
        $clientes = Client::all();

        session()->flash('desdeCobros', true);

        return view('vender.clientes');
    }

    public function create(Client $client)
    {
        $bancos = Bank::all();
        $metodosPago = MetodoPago::all();
        $cheques = Cheque::where(['estado' => 1])->orderBy('created_at', 'DESC')->get();

        return view('cobros.create')
            ->with('bancos', $bancos)
            ->with('metodosPago', $metodosPago)
            ->with('cheques', $cheques)
            ->with('client', $client);
    }


    public function store(Request $request)
    {
        if (empty($request['importe'])) {
            return response()->json(['error' => ['Debes agregar un metodo de pago']]);
        }

        $rules = array(
            'client_id' => 'required|numeric',
            'fecha' => 'required|date|before:2099-12-31|after:2000-01-01',
            'iva' => 'required|numeric',
            'observaciones' => 'nullable'
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        return DB::transaction(function () use ($request) {

            try {
                $cobro = Cobro::create([
                    'client_id' => $request['client_id'],
                    'fecha' => $request['fecha'],
                    'iva' => $request['iva'],
                    'montoTotal' => array_sum($request['importe']),
                    'observaciones' => $request['observaciones'],
                ]);

                for ($i = 0; $i < count($request['importe']); $i++) {
                    DetalleCobro::create([
                        'cobro_id' => $cobro->id,
                        'metodo_pago_id' => $request['id_metodo_pago'][$i],
                        'cheque_id' => $request['id_cheque'][$i],
                        'monto' => $request['importe'][$i],
                    ]);

                    if ($request['id_cheque'][$i] != null || $request['id_cheque'][$i] != '') {
                        DB::table('cheques')->where('id', $request['id_cheque'][$i])->update(['estado' => Cheque::CHEQUE_ENTREGADO]);
                    }
                }

                BalanceVentasCobros::create([
                    'client_id' => $request['client_id'],
                    'tipoMovimiento' => BalanceVentasCobros::COBRO,
                    'movimiento_id' => $cobro->id,
                    'debe' => $cobro->montoTotal,
                    'fecha' => $cobro->fecha,
                ]);

                $request->session()->flash('status', 'Cobro agregado con éxito');
                return response()->json(['success' => 'Cobro agregado']);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => [$e->getMessage()]]);
            }
        });
    }


    public function edit(Cobro $cobro)
    {
        return view('cobros.edit')->with('cobro', $cobro);
    }


    public function update(Request $request, Cobro $cobro)
    {
        $data = $request->validate([
            'provider_id' => 'required|numeric',
            'montoTotal' => 'required|numeric|digits_between:1,10',
            'fecha' => 'required|date|before:2099-12-31|after:2000-01-01',
            'metodoPago' => 'required',
            'observaciones' => 'nullable'
        ]);

        return DB::transaction(function () use ($data, $cobro, $request) {
            try {

                $request->montoTotal != $cobro->montoTotal ? $monto = $request->montoTotal : '';

                $cobro->update($data);

                if (isset($monto)) {
                    $balance = BalanceVentasCobros::where('movimiento_id', $cobro->id)
                        ->where('tipoMovimiento', BalanceVentasCobros::COBRO);

                    $balance->update([
                        'debe' => $monto,
                    ]);
                }
                return redirect()->action('CobroController@index');
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()]);
            }
        });
    }

    public function metodosPago($idcobro)
    {
        $metodosPago = DB::table('detalle_cobros')->where('cobro_id', $idcobro)
            ->join('metodos_pago', 'metodos_pago.id', 'detalle_cobros.metodo_pago_id')
            ->leftjoin('cheques', 'cheques.id', 'detalle_cobros.cheque_id')
            ->select('detalle_cobros.*', 'metodos_pago.nombre', 'cheques.nroCheque')
            ->get();

        return response()->json($metodosPago);
    }

    public function delete(Cobro $cobro)
    {
        return DB::transaction(function () use ($cobro) {
            try {

                $cobro->update(['borrado' => 1]);

                DB::table('balance_ventas_cobros')
                    ->where('movimiento_id', '=', $cobro->id)
                    ->where('tipoMovimiento', '=', BalanceVentasCobros::COBRO)
                    ->update(['borrado' => 1]);

                foreach ($cobro->detalleCobro as $detalle) {
                    if ($detalle->cheque_id != null) {
                        DB::table('cheques')->where('id', $detalle->cheque_id)->update(['estado' => Cheque::CHEQUE_AGREGADO]);
                    }
                }

                return response()->json(['success' => 'Cobro dado de baja con éxito']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()]);
            }
        });
    }
}
