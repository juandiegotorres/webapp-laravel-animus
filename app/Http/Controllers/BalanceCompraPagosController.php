<?php

namespace App\Http\Controllers;

use App\BalanceCompraPagos;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class BalanceCompraPagosController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:balance-compras']);
    }

    public function index()
    {
        return view('balance-compras-pagos.index');
    }

    public function datatable($idproveedor = null)
    {
        $balance = BalanceCompraPagos::where('provider_id', $idproveedor)->get();

        return DataTables::of($balance)
            ->editColumn('fecha', '{{ date("d-m-y", strtotime($fecha)) }}')
            ->make(true);
    }

    public function filtrarFechas($idproveedor = null, $fechaInicio = null, $fechaFin = null)
    {
        $error = array();
        if ($fechaInicio != null || $fechaFin != null) {
            if ($fechaInicio == 'vacio') {
                $error = [
                    'status' => 'error',
                    'message' => 'La fecha de inicio no puede estar vacia'
                ];
            } elseif ($fechaFin > '2099-12-31' || $fechaInicio < '1900-01-01') {
                $error = [
                    'status' => 'error',
                    'message' => 'Introduzca una fecha válida'
                ];
            } elseif ($fechaInicio == $fechaFin) {
                $error = [
                    'status' => 'error',
                    'message' => 'Las fechas no pueden ser iguales'
                ];
            } elseif ($fechaInicio > $fechaFin) {
                $error = [
                    'status' => 'error',
                    'message' => 'La fecha de inicio no puede ser mayor que la fecha de fin'
                ];
            }
            if (count($error) != 0) {
                return response()->json($error);
                exit();
            }
            $balance = BalanceCompraPagos::where('provider_id', $idproveedor)
                ->whereBetween('fecha', [$fechaInicio, $fechaFin])
                ->orderBy('created_at', 'DESC')
                ->get();

            return DataTables::of($balance)
                ->editColumn('fecha', '{{ date("d-m-y", strtotime($fecha)) }}')
                ->make(true);
        }
    }
}
