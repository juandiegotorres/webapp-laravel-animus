<?php

namespace App\Http\Controllers;

use DateTime;
use App\Supplie;
use App\Purchase;
use App\FaltanteFacturar;
use App\BalanceCompraPagos;
use App\BalanceVentasCobros;
use App\PurchaseIndumentary;
use App\PurchaseRawMaterial;
use Illuminate\Http\Request;
use App\PurchaseSupplieService;
use Yajra\DataTables\DataTables;
use App\DetailPurchaseRawMaterial;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PurchaseHistoryController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('purchase-history.index')) {
            abort(403, 'Acción no autorizada.');
        }
        return view('purchase-history.index');
    }

    public function obtenerHistorialInsumos(Request $request)
    {
        // if (!auth()->user()->can('purchase-history.is')) {
        //     abort(403, 'Acción no autorizada.');
        // }
        $comprasInsumoServicio = PurchaseSupplieService::historialCompra()->where('tipoCompra', 'I')->get();


        return DataTables::of($comprasInsumoServicio)
            ->editColumn('fecha', '{{date("d-m-Y", strtotime($fecha))}}')
            ->addColumn('proveedor', '<a target="_blank" href="{{ route(\'providers.show\', [\'provider\' => $provider_id]) }}"> {{$nombreCompleto}}</a>')
            ->addColumn('iva', ' $ {{ number_format($iva, 2, ",", ".") }}')
            ->addColumn('total', ' $ {{ number_format($total, 2, ",", ".") }}')
            ->addColumn('opciones', '<a href="{{ route(\'purchase-supplie-service.show\', [\'purchaseSupplieService\' => $id]) }}" class="btn btn-success btn-sm">' . ('Ver detalles') . '</a> <a data-id="{{$id}}" data-tipo="IS" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>')
            ->addColumn('created_at', '{{ $created_at }}')
            ->rawColumns(['opciones', 'proveedor'])
            ->toJson();
    }

    public function obtenerHistorialServicios(Request $request)
    {
        // if (!auth()->user()->can('purchase-history.is')) {
        //     abort(403, 'Acción no autorizada.');
        // }
        $comprasInsumoServicio = PurchaseSupplieService::historialCompra()->where('tipoCompra', 'S')->get();


        return DataTables::of($comprasInsumoServicio)
            ->editColumn('fecha', '{{date("d-m-Y", strtotime($fecha))}}')
            ->addColumn('proveedor', '<a target="_blank" href="{{ route(\'providers.show\', [\'provider\' => $provider_id]) }}"> {{$nombreCompleto}}</a>')
            ->addColumn('iva', ' $ {{ number_format($iva, 2, ",", ".") }}')
            ->addColumn('total', ' $ {{ number_format($total, 2, ",", ".") }}')
            ->addColumn('opciones', '<a href="{{ route(\'purchase-supplie-service.show\', [\'purchaseSupplieService\' => $id]) }}" class="btn btn-success btn-sm">' . ('Ver detalles') . '</a> <a data-id="{{$id}}" data-tipo="SV" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>')
            ->addColumn('created_at', '{{ $created_at }}')
            ->rawColumns(['opciones', 'proveedor'])
            ->toJson();
    }

    public function obtenerHistorialIndumentarias(Request $request)
    {
        // if (!auth()->user()->can('purchase-history.is')) {
        //     abort(403, 'Acción no autorizada.');
        // }
        $comprasInsumoServicio = PurchaseIndumentary::historialCompra()->get();


        return DataTables::of($comprasInsumoServicio)
            ->editColumn('fecha', '{{date("d-m-Y", strtotime($fecha))}}')
            ->editColumn('iva', ' $ {{ number_format($iva, 2, ",", ".") }}')
            ->editColumn('total', ' $ {{ number_format($total, 2, ",", ".") }}')
            ->addColumn('proveedor', '<a target="_blank" href="{{ route(\'providers.show\', [\'provider\' => $provider_id]) }}"> {{$nombreCompleto}}</a>')
            ->addColumn('opciones', '<a href="{{ route(\'purchase-indumentary.show\', [\'purchaseIndumentary\' => $id]) }}" class="btn btn-success btn-sm">Ver detalles</a> <a data-id="{{$id}}" data-tipo="ID" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>')
            ->rawColumns(['opciones', 'proveedor'])
            ->make(true);
        //{{ route(\'purchase-supplie-service.show\', [\'purchaseSupplieService\' => $id]) }}
        // ->addColumn('created_at', '{{ $created_at }}')
        // ->toJson();
    }

    public function obtenerHistorialMP(Request $request)
    {
        if (!auth()->user()->can('purchase-history.mp')) {
            abort(403, 'Acción no autorizada.');
        }
        $comprasMateriaPrima = PurchaseRawMaterial::historialCompra()->get();

        return DataTables::of($comprasMateriaPrima)
            ->editColumn('fecha', '{{date("d-m-Y", strtotime($fecha))}}')
            ->addColumn('proveedor', '<a target="_blank" href="{{ route(\'providers.show\', [\'provider\' => $provider_id]) }}"> {{$nombreCompleto}}</a>')
            ->editColumn('iva', ' $ {{ number_format($iva, 2, ",", ".") }}')
            ->addColumn('total', ' $ {{ number_format($total, 2, ",", ".") }}')
            ->addColumn('opciones', '<a href="{{ route(\'purchase-raw-material.show\', [\'purchaseRawMaterial\' => $id]) }}" class="btn btn-success btn-sm">' . ('Ver detalles') . '</a>
            <a data-id="{{$id}}" data-tipo="MP" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a> ')
            ->addColumn('created_at', '{{ $created_at }}')
            ->rawColumns(['opciones', 'proveedor'])
            ->toJson();
    }



    public function filtrar(Request $request)
    {
        // return response()->json($request->all());
        $fechaInicio = $request['fechaInicio'];
        $fechaFin = $request['fechaFin'];
        $idproveedor = $request['proveedor'];
        $tipoCompra = $request['tipoCompra'];
        // $error = array();

        $rules = array(
            'fechaInicio' => 'nullable|date|before:fechaFin|after:2000-01-01',
            'fechaFin' => 'required|date|before:2099-12-31|after:fechaInicio',
            'proveedor' => 'nullable|required_without:fechaInicio',
        );

        $error = Validator::make($request->all(), $rules, ['proveedor.required_without' => 'El campo proveedor es obligatorio cuando fecha inicio no está presente o viceversa.']);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }


        if ($fechaInicio == null) {
            //Solo filtrar por proveedor
            $data = '';
            switch ($tipoCompra) {
                case 'MP':
                    $data = PurchaseRawMaterial::join('providers', 'providers.id', '=', 'purchase_raw_materials.provider_id')->where('provider_id', $idproveedor)->get();
                    $ruta = '<a href="{{ route(\'purchase-raw-material.show\', [\'purchaseRawMaterial\' => $id]) }}" class="btn btn-success btn-sm">' . ('Ver detalles') . '</a> <a data-id="{{$id}}" data-tipo="MP" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a> ';
                    break;
                case 'IS':
                    $data = PurchaseSupplieService::where('tipoCompra', PurchaseSupplieService::COMPRA_INSUMO)->join('providers', 'providers.id', '=', 'purchase_supplie_services.provider_id')->where('provider_id', $idproveedor)->get();
                    $ruta = '<a href="{{ route(\'purchase-supplie-service.show\', [\'purchaseSupplieService\' => $id]) }}" class="btn btn-success btn-sm">' . ('Ver detalles') . '</a> <a data-id="{{$id}}" data-tipo="IS" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    break;
                case 'SV':
                    $data = PurchaseSupplieService::where('tipoCompra', PurchaseSupplieService::COMPRA_SERVICIO)->join('providers', 'providers.id', '=', 'purchase_supplie_services.provider_id')->where('provider_id', $idproveedor)->get();
                    $ruta = '<a href="{{ route(\'purchase-supplie-service.show\', [\'purchaseSupplieService\' => $id]) }}" class="btn btn-success btn-sm">' . ('Ver detalles') . '</a> <a data-id="{{$id}}" data-tipo="SV" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    break;
                case 'ID':
                    $data = PurchaseIndumentary::join('providers', 'providers.id', '=', 'purchase_indumentaries.provider_id')->where('provider_id', $idproveedor)->get();
                    $ruta = '<a href="{{ route(\'purchase-indumentary.show\', [\'purchaseIndumentary\' => $id]) }}" class="btn btn-success btn-sm">Ver detalles</a> <a data-id="{{$id}}" data-tipo="ID" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    break;
                default:
                    abort(404);
                    break;
            }
            //
        } elseif ($idproveedor == '') {
            //Solo debo filtrar entre fechas
            switch ($tipoCompra) {
                case 'MP':
                    $data = PurchaseRawMaterial::join('providers', 'providers.id', '=', 'purchase_raw_materials.provider_id')->whereBetween('fecha', [$fechaInicio, $fechaFin])->get();
                    $ruta = '<a href="{{ route(\'purchase-raw-material.show\', [\'purchaseRawMaterial\' => $id]) }}" class="btn btn-success btn-sm">' . ('Ver detalles') . '</a> <a data-id="{{$id}}" data-tipo="MP" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    break;
                case 'IS':
                    $data = PurchaseSupplieService::where('tipoCompra', PurchaseSupplieService::COMPRA_INSUMO)->join('providers', 'providers.id', '=', 'purchase_supplie_services.provider_id')->whereBetween('fecha', [$fechaInicio, $fechaFin])->get();
                    $ruta = '<a href="{{ route(\'purchase-supplie-service.show\', [\'purchaseSupplieService\' => $id]) }}" class="btn btn-success btn-sm">' . ('Ver detalles') . '</a> <a data-id="{{$id}}" data-tipo="IS" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    break;
                case 'SV':
                    $data = PurchaseSupplieService::where('tipoCompra', PurchaseSupplieService::COMPRA_SERVICIO)->join('providers', 'providers.id', '=', 'purchase_supplie_services.provider_id')->whereBetween('fecha', [$fechaInicio, $fechaFin])->get();
                    $ruta = '<a href="{{ route(\'purchase-supplie-service.show\', [\'purchaseSupplieService\' => $id]) }}" class="btn btn-success btn-sm">' . ('Ver detalles') . '</a> <a data-id="{{$id}}" data-tipo="SV" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    break;
                case 'ID':
                    $data = PurchaseIndumentary::join('providers', 'providers.id', '=', 'purchase_indumentaries.provider_id')->whereBetween('fecha', [$fechaInicio, $fechaFin])->get();;
                    $ruta = '<a href="{{ route(\'purchase-indumentary.show\', [\'purchaseIndumentary\' => $id]) }}" class="btn btn-success btn-sm">Ver detalles</a> <a data-id="{{$id}}" data-tipo="ID" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    break;
                default:
                    abort(404);
                    break;
            }
        } else {
            //Debo filtrar entre ambos
            switch ($tipoCompra) {
                case 'MP':
                    $data = PurchaseRawMaterial::join('providers', 'providers.id', '=', 'purchase_raw_materials.provider_id')->whereBetween('fecha', [$fechaInicio, $fechaFin])->where('provider_id', $idproveedor)->get();
                    $ruta = '<a href="{{ route(\'purchase-raw-material.show\', [\'purchaseRawMaterial\' => $id]) }}" class="btn btn-success btn-sm">' . ('Ver detalles') . '</a> <a data-id="{{$id}}" data-tipo="MP" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    break;
                case 'IS':
                    $data = PurchaseSupplieService::where('tipoCompra', PurchaseSupplieService::COMPRA_INSUMO)->join('providers', 'providers.id', '=', 'purchase_supplie_services.provider_id')->whereBetween('fecha', [$fechaInicio, $fechaFin])->where('provider_id', $idproveedor)->get();
                    $ruta = '<a href="{{ route(\'purchase-supplie-service.show\', [\'purchaseSupplieService\' => $id]) }}" class="btn btn-success btn-sm">' . ('Ver detalles') . '</a> <a data-id="{{$id}}" data-tipo="IS" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    break;
                case 'SV':
                    $data = PurchaseSupplieService::where('tipoCompra', PurchaseSupplieService::COMPRA_SERVICIO)->join('providers', 'providers.id', '=', 'purchase_supplie_services.provider_id')->whereBetween('fecha', [$fechaInicio, $fechaFin])->where('provider_id', $idproveedor)->get();
                    $ruta = '<a href="{{ route(\'purchase-supplie-service.show\', [\'purchaseSupplieService\' => $id]) }}" class="btn btn-success btn-sm">' . ('Ver detalles') . '</a> <a data-id="{{$id}}" data-tipo="SV" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    break;
                case 'ID':
                    $data = PurchaseIndumentary::join('providers', 'providers.id', '=', 'purchase_indumentaries.provider_id')->whereBetween('fecha', [$fechaInicio, $fechaFin])->where('provider_id', $idproveedor)->get();
                    $ruta = '<a href="{{ route(\'purchase-indumentary.show\', [\'purchaseIndumentary\' => $id]) }}" class="btn btn-success btn-sm">Ver detalles</a> <a data-id="{{$id}}" data-tipo="ID" class="btn btn-danger btnEliminar btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    break;
                default:
                    abort(404);
                    break;
            }
        }
        return DataTables::of($data)
            ->editColumn('fecha', '{{date("d-m-y", strtotime($fecha))}}')
            ->addColumn('proveedor', '<a target="_blank" href="{{ route(\'providers.show\', [\'provider\' => $provider_id]) }}"> {{$nombreCompleto}}</a>')
            ->addColumn('total', ' $ {{ number_format($total, 2, ",", ".") }}')
            ->addColumn('opciones', $ruta)
            ->rawColumns(['opciones', 'proveedor'])
            ->make(true);
    }

    public function imprimirCompraMP($idCompra)
    {
        $compra = PurchaseRawMaterial::findOrFail($idCompra)
            ->join('providers', 'purchase_raw_materials.provider_id', '=', 'providers.id')
            ->select('purchase_raw_materials.*', 'providers.nombreCompleto')
            ->get();
        $detalleCompra = DetailPurchaseRawMaterial::where('purchase_raw_material_id', '=', $idCompra)
            ->join('raw_materials', 'detail_purchase_raw_materials.raw_material_id', '=', 'raw_materials.id')
            ->select('detail_purchase_raw_materials.*', 'raw_materials.fruta', 'raw_materials.variedad')
            ->get();
        $pdf = PDF::loadView('pdf.compra', compact('detalleCompra', 'compra'));
        return $pdf->stream('compra.pdf');
    }

    public function imprimirCompraIS($idCompra)
    {
        $compra = PurchaseSupplieService::findOrFail($idCompra);
        $pdf = PDF::loadView('pdf.factura');
        return $pdf->stream('factura.pdf');
    }

    public function cancelarCompra(Request $request)
    {
        // return response()->json($request->all());
        return DB::transaction(function () use ($request) {
            try {
                switch ($request['tipoCompra']) {
                        //Cancelar compra de insumos
                    case 'MP':
                        try {
                            $compraMP = PurchaseRawMaterial::findOrFail($request['id']);
                            $compraMP->update(['borrado' => 1]);

                            foreach ($compraMP->detalleCompra as $detalle) {
                                //Si existe el faltante a facturar vuelvo a incrementar la cantidad
                                if (FaltanteFacturar::where('provider_id', $compraMP->provider_id)->where('raw_material_id', $detalle->raw_material_id)->exists()) {
                                    DB::table('faltante_facturar')
                                        ->where('provider_id', $compraMP->provider_id)
                                        ->where('raw_material_id', $detalle->raw_material_id)
                                        ->increment('cantidadKg', $detalle->cantidad);
                                }
                            }

                            return response()->json(['success' => 'Compra cancelada con éxito']);
                            //
                        } catch (\Exception $e) {
                            DB::rollback();
                            return response()->json(['error' => [$e->getMessage()]]);
                        }
                        break;
                    case 'IS':
                        //Guardo la compra en una variable
                        $compraInsumo = PurchaseSupplieService::findOrFail($request['id']);
                        //Pongo su estado en borrado
                        $compraInsumo->update(['borrado' => 1]);
                        //Recorro los detalles de compra
                        foreach ($compraInsumo->detalleCompra as $detalle) {
                            //Resto las cantidades correspondientes a cada insumo del que se esta cancelando la compra
                            DB::table('supplies')
                                ->where('id', $detalle->supplie_id)
                                ->update(['cantidad' => DB::raw('GREATEST(cantidad - ' . $detalle->cantidad . ', 0)')]);
                        }

                        BalanceCompraPagos::where('movimiento_id', $compraInsumo->id)
                            ->where('tipoCompra', BalanceCompraPagos::COMPRA_INSUMO)
                            ->update(['borrado' => 1]);

                        break;

                    case 'SV':
                        PurchaseSupplieService::whereId($request['id'])->update(['borrado' => 1]);

                        BalanceCompraPagos::where('movimiento_id', $request['id'])
                            ->where('tipoCompra', BalanceCompraPagos::COMPRA_SERVICIO)
                            ->update(['borrado' => 1]);

                        break;

                    case 'ID':

                        //Guardo la compra en una variable
                        $compraIndumentaria = PurchaseIndumentary::findOrFail($request['id']);
                        //Pongo su estado en borrado
                        $compraIndumentaria->update(['borrado' => 1]);
                        //Recorro los detalles de compra
                        foreach ($compraIndumentaria->detalleCompra as $detalle) {
                            //Res las cantidades correspondientes a cada indumentaria del que se esta cancelando la compra
                            DB::table('indumentaries')
                                ->where('id', $detalle->indumentary_id)
                                ->update(['cantidad' => DB::raw('GREATEST(cantidad - ' . $detalle->cantidad . ', 0)')]);
                        }

                        BalanceCompraPagos::where('movimiento_id', $compraIndumentaria->id)
                            ->where('tipoCompra', BalanceCompraPagos::COMPRA_INDUMETARIA)
                            ->update(['borrado' => 1]);

                        break;
                    default:
                        abort(404);
                        break;
                }
                return response()->json(['success' => 'Compra cancelada con éxito']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()]);
            }
        });
    }
}
