<?php

namespace App\Http\Controllers;

use App\Employee;
use Carbon\Carbon;
use App\SalaryPayment;
use Illuminate\Http\Request;
use App\EmployeeSalaryBalance;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\SalaryPaymentRequest;

class SalaryPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['permission:pago-sueldos']);
    }

    public function index()
    {
        if (!auth()->user()->can('salary-payments.index')) {
            abort(403, 'Acción no autorizada.');
        }
        $empleados = Employee::all();
        return view('salary-payments.index')
            ->with('empleados', $empleados);
    }

    public function indexDT()
    {
        if (!auth()->user()->can('salary-payments.index')) {
            abort(403, 'Acción no autorizada.');
        }
        $pagoSueldos = DB::table('salary_payments')->join('employees', 'employees.id', '=', 'salary_payments.employee_id')
            ->where('salary_payments.borrado', 0)->select('salary_payments.*', 'nombreCompleto')
            ->get();

        return DataTables::of($pagoSueldos)
            ->addColumn('empleado', '  <a target="_blank" href="{{ route(\'employees.show\', [\'employee\' => $employee_id]) }}"> {{$nombreCompleto}}</a>')
            ->addColumn('fecha', '{{ date("d-m-Y", strtotime($fecha)) }}')
            ->addColumn('concepto', '{{ $concepto }}')
            ->addColumn('total', '{{ $total }}')
            ->addColumn(
                'eliminar',
                function ($pagoSueldos) {
                    $html = '';
                    if (auth()->user()->can('salary-payments.edit')) {
                        $html .=
                            '<a href="' . route('pago-sueldo.pdf', ['pagoSueldo' => $pagoSueldos->id]) . '" target="_blank" class="btn btn-warning btn-sm mr-2"><i class="fa fa-print"></i></a> 
                            <a href="' . route('salary-payments.edit', ['salaryPayment' => $pagoSueldos->id]) . '" class="btn btn-primary btn-sm mr-2"><i class="fa fa-pen"></i></a>';
                    }
                    if (auth()->user()->can('salary-payments.delete')) {
                        $html .=
                            '<a data-id="' . $pagoSueldos->id . '" class="btn btn-danger btn-sm btnEliminar"><i class="fa fa-trash"></i></a>';
                    }
                    return $html;
                }

            )
            ->rawColumns(['empleado', 'eliminar'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->can('salary-payments.create')) {
            abort(403, 'Acción no autorizada.');
        }
        $empleados = Employee::select('nombreCompleto', 'id')->get();
        return view('salary-payments.create')
            ->with('empleados', $empleados);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalaryPaymentRequest $request)
    {
        if (!auth()->user()->can('salary-payments.create')) {
            abort(403, 'Acción no autorizada.');
        }
        DB::transaction(function () use ($request) {
            try {

                $request['estado'] = 1;
                $request['borrado'] = 0;
                SalaryPayment::create($request->all());

                $id = SalaryPayment::select('id')->max('id');

                EmployeeSalaryBalance::create([
                    'employee_id' => $request['employee_id'],
                    'tipoMovimiento' => EmployeeSalaryBalance::MOVIMIENTO_HABER,
                    'movimiento_id' => $id,
                    'haber' => $request['total'],
                    'fecha' => Carbon::now(),
                    'estado' => 1,
                    'borrado' => 0,
                ]);

                $request->session()->flash('status', 'Pago de sueldo agregado con éxito');
                //
            } catch (\Throwable $th) {
                Db::rollback();
                return response()->json('Error al agregar pago sueldo y movimiento cuenta. Error: ' . $th);
            }
        });

        return redirect()->action('SalaryPaymentController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalaryPayment  $salaryPayment
     * @return \Illuminate\Http\Response
     */
    public function show(SalaryPayment $salaryPayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalaryPayment  $salaryPayment
     * @return \Illuminate\Http\Response
     */
    public function edit(SalaryPayment $salaryPayment)
    {
        if (!auth()->user()->can('salary-payments.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        $empleados = Employee::select('nombreCompleto', 'id')->get();

        return view('salary-payments.edit')
            ->with('salaryPayment', $salaryPayment)
            ->with('empleados', $empleados);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalaryPayment  $salaryPayment
     * @return \Illuminate\Http\Response
     */
    public function update(SalaryPaymentRequest $request, SalaryPayment $salaryPayment)
    {
        if (!auth()->user()->can('salary-payments.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        return DB::transaction(function () use ($request, $salaryPayment) {
            try {

                //Pregunto si el total que viene en el request es diferente del que ya esta en la base de datos,
                //si es asi significa que se cambio la variable 'total' y no algun otro campo. Lo que significa que
                //tengo que actualizar la tabla de movimientos tambien
                if ($salaryPayment->total != $request['total']) {
                    //Selecciono la tabla de movimientos, si el tipo de movimiento es 'D' significa 'DEBE'(Sueldo), si es
                    //'H es 'HABER'(Pago de sueldo). En este caso estamos modificando el sueldo por lo que debo seleccionar el movmiento
                    //D y filtrar por el id del sueldo tambien, de esa forma puedo afectar la fila correctamente. Luego actualizo el monto
                    DB::table('employee_salary_balances')
                        ->where('tipoMovimiento', '=', 'H')
                        ->where('movimiento_id', '=', $salaryPayment->id)
                        ->update(['haber' => $request['total']]);
                }


                $salaryPayment->update($request->all());

                $request->session()->flash('status', 'Pago de sueldo modificado con éxito');

                return redirect()->action('SalaryPaymentController@index');
                //
            } catch (\Throwable $th) {
                DB::rollback();
                return response()->json('Error al modificar pago sueldo y movimiento cuenta. Error: ' . $th);
            }
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalaryPayment  $salaryPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalaryPayment $salaryPayment)
    {
        //
    }

    public function delete($salaryPayment)
    {
        if (!auth()->user()->can('salary-payments.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        return DB::transaction(function () use ($salaryPayment) {
            try {
                SalaryPayment::whereId($salaryPayment)->update(['borrado' => 1]);
                //Cuando doy de baja el pago de sueldo tambien debo dar de baja el movimiento de la tabla 'movimientos'
                //valga la redundancia.
                DB::table('employee_salary_balances')
                    ->where('tipoMovimiento', '=', 'H')
                    ->where('movimiento_id', '=', $salaryPayment)
                    ->update(['borrado' => 1]);
            } catch (\Throwable $th) {
                DB::rollback();
                return response()->json('Error al dar de baja el pago de sueldo y movimiento cuenta. Error: ' . $th);
            }
        });
    }

    public function filtrar(Request $request)
    {
        try {
            $fechaInicio = $request['fechaInicio'];
            $fechaFin = $request['fechaFin'];
            $idempleado = $request['empleado'];

            $rules = array(
                'fechaInicio' => 'nullable|date|before:fechaFin|after:2000-01-01',
                'fechaFin' => 'required|date|before:2099-12-31|after:fechaInicio',
                'empleado' => 'nullable|required_without:fechaInicio',
            );

            $error = Validator::make($request->all(), $rules, ['empleado.required_without' => 'El campo empleado es obligatorio cuando fecha inicio no está presente o viceversa.']);

            if ($error->fails()) {
                return response()->json(['error' => $error->errors()->all()]);
            }

            $query = DB::table('salary_payments')->join('employees', 'employees.id', '=', 'salary_payments.employee_id')
                ->where('salary_payments.borrado', 0)->select('salary_payments.*', 'nombreCompleto');


            if ($fechaInicio == null) {
                //Solo filtrar por empleado
                $pagoSueldos = $query->where('employee_id', $idempleado)->get();
                //
            } elseif ($idempleado == '') {
                //Solo debo filtrar entre fechas
                $pagoSueldos = $query->whereBetween('fecha', [$fechaInicio, $fechaFin])->get();
            } else {
                //Debo filtrar entre ambos
                $pagoSueldos = $query->whereBetween('fecha', [$fechaInicio, $fechaFin])->where('employee_id', $idempleado)->get();
            }

            return DataTables::of($pagoSueldos)
                ->addColumn('empleado', '  <a target="_blank" href="{{ route(\'employees.show\', [\'employee\' => $employee_id]) }}"> {{$nombreCompleto}}</a>')
                ->addColumn('fecha', '{{ date("d-m-Y", strtotime($fecha)) }}')
                ->addColumn('concepto', '{{ $concepto }}')
                ->addColumn('total', '{{ $total }}')
                ->addColumn(
                    'eliminar',
                    function ($pagoSueldos) {
                        $html = '';
                        if (auth()->user()->can('salary-payments.edit')) {
                            $html .=
                                '<a href="' . route('pago-sueldo.pdf', ['pagoSueldo' => $pagoSueldos->id]) . '" target="_blank" class="btn btn-warning btn-sm mr-2"><i class="fa fa-print"></i></a> 
                            <a href="' . route('salary-payments.edit', ['salaryPayment' => $pagoSueldos->id]) . '" class="btn btn-primary btn-sm mr-2"><i class="fa fa-pen"></i></a>';
                        }
                        if (auth()->user()->can('salary-payments.delete')) {
                            $html .=
                                '<a data-id="' . $pagoSueldos->id . '" class="btn btn-danger btn-sm btnEliminar"><i class="fa fa-trash"></i></a>';
                        }
                        return $html;
                    }

                )
                ->rawColumns(['empleado', 'eliminar'])
                ->toJson();
            //
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => [$e->getMessage()]]);
        }
    }
}
