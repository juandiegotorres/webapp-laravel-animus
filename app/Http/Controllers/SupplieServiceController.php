<?php

namespace App\Http\Controllers;

use App\DetailPurchaseSupplieService;
use App\Service;
use App\Supplie;
use App\OrdenCompra;
use App\PurchaseSupplieService;
use App\SupplieService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SupplieServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:insumos-servicios-indumentarias']);
    }

    private $rulesInsumos = array(
        'nombre' => 'required',
        'cantidad' => 'required|max:8',
    );

    private $rulesServicios =  array(
        'nombre' => 'required',
    );

    public function dtInsumos()
    {
        if (!auth()->user()->can('supplie-service.index')) {
            abort(403, 'Acción no autorizada.');
        }

        $insumos = Supplie::select('id', 'nombre', 'cantidad', 'created_at')->get();
        return DataTables::of($insumos)
            ->addColumn('nombre', '{{ $nombre }}')
            ->addColumn('cantidad', '{{ $cantidad }}')
            ->addColumn(
                'opciones',
                function ($insumos) {
                    $user = Auth::user();
                    $html = '<div class="d-flex justify-content-end">';
                    if ($user->can('supplie-service.edit')) {
                        $html .= '<a data-id="' . $insumos->id . '" data-nombre="' . $insumos->nombre . '" data-cantidad="' . $insumos->cantidad . '" data-toggle="modal" data-target="#modalInsumoServicioEditar"> 
                            <button class="btn btn-primary btn-sm mr-2">
                                Editar <i class="fa fa-pen"></i>
                            </button>
                        </a>';
                    }
                    if ($user->can('supplie-service.delete')) {
                        $html .= '<button  data-id="' . $insumos->id .  '" data-url="insumo" class="btn btnEliminar btn-danger btn-sm mr-2">
                            Borrar <i class="fa fa-trash"></i>
                        </button>';
                    }

                    return $html . '</div>';
                }
            )
            ->addColumn('created_ad', '{{ $created_at }}')
            ->rawColumns(['opciones'])
            ->toJson();
        // onclick="deleteItem(this)"
    }

    public function dtServicios()
    {
        if (!auth()->user()->can('supplie-service.index')) {
            abort(403, 'Acción no autorizada.');
        }

        $servicios = Service::all();
        return DataTables::of($servicios)
            ->addColumn('nombre', '{{ $nombre }}')
            ->addColumn('opciones', function ($servicios) {
                $html = '<div class="d-flex justify-content-end">';
                if (auth()->user()->can('supplie-service.edit')) {
                    $html .= '<a data-id="' . $servicios->id . '" data-nombre="' . $servicios->nombre . '" data-toggle="modal" data-target="#modalInsumoServicioEditar">
                            <button class="btn btn-primary btn-sm mr-2">
                                Editar <i class="fa fa-pen"></i>
                            </button>
                        </a>';
                }
                if (auth()->user()->can('supplie-service.delete')) {
                    $html .= '<button data-id="' . $servicios->id . '" data-url="servicio" class="btn btnEliminar btn-danger btn-sm mr-2">
                            Borrar <i class="fa fa-trash"></i>
                        </button>';
                }
                return $html .= '</div>';
            })
            ->addColumn('created_ad', '{{ $created_at }}')
            ->rawColumns(['opciones'])
            ->toJson();
    }

    public function index()
    {
        if (!auth()->user()->can('supplie-service.index')) {
            abort(403, 'Acción no autorizada.');
        }
        return view('supplies-services.index');
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('supplie-service.create')) {
            abort(403, 'Acción no autorizada.');
        }
        if ($request['tipo'] == 'IS') {

            $error = Validator::make($request->all(), $this->rulesInsumos);
            if ($error->fails()) {
                return response()->json(['error' => $error->errors()->all()]);
            }

            $data = array(
                'nombre' => $request['nombre'],
                'cantidad' => $request['cantidad'],
                'estado' => 1,
                'borrado' => 0,
            );

            Supplie::create($data);

            return response()->json(['success' => 'Insumo agregado con éxito']);
            //
        } elseif ($request['tipo'] == 'SV') {

            $error = Validator::make($request->all(), $this->rulesServicios);

            if ($error->fails()) {
                return response()->json(['errors' => $error->errors()->all()]);
            }

            $data = array(
                'nombre' => $request['nombre'],
                'estado' => 1,
                'borrado' => 0,
            );

            Service::create($data);

            return response()->json(['success' => 'Servicio agregado con éxito']);
        }
    }

    public function update(Request $request)
    {
        if (!auth()->user()->can('supplie-service.edit')) {
            abort(403, 'Acción no autorizada.');
        }

        if ($request['InsOServ'] == 'insumo') {

            $error = Validator::make($request->all(), $this->rulesInsumos);
            if ($error->fails()) {
                return response()->json(['error' => $error->errors()->all()]);
            }

            $data = array(
                'id' => $request['id'],
                'nombre' => $request['nombre'],
                'cantidad' => $request['cantidad'],
            );

            Supplie::where('id', $data['id'])->update(['nombre' => $data['nombre'], 'cantidad' => $data['cantidad']]);

            return response()->json(['success' => 'Insumo editado con éxito']);
            //
        } elseif ($request['InsOServ'] == 'servicio') {

            $error = Validator::make($request->all(), $this->rulesServicios);

            if ($error->fails()) {
                return response()->json(['error' => $error->errors()->all()]);
            }

            $data = array(
                'id' => $request['id'],
                'nombre' => $request['nombre'],
            );

            Service::where('id', $data['id'])->update(['nombre' => $data['nombre']]);

            return response()->json(['success' => 'Servicio editado con éxito']);
        }
    }

    public function deleteInsumo($insumo)
    {
        if (!auth()->user()->can('supplie-service.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        Supplie::whereId($insumo)->update(['borrado' => 1]);
    }

    public function deleteServicio($servicio)
    {
        if (!auth()->user()->can('supplie-service.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        Service::whereId($servicio)->update(['borrado' => 1]);
    }

    public function storeFromPurchase(Request $request)
    {
        if (!auth()->user()->can('supplie-service.create')) {
            abort(403, 'Acción no autorizada.');
        }
        $rules = array(
            'nombre' => 'required',
            'tipo' => 'required',
        );

        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return response()->json(array('error' => $error));
        }

        $data = array(
            'nombre' => $request['nombre'],
            'tipo' => $request['tipo'],
            'estado' => 1,
            'borrado' => 0,
        );

        SupplieService::create($data);

        return response()->json(['success' => 'Datos OK']);
    }
}
