<?php

namespace App\Http\Controllers;

use App\Bank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BankController extends Controller
{
    public function index()
    {
        $bancos = Bank::all();
        return response()->json($bancos);
    }

    public function store(Request $request)
    {
        $rules = array(
            'nombre' => 'required',
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        Bank::create(array_merge($request->all(), ['estado' => 1, 'borrado' => 0]));

        return response()->json(['success' => 'Banco agregado con éxito']);
    }

    public function update(Request $request, Bank $bank)
    {
        $rules = array(
            'nombre' => 'required',
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        if ($request->has('nombre')) {
            $bank->nombre = $request->nombre;
        }

        if (!$bank->isDirty()) {
            return response()->json(['error' => ['Se debe especificar al menos un valor diferente para actualizar']]);
        }

        $bank->save();

        return response()->json(['success' => 'Banco modificado con éxito']);
    }

    public function delete(Bank $bank)
    {
        //
    }
}
