<?php

namespace App\Http\Controllers;

use App\Charge;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ChargeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:cargos']);
    }

    public function index()
    {
        if (!auth()->user()->can('charges.index')) {
            abort(403, 'Acción no autorizada.');
        }

        $charges = Charge::where('borrado', 0)->get();
        return view('charges.index')->with('charges', $charges);
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('charges.create')) {
            abort(403, 'Acción no autorizada.');
        }
        if ($request['desde'] == 'desdeEmpleado') {
            $rules = array(
                'nombreCargo' => 'required|min:3'
            );

            $error = Validator::make($request->all(), $rules);

            if ($error->fails()) {
                return response()->json(['errors' => $error->errors()->all()]);
            }

            $data = array(
                'nombre' => $request['nombreCargo'],
                'estado' => 1,
                'borrado' => 0,
            );


            Charge::create($data);

            return response()->json(['success' => 'Cargo creado con éxito']);
        } else {
            $data = request()->validate([
                'nombreCargo' => 'required|min:3'
            ]);

            Charge::create([
                'nombre' => $data['nombreCargo'],
                'estado' => 1,
                'borrado' => 0
            ]);

            $request->session()->flash('status', 'Cargo creado con éxito');

            return redirect()->action('ChargeController@index');
        }
    }

    public function update(Request $request)
    {
        if (!auth()->user()->can('charges.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        if ($request['desde'] == 'desdeEmpleado') {
            $rules = array(
                'nombreCargoEditar' => 'required|min:3',
                'id' => 'required'
            );

            $error = Validator::make($request->all(), $rules);

            if ($error->fails()) {
                return response()->json(['errors' => $error->errors()->all()]);
            }

            $data = array(
                'nombre' => $request['nombreCargoEditar'],
                'id' => $request['id'],
                'estado' => 1,
                'borrado' => 0,
            );


            Charge::where('id', $data['id'])->update(['nombre' => $data['nombre']]);

            return response()->json(['success' => 'Cargo modificado con éxito']);
        } else {

            $data = request()->validate([
                'nombreCargoEditar' => 'required|min:3',
                'id' => 'required'
            ]);

            Charge::where('id', $data['id'])->update(['nombre' => $data['nombreCargoEditar']]);

            $request->session()->flash('status', 'Cargo modificado con éxito');

            return redirect()->action('ChargeController@index');
        }
    }

    public function delete(Charge $charge)
    {
        if (!auth()->user()->can('charges.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        DB::table('charges')
            ->where('id', $charge->id)
            ->update(['borrado' => 1]);
    }
}
