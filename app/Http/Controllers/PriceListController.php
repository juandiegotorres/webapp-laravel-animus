<?php

namespace App\Http\Controllers;

use App\PriceList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Contracts\DataTable;

class PriceListController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:lista-precios']);
    }

    public function index()
    {
        if (!auth()->user()->can('price-lists.index')) {
            abort(403, 'Acción no autorizada.');
        }
        return view('price-lists.index');
    }

    public function datatable()
    {
        if (!auth()->user()->can('price-lists.index')) {
            abort(403, 'Acción no autorizada.');
        }

        $listas = PriceList::all();

        return DataTables::of($listas)
            ->addColumn('nombre', '{{ $nombre }}')
            ->addColumn('fechaVigencia', '{{ date("d-m-Y", strtotime($fechaVigencia)) }}')
            ->addColumn('created_at', '{{ $created_at }}')
            ->editColumn('opciones', function ($listas) {
                $html = '<div class="d-flex justify-content-end">';
                if (auth()->user()->can('detail-price-lists.edit')) {
                    $html .= '<a href="' . route('detail-price-lists.index', ['priceList' => $listas->id]) . '" class="btn btn-success btn-sm mr-2">Editar precios</a>';
                    if (count($listas->detalleLista) == 0) {
                        $html .= '<button class="boton btn btn-secondary btn-sm mr-2" type="button" data-toggle="tooltip" data-placement="top" title="Esta lista no tiene ningun precio cargado"><i class="fa fa-print"></i></button>';
                    } else {
                        $html .= '<a href="' . route('lista-precio.pdf', ['priceList' =>  $listas->id]) . '" target="_blank" class="btn btn-warning btn-sm mr-2"><i class="fa fa-print"></i></a>';
                    }
                }
                if (auth()->user()->can('price-lists.edit')) {
                    $html .= '<a data-id="' . $listas->id . '" data-nombre="' . $listas->nombre . '" data-fecha_vigencia="' . $listas->fechaVigencia . '" 
                        data-toggle="modal" data-target="#modalListaAgregarEditar" id="editarLista"> 
                            <button class="btn btn-primary btn-sm mr-2">
                                <i class="fa fa-pen"></i>
                            </button>
                        </a>';
                }
                if (auth()->user()->can('price-lists.delete')) {
                    $html .= '<button  data-id="' . $listas->id . '" class="btn btnEliminarListaPrecio btn-danger btn-sm mr-2">
                            <i class="fa fa-trash"></i>
                        </button>';
                }
                return $html .= '</div>';
            })
            ->rawColumns(['opciones'])
            ->toJson();
    }

    public function store(Request $request)
    {
        $rules = array(
            'nombre' => 'required',
            'fechaVigencia' => 'required|date|date_format:Y-m-d|before:2099-12-31|after:today',
        );

        $error = Validator::make($request->all(), $rules, ['fechaVigencia.date_format' => 'Introduzca una fecha válida', 'fechaVigencia.after' => 'El campo fecha vigencia debe ser una fecha posterior a hoy.']);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        PriceList::create(array_merge($request->all(), ['estado' => 1, 'borrado' => 0]));

        return response()->json(['success' => 'Lista de precios creada con éxito']);
    }


    public function update(Request $request, PriceList $priceList)
    {
        if (!auth()->user()->can('price-lists.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        $rules = array(
            'nombre' => 'required',
            'fechaVigencia' => 'required|date|date_format:Y-m-d|before:2099-12-31|after:1900-01-01',
        );

        $error = Validator::make($request->all(), $rules, ['fechaVigencia.date_format' => 'Introduzca una fecha válida']);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        if ($request->has('nombre')) {
            $priceList->nombre = $request->nombre;
        }
        if ($request->has('fechaVigencia')) {
            $priceList->fechaVigencia = $request->fechaVigencia;
        }

        if (!$priceList->isDirty()) {
            return response()->json(['error' => ['Se debe especificar al menos un valor diferente para actualizar']]);
        }

        $priceList->save();

        return response()->json(['success' => 'Lista de precios modificada con éxito']);
    }

    public function delete(PriceList $priceList)
    {
        if (!auth()->user()->can('price-lists.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        DB::table('price_lists')->whereId($priceList->id)->update(['borrado' => 1]);
    }
}
