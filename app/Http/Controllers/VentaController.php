<?php

namespace App\Http\Controllers;

use App\Venta;
use App\Client;
use App\Product;
use App\PriceList;
use Carbon\Carbon;
use App\OrdenVenta;
use App\DetalleVenta;
use App\DetailPriceList;
use App\VarianteProducto;
use App\BalanceVentasCobros;
use Illuminate\Http\Request;
use App\StockProductosInsumos;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['permission:vender']);
    }

    public function index(Request $request)
    {
        // return response()->json($request->all());
        $cliente = Client::findOrFail($request->cliente);
        $listaPrecio = PriceList::findOrFail($request->listaPrecios);

        $preciosProductos = DB::table('variantes_productos')->where('variantes_productos.borrado', 0)
            ->leftJoin('detail_price_lists', 'variantes_productos.id', '=', 'detail_price_lists.variante_id')
            ->join('products', 'products.id', 'variantes_productos.product_id')
            ->where('detail_price_lists.borrado', 0)
            ->where('price_list_id', $request->listaPrecios)
            ->orWhereNull('price_list_id')
            ->select(
                'variantes_productos.id as idVariante',
                'variantes_productos.product_id',
                'variantes_productos.tipo',
                'variantes_productos.kg',
                'variantes_productos.cantidad',
                'variantes_productos.borrado AS estaBorrado',
                'products.nombreCompleto',
                'detail_price_lists.*'
            )
            ->get();
        // return $preciosProductos;
        $productos = Product::all();
        return view('vender.index')
            ->with('productos', $productos)
            ->with('cliente', $cliente)
            ->with('listaPrecio', $listaPrecio)
            ->with('preciosProductos', $preciosProductos);
    }

    public function showFromOrden(OrdenVenta $orden)
    {
        return view('vender.show-from-orden')
            ->with('orden', $orden);
    }

    public function storeFromOrden(Request $request)
    {
        // return response()->json($request->all());
        //===================== VALIDACION ==========================
        $arrayPrecios = $request['precio'];
        $arrayCantidades = $request['cantidad'];

        $hayError = 0;

        for ($i = 0; $i < count($arrayPrecios); $i++) {
            if (($arrayPrecios[$i] != null && $arrayCantidades[$i] == null) || ($arrayPrecios[$i] == null && $arrayCantidades[$i] != null)) {
                $hayError += 1;
            }
        }

        if ($hayError >= 1) {
            //Error para mostrar en el sweet alert
            return response()->json(['error' => ['Si se establece una cantidad se debe establecer un precio, o viceversa.']]);
        }

        $mensajesError = [
            'cantidad.*.required' => 'Los campos cantidad no pueden estar vacios',
            'precio.*.required' => 'Los campos precio no pueden estar vacios',
            'cantidad.*.numeric' => 'El campo cantidad debe ser numerico',
            'precio.*.numeric' => 'El campo precio debe ser numerico',
            'cantidad.*.not_in' => 'La cantidad debe ser mayor que 0',
            'precio.*.not_in' => 'El precio debe ser mayor que 0'
        ];
        //Valido los campos
        $error = Validator::make($request->all(), [
            'id_variante.*' => 'required',
            'cantidad.*' => 'numeric|required|min:0|not_in:0',
            'precio.*' => 'numeric|required|min:0|not_in:0'
        ], $mensajesError);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        //Comienzo la transaccion porque voy a efectuar insersiones en dos tablas distintas
        return DB::transaction(function () use ($request) {

            try {
                //Primeramente inserto el la compra general
                $venta = Venta::create([
                    'client_id' => $request['idCliente'],
                    'iva' => $request['iva'],
                    'total' => $request['total'],
                    'fecha' => $request['fecha'],
                ]);

                BalanceVentasCobros::create([
                    'client_id' => $request['idCliente'],
                    'tipoMovimiento' => BalanceVentasCobros::VENTA,
                    'movimiento_id' => $venta->id,
                    'haber' => $request['total'] + $request['iva'],
                    'fecha' => $request['fecha'],
                ]);

                //Y ahora empiezo a recorrer el request para obtener las distintas compras que se hicieron al proveedor
                //Y las inserto en el detalle de la compra
                for ($i = 0; $i < count($request['cantidad']); $i++) {
                    $variante = $request['id_variante'][$i];
                    $cantidad = $request['cantidad'][$i];
                    $precioProducto = $request['precio'][$i];

                    DetalleVenta::create([
                        'id_venta' => $venta->id,
                        'id_variante' => $variante,
                        'cantidad' => $cantidad,
                        'precio_producto' => $precioProducto,
                        'tipo_precio' => 'PP',
                        'subtotal' => ($cantidad * $precioProducto),
                        'estado' => 1,
                        'borrado' => 0,
                    ]);

                    //Cambio el estado de la orden de compra para que no se pueda borrar
                    DB::table('orden_ventas')->where('id', $request['id_orden'])
                        ->update(['estado' => 0]);

                    //Actualiza el stock de la base de datos. Greatest sirve para que si la cantidad a restar es mas grande que la
                    //actual se reemplazara con un 0 y no un numero negativo
                    DB::table('variantes_productos')
                        ->where('id', $variante)
                        ->update(['cantidad' => DB::raw('GREATEST(cantidad - ' . $cantidad . ', 0)')]);
                }


                return response()->json(['success' => 'Venta realizada con éxito']);
                // }
            } catch (\Exception $e) {
                //En caso de haber algun error se hace rollback
                DB::rollback();
                return response()->json(['error' => 'Sucedio un error: ' . $e->getMessage()]);
            }
        });
    }

    public function store(Request $request)
    {
        // return response()->json($request->all());

        if (empty($request['idVariante'])) {
            return response()->json(['error' => 'Debe vender algo']);
            // return redirect()->back();
        }
        //Cuento la cantidad de registros que vienen dentro del array idVariante y lo guardo en una variable
        //en este caso use idProduco, pero puede ser cualquier array que venga del request(cantidad, precioProducto o subtotal)
        $registros = count($request['idVariante']);

        //Comienzo la transaccion porque voy a efectuar insersiones en dos tablas distintas
        return DB::transaction(function () use ($request, $registros) {

            try {
                //Primeramente inserto el la compra general
                $venta = Venta::create([
                    'client_id' => $request['idCliente'],
                    'iva' => $request['iva'],
                    'total' => $request['total'],
                    'fecha' => $request['fecha'],
                ]);

                BalanceVentasCobros::create([
                    'client_id' => $request['idCliente'],
                    'tipoMovimiento' => BalanceVentasCobros::VENTA,
                    'movimiento_id' => $venta->id,
                    'haber' => $request['total'] + $request['iva'],
                    'fecha' => $request['fecha'],
                ]);

                //Y ahora empiezo a recorrer el request para obtener las distintas compras que se hicieron al proveedor
                //Y las inserto en el detalle de la compra
                for ($i = 0; $i < $registros; $i++) {
                    $variante = $request['idVariante'][$i];
                    $cantidad = $request['cantidad'][$i];
                    $precioProducto = $request['precioProducto'][$i];
                    $subtotal = $request['subtotal'][$i];

                    switch ($request['tipoPrecio'][$i]) {
                        case '0':
                            //0 = Precio x Unidad o Kg
                            $tipoPrecio = 'PKG';
                            break;
                        case '1':
                            //1 = Precio x caja
                            $tipoPrecio = 'PC';
                            break;
                        default:
                            //2 = Precio personalizado
                            $tipoPrecio = 'PP';
                            break;
                    }
                    DetalleVenta::create([
                        'id_venta' => $venta->id,
                        'id_variante' => $variante,
                        'cantidad' => $cantidad,
                        'precio_producto' => $precioProducto,
                        'tipo_precio' => $tipoPrecio,
                        'subtotal' => $subtotal,
                        'estado' => 1,
                        'borrado' => 0,
                    ]);


                    //Actualiza el stock de la base de datos. Greatest sirve para que si la cantidad a restar es mas grande que la
                    //actual se reemplazara con un 0 y no un numero negativo
                    DB::table('variantes_productos')
                        ->where('id', $variante)
                        ->update(['cantidad' => DB::raw('GREATEST(cantidad - ' . $cantidad . ', 0)')]);
                }

                // //Lo unico que devuelve esta funcion es un error en el catch. Entonces lo asigno a la variable
                // $error = Purchase::restarCantidadAFacturar($request['idProveedor'], $request['materia-prima'], $request['cantidad']);
                //Si la variable existe, es que hay un error por lo que lanzo una excepcion
                // if ($error) {
                //     throw new Exception($error);
                // } else {
                //     //Si todo esta bien retorno el estado success (exito)
                return response()->json(['success' => 'Venta realizada con éxito']);
                // }
            } catch (\Exception $e) {
                //En caso de haber algun error se hace rollback
                DB::rollback();
                return response()->json(['error' => 'Sucedio un error: ' . $e->getMessage()]);
            }
        });
    }


    public function delete(Venta $venta)
    {
        return DB::transaction(function () use ($venta) {
            try {
                $venta->update(['borrado' => 1]);
                BalanceVentasCobros::where('movimiento_id', $venta->id)
                    ->where('tipoMovimiento', BalanceVentasCobros::VENTA)
                    ->update(['borrado' => 1]);

                foreach ($venta->detalleVenta as $detalle) {
                    DB::table('variantes_productos')->where('id', $detalle->id_variante)->increment('cantidad', $detalle->cantidad);
                }

                return response()->json(['success' => 'Venta cancelada con éxito']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()]);
            }
        });
    }

    public function seleccionarCliente()
    {
        // $clientes = Client::all();
        $listasPrecios = PriceList::all();
        return view('vender.clientes')
            ->with('listasPrecios', $listasPrecios);
    }
}
