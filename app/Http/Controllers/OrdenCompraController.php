<?php

namespace App\Http\Controllers;

use App\Service;
use App\Supplie;
use App\Provider;
use App\Indumentary;
use App\OrdenCompra;
use App\DetalleOrdenCompra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class OrdenCompraController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:compras']);
    }

    public function index()
    {
        return view('orden-compra.index');
    }

    public function listar()
    {
        $proveedores = Provider::all();
        return view('orden-compra.listar')
            ->with('proveedores', $proveedores);
    }

    public function datatable($data)
    {
        return DataTables::of($data)
            ->editColumn('fecha', '{{date("d-m-Y" , strtotime($fecha))}}')
            ->editColumn('tipoOrden', function ($data) {
                switch ($data->tipoOrden) {
                    case 'ID':
                        return 'Indumentaria';
                        break;
                    case 'IS':
                        return 'Insumo';
                        break;
                    default:
                        return 'No definido';
                        break;
                }
            })
            ->addColumn('opciones', function ($data) {
                $html = '';
                $html .= '<a href="' . route('orden-compra.show', ['orden' => $data->id]) . '" class="btn btn-success btn-sm mr-2"><i class="fa fa-info-circle" aria-hidden="true"></i></a>';
                //Si el estado es 0 significa que ya converti la orden en una compra
                if ($data->estado == 0) {
                    $html .= '<a class="btn btn-secondary btn-sm mr-2 boton">Compra realizada</a>';
                    $html .= '<button class="boton btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="left" title="No se puede eliminar una orden que ya se convirtió en compra"><i class="fa fa-trash"></i></button>';
                } else {
                    $html .= '<a href="' . route('purchase-supplie.show-from-orden', ['orden' => $data->id]) . '" class="btn btn-primary btn-sm mr-2">Comprar</a>';
                    $html .= '<a data-id="' . $data->id . '" class="btn btn-sm btn-danger btnEliminarOrden"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                }
                return $html;
            })
            ->rawColumns(['opciones'])
            ->make(true);
    }

    public function dt()
    {
        $ordenes = DB::table('orden_compras')->where('orden_compras.borrado', 0)
            ->join('providers', 'providers.id', 'orden_compras.provider_id')
            ->select('orden_compras.*', 'providers.nombreCompleto')->get();

        return $this->datatable($ordenes);
    }

    public function filtrar(Request $request)
    {
        // return response()->json($request->all());
        $fechaInicio = $request['fechaInicio'];
        $fechaFin = $request['fechaFin'];
        $idproveedor = $request['proveedor'];


        $rules = array(
            'fechaInicio' => 'nullable|date|before:fechaFin|after:2000-01-01',
            'fechaFin' => 'required|date|before:2099-12-31|after:fechaInicio',
            'proveedor' => 'nullable|required_without:fechaInicio',
        );

        $error = Validator::make($request->all(), $rules, ['proveedor.required_without' => 'El campo proveedor es obligatorio cuando fecha inicio no está presente o viceversa.']);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        $query = DB::table('orden_compras')->join('providers', 'providers.id', '=', 'orden_compras.provider_id')
            ->where('orden_compras.borrado', 0)->select('orden_compras.*', 'nombreCompleto')->orderBy('created_at', 'DESC');

        if ($fechaInicio == null) {
            //Solo filtrar por empleado
            $ordenes = $query->where('provider_id', $idproveedor)->get();

            //
        } elseif ($idproveedor == '') {
            //Solo debo filtrar entre fechas
            $ordenes = $query->whereBetween('fecha', [$fechaInicio, $fechaFin])->get();
        } else {
            //Debo filtrar entre ambos
            $ordenes = $query->whereBetween('fecha', [$fechaInicio, $fechaFin])->where('provider_id', $idproveedor)->get();
        }

        return $this->datatable($ordenes);
    }

    public function show(OrdenCompra $orden)
    {
        $query = DB::table('detalle_orden_compras')->where('id_orden', $orden->id);

        if ($orden->tipoOrden == 'IS') {
            $detalles = $query->join('supplies', 'supplies.id', 'detalle_orden_compras.id_objeto')
                ->select('detalle_orden_compras.*', 'supplies.nombre')->get();
        } else if ($orden->tipoOrden == 'ID') {
            $detalles = $query->join('indumentaries', 'indumentaries.id', 'detalle_orden_compras.id_objeto')
                ->select('detalle_orden_compras.*', DB::raw("CONCAT(indumentaries.nombre, ', ', indumentaries.modelo) AS nombre"))->get();
        }

        return view('orden-compra.show')
            ->with('orden', $orden)
            ->with('detalles', $detalles);
    }

    public function showFromOrden(OrdenCompra $orden)
    {

        $query = DB::table('detalle_orden_compras')->where('id_orden', $orden->id);

        if ($orden->tipoOrden == 'IS') {
            $detalles = $query->join('supplies', 'supplies.id', 'detalle_orden_compras.id_objeto')
                ->select('detalle_orden_compras.*', 'supplies.nombre')->get();
        } else if ($orden->tipoOrden == 'ID') {
            $detalles = $query->join('indumentaries', 'indumentaries.id', 'detalle_orden_compras.id_objeto')
                ->select('detalle_orden_compras.*', DB::raw("CONCAT(indumentaries.nombre, ', ', indumentaries.modelo) AS nombre"))->get();
        }

        return view('purchase-supplie-service.show-from-orden')
            ->with('orden', $orden)
            ->with('detalles', $detalles);
    }

    public function create($tipoCompra, $idProveedor)
    {
        $proveedor = Provider::findOrFail($idProveedor);
        switch ($tipoCompra) {
            case 'IS':

                $insumos = Supplie::all();

                return view('orden-compra.insumos-pre-orden')
                    ->with('proveedor', $proveedor)
                    ->with('insumos', $insumos);

                break;
            case 'SV':

                $servicios = Service::all();

                return view('orden-compra.servicios-pre-orden')
                    ->with('proveedor', $proveedor)
                    ->with('servicios', $servicios);
                break;
            case 'ID':
                $indumentarias = Indumentary::all();

                return view('orden-compra.indumentarias-pre-orden')
                    ->with('proveedor', $proveedor)
                    ->with('indumentarias', $indumentarias);
                break;
            default:
                abort(404);
                break;
        }
    }

    public function storeInsumo(Request $request)
    {
        // return response()->json($request->all());
        if (empty($request['insumo-servicio'])) {
            $request->session()->flash('status', ['message' => 'Debes ordenar algo', 'type' => 'error']);
            return redirect()->back();
        }

        list($insumos, $cantidades) = $this->valoresDuplicados($request['insumo-servicio'], $request['cantidad']);

        DB::transaction(function () use ($request, $insumos, $cantidades) {
            try {

                $orden = OrdenCompra::create([
                    'provider_id' => $request['idProveedor'],
                    'tipoOrden' => 'IS',
                    'fecha' => $request['fecha'],
                    'detalle' => $request['detalle'],
                ]);

                for ($i = 0; $i < count($insumos); $i++) {
                    $insumo = $insumos[$i];
                    $cantidad = $cantidades[$i];


                    DetalleOrdenCompra::create([
                        'id_orden' => $orden->id,
                        'id_objeto' => $insumo,
                        'cantidad' => $cantidad,
                    ]);

                    $request->session()->flash('status', ['message' => 'Orden de compra realizada con éxito', 'type' => 'success']);
                }
            } catch (\Exception $e) {
                //En caso de haber algun erro se hace rollback
                DB::rollback();
                $request->session()->flash('status', ['message' => 'Sucedio un error' . $e->getMessage(), 'type' => 'error']);
            }
        });
        return redirect()->action('OrdenCompraController@listar');
    }

    public function storeIndumentaria(Request $request)
    {
        if (empty($request['indumentarias'])) {
            $request->session()->flash('status', ['message' => 'Debes ordenar algo', 'type' => 'error']);
            return redirect()->back();
        }

        list($indumentarias, $cantidades) = $this->valoresDuplicados($request['indumentarias'], $request['cantidad']);

        DB::transaction(function () use ($request, $indumentarias, $cantidades) {
            try {

                $orden = OrdenCompra::create([
                    'provider_id' => $request['idProveedor'],
                    'tipoOrden' => 'ID',
                    'fecha' => $request['fecha'],
                    'detalle' => $request['detalle'],
                ]);

                for ($i = 0; $i < count($indumentarias); $i++) {
                    $indumentaria = $indumentarias[$i];
                    $cantidad = $cantidades[$i];


                    DetalleOrdenCompra::create([
                        'id_orden' => $orden->id,
                        'id_objeto' => $indumentaria,
                        'cantidad' => $cantidad,
                    ]);

                    $request->session()->flash('status', ['message' => 'Orden de compra realizada con éxito', 'type' => 'success']);
                }
            } catch (\Exception $e) {
                //En caso de haber algun erro se hace rollback
                DB::rollback();
                $request->session()->flash('status', ['message' => 'Sucedio un error' . $e->getMessage(), 'type' => 'error']);
            }
        });
        return redirect()->action('OrdenCompraController@listar');;
    }

    public function storeServicio(Request $request)
    {
        if (empty($request['insumo-servicio'])) {
            $request->session()->flash('status', ['message' => 'Debes ordenar algo', 'type' => 'error']);
            return redirect()->back();
        }

        DB::transaction(function () use ($request) {
            try {

                $orden = OrdenCompra::create([
                    'provider_id' => $request['idProveedor'],
                    'tipoOrden' => 'SV',
                    'fecha' => $request['fecha'],
                    'detalle' => $request['detalle'],
                ]);

                for ($i = 0; $i < count($request['insumo-servicio']); $i++) {
                    DetalleOrdenCompra::create([
                        'id_orden' => $orden->id,
                        'id_objeto' => $request['insumo-servicio'][$i],
                    ]);

                    $request->session()->flash('status', ['message' => 'Orden de compra realizada con éxito', 'type' => 'success']);
                }
            } catch (\Exception $e) {
                //En caso de haber algun erro se hace rollback
                DB::rollback();
                $request->session()->flash('status', ['message' => 'Sucedio un error' . $e->getMessage(), 'type' => 'error']);
            }
        });
        return redirect()->route('orden-compra.listar');
    }

    public function delete(OrdenCompra $orden)
    {
        try {
            $orden->borrado = 1;
            $orden->save();
            return response()->json(['success' => 'Orden cancelada']);
            //
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => $e->getMessage()]);
        }
    }


    public function valoresDuplicados($ids, $cantidades)
    {
        // Esta funcion surge del problema que desde el front, el usuario haya agregado una materia prima mas de una vez con distintas cantidades a comprar/ingresar respectivamente
        // Por lo que esta funcion lo que hace es recorrer el array y sumar todas las cantidades de las materias primas que estan repetidas
        // Obtengo los valores unicos dentro del array de ids de materias primas, y los valores duplicados
        $valoresUnicos = array_unique($ids);
        $valoresDuplicados = array_diff_assoc($ids, $valoresUnicos);

        $totalDuplicadas = 0;
        $suma = array();

        foreach ($valoresUnicos as $idUnico => $valorUnico) {
            //Agrego al array un registro con el id de la materia prima y la cantidad
            $suma[$valorUnico] = $cantidades[$idUnico];

            //Recorro el array de los valores que estan duplicados
            foreach ($valoresDuplicados as $idDuplicado => $valorDuplicado) {
                //Si se repiten con el array de ids unicos
                if ($valorUnico == $valorDuplicado) {
                    //Si no existe el valor dentro de larray de las sumas  
                    if (isset($suma[$valorDuplicado])) {
                        $suma[$valorUnico] += $cantidades[$idDuplicado];
                    } else {
                        $totalDuplicadas = 0;
                        $totalDuplicadas += $cantidades[$idUnico];
                        $suma[$valorUnico] = $totalDuplicadas += $cantidades[$idDuplicado];
                    }
                }
            }
        }

        //Separo los valores y los vuelvo a asignar a las variables iniciales
        $materiasPrimas = array_keys($suma);
        $cantidades = array_values($suma);

        return array($materiasPrimas, $cantidades);
    }
}
