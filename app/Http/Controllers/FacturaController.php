<?php

namespace App\Http\Controllers;

use App\Factura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\FacturaRequest;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['permission:facturas']);
    }

    public function index()
    {
        return view('facturas.index');
    }

    public function datatable(Request $request)
    {
        // return response()->json($request->all());

        switch ($request['filtro']) {
            case 'recibida':
                $facturas = Factura::where('emitida_recibida', '=', 'recibida')->get();
                break;
            case 'emitida':
                $facturas = Factura::where('emitida_recibida', '=', 'emitida')->get();
                break;
            case 'todas':
                $facturas = Factura::all();
                break;
        }

        return DataTables::of($facturas)
            ->editColumn('fechaEmision', '{{date("d-m-Y", strtotime($fechaEmision))}}')
            ->editColumn('emitida_recibida', '{{ ucfirst($emitida_recibida) }}')
            ->addColumn('opciones', function ($facturas) {
                $html = '<div class="d-flex justify-content-end">';
                if (auth()->user()->can('facturas.show')) {
                    $html .= '<a data-id="' . $facturas->id . '" class="btn btn-success btn-sm mr-2" data-toggle="modal" data-target="#modalMostrarFactura"> 
                            <i class="fa fa-info-circle"></i>
                        </a>';
                }

                if (auth()->user()->can('facturas.edit')) {
                    $html .=  '<a class="btn btn-primary btn-sm mr-2" href="' . route('facturas.edit', ['factura' => $facturas->id]) . '"> 
                            <i class="fa fa-pen"></i>
                        </a>';
                }

                if (auth()->user()->can('facturas.delete')) {
                    $html .= '<a  data-id="' . $facturas->id . '" class="btn btn-danger btn-sm mr-2 btnEliminarFactura">
                            <i class="fa fa-trash"></i>
                        </a>';
                }
                return $html;
            })
            ->rawColumns(['opciones'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($emitida_recibida)
    {
        if ($emitida_recibida == 'emitida') {
            $campoValidado = 'emitida';
        } elseif ($emitida_recibida == 'recibida') {
            $campoValidado = 'recibida';
        } else {
            abort(400, 'Parámetro inválido');
        }

        return view('facturas.create')->with('emitida_recibida', $campoValidado);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FacturaRequest $request)
    {
        if ($request->hasFile('pdf')) {
            $pdf = $request->pdf;
            $nombrePdf = $pdf->getClientOriginalName();
            $nombrePdf = time() . '_' . str_replace(' ', '_', $nombrePdf);

            $path = $pdf->storeAs('pdf_facturas', $nombrePdf, 'public');
            // dd($path);
        } else {
            $path = null;
        }

        $iva = (($request->totalNeto * $request->IVA) / 100);

        $data = array_merge(
            $request->except(['IVA']),
            [
                'IVA' => $iva,
                'total' => ($iva + $request->totalNeto),
                'pdf' => $path
            ]
        );

        Factura::create($data);

        $request->session()->flash('status', 'Facutra creada con éxito');
        return redirect()->action('FacturaController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!auth()->user()->can('facturas.show')) {
            abort(403, 'Acción no autorizada.');
        }
        $factura = Factura::find($id);
        return response()->json($factura);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function edit(Factura $factura)
    {
        if (!auth()->user()->can('facturas.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        return view('facturas.edit')->with('factura', $factura);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function update(FacturaRequest $request, Factura $factura)
    {
        if ($request->hasFile('pdf')) {
            $pdf = $request->pdf;
            $nombrePdf = $pdf->getClientOriginalName();
            $nombrePdf = time() . '_' . str_replace(' ', '_', $nombrePdf);

            $pathNuevo = $pdf->storeAs('pdf_facturas', $nombrePdf, 'public');

            $pathViejo = $factura->pdf;
        } else {
            $pathNuevo = null;
            $pathViejo = null;
        }

        $iva = (($request->totalNeto * $request->IVA) / 100);

        $data = array_merge(
            $request->except(['_method', '_token', 'IVA']),
            [
                'IVA' => $iva,
                'total' => ($iva + $request->totalNeto),
                'pdf' => $pathNuevo
            ]
        );

        // dd($path, $pathViejo);

        if ($factura->update($data)) {
            if (Storage::disk('public')->exists($pathViejo)) {
                Storage::disk('public')->delete($pathViejo);
            }
            $request->session()->flash('status', 'Facutra modificada con éxito');
        } else {
            $request->session()->flash('status', 'No se pudo modificar :(');
        }
        return redirect()->action('FacturaController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function delete(Factura $factura)
    {
        if (!auth()->user()->can('facturas.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        DB::table('facturas')->whereId($factura->id)->update(['borrado' => 1]);
    }
}
