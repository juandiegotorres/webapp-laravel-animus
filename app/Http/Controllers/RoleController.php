<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Crypt;
use Spatie\Permission\Models\Permission;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{

    public function index()
    {
        return view('roles.index');
    }


    public function dtRoles()
    {
        $roles = Role::all();

        return DataTables::of($roles)
            ->editColumn('name', '{{ ucfirst($name) }}')
            ->addColumn('opciones', function ($roles) {
                $html = '<div class="text-right">';
                if ($roles->id == 1) {
                    $html .= '<button class="btn btn-secondary btn-sm mr-1" disabled>Todos los permisos</button>';
                } else {
                    $html .= '<a href="' . route('permisos.index', ['idRol' => Crypt::encrypt($roles->id)]) . '" class="btn btn-success btn-sm mr-1">Administrar permisos</a>
                                <a data-id="' . $roles->id . '" data-name="' . $roles->name . '" class="btn btn-primary btn-sm mr-1" data-toggle="modal" data-target="#modalNuevoRol" id="editarRol">Editar nombre</a>
                                <a data-id="' . $roles->id . '" class="btn btn-danger btn-sm btnEliminar" disabled>Eliminar Rol</a>';
                }
                return $html .= '</div>';
            })
            ->rawColumns(['opciones'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
        ], [], ['name' => 'nombre']);

        if ($validator->passes()) {
            Role::create(['name' => strtolower($request->name)]);
            return response()->json(['success' => 'Rol agregado con éxito. ']);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }


    public function update(Role $role, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
        ], [], ['name' => 'nombre']);

        if ($validator->passes()) {
            $role->update(['name' => strtolower($request->name)]);
            return response()->json(['success' => 'Rol modificado con éxito. ']);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }


    public function destroy(Role $role)
    {
        if ($role->delete()) {
            return response()->json(['success' => 'Rol eliminado']);
        }
        // return redirect()->action('RoleController@index')->with('info', 'El rol se elminó correctamente');
    }

    public function mostrarPermisos($idRol)
    {
        $rol = Role::findOrFail(Crypt::decrypt($idRol));
        $permisos = Permission::all();
        return view('roles.permisos')->with('rol', $rol)->with('permisos', $permisos);
    }

    public function editarPermisos(Role $role, Request $request)
    {
        $role->syncPermissions($request->permissions);

        return redirect()->action('RoleController@index')->with('info', 'Permisos actualizados con éxito');
    }
}
