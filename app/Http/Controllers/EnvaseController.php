<?php

namespace App\Http\Controllers;

use App\Envase;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class EnvaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['permission:envases']);
    }

    public function index()
    {
        return view('envases.index');
    }

    public function datatable()
    {
        $envases = Envase::all();

        return DataTables::of($envases)
            ->addColumn('opciones', '<div class="d-flex justify-content-end">
                        <a data-id="{{ $id }}" data-nombre="{{ $nombre }}" data-peso="{{ $peso }}" data-cantidad="{{ $cantidad }}"
                        data-toggle="modal" data-target="#modalEnvaseAgregarEditar" id="editarEnvase"> 
                            <button class="btn btn-primary btn-sm mr-2">
                                Editar <i class="fa fa-pen"></i>
                            </button>
                        </a>
        
                        <button  data-id="{{ $id }}" class="btn btnEliminarEnvase btn-danger btn-sm mr-2">
                            Borrar <i class="fa fa-trash"></i>
                        </button>
                  </div> ')
            ->rawColumns(['opciones'])
            ->make(true);
    }

    public function store(Request $request)
    {
        // return response()->json($request->all());
        $rules = array(
            'nombre' => 'required',
            'cantidad' => 'required|numeric',
            'peso' => 'required|numeric',
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        Envase::create($request->all());

        return response()->json(['success' => 'Envase agregado con éxito']);
    }


    public function update(Request $request, Envase $envase)
    {
        $rules = array(
            'nombre' => 'required',
            'cantidad' => 'required|numeric|gt:0',
            'peso' => 'required|numeric|gt:0',
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        if ($request->has('nombre')) {
            $envase->nombre = $request->nombre;
        }
        if ($request->has('cantidad')) {
            $envase->cantidad = $request->cantidad;
        }
        if ($request->has('peso')) {
            $envase->peso = $request->peso;
        }

        if (!$envase->isDirty()) {
            return response()->json(['error' => ['Se debe especificar al menos un valor diferente para actualizar']]);
        }

        $envase->save();

        return response()->json(['success' => 'Envase modificado con éxito']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Envase  $envase
     * @return \Illuminate\Http\Response
     */
    public function delete(Envase $envase)
    {
        try {
            $envase->update(['borrado' => 1]);
            return response()->json(['success' => 'Envase dado de baja']);
            //
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => [$e->getMessage()]]);
        }
    }
}
