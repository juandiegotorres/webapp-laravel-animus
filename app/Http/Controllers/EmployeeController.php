<?php

namespace App\Http\Controllers;

use App\Charge;
use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Database\Eloquent\Model;

class EmployeeController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:empleados']);
    }

    public function index()
    {
        if (!auth()->user()->can('employees.index')) {
            abort(403, 'Acción no autorizada.');
        }
        $employees = Employee::where('borrado', 0)->get();

        return view('employees.index')->with('employees', $employees);
    }


    public function create()
    {
        $cargos = Charge::where('borrado', 0)->get();

        return view('employees.create')->with('cargos', $cargos);
    }

    public function store(EmployeeRequest $request)
    {
        $request['estado'] = 1;
        $request['borrado'] = 0;

        $request['nombreCompleto'] = Employee::nombreCompleto($request['nombre'], $request['apellido']);

        Employee::create($request->except(['nombre', 'apellido']));

        $request->session()->flash('status', 'Empleado creado con éxito');
        return redirect()->action('EmployeeController@index');
    }


    public function show(Employee $employee)
    {
        $cargos = Charge::where('borrado', 0)->get();

        return view('employees.show')
            ->with('employee', $employee)
            ->with('cargos', $cargos);
    }


    public function edit(Employee $employee)
    {
        $cargos = Charge::where('borrado', 0)->get();

        $apellidoNombre = explode(", ", $employee->nombreCompleto);
        if (count($apellidoNombre) > 1) {
            $employee->apellido = $apellidoNombre[0];
            $employee->nombre = $apellidoNombre[1];
        } else {
            $apellidoNombre = explode(" ", $employee->nombreCompleto);
            $employee->nombre = $apellidoNombre[0];
            $employee->apellido = $apellidoNombre[1];
        }

        return view('employees.edit')
            ->with('employee', $employee)
            ->with('cargos', $cargos);
    }


    public function update(EmployeeRequest $request, Employee $employee)
    {
        //Explicada la funcion de este if en el método store de este controlador
        // if (!empty($request['cambioCargos'])) {
        //     $cargos = explode(',', $request['cambioCargos']);

        //     $this->agregarModificarCargos($cargos);

        //     if (!is_numeric($request['charge_id'])) {
        //         $id = DB::table('charges')->where('nombre', $request['charge_id'])->value('id');
        //         $request['charge_id'] = $id;
        //     }
        // }

        $request['nombreCompleto'] = Employee::nombreCompleto($request['nombre'], $request['apellido']);

        $employee->update($request->except(['nombre', 'apellido']));

        $request->session()->flash('status', 'Empleado modificado con éxito');

        return redirect()->action('EmployeeController@index');
    }

    public function delete(Employee $employee)
    {
        DB::table('employees')
            ->where('id', $employee->id)
            ->update(['borrado' => 1]);
    }


    function obtenerCargos()
    {
        $cargos = Charge::where('borrado', 0)->get();
        return response()->json($cargos);
    }
}
