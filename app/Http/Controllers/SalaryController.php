<?php

namespace App\Http\Controllers;

use App\Salary;
use App\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\EmployeeSalaryBalance;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\SalaryRequest;
use Illuminate\Support\Facades\Validator;

class SalaryController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:sueldos']);
    }

    public function index()
    {
        if (!auth()->user()->can('salaries.index')) {
            abort(403, 'Acción no autorizada.');
        }
        $empleados = Employee::all();
        return view('salaries.index')
            ->with('empleados', $empleados);
    }


    public function indexDT()
    {
        if (!auth()->user()->can('salaries.index')) {
            abort(403, 'Acción no autorizada.');
        }
        $sueldos = DB::table('salaries')->join('employees', 'employees.id', '=', 'salaries.employee_id')
            ->where('salaries.borrado', 0)->select('salaries.*', 'nombreCompleto')->orderBy('created_at', 'DESC')
            ->get();

        return DataTables::of($sueldos)
            ->addColumn('empleado', '  <a target="_blank" href="{{ route(\'employees.show\', [\'employee\' => $employee_id]) }}"> {{$nombreCompleto}}</a>')
            ->addColumn('precioHora', '{{ $precioHora }}')
            ->addColumn('horas', '{{ $horas }}')
            ->addColumn('total', '{{ $total }}')
            ->addColumn('observaciones', '<a class="btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="{{ $observaciones }}">Observaciones <i class="fa fa-eye"></i></a>')
            ->addColumn('fechaInicio', '{{ date("d-m-Y", strtotime($fechaInicio)) }}')
            ->addColumn('fechaFin', '{{ date("d-m-Y", strtotime($fechaFin)) }}')
            ->addColumn('created_at', '{{ $created_at }}')
            ->addColumn(
                'eliminar',
                function ($sueldos) {
                    $html = '';
                    $html .= '<a href="' . route('sueldo.pdf', ['sueldo' => $sueldos->id]) . '" target="_blank" class="btn btn-sm btn-warning mr-2"><i class="fa fa-print" aria-hidden="true"></i></a>';
                    if (auth()->user()->can('salaries.edit')) {
                        $html .= '<a href="' . route('salaries.edit', ['salary' => $sueldos->id]) . '" class="btn btn-primary btn-sm mr-2"><i class="fa fa-pen"></i></a>';
                    }
                    if (auth()->user()->can('salaries.delete')) {
                        $html .= '<a data-id="' . $sueldos->id . '" class="btn btn-danger btn-sm btnEliminar"><i class="fa fa-trash"></i></a>';
                    }
                    return $html;
                }


            )
            ->rawColumns(['empleado', 'eliminar', 'observaciones'])
            ->toJson();
    }


    public function create()
    {
        if (!auth()->user()->can('salaries.create')) {
            abort(403, 'Acción no autorizada.');
        }
        $empleados = Employee::select('nombreCompleto', 'id')->get();
        return view('salaries.create')
            ->with('empleados', $empleados);
    }

    public function store(SalaryRequest $request)
    {

        if (!auth()->user()->can('salaries.create')) {
            abort(403, 'Acción no autorizada.');
        }
        return DB::transaction(function () use ($request) {
            try {

                if (empty($request['observaciones'])) {
                    $request['observaciones'] = "Sin observaciones";
                }

                $request['estado'] = 1;
                $request['borrado'] = 0;

                Salary::create($request->all());

                $id = Salary::select('id')->max('id');

                EmployeeSalaryBalance::create([
                    'employee_id' => $request['employee_id'],
                    'tipoMovimiento' => EmployeeSalaryBalance::MOVIMIENTO_DEBE,
                    'movimiento_id' => $id,
                    'debe' => $request['total'],
                    'fecha' => Carbon::now(),
                    'estado' => 1,
                    'borrado' => 0,
                ]);

                $request->session()->flash('status', 'Sueldo agregado con éxito');
                return redirect()->action('SalaryController@index');
                //
            } catch (\Throwable $th) {
                DB::rollback();
                return response()->json('Error al agregar sueldo y movimiento cuenta. Error: ' . $th);
            }
        });
    }

    public function edit(Salary $salary)
    {
        if (!auth()->user()->can('salaries.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        $empleados = Employee::select('nombreCompleto', 'id')->get();

        return view('salaries.edit')
            ->with('salary', $salary)
            ->with('empleados', $empleados);
    }

    public function update(SalaryRequest $request, Salary $salary)
    {
        if (!auth()->user()->can('salaries.edit')) {
            abort(403, 'Acción no autorizada.');
        }
        return DB::transaction(function () use ($request, $salary) {
            try {
                if (empty($request['observaciones'])) {
                    $request['observaciones'] = "Sin observaciones";
                }

                //Pregunto si el total que viene en el request es diferente del que ya esta en la base de datos,
                //si es asi significa que se cambio la variable 'total' y no algun otro campo. Lo que significa que
                //tengo que actualizar la tabla de movimientos tambien
                if ($salary->total != $request['total']) {
                    //Selecciono la tabla de movimientos, si el tipo de movimiento es 'D' significa 'DEBE'(Sueldo), si es
                    //'H es 'HABER'(Pago de sueldo). En este caso estamos modificando el sueldo por lo que debo seleccionar el movmiento
                    //D y filtrar por el id del sueldo tambien, de esa forma puedo afectar la fila correctamente. Luego actualizo el monto
                    DB::table('employee_salary_balances')
                        ->where('tipoMovimiento', '=', 'D')
                        ->where('movimiento_id', '=', $salary->id)
                        ->update(['debe' => $request['total']]);
                }

                $salary->update($request->all());

                $request->session()->flash('status', 'Sueldo modificado con éxito');

                return redirect()->action('SalaryController@index');

                //
            } catch (\Throwable $th) {
                DB::rollback();
                return response()->json('Error al modificar sueldo y movimiento cuenta. Error: ' . $th);
            }
        });
    }

    public function destroy(Salary $salary)
    {
        //
    }

    public function delete($salary)
    {
        if (!auth()->user()->can('salaries.delete')) {
            abort(403, 'Acción no autorizada.');
        }
        return DB::transaction(function () use ($salary) {
            try {
                Salary::whereId($salary)->update(['borrado' => 1]);
                //Cuando doy de baja el pago de sueldo tambien debo dar de baja el movimiento de la tabla 'movimientos'
                //valga la redundancia.
                DB::table('employee_salary_balances')
                    ->where('tipoMovimiento', '=', 'D')
                    ->where('movimiento_id', '=', $salary)
                    ->update(['borrado' => 1]);
            } catch (\Throwable $th) {
                DB::rollback();
                return response()->json('Error al dar de baja sueldo y movimiento cuenta. Error: ' . $th);
            }
        });
    }

    public function filtrar(Request $request)
    {
        try {
            $fechaInicio = $request['fechaInicio'];
            $fechaFin = $request['fechaFin'];
            $idempleado = $request['empleado'];

            $rules = array(
                'fechaInicio' => 'nullable|date|before:fechaFin|after:2000-01-01',
                'fechaFin' => 'required|date|before:2099-12-31|after:fechaInicio',
                'empleado' => 'nullable|required_without:fechaInicio',
            );

            $error = Validator::make($request->all(), $rules, ['empleado.required_without' => 'El campo empleado es obligatorio cuando fecha inicio no está presente o viceversa.']);

            if ($error->fails()) {
                return response()->json(['error' => $error->errors()->all()]);
            }

            $query = DB::table('salaries')->join('employees', 'employees.id', '=', 'salaries.employee_id')
                ->where('salaries.borrado', 0)->select('salaries.*', 'nombreCompleto')->orderBy('created_at', 'DESC');

            if ($fechaInicio == null) {
                //Solo filtrar por empleado
                $sueldos = $query->where('employee_id', $idempleado)->get();

                //
            } elseif ($idempleado == '') {
                //Solo debo filtrar entre fechas
                $sueldos = $query->whereBetween('fechaFin', [$fechaInicio, $fechaFin])->get();
            } else {
                //Debo filtrar entre ambos
                $sueldos = $query->whereBetween('fechaFin', [$fechaInicio, $fechaFin])->where('employee_id', $idempleado)->get();
            }

            return DataTables::of($sueldos)
                ->addColumn('empleado', '  <a target="_blank" href="{{ route(\'employees.show\', [\'employee\' => $employee_id]) }}"> {{$nombreCompleto}}</a>')
                ->addColumn('observaciones', '<a class="btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="{{ $observaciones }}">Observaciones <i class="fa fa-eye"></i></a>')
                ->addColumn('fechaInicio', '{{ date("d-m-Y", strtotime($fechaInicio)) }}')
                ->addColumn('fechaFin', '{{ date("d-m-Y", strtotime($fechaFin)) }}')
                ->addColumn(
                    'eliminar',
                    function ($sueldos) {
                        $html = '';
                        if (auth()->user()->can('salaries.edit')) {
                            $html .= '<a href="' . route('salaries.edit', ['salary' => $sueldos->id]) . '" class="btn btn-primary btn-sm mr-2"><i class="fa fa-pen"></i></a>';
                        }
                        if (auth()->user()->can('salaries.delete')) {
                            $html .= '<a data-id="' . $sueldos->id . '" class="btn btn-danger btn-sm btnEliminar"><i class="fa fa-trash"></i></a>';
                        }
                        return $html;
                    }


                )
                ->rawColumns(['empleado', 'eliminar', 'observaciones'])
                ->toJson();
            //
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => [$e->getMessage()]]);
        }
    }
}
