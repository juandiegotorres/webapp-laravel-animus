<?php

namespace App\Http\Controllers;

use App\Pago;
use App\User;
use App\Venta;
use App\Salary;
use App\SalaryPayment;

use App\RawMaterialEntry;
use Illuminate\Http\Request;
use mikehaertl\wkhtmlto\Pdf;
use App\DetailRawMaterialEntry;
use App\IndumentaryDistribution;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\DetailIndumentaryDistribution;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Permission;

class ServicesController extends Controller
{
    public function store()
    {

        $html = view('');
        $pdf = new Pdf('./resources/views/welcome.blade.php');
        return $pdf;
        // $ids = [1, 1, 3];
        // $valoresUnicos = array_unique($ids);
        // print_r($valoresUnicos);
        // $preciosProductos = DB::table('variantes_productos')->leftJoin('detail_price_lists', 'variantes_productos.id', '=', 'detail_price_lists.variante_id')
        //     ->join('products', 'products.id', 'variantes_productos.product_id')
        //     ->where('detail_price_lists.borrado', 0)
        //     ->where('price_list_id', $id)
        //     ->orWhereNull('price_list_id')
        //     ->select(
        //         'variantes_productos.id as idVariante',
        //         'variantes_productos.product_id',
        //         'variantes_productos.tipo',
        //         'variantes_productos.cantidad',
        //         'products.nombreCompleto',
        //         'detail_price_lists.*'
        //     )
        //     ->get();
        // return $preciosProductos;

        // User::findOrFail(3)->syncRoles('administrador');
        // $usuario = User::find(3);
        // $roles = $usuario->getRoleNames();
        // print_r($roles);
        // //Venta 
        // $venta = Venta::find(1);
        // $cliente = $venta->cliente;
        // $detalleVenta = $venta->detalleVenta;
        // // return view('pdf.recibo-venta', compact('venta', 'cliente', 'detalleVenta'));
        // $pdf = PDF::loadView('pdf.recibo-venta', compact('venta', 'cliente', 'detalleVenta'));
        // return $pdf->stream('pdf.recibo-venta');

        // //Pago sueldo
        // $pagoSueldo = SalaryPayment::find(27);
        // $empleado = $pagoSueldo->empleado;
        // $sueldo = Salary::where('employee_id', $empleado->id)->orderBy('fechaInicio', 'desc')->get();
        // $pdf = PDF::loadView('pdf.pago-sueldo', compact('pagoSueldo', 'empleado', 'sueldo'));
        // return $pdf->stream('pdf.pago-sueldo');

        // //Recibo pago productor
        // $pago = Pago::find(2);
        // $proveedor = $pago->proveedor;
        // $pdf = PDF::loadView('pdf.recibo-pago', compact('pago', 'proveedor'));
        // return $pdf->stream('pdf.recibo-pago');

        // //Ingreso Fruta
        // $ingreso = RawMaterialEntry::findOrFail(1);
        // $proveedor = $ingreso->proveedor;
        // $detalleIngreso = DetailRawMaterialEntry::where(['raw_material_entries_id' => 1])->get();
        // $pdf = PDF::loadView('pdf.ingreso-fruta', compact('ingreso', 'proveedor', 'detalleIngreso'));
        // return $pdf->stream('pdf.ingreso-fruta');

        // //Entrega indumentaria
        // $entrega = IndumentaryDistribution::findOrFail(1);
        // $empleado = $entrega->empleado;
        // $detalleEntrega = DetailIndumentaryDistribution::where('indumentary_distributions_id', 1)
        //     ->join('indumentaries', 'indumentaries.id', '=', 'detail_indumentary_distributions.indumentary_id')
        //     ->select('detail_indumentary_distributions.*', 'indumentaries.nombre', 'indumentaries.tipo_modelo', 'indumentaries.modelo')
        //     ->get();
        // // $pdf->loadView('pdf.entrega-indumentaria', $entregas);
        // $pdf = PDF::loadView('pdf.entrega-indumentaria', compact('detalleEntrega', 'empleado', 'entrega'));
        // return $pdf->stream('pdf.entrega-indumentaria');



        // $user = User::find(6)->getRoleNames()->toJson();
        // // $permissions = $user->getRoleNames();
        // // str_replace(array('[', ']'), '', $permissions);
        // return $user;
    }
}
