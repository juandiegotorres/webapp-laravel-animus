<?php

namespace App\Http\Controllers;

use App\Envase;
use App\Product;
use App\Supplie;
use App\Provider;
use Illuminate\Http\Request;
use App\StockProductosInsumos;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class StockProductosInsumosController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:stock-general'], ['except' => ['index']]);
    }



    public function index()
    {
        $productos = DB::table('products')->where('products.borrado', 0)
            ->where('variantes_productos.borrado', 0)
            ->join('variantes_productos', 'variantes_productos.product_id', 'products.id')
            ->select('products.*', 'variantes_productos.*')
            ->get();

        $insumos = Supplie::select('id', 'nombre', 'cantidad', 'created_at')->get();



        return view('stock.index')
            ->with('productos', $productos)
            ->with('insumos', $insumos);
    }


    public function datatableProductos()
    {
        $productos = StockProductosInsumos::where('tipo_variante_insumo', StockProductosInsumos::ES_VARIANTE)
            ->join('variantes_productos', 'variantes_productos.id', 'id_variante_insumo')
            ->join('products', 'products.id', 'variantes_productos.product_id')
            ->select('stock_productos_insumos.*', 'products.nombreCompleto', 'variantes_productos.tipo', 'variantes_productos.kg')
            ->get();

        return DataTables::of($productos)
            ->addColumn('nombre', function ($productos) {
                $nombreCompleto = $productos->nombreCompleto;
                switch ($productos->tipo) {
                    case '0':
                        return $nombreCompleto .= ' - Caja, ' . $productos->kg . ' kg(s)';
                        break;
                    case '1':
                        return $nombreCompleto .= ' - Bolsa, ' . $productos->kg . ' kg(s)';
                        break;
                    default:
                        break;
                }
            })
            ->addColumn('tipoMovimiento', function ($productos) {
                if ($productos->tipo_movimiento == StockProductosInsumos::MOVIMIENTO_RESTOCK_PRODUCTO) {
                    return 'Reestock del producto';
                } else if ($productos->tipo_movimiento == StockProductosInsumos::MOVIMIENTO_VENTA_PRODUCTO) {
                    return 'Venta';
                }
            })
            ->editColumn('fecha', '{{ date("d-m-Y", strtotime($fecha)) }}')
            ->addColumn('opciones', '<a data-id="{{$id}}" class="btn btn-danger btn-sm btnEliminarMovimientoProductoInsumo"><i class="fa fa-trash" aria-hidden="true"></i></a>')
            ->rawColumns(['opciones'])
            ->make(true);
    }

    public function datatableInsumos()
    {

        $insumos = StockProductosInsumos::where('tipo_variante_insumo', StockProductosInsumos::ES_INSUMO)
            ->join('supplies', 'supplies.id', 'id_variante_insumo')
            ->select('stock_productos_insumos.*', 'supplies.nombre')
            ->get();

        return DataTables::of($insumos)
            ->addColumn('tipoMovimiento', function ($productos) {
                if ($productos->tipo_movimiento == StockProductosInsumos::MOVIMENTO_RESTA_INSUMO) {
                    return 'Resta de insumo por el usuario';
                } else if ($productos->tipo_movimiento == StockProductosInsumos::MOVIMIENTO_COMPRA_INSUMO) {
                    return 'Compra de insumo';
                }
            })
            ->addColumn(
                'opciones',
                '<a data-id="{{$id}}" class="btn btn-danger btn-sm btnEliminarMovimientoProductoInsumo"><i class="fa fa-trash" aria-hidden="true"></i></a>'
            )
            ->editColumn('fecha', '{{ date("d-m-Y", strtotime($fecha)) }}')
            ->rawColumns(['opciones'])
            ->toJson();
    }

    public function storeMovimientoProducto(Request $request)
    {
        // return response()->json($request->all());
        $rules = array(
            'producto' => 'required',
            'fecha' => 'required|before:tomorrow',
            'cantidad' => 'required|integer|gt:0',
            'descripcion' => 'required',
        );

        $error = Validator::make($request->all(), $rules, ['fecha.tomorrow' => 'La fecha debe ser menor o igual a hoy']);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        return DB::transaction(function () use ($request) {

            try {
                StockProductosInsumos::create([
                    'id_variante_insumo' => $request['producto'],
                    'tipo_variante_insumo' => StockProductosInsumos::ES_VARIANTE,
                    'tipo_movimiento' => StockProductosInsumos::MOVIMIENTO_RESTOCK_PRODUCTO,
                    'fecha' => $request['fecha'],
                    'descripcion' => $request['descripcion'],
                    'cantidad' => $request['cantidad']
                ]);

                DB::table('variantes_productos')->where('id', $request['producto'])->increment('cantidad', $request['cantidad']);

                return response()->json(['success' => 'Movimiento de stock registrado']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()], 404);
            }
        });
    }

    public function storeMovimientoInsumo(Request $request)
    {
        // return response()->json($request->all());
        $rules = array(
            'insumo' => 'required',
            'fecha' => 'required|before:tomorrow',
            'cantidad' => 'required|integer|gt:0',
            'descripcion' => 'required',
        );

        $error = Validator::make($request->all(), $rules, ['fecha.tomorrow' => 'La fecha debe ser menor o igual a hoy']);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }

        return DB::transaction(function () use ($request) {

            try {
                StockProductosInsumos::create([
                    'id_variante_insumo' => $request['insumo'],
                    'tipo_variante_insumo' => StockProductosInsumos::ES_INSUMO,
                    'tipo_movimiento' => StockProductosInsumos::MOVIMENTO_RESTA_INSUMO,
                    'fecha' => $request['fecha'],
                    'descripcion' => $request['descripcion'],
                    'cantidad' => $request['cantidad']
                ]);

                DB::table('supplies')
                    ->where('id', $request['insumo'])
                    ->update(['cantidad' => DB::raw('GREATEST(cantidad - ' . $request['cantidad'] . ', 0)')]);

                return response()->json(['success' => 'Movimiento de stock registrado']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => $e->getMessage()], 404);
            }
        });
    }

    public function cancelarMovimiento(StockProductosInsumos $movimiento)
    {
        try {
            $movimiento->update(['borrado' => 1]);
            if ($movimiento->tipo_variante_insumo == StockProductosInsumos::ES_VARIANTE) { //Si es una variante
                //Resto el stock que habia sumado
                DB::table('variantes_productos')
                    ->where('id', $movimiento->id_variante_insumo)
                    ->update(['cantidad' => DB::raw('GREATEST(cantidad - ' . $movimiento->cantidad . ', 0)')]);
            } else if ($movimiento->tipo_variante_insumo == StockProductosInsumos::ES_INSUMO) { //Si es insumo
                //Vuelvo a incremetar el stock que habia restado
                DB::table('supplies')->where('id', $movimiento->id_variante_insumo)->increment('cantidad', $movimiento->cantidad);
            }
            return response()->json([['success' => 'Movimiento cancelado con éxito'], ['es_variante' => $movimiento->tipo_variante_insumo]]);
            //
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => $e->getMessage()]);
        }
    }
}
