<?php

namespace App\Http\Controllers;

use App\Service;
use App\Supplie;
use App\Provider;
use App\SupplieService;
use App\BalanceCompraPagos;
use Illuminate\Http\Request;
use App\StockProductosInsumos;
use Illuminate\Support\Carbon;
use App\PurchaseSupplieService;
use Illuminate\Support\Facades\DB;
use App\DetailPurchaseSupplieService;
use Illuminate\Support\Facades\Validator;

class PurchaseSupplieServiceController extends Controller
{
    public function index($id_proveedor)
    {
        if (!auth()->user()->can('purchase-supplie-service.index')) {
            abort(403, 'Acción no autorizada.');
        }
        $insumos = Supplie::get();
        $servicios = Service::get();
        $proveedor = Provider::whereId($id_proveedor);

        return view('purchase-supplie-service.index')
            ->with('proveedor', $proveedor)
            ->with('insumos', $insumos)
            ->with('servicios', $servicios);
    }

    public function storeInsumo(Request $request)
    {
        if (!auth()->user()->can('purchase-supplie-service.index')) {
            abort(403, 'Acción no autorizada.');
        }
        if (empty($request['insumo-servicio'])) {
            $request->session()->flash('status', ['message' => 'Debes comprar algo', 'type' => 'error']);
            return redirect()->back();
        }
        //Cuento la cantidad de registros que vienen dentro del array insumo-servicio y lo guardo en una variable
        //en este caso use insumo-servicio, pero puede ser cualquier array que venga del request(cantidad, precioUnitario o totalParcial)
        $registros = count($request['insumo-servicio']);

        //Comienzo la transaccion porque voy a efectuar insersiones en dos tablas distintas
        DB::transaction(function () use ($request, $registros) {
            try {

                //Primeramente inserto el la compra general
                $compra = PurchaseSupplieService::create([
                    'provider_id' => $request['idProveedor'],
                    'tipoCompra' => 'I',
                    'iva' => $request['iva'],
                    'total' => $request['total'],
                    'detalle' => $request['detalle'],
                    'fecha' => $request['fecha'],
                    'estado' => 1,
                    'borrado' => 0,
                ]);

                BalanceCompraPagos::create([
                    'provider_id' => $request['idProveedor'],
                    'tipoCompra' => BalanceCompraPagos::COMPRA_INSUMO,
                    'movimiento_id' => $compra->id,
                    'debe' => ($request['total'] + $request['iva']),
                    'fecha' => now(),
                ]);

                //Y ahora empiezo a recorrer el request para obtener las distintas compras que se hicieron al proveedor
                //Y las inserto en el detalle de la compra
                for ($i = 0; $i < $registros; $i++) {
                    $insumo = $request['insumo-servicio'][$i];
                    $cantidad = $request['cantidad'][$i];
                    $precioUnitario = $request['precioUnitario'][$i];
                    $totalParcial = $request['totalParcial'][$i];

                    DetailPurchaseSupplieService::create([
                        'purchase_supplie_services_id' => $compra->id,
                        'supplie_id' => $insumo,
                        'cantidad' => $cantidad,
                        'precioUnitario' => $precioUnitario,
                        'totalParcial' => $totalParcial,
                        'estado' => 1,
                        'borrado' => 0,
                    ]);


                    Supplie::whereId($insumo)->increment('cantidad', $cantidad);

                    $request->session()->flash('status', ['message' => 'Compra realizada con éxito', 'type' => 'success']);
                }
            } catch (\Exception $e) {
                //En caso de haber algun erro se hace rollback
                DB::rollback();
                $request->session()->flash('status', ['message' => 'Sucedio un error' . $e->getMessage(), 'type' => 'error']);
            }
        });
        return redirect()->route('purchase-history.index');
    }

    public function storeServicio(Request $request)
    {
        if (!auth()->user()->can('purchase-supplie-service.index')) {
            abort(403, 'Acción no autorizada.');
        }
        if (empty($request['insumo-servicio'])) {
            $request->session()->flash('status', ['message' => 'Debes comprar algo', 'type' => 'error']);
            return redirect()->back();
        }
        //Cuento la cantidad de registros que vienen dentro del array insumo-servicio y lo guardo en una variable
        //en este caso use insumo-servicio, pero puede ser cualquier array que venga del request(cantidad, precioUnitario o totalParcial)
        $registros = count($request['insumo-servicio']);

        //Comienzo la transaccion porque voy a efectuar insersiones en dos tablas distintas
        DB::transaction(function () use ($request, $registros) {
            try {
                //Primeramente inserto el la compra general
                $compra = PurchaseSupplieService::create([
                    'provider_id' => $request['idProveedor'],
                    'tipoCompra' => 'S',
                    'iva' => $request['iva'],
                    'total' => $request['total'],
                    'detalle' => $request['detalle'],
                    'fecha' => $request['fecha'],
                    'estado' => 1,
                    'borrado' => 0,
                ]);

                BalanceCompraPagos::create([
                    'provider_id' => $request['idProveedor'],
                    'tipoCompra' => BalanceCompraPagos::COMPRA_SERVICIO,
                    'movimiento_id' => $compra->id,
                    'debe' => ($request['total'] + $request['iva']),
                    'fecha' => now(),
                ]);

                //Y ahora empiezo a recorrer el request para obtener las distintas compras que se hicieron al proveedor
                //Y las inserto en el detalle de la compra
                for ($i = 0; $i < $registros; $i++) {
                    $servicio = $request['insumo-servicio'][$i];
                    $totalParcial = $request['totalParcial'][$i];

                    DetailPurchaseSupplieService::create([
                        'purchase_supplie_services_id' => $compra->id,
                        'service_id' => $servicio,
                        'cantidad' => 1,
                        'precioUnitario' => $totalParcial,
                        'totalParcial' => $totalParcial,
                        'estado' => 1,
                        'borrado' => 0,
                    ]);


                    $request->session()->flash('status', ['message' => 'Compra realizada con éxito', 'type' => 'success']);
                }
            } catch (\Exception $e) {
                //En caso de haber algun erro se hace rollback
                DB::rollback();
                $request->session()->flash('status', ['message' => 'Sucedio un error' . $e->getMessage(), 'type' => 'error']);
            }
        });
        return redirect()->route('purchase-history.index');
    }

    //Guarda la compra de insumo desde una orden de compra
    public function storeFromOrden(Request $request)
    {
        // return response()->json($request->all());
        //===================== VALIDACION ==========================
        $arrayPrecios = $request['precio'];
        $arrayCantidades = $request['cantidad'];

        $hayError = 0;

        for ($i = 0; $i < count($arrayPrecios); $i++) {
            if (($arrayPrecios[$i] != null && $arrayCantidades[$i] == null) || ($arrayPrecios[$i] == null && $arrayCantidades[$i] != null)) {
                $hayError += 1;
            }
        }

        if ($hayError >= 1) {
            //Error para mostrar en el sweet alert
            return response()->json(['error' => ['Si se establece una cantidad se debe establecer un precio, o viceversa.']]);
        }

        $mensajesError = [
            'cantidad.*.required' => 'Los campos cantidad no pueden estar vacios',
            'precio.*.required' => 'Los campos precio no pueden estar vacios',
            'cantidad.*.numeric' => 'El campo cantidad debe ser numerico',
            'precio.*.numeric' => 'El campo precio debe ser numerico',
            'cantidad.*.not_in' => 'La cantidad debe ser mayor que 0',
            'precio.*.not_in' => 'El precio debe ser mayor que 0'
        ];
        //Valido los campos
        $error = Validator::make($request->all(), [
            'cantidad.*' => 'numeric|required|min:0|not_in:0',
            'precio.*' => 'numeric|required|min:0|not_in:0'
        ], $mensajesError);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }
        //==================== FIN VALIDACION ========================
        return DB::transaction(function () use ($request) {
            try {
                $compra = PurchaseSupplieService::create([
                    'provider_id' => $request['id_proveedor'],
                    'tipoCompra' => 'I',
                    'iva' => $request['iva'],
                    'total' => $request['total'],
                    'detalle' => $request['detalle'],
                    'fecha' => $request['fecha'],
                    'estado' => 1,
                    'borrado' => 0,
                ]);

                BalanceCompraPagos::create([
                    'provider_id' => $request['id_proveedor'],
                    'tipoCompra' => BalanceCompraPagos::COMPRA_INSUMO,
                    'movimiento_id' => $compra->id,
                    'debe' => $request['total'],
                    'fecha' => now(),
                ]);

                for ($i = 0; $i < count($request['precio']); $i++) {
                    DetailPurchaseSupplieService::create([
                        'purchase_supplie_services_id' => $compra->id,
                        'supplie_id' => $request['id_objeto'][$i],
                        'cantidad' => $request['cantidad'][$i],
                        'precioUnitario' => $request['precio'][$i],
                        'totalParcial' => ($request['cantidad'][$i] * $request['precio'][$i]),
                        'estado' => 1,
                        'borrado' => 0,
                    ]);

                    Supplie::whereId($request['id_objeto'][$i])->increment('cantidad', $request['cantidad'][$i]);
                }
                //Cambio el estado de la orden de compra para que no se pueda borrar
                DB::table('orden_compras')->where('id', $request['id_orden'])
                    ->update(['estado' => 0]);

                return response()->json(['success' => 'Compra registrada con éxito']);
                //
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => [$e->getMessage()]]);
            }
        });
    }

    public function show(PurchaseSupplieService $purchaseSupplieService)
    {
        if (!auth()->user()->can('purchase-supplie-service.show')) {
            abort(403, 'Acción no autorizada.');
        }
        $detalleCompra = DetailPurchaseSupplieService::where('purchase_supplie_services_id', $purchaseSupplieService->id)->get();
        $maestroCompra = $purchaseSupplieService;
        return view('purchase-supplie-service.show')->with('detalleCompra', $detalleCompra)->with('maestroCompra', $maestroCompra);
    }

    public function proveedoresInsumosServicios()
    {
        $proveedores = DB::table('providers')->where('borrado', 0)->where('tipoProveedor', 'IS')->get();
        return response()->json($proveedores);
    }

    public function obtenerInsumos()
    {
        $insumos = Supplie::get();
        return response()->json($insumos);
    }

    public function obtenerServicios()
    {
        $servicios = Service::get();
        return response()->json($servicios);
    }
}
