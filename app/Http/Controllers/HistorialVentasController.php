<?php

namespace App\Http\Controllers;

use App\Venta;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class HistorialVentasController extends Controller
{
    public function index()
    {
        $clientes = Client::all();
        return view('historial-ventas.index')->with('clientes', $clientes);
    }

    public function datatable()
    {
        $ventas = Venta::join('clients', 'clients.id', 'ventas.client_id')->select('ventas.*', 'clients.nombreCompleto')->get();

        return DataTables::of($ventas)
            ->editColumn('fecha', '{{ date("d-m-Y", strtotime($fecha)) }}')
            ->addColumn('cliente', '<a href="{{ route(\'clients.show\', [\'client\' => $client_id]) }}" target="_blank">{{$nombreCompleto}}</a>')
            ->addColumn('opciones', '<a href="{{ route(\'historial-ventas.show\', [\'venta\' => $id]) }}" class="btn btn-success btn-sm">Ver detalles</a>
                                    <a href="{{ route(\'venta.pdf\', [\'venta\' => $id]) }}" target="_blank" class="btn btn-warning btn-sm"><i class="fa fa-print"></i></a>
                                     <a data-id="{{$id}}" class="btnEliminarVenta btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>')
            ->editColumn('iva', ' $ {{ number_format($iva, 2, ",", ".") }}')
            ->editColumn('total', ' $ {{ number_format($total, 2, ",", ".") }}')
            ->rawColumns(['opciones', 'cliente'])
            ->make(true);
    }

    public function show(Venta $venta)
    {
        $detalles = DB::table('detalle_ventas')->where('id_venta', $venta->id)
            ->join('variantes_productos', 'variantes_productos.id', 'detalle_ventas.id_variante')
            ->join('products', 'products.id', 'variantes_productos.product_id')
            ->select('detalle_ventas.*', 'variantes_productos.tipo', 'variantes_productos.kg', 'products.nombreCompleto')
            ->get();

        return view('historial-ventas.show')
            ->with('detalles', $detalles)
            ->with('venta', $venta);
    }

    public function filtrar(Request $request)
    {
        // return response()->json($request->all());
        $fechaInicio = $request['fechaInicio'];
        $fechaFin = $request['fechaFin'];
        $idcliente = $request['cliente'];
        // $error = array();

        $rules = array(
            'fechaInicio' => 'nullable|date|before:fechaFin|after:2000-01-01',
            'fechaFin' => 'required|date|before:2099-12-31|after:fechaInicio',
            'cliente' => 'nullable|required_without:fechaInicio',
        );

        $error = Validator::make($request->all(), $rules, ['cliente.required_without' => 'El campo cliente es obligatorio cuando fecha inicio no está presente o viceversa.']);

        if ($error->fails()) {
            return response()->json(['error' => $error->errors()->all()]);
        }


        $data = '';
        if ($fechaInicio == null) {
            //Solo filtrar por cliente
            $data = Venta::where('client_id', $idcliente)->join('clients', 'clients.id', '=', 'ventas.client_id')->get();
            //  
        } elseif ($idcliente == '') {
            //Solo debo filtrar entre fechas
            $data = Venta::whereBetween('fecha', [$fechaInicio, $fechaFin])->join('clients', 'clients.id', '=', 'ventas.client_id')->get();
        } else {
            //Debo filtrar entre ambos
            $data = Venta::where('client_id', $idcliente)->whereBetween('fecha', [$fechaInicio, $fechaFin])->join('clients', 'clients.id', '=', 'ventas.client_id')->get();
        }

        return DataTables::of($data)
            ->editColumn('fecha', '{{date("d-m-Y", strtotime($fecha))}}')
            ->addColumn('cliente', '<a target="_blank" href="{{ route(\'clients.show\', [\'client\' => $client_id]) }}"> {{$nombreCompleto}}</a>')
            ->editColumn('iva', ' $ {{ number_format($iva, 2, ",", ".") }}')
            ->addColumn('total', ' $ {{ number_format($total, 2, ",", ".") }}')
            ->addColumn('opciones', '<a href="{{ route(\'historial-ventas.show\', [\'venta\' => $id]) }}" class="btn btn-success btn-sm">Ver detalles</a> <a data-id="{{$id}}" class="btnEliminarVenta btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>')
            ->rawColumns(['opciones', 'cliente'])
            ->make(true);
    }
}
