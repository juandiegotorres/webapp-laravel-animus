<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalaryPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => 'required',
            'fecha' => 'required|date|date_format:Y-m-d|before:tomorrow|after:1900-01-01',
            'total' => 'required|numeric|digits_between:1,8',
            'concepto' => 'required|max:500',
        ];
    }

    public function attributes()
    {
        return [
            'employee_id' => 'empleado',
        ];
    }

    public function messages()
    {
        return [
            'fecha.date_format' => 'Introduzca una fecha válida.',
            'fecha.before' => 'El campo fecha debe ser una fecha anterior o igual a hoy.',
        ];
    }
}
