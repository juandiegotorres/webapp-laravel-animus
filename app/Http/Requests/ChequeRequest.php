<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class ChequeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->tipoCheque == 'emitido') {
            return [
                'nroCheque' => 'required|numeric',
                'fechaEmision' => 'required|date|date_format:Y-m-d|before:2099-12-31|after:2000-01-01',
                'fechaCobro' => 'required|date|date_format:Y-m-d|before:2099-12-31|after:fechaEmision',
                'fechaRecepcion' => 'nullable|date_format:Y-m-d|date|before:2099-12-31|after:fechaEmision',
                'titular' => 'nullable',
                'entregadoPor' => 'nullable',
                'entregadoA' => 'nullable',
                'importe' => 'required|numeric',
                'bank_id' => 'required|numeric',
                'tipoCheque' => 'nullable',
            ];
        } else {
            return [
                'nroCheque' => 'required|numeric',
                'fechaCobro' => 'required|date|date_format:Y-m-d|before:' . Carbon::parse($this->fechaRecepcion)->addMonth() . '|after:fechaRecepcion',
                'fechaRecepcion' => 'required|date|date_format:Y-m-d|before:2099-12-31|after:2000-01-01',
                'titular' => 'required',
                'entregadoPor' => 'nullable',
                'entregadoA' => 'nullable',
                'importe' => 'required|numeric',
                'bank_id' => 'required|numeric',
                'tipoCheque' => 'nullable',
            ];
        }
    }

    public function attributes()
    {
        if ($this->tipoCheque == 'emitido') {
            return [
                'nroCheque' => 'número de cheque',
                'fechaEmision' => 'fecha de emision',
                'fechaCobro' => 'fecha de pago',
                'fechaRecepcion' => 'fecha de recepción',
                'bank_id' => 'banco'
            ];
        } else {
            return [
                'nroCheque' => 'número de cheque',
                'fechaEmision' => 'fecha de emision',
                'fechaCobro' => 'fecha de cobro',
                'fechaRecepcion' => 'fecha de recepción',
                'bank_id' => 'banco'
            ];
        }
    }

    public function messages()
    {
        return [
            'fechaEmision.date_format' => 'Introduzca una fecha válida.',
            'fechaCobro.date_format' => 'Introduzca una fecha válida.',
            'fechaCobro.before' => 'La fecha de cobro no puede ser mayor al ' . date('d-m-Y', strtotime(Carbon::parse($this->fechaRecepcion)->addMonth())),
            'fechaRecepcion.date_format' => 'Introduzca una fecha válida.',
        ];
    }
}
