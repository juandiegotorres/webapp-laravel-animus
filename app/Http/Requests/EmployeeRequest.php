<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'nombre' => 'required|min:3',
                        'apellido' => 'required|min:3',
                        'dni' => 'nullable|numeric|digits_between:7,8|unique:employees,dni',
                        'cuil' => 'nullable|numeric|digits_between:10,11|unique:employees,cuil',
                        'tramiteDocumento' => 'nullable|numeric|min:11|unique:employees,tramiteDocumento',
                        'fechaNacimiento' => 'nullable|date|date_format:Y-m-d|before:today|after:1900-01-01',
                        'sexo' => 'nullable',
                        'direccion' => 'nullable|min:3',
                        'telefono' => 'nullable|numeric|min:6',
                        'charge_id' => 'required',
                        'fechaActividadLaboral' => 'nullable|date|date_format:Y-m-d|before:today|after:1900-01-01',
                        'cbu' => 'nullable|numeric|digits:22',
                    ];
                }
            case 'PUT': {
                    return [
                        'nombre' => 'required|min:3',
                        'apellido' => 'required|min:3',
                        'dni' => 'nullable|numeric|digits_between:7,8|unique:employees,dni,' . $this->employee['id'],
                        'cuil' => 'nullable|numeric|digits_between:10,11|unique:employees,cuil,' . $this->employee['id'],
                        'tramiteDocumento' => 'nullable|numeric|min:11|unique:employees,tramiteDocumento,' . $this->employee['id'],
                        'fechaNacimiento' => 'nullable|date|date_format:Y-m-d|before:today|after:1900-01-01',
                        'sexo' => 'nullable',
                        'direccion' => 'nullable|min:3',
                        'telefono' => 'nullable|numeric|min:6',
                        'charge_id' => 'required',
                        'fechaActividadLaboral' => 'nullable|date|date_format:Y-m-d|before:today|after:1900-01-01',
                        'cbu' => 'nullable|numeric|digits:22',
                    ];
                }
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'dni.required' => 'El campo DNI no puede estar vacio.',
            'dni.numeric' => 'El campo DNI no contener puede símbolos ni letras.',
            'dni.digits_between' => 'El campo DNI solo puede contener 7 u 8 caracteres.',
            'dni.unique' => 'Ya existe otro empleado registrado con este DNI.',
            'tramiteDocumento.required' => 'El campo número de trámite no puede estar vacio.',
            'tramiteDocumento.numeric' => 'El campo número de trámite no contener puede símbolos ni letras.',
            'tramiteDocumento.min' => 'El campo número de trámite debe contener al menos 11 caracteres.',
            'tramiteDocumento.max' => 'El campo número de trámite debe contener máximo 15 caracteres.',
            'tramiteDocumento.unique' => 'Ya existe otro empleado registrado con este número de trámite.',
            'fechaNacimiento.required' => 'Debe introducir una fecha de nacimiento.',
            'fechaNacimiento.date' => 'La fecha de nacimiento debe ser una fecha válida.',
            'fechaNacimiento.before' => 'La fecha nacimiento debe ser una fecha anterior a hoy.',
            // 'fechaNacimiento.after' => 'La fecha de nacimiento debe ser una fecha válida.',
            'sexo.required' => 'Debe elegir un sexo respectivo para el empleado.',
            'direccion.required' => 'El campo dirección no puede estar vacio.',
            'direccion.min' => 'El campo dirección debe tener mínimo 3 caracteres.',
            'telefono.required' => 'El campo teléfono no puede estar vacio.',
            'telefono.numeric' => 'El campo teléfono no puede contener símbolos ni letras.',
            'telefono.min' => 'El campo teléfono debe tener mínimo 6 caracteres.',
            'charge_id.required' => 'Debe seleccionar una cargo para el empleado',
            'fechaActividadLaboral.required' => 'Debe introducir una fecha de actividad laboral.',
            'fechaActividadLaboral.date_format' => 'La fecha de actividad laboral debe ser una fecha válida.',
            'fechaActividadLaboral.before' => 'La fecha de actividad laboral debe ser una fecha anterior a hoy.',
            // 'fechaActividadLaboral.after' => 'La fecha de actividad laboral debe ser una fecha válida.',
            'cbu.required' => 'El campo CBU no puede estar vacio.',
            'cbu.numeric' => 'El campo CBU no contener puede símbolos ni letras.',
            'cbu.digits' => 'El campo CBU debe tener 22 caracteres.',
        ];
    }
}
