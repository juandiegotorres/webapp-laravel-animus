<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FacturaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nroFactura' => 'required|numeric',
            'fechaEmision' => 'required|date|date_format:Y-m-d|before:2099-12-31|after:2000-01-01',
            'totalNeto' => 'required|numeric',
            'IVA' => 'required|numeric',
            'total' => 'nullable|numeric',
            'emisor_receptor' => 'nullable',
            'pdf' => 'nullable|mimes:pdf|max:4096',
        ];
    }

    public function attributes()
    {
        return [
            'nroFactura' => 'número de factura',
            'fechaEmision' => 'fecha de emision',
            'totalNeto' => 'total neto',
            'IVA' => 'IVA',
        ];
    }

    public function messages()
    {
        return [
            'fechaEmision.date_format' => 'La fecha de emision debe ser una fecha válida.',
        ];
    }
}
