<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalaryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'employee_id' => 'required',
            'horas' => 'required|numeric|digits_between:1,5',
            'precioHora' => 'required|numeric|integer|digits_between:1,6',
            'total' => 'required|numeric|digits_between:1,12',
            'fechaInicio' => 'required|date|date_format:Y-m-d|before:fechaFin|after:1900-01-01',
            'fechaFin' => 'required|date|date_format:Y-m-d|before:2099-12-31|after:fechaInicio',
            'observaciones' => 'max:500',
        ];
    }

    public function attributes()
    {
        return [
            'employee_id' => 'empleado',
            'precioHora' => 'precio por hora',
            'fechaInicio' => 'fecha de inicio',
            'fechaFin' => 'fecha de fin'
        ];
    }

    public function messages()
    {
        return [
            'fechaInicio.date_format' => 'La fecha de inicio debe ser una fecha válida.',
            'fechaFin.date_format' => 'La fecha de fin debe ser una fecha válida.',
        ];
    }
}
