<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProviderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'nombre' =>  $this->personaEmpresa == 'persona' ? 'required' : 'nullable' . '|min:3',
                        'apellido' =>  $this->personaEmpresa == 'persona' ? 'required' : 'nullable' . '|min:3',
                        'razonSocial' =>  $this->personaEmpresa == 'empresa' ? 'required' : 'nullable',
                        'dni' => 'nullable|unique:providers|integer|digits_between:7,8',
                        'cuit' => 'nullable|unique:providers|integer|digits_between:10,11',
                        'direccion' => 'nullable|min:3',
                        'telefono' => 'nullable|integer|min:6',
                        'email' => 'nullable|email',
                        'cbu' => 'nullable|integer|digits:22',
                        'tipoProveedor' => 'required',
                        'idLocalidad' => 'required',
                        'provincia' => 'required'
                    ];
                }
            case 'PUT': {
                    return [
                        'nombre' =>  $this->personaEmpresa == 'persona' ? 'required' : 'nullable' . '|min:3',
                        'apellido' =>  $this->personaEmpresa == 'persona' ? 'required' : 'nullable' . '|min:3',
                        'razonSocial' =>  $this->personaEmpresa == 'empresa' ? 'required' : 'nullable',
                        'dni' => 'nullable|integer|digits_between:7,8|unique:providers,dni,' . $this->provider['id'],
                        'cuit' => 'nullable|integer|digits_between:10,11|unique:providers,cuit,' . $this->provider['id'],
                        'direccion' => 'nullable|min:3',
                        'telefono' => 'nullable|integer|min:6',
                        'email' => 'nullable|email',
                        'cbu' => 'nullable|integer|digits:22',
                        'tipoProveedor' => 'required',
                        'idLocalidad' => 'required',
                        'provincia' => 'required',
                    ];
                }
            default:
                break;
        }
    }
    public function messages()
    {
        return  [
            'nombreCompleto.required' => 'El campo nombre no puede estar vacio.',
            'nombreCompleto.min' => 'El campo nombre debe tener mínimo 3 caracteres.',
            'dni.required' => 'El campo DNI no puede estar vacio.',
            'dni.numeric' => 'El campo DNI no contener puede símbolos ni letras.',
            'dni.digits_between' => 'El campo DNI solo puede contener 7 u 8 caracteres.',
            'dni.unique' => 'Ya existe otro proveedor registrado con este DNI.',
            'cuit.required' => 'El campo CUIT / CUIL no puede estar vacio.',
            'cuit.numeric' => 'El campo CUIT / CUIL no contener puede símbolos ni letras.',
            'cuit.digits_between' => 'El campo CUIT / CUIL solo puede contener 10 u 11 caracteres.',
            'cuit.unique' => 'Ya existe otro proveedor registrado con este CUIT / CUIL.',
            'telefono.required' => 'El campo teléfono no puede estar vacio.',
            'telefono.numeric' => 'El campo teléfono no puede contener símbolos ni letras.',
            'telefono.min' => 'El campo teléfono debe tener mínimo 6 caracteres.',
            'email.required' => 'El campo email no puede estar vacio.',
            'email.email' => 'Debe introducir una direccion de email válida.',
            'cbu.required' => 'El campo CBU no puede estar vacio.',
            'cbu.numeric' => 'El campo CBU no contener puede símbolos ni letras.',
            'cbu.digits' => 'El campo CBU debe tener 22 caracteres.',
            'direccion.required' => 'El campo dirección no puede estar vacio.',
            'direccion.min' => 'El campo dirección debe tener mínimo 3 caracteres.',
            'idLocalidad.required' => 'Debe seleccionar una localidad',
            'provincia.required' => 'Debe seleccionar una provincia',
            'tipoProveedor.required' => 'El campo tipo de proveedor no puede estar vacio.',
        ];
    }
}
