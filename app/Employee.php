<?php

namespace App;

use App\Charge;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\NoBorradoScope;

class Employee extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'nombreCompleto', 'dni', 'cuil', 'tramiteDocumento',
        'fechaNacimiento', 'sexo', 'direccion', 'telefono', 'charge_id',
        'fechaActividadLaboral', 'cbu', 'estado', 'borrado'
    ];

    public function cargo()
    {
        return $this->belongsTo(Charge::class, 'charge_id');
    }

    public static function nombreCompleto($nombre, $apellido)
    {
        return $apellido . ', ' . $nombre;
    }
}
