<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class DetailPriceList extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'price_list_id',
        'variante_id',
        'precioKg',
        'precioCaja',
        'estado',
        'borrado'
    ];
}
