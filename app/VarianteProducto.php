<?php

namespace App;

use App\Product;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class VarianteProducto extends Model
{
    protected $fillable = [
        'product_id',
        'tipo',
        'kg',
        'cantidad',
        'estado',
        'borrado',
    ];

    protected $table = 'variantes_productos';

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    public function producto()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    const TIPO_CAJA = 0;
    const TIPO_BOLSA = 1;
}
