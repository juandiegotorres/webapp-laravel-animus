<?php

namespace App;

use App\Client;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class Cobro extends Model
{
    protected $fillable = [
        'client_id',
        'fecha',
        'iva',
        'montoTotal',
        'observaciones',
        'estado',
        'borrado',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    public function cliente()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function detalleCobro()
    {
        return $this->hasMany(DetalleCobro::class, 'cobro_id');
    }
}
