<?php

namespace App;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{

    public static function restarCantidadAFacturar($idProveedor, $materiasPrimas, $cantidades)
    {
        //La problematica que habia que resolver era la de que tenemos que actualizar un campo de la base de datos restandole una cantidad que el usuario ha elegido
        //el problema es que si la cantidad es mas grande que el campo inicial hay que actualizar otro registro de la base de datos con ese id de materia prima y ese id proveedor
        // y restarle a ese registro el faltante


        //                           tabla: detail_raw_material_entries
        //  ╔════╦═════════════════════════╦═════════════════╦════════════╦══════════════════╗
        // ║ id ║ raw_material_entries_id ║ raw_material_id ║ cantidadKg ║ faltanteFacturar ║
        // ╠════╬═════════════════════════╬═════════════════╬════════════╬══════════════════╣
        // ║ 1  ║ 1                       ║ 1               ║ 2000       ║ 1500             ║
        // ╠════╬═════════════════════════╬═════════════════╬════════════╬══════════════════╣
        // ║ 2  ║ 1                       ║ 4               ║ 4000       ║ 3000             ║
        // ╠════╬═════════════════════════╬═════════════════╬════════════╬══════════════════╣
        // ║ 3  ║ 2                       ║ 1               ║ 1000       ║ 1000             ║
        // ╚════╩═════════════════════════╩═════════════════╩════════════╩══════════════════╝

        //Ej en este caso tengo la cantidad de Kg inciciales y faltante facturar que solo se va a decrementar por un descarte de la materia prima o por una compra
        //En este caso por ejemplo si yo quiero facturar 2000kg tengo que restarle 1500 al registro con id 1 y 500 mas al registro con id 3. Eso es lo que resuelve este algoritmo

        //                             tabla: detail_raw_material_entries
        //  ╔════╦═════════════════════════╦═════════════════╦════════════╦══════════════════╗
        // ║ id ║ raw_material_entries_id ║ raw_material_id ║ cantidadKg ║ faltanteFacturar ║
        // ╠════╬═════════════════════════╬═════════════════╬════════════╬══════════════════╣
        // ║ 1  ║ 1                       ║ 1               ║ 2000       ║ 0                ║
        // ╠════╬═════════════════════════╬═════════════════╬════════════╬══════════════════╣
        // ║ 2  ║ 1                       ║ 4               ║ 4000       ║ 3000             ║
        // ╠════╬═════════════════════════╬═════════════════╬════════════╬══════════════════╣
        // ║ 3  ║ 2                       ║ 1               ║ 1000       ║ 500              ║
        // ╚════╩═════════════════════════╩═════════════════╩════════════╩══════════════════╝


        //↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑  Y este deberia ser el resultado  ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ 


        return DB::transaction(function () use ($idProveedor, $materiasPrimas, $cantidades) {
            try {
                //Si es menor significa que hay valores repetidos
                if (count(array_unique($materiasPrimas)) < count($materiasPrimas)) {
                    throw new Exception('No pueden haber materias primas repetidas');
                }
                for ($i = 0; $i < count($materiasPrimas); $i++) {
                    $diferencia = 0;

                    //Trae todos los registros de la materia prima seleccionada por el usiario desde 
                    //detalle de ingreso. Donde selecciono el id de la materia prima, el fantante a facturar
                    //y el id del detalle
                    $consultaDB = DB::table('detail_raw_material_entries')
                        ->join('raw_material_entries', 'detail_raw_material_entries.raw_material_entries_id', '=', 'raw_material_entries.id')
                        ->where('raw_material_entries.provider_id', '=', $idProveedor)
                        ->where('detail_raw_material_entries.raw_material_id', $materiasPrimas[$i])
                        ->where('detail_raw_material_entries.faltanteFacturar', '>', 0)
                        ->select('detail_raw_material_entries.raw_material_id', 'faltanteFacturar', 'detail_raw_material_entries.id')
                        ->orderBy('raw_material_id')
                        ->get();

                    if (!empty($consultaDB)) {
                        $balance =  abs($diferencia);
                        for ($j = 0; $j < count($consultaDB); $j++) {
                            if ($j == 0) {
                                $diferencia = $consultaDB[$j]->faltanteFacturar - $cantidades[$i];
                            } else {
                                $diferencia = $consultaDB[$j]->faltanteFacturar - abs($diferencia);
                            }
                            if ($balance > 0) {
                                DB::table('detail_raw_material_entries')
                                    ->whereId($consultaDB[$j]->id)
                                    ->update(['faltanteFacturar' => $diferencia]);
                            } else {
                                if ($diferencia < 0) {
                                    DB::table('detail_raw_material_entries')
                                        ->whereId($consultaDB[$j]->id)
                                        ->update(['faltanteFacturar' => 0]);
                                } else {
                                    DB::table('detail_raw_material_entries')
                                        ->whereId($consultaDB[$j]->id)
                                        ->update(['faltanteFacturar' =>  $diferencia]);
                                    break;
                                }
                            }
                        }
                    }
                }
                //
            } catch (\Exception $e) {
                DB::rollback();
                return  $e->getMessage();
            }
        });
    }

    public function valoresRepetidos($materiasPrimas, $cantidades)
    {

        // Esta funcion surge del problema que desde el front, el usuario haya agregado una materia prima mas de una vez con distintas cantidades a comprar/ingresar respectivamente
        // Por lo que esta funcion lo que hace es recorrer el array y sumar todas las cantidades de las materias primas que estan repetidas

        // Obtengo los valores unicos dentro del array de ids de materias primas, y los valores duplicados
        $valoresUnicos = array_unique($materiasPrimas);
        $valoresDuplicados = array_diff_assoc($materiasPrimas, $valoresUnicos);

        $totalDuplicadas = 0;
        $suma = array();

        foreach ($valoresUnicos as $idUnico => $valorUnico) {
            //Agrego al array un registro con el id de la materia prima y la cantidad
            $suma[$valorUnico] = $cantidades[$idUnico];

            //Recorro el array de los valores que estan duplicados
            foreach ($valoresDuplicados as $idDuplicado => $valorDuplicado) {
                //Si se repiten con el array de ids unicos
                if ($valorUnico == $valorDuplicado) {
                    //Si no existe el valor dentro de larray de las sumas  
                    if (isset($suma[$valorDuplicado])) {
                        $suma[$valorUnico] += $cantidades[$idDuplicado];
                    } else {
                        $totalDuplicadas = 0;
                        $totalDuplicadas += $cantidades[$idUnico];
                        $suma[$valorUnico] = $totalDuplicadas += $cantidades[$idDuplicado];
                    }
                }
            }
            //Separo los valores y los vuelvo a asignar a las variables iniciales
            $materiasPrimas = array_keys($suma);
            $cantidades = array_values($suma);

            return array($materiasPrimas, $cantidades);
        }
    }

    public function scopeHistorialCompra($query, $tabla)
    {
        $query->join('providers', $tabla . '.provider_id', '=', 'providers.id')
            ->select($tabla . '.id', 'fecha', 'total', 'provider_id', 'nombreCompleto')
            ->where($tabla . '.borrado', 0)
            ->orderBy('fecha', 'DESC');
    }
}
