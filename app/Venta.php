<?php

namespace App;

use App\DetalleVenta;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $fillable = [
        'client_id',
        'fecha',
        'iva',
        'total',
        'estado',
        'borrado'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    public function detalleVenta()
    {
        return $this->hasMany(DetalleVenta::class, 'id_venta');
    }

    public function cliente()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
