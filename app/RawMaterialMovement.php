<?php

namespace App;

use App\RawMaterial;
use App\RawMaterialEntry;
use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class RawMaterialMovement extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'id_ingreso',
        'fechaMovimiento',
        'porcentaje',
        'cantidadKg',
        'observaciones',
        'usuarioGalpon',
        'estado',
        'borrado',
    ];

    public function ingreso()
    {
        return $this->belongsTo(RawMaterialEntry::class, 'id_ingreso');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuarioGalpon');
    }
}
