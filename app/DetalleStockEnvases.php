<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleStockEnvases extends Model
{
    protected $fillable = [
        'id_stock_envases',
        'id_envase',
        'cantidad',
    ];
}
