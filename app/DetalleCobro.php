<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCobro extends Model
{
    protected $fillable = [
        'cobro_id',
        'metodo_pago_id',
        'cheque_id',
        'monto',
    ];
}
