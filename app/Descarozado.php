<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descarozado extends Model
{
    public function empleado()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
