<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = [
        'id', 'nombreCompleto', 'dni', 'cuit', 'direccion', 'telefono', 'email',
        'idLocalidad', 'cbu', 'tipoProveedor', 'estado', 'borrado'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    public static function nombreCompleto($nombre, $apellido)
    {
        return $apellido . ', ' . $nombre;
    }

    public function scopeDeMateriaPrima($query)
    {
        return $query->where('tipoProveedor', 'like', '%MP%');
    }

    public function localidad()
    {
        return $this->belongsTo(Locality::class, 'idLocalidad');
    }

    public function scopeMateriaPrima($query)
    {
        return $query->where('tipoProveedor', 'like', '%MP%');
    }
}
