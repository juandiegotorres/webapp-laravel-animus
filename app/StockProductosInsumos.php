<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class StockProductosInsumos extends Model
{
    protected $fillable = [
        'id_variante_insumo',
        'tipo_variante_insumo',
        'tipo_movimiento',
        'fecha',
        'descripcion',
        'cantidad',
        'estado',
        'borrado',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }


    const ES_VARIANTE = 0;
    const ES_INSUMO = 1;

    const MOVIMIENTO_VENTA_PRODUCTO = 'VP';
    const MOVIMIENTO_RESTOCK_PRODUCTO = 'RP';
    const MOVIMIENTO_COMPRA_INSUMO = 'CI';
    const MOVIMENTO_RESTA_INSUMO = 'RI';
}
