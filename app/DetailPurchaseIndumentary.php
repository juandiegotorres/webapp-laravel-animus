<?php

namespace App;

use App\Indumentary;
use App\PurchaseIndumentary;
use Illuminate\Database\Eloquent\Model;

class DetailPurchaseIndumentary extends Model
{
    protected $fillable = [
        'purchase_indumentary_id',
        'indumentary_id',
        'cantidad',
        'precioUnitario',
        'totalParcial',
        'estado',
        'borrado'
    ];

    public function maestroCompra()
    {
        return $this->belongsTo(PurchaseIndumentary::class, 'purchase_indumentary_id');
    }

    public function indumentaria()
    {
        return $this->belongsTo(Indumentary::class, 'indumentary_id');
    }
}
