<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleOrdenVenta extends Model
{
    protected $fillable = [
        'id_orden_venta',
        'id_variante',
        'cantidad',
        'precio',
    ];

    public function variante()
    {
        return $this->belongsTo(VarianteProducto::class, 'id_variante');
    }
}
