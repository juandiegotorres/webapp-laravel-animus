<?php

namespace App;

use App\Scopes\NoBorradoScope;
use Illuminate\Database\Eloquent\Model;

class Indumentary extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new NoBorradoScope);
    }

    protected $fillable = [
        'nombre',
        'tipo_modelo',
        'modelo',
        'cantidad',
        'estado',
        'borrado'
    ];
}
