<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For detailed instructions you can look the title section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'title' => '',
    'title_prefix' => 'Hfrut |',
    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Favicon
    |--------------------------------------------------------------------------
    |
    | Here you can activate the favicon.
    |
    | For detailed instructions you can look the favicon section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'use_ico_only' => false,
    'use_full_favicon' => false,

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For detailed instructions you can look the logo section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'logo' => '<b>Hfrut</b>',
    'logo_img' => '../img/logo.png',
    'logo_img_class' => 'brand-image img-circle ',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-x',
    'logo_img_alt' => 'Hfrut',

    /*
    |--------------------------------------------------------------------------
    | User Menu
    |--------------------------------------------------------------------------
    |
    | Here you can activate and change the user menu.
    |
    | For detailed instructions you can look the user menu section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'usermenu_enabled' => false,
    'usermenu_header' => false,
    'usermenu_header_class' => 'bg-primary',
    'usermenu_image' => false,
    'usermenu_desc' => false,
    'usermenu_profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For detailed instructions you can look the layout section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => null,
    'layout_fixed_navbar' => null,
    'layout_fixed_footer' => null,
    'layout_dark_mode' => null,

    /*
    |--------------------------------------------------------------------------
    | Authentication Views Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the authentication views.
    |
    | For detailed instructions you can look the auth classes section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'classes_auth_card' => 'card-outline card-primary',
    'classes_auth_header' => '',
    'classes_auth_body' => '',
    'classes_auth_footer' => '',
    'classes_auth_icon' => '',
    'classes_auth_btn' => 'btn-flat btn-primary',

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For detailed instructions you can look the admin panel classes here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_wrapper' => '',
    'classes_content_header' => '',
    'classes_content' => '',
    'classes_sidebar' => 'sidebar-dark-success elevation-4',
    'classes_sidebar_nav' => 'nav-child-indent',
    'classes_topnav' => 'navbar-white navbar-light',
    'classes_topnav_nav' => 'navbar-expand',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For detailed instructions you can look the sidebar section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'sidebar_mini' => 'lg',
    'sidebar_collapse' => false,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => false,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For detailed instructions you can look the right sidebar section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => false,
    'right_sidebar_push' => false,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For detailed instructions you can look the urls section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'use_route_url' => false,
    'dashboard_url' => 'home',
    'logout_url' => 'logout',
    'login_url' => 'login',
    'register_url' => 'register',
    'password_reset_url' => 'password/reset',
    'password_email_url' => 'password/email',
    'profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For detailed instructions you can look the laravel mix section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Other-Configuration
    |
    */

    'enabled_laravel_mix' => false,
    'laravel_mix_css_path' => 'css/app.css',
    'laravel_mix_js_path' => 'js/app.js',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Menu-Configuration
    |
    */

    'menu' => [
        // Navbar items:
        [
            'text'         => 'search',
            'topnav_right' => true,
        ],
        [
            'type'         => 'fullscreen-widget',
            'topnav_right' => true,
        ],

        // Sidebar items:
        // [
        //     'type' => 'sidebar-menu-search',
        //     'text' => 'search',
        // ],
        // [
        //     'text' => 'blog',
        //     'url'  => 'admin/blog',
        //     'can'  => 'manage-blog',
        // ],
        // [
        //     'text'        => 'pages',
        //     'url'         => 'admin/pages',
        //     'icon'        => 'far fa-fw fa-file',
        //     'label'       => 4,
        //     'label_color' => 'success',
        // ],
        // ['header' => 'account_settings'],


        [
            'text' => 'Ventas',
            'icon' => 'fas fa-fw fa-share',
            'can' => 'vender',
            'submenu' => [
                [
                    'text' => 'Vender',
                    'route'  => 'historial-ventas.index',
                    'icon' => 'fas fa-fw fa-cash-register',
                    'can' => 'vender'
                ],
                [
                    'text' => 'Ordenes de venta',
                    'route'  => 'orden-venta.index',
                    'icon' => 'fas fa-fw fa-cash-register',
                    'can' => 'vender'
                ],
                [
                    'text' => 'Clientes',
                    'route'  => 'clients.index',
                    'icon' => 'fas fa-fw fa-comment-dollar',
                    'can' => 'clientes'
                ],
                [
                    'text' => 'Cobros a clientes',
                    'route'  => 'cobros.index',
                    'icon' => 'fas fa-fw fa-hand-holding-usd',
                    'can' => 'cobros'
                ],
                [
                    'text' => 'Balance ventas-cobros',
                    'route'  => 'balance-cliente.index',
                    'icon' => 'fas fa-fw fa-balance-scale',
                    'can' => 'balance-cobros'
                ],
                [
                    'text' => 'Listas de precios',
                    'route'  => 'price-lists.index',
                    'icon' => 'fas fa-tags',
                    'can' => 'lista-precios'
                ],
            ]
        ],

        [
            'text'    => 'Compras',
            'icon'    => 'fas fa-fw fa-share',
            'submenu' => [
                [
                    'text' => 'Compras',
                    'route'  => 'purchase-history.index',
                    'icon' => 'fas fa-fw fa-shopping-cart',
                    'can' => 'compras'
                ],
                [
                    'text' => 'Ordenes de compra',
                    'route'  => 'orden-compra.listar',
                    'icon' => 'fas fa-fw fa-shopping-cart',
                    'can' => 'compras'
                ],
                [
                    'text' => 'Proveedores',
                    'route'  => 'providers.index',
                    'icon' => 'fas fa-fw fa-truck',
                    'can' => 'proveedores'
                ],
                [
                    'text' => 'Pagos a proveedores',
                    'route'  => 'pagos.index',
                    'icon' => 'fas fa-fw fa-credit-card',
                    'can' => 'pagos'
                ],
                [
                    'text' => 'Balance compras-pagos',
                    'route'  => 'balance-proveedor.index',
                    'icon' => 'fas fa-fw fa-balance-scale',
                    'can' => 'balance-compras'
                ],
            ]
        ],


        [
            'text' => 'Productos',
            'icon' => 'fas fa-fw fa-share',
            'can' => '',
            'submenu' => [
                [
                    'text' => 'Productos',
                    'route'  => 'products.index',
                    'icon' => 'fas fa-leaf',
                    'can' => 'productos'
                ],
                [
                    'text' => 'Materias Primas',
                    'route'  => 'raw-material.index',
                    'icon' => 'fas fa-fw fa-seedling',
                    'can' => 'materias-primas'
                ],
                [
                    'text' => 'Insumos-Servicios-Indumentaria',
                    'route'  => 'supplie-service.index',
                    'icon' => 'fas fa-fw fa-people-carry',
                    'can' => 'insumos-servicios-indumentarias'
                ],
                [
                    'text' => 'Listas de precios',
                    'route'  => 'price-lists.index2',
                    'icon' => 'fas fa-tags',
                    'can' => 'lista-precios'
                ],
            ]
        ],

        [
            'text'    => 'Personal',
            'icon'    => 'fas fa-fw fa-share',
            'can' => 'empleados',
            'submenu' => [
                [
                    'text' => 'Empleados',
                    'route'  => 'employees.index',
                    'icon' => 'fas fa-fw fa-address-card',
                    'can' => 'empleados'
                ],
                [
                    'text' => 'Cargos',
                    'route'  => 'charges.index',
                    'icon' => 'fas fa-fw fa-user-cog',
                    'can' => 'cargos'
                ],
                [
                    'text' => 'Sueldos',
                    'route'  => 'salaries.index',
                    'icon' => 'fas fa-fw fa-money-bill-alt',
                    'can' => 'sueldos'
                ],
                [
                    'text' => 'Pago sueldos',
                    'route'  => 'salary-payments.index',
                    'icon' => 'fas fa-fw fa-money-bill-alt',
                    'can' => 'pago-sueldos'
                ],
                [
                    'text' => 'Balance Sueldos',
                    'route'  => 'salary-balance.index',
                    'icon' => 'fas fa-fw fa-money-bill-alt',
                    'can' => 'balance-sueldos'
                ],
                [
                    'text' => 'Entregas de indumentaria',
                    'route'  => 'distribution-history.index',
                    'icon' => 'fas fa-fw fa-users',
                    'can' => 'entrega-indumentaria'
                ],
            ]
        ],


        [
            'text'    => 'Balance impositivo',
            'icon'    => 'fas fa-fw fa-share',
            'can' => 'facturas',
            'submenu' => [
                [
                    'text' => 'Facturas',
                    'route'  => 'facturas.index',
                    'icon' => 'fas fa-file-invoice-dollar',
                    'can' => 'facturas'
                ],
                [
                    'text' => 'Balance impositivo',
                    'route'  => 'balance-impositivo.index',
                    'icon' => 'fas fa-file-invoice-dollar',
                    'can' => 'balance-impositivo'
                ],
            ]
        ],


        // [
        //     'text'    => 'Usuarios',
        //     'icon'    => 'fas fa-fw fa-share',
        //     'can' => 'usuarios',
        //     'submenu' => [,
        //         [
        //             'text' => 'Roles',
        //             'route'  => 'roles.index',
        //             'icon' => 'fas fa-user-cog',
        //             'class' => 'ml-4'
        //         ],
        //     ]
        // ],

        [
            'text'    => 'Galpón acopio',
            'icon'    => 'fas fa-fw fa-share',
            'submenu' => [
                [
                    'text' => 'Ingreso de materia prima',
                    'route'  => 'ingresoMP.index',
                    'icon' => 'fas fa-money-check-alt',
                    'can' => 'ingreso-materia-prima'
                ],
                [
                    'text' => 'Totales de ingresos',
                    'route'  => 'totales-ingreso.index',
                    'icon' => 'fas fa-money-check-alt',
                    'can' => 'totales-ingresos'
                ],
                [
                    'text' => 'Proveedores de materia prima',
                    'route'  => ['providers.index', ['tipo' => 'materia-prima']],
                    'icon' => 'fas fa-fw fa-truck',
                    'can' => 'proveedores'
                ],
                [
                    'text' => 'Materias Primas',
                    'route'  => 'raw-material.index2',
                    'icon' => 'fas fa-fw fa-seedling',
                    'can' => 'materias-primas'
                ],
                [
                    'text' => 'Entregas descarozado',
                    'route'  => 'entrega-descarozado.index',
                    'icon' => 'fas fa-fw fa-seedling',
                    'can' => 'entregas-descarozado'
                ],
                [
                    'text' => 'Envases',
                    'route'  => 'envases.index',
                    'icon' => 'fas fa-box-open',
                    'can' => 'envases'
                ],
            ]
        ],

        [
            'text' => 'Cheques',
            'route'  => 'cheques.index',
            'icon' => 'fas fa-money-check-alt',
            'can' => 'cheques'
        ],

        [
            'text' => 'Stock',
            'route'  => 'stock.index',
            'icon' => 'fas fa-list-ol',
            'can' => 'stock-envases'
        ],
        [
            'text' => 'Liquidaciones',
            'route'  => 'liquidaciones.index',
            'icon' => 'fas fa-search-dollar',
            'can' => 'liquidacion'
        ],
        [
            'text' => 'Usuarios',
            'route'  => 'users.index',
            'icon' => 'fas fa-fw fa-users',
            'can' => 'usuarios',
        ],








        // [
        //     'text'    => 'multilevel',
        //     'icon'    => 'fas fa-fw fa-share',
        //     'submenu' => [
        //         [
        //             'text' => 'level_one',
        //             'url'  => '#',
        //         ],
        //         [
        //             'text'    => 'level_one',
        //             'url'     => '#',
        //             'submenu' => [
        //                 [
        //                     'text' => 'level_two',
        //                     'url'  => '#',
        //                 ],
        //                 [
        //                     'text'    => 'level_two',
        //                     'url'     => '#',
        //                     'submenu' => [
        //                         [
        //                             'text' => 'level_three',
        //                             'url'  => '#',
        //                         ],
        //                         [
        //                             'text' => 'level_three',
        //                             'url'  => '#',
        //                         ],
        //                     ],
        //                 ],
        //             ],
        //         ],
        //         [
        //             'text' => 'level_one',
        //             'url'  => '#',
        //         ],
        //     ],
        // ],
        // ['header' => 'labels'],
        // [
        //     'text'       => 'important',
        //     'icon_color' => 'red',
        //     'url'        => '#',
        // ],
        // [
        //     'text'       => 'warning',
        //     'icon_color' => 'yellow',
        //     'url'        => '#',
        // ],
        // [
        //     'text'       => 'information',
        //     'icon_color' => 'cyan',
        //     'url'        => '#',
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For detailed instructions you can look the menu filters section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Menu-Configuration
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\DataFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For detailed instructions you can look the plugins section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Plugins-Configuration
    |
    */

    'plugins' => [
        'Datatables' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        'Select2' => [
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        'Chartjs' => [
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        'Sweetalert2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        'Pace' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Livewire
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Livewire support.
    |
    | For detailed instructions you can look the livewire here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Other-Configuration
    */

    'livewire' => false,
];
